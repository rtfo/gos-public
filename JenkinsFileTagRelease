///////////////////////////////////////////////////////////
//
//
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Map of
// Service : Version (commit hash / registry tag)
///////////////////////////////////////////////////////////
SERVICES = [
			"GOS.Website" 		: ""
			,"GOS.Identity.API"	: ""
			,"GOS.Organisations.API"	: ""
			,"GOS.GhgFuels.API" : ""
			,"GOS.GhgApplications.API" : ""
			,"GOS.ReportingPeriods.API": ""
			,"GOS.GhgCalculations.API": ""
			,"GOS.ClamAV": ""
			,"GOS.SystemParameters.API": ""
			,"GOS.GhgBalanceView.API": ""
			,"GOS.GhgBalanceOrchestrator.API": ""
			,"GOS.GhgLedger.API": ""
			,"GOS.RtfoApplications.API": ""
			,"GOS.RtfoApplications.Scheduler": ""
			,"GOS.GhgMarketplace.API": ""
			,"GOS.DBAnonymiser": ""
			]

////////////////////////////////////////////////
//
// The structure in the tracking repo is
// branches/<branchname>/<servicename>
//
// or
//
// environment/<ClusterRegion>/<Cluster>/<NameSpace>/<servicename>
//
//////////////////////////////////////////////////
def String GetTrackingRepoSourceFileName(String CLUSTER_REGION, String CLUSTER, String SOURCE, String SERVICE)
{
	if(SOURCE.toLowerCase().startsWith("branch:"))
	{
		TRACKING_REPO_FILE = "branches/${SOURCE.split(":")[1]}/${SERVICE.toLowerCase()}"
	}
	else if (SOURCE.toLowerCase().startsWith("env:"))
	{
		TRACKING_REPO_FILE = "environment/${CLUSTER_REGION}/${CLUSTER}/${SOURCE.toLowerCase().split(":")[1]}/${SERVICE.toLowerCase()}"
		}
	return TRACKING_REPO_FILE
}

pipeline {
	agent {
		label 'docker'
	}

	parameters {
		string(defaultValue: "europe-west2-a", description: "Cluster Region", name : 'CLUSTER_REGION')
		string(defaultValue: "rtfocluster", description: "Cluster Name", name : 'CLUSTERNAME')
		string(defaultValue: "branch:master", description: "Source branch:<branchname> or env:<namespace>", name : 'SOURCE')
		string(defaultValue: "1.0.0", description: "Release Tag", name : 'RELEASE_TAG')
		booleanParam(defaultValue: false, description: "Force a tag of images. WARNING: This will update exiting deployments and could potentially result in a PRODUCTION release", name : 'FORCE')

	}

	environment {
		SLACK_TOKEN = credentials("ROSGOS_SLACK_TOKEN")
		REGISTRY_NAME = "eu.gcr.io"
		PROJECT_ID = "dft-gos"
	}

	stages {
	   stage('Preparation') { // for display purposes
		steps {
			checkout scm
		}
	   }

	   	////////////////////////////////////
		//
		// Log into Azure with a service
		// principal account and set the
		// default subscription (incase
		// there are multiple)
		//
		////////////////////////////////////
		stage('GCP login') {
			environment {
				SERVICE_ACCOUNT = credentials('GCP_SERVICE_ACCOUNT')
			}
			steps {
				script {
					sh "gcloud auth activate-service-account --key-file '$SERVICE_ACCOUNT' --project ${PROJECT_ID}"
				}
			}
	   }

		/////////////////////////////////////////////////////////
		//
		// Collate the versions of each microservice from the
		// tracking repo for the source namespace/environment
		//
		/////////////////////////////////////////////////////////
		stage ('Read Tracking Repo')
		{
			environment {
				TRACKING_REPO = credentials('RTFO_K8S_TRACKING_REPO')
			}
			steps
			{
				git credentialsId: "346c4743-d78e-484d-8998-f17a2f90f5a9", url: "${TRACKING_REPO}"

				script
				{
					SERVICES.keySet().each { srv ->
						TRACKING_REPO_FILE = GetTrackingRepoSourceFileName(CLUSTER_REGION, CLUSTERNAME, SOURCE,srv)

						//if there's a specific GIT HASH for this service (i.e it's one we've built)
						if ( fileExists(TRACKING_REPO_FILE))
						{
							tracking_version = sh ( script: "cat ${TRACKING_REPO_FILE}", returnStdout: true)
							IMAGE_GIT_HASH = tracking_version.split("=")[1]
							SERVICES.put(srv,IMAGE_GIT_HASH)
						}
					}
					echo "Service Versions are"
					SERVICES.each  { srv ->
							println srv
					}
				}
			}
		}

		/////////////////////////////////////////////////////////
		//
		// Parse and Update the Kubernetes config for each service
		// setting the appropriate image version/tag, jenkins build
		// number and target namespace then call kubectl apply to
		// deploy to the cluster
		//
		/////////////////////////////////////////////////////////
	  stage('Apply Tag') {
			steps {
				script
				{
					try
					{
						SERVICES.each{ key, value ->
							if (value?.trim())
							{
								TARGET_IMAGE="${REGISTRY_NAME}/${PROJECT_ID}/" + key.toLowerCase()
								CURRENT_TAG=sh(returnStdout:  true, script: "gcloud container images list-tags --filter=\"tags~${RELEASE_TAG}\" ${TARGET_IMAGE}")
								if( !CURRENT_TAG || FORCE ) 
								{
									SOURCE_TAG="${REGISTRY_NAME}/${PROJECT_ID}/" + key.toLowerCase() + ":" + value
									TARGET_TAG="${TARGET_IMAGE}:${RELEASE_TAG}"
									println ("Tagging ${SOURCE_TAG} with ${TARGET_TAG}")
									sh "docker pull ${SOURCE_TAG}"
									sh "docker tag ${SOURCE_TAG} ${TARGET_TAG}"
									sh "docker push ${TARGET_TAG}"
								} else {
									error("Build failed as tag: ${RELEASE_TAG} already exits for " + key.toLowerCase())
								}
							}
							else
							{
								println ("Skipping unset ${key}")
							}
						}
					} 
					catch (e) 
					{
						echo "${e}"
						currentBuild.result = 'UNSTABLE'
						return
					}
				}
			}
	   }
	}
	post {
		success {
			slackSend (color: '#00FF00', channel: '#gosbuildanddeploy', message: "Successfully Tagged Release in ${SOURCE} with ${RELEASE_TAG} at ${CLUSTER_REGION}/${CLUSTERNAME}", teamDomain: 'rosgos', token: "${env.SLACK_TOKEN}")
		}
		failure {
			slackSend (color: '#FF0000', channel: '#gosbuildanddeploy', message: "Failed to apply release Tag to ${SOURCE} at ${CLUSTER_REGION}/${CLUSTERNAME} (${env.BUILD_URL})", teamDomain: 'rosgos', token: "${env.SLACK_TOKEN}")
		}
	}
}
