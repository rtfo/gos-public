# Manage Motor Fuel Greenhouse Gas Emissions Service #
 
The Department for Transport's GOV.UK Service for managing Motor Fuel Greenhouse Gas Emissions.

## Licence ##

[MIT License](LICENCE)

## Solution Notes ##

### Setup notes ###

* Install Docker for windows.
* Run visual studio as admin user to ensure correct permissions have been given to docker. 
* Set "docker-compose" as the startup project.
* Ensure a shared drive is configured in docker for windows. You might need to reset to factory settings to make sure credentials are set!

### Database migrations ###

Entity Framework code first migration are used for database scripting. New database scripts (for new services) can be created using the package manager console using the 'Add-Migration InitialCreate' command, which 
will create a 'Migrations' folder and some migration classes for the DBContext specialisation.

To enable automatic migrations the context.Database.Migrate() call can be made on service startup. It should be noted that the migration will fail with a message similar to "Unable to create an object of 
type 'GosIdentityDataContext'. Add an implementation of 'IDesignTimeDbContextFactory<GosIdentityDataContext>' to the project, or see https://go.microsoft.com/fwlink/?linkid=851728 for additional patterns 
supported at design time". This can be caused if the context.Database.Migrate() method is called within startup - uncommenting this or wrapping in a try-catch may resolve this issue.

For migrations to work for PostgreSQL, the following assemblies are required:
* Microsoft.EntityFrameworkCore.Tools
* Microsoft.EntityFrameworkCore.PostgreSQL
* Microsoft.EntityFrameworkCore.PostgreSQL.Design

To create a new migration set the Solution Configuration to `Migration` (rather than Debug or Release). Select the project that the migration is for. 
Then in the Package Manager Console, set the project and run `Add-Migration <migration name>`.

### Docker clients ###

* A very simple PostgreSQL client is available when docker is running in development using http://localhost:8080. 
* A very simple MongoDb client is available when docker is running in development using http://localhost:8085.
* The RabbitMQ management GUI is available when docker is running in development using http://localhost:8086.

### Service APIs ###

* Website - http://localhost:28572.

Swagger API documentation is provided for each service when docker is running in development:

* Identity API - http://localhost:9000.
* Organisations API - http://localhost:9001.
* Reporting Periods API - http://localhost:9002.
* Fuel Types API - http://localhost:9003.
* Applications API - http://localhost:9004.
* System Parameters API - http://localhost:9005.
* GHG Calculations API - http://localhost:9006.
* GHG Balance Orchestrator API - http://localhost:9007.
* GHG Balance View API - http://localhost:9008.
* GHG RTFO Applications API - http://localhost:9009.
* GHG Ledger API - http://localhost:9010.
* GHG Marketplace API - http://localhost:9012.
* Notifications API - http://localhost:9013.

* RTFO Applications Scheduler (Not a REST API, just runs in the background and so no swagger) - http://localhost:9011

### Service Health checks ###

Health checks for readiness and liveness are available for each service at ~/health/ready and ~/health/live.

### Troubleshooting ### 

* Sometimes services, such as rabbitMQ, don't corectly start up because not enough disk space has been assigned to the docker engine (this can be checked in Kitematic or through the command line). Increase the space using docker->settings->advances->"Disk image max size". At the time of writing, 144GB was enough disk space to comfortably run all containers.
* Occasionally debugging locally using docker runs very slowly or not start up services. If this is the case try deleting the C:/Users/[username]/vsdbg folder and restarting docker. 