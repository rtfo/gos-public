﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Configuration;
using DfT.GOS.Website.Models.CreditsViewModels;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Website.Models.ApplicationsViewModels;
using DfT.GOS.Web.Models;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using DfT.GOS.UnitTest.Common.Attributes;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalance.Common.Models;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the credits contoller
    /// </summary>
    [TestClass]
    public class CreditsControllerTest
    {
        #region Constants

        private const int UserOrganisationId = 50;
        private const int ObligationPeriodId13 = 13;
        private const int ApplicationId1 = 1;
        private const int FuelType1 = 1;

        #endregion Constants

        #region Properties

        private CreditsController Controller { get; set; }
        private Mock<IOptions<AppSettings>> MockOptions { get; set; }
        private Mock<IApplicationsService> MockApplicationsService { get; set; }
        private Mock<IReviewApplicationsService> MockReviewApplicationsService { get; set; }
        private Mock<IGhgFuelsService> MockGhgFuelsService { get; set; }
        private Mock<IOrganisationsService> MockOrganisationsService { get; set; }
        private Mock<ICalculationsService> MockCalculationsService { get; set; }
        private Mock<IReportingPeriodsService> MockReportingPeriodsService { get; set; }
        private Mock<ITokenService> MockTokenService { get; set; }
        private Mock<IBalanceViewService> MockBalanceViewService { get; set; }

        #endregion Properties}

        #region Initialize

        /// <summary>
        /// Initialises before each test
        /// </summary>
        [TestInitialize]
        public void Intialize()
        {
            this.MockOptions = new Mock<IOptions<AppSettings>>();
            this.MockReviewApplicationsService = new Mock<IReviewApplicationsService>();
            this.MockApplicationsService = new Mock<IApplicationsService>();
            this.MockGhgFuelsService = new Mock<IGhgFuelsService>();
            this.MockCalculationsService = new Mock<ICalculationsService>();
            this.MockOrganisationsService = new Mock<IOrganisationsService>();
            this.MockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            this.MockTokenService = new Mock<ITokenService>();
            this.MockBalanceViewService = new Mock<IBalanceViewService>();

            this.Controller = new CreditsController(this.MockOptions.Object
                , this.MockReviewApplicationsService.Object
                , this.MockApplicationsService.Object
                , this.MockGhgFuelsService.Object
                , this.MockCalculationsService.Object
                , this.MockOrganisationsService.Object
                , this.MockReportingPeriodsService.Object
                , this.MockTokenService.Object
                , this.MockBalanceViewService.Object);

            // - user principal
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", Guid.NewGuid().ToString()));
            claims.Add(new Claim("OrganisationId", UserOrganisationId.ToString()));

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialize

        #region Issue Credits Tests

        /// <summary>
        /// Tests that the Issue Credits method returns the correct result when NULL applications are returned by the service
        /// </summary>
        [TestMethod]
        public async Task IssueCredits_Returns_Result_With_Null_Applications()
        {
            //arrange
            this.SetupAppSettings();
            this.SetupNullApplicationsForIssue();

            //act
            var result = await this.Controller.Issue(null, 1);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("Issue", ((ViewResult)result).ViewName);

            var model = (PagedIssueCreditsViewModel)((ViewResult)result).Model;
            Assert.AreEqual(0, model.Applications.TotalResults);
            Assert.AreEqual(5, model.Applications.MaximumPagesToDisplay);
            Assert.AreEqual(0, model.Applications.PageCount);
        }

        /// <summary>
        /// Tests that the Issue Credits method returns the correct result when Zero applications are returned by the service
        /// </summary>
        [TestMethod]
        public async Task IssueCredits_Returns_Result_With_Zero_Applications()
        {
            //arrange
            this.SetupAppSettings();
            this.SetupZeroApplicationsForIssue();

            //act
            var result = await this.Controller.Issue(null, 1);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("Issue", ((ViewResult)result).ViewName);

            var model = (PagedIssueCreditsViewModel)((ViewResult)result).Model;
            Assert.AreEqual(0, model.Applications.TotalResults);
            Assert.AreEqual(5, model.Applications.MaximumPagesToDisplay);
            Assert.AreEqual(0, model.Applications.PageCount);
        }

        /// <summary>
        /// Tests that the Issue Credits method throws an error if an obligation period cannot be found
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentException), "No obligation period could be found with ID 13", true)]
        public async Task IssueCredits_Throws_Exception_When_ObligationPeriod_Not_Found()
        {
            //arrange
            this.SetupAppSettings();
            this.SetupApplicationsForIssue();
            this.SetupCalculcationService();
            this.SetupOrgansationService();
            this.SetupFuelTypes();

            this.MockReportingPeriodsService.Setup(rp => rp.GetObligationPeriod(It.IsAny<int>())).Returns(Task.FromResult<ObligationPeriod>(null));

            //act
            await this.Controller.Issue(null, 1);

            //assert
        }

        /// <summary>
        /// Tests that the Issue Credits method returns the correct result when a number of applications are returned by the service
        /// </summary>
        [TestMethod]
        public async Task IssueCredits_Returns_Result_With_Applications()
        {
            //arrange
            this.SetupAppSettings();
            this.SetupApplicationsForIssue();
            this.SetupCalculcationService();
            this.SetupOrgansationService();
            this.SetupFuelTypes();
            this.SetupObligationPeriods();

            //act
            var result = await this.Controller.Issue(null, 1);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("Issue", ((ViewResult)result).ViewName);

            var model = (PagedIssueCreditsViewModel)((ViewResult)result).Model;
            Assert.AreEqual(1, model.Applications.TotalResults);
            Assert.AreEqual(5, model.Applications.MaximumPagesToDisplay);
            Assert.AreEqual(1, model.Applications.PageCount);
            Assert.AreEqual(1, model.Applications.Result.Count);
            Assert.AreEqual(ApplicationId1, model.Applications.Result[0].ApplicationId);
        }

        #endregion Issue Credits Tests

        #region Summary Tests

        /// <summary>
        /// Test GET version of Summary returns result
        /// </summary>
        [TestMethod]
        public async Task Summary_Returns_Result()
        {
            //arrange
            this.SetupAppSettings();
            MockBalanceViewService
                .Setup(x => x.GetCreditTransfers(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<CreditTransferSummary>>(new PagedResult<CreditTransferSummary>() { Result = new List<CreditTransferSummary>() })));
            MockReportingPeriodsService
                .Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(1, null, DateTime.Now, false, null, 0, 0, null, null, null, null, null, null)));

            //act
            var result = await this.Controller.Summary(0, "");

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Test POST version of Summary returns result
        /// </summary>
        [TestMethod]
        public void Summary_Post_Returns_Result()
        {
            //act
            var result = Controller.SummaryPost(new CreditTransfersViewModel(It.IsAny<int>(), null, It.IsAny<string>(), null), 1);

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.IsTrue(((RedirectToActionResult)result).RouteValues.ContainsKey("page"));
            Assert.IsTrue(((RedirectToActionResult)result).RouteValues.ContainsKey("search"));
            Assert.AreEqual("Summary", ((RedirectToActionResult)result).ActionName);
        }

        #endregion Summary Tests

        #region Credits CSV tests

        /// <summary>
        /// Tests that the Credits CSV method returns a file
        /// </summary>
        [TestMethod]
        public async Task Credits_SummaryCsv_Returns_File()
        {
            //arrange
            var fakeFileContents = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
            var fakeObligationPeriodId = 1;

            this.MockBalanceViewService.Setup(s => s.GetCreditTransfersCsv(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<byte[]>(fakeFileContents)));
            //act
            var result = await this.Controller.SummaryCsv(fakeObligationPeriodId, "");

            //assert
            Assert.IsInstanceOfType(result, typeof(FileContentResult));
            Assert.AreEqual(fakeFileContents, result.FileContents);
            Assert.AreEqual($"CreditTransfers-ObligationPeriod{fakeObligationPeriodId.ToString()}-{DateTime.Now.ToString("ddMMyyhhmmss")}.csv", result.FileDownloadName);
        }

        #endregion Credits CSV tests

        #region Private Methods

        /// <summary>
        /// Sets up the Application settings 
        /// </summary>
        private void SetupAppSettings()
        {
            var appSettings = new AppSettings()
            {
                Pagination = new PaginationConfigurationSettings()
                {
                    ResultsPerPage = 5,
                    MaximumPagesToDisplay = 5
                }
            };
            this.MockOptions.Setup(s => s.Value).Returns(appSettings);
        }

        /// <summary>
        /// Sets up the review applications service to return Null applications for issue
        /// </summary>
        private void SetupNullApplicationsForIssue()
        {
            var pageResult = new PagedResult<ApplicationSummary>();

            this.MockReviewApplicationsService.Setup(s => s.GetApplicationsReadyForIssuePagedResult(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<ApplicationSummary>>(pageResult)));
        }

        /// <summary>
        /// Sets up the review applications service to return zero applications for issue
        /// </summary>
        private void SetupZeroApplicationsForIssue()
        {
            var pageResult = new PagedResult<ApplicationSummary>();
            pageResult.Result = new List<ApplicationSummary>();

            this.MockReviewApplicationsService.Setup(s => s.GetApplicationsReadyForIssuePagedResult(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<ApplicationSummary>>(pageResult)));
        }

        /// <summary>
        /// Sets up the review applications service to return applications for issue
        /// </summary>
        private void SetupApplicationsForIssue()
        {
            //Review Application Service
            var pageResult = new PagedResult<ApplicationSummary>()
            {
                TotalResults = 1,
            };

            var applications = new List<ApplicationSummary>();
            applications.Add(new ApplicationSummary(ApplicationId1, UserOrganisationId, ObligationPeriodId13));

            pageResult.Result = applications;

            this.MockReviewApplicationsService.Setup(s => s.GetApplicationsReadyForIssuePagedResult(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<ApplicationSummary>>(pageResult)));

            //Application Service
            var applicationItems = new List<ApplicationItemSummary>();
            applicationItems.Add(new ApplicationItemSummary(ApplicationId1, 1, FuelType1, null, 1000, 26, 20));
            var result = new HttpObjectResponse<IList<ApplicationItemSummary>>(applicationItems);

            //Application Items
            this.MockApplicationsService.Setup(s => s.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(result));
        }

        /// <summary>
        /// Sets up the calculation service 
        /// </summary>
        private void SetupCalculcationService()
        {
            var result = new CalculationSummaryResult(UserOrganisationId, ObligationPeriodId13, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());
            this.MockCalculationsService.Setup(s => s.GhgCalculationSummary(It.IsAny<CalculationCommand>())).Returns(Task.FromResult(result));
        }

        /// <summary>
        /// Sets up the organisation service
        /// </summary>
        private void SetupOrgansationService()
        {
            var result = new HttpObjectResponse<Organisation>(new Organisation(UserOrganisationId, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()));
            this.MockOrganisationsService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(result));
        }

        /// <summary>
        /// Sets up the GHG Fuels service
        /// </summary>
        private void SetupFuelTypes()
        {
            var fuelTypes = new List<FuelType>();
            fuelTypes.Add(new FuelType(FuelType1
                , (int)ApplicationDetailsViewModel.FuelCategories.FossilGas
                , It.IsAny<string>()
                , It.IsAny<string>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>()));

            this.MockGhgFuelsService.Setup(x => x.GetFuelTypes()).Returns(Task.FromResult<IList<FuelType>>(fuelTypes));
        }

        /// <summary>
        /// Sets up the Obligation Periods Service
        /// </summary>
        private void SetupObligationPeriods()
        {
            var fakeObligationPeriod = new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>());

            this.MockReportingPeriodsService.Setup(rp => rp.GetObligationPeriod(It.IsAny<int>())).ReturnsAsync(fakeObligationPeriod);
        }

        #endregion Private Methods
    }
}
