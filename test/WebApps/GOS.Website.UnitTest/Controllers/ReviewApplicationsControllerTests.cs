﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Models;
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Services;
using DfT.GOS.Web.Models;
using DfT.GOS.Website.Configuration;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Models.ApplicationsViewModels;
using DfT.GOS.Website.Models.ReviewApplicationsViewModels;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    /// <summary>
    /// Tests for the application approvals controller
    /// </summary>
    [TestClass]
    public class ApplicationApprovalsControllerTests
    {
        #region Private members
        private Mock<IApplicationsService> _mockApplicationsService;
        private Mock<IReviewApplicationsService> _mockReviewApplicationsService;
        private Mock<IGhgFuelsService> _mockGhgFuelsService;
        private Mock<IOrganisationsService> _mockOrganisationsService;
        private Mock<IReportingPeriodsService> _mockReportingPeriodsService;
        private Mock<ICalculationsService> _mockCalculationsService;
        private Mock<ITokenService> _mockTokenService;
        private Mock<IUsersService> _mockUsersService;
        private Mock<ILogger<ReviewApplicationsController>> _mockLogger;
        private Mock<IOptions<AppSettings>> _mockOptions;
        private ReviewApplicationsController _controller;
        const int _userOrganisationId = 1000;
        const int _anotherOrganisaitonID = 1001;
        private Guid _userId = Guid.NewGuid();
        #endregion

        #region Intialize
        [TestInitialize]
        public void Intialize()
        {
            _mockApplicationsService = new Mock<IApplicationsService>();
            _mockReviewApplicationsService = new Mock<IReviewApplicationsService>();
            _mockGhgFuelsService = new Mock<IGhgFuelsService>();
            _mockOrganisationsService = new Mock<IOrganisationsService>();
            _mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            _mockCalculationsService = new Mock<ICalculationsService>();
            _mockTokenService = new Mock<ITokenService>();
            _mockUsersService = new Mock<IUsersService>();
            _mockLogger = new Mock<ILogger<ReviewApplicationsController>>();
            _mockOptions = new Mock<IOptions<AppSettings>>();
            var appSettings = new AppSettings() { Pagination = new PaginationConfigurationSettings { MaximumPagesToDisplay = 10, ResultsPerPage = 5 } };
            var options = Options.Create(appSettings);
            _controller = new ReviewApplicationsController(_mockApplicationsService.Object
                , _mockReviewApplicationsService.Object
                , _mockGhgFuelsService.Object
                , _mockOrganisationsService.Object
                , _mockReportingPeriodsService.Object
                , _mockCalculationsService.Object
                , _mockTokenService.Object
                , _mockUsersService.Object
                , _mockLogger.Object
                , options);
            // - user principal
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", _userId.ToString()));
            claims.Add(new Claim("OrganisationId", _userOrganisationId.ToString()));

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }
        #endregion

        #region Pending Applications Tests

        /// <summary>
        /// Tests the GetPendingApplications
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_GetPendingApplications()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetPendingApplicationPagedResult(It.IsAny<string>()
                , It.IsAny<PendingApplicationParameters>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<ApplicationSummary>>
                (new PagedResult<ApplicationSummary>()
                {
                    Result = new List<ApplicationSummary>()
                    {
                        new ApplicationSummary(It.IsAny<int>(),It.IsAny<int>(),It.IsAny<int>())
                    }
                })));

            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<Organisation>(
                        new Organisation(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));

            _mockApplicationsService.Setup(x => x.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<ApplicationItemSummary>>(new List<ApplicationItemSummary>())));

            _mockCalculationsService.Setup(x => x.GhgCalculationSummary(It.IsAny<CalculationCommand>()))
                .Returns(Task.FromResult(new CalculationSummaryResult(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()
                , It.IsAny<decimal>(), It.IsAny<decimal>())));

            this.SetupMockObligationPeriod();

            //act
            var result = await _controller.Index(new PendingApplicationParameters(), null);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", ((ViewResult)result).ViewName);
            _mockReviewApplicationsService.Verify(x => x.GetPendingApplicationPagedResult(It.IsAny<string>(), It.IsAny<PendingApplicationParameters>()), Times.Once);
        }

        /// <summary>
        /// Tests the GetPendingApplications logs an error when an obligation period cannot be found
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_GetPendingApplications_Logs_Not_Found_ObligationPeriod_Error()
        {
            //arrange
            var fakeObligationPeriodId = 1;

            _mockReviewApplicationsService.Setup(x => x.GetPendingApplicationPagedResult(It.IsAny<string>()
               , It.IsAny<PendingApplicationParameters>()))
               .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<ApplicationSummary>>
               (new PagedResult<ApplicationSummary>()
               {
                   Result = new List<ApplicationSummary>()
                   {
                        new ApplicationSummary(It.IsAny<int>(),It.IsAny<int>(), fakeObligationPeriodId)
                   }
               })));

            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
              .Returns(Task.FromResult<ObligationPeriod>(null));

            //act
            await _controller.Index(new PendingApplicationParameters(), null);

            //assert
            _mockLogger.Verify(l => l.Log(LogLevel.Error, It.IsAny<EventId>(), It.IsAny<It.IsAnyType>(), It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
        }

        ///<summary>
        /// Tests the GetPendingAplications can handle when an organisation isn't found
        ///</summary>
        [TestMethod]
        public async Task ApplicationController_GetPendingApplicationsViewModel_Is_Created_When_Organisations_Cannot_Be_Found()
        {
            //arrange
            var applicationId = 1;
            var supplierOrganisationId = 1;
            var expectedOrgThree = "Org 3";
            var expectedOrgOne = "Supplier Id 1";
            _mockReviewApplicationsService.Setup(x => x.GetPendingApplicationPagedResult(It.IsAny<string>()
                , It.IsAny<PendingApplicationParameters>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<ApplicationSummary>>
                (new PagedResult<ApplicationSummary>
                {
                    Result = new List<ApplicationSummary>
                    {
                        new ApplicationSummary(applicationId++, supplierOrganisationId++, It.IsAny<int>()),
                        new ApplicationSummary(applicationId++, supplierOrganisationId++,It.IsAny<int>()),
                        new ApplicationSummary(applicationId++, supplierOrganisationId++,It.IsAny<int>()),
                        new ApplicationSummary(applicationId++, supplierOrganisationId++,It.IsAny<int>()),
                        new ApplicationSummary(applicationId, supplierOrganisationId,It.IsAny<int>())

                    }
                })));

            _mockOrganisationsService.SetupSequence(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpObjectResponse<Organisation>>(null))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<Organisation>(
                        new Organisation(3, expectedOrgThree, It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))))
            .Returns(Task.FromResult(
                    new HttpObjectResponse<Organisation>(
                        new Organisation(4, "Org 4", It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))))
            .Returns(Task.FromResult(
                    new HttpObjectResponse<Organisation>(
                        new Organisation(5, "Org 5", It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));

            _mockApplicationsService.Setup(x => x.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<ApplicationItemSummary>>(new List<ApplicationItemSummary>())));

            _mockCalculationsService.Setup(x => x.GhgCalculationSummary(It.IsAny<CalculationCommand>()))
                .Returns(Task.FromResult(new CalculationSummaryResult(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()
                , It.IsAny<decimal>(), It.IsAny<decimal>())));

            this.SetupMockObligationPeriod();

            //act
            var result = await _controller.Index(new PendingApplicationParameters(), null);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", ((ViewResult)result).ViewName);
            var view = result as ViewResult;
            Assert.IsInstanceOfType(view.Model, typeof(PagedPendingApplicationViewModel));
            var model = view.Model as PagedPendingApplicationViewModel;
            Assert.IsNotNull(model);
            var paged = model.Result;
            Assert.AreEqual(expectedOrgOne, paged.Result[0].Supplier);
            Assert.AreEqual(expectedOrgThree, paged.Result[2].Supplier);

        }

        #endregion Pending Applications Tests

        #region Application Review tests

        /// <summary>
        /// Throws a object not found result for invalid application id
        /// </summary>
        [TestMethod]
        public void ApplicationController_Approve_Returns_NoFound_Result()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));

            //act
            var result = _controller.Approve(12, "", null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests approve methods returns ridirecttoaction result
        /// </summary>
        [TestMethod]
        public void ApplicationController_Approve_Returns_RedirectToAction_Result()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary { ApplicationStatus = ApplicationStatus.Open })));

            //act
            var result = _controller.Approve(12, "", null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }

        /// <summary>
        /// Tests that the approve method returns a view of the application when it is approved (rather than redirecting)
        /// </summary>
        [TestMethod]
        public void ApplicationController_Approve_Returns_View_For_Approved_Applications()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<Organisation>(
                        new Organisation(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(
                    It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary { 
                        ApplicationStatus = ApplicationStatus.Approved,
                        ApplicationId = 1,
                        SubmittedDate = DateTime.Now,
                        ApplicationVolumesReviewStatus = new ReviewStatus(),
                        CalculationReviewStatus = new ReviewStatus(),
                        SupportingDocumentsReviewStatus = new ReviewStatus()
                    })));

            //act
            var result = _controller.Approve(12, "", null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Throws a invalid model 
        /// </summary>
        [TestMethod]
        public void ApplicationController_Approve_Returns_Invalid_ViewModel_Result()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary
                    {
                        ApplicationVolumesReviewStatus = new ReviewStatus { ReviewStatusId = 1 }, //Volumes not reviewed
                        SupplierOrganisationId = _userOrganisationId
                    }
                    )));

            //act
            var result = _controller.Approve(12, new Website.Models.ApplicationsViewModels.ApplicationApprovalViewModel(12));

            //assert
            Assert.AreEqual(1, _controller.ModelState.ErrorCount);
        }
        /// <summary>
        /// Tests RecommendForApproval returns notfound result when application is not found
        /// </summary>
        [TestMethod]
        public void ApplicationController_RecommendForApproval_Returns_NotFound()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));
            //act
            var result = _controller.RecommendForApproval(12, new Website.Models.ApplicationsViewModels.ApplicationApprovalViewModel(12));

            //assert
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOfType(result.Result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Returns the view with Model error
        /// </summary>
        [TestMethod]
        public void ApplicationController_RecommendForApproval_Returns_ViewModel_With_Errors()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary
                    {
                        ApplicationVolumesReviewStatus = new ReviewStatus { ReviewStatusId = 2 }, //Volumes not reviewed
                        SupplierOrganisationId = _userOrganisationId
                    }
                    )));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
             .Returns(Task.FromResult(
                 new HttpObjectResponse<Organisation>(
                     new Organisation(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(),
                     It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(
                    It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            _controller.ModelState.AddModelError("", "Model not valid");
            //act
            var result = _controller.RecommendForApproval(12, new Website.Models.ApplicationsViewModels.ApplicationApprovalViewModel(12));

            //assert
            Assert.AreEqual(1, _controller.ModelState.ErrorCount);
            Assert.AreEqual("Approve", ((ViewResult)result.Result).ViewName);
        }

        /// <summary>
        /// Returns the view with Model error when review is not complete
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_RecommendForApproval_Returns_ViewModel_With_Errors_Incomplete()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary
                    {
                        ApplicationVolumesReviewStatus = new ReviewStatus { ReviewStatusId = 2 }, //Volumes not reviewed
                        SupplierOrganisationId = _userOrganisationId
                    }
                    )));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
             .Returns(Task.FromResult(
                 new HttpObjectResponse<Organisation>(
                     new Organisation(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(),
                     It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(
                    It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));

            //act
            var result = await _controller.RecommendForApproval(12, new ApplicationApprovalViewModel(12));

            //assert
            Assert.AreEqual(1, _controller.ModelState.ErrorCount);
            Assert.AreEqual("Approve", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Returns view with modelstate errors
        /// </summary>
        [TestMethod]
        public void ApplicationController_Approve_Returns_ModelError()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary
                    {
                        ApplicationVolumesReviewStatus = new ReviewStatus { ReviewStatusId = 2 }, //Volumes not reviewed
                        CalculationReviewStatus = new ReviewStatus { ReviewStatusId = 2 },
                        SupportingDocumentsReviewStatus = new ReviewStatus { ReviewStatusId = 3 },
                        SupplierOrganisationId = _userOrganisationId
                    }
                    )));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<Organisation>(
                        new Organisation(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(
                    It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            _mockUsersService.Setup(x => x.FindUserByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(new GosApplicationUser()));
            _mockReviewApplicationsService.Setup(x => x.ReviewApplication(It.IsAny<ReviewApplicationCommand>(),
                It.IsAny<string>()))
                .Returns(Task.FromResult(false));

            _mockTokenService.Setup(x => x.GetJwtToken()).Returns(Task.FromResult("token"));

            //act
            var result = _controller.RecommendForApproval(12, new Website.Models.ApplicationsViewModels.ApplicationApprovalViewModel(12)).Result;

            //assert
            _mockReviewApplicationsService.Verify(x => x.ReviewApplication(
                It.IsAny<ReviewApplicationCommand>(), "token"), Times.Once);
            Assert.AreEqual(1, _controller.ModelState.ErrorCount);
        }

        /// <summary>
        /// Returns a valid result RedirectToActionResult 
        /// </summary>
        [TestMethod]
        public void ApplicationController_RecommendForApproval_Returns_Review_Summary_Rejected()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary
                    {
                        ApplicationVolumesReviewStatus = new ReviewStatus { ReviewStatusId = 2 }, //Volumes not reviewed
                        CalculationReviewStatus = new ReviewStatus { ReviewStatusId = 2 },
                        SupportingDocumentsReviewStatus = new ReviewStatus { ReviewStatusId = 3 },
                        SupplierOrganisationId = _userOrganisationId
                    }
                    )));

            _mockReviewApplicationsService.Setup(x => x.ReviewApplication(It.IsAny<ReviewApplicationCommand>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            _mockTokenService.Setup(x => x.GetJwtToken()).Returns(Task.FromResult("token"));
            //act
            var result = _controller.RecommendForApproval(12, new Website.Models.ApplicationsViewModels.ApplicationApprovalViewModel(12)).Result;

            //assert
            _mockReviewApplicationsService.Verify(x => x.ReviewApplication(
                It.IsAny<ReviewApplicationCommand>(), "token"), Times.Once);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }

        /// <summary>
        /// Returns a valid view result with Application review summary
        /// </summary>
        [TestMethod]
        public void ApplicationController_Approve_Returns_Review_Summary()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary()
                    {
                        ApplicationId = 1,
                        ApplicationStatus = ApplicationStatus.Submitted
                    })));

            this.SetupMockSupplierOrganisation();
            this.SetupMockObligationPeriod();

            _mockUsersService.Setup(x => x.FindUserByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(new GosApplicationUser()));
            //act
            var result = _controller.Approve(1, "", null).Result;

            //assert
            _mockReviewApplicationsService.Verify(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            _mockOrganisationsService.Verify(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            _mockUsersService.Verify(x => x.FindUserByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
        #endregion Application Review tests

        #region Approve Volumes Tests

        /// <summary>
        /// Returns a Not Found if the application returns a not found
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveVolumes_Get_Returns_NotFound_Result()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));

            //act
            var result = _controller.ApproveVolumes(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Returns a redirect to the Index view if the review is in the incorrect state
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveVolumes_Get_Returns_Index_Redirect_Result_When_Application_Not_Found()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.ApplicationStatusId = (int)ApplicationStatus.Open;

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            //act
            var result = _controller.ApproveVolumes(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Index", ((RedirectToActionResult)result).ActionName);
        }

        /// <summary>
        /// Returns the Approve Volumes view on success
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveVolumes_Get_Returns_Approve_Volumes_Result()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.ApplicationStatusId = (int)ApplicationStatus.Submitted;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.ApplicationVolumesReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveVolumes(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ApproveVolumes", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Returns the Approve Volumes view on success, when the application is approved and awaiting credits issued
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveVolumes_Get_Returns_Approve_Volumes_Result_For_Approved_applications()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.ApplicationStatusId = (int)ApplicationStatus.Approved;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.ApplicationVolumesReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveVolumes(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ApproveVolumes", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Returns a Not Found if the application returns a not found on the Review Applications (POST) method
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveVolumes_Post_Returns_NotFound_Result()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));

            //act
            var result = _controller.ApproveVolumes(1, new ApproveApplicationVolumesViewModel()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Returns a the original (Approve Volumes) view if there are any validation issues
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveVolumes_Post_Handles_Invalid_Model_State()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.SupplierOrganisationId = _userOrganisationId;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.ApplicationVolumesReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));
            _controller.ModelState.AddModelError("1", "Fake Model Error");

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveVolumes(1, new ApproveApplicationVolumesViewModel()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ApproveVolumes", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Calls the correct serive method on success and returns to the main approve application screen
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveVolumes_Post_Call_Review_Application_Volumes_On_Success()
        {
            //arrange
            var applicationId = 20;
            ReviewApplicationVolumesCommand commandSent = null;

            var viewModel = new ApproveApplicationVolumesViewModel();
            viewModel.ApplicationId = applicationId;
            viewModel.Review.ReviewStatus = Review.ReviewStatuses.Approved;
            viewModel.Review.RejectionReason = "This should not be passed to the command because it the volumes are approved";

            var applicationReview = new ApplicationReviewSummary();
            applicationReview.SupplierOrganisationId = _userOrganisationId;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.ApplicationVolumesReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            _mockReviewApplicationsService.Setup(s => s.ReviewApplicationVolumes(It.IsAny<ReviewApplicationVolumesCommand>(), It.IsAny<string>()))
                .Callback<ReviewApplicationVolumesCommand, string>((command, token) => { commandSent = command; })
                .Returns(Task.FromResult(true));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveVolumes(1, viewModel).Result;

            //assert
            // - we're returned to the approve view
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Approve", ((RedirectToActionResult)result).ActionName);
            // - the service method was called
            _mockReviewApplicationsService.Verify(s => s.ReviewApplicationVolumes(It.IsAny<ReviewApplicationVolumesCommand>(), It.IsAny<string>()), Times.Once());
            // - The command was valid
            Assert.AreEqual(applicationId, commandSent.ApplicationId);
            Assert.AreEqual(null, commandSent.RejectionReason);
            Assert.AreEqual((int)Review.ReviewStatuses.Approved, commandSent.VolumesReviewStatusId);
        }

        /// <summary>
        /// The Approve volumes (POST)  doesn't call the API mehod if no values have changed
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveVolumes_Post_Doesnt_Call_API_If_No_Changes()
        {
            //arrange
            var viewModel = new ApproveApplicationVolumesViewModel();
            viewModel.Review.ReviewStatus = Review.ReviewStatuses.Approved;

            var applicationReview = new ApplicationReviewSummary();
            applicationReview.SupplierOrganisationId = _userOrganisationId;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.ApplicationVolumesReviewStatus = new ReviewStatus() { ReviewStatusId = (int)Review.ReviewStatuses.Approved };

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveVolumes(1, viewModel).Result;

            //assert
            // - we're returned to the approve view
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Approve", ((RedirectToActionResult)result).ActionName);
            // - the service method was called
            _mockReviewApplicationsService.Verify(s => s.ReviewApplicationVolumes(It.IsAny<ReviewApplicationVolumesCommand>(), It.IsAny<string>()), Times.Never());
        }

        #endregion Approve Volumes Tests

        #region Approve Supporting Documents Tests

        /// <summary>
        /// Returns a Not Found if the application returns a not found
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveSupportingDocuments_Get_Returns_NotFound_Result()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));

            //act
            var result = _controller.ApproveSupportingDocuments(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Returns a redirect to the Index view if the review is in the incorrect state
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveSupportingDocument_Get_Returns_Index_Redirect_Result_When_Application_Not_Found()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.ApplicationStatusId = (int)ApplicationStatus.Open;

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            //act
            var result = _controller.ApproveSupportingDocuments(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Index", ((RedirectToActionResult)result).ActionName);
        }

        /// <summary>
        /// Returns a ApproveSupportingDocuments view to show the user documents for review
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveSupportingDocument_Get_Returns_Approve_Documents_Result()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.ApplicationStatusId = (int)ApplicationStatus.Submitted;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.SupportingDocumentsReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockSupportingDocuments();

            //act
            var result = _controller.ApproveSupportingDocuments(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ApproveSupportingDocuments", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Returns a ApproveSupportingDocuments view to show the user documents for review, when the application has been approved
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveSupportingDocument_Get_Returns_Approve_Documents_Result_When_approved()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.ApplicationStatusId = (int)ApplicationStatus.Approved;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.SupportingDocumentsReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockSupportingDocuments();

            //act
            var result = _controller.ApproveSupportingDocuments(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ApproveSupportingDocuments", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Returns a Not Found if the application returns a not found on the Review Applications (POST) method
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveSupportingDocument_Post_Returns_NotFound_Result()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));

            //act
            var result = _controller.ApproveSupportingDocuments(1, new ApproveApplicationSupportingDocumentsViewModel()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Returns a the original (Approve Supporting Documents) view if there are any validation issues
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveSupportingDocuments_Post_Handles_Invalid_Model_State()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.SupplierOrganisationId = _userOrganisationId;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.SupportingDocumentsReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));
            _controller.ModelState.AddModelError("1", "Fake Model Error");

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockSupportingDocuments();

            //act
            var result = _controller.ApproveSupportingDocuments(1, new ApproveApplicationSupportingDocumentsViewModel()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ApproveSupportingDocuments", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Calls the correct serive method on success and returns to the main approve application screen
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveSupportingDocuments_Post_Call_Review_Application_Volumes_On_Success()
        {
            //arrange
            var applicationId = 20;
            ReviewApplicationSupportingDocumentsCommand commandSent = null;

            var viewModel = new ApproveApplicationSupportingDocumentsViewModel();
            viewModel.ApplicationId = applicationId;
            viewModel.Review.ReviewStatus = Review.ReviewStatuses.Approved;
            viewModel.Review.RejectionReason = "This should not be passed to the command because it the volumes are approved";

            var applicationReview = new ApplicationReviewSummary();
            applicationReview.SupplierOrganisationId = _userOrganisationId;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.SupportingDocumentsReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            _mockReviewApplicationsService.Setup(s => s.ReviewApplicationSupportingDocuments(It.IsAny<ReviewApplicationSupportingDocumentsCommand>(), It.IsAny<string>()))
                .Callback<ReviewApplicationSupportingDocumentsCommand, string>((command, token) => { commandSent = command; })
                .Returns(Task.FromResult(true));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockSupportingDocuments();

            //act
            var result = _controller.ApproveSupportingDocuments(1, viewModel).Result;

            //assert
            // - we're returned to the approve view
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Approve", ((RedirectToActionResult)result).ActionName);
            // - the service method was called
            _mockReviewApplicationsService.Verify(s => s.ReviewApplicationSupportingDocuments(It.IsAny<ReviewApplicationSupportingDocumentsCommand>(), It.IsAny<string>()), Times.Once());
            // - The command was valid
            Assert.AreEqual(applicationId, commandSent.ApplicationId);
            Assert.AreEqual(null, commandSent.RejectionReason);
            Assert.AreEqual((int)Review.ReviewStatuses.Approved, commandSent.SupportingDocumentsReviewStatusId);
        }

        /// <summary>
        /// The Approve supporting documents (POST)  doesn't call the API mehod if no values have changed
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveSupportingDocuments_Post_Doesnt_Call_API_If_No_Changes()
        {
            //arrange
            var viewModel = new ApproveApplicationSupportingDocumentsViewModel();
            viewModel.Review.ReviewStatus = Review.ReviewStatuses.Approved;

            var applicationReview = new ApplicationReviewSummary();
            applicationReview.SupplierOrganisationId = _userOrganisationId;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.SupportingDocumentsReviewStatus = new ReviewStatus() { ReviewStatusId = (int)Review.ReviewStatuses.Approved };

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockSupportingDocuments();

            //act
            var result = _controller.ApproveSupportingDocuments(1, viewModel).Result;

            //assert
            // - we're returned to the approve view
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Approve", ((RedirectToActionResult)result).ActionName);
            // - the service method was called
            _mockReviewApplicationsService.Verify(s => s.ReviewApplicationSupportingDocuments(It.IsAny<ReviewApplicationSupportingDocumentsCommand>(), It.IsAny<string>()), Times.Never());
        }

        #endregion Approve Volumes Tests

        #region Approve Volumes Tests

        /// <summary>
        /// Returns a Not Found if the application returns a not found
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveCalculation_Get_Returns_NotFound_Result()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));

            //act
            var result = _controller.ApproveCalculation(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Returns a redirect to the Index view if the review is in the incorrect state
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveCalculation_Get_Returns_Index_Redirect_Result_When_Application_Not_Found()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.ApplicationStatusId = (int)ApplicationStatus.Open;

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            //act
            var result = _controller.ApproveCalculation(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Index", ((RedirectToActionResult)result).ActionName);
        }

        /// <summary>
        /// Returns a Approve Calculation view to show to the approver
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveCalculation_Get_Returns_Approve_Volumes_Result()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.ApplicationStatusId = (int)ApplicationStatus.Submitted;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.CalculationReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveCalculation(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ApproveCalculation", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Returns a Approve Calculation view to show to the approver, when the application has been approved
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveCalculation_Get_Returns_Approve_Volumes_Result_When_Approved()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.ApplicationStatusId = (int)ApplicationStatus.Approved;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.CalculationReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveCalculation(1, (int?)null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ApproveCalculation", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Returns a Not Found if the application returns a not found on the Review Application calculation (POST) method
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveCalculation_Post_Returns_NotFound_Result()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));

            //act
            var result = _controller.ApproveCalculation(1, new ApproveApplicationCalculationViewModel()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Returns a the original (Approve calculation) view if there are any validation issues
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveCalculation_Post_Handles_Invalid_Model_State()
        {
            //arrange
            var applicationReview = new ApplicationReviewSummary();
            applicationReview.SupplierOrganisationId = _userOrganisationId;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.CalculationReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));
            _controller.ModelState.AddModelError("1", "Fake Model Error");

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveCalculation(1, new ApproveApplicationCalculationViewModel()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ApproveCalculation", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Calls the correct serive method on success and returns to the main approve application screen
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveCalculation_Post_Call_Review_Application_Volumes_On_Success()
        {
            //arrange
            var applicationId = 20;
            ReviewApplicationCalculationCommand commandSent = null;

            var viewModel = new ApproveApplicationCalculationViewModel();
            viewModel.ApplicationId = applicationId;
            viewModel.Review.ReviewStatus = Review.ReviewStatuses.Approved;
            viewModel.Review.RejectionReason = "This should not be passed to the command because the calculation is approved";

            var applicationReview = new ApplicationReviewSummary();
            applicationReview.SupplierOrganisationId = _userOrganisationId;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.CalculationReviewStatus = new ReviewStatus();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            _mockReviewApplicationsService.Setup(s => s.ReviewApplicationCalculation(It.IsAny<ReviewApplicationCalculationCommand>(), It.IsAny<string>()))
                .Callback<ReviewApplicationCalculationCommand, string>((command, token) => { commandSent = command; })
                .Returns(Task.FromResult(true));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveCalculation(1, viewModel).Result;

            //assert
            // - we're returned to the approve view
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Approve", ((RedirectToActionResult)result).ActionName);
            // - the service method was called
            _mockReviewApplicationsService.Verify(s => s.ReviewApplicationCalculation(It.IsAny<ReviewApplicationCalculationCommand>(), It.IsAny<string>()), Times.Once());
            // - The command was valid
            Assert.AreEqual(applicationId, commandSent.ApplicationId);
            Assert.AreEqual(null, commandSent.RejectionReason);
            Assert.AreEqual((int)Review.ReviewStatuses.Approved, commandSent.CalculationReviewStatusId);
        }

        /// <summary>
        /// The Approve calculation (POST)  doesn't call the API mehod if no values have changed
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_ApproveCalculations_Post_Doesnt_Call_API_If_No_Changes()
        {
            //arrange
            var viewModel = new ApproveApplicationCalculationViewModel();
            viewModel.Review.ReviewStatus = Review.ReviewStatuses.Approved;

            var applicationReview = new ApplicationReviewSummary();
            applicationReview.SupplierOrganisationId = _userOrganisationId;
            applicationReview.SubmittedDate = DateTime.Now;
            applicationReview.CalculationReviewStatus = new ReviewStatus() { ReviewStatusId = (int)Review.ReviewStatuses.Approved };

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(applicationReview)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();
            this.SetupMockApplicationItems();

            //act
            var result = _controller.ApproveCalculation(1, viewModel).Result;

            //assert
            // - we're returned to the approve view
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Approve", ((RedirectToActionResult)result).ActionName);
            // - the service method was called
            _mockReviewApplicationsService.Verify(s => s.ReviewApplicationCalculation(It.IsAny<ReviewApplicationCalculationCommand>(), It.IsAny<string>()), Times.Never());
        }

        #endregion Approve Volumes Tests

        #region Approve Application Tests

        /// <summary>
        /// Returns a Not Found if the application returns a not found
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_Approve_Handles_NotFound()
        {
            //arrange
            var applicationId = 1;
            var viewModel = new ApplicationApprovalViewModel();

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(
                   new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));

            //act
            var result = _controller.Approve(applicationId, viewModel).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Returns a Not Found if the application relates to an organisation the user doesn't have access to
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_Approve_Handles_Forbid()
        {
            //arrange
            var applicationId = 1;
            var viewModel = new ApplicationApprovalViewModel();
            var application = new ApplicationReviewSummary() { SupplierOrganisationId = _anotherOrganisaitonID };

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(
                   new HttpObjectResponse<ApplicationReviewSummary>(application)));

            //act
            var result = _controller.Approve(applicationId, viewModel).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Approve method handles an invalid approved state
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_Approve_Handles_Invalid_Approved_State()
        {
            //arrange
            var applicationId = 1;
            var viewModel = new ApplicationApprovalViewModel()
            {
                ApproveApplication = null
            };

            var application = new ApplicationReviewSummary()
            {
                SupplierOrganisationId = _userOrganisationId
            };

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(
                   new HttpObjectResponse<ApplicationReviewSummary>(application)));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();

            //act
            var result = _controller.Approve(applicationId, viewModel).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("Approve", ((ViewResult)result).ViewName);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(ApplicationApprovalViewModel));

            // - The service methods shouldn't have been invoked
            _mockReviewApplicationsService.Verify(s => s.ApproveApplication(It.IsAny<ApproveApplicationCommand>(), It.IsAny<string>()), Times.Never);
            _mockReviewApplicationsService.Verify(s => s.RejectApplication(It.IsAny<RejectApplicationCommand>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Tests the Approve method handles approval of an application correctly
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_Approve_ApproveApplication_Success()
        {
            //arrange
            var applicationId = 1;
            var viewModel = new ApplicationApprovalViewModel()
            {
                ApproveApplication = true
            };

            var application = new ApplicationReviewSummary() { SupplierOrganisationId = _userOrganisationId };

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationReviewSummary>(application)));

            _mockReviewApplicationsService.Setup(s => s.ApproveApplication(It.IsAny<ApproveApplicationCommand>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            //act
            var result = _controller.Approve(applicationId, viewModel).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            // - Redirect to the pending applications page
            Assert.AreEqual("Index", ((RedirectToActionResult)result).ActionName);

            // - The approve service method should have been invoked
            _mockReviewApplicationsService.Verify(s => s.ApproveApplication(It.IsAny<ApproveApplicationCommand>(), It.IsAny<string>()), Times.Once);
            _mockReviewApplicationsService.Verify(s => s.RejectApplication(It.IsAny<RejectApplicationCommand>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Tests the Approve method handles approval of an application correctly
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_Approve_RejectApplication_Success()
        {
            //arrange
            var applicationId = 1;
            var viewModel = new ApplicationApprovalViewModel()
            {
                ApproveApplication = false
            };

            var application = new ApplicationReviewSummary() { SupplierOrganisationId = _userOrganisationId };

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationReviewSummary>(application)));

            _mockReviewApplicationsService.Setup(s => s.RejectApplication(It.IsAny<RejectApplicationCommand>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            //act
            var result = _controller.Approve(applicationId, viewModel).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            // - Redirect to the pending applications page
            Assert.AreEqual("Index", ((RedirectToActionResult)result).ActionName);

            // - The reject service method should have been invoked
            _mockReviewApplicationsService.Verify(s => s.ApproveApplication(It.IsAny<ApproveApplicationCommand>(), It.IsAny<string>()), Times.Never);
            _mockReviewApplicationsService.Verify(s => s.RejectApplication(It.IsAny<RejectApplicationCommand>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Tests the Approve method handles a service layer issue
        /// </summary>
        [TestMethod]
        public void ReviewApplicationsController_Approve_Handles_API_Error()
        {
            //arrange
            var applicationId = 1;
            var viewModel = new ApplicationApprovalViewModel()
            {
                ApproveApplication = false
            };

            var application = new ApplicationReviewSummary() { SupplierOrganisationId = _userOrganisationId };

            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationReviewSummary>(application)));

            // - Simulate error in API layer
            _mockReviewApplicationsService.Setup(s => s.RejectApplication(It.IsAny<RejectApplicationCommand>(), It.IsAny<string>()))
                .Returns(Task.FromResult(false));

            this.SetupMockObligationPeriod();
            this.SetupMockSupplierOrganisation();

            //act
            var result = _controller.Approve(applicationId, viewModel).Result;

            //assert - show the page again because of API error
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("Approve", ((ViewResult)result).ViewName);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(ApplicationApprovalViewModel));
        }

        #endregion Approve Application Tests

        #region Reject Application
        /// <summary>
        /// Tests for invalid application id
        /// </summary>
        [TestMethod]
        public void ApplicationController_ReviewResult_Returns_NotfoundResult()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(HttpStatusCode.NotFound)));

            //act
            var result = _controller.ReviewResult(12).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests for incorrect application status
        /// </summary>
        [TestMethod]
        public void ApplicationController_ReviewResult_Returns_RedirectResult_With_Status_Otherthan_Rejected()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary
                    {
                        ApplicationId = 12,
                        ApplicationStatus = ApplicationStatus.RecommendApproval
                    })));

            //act
            var result = _controller.ReviewResult(12).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }

        /// <summary>
        /// Tests for valid rejection review result
        /// </summary>
        [TestMethod]
        public void ApplicationController_ReviewResult_Returns_ViewModel()
        {
            //arrange
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary
                    {
                        ApplicationId = 12,
                        ApplicationStatus = ApplicationStatus.Rejected
                    })));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<Organisation>(
                        new Organisation(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(
                    It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));

            //act
            var result = _controller.ReviewResult(12).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Sets up a mock obligation period on the reporting periods service
        /// </summary>
        private void SetupMockObligationPeriod()
        {
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
               .Returns(Task.FromResult(new ObligationPeriod(
                   It.IsAny<int>()
               , DateTime.Now.AddDays(-1)
               , DateTime.Now.AddDays(1)
               , It.IsAny<bool>()
               , It.IsAny<DateTime>()
               , It.IsAny<int>()
               , It.IsAny<int>()
               , It.IsAny<DateTime>()
               , It.IsAny<DateTime>()
               , It.IsAny<DateTime>()
               , It.IsAny<decimal>()
               , It.IsAny<int>()
               , It.IsAny<decimal>())));
        }

        /// <summary>
        /// Sets up a mock supplier organisation on the organisations service
        /// </summary>
        private void SetupMockSupplierOrganisation()
        {
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
               , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
        }

        /// <summary>
        /// Sets up a mock list of application items on the applications service
        /// </summary>
        private void SetupMockApplicationItems()
        {
            var mockApplicationItems = new List<ApplicationItemSummary>();

            _mockApplicationsService.Setup(x => x.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<ApplicationItemSummary>>(mockApplicationItems)));
        }

        /// <summary>
        /// Sets up a mock list of supporting documents on the applications service
        /// </summary>
        private void SetupMockSupportingDocuments()
        {
            var mockSupportingDocuments = new List<UploadedSupportingDocument>();

            _mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(mockSupportingDocuments)));
        }

        #endregion Private Methods       
    }
}
