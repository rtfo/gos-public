﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Configuration;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Net;
using DfT.GOS.Security.Claims;
using DfT.GOS.Website.Models.ObligationPeriodViewModels;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the supplier controller
    /// </summary>
    [TestClass]
    public class ObligationPeriodControllerTest
    {
        #region Constants

        private const int _userOrganisationId = 1000;

        #endregion Constants
        #region Properties

        private Mock<IOrganisationsService> _mockOrganisationsService { get; set; }
        private Mock<IReportingPeriodsService> _mockReportingPeriodsService { get; set; }
        private Mock<ITokenService> _mockTokenService { get; set; }
        private ObligationPeriodController _controller;
        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialisation before each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            _mockTokenService = new Mock<ITokenService>();
            _mockOrganisationsService = new Mock<IOrganisationsService>();

           

            _mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            _mockReportingPeriodsService.Setup(s => s.GetCurrentObligationPeriod()).Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));

            var appSettings = new AppSettings()
            {
                Pagination = new PaginationConfigurationSettings
                {
                    MaximumPagesToDisplay = 10,
                    ResultsPerPage = 5
                }
            };
            var options = Options.Create(appSettings);
            _mockTokenService.Setup(x => x.GetJwtToken())
                .Returns(Task.FromResult(It.IsAny<string>()));

            _controller = new ObligationPeriodController(_mockOrganisationsService.Object
                , _mockReportingPeriodsService.Object
                , _mockTokenService.Object);
            // - user principal
            var claims = new List<Claim>
            {
                new Claim("UserId", Guid.NewGuid().ToString()),
                new Claim("OrganisationId", _userOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialisation

        #region SelectObligation tests
        /// <summary>
        /// Tests that select obligation returns forbid result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SelectObligation_Returns_ForbidResult()
        {
            //act
            var result = await this._controller.SelectObligation(1, 12121);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }
        /// <summary>
        /// Tests that selectobligation action returns notfound result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SelectObligation_Returns_NotFound()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            //act
            var result = await this._controller.SelectObligation(1, _userOrganisationId);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }
        /// <summary>
        /// Tests that selectobligation returns view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SelectObligation_Returns_ViewResult()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(
                1000, "Fake Organisation",
                1, "Fake Status",
                2, "type"))));
            //act
            var result = await this._controller.SelectObligation(1, _userOrganisationId);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(SelectObligationViewModel));
        }
        #endregion SelectObligation tests

        #region SelectObligationAdmin tests
        /// <summary>
        /// Tests that selectobligationadmin returns view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SelectObligationAdmin_Returns_ViewResult()
        {
            //act
            var result = await this._controller.SelectObligationAdmin(1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(SelectObligationViewModel));
        }
        #endregion SelectObligationAdmin tests

        #region SelectObligationAdminHome tests
        /// <summary>
        /// Tests that selectobligationadminhome returns view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SelectObligationAdminHome_Returns_ViewResult()
        {
            //act
            var result = await this._controller.SelectObligationAdminHome(1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(SelectObligationViewModel));
        }
        #endregion SelectObligationAdminHome tests

        #region SelectObligation Post Tests
        /// <summary>
        /// Tests that SelectObligation post returns redirect result to Index view for admin users
        /// </summary>
        [TestMethod]
        public async Task SelectObligation_Post_Returns_RedirectResult_Admin()
        {
            //arrange
            var claims = new List<Claim>
            {
                new Claim("UserId", Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, Roles.Administrator)
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
            //act
            var result = await this._controller.SelectObligation(new SelectObligationViewModel());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "Index");
        }
        /// <summary>
        /// Tests that SelectObligation post returns redirect result to Index view for admin users
        /// </summary>
        [TestMethod]
        public async Task SelectObligation_Post_Returns_RedirectResult_Admin_AsSupplier()
        {
            //arrange
            var claims = new List<Claim>
            {
                new Claim("UserId", Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, Roles.Administrator)
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(
                1000, "Fake Organisation",
                1, "Fake Status",
                1, "type"))));
            //act
            var result = await this._controller.SelectObligation(new SelectObligationViewModel() { OrganisationId = 1});
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "SupplierHome");
        }
        /// <summary>
        /// Tests that SelectObligation post returns redirect result to Index view for admin users
        /// </summary>
        [TestMethod]
        public async Task SelectObligation_Post_Returns_RedirectResult_Admin_AsTrader()
        {
            //arrange
            var claims = new List<Claim>
            {
                new Claim("UserId", Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, Roles.Administrator)
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(
                1000, "Fake Organisation",
                1, "Fake Status",
                2, "type"))));
            //act
            var result = await this._controller.SelectObligation(new SelectObligationViewModel() { OrganisationId = 1 });
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "TraderHome");
        }
        /// <summary>
        /// Tests that SelectObligation post returns redirect result to Home view for traders and supplier
        /// </summary>
        [TestMethod]
        public async Task SelectObligation_Post_Returns_RedirectResult()
        {
            //act
            var result = await this._controller.SelectObligation(new SelectObligationViewModel());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "Home");
        }
        #endregion SelectObligation Post Tests
    }
}
