﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgMarketplace.API.Client.Services;
using DfT.GOS.GhgMarketplace.Common.Commands;
using DfT.GOS.GhgMarketplace.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Controllers;
using DfT.GOS.Website.Models.MarketplaceViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    /// <summary>
    /// Tests for the Marketplace Controller
    /// </summary>
    [TestClass]
    public class MarketplaceContollerTest
    {
        #region Constants

        private const int UserOrganisationId = 1;
        private const int ObligationPeriodId = 1;
        private const int OrganisationIdCurrent = 1;
        private const int OrganisationIdAdvertisingToBuy = 2;
        private const int OrganisationIdAdvertisingToSell_A = 3;
        private const int OrganisationIdAdvertisingToSell_B = 4;

        #endregion

        #region Private members

        private Mock<IMarketplaceService> MockMarketplaceService;
        private Mock<ILogger<MarketplaceController>> MockLogger;
        private Mock<IReportingPeriodsService> MockReportingPeriodService;
        private Mock<ITokenService> MockTokenService;
        private Mock<IOrganisationsService> MockOrganisationService;
        private Mock<IBalanceViewService> MockBalanceViewService;

        private MarketplaceController Controller;

        #endregion
        
        #region Test Initialisation

        /// <summary>
        /// Initialisation prior to every test
        /// </summary>
        [TestInitialize]
        public void Initialise()
        {
            this.MockMarketplaceService = new Mock<IMarketplaceService>();
            this.MockLogger = new Mock<ILogger<MarketplaceController>>();
            this.MockReportingPeriodService = new Mock<IReportingPeriodsService>();
            this.MockTokenService = new Mock<ITokenService>();
            this.MockOrganisationService = new Mock<IOrganisationsService>();
            this.MockBalanceViewService = new Mock<IBalanceViewService>();
            this.Controller = new MarketplaceController(
                this.MockMarketplaceService.Object
                , this.MockLogger.Object
                , this.MockReportingPeriodService.Object
                , this.MockTokenService.Object
                , this.MockBalanceViewService.Object
                , this.MockOrganisationService.Object
                );

            // - user principal
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", Guid.NewGuid().ToString()));
            claims.Add(new Claim("OrganisationId", UserOrganisationId.ToString()));

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }

            };

            // Setup mocks
            this.MockReportingPeriodService.Setup(s => s.GetObligationPeriod(It.IsAny<int>()))
                .ReturnsAsync(new ObligationPeriod(ObligationPeriodId, It.IsAny<DateTime>(), new DateTime(),
                true, It.IsAny<DateTime>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<decimal>()));
            this.MockOrganisationService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(
                    new Organisation(UserOrganisationId, "Name", It.IsAny<int>(), It.IsAny<string>(), 1, "Supplier"))
                    );
            this.MockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<GhgBalanceView.Common.Models.BalanceSummary>(
                    new GhgBalanceView.Common.Models.BalanceSummary(
                        It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), 1000, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()))
                    );

        }

        #endregion

        #region AdviseMarket Tests

        ///<summary>
        /// Checks that only users who match the organisation can view the advise market view
        /// for that organisation
        ///</summary>
        [TestMethod]
        public async Task AdviseMarket_Only_Allows_Access_To_Organisations_Users()
        {
            // Arrange 
            var organisationId = 2;

            // Act
            var result = await this.Controller.AdviseMarket(organisationId, ObligationPeriodId);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }


        ///<summary>
        /// Checks that 400 series responses are handled
        ///</summary>
        [TestMethod]
        public async Task AdviseMarket_Handles_Bad_Request_Responses()
        {
            // Arrange 
            var obligationPeriod = 1;
            this.MockMarketplaceService.Setup(s => s.GetGhGCreditAdvert(UserOrganisationId, obligationPeriod, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<MarketplaceAdvert>(HttpStatusCode.Forbidden));

            // Act
            var result = await this.Controller.AdviseMarket(UserOrganisationId, obligationPeriod);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        ///<summary>
        /// ViewModel has all the required fields
        ///</summary>
        [TestMethod]
        public async Task AdviseMarket_Creates_A_View_Model_With_All_The_Exected_Fields()
        {
            // Arrange
            var expectedObligationEndDate = new DateTime();
            var expectedOrgName = "Org1";
            var expectedBuy = true;
            var expectedSell = true;
            var expectedCredits = 1000;

            //Set mocks to be test specific
            this.MockMarketplaceService.Setup(s => s.GetGhGCreditAdvert(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<MarketplaceAdvert>(new MarketplaceAdvert(1, expectedBuy, expectedSell, null, null, null)));
            this.MockReportingPeriodService.Setup(s => s.GetObligationPeriod(ObligationPeriodId))
                .ReturnsAsync(new ObligationPeriod(It.IsAny<int>(), It.IsAny<DateTime>(), expectedObligationEndDate,
                true, It.IsAny<DateTime>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<decimal>()));
            this.MockOrganisationService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisations.API.Client.Models.Organisation>(
                    new Organisations.API.Client.Models.Organisation(UserOrganisationId, expectedOrgName, It.IsAny<int>(), It.IsAny<string>(), 1, "Supplier"))
                    );
            this.MockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<GhgBalanceView.Common.Models.BalanceSummary>(
                    new GhgBalanceView.Common.Models.BalanceSummary(
                        It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), expectedCredits, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()))
                    );

            // Act
            var result = await this.Controller.AdviseMarket(UserOrganisationId, ObligationPeriodId);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult.Model);
            var model = viewResult.Model as MarketplaceAdvertViewModel;
            Assert.AreEqual(expectedObligationEndDate, model.ObligationPeriodEndDate);
            Assert.AreEqual(expectedOrgName, model.OrganisationName);
            Assert.AreEqual(expectedBuy, model.BuyCredits);
            Assert.AreEqual(expectedSell, model.SellCredits);
            Assert.AreEqual(expectedCredits, model.Credits);
        }

        #endregion AdviseMarket Tests

        #region Advertise Tests

        ///<summary>
        /// Checks that only users who match the organisation can post an advert upadte
        /// for that organisation
        ///</summary>
        [TestMethod]
        public async Task Advertise_Only_Allows_Access_To_Organisations_Users()
        {
            // Arrange 
            var organisationId = 2;

            // Act
            var result = await this.Controller.Advertise(organisationId, ObligationPeriodId, null);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        ///<summary>
        /// A successful update redirects to the marketplace
        ///</summary>
        [TestMethod]
        public async Task Advertise_Redirects_To_The_Marketplace_On_Success()
        {
            // Arrange 
            this.MockMarketplaceService.Setup(s => s.SetGhGCreditAdvert(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<MarketplaceAdvertCommand>(), It.IsAny<string>()))
                .ReturnsAsync(true);            

            // Act
            var result = await this.Controller.Advertise(UserOrganisationId, ObligationPeriodId, new MarketplaceAdvertViewModel());

            // Assert 
            Assert.IsInstanceOfType(result, typeof(RedirectResult));
            var redirect = result as RedirectResult;
            Assert.IsTrue(redirect.Url.ToString().Contains("/Marketplace"));
        }

        ///<summary>
        /// An unsuccessful updates adds an error to the model state
        ///</summary>
        [TestMethod]
        public async Task Advertise_Adds_An_Error_When_It_Fails()
        {
            // Arrange
            this.MockMarketplaceService.Setup(s => s.SetGhGCreditAdvert(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<MarketplaceAdvertCommand>(), It.IsAny<string>()))
                .ReturnsAsync(false);

            // Act
            var result = await this.Controller.Advertise(UserOrganisationId, ObligationPeriodId, new MarketplaceAdvertViewModel());

            // Assert 
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.AreEqual(1, viewResult.ViewData.ModelState.ErrorCount);
        }

        #endregion Advertise Tests

        #region Marketplace Tests

        /// <summary>
        /// Tests that the Marketplace method handles an invalid obligation period
        /// </summary>
        [TestMethod]
        public async Task Marketplace_Handles_Invalid_ObligationPeriodId()
        {
            //arrange
            var invalidObligationPeriodId = 900;
            this.MockReportingPeriodService.Setup(s => s.GetObligationPeriod(invalidObligationPeriodId)).ReturnsAsync((ObligationPeriod)null);

            //act
            var result = await this.Controller.Marketplace(invalidObligationPeriodId);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the Marketplace method defaults to the current obligation period
        /// </summary>
        [TestMethod]
        public async Task Marketplace_Defaults_To_Current_ObligationPeriod()
        {
            //arrange
            var currentObligationPeriodId = 101;

            this.MockReportingPeriodService.Setup(s => s.GetCurrentObligationPeriod()).ReturnsAsync(new ObligationPeriod(currentObligationPeriodId, It.IsAny<DateTime>(), new DateTime(),
                true, It.IsAny<DateTime>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<decimal>()));

            this.SetupGetMarketplaceAdverts();

            //act
            var result = await this.Controller.Marketplace();

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(MarketplaceAdvertsViewModel));
            var model = (MarketplaceAdvertsViewModel)((ViewResult)result).Model;
            //assert - the correct obligation period was set
            Assert.AreEqual(currentObligationPeriodId, model.ObligationPeriod.Id);
            //assert - the model was broadly populated correctly
            Assert.IsNotNull(model.OrganisationsLookingToBuy);
            Assert.IsNotNull(model.OrganisationsLookingToSell);
            Assert.IsTrue(model.MarketplaceAvailable);
        }

        /// <summary>
        /// Tests that the Marketplace method handles connection issues to the marketplace service
        /// </summary>
        [TestMethod]
        public async Task Marketplace_Handles_MarketplaceService_Connection_Issue()
        {
            //arrange
            this.MockMarketplaceService.Setup(s => s.GetMarketplaceAdverts(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<MarketplaceAdverts>(HttpStatusCode.BadGateway));

            //act
            var result = await this.Controller.Marketplace(ObligationPeriodId);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(MarketplaceAdvertsViewModel));
            var model = (MarketplaceAdvertsViewModel)((ViewResult)result).Model;
            Assert.IsFalse(model.MarketplaceAvailable);
        }

        /// <summary>
        /// Tests that the Marketplace method returns the expected result
        /// </summary>
        [TestMethod]
        public async Task Marketplace_Returns_Result()
        {
            //arrange
            this.SetupGetMarketplaceAdverts();
            var buyerOne = "Buyer One";
            var aSeller = "a Seller";
            var zSeller = "z Seller";

            this.MockOrganisationService.Setup(s => s.GetOrganisation(OrganisationIdAdvertisingToBuy, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(OrganisationIdAdvertisingToBuy, buyerOne, 1, "mock status", 1, "mock type")));
            this.MockOrganisationService.Setup(s => s.GetOrganisation(OrganisationIdAdvertisingToSell_A, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(OrganisationIdAdvertisingToSell_A, aSeller, 1, "mock status", 1, "mock type")));
            this.MockOrganisationService.Setup(s => s.GetOrganisation(OrganisationIdAdvertisingToSell_B, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(OrganisationIdAdvertisingToSell_B, zSeller, 1, "mock status", 1, "mock type")));

            //act
            var result = await this.Controller.Marketplace(ObligationPeriodId, OrganisationIdCurrent);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(MarketplaceAdvertsViewModel));
            var model = (MarketplaceAdvertsViewModel)((ViewResult)result).Model;
            //assert - the correct obligation period was set
            Assert.AreEqual(ObligationPeriodId, model.ObligationPeriod.Id);
            //assert - the organisation id and name are populated for breadcrumb
            Assert.IsNotNull(model.OrganisationId);
            Assert.IsNotNull(model.OrganisationName);
            //assert - the model was populated correctly
            Assert.IsNotNull(model.OrganisationsLookingToBuy);
            Assert.AreEqual(1, model.OrganisationsLookingToBuy.Count);
            Assert.AreEqual(buyerOne, model.OrganisationsLookingToBuy[0].OrganisationName);
            Assert.IsNotNull(model.OrganisationsLookingToSell);
            Assert.AreEqual(2, model.OrganisationsLookingToSell.Count);
            Assert.AreEqual("Contact", model.OrganisationsLookingToSell[0].FullName);
            //Assert - the results were ordered correctly
            Assert.AreEqual(aSeller, model.OrganisationsLookingToSell[0].OrganisationName);
            Assert.AreEqual(zSeller, model.OrganisationsLookingToSell[1].OrganisationName);
            Assert.IsTrue(model.MarketplaceAvailable);
        }

        #endregion Marketplace Tests

        #region Private Methods

        /// <summary>
        /// Sets up mock GHG Marketplace Adverts
        /// </summary>
        private void SetupGetMarketplaceAdverts()
        {
            var organisationsAdvertisingToBuy = new List<MarketplaceAdvert>();
            organisationsAdvertisingToBuy.Add(new MarketplaceAdvert(OrganisationIdAdvertisingToBuy, true, false, null, null, null));
            var organisationsAdvertisingToSell = new List<MarketplaceAdvert>();
            organisationsAdvertisingToSell.Add(new MarketplaceAdvert(OrganisationIdAdvertisingToSell_B, false, true, null, null, null));
            organisationsAdvertisingToSell.Add(new MarketplaceAdvert(OrganisationIdAdvertisingToSell_A, false, true, "Contact", null, null));

            var marketplaceAdverts = new MarketplaceAdverts(ObligationPeriodId, organisationsAdvertisingToBuy, organisationsAdvertisingToSell);
            var marketplaceAdvertsResult = new HttpObjectResponse<MarketplaceAdverts>(marketplaceAdverts);

            this.MockMarketplaceService.Setup(s => s.GetMarketplaceAdverts(It.IsAny<int>(), It.IsAny<string>())).ReturnsAsync(marketplaceAdvertsResult);
        }

        #endregion Private Methods
    }
}
