﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Models.AdministratorViewModels;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    /// <summary>
    /// Tests for the administrator controller
    /// </summary>
    [TestClass]
    public class AdministratorControllerTest
    {
        #region Properties

        private AdministratorController Controller { get; set; }

        private Mock<IUsersService> _mockUsersService;
        private Mock<ITokenService> _mockTokenService;
        private Mock<IOrganisationsService> _mockOrganisationService;
        private Mock<ILogger<AdministratorController>> _mockLogger;
        private int _userOrganisationId = 1000;
        private string _userId = Guid.NewGuid().ToString();
        #endregion Properties

        #region Initialize

        /// <summary>
        /// Initialises before each test
        /// </summary>
        [TestInitialize]
        public void Intialize()
        {
            _mockUsersService = new Mock<IUsersService>();
            _mockTokenService = new Mock<ITokenService>();
            _mockOrganisationService = new Mock<IOrganisationsService>();
            _mockLogger = new Mock<ILogger<AdministratorController>>();
            this.Controller = new AdministratorController(_mockUsersService.Object
                , _mockTokenService.Object
                , _mockOrganisationService.Object
                , _mockLogger.Object);
            // - user principal
            var claims = new List<Claim>()
            {
                new Claim("UserId", _userId),
                new Claim("OrganisationId", _userOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialize

        #region Further Action Tests

        /// <summary>
        /// Tests that the further actions method returns the correct view
        /// </summary>
        [TestMethod]
        public void FurtherActions_Returns_Result()
        {
            //arrange
            var expectedView = "FurtherActions";

            //act
            var result = this.Controller.FurtherActions();

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(expectedView, ((ViewResult)result).ViewName);
        }
        #endregion

        #region Manage User tests
        [TestMethod]
        public void ManageUsers_Returns_Result()
        {
            //arrange
            _mockUsersService.Setup(x => x.GetAdminUsers(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<GosApplicationUser>>(new List<GosApplicationUser>() { })));
            

            //act
            var result = this.Controller.ManageUsers(null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
        [TestMethod]
        public void ManageUsers_Post_Invalid_Email()
        {
            //arrange
            var model = new ManageAdminUsersViewModel
            {
                Email = "user@123.com"
            };
            _mockUsersService.Setup(x => x.GetAdminUsers(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<GosApplicationUser>>(new List<GosApplicationUser>() { })));
            _mockUsersService.Setup(x => x.FindUserByEmailAsync(It.IsAny<string>()
                , It.IsAny<CancellationToken>()
                , It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(null));
            
            //act
            var result = this.Controller.ManageUsers(model, null).Result;

            //assert
            Assert.IsFalse(this.Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void ManageUsers_Post_Is_Organisation_User()
        {
            //arrange
            var model = new ManageAdminUsersViewModel
            {
                Email = "user@123.com"
            };
            _mockUsersService.Setup(x => x.GetAdminUsers(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<GosApplicationUser>>(new List<GosApplicationUser>() { })));
            _mockUsersService.Setup(x => x.FindUserByEmailAsync(It.IsAny<string>()
                , It.IsAny<CancellationToken>()
                , It.IsAny<string>()))
                .Returns(Task.FromResult(new GosApplicationUser { OrganisationId = 1212 }));
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            //act
            var result = this.Controller.ManageUsers(model, (int?)null).Result;

            //assert
            Assert.IsFalse(this.Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void ManageUsers_Post_Already_An_Admin()
        {
            //arrange
            var model = new ManageAdminUsersViewModel
            {
                Email = "user@123.com"
            };
            _mockUsersService.Setup(x => x.GetAdminUsers(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<GosApplicationUser>>(
                    new List<GosApplicationUser>() { new GosApplicationUser() { Id = "111" } })));
            _mockUsersService.Setup(x => x.FindUserByEmailAsync(It.IsAny<string>()
                , It.IsAny<CancellationToken>()
                , It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser() { Id = "111" }));

            //act
            var result = this.Controller.ManageUsers(model, (int?)null).Result;

            //assert
            Assert.IsFalse(this.Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void ManageUsers_Post_SetAdminRole_ThrowsException()
        {
            //arrange
            var model = new ManageAdminUsersViewModel
            {
                Email = "user@123.com"
            };
            
            _mockUsersService.Setup(x => x.GetAdminUsers(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<GosApplicationUser>>(new List<GosApplicationUser>() { })));
            _mockUsersService.Setup(x => x.FindUserByEmailAsync(It.IsAny<string>()
                , It.IsAny<CancellationToken>()
                , It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser() { Id = "111" }));
            _mockUsersService.Setup(x => x.AddAdminUser(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromException<AddNewAdminUserResult>(new Exception()));

            //act
            var result = this.Controller.ManageUsers(model, null).Result;

            //assert
            Assert.IsFalse(this.Controller.ModelState.IsValid);
        }

        [TestMethod]
        public void ManageUsers_Post_SetAdminRole_Shows_Confirmation_When_User_Invited()
        {
            //arrange
            var model = new ManageAdminUsersViewModel
            {
                Email = "user@123.com"
            };
            _mockUsersService.Setup(x => x.GetAdminUsers(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<GosApplicationUser>>(new List<GosApplicationUser>{ })));
            _mockUsersService.Setup(x => x.AddAdminUser(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new AddNewAdminUserResult { StatusCode = System.Net.HttpStatusCode.OK, UserInvited = true}));

            //act
            var result = this.Controller.ManageUsers(model, null).Result;

            //assert
            Assert.IsTrue(this.Controller.ModelState.IsValid);
            Assert.AreEqual("ManageUsersConfirmation", ((ViewResult)result).ViewName);
        }

        [TestMethod]
        public void ManageUsers_Post_SetAdminRole_Shows_Link_Confirmation_When_User_Exists()
        {
            //arrange
            var model = new ManageAdminUsersViewModel
            {
                Email = "user@123.com"
            };
            _mockUsersService.Setup(x => x.GetAdminUsers(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<GosApplicationUser>>(new List<GosApplicationUser> { })));
            _mockUsersService.Setup(x => x.AddAdminUser(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new AddNewAdminUserResult { StatusCode = System.Net.HttpStatusCode.OK, UserInvited = false }));

            //act
            var result = this.Controller.ManageUsers(model, null).Result;

            //assert
            Assert.IsTrue(this.Controller.ModelState.IsValid);
            Assert.AreEqual("ManageUsersLinkedConfirmation", ((ViewResult)result).ViewName);
        }
        #endregion

        #region Remove Admin user
        [TestMethod]
        public void ManageUsersRemove_Invalid_User()
        {
            //arrange
            _mockUsersService.Setup(x => x.FindUserByEmailAsync(It.IsAny<string>()
               , It.IsAny<CancellationToken>()
               , It.IsAny<string>()))
               .Returns(Task.FromResult<GosApplicationUser>(null));

            //act
            var result = this.Controller.ManageUsersRemove("123", null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }

        [TestMethod]
        public void ManageUsersRemove_No_Admin_User()
        {
            //arrange
            _mockUsersService.Setup(x => x.FindUserByEmailAsync(It.IsAny<string>()
               , It.IsAny<CancellationToken>()
               , It.IsAny<string>()))
               .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser() { Id = "123" }));
            _mockUsersService.Setup(x => x.GetAdminUsers(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<GosApplicationUser>>(new List<GosApplicationUser>() { new GosApplicationUser() { Id = "111" } })));

            //act
            var result = this.Controller.ManageUsersRemove("123", null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }

        [TestMethod]
        public void ManageUsersRemove_Remove()
        {
            //arrange
            _mockUsersService.Setup(x => x.FindUserByIdAsync(It.IsAny<string>()
                , It.IsAny<string>()
               , It.IsAny<CancellationToken>()))
               .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser() { Id = "123" }));
            _mockUsersService.Setup(x => x.GetAdminUsers(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<GosApplicationUser>>(new List<GosApplicationUser>()
                { new GosApplicationUser() { Id = "123" } })));

            //act
            var result = this.Controller.ManageUsersRemove("123", null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ManageUsersRemove", ((ViewResult)result).ViewName);
        }

        [TestMethod]
        public void ManageUsersRemove_Post_OwnAccount()
        {
            //arrange
            var model = new AdminUserViewModel
            {
                Id = _userId
            };
            //act
            var result = this.Controller.ManageUsersRemove(model, _userId, null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        [TestMethod]
        public void ManageUsersRemove_Post_Thows_Exception()
        {
            //arrange
            var model = new AdminUserViewModel
            {
                Id = "123"
            };
            _mockUsersService.Setup(x => x.RemoveAdministratorRole(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromException<HttpResponseMessage>(new Exception()));
            //act
            var result = this.Controller.ManageUsersRemove(model, "123", null).Result;

            //assert
            Assert.IsFalse(this.Controller.ModelState.IsValid);
        }

        [TestMethod]
        public void ManageUsersRemove_Post_Removes_Admin_Role()
        {
            //arrange
            var model = new AdminUserViewModel
            {
                Id = "123"
            };
            _mockUsersService.Setup(x => x.RemoveAdministratorRole(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpResponseMessage>(new HttpResponseMessage(System.Net.HttpStatusCode.OK)));
            //act
            var result = this.Controller.ManageUsersRemove(model, "123", null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }
        #endregion
    }
}
