﻿using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Website.Controllers;
using DfT.GOS.Website.Models.ManageViewModels;
using DfT.GOS.Website.UnitTest.Fakes;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    [TestClass]
    public class ManageControllerTest
    {
        #region Constants
        private const string ValidUserName = "validuser";
        private const string ValidPassword = "validpassword";

        private const string InvalidUserName = "invaliduser";
        private const string InvalidPassword = "invalidpassword";

        private const string LockedUserName = "lockeduser";
        private const string LockedPassword = "lockedpassword";
        private const string ExternalValidUserId = "validuser";
        private const string ExternalInvalidUserName = "invaliduser";
        #endregion

        #region Private Members
        private Mock<IUserStore<GosApplicationUser>> _mockUserStore;
        private Mock<UserManager<GosApplicationUser>> _userManager;
        private FakeSignInManager<GosApplicationUser> _signInManager;
        private Mock<ILogger<ManageController>> _mockLogger;
        private Mock<UrlEncoder> _mockUrlEncoder;
        private ManageController _controller;
        private Mock<IServiceProvider> serviceProviderMock;
        private Mock<IUrlHelperFactory> urlHelperFactory;
        #endregion Private Members

        #region Initialize
        /// <summary>
        /// Initialize the controller and its dependencies
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            _mockUserStore = new Mock<IUserStore<GosApplicationUser>>();
            var userManager = new UserManager<GosApplicationUser>(_mockUserStore.Object, null, null, null, null, null, null, null, null);
            var contextAccessor = new Mock<IHttpContextAccessor>();
            var claimsFactory = new Mock<IUserClaimsPrincipalFactory<GosApplicationUser>>();
            var optionsAccessor = new Mock<IOptions<IdentityOptions>>();
            var simLogger = new Mock<ILogger<SignInManager<GosApplicationUser>>>();
            var schemes = new Mock<IAuthenticationSchemeProvider>();
            var confirmation = new Mock<IUserConfirmation<GosApplicationUser>>();
            var logins = new Dictionary<string, Microsoft.AspNetCore.Identity.SignInResult>()
            {
                { ValidUserName, Microsoft.AspNetCore.Identity.SignInResult.Success  },
                { InvalidUserName, Microsoft.AspNetCore.Identity.SignInResult.Failed },
                { LockedUserName, Microsoft.AspNetCore.Identity.SignInResult.LockedOut }
            };
            var externalLogins = new Dictionary<string, ExternalLoginInfo>()
            {
                { ExternalValidUserId, new ExternalLoginInfo(It.IsAny<ClaimsPrincipal>(),It.IsAny<string>(),It.IsAny<string>(),It.IsAny<string>())  },
                { ExternalInvalidUserName, null }
            };
            _signInManager = new FakeSignInManager<GosApplicationUser>(userManager,
                contextAccessor.Object, claimsFactory.Object, optionsAccessor.Object,
                simLogger.Object, schemes.Object, logins, confirmation.Object, externalLogins);

            _mockLogger = new Mock<ILogger<ManageController>>();

            _mockUrlEncoder = new Mock<UrlEncoder>();
            _userManager = new Mock<UserManager<GosApplicationUser>>(_mockUserStore.Object
                , null, null, null, null, null, null, null, null);
            urlHelperFactory = new Mock<IUrlHelperFactory>();


            _controller = new ManageController(_userManager.Object, _signInManager, _mockLogger.Object, _mockUrlEncoder.Object);

            // - user principal
            var claims = new List<Claim>
            {
                new Claim("UserId",Guid.NewGuid().ToString()),
                new Claim("OrganisationId", "100")
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }
        #endregion

        #region Index tests

        /// <summary>
        /// Tests that the Index action throws Application Exception
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task Index_Throws_ApplicationException()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
                .Returns(Task.FromResult<GosApplicationUser>(null));

            //act
            var result = await _controller.Index();
        }

        /// <summary>
        /// Tests that the Index action returns view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Index_Returns_ViewResult()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser()));

            //act
            var result = await _controller.Index();

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
        #endregion Index tests

        #region Index post tests
        /// <summary>
        /// Tests that the index post action returns view model with errors
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Index_Post_Returns_View_Result_WithModel_Errors()
        {
            //arrange
            var vModel = new IndexViewModel();
            _controller.ModelState.AddModelError("", "User name is required");

            //act
            var result = await _controller.Index(vModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Tests that the index post action throws application exception
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task Index_Post_Throws_ApplicationException()
        {
            //arrange
            var vModel = new IndexViewModel()
            {
                Username = "test@test.com",
                Email = "test@test.com"
            };
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
                .Returns(Task.FromResult<GosApplicationUser>(null));

            //act
            var result = await _controller.Index(vModel);
        }

        /// <summary>
        /// Tests that index post action throws application exception when Email save failed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task Index_Post_Throws_ApplicationException_Email_Save_Failed()
        {
            //arrange
            var vModel = new IndexViewModel()
            {
                Username = "test@test.com",
                Email = "test@test.com"
            };
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
                .Returns(Task.FromResult(new GosApplicationUser() { Email = "user@test.com" }));
            _userManager.Setup(x => x.SetEmailAsync(It.IsAny<GosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Failed(new IdentityError { Description = "test" })));

            //act
            var result = await _controller.Index(vModel);
        }

        /// <summary>
        /// Tests that the index post action throws application exception when PhoneNUmber save failed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task Index_Post_Throws_ApplicationException_PhoneNumber_Save_Failed()
        {
            //arrange
            var vModel = new IndexViewModel()
            {
                Username = "test@test.com",
                Email = "test@test.com",
                PhoneNumber = "331313"
            };
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
                .Returns(Task.FromResult(new GosApplicationUser() { Email = "test@test.com", PhoneNumber = "987654" }));
            _userManager.Setup(x => x.SetPhoneNumberAsync(It.IsAny<GosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Failed(new IdentityError { Description = "test" })));
            _userManager.Setup(x => x.SetEmailAsync(It.IsAny<GosApplicationUser>(), It.IsAny<string>()))
               .Returns(Task.FromResult(IdentityResult.Success));

            //act
            var result = await _controller.Index(vModel);
        }

        /// <summary>
        /// Tests that the index post action returns redirect to action result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Index_Post_Returns_RedirectResult()
        {
            //arrange
            var vModel = new IndexViewModel()
            {
                Username = "test@test.com",
                Email = "test@test.com",
                PhoneNumber = "331313"
            };
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
                .Returns(Task.FromResult(new GosApplicationUser() { Email = "test@test.com", PhoneNumber = "987654" }));
            _userManager.Setup(x => x.SetPhoneNumberAsync(It.IsAny<GosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            _userManager.Setup(x => x.SetEmailAsync(It.IsAny<GosApplicationUser>(), It.IsAny<string>()))
               .Returns(Task.FromResult(IdentityResult.Success));

            //act
            var result = await _controller.Index(vModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "Index");
        }

        #endregion Index post tests

        #region ChangePassword tests
        /// <summary>
        /// Tests that ChangePassword action returns view model with errors
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ChangePassword_Returns_View_Result_WithModel_Errors()
        {
            //arrange
            var vModel = new ChangePasswordViewModel();
            _controller.ModelState.AddModelError("", "User name is required");

            //act
            var result = await _controller.ChangePassword(vModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Tests that ChangePassword action throws application exception when user is null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task ChangePassword_Throws_ApplicationException_When_UserIsNull()
        {
            //arrange
            var vModel = new ChangePasswordViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
               .Returns(Task.FromResult<GosApplicationUser>(null));

            //act
            var result = await _controller.ChangePassword(vModel);
        }

        /// <summary>
        /// Tests that ChangePassword action returns view model with erros when password save failed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ChangePassword_Returns_View_Result_WithModel_Errors_When_Password_Save_Failed()
        {
            //arrange
            var vModel = new ChangePasswordViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
               .Returns(Task.FromResult(new GosApplicationUser() { Email = "test@test.com", PhoneNumber = "987654" }));
            _userManager.Setup(x => x.ChangePasswordAsync(It.IsAny<GosApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Failed(new IdentityError { Description = "test" })));

            //act
            var result = await _controller.ChangePassword(vModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Tests that ChangePassword action returns redirecttoaction
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ChangePassword_Returns_RedirectToAction()
        {
            //arrange
            var vModel = new ChangePasswordViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
               .Returns(Task.FromResult(new GosApplicationUser() { Email = "test@test.com", PhoneNumber = "987654" }));
            _userManager.Setup(x => x.ChangePasswordAsync(It.IsAny<GosApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));

            //act
            var result = await _controller.ChangePassword(vModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "ChangePassword");
        }
        #endregion ChangePassword tests

        #region SetPassword tests
        /// <summary>
        /// Tests that SetPassword action throws application exception
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task SetPassword_Throws_ApplicationException()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
              .Returns(Task.FromResult<GosApplicationUser>(null));
            //act
            var result = await _controller.SetPassword();
        }
        /// <summary>
        /// Tests that SetPassword action returns redirect to action result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SetPassword_Returns_RedirectToActionResult()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
              .Returns(Task.FromResult(new GosApplicationUser()));
            _userManager.Setup(x => x.HasPasswordAsync(It.IsAny<GosApplicationUser>()))
              .Returns(Task.FromResult(false));
            //act
            var result = await _controller.SetPassword();
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
        /// <summary>
        /// Tests that SetPassword action returns view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SetPassword_Returns_ViewResult()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
              .Returns(Task.FromResult(new GosApplicationUser()));
            _userManager.Setup(x => x.HasPasswordAsync(It.IsAny<GosApplicationUser>()))
              .Returns(Task.FromResult(true));
            //act
            var result = await _controller.SetPassword();
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "ChangePassword");
        }
        #endregion SetPassword tests

        #region SetPassword Post tests
        /// <summary>
        /// Tests that SetPassword post action returns view model with errors
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SetPassword_Post_Returns_ViewResult_With_ModelErrors()
        {
            //arrange
            var vModel = new SetPasswordViewModel();
            _controller.ModelState.AddModelError("", "User name is required");

            //act
            var result = await _controller.SetPassword(vModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Tests that SetPassword post action throws application exception
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task SetPassword_Post_Throws_ApplicationException()
        {
            //arrange
            var vModel = new SetPasswordViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
             .Returns(Task.FromResult<GosApplicationUser>(null));

            //act
            var result = await _controller.SetPassword(vModel);
        }

        /// <summary>
        /// Tests that SetPassword post action returns view model with errors
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SetPassword_Post_Returns_ViewResult_With_Errors()
        {
            //arrange
            var vModel = new SetPasswordViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
             .Returns(Task.FromResult(new GosApplicationUser()));
            _userManager.Setup(x => x.AddPasswordAsync(It.IsAny<GosApplicationUser>(), It.IsAny<string>()))
             .Returns(Task.FromResult(IdentityResult.Failed(new IdentityError { Description = "Error" })));

            //act
            var result = await _controller.SetPassword(vModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Tests that SetPassword post action returns redirect to action
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SetPassword_Post_Returns_RedirectToAction()
        {
            //arrange
            var vModel = new SetPasswordViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
             .Returns(Task.FromResult(new GosApplicationUser()));
            _userManager.Setup(x => x.AddPasswordAsync(It.IsAny<GosApplicationUser>(), It.IsAny<string>()))
             .Returns(Task.FromResult(IdentityResult.Success));

            //act
            var result = await _controller.SetPassword(vModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "SetPassword");
        }
        #endregion SetPassword Post tests

        #region ExternalLogins Tests
        /// <summary>
        /// Tests that ExternalLogins action throws applcation exception
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task ExternalLogins_Throws_ApplicationException()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult<GosApplicationUser>(null));

            //act
            var result = await _controller.ExternalLogins();
        }
        /// <summary>
        /// Tests that ExternalLogins action returns view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ExternalLogins_Returns_ViewResult()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult(new GosApplicationUser()));
            _userManager.Setup(x => x.GetLoginsAsync(It.IsAny<GosApplicationUser>()))
            .Returns(Task.FromResult<IList<UserLoginInfo>>(new List<UserLoginInfo>()));

            //act
            var result = await _controller.ExternalLogins();

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(((ViewResult)result).Model.GetType(), typeof(ExternalLoginsViewModel));
        }
        #endregion ExternalLogins Tests

        #region LinkLogin post tests
        /// <summary>
        /// Tests that ExternalLogins post action returns challenfe result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task LinkLogin_Post_Returns_ChallengeResult()
        {
            //arrange
            serviceProviderMock = new Mock<IServiceProvider>();
            var authenticationServiceMock = new Mock<IAuthenticationService>();
            authenticationServiceMock
                .Setup(a => a.SignInAsync(It.IsAny<HttpContext>(), It.IsAny<string>(), It.IsAny<ClaimsPrincipal>()
                , It.IsAny<Microsoft.AspNetCore.Authentication.AuthenticationProperties>()))
                .Returns(Task.CompletedTask);
            serviceProviderMock
                .Setup(s => s.GetService(typeof(IAuthenticationService)))
                .Returns(authenticationServiceMock.Object);
            serviceProviderMock
                .Setup(s => s.GetService(typeof(IUrlHelperFactory)))
                .Returns(urlHelperFactory.Object);
            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    RequestServices = serviceProviderMock.Object,
                },
            };
            _controller.Url = new Mock<IUrlHelper>().Object;
            //act
            var result = await _controller.LinkLogin("");
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ChallengeResult));
        }
        #endregion LinkLogin post tests

        #region LinkLoginCallback Tests
        /// <summary>
        /// Tests that LinkLoginCallback action throws application exception when user is null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task LinkLoginCallback_Throws_ApplicationException_UserIsNull()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult<GosApplicationUser>(null));
            //act
            var result = await _controller.LinkLoginCallback();
        }

        /// <summary>
        ///  Tests that LinkLoginCallback action throws application exception externallogininfo is null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task LinkLoginCallback_Throws_ApplicationException_ExternalLoginInfoIsNull()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult(new GosApplicationUser()));

            //act
            var result = await _controller.LinkLoginCallback();
        }

        /// <summary>
        ///  Tests that LinkLoginCallback action throws application exception when user info fails
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task LinkLoginCallback_Throws_ApplicationException_UserInfoFails()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult(new GosApplicationUser()));

            _userManager.Setup(x => x.AddLoginAsync(It.IsAny<GosApplicationUser>(), It.IsAny<ExternalLoginInfo>()))
           .Returns(Task.FromResult(IdentityResult.Failed(new IdentityError { Description = "Error" })));
            //act
            var result = await _controller.LinkLoginCallback();
        }
        /// <summary>
        ///  Tests that LinkLoginCallback action returns redirect to action result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task LinkLoginCallback_Returns_RedirectToAction()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult(new GosApplicationUser() { Id = ExternalValidUserId }));

            _userManager.Setup(x => x.AddLoginAsync(It.IsAny<GosApplicationUser>(), It.IsAny<ExternalLoginInfo>()))
           .Returns(Task.FromResult(IdentityResult.Success));
            serviceProviderMock = new Mock<IServiceProvider>();
            var claims = new List<Claim>
            {
                new Claim("UserId",Guid.NewGuid().ToString()),
                new Claim("OrganisationId", "100")
            };
            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            var authenticationServiceMock = new Mock<IAuthenticationService>();
            authenticationServiceMock
                .Setup(a => a.SignInAsync(It.IsAny<HttpContext>(), It.IsAny<string>(), It.IsAny<ClaimsPrincipal>()
                , It.IsAny<Microsoft.AspNetCore.Authentication.AuthenticationProperties>()))
                .Returns(Task.CompletedTask);
            serviceProviderMock
                .Setup(s => s.GetService(typeof(IAuthenticationService)))
                .Returns(authenticationServiceMock.Object);
            serviceProviderMock
                .Setup(s => s.GetService(typeof(IUrlHelperFactory)))
                .Returns(urlHelperFactory.Object);
            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    RequestServices = serviceProviderMock.Object
                }
            };
            //act
            var result = await _controller.LinkLoginCallback();
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "ExternalLogins");
        }
        #endregion LinkLoginCallback Tests

        #region RemoveLogin tests
        /// <summary>
        ///  Tests that RemoveLogin action throws application exception
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task RemoveLogin_Throws_ApplicationException_UserIsNull()
        {
            //arrange
            var viewModel = new RemoveLoginViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult<GosApplicationUser>(null));
            //act
            var result = await _controller.RemoveLogin(viewModel);
        }
        /// <summary>
        /// Tests that RemoveLogin action throws application exception when UserManager.RemoveLogin call fails
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task RemoveLogin_Throws_ApplicationException_RemoveLogin_Fails()
        {
            //arrange
            var viewModel = new RemoveLoginViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult(new GosApplicationUser()));
            _userManager.Setup(x => x.RemoveLoginAsync(It.IsAny<GosApplicationUser>()
                , It.IsAny<string>(), It.IsAny<string>()))
            .Returns(Task.FromResult(IdentityResult.Failed(new IdentityError { Description = "Error" })));
            //act
            var result = await _controller.RemoveLogin(viewModel);
        }
        /// <summary>
        /// Tests that RemoveLogin action returns Redirect to action result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RemoveLogin_Returns_RedirectToAction()
        {
            //arrange
            var viewModel = new RemoveLoginViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult(new GosApplicationUser()));
            _userManager.Setup(x => x.RemoveLoginAsync(It.IsAny<GosApplicationUser>()
                , It.IsAny<string>(), It.IsAny<string>()))
            .Returns(Task.FromResult(IdentityResult.Success));
            //act
            var result = await _controller.RemoveLogin(viewModel);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "ExternalLogins");
        }
        #endregion RemoveLogin tests

        #region EnableAuthenticator tests
        /// <summary>
        /// Tests that EnableAuthenticator action throws application exception when UserManager.GetUser returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task EnableAuthenticator_Throws_ApplicationException_UserIsNull()
        {
            //arrange
            var viewModel = new RemoveLoginViewModel();
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult<GosApplicationUser>(null));
            //act
            var result = await _controller.RemoveLogin(viewModel);
        }

        /// <summary>
        /// Tests that EnableAuthenticator action returns view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task EnableAuthenticator_Returns_ViewResult()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult(new GosApplicationUser()));
            _userManager.Setup(x => x.GetAuthenticatorKeyAsync(It.IsAny<GosApplicationUser>()))
           .Returns(Task.FromResult(""));
            //act
            var result = await _controller.EnableAuthenticator();
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(((ViewResult)result).Model.GetType(), typeof(EnableAuthenticatorViewModel));
        }
        #endregion EnableAuthenticator tests

        #region ResetAuthenticatorWarning tests
        /// <summary>
        /// Tests that ResetAuthenticatorWarning action returns view result
        /// </summary>
        [TestMethod]
        public void ResetAuthenticatorWarning_Returns_ViewResult()
        {
            //act
            var result = _controller.ResetAuthenticatorWarning();
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(((ViewResult)result).ViewName, "ResetAuthenticator");
        }
        #endregion ResetAuthenticatorWarning tests

        #region ResetAuthenticator tests
        /// <summary>
        /// Tests that ResetAuthenticatorWarning action throws application exception when the userManager.GetUser returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task ResetAuthenticator_Throws_ApplicationException_UserIsNull()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult<GosApplicationUser>(null));
            //act
            var result = await _controller.ResetAuthenticator();
        }
        /// <summary>
        /// Tests that ResetAuthenticatorWarning action returns view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ResetAuthenticator_Returns_ViewResult()
        {
            //arrange
            _userManager.Setup(x => x.GetUserAsync(It.IsAny<ClaimsPrincipal>()))
            .Returns(Task.FromResult(new GosApplicationUser()));
            _userManager.Setup(x => x.GetAuthenticatorKeyAsync(It.IsAny<GosApplicationUser>()))
           .Returns(Task.FromResult(""));
            //act
            var result = await _controller.ResetAuthenticator();
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual(((RedirectToActionResult)result).ActionName, "EnableAuthenticator");
        }
        #endregion ResetAuthenticator tests
    }
}
