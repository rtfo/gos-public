﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Web.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Configuration;
using DfT.GOS.Website.Models.SuppliersViewModels;
using DfT.GOS.Website.Orchestrators;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Website.Models.OrganisationsViewModels;
using System.Text;
using DfT.GOS.Website.Services;
using DfT.GOS.Identity.API.Client.Models;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the supplier controller
    /// </summary>
    [TestClass]
    public class OrganisationsControllerTest
    {
        #region Constants

        private const int _userOrganisationId = 1000;

        #endregion Constants

        #region Properties

        private Mock<IOrganisationsService> _mockOrganisationsService { get; set; }
        private Mock<IOptions<AppSettings>> _mockAppSettings { get; set; }
        private Mock<IUsersService> _mockIdentityService { get; set; }
        private Mock<IReportingPeriodsService> _mockReportingPeriodsService { get; set; }
        private Mock<ILogger<SuppliersController>> _mockLogger { get; set; }
        private Mock<IMapper> _mockMapper { get; set; }
        private Mock<ITokenService> _mockTokenService { get; set; }
        private Mock<IUsersService> _mockUsersService { get; set; }
        private Mock<IApplicationsService> _mockApplicationsService { get; set; }
        private Mock<IBalanceViewService> _mockBalanceViewService { get; set; }
        private Mock<ISupplierReportingSummaryOrchestrator> _mockOrchestrator { get; set; }
        private IOrganisationsSummaryService _mockOrganisationsSummaryService { get; set; }
        private OrganisationsController _controller { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialisation before each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            _mockMapper = new Mock<IMapper>();
            _mockTokenService = new Mock<ITokenService>();
            _mockIdentityService = new Mock<IUsersService>();
            _mockOrganisationsService = new Mock<IOrganisationsService>();

            _mockBalanceViewService = new Mock<IBalanceViewService>();
            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()))));

            _mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            _mockReportingPeriodsService.Setup(s => s.GetCurrentObligationPeriod()).Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));
            _mockReportingPeriodsService.Setup(s => s.GetObligationPeriod(It.IsAny<int>())).Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));

            var appSettings = new AppSettings()
            {
                Pagination = new PaginationConfigurationSettings
                {
                    MaximumPagesToDisplay = 10,
                    ResultsPerPage = 5
                }
            };
            var options = Options.Create(appSettings);
            _mockTokenService.Setup(x => x.GetJwtToken())
                .Returns(Task.FromResult(It.IsAny<string>()));
            _mockOrchestrator = new Mock<ISupplierReportingSummaryOrchestrator>();
            _mockUsersService = new Mock<IUsersService>();
            _mockOrganisationsSummaryService = new OrganisationsSummaryService(_mockOrganisationsService.Object, _mockTokenService.Object, _mockBalanceViewService.Object, _mockUsersService.Object);

            _controller = new OrganisationsController(options
                , _mockOrganisationsService.Object
                , _mockIdentityService.Object
                , _mockReportingPeriodsService.Object
                , _mockTokenService.Object
                , _mockOrchestrator.Object
                , _mockMapper.Object
                , _mockOrganisationsSummaryService);
            // - user principal
            var claims = new List<Claim>
            {
                new Claim("UserId", Guid.NewGuid().ToString()),
                new Claim("OrganisationId", _userOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialisation

        #region Tests

        #region Index test
        [TestMethod]
        public async Task Index_Returns_Result()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetSuppliersAndTraders(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<Organisation>>(
                    new PagedResult<Organisation>() { Result = new List<Organisation>() })));

            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, DateTime.Now, false, null, 0, 0, null, null, null, null, null, null)));

            var pagedResult = new PagedResult<SupplierBalanceSummary>();
            pagedResult.Result = new List<SupplierBalanceSummary>();

            _mockMapper.Setup(m => m.Map<PagedResult<SupplierBalanceSummary>>(It.IsAny<PagedResult<Organisation>>()))
                .Returns(pagedResult);

            //act
            var result = await _controller.Index();

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Index_Post_Returns_Result()
        {
            //act
            var result = _controller.Index(new ViewOrganisationsViewModel(), 1);

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.IsTrue(((RedirectToActionResult)result).RouteValues.ContainsKey("page"));
            Assert.IsTrue(((RedirectToActionResult)result).RouteValues.ContainsKey("search"));
        }
        #endregion Index test

        #region GenerateExport Tests
        [TestMethod]
        public async Task GenerateExport_Errors_On_Invalid_Obligation_Period_ID()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>())).Returns(Task.FromResult<ObligationPeriod>(null));
            //act
            var result = await _controller.GenerateExport(1);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task GenerateExport_Returns_A_File_With_The_Expected_CSV()
        {
            // arrange
            var expectedName = "Organisations";
            var expectedFileContents = "Organisation,Status,Type,GHG Credits Available,Obligation,Obligated Status,Has Account\r\n"
                + "Supplier Eight,Approved,Supplier,38000000,4000000,In Credit,True\r\n"
                + "Trader Eighteen,Approved,Trader,4000,5000,Obligated,False\r\n";

            _mockOrganisationsService.Setup(s => s.GetSuppliersAndTraders(It.IsAny<string>())).Returns(Task.FromResult(
                new HttpObjectResponse<IList<Organisation>>(
                    new List<Organisation>()
                    {
                        new Organisation(1, "Supplier Eight", 1, "Approved", 1, "Supplier"),
                        new Organisation(2, "Trader Eighteen", 1, "Approved", 2, "Trader")
                    })));

            _mockBalanceViewService.Setup(s => s.GetSummary(1, It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(
                new HttpObjectResponse<BalanceSummary>(new BalanceSummary(
                    0, 0, 0, 38000000, 4000000, 0, 0, 0
            ))));
            _mockBalanceViewService.Setup(s => s.GetSummary(2, It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(
                new HttpObjectResponse<BalanceSummary>(new BalanceSummary(
                    0, 0, 0, 4000, 5000, 0, 0, 0
            ))));

            _mockUsersService.Setup(s => s.GetOrganisationUsers(1, It.IsAny<string>())).Returns(Task.FromResult(new List<GosApplicationUser>()
            {
                new GosApplicationUser()
            }));

            _mockUsersService.Setup(s => s.GetOrganisationUsers(2, It.IsAny<string>())).Returns(Task.FromResult(new List<GosApplicationUser>()));

            // act
            var result = await _controller.GenerateExport(15);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(FileResult));
            FileContentResult file = (FileContentResult)result;
            StringAssert.Contains(file.FileDownloadName, expectedName);

            var contents = Encoding.Unicode.GetString(file.FileContents, 0, file.FileContents.Length);
            Assert.AreEqual(expectedFileContents, contents);
        }

        [TestMethod]
        public async Task GenerateExport_Returns_A_File_Ordered_By_Name()
        {
            var orgs = new List<Organisation>()
                    {
                        new Organisation(5, "A", 3, "Approved", 1, "Supplier"),
                        new Organisation(3, "C", 3, "Approved", 2, "Trader"),
                        new Organisation(1, "B", 3, "Approved", 2, "Trader"),
                        new Organisation(6, "D", 1, "Submitted", 2, "Trader"),
                        new Organisation(2, "E", 3, "Approved", 2, "Trader"),
                        new Organisation(4, "F", 3, "Approved", 1, "Supplier")
                    };

            _mockOrganisationsService.Setup(s => s.GetSuppliersAndTraders(It.IsAny<string>())).Returns(Task.FromResult(
                new HttpObjectResponse<IList<Organisation>>(
                    orgs
            )));

            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(
                new HttpObjectResponse<BalanceSummary>(new BalanceSummary(
                    0, 0, 0, 38000000, 4000000, 0, 0, 0
            ))));

            _mockUsersService.Setup(s => s.GetOrganisationUsers(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new List<GosApplicationUser>()));

            // act
            var result = await _controller.GenerateExport(15);

            //assert
            Assert.IsNotNull(result);

            FileContentResult file = (FileContentResult)result;

            var contents = Encoding.Unicode.GetString(file.FileContents, 0, file.FileContents.Length);

            var expected = "Organisation,Status,Type,GHG Credits Available,Obligation,Obligated Status,Has Account\r\nA,Approved,Supplier,38000000,4000000,In Credit,False\r\nB,Approved,Trader,38000000,4000000,In Credit,False\r\nC,Approved,Trader,38000000,4000000,In Credit,False\r\nD,Submitted,Trader,38000000,4000000,In Credit,False\r\nE,Approved,Trader,38000000,4000000,In Credit,False\r\nF,Approved,Supplier,38000000,4000000,In Credit,False\r\n";

            Assert.AreEqual(contents, expected);
        }
        #endregion GenerateExport Tests

        #endregion Tests
    }
}
