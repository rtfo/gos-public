﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using AutoMapper;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Configuration;
using DfT.GOS.Website.Models.SuppliersViewModels;
using DfT.GOS.Website.Models.TraderViewModels;
using DfT.GOS.Website.Orchestrators;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    [TestClass]
    public class TraderControllerTest
    {
        #region Constants
        private const int _userOrganisationId = 1000;
        private const string _userId = "UserId";
        #endregion Constants

        #region Private properties
        private TraderController controller;
        private Mock<IOrganisationsService> OrganisationsService;
        private Mock<IUsersService> IdentityService;
        private Mock<IReportingPeriodsService> ReportingPeriodsService { get; set; }
        private Mock<ILogger<TraderController>> Logger { get; set; }
        private Mock<ITokenService> TokenService { get; set; }
        private Mock<ISupplierReportingSummaryOrchestrator> Orchestrator { get; set; }
        private Mock<IMapper> Mapper { get; set; }
        #endregion

        #region Initialise
        [TestInitialize]
        public void Initialise()
        {
            var appSettings = new AppSettings()
            {
                Pagination = new PaginationConfigurationSettings
                {
                    MaximumPagesToDisplay = 10,
                    ResultsPerPage = 5
                }
            };
            var options = Options.Create(appSettings);
            OrganisationsService = new Mock<IOrganisationsService>();
            IdentityService = new Mock<IUsersService>();
            ReportingPeriodsService = new Mock<IReportingPeriodsService>();
            TokenService = new Mock<ITokenService>();
            Orchestrator = new Mock<ISupplierReportingSummaryOrchestrator>();
            Logger = new Mock<ILogger<TraderController>>();
            Mapper = new Mock<IMapper>();
            controller = new TraderController(options, OrganisationsService.Object, IdentityService.Object
                , ReportingPeriodsService.Object, Logger.Object, TokenService.Object
                , Orchestrator.Object, Mapper.Object);

            var claims = new List<Claim>
            {
                new Claim("UserId", _userId),
                new Claim("OrganisationId", _userOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }
        #endregion

        #region Tests

        #region TraderHome tests
        /// <summary>
        /// Tests the TraderHome action method return a forbid result when a incorrect organisation is if supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TraderHome_Returns_ForbidResult()
        {
            //Arrange
            var fakeOrganisationId = 22;

            //Act
            var response = await controller.TraderHome(fakeOrganisationId,15);

            //Assert
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the TraderHome action method return a not found result when view model is null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TraderHome_Returns_NotFoundResult()
        {
            //Arrange
            ReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));
            Orchestrator.Setup(x => x.GetTraderReportingSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(Task.FromResult<TraderSummaryViewModel>(null));

            //Act
            var response = await controller.TraderHome(_userOrganisationId, 15);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests the TraderHome action method return a view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TraderHome_Returns_ViewResult()
        {
            //Arrange
            ReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));
            Orchestrator.Setup(x => x.GetTraderReportingSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<ClaimsPrincipal>()))
                .Returns(Task.FromResult<TraderSummaryViewModel>(new TraderSummaryViewModel("user1") {
                    SupplierId = _userOrganisationId
                }));

            //Act
            var response = await controller.TraderHome(_userOrganisationId, 15);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("TraderHome", ((ViewResult)response).ViewName);
        }
        #endregion TraderHome tests


        #region FurtherActions Tests
        /// <summary>
        /// Tests the FurtherActions action method return a forbid result when a incorrect organisation is if supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FurtherActions_Returns_ForbidResult()
        {
            //Arrange
            var fakeOrganisationId = 22;

            //Act
            var response = await controller.FurtherActions(fakeOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the FurtherActions action method return a notfound result when a incorrect organisation is supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FurtherActions_Returns_NotFound()
        {
            //Arrange
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            ReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));
           

            //Act
            var response = await controller.FurtherActions(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests the FurtherActions action method return a notfound result when organisation service results forbidden result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FurtherActions_Returns_Forbidden_When_OrganisationService_Returns_Forbidden()
        {
            //Arrange
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.Forbidden)));
            ReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));


            //Act
            var response = await controller.FurtherActions(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the FurtherActions action method return a view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FurtherActions_Returns_ViewResult()
        {
            //Arrange
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            ReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));


            //Act
            var response = await controller.FurtherActions(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("FurtherActions", ((ViewResult)response).ViewName);
        }
        #endregion FurtherActions Tests

        #region ManageUsers Tests
        /// <summary>
        /// Tests the ManageUsers action method return a forbid result when a incorrect organisation is if supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Returns_Forbid()
        {
            //Arrange
            var fakeOrganisationId = 22;

            //Act
            var response = await controller.ManageUsers(fakeOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ManageUsers action method return a not found result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Returns_NotFoundResult()
        {
            //Arrange
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //Act
            var response = await controller.ManageUsers(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests the ManageUsers action method return a not found result when organisation service results forbidden result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Returns_NotFoundResult_When_OrganisationService_Returns_Forbidden()
        {
            //Arrange
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //Act
            var response = await controller.ManageUsers(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests the ManageUsers action method return a view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Returns_ViewResult()
        {
            //Arrange
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            IdentityService.Setup(x => x.GetOrganisationUsers(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<GosApplicationUser>() { new GosApplicationUser() }));
            ReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));

            //Act
            var response = await controller.ManageUsers(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsers", ((ViewResult)response).ViewName);
        }
        #endregion ManageUsers Tests

        #region ManageUsers Post tests
        /// <summary>
        /// Tests the ManageUsers post action method return a forbidden result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_Forbid()
        {
            //Arrange
            var fakeOrganisationId = 22;
            var model = new ViewSupplierViewModel() { Id = fakeOrganisationId };
            //Act
            var response = await controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }
        /// <summary>
        /// Tests the ManageUsers post action method return a not found result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_NotFoundResult()
        {
            //Arrange
            var model = new ViewSupplierViewModel() { Id = _userOrganisationId };
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                   HttpStatusCode.NotFound)));
            //act
            var response = await controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests the ManageUsers post action method return a view result with model errors
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_Model_With_Error()
        {
            //Arrange
            var model = new ViewSupplierViewModel() { Id = _userOrganisationId };
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            this.IdentityService.Setup(x => x.AddUserAsync(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromException<AddNewGosApplicationUserResult>(new Exception()));
            //act
            var response = await controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsersConfirmationError", ((ViewResult)response).ViewName);
        }
        /// <summary>
        /// Tests the ManageUsers post action method return a view result with model errors while adding user
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_Model_Errors_While_AddingUser()
        {
            //Arrange
            var model = new ViewSupplierViewModel() { Id = _userOrganisationId };
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            this.IdentityService.Setup(x => x.AddUserAsync(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult<AddNewGosApplicationUserResult>(new AddNewGosApplicationUserResult() {Message = "Error" }));
            //act
            var response = await controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsFalse(controller.ModelState.IsValid);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
        }
        /// <summary>
        /// Tests the ManageUsers post action method return a view result ManageUsersLinkedConfirmation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_ViewResult_UserAssociation()
        {
            //Arrange
            var model = new ViewSupplierViewModel() { Id = _userOrganisationId };
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            this.IdentityService.Setup(x => x.AddUserAsync(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult<AddNewGosApplicationUserResult>(new AddNewGosApplicationUserResult()
                { Message = "", UserAssociatedWithOrganisation = true }));
            IdentityService.Setup(x => x.GetOrganisationUsers(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<GosApplicationUser>() { new GosApplicationUser() }));
            //act
            var response = await controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(controller.ModelState.IsValid);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsersLinkedConfirmation", ((ViewResult)response).ViewName);
        }

        /// <summary>
        /// Tests the ManageUsers post action method return a view result UserConfirmation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_ViewResult_UserConfirmation()
        {
            //Arrange
            var model = new ViewSupplierViewModel() { Id = _userOrganisationId };
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            this.IdentityService.Setup(x => x.AddUserAsync(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult<AddNewGosApplicationUserResult>(new AddNewGosApplicationUserResult()
                { Message = "", UserAssociatedWithOrganisation = false }));
            IdentityService.Setup(x => x.GetOrganisationUsers(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<GosApplicationUser>() { new GosApplicationUser() }));
            //act
            var response = await controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(controller.ModelState.IsValid);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsersConfirmation", ((ViewResult)response).ViewName);
        }
        #endregion ManageUsers Post Tests

        #region ManageUsersRemove Tests
        /// <summary>
        /// Tests the ManageUsersRemove action method return a forbid result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Returns_Forbid()
        {
            //Arrange
            var fakeOrganisationId = 22;

            //Act
            var response = await controller.ManageUsersRemove(fakeOrganisationId,"userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ManageUsersRemove action method return a notfound result when user is null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Returns_NotFoundResult_When_UserIsNull()
        {
            //Arrange
            this.IdentityService.Setup(x => x.FindUserByIdAsync(It.IsAny<string>()
                , It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult<GosApplicationUser>(null));
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                   new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            //Act
            var response = await controller.ManageUsersRemove(_userOrganisationId, "userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundObjectResult));
        }
        /// <summary>
        /// Tests the ManageUsersRemove action method return a not found result when organisation is null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Returns_NotFoundResult_When_OrganisationIsNull()
        {
            //Arrange
            this.IdentityService.Setup(x => x.FindUserByIdAsync(It.IsAny<string>()
                , It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser()));
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            //Act
            var response = await controller.ManageUsersRemove(_userOrganisationId, "userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests the ManageUsersRemove action method return a view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Returns_ViewResult()
        {
            //Arrange
            this.IdentityService.Setup(x => x.FindUserByIdAsync(It.IsAny<string>()
                , It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser()));
            OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                  new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            //Act
            var response = await controller.ManageUsersRemove(_userOrganisationId, "userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsersRemove", ((ViewResult)response).ViewName);

        }
        #endregion ManageUsersRemove Tests

        #region ManageUsersRemove Post Tests
        /// <summary>
        /// Tests the ManageUsersRemove post action method return a forbid result when organisation is incorrect
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Post_Returns_Forbid_Incorrect_OrgId()
        {
            //Arrange
            var fakeOrganisationId = 22;
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };

            //Act
            var response = await controller.ManageUsersRemove(model, fakeOrganisationId, "userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ManageUsersRemove post action method return a forbid result when userid is incorrect
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Post_Returns_Forbid_Incorrect_UserId()
        {
            //Arrange
            var userId = _userId;
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };

            //Act
            var response = await controller.ManageUsersRemove(model, _userOrganisationId, userId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ManageUsersRemove post action method return a redirect result when exception is thrown
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Post_Returns_RedirectResult_Exception()
        {
            //arrange
            this.IdentityService.Setup(x => x.RemoveUserAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromException<string>(new Exception()));
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };
            //act
            var response = await this.controller.ManageUsersRemove(model, _userOrganisationId, "122");
            //assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(RedirectResult));

        }

        /// <summary>
        /// Tests the ManageUsersRemove post action method return a redirect result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Post_Returns_RedirectResult()
        {
            //arrange
            this.IdentityService.Setup(x => x.RemoveUserAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult<string>("added"));
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };
            //act
            var response = await this.controller.ManageUsersRemove(model, _userOrganisationId, "122");
            //assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(RedirectResult));

        }
        #endregion ManageUsersRemove Post Tests


        #region Trading tests

        /// <summary>
        /// Tests that the Trading method returns the Trading view on success
        /// </summary>
        [TestMethod]
        public async Task Trading_Returns_Trading_View()
        {
            //Arrange
            Orchestrator.Setup(mock => mock.GetTradingSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new TradingSummaryViewModel(true, 1, "Fake Org Name", null, null));

            //Act
            var response = await controller.Trading(_userOrganisationId);

            //Assert
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("Trading", ((ViewResult)response).ViewName);
        }

        /// <summary>
        /// Tests that the Trading method returns the Trading view on success with different obligation perfiod
        /// </summary>
        [TestMethod]
        public async Task Trading_With_ObligationPeriod_Returns_Trading_View()
        {
            //Arrange
            Orchestrator.Setup(mock => mock.GetTradingSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new TradingSummaryViewModel(true, 1, "Fake Org Name", null, null));

            //Act
            var response = await controller.Trading(_userOrganisationId, 12);

            //Assert
            ReportingPeriodsService.Verify(x => x.GetObligationPeriod(12), Times.Once);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("Trading", ((ViewResult)response).ViewName);
        }

        /// <summary>
        /// Tests that the trading method returns the notfound result
        /// </summary>
        [TestMethod]
        public async Task Trading_Returns_NotFound()
        {
            //Arrange

            //Act
            var response = await controller.Trading(_userOrganisationId);

            //Assert
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the trading method returns Forbid result if the user doesn't have permission to view the organisation
        /// </summary>
        [TestMethod]
        public async Task Trading_Returns_Forbid()
        {
            //Arrange
            var someOtherOrganisationId = 50;

            //Act
            var response = await controller.Trading(someOtherOrganisationId);

            //Assert
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        #endregion Trading tests
        #endregion
    }
}
