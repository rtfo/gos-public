﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Website.Configuration;
using DfT.GOS.Website.Models.SuppliersViewModels;
using DfT.GOS.Website.Orchestrators;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    [TestClass]
    public class SupplierSubmissionsControllerTest
    {
        #region Constants

        private const int _userOrganisationId = 1000;

        #endregion Constants

        #region Properties

        private Mock<IReportingPeriodsService> _mockReportingPeriodsService { get; set; }
        private Mock<ISupplierReportingSummaryOrchestrator> _mockOrchestrator { get; set; }
        private Mock<IBalanceViewService> _mockBalanceViewService { get; set; }
        private Mock<IOrganisationsService> _mockOrganisationsService { get; set; }
        private Mock<ITokenService> _mockTokenService { get; set; }
        private SupplierSubmissionsController _controller { get; set; }

        #endregion Properties

        #region Initialization

        [TestInitialize]
        public void Initialize()
        {
            var appSettings = new AppSettings() { Pagination = new PaginationConfigurationSettings { MaximumPagesToDisplay = 10, ResultsPerPage = 5 } };
            var options = Options.Create(appSettings);
            _mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            _mockReportingPeriodsService.Setup(s => s.GetCurrentObligationPeriod())
                .ReturnsAsync(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null));

            _mockOrchestrator = new Mock<ISupplierReportingSummaryOrchestrator>();
            _mockBalanceViewService = new Mock<IBalanceViewService>();
            _mockTokenService = new Mock<ITokenService>();

            _mockOrganisationsService = new Mock<IOrganisationsService>();
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(
                1000, "Fake Organisation",
                1, "Fake Status",
                2, "type"))));



            _controller = new SupplierSubmissionsController(_mockReportingPeriodsService.Object
                , _mockOrchestrator.Object
                , _mockBalanceViewService.Object
                , _mockTokenService.Object
                , _mockOrganisationsService.Object
                , options);

            // - user principal
            var claims = new List<Claim>
            {
                new Claim("UserId", Guid.NewGuid().ToString()),
                new Claim("OrganisationId", _userOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialization

        #region Tests

        #region Index tests

        /// <summary>
        /// Tests that the Supplier Submissions Index method returns the Submissions view on success
        /// </summary>
        [TestMethod]
        public async Task SupplierSubmissions_Index_Returns_Submissions_View()
        {
            //Arrange
            _mockOrchestrator.Setup(mock => mock.GetSupplierSubmissionsSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new SupplierSubmissionSummaryViewModel());

            //Act
            var response = await _controller.Index(_userOrganisationId);

            //Assert
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("Submissions", ((ViewResult)response).ViewName);
        }

        /// <summary>
        /// Tests that the Supplier Submissions Index method returns the Submissions view on success with different obligation perfiod
        /// </summary>
        [TestMethod]
        public async Task SupplierSubmissions_Index_With_ObligationPeriod_Returns_Submissions_View()
        {
            //Arrange
            _mockOrchestrator.Setup(mock => mock.GetSupplierSubmissionsSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new SupplierSubmissionSummaryViewModel());

            //Act
            var response = await _controller.Index(_userOrganisationId, 12);

            //Assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(12), Times.Once);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("Submissions", ((ViewResult)response).ViewName);
        }

        /// <summary>
        /// Tests that the submissions Index method returns the notfound result
        /// </summary>
        [TestMethod]
        public async Task SupplierSubmissions_Index_Returns_Organisation_NotFound()
        {
            //Arrange

            //Act
            var response = await _controller.Index(_userOrganisationId);

            //Assert
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the submissions Index method returns Forbid result if the user doesn't have permission to view the organisation
        /// </summary>
        [TestMethod]
        public async Task SupplierSubmissions_Index_Returns_Forbid()
        {
            //Arrange
            var someOtherOrganisationId = 50;

            //Act
            var response = await _controller.Index(someOtherOrganisationId);

            //Assert
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        #endregion Index tests

        #region LiquidGasSummary tests

        /// <summary>
        /// Tests that the LiquidGasSummary method returns the expected view for the current Obligation Period
        /// </summary>
        [TestMethod]
        public async Task SupplierSubmissions_LiquidGasSummary_Returns_View_For_Current_ObligationPeriod()
        {
            //Arrange
            var currentObligationPeriodId = 15;
            this._mockReportingPeriodsService.Setup(mock => mock.GetCurrentObligationPeriod())
                .ReturnsAsync(new ObligationPeriod(currentObligationPeriodId, null, null, true, null, 0, 0, null, null, null, null, null, null));

            this._mockOrchestrator.Setup(mock => mock.GetSupplierSubmissionsLiquidGasSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new SupplierSubmissionLiquidGasSummaryViewModel());

            //Act
            var result = await _controller.LiquidGasSummary(_userOrganisationId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("LiquidAndGas", ((ViewResult)result).ViewName);
            Assert.IsNotNull(((ViewResult)result).Model);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(SupplierSubmissionLiquidGasSummaryViewModel));
        }

        /// <summary>
        /// Tests that the LiquidGasSummary method returns the expected view for the given Obligation Period
        /// </summary>
        [TestMethod]
        public async Task SupplierSubmissions_LiquidGasSummary_Returns_View_For_Given_ObligationPeriod()
        {
            //Arrange
            var obligationPeriodId = 13;
            this._mockReportingPeriodsService.Setup(mock => mock.GetObligationPeriod(obligationPeriodId))
                .ReturnsAsync(new ObligationPeriod(obligationPeriodId, null, null, true, null, 0, 0, null, null, null, null, null, null));
            this._mockOrchestrator.Setup(mock => mock.GetSupplierSubmissionsLiquidGasSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new SupplierSubmissionLiquidGasSummaryViewModel());

            //Act
            var result = await _controller.LiquidGasSummary(_userOrganisationId, obligationPeriodId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("LiquidAndGas", ((ViewResult)result).ViewName);
            Assert.IsNotNull(((ViewResult)result).Model);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(SupplierSubmissionLiquidGasSummaryViewModel));
        }

        /// <summary>
        /// Tests that the LiquidGasSummary method prevents access to organisations the user cannot view
        /// </summary>
        [TestMethod]
        public async Task SupplierSubmissions_LiquidGasSummary_Returns_Forbid()
        {
            //Arrange
            var invalidOrganisationId = 10;

            //Act
            var result = await _controller.LiquidGasSummary(invalidOrganisationId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the LiquidGasSummary method handles the situation where the balance data is not available
        /// </summary>
        [TestMethod]
        public async Task SupplierSubmissions_LiquidGasSummary_Returns_NotFound_If_Balance_Not_Available()
        {
            //Arrange

            //Act
            var result = await _controller.LiquidGasSummary(_userOrganisationId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the LiquidGasSummary method handles the situation where the requested Obligation Period does not exist
        /// </summary>
        [TestMethod]
        public async Task SupplierSubmissions_LiquidGasSummary_Returns_NotFound_If_Invalid_ObligationPeriod_Specified()
        {
            //Arrange
            var invalidObligationPeriodId = 1133;

            //Act
            var result = await _controller.LiquidGasSummary(_userOrganisationId, invalidObligationPeriodId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        #endregion LiquidGasSummary tests

        #region Trading tests

        /// <summary>
        /// Tests that the Trading method returns the Trading view on success
        /// </summary>
        [TestMethod]
        public async Task Trading_Returns_Trading_View()
        {
            //Arrange
            _mockOrchestrator.Setup(mock => mock.GetTradingSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new TradingSummaryViewModel(true, 1, "Fake Org Name", null, null));

            //Act
            var response = await _controller.Trading(_userOrganisationId);

            //Assert
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("Trading", ((ViewResult)response).ViewName);
        }

        /// <summary>
        /// Tests that the Trading method returns the Trading view on success with different obligation perfiod
        /// </summary>
        [TestMethod]
        public async Task Trading_With_ObligationPeriod_Returns_Trading_View()
        {
            //Arrange
            _mockOrchestrator.Setup(mock => mock.GetTradingSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<int>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new TradingSummaryViewModel(true, 1, "Fake Org Name", null, null));

            //Act
            var response = await _controller.Trading(_userOrganisationId, 12);

            //Assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(12), Times.Once);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("Trading", ((ViewResult)response).ViewName);
        }

        /// <summary>
        /// Tests that the trading method returns the notfound result
        /// </summary>
        [TestMethod]
        public async Task Trading_Returns_NotFound()
        {
            //Arrange

            //Act
            var response = await _controller.Trading(_userOrganisationId);

            //Assert
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the trading method returns Forbid result if the user doesn't have permission to view the organisation
        /// </summary>
        [TestMethod]
        public async Task Trading_Returns_Forbid()
        {
            //Arrange
            var someOtherOrganisationId = 50;

            //Act
            var response = await _controller.Trading(someOtherOrganisationId);

            //Assert
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        #endregion Trading tests

        #region OtherRtfoVolumes
        /// <summary>
        /// Tests OtherRtfoVolumes returns a Forbid result when try to access with other organisation id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task OtherRtfoVolumes_Returns_Forbid_With_Invalid_OrgId()
        {
            //arrange
            var otherOrgId = 2121;
            //act
            var result = await _controller.OtherRtfoVolumes(otherOrgId, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests OtherRtfoVolumes calls the GetCurrentObligationPeriod when no obligation period id is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task OtherRtfoVolumes_Calls_GetCurrentObligationPeriod_With_No_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.OtherRtfoVolumes(_userOrganisationId, 1);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetCurrentObligationPeriod(), Times.Once);
        }

        /// <summary>
        /// Tests OtherRtfoVolumes calls the GetObligationPeriod when obligation period id is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task OtherRtfoVolumes_Calls_GetObligationPeriod_When_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.OtherRtfoVolumes(_userOrganisationId, 1, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests OtherRtfoVolumes returns NotFoundResult when Orchestrator returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task OtherRtfoVolumes_Returns_NotFoundResult_When_Orchestrator_Returns_Null()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            _mockOrchestrator.Setup(x => x.GetSupplierSubmissionsOtherRtfoVolumes(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult<SupplierSubmissionOtherRtfoVolumesViewModel>(null));
            //act
            var result = await _controller.OtherRtfoVolumes(_userOrganisationId, 1, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests OtherRtfoVolumes returns ViewResult
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task OtherRtfoVolumes_Returns_ViewResult()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            _mockOrchestrator.Setup(x => x.GetSupplierSubmissionsOtherRtfoVolumes(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult<SupplierSubmissionOtherRtfoVolumesViewModel>(new SupplierSubmissionOtherRtfoVolumesViewModel()));
            //act
            var result = await _controller.OtherRtfoVolumes(_userOrganisationId, 1, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("OtherRtfoVolumes", ((ViewResult)result).ViewName);
        }

        #endregion OtherRtfoVolumes

        #region RtfoAdminConsignments
        /// <summary>
        /// Tests RtfoAdminConsignments returns a Forbid result when try to acces with other organisation id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RtfoAdminConsignments_Returns_Forbid_With_Invalid_OrgId()
        {
            //arrange
            var otherOrgId = 2121;
            //act
            var result = await _controller.RtfoAdminConsignments(otherOrgId);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests RtfoAdminConsignments calls the GetCurrentObligationPeriod when no obligation period id is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RtfoAdminConsignments_Calls_GetCurrentObligationPeriod_With_No_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.RtfoAdminConsignments(_userOrganisationId);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetCurrentObligationPeriod(), Times.Once);
        }

        /// <summary>
        /// Tests RtfoAdminConsignments calls the GetObligationPeriod when obligation period id is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RtfoAdminConsignments_Calls_GetObligationPeriod_When_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.RtfoAdminConsignments(_userOrganisationId,obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests RtfoAdminConsignments returns NotFoundResult when Orchestrator returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RtfoAdminConsignments_Returns_NotFoundResult_When_Orchestrator_Returns_Null()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            _mockOrchestrator.Setup(x => x.GetSupplierRtfoAdminConsignment(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult<SupplierRtfoAdminConsignmentViewModel>(null));
            //act
            var result = await _controller.RtfoAdminConsignments(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests RtfoAdminConsignments returns ViewResult
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RtfoAdminConsignments_Returns_ViewResult()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            _mockOrchestrator.Setup(x => x.GetSupplierRtfoAdminConsignment(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult<SupplierRtfoAdminConsignmentViewModel>(new SupplierRtfoAdminConsignmentViewModel()));
            //act
            var result = await _controller.RtfoAdminConsignments(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("RtfoAdminConsignments", ((ViewResult)result).ViewName);
        }

        #endregion RtfoAdminConsignments

        #region FossilFuels
        /// <summary>
        /// Tests RtfoAdminConsignments returns a Forbid result when try to acces with other organisation id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FossilFuels_Returns_Forbid_With_Invalid_OrgId()
        {
            //arrange
            var otherOrgId = 2121;
            //act
            var result = await _controller.FossilFuels(otherOrgId);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests RtfoAdminConsignments calls the GetCurrentObligationPeriod when no obligation period id is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FossilFuels_Calls_GetCurrentObligationPeriod_With_No_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.FossilFuels(_userOrganisationId);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetCurrentObligationPeriod(), Times.Once);
        }

        /// <summary>
        /// Tests FossilFuels calls the GetObligationPeriod when obligation period id is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FossilFuels_Calls_GetObligationPeriod_When_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.RtfoAdminConsignments(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests FossilFuels returns NotFoundResult when Orchestrator returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FossilFuels_Returns_NotFoundResult_When_Orchestrator_Returns_Null()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            _mockOrchestrator.Setup(x => x.GetFossilFuelSubmissions(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult<SupplierFossilFuelSubmissionsViewModel>(null));
            //act
            var result = await _controller.FossilFuels(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests FossilFuels returns ViewResult
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FossilFuels_Returns_ViewResult()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            _mockOrchestrator.Setup(x => x.GetFossilFuelSubmissions(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult<SupplierFossilFuelSubmissionsViewModel>(new SupplierFossilFuelSubmissionsViewModel()));
            //act
            var result = await _controller.FossilFuels(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("FossilFuels", ((ViewResult)result).ViewName);
        }

        #endregion FossilFuels

        #region Electricity
        /// <summary>
        /// Tests Electricity returns a Forbid result when try to acces with other organisation id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Electricity_Returns_Forbid_With_Invalid_OrgId()
        {
            //arrange
            var otherOrgId = 2121;
            //act
            var result = await _controller.Electricity(otherOrgId);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests Electricity calls the GetCurrentObligationPeriod when no obligation period id is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Electricity_Calls_GetCurrentObligationPeriod_With_No_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.Electricity(_userOrganisationId);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetCurrentObligationPeriod(), Times.Once);
        }

        /// <summary>
        /// Tests Electricity calls the GetObligationPeriod when obligation period id is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Electricity_Calls_GetObligationPeriod_When_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.Electricity(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests Electricity returns NotFoundResult when Orchestrator returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Electricity_Returns_NotFoundResult_When_Orchestrator_Returns_Null()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            _mockOrchestrator.Setup(x => x.GetElectricitySubmissions(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult<SupplierElectricitySubmissionsViewModel>(null));
            //act
            var result = await _controller.Electricity(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests Electricity returns ViewResult
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Electricity_Returns_ViewResult()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            _mockOrchestrator.Setup(x => x.GetElectricitySubmissions(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult<SupplierElectricitySubmissionsViewModel>(new SupplierElectricitySubmissionsViewModel()));
            //act
            var result = await _controller.Electricity(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("Electricity", ((ViewResult)result).ViewName);
        }

        #endregion Electricity

        #region UpstreamEmissionsReductions

        /// <summary>
        /// Tests UpstreamEmissionsReductions returns a Forbid result when try to acces with other organisation id
        /// </summary>
        [TestMethod]
        public async Task UpstreamEmissionsReductions_Returns_Forbid_With_Invalid_OrgId()
        {
            //arrange
            var otherOrgId = 2121;
            //act
            var result = await _controller.UpstreamEmissionsReductions(otherOrgId);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests UpstreamEmissionsReductions calls the GetCurrentObligationPeriod when no obligation period id is passed
        /// </summary>
        [TestMethod]
        public async Task UpstreamEmissionsReductions_Calls_GetCurrentObligationPeriod_With_No_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.UpstreamEmissionsReductions(_userOrganisationId);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetCurrentObligationPeriod(), Times.Once);
        }

        /// <summary>
        /// Tests UpstreamEmissionsReductions calls the GetObligationPeriod when obligation period id is passed
        /// </summary>
        [TestMethod]
        public async Task UpstreamEmissionsReductions_Calls_GetObligationPeriod_When_ObligationPeriodId_Passed()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            //act
            var result = await _controller.UpstreamEmissionsReductions(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests UpstreamEmissionsReductions returns NotFoundResult when Orchestrator returns null
        /// </summary>
        [TestMethod]
        public async Task UpstreamEmissionsReductions_Returns_NotFoundResult_When_Orchestrator_Returns_Null()
        {
            //arrange
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null)));
            _mockOrchestrator.Setup(x => x.GetUpstreamEmissionsReductionsSubmissions(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult<SupplierUpstreamEmissionsReductionsSubmissionsViewModel>(null));
            //act
            var result = await _controller.UpstreamEmissionsReductions(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests UpstreamEmissionsReductions returns ViewResult
        /// </summary>
        [TestMethod]
        public async Task UpstreamEmissionsReductions_Returns_ViewResult()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(15, null, null, true, null, 0, 0, null, null, null, null, null, null);
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(obligationPeriod));

            var viewModel = new SupplierUpstreamEmissionsReductionsSubmissionsViewModel(true, _userOrganisationId, "Fake org name", obligationPeriod, null, 1000);

            _mockOrchestrator.Setup(x => x.GetUpstreamEmissionsReductionsSubmissions(It.IsAny<int>(), It.IsAny<ObligationPeriod>()
                , It.IsAny<int>(), _controller.User))
                .Returns(Task.FromResult(viewModel));
            //act
            var result = await _controller.UpstreamEmissionsReductions(_userOrganisationId, obligationPeriodId: 15);
            //assert
            _mockReportingPeriodsService.Verify(x => x.GetObligationPeriod(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("UpstreamEmissionsReductions", ((ViewResult)result).ViewName);
        }

        #endregion UpstreamEmissionsReductions

        #region GenerateExportDownload

        /// <summary>
        /// Tests GenerateExportDownload returns a Forbid result when try to access with other organisation id
        /// </summary>
        [TestMethod]
        public async Task GenerateExportDownload_Returns_Forbid_With_Invalid_OrgId()
        {
            //arrange
            var otherOrgId = 2121;
            //act
            var result = await _controller.GenerateExportDownload(otherOrgId, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests GenerateExportDownload returns a file
        /// </summary>
        [TestMethod]
        public async Task GenerateExportDownload_Returns_File()
        {
            //arrange
            var expectedName = "FakeOrganisation-2019";
            _mockReportingPeriodsService.Setup(s => s.GetObligationPeriod(15))
                .ReturnsAsync(new ObligationPeriod(1, null, new DateTime(2019,1,1), false, null, 0, 0, null, null, null, null, null, null));

            _mockBalanceViewService.Setup(s => s.GenerateExportForOrganisation(_userOrganisationId, It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<byte[]>(System.Text.Encoding.Unicode.GetBytes(""))));

            //act
            var result = await _controller.GenerateExportDownload(_userOrganisationId, obligationPeriodId: 15);
            FileContentResult file = (FileContentResult)result;

            //assert
            Assert.IsNotNull(result);
            StringAssert.Contains(file.FileDownloadName, expectedName);
            Assert.IsInstanceOfType(result, typeof(FileResult));
        }


        #endregion GenerateExportDownload

        #region DownloadAllApplications

        /// <summary>
        /// Tests DownloadAllApplications returns a Page Not Found result when you try to access with invalid Obligation Period Id
        /// </summary>
        [TestMethod]
        public async Task DownloadAllApplications_Returns_NotFound_When_Invalid_ObligationPeriodId()
        {
            //arrange
            var invalidObligationPeriodId = 1000;

            //act
            var result = await _controller.DownloadAllApplications(invalidObligationPeriodId);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests GenerateExportDownload returns a file
        /// </summary>
        [TestMethod]
        public async Task DownloadAllApplications_Returns_File()
        {
            //arrange
            var expectedName = "AllOrganisations-2019";
            _mockReportingPeriodsService.Setup(s => s.GetObligationPeriod(15))
                .ReturnsAsync(new ObligationPeriod(1, null, new DateTime(2019, 1, 1), false, null, 0, 0, null, null, null, null, null, null));

            _mockBalanceViewService.Setup(s => s.GenerateExportForAllOrganisations(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<byte[]>(System.Text.Encoding.Unicode.GetBytes(""))));

            //act
            var result = await _controller.DownloadAllApplications(obligationPeriodId: 15);
            FileContentResult file = (FileContentResult)result;

            //assert
            Assert.IsNotNull(result);
            StringAssert.Contains(file.FileDownloadName, expectedName);
            Assert.IsInstanceOfType(result, typeof(FileResult));
        }

        #endregion DownloadAllApplications

        #endregion Tests
    }
}
