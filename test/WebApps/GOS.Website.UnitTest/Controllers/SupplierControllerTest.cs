﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Configuration;
using DfT.GOS.Website.Models.SuppliersViewModels;
using DfT.GOS.Website.Orchestrators;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using DfT.GOS.Identity.API.Client.Models;
using System.Threading;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the supplier controller
    /// </summary>
    [TestClass]
    public class SupplierControllerTest
    {
        #region Constants

        private const int _userOrganisationId = 1000;
        private const string _userId = "UserId";

        #endregion Constants

        #region Properties

        private Mock<IOrganisationsService> _mockOrganisationsService { get; set; }
        private Mock<IUsersService> _mockIdentityService { get; set; }
        private Mock<IReportingPeriodsService> _mockReportingPeriodsService { get; set; }
        private Mock<ILogger<SuppliersController>> _mockLogger { get; set; }
        private Mock<IMapper> _mockMapper { get; set; }
        private Mock<ITokenService> _mockTokenService { get; set; }
        private Mock<IBalanceViewService> _mockBalanceViewService { get; set; }
        private Mock<ISupplierReportingSummaryOrchestrator> _mockOrchestrator { get; set; }
        private SuppliersController _controller { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialisation before each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            _mockLogger = new Mock<ILogger<SuppliersController>>();
            _mockMapper = new Mock<IMapper>();          
            _mockTokenService = new Mock<ITokenService>();
            _mockIdentityService = new Mock<IUsersService>();
            _mockOrganisationsService = new Mock<IOrganisationsService>();

            _mockBalanceViewService = new Mock<IBalanceViewService>();
            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()
                                                                                                    , It.IsAny<decimal>()))));

            _mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            _mockReportingPeriodsService.Setup(s => s.GetCurrentObligationPeriod()).Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));

            var appSettings = new AppSettings { Pagination = new PaginationConfigurationSettings {
                MaximumPagesToDisplay = 10, ResultsPerPage = 5 } };
            var options = Options.Create(appSettings);
            _mockTokenService.Setup(x => x.GetJwtToken())
                .Returns(Task.FromResult(It.IsAny<string>()));
            _mockOrchestrator = new Mock<ISupplierReportingSummaryOrchestrator>();

            _controller = new SuppliersController(_mockOrganisationsService.Object
                , _mockIdentityService.Object
                , _mockReportingPeriodsService.Object
                , _mockLogger.Object
                , _mockTokenService.Object
                , _mockOrchestrator.Object);
            // - user principal
            var claims = new List<Claim>
            {
                new Claim("UserId",_userId),
                new Claim("OrganisationId", _userOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialisation

        #region Tests

        #region Supplier Home test

        /// <summary>
        /// Tests that the Supplier Home method returns the SupplierHome view on success
        /// </summary>
        [TestMethod]
        public async Task SupplierHome_Returns_SupplierHome_View()
        {
            //Arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 1, "Fake Status", 1, "Supplier");

            _mockOrganisationsService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpObjectResponse<Organisation>>(new HttpObjectResponse<Organisation>(fakeOrganisation)));

            _mockOrchestrator.Setup(mock => mock.GetSupplierReportingSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new SupplierReportingSummaryViewModel());

            //Act
            var response = await _controller.SupplierHome(_userOrganisationId,15);

            //Assert
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("SupplierHome", ((ViewResult)response).ViewName);
        }

        /// <summary>
        /// Tests that the Supplier Home method returns a Not Found when no organisation is found
        /// </summary>
        [TestMethod]
        public async Task SupplierHome_Returns_NotFound_With_NotFound_Organisation()
        {
            //Arrange
            _mockOrganisationsService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpObjectResponse<Organisation>>(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //Act
            var response = await _controller.SupplierHome(_userOrganisationId,15);

            //Assert
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the Supplier Home method returns a Forbid if the user doesn't have permission to view the organisation
        /// </summary>
        [TestMethod]
        public async Task SupplierHome_Returns_Forbid()
        {
            //Arrange
            var someOtherOrganisationId = 50;

            //Act
            var response = await _controller.SupplierHome(someOtherOrganisationId,15);

            //Assert
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }
        #endregion  Supplier Home test


        #region FurtherActions Tests
        /// <summary>
        /// Tests the FurtherActions action method return a forbid result when a incorrect organisation is if supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FurtherActions_Returns_Forbid_Result()
        {
            //Arrange
            var fakeOrganisationId = 22;

            //Act
            var response = await _controller.FurtherActions(fakeOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the FurtherActions action method return a notfound result when a incorrect organisation is supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FurtherActions_Returns_NotFound()
        {
            //Arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));


            //Act
            var response = await _controller.FurtherActions(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests the FurtherActions action method return a notfound result when organisation service results forbidden result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FurtherActions_Returns_Forbidden_When_OrganisationService_Returns_Forbidden()
        {
            //Arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.Forbidden)));
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));


            //Act
            var response = await _controller.FurtherActions(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the FurtherActions action method return a view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task FurtherActions_Returns_ViewResult()
        {
            //Arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null)));


            //Act
            var response = await _controller.FurtherActions(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("FurtherActions", ((ViewResult)response).ViewName);
            Assert.IsInstanceOfType(((ViewResult)response).Model, typeof(SupplierFurtherActionsViewModel));
        }
        #endregion FurtherActions Tests

        #region ManageUsers Tests
        /// <summary>
        /// Tests the ManageUsers action method return a forbid result when a incorrect organisation is if supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Returns_Forbid()
        {
            //Arrange
            var fakeOrganisationId = 22;

            //Act
            var response = await _controller.ManageUsers(fakeOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ManageUsers action method return a not found result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Returns_NotFoundResult()
        {
            //Arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //Act
            var response = await _controller.ManageUsers(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests the ManageUsers action method return a not found result when organisation service results forbidden result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Returns_ForbidResult_When_OrganisationService_Returns_Forbidden()
        {
            //Arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.Forbidden)));

            //Act
            var response = await _controller.ManageUsers(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ManageUsers action method return a view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Returns_ViewResult()
        {
            //Arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            _mockIdentityService.Setup(x => x.GetOrganisationUsers(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<GosApplicationUser> { new GosApplicationUser() }));
            //Act
            var response = await _controller.ManageUsers(_userOrganisationId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsers", ((ViewResult)response).ViewName);
            Assert.IsInstanceOfType(((ViewResult)response).Model, typeof(ViewSupplierViewModel));
        }
        #endregion ManageUsers Tests

        #region ManageUsers Post tests
        /// <summary>
        /// Tests the ManageUsers post action method return a forbidden result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_Forbid()
        {
            //Arrange
            var fakeOrganisationId = 22;
            var model = new ViewSupplierViewModel { Id = fakeOrganisationId };
            //Act
            var response = await _controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }
        /// <summary>
        /// Tests the ManageUsers post action method return a not found result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_NotFoundResult()
        {
            //Arrange
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                   HttpStatusCode.NotFound)));
            //act
            var response = await _controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests the ManageUsers post action method return a view result with model errors
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_Model_With_Error()
        {
            //Arrange
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            this._mockIdentityService.Setup(x => x.AddUserAsync(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromException<AddNewGosApplicationUserResult>(new Exception()));
            //act
            var response = await _controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsersConfirmationError", ((ViewResult)response).ViewName);
            Assert.IsInstanceOfType(((ViewResult)response).Model, typeof(ViewSupplierViewModel));
        }
        /// <summary>
        /// Tests the ManageUsers post action method return a view result with model errors while adding user
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_Model_Errors_While_AddingUser()
        {
            //Arrange
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            this._mockIdentityService.Setup(x => x.AddUserAsync(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult<AddNewGosApplicationUserResult>(new AddNewGosApplicationUserResult { Message = "Error" }));
            //act
            var response = await _controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)response).Model, typeof(ViewSupplierViewModel));
        }
        /// <summary>
        /// Tests the ManageUsers post action method return a view result ManageUsersLinkedConfirmation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_ViewResult_UserAssociation()
        {
            //Arrange
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            this._mockIdentityService.Setup(x => x.AddUserAsync(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult<AddNewGosApplicationUserResult>(new AddNewGosApplicationUserResult
                { Message = "", UserAssociatedWithOrganisation = true }));
            _mockIdentityService.Setup(x => x.GetOrganisationUsers(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<GosApplicationUser> { new GosApplicationUser() }));
            //act
            var response = await _controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsersLinkedConfirmation", ((ViewResult)response).ViewName);
            Assert.IsInstanceOfType(((ViewResult)response).Model, typeof(ViewSupplierViewModel));
        }

        /// <summary>
        /// Tests the ManageUsers post action method return a view result UserConfirmation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsers_Post_Returns_ViewResult_UserConfirmation()
        {
            //Arrange
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            this._mockIdentityService.Setup(x => x.AddUserAsync(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult<AddNewGosApplicationUserResult>(new AddNewGosApplicationUserResult
                { Message = "", UserAssociatedWithOrganisation = false }));
            this._mockIdentityService.Setup(x => x.GetOrganisationUsers(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<GosApplicationUser> { new GosApplicationUser() }));
            //act
            var response = await _controller.ManageUsers(model);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsersConfirmation", ((ViewResult)response).ViewName);
            Assert.IsInstanceOfType(((ViewResult)response).Model, typeof(ViewSupplierViewModel));
        }
        #endregion ManageUsers Post Tests

        #region ManageUsersRemove Tests
        /// <summary>
        /// Tests the ManageUsersRemove action method return a forbid result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Returns_Forbid()
        {
            //Arrange
            var fakeOrganisationId = 22;

            //Act
            var response = await _controller.ManageUsersRemove(fakeOrganisationId, "userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ManageUsersRemove action method return a notfound result when user is null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Returns_NotFoundResult_When_UserIsNull()
        {
            //Arrange
            this._mockIdentityService.Setup(x => x.FindUserByIdAsync(It.IsAny<string>()
                , It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult<GosApplicationUser>(null));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                   new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            //Act
            var response = await _controller.ManageUsersRemove(_userOrganisationId, "userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundObjectResult));
        }
        /// <summary>
        /// Tests the ManageUsersRemove action method return a not found result when organisation is null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Returns_NotFoundResult_When_OrganisationIsNull()
        {
            //Arrange
            this._mockIdentityService.Setup(x => x.FindUserByIdAsync(It.IsAny<string>()
                , It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser()));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            //Act
            var response = await _controller.ManageUsersRemove(_userOrganisationId, "userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests the ManageUsersRemove action method return a view result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Returns_ViewResult()
        {
            //Arrange
            this._mockIdentityService.Setup(x => x.FindUserByIdAsync(It.IsAny<string>()
                , It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser()));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                  new Organisation(_userOrganisationId, "Supplier", 1, "Status", 1, "Supplier"))));
            //Act
            var response = await _controller.ManageUsersRemove(_userOrganisationId, "userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ViewResult));
            Assert.AreEqual("ManageUsersRemove", ((ViewResult)response).ViewName);
            Assert.IsInstanceOfType(((ViewResult)response).Model, typeof(ViewSupplierUserViewModel));
        }
        #endregion ManageUsersRemove Tests

        #region ManageUsersRemove Post Tests
        /// <summary>
        /// Tests the ManageUsersRemove post action method return a forbid result when organisation is incorrect
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Post_Returns_Forbid_Incorrect_OrgId()
        {
            //Arrange
            var fakeOrganisationId = 22;
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };

            //Act
            var response = await _controller.ManageUsersRemove(model, fakeOrganisationId, "userId");

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ManageUsersRemove post action method return a forbid result when userid is incorrect
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Post_Returns_Forbid_Incorrect_UserId()
        {
            //Arrange
            var userId = _userId;
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };

            //Act
            var response = await _controller.ManageUsersRemove(model, _userOrganisationId, userId);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ManageUsersRemove post action method return a redirect result when exception is thrown
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Post_Returns_RedirectResult_Exception()
        {
            //arrange
            this._mockIdentityService.Setup(x => x.RemoveUserAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromException<string>(new Exception()));
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };
            //act
            var response = await this._controller.ManageUsersRemove(model, _userOrganisationId, "122");
            //assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(RedirectResult));
            Assert.AreEqual(((RedirectResult)response).Url, $"/suppliers/{_userOrganisationId}/manageusers");
        }

        /// <summary>
        /// Tests the ManageUsersRemove post action method return a redirect result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ManageUsersRemove_Post_Returns_RedirectResult()
        {
            //arrange
            this._mockIdentityService.Setup(x => x.RemoveUserAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult<string>("added"));
            var model = new ViewSupplierViewModel { Id = _userOrganisationId };
            //act
            var response = await this._controller.ManageUsersRemove(model, _userOrganisationId, "122");
            //assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(RedirectResult));
            Assert.AreEqual(((RedirectResult)response).Url, $"/suppliers/{_userOrganisationId}/manageusers");
        }
        #endregion ManageUsersRemove Post Tests
        #endregion Tests
    }
}
