﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgBalanceOrchestrator.API.Client.Models;
using DfT.GOS.GhgBalanceOrchestrator.API.Client.Services;
using DfT.GOS.GhgLedger.API.Client.Services;
using DfT.GOS.GhgLedger.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Identity.Common.Models;
using DfT.GOS.RtfoApplications.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Services;
using GOS.RtfoApplications.Common.Models;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    /// <summary>
    /// Tests for the System Administration Controller
    /// </summary>
    [TestClass]
    public class SystemAdministrationControllerTest
    {
        #region Private variables

        private Mock<IBalanceAdministrationService> _mockBalanceAdministrationService;
        private Mock<ITokenService> _mockTokenService;
        private Mock<IApplicationsService> _mockApplicationsService;
        private Mock<IUsersService> _mockUsersService;
        private Mock<ILedgerService> _mockLedgerService;
        private Mock<IRtfoPerformanceDataService> _mockRtfoPerformanceDataService;
        private Mock<IPerformanceDataHelperService> _mockPerformanceDataHelperService;
        private SystemAdministrationController _controller;
        private Mock<ILogger<SystemAdministrationController>> _mockLogger;

        #endregion Private variable

        #region Test initialisation

        /// <summary>
        /// Setup to be run before each test
        /// </summary>
        [TestInitialize]
        public void Initialisation()
        {
            _mockApplicationsService = new Mock<IApplicationsService>();
            _mockUsersService = new Mock<IUsersService>();
            _mockLedgerService = new Mock<ILedgerService>();
            _mockBalanceAdministrationService = new Mock<IBalanceAdministrationService>();
            _mockRtfoPerformanceDataService = new Mock<IRtfoPerformanceDataService>();
            _mockPerformanceDataHelperService = new Mock<IPerformanceDataHelperService>();
            _mockTokenService = new Mock<ITokenService>();
            var mockOptions = new Mock<IOptions<AppSettings>>();
            mockOptions.Setup(mock => mock.Value)
                .Returns(new AppSettings() { UpdateGhgBalanceConfirmationRefreshIntervalInSeconds = 5 });
            _mockLogger = new Mock<ILogger<SystemAdministrationController>>();

            _controller = new SystemAdministrationController(_mockApplicationsService.Object
                , _mockUsersService.Object
                , _mockLedgerService.Object
                , _mockBalanceAdministrationService.Object
                , _mockRtfoPerformanceDataService.Object
                , _mockPerformanceDataHelperService.Object
                , _mockTokenService.Object
                , mockOptions.Object
                , _mockLogger.Object);
        }

        #endregion Test initialisation

        #region GHG Balance Tests

        #region GhgBalances Tests

        /// <summary>
        /// Tests that the GHGBalances Get method returns the correct view
        /// </summary>
        [TestMethod]
        public void GhgBalances_Returns_View()
        {
            //Arrange 
            //Act
            var result = _controller.GhgBalances(null);

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsNull(((ViewResult)result).ViewName);
        }

        #endregion GhgBalances Tests

        #region UpdateGhgBalances Tests

        /// <summary>
        /// Tests that the update GHG balances POST method handles a service error
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalances_Post_Handles_Service_Error()
        {
            //Arrange
            _mockBalanceAdministrationService.Setup(s => s.UpdateCollections(It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<string>(HttpStatusCode.Forbidden));

            //Act
            var result = await _controller.UpdateGhgBalances(null);

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("GhgBalances", ((ViewResult)result).ViewName);
            Assert.AreEqual(1, _controller.ModelState.ErrorCount);
        }

        /// <summary>
        /// Tests that the update GHG balances POST method handles a TaskCanceled exception
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalances_Post_Handles_TaskCanceledException()
        {
            //Arrange
            _mockBalanceAdministrationService.Setup(s => s.UpdateCollections(It.IsAny<string>()))
                .ThrowsAsync(new TaskCanceledException("test error"));

            //Act
            var result = await _controller.UpdateGhgBalances(null);

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("GhgBalances", ((ViewResult)result).ViewName);
            Assert.AreEqual(1, _controller.ModelState.ErrorCount);
        }

        /// <summary>
        /// Tests that the update GHG balances POST method redirects to the confirmation page on success from the service
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalances_Post_Handles_Redirects_To_Confirmation_Page_On_Success()
        {
            //Arrange
            _mockBalanceAdministrationService.Setup(s => s.UpdateCollections(It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<string>(string.Empty));

            //Act
            var result = await _controller.UpdateGhgBalances(null);

            //Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("UpdateGhgBalances", ((RedirectToActionResult)result).ActionName);
        }

        #endregion UpdateGhgBalances Tests

        #region UpdateGhgBalanceConfirmation Tests

        /// <summary>
        /// Tests that the UpdateGhgBalances Get method returns the correct view
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalances_Get_Returns_View()
        {
            //Arrange
            var bulkUpdateUniqueId = string.Empty;
            this._mockBalanceAdministrationService.Setup(mock => mock.GetUpdateStatus(bulkUpdateUniqueId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<CollectionAdministration>(new CollectionAdministration() { Status = CollectionAdministrationStatus.Succeeded }));

            //Act
            var result = await _controller.UpdateGhgBalances(bulkUpdateUniqueId, null);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("UpdateGhgBalanceConfirmation", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests that the UpdateGhgBalances Get method handles errors as expected
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalances_Get_Handles_Error()
        {
            //Arrange
            var bulkUpdateUniqueId = string.Empty;
            this._mockBalanceAdministrationService.Setup(mock => mock.GetUpdateStatus(bulkUpdateUniqueId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<CollectionAdministration>(HttpStatusCode.Unauthorized));

            //Act
            var result = await _controller.UpdateGhgBalances(bulkUpdateUniqueId, null);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("UpdateGhgBalanceConfirmation", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests that the UpdateGhgBalances Get method handles a TaskCanceledException
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalances_Get_Handles_TaskCanceledException()
        {
            //Arrange
            var bulkUpdateUniqueId = string.Empty;
            this._mockBalanceAdministrationService.Setup(mock => mock.GetUpdateStatus(bulkUpdateUniqueId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<CollectionAdministration>(HttpStatusCode.Unauthorized));

            //Act
            var result = await _controller.UpdateGhgBalances(bulkUpdateUniqueId, null);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("UpdateGhgBalanceConfirmation", ((ViewResult)result).ViewName);
        }

        #endregion UpdateGhgBalanceConfirmation Tests

        #region UpdateGhgBalanceConfirmationPartial Tests

        /// <summary>
        /// Tests that the UpdateGhgBalancePartial Get method returns the correct view
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalancesPartial_Returns_View()
        {
            //Arrange
            var bulkUpdateUniqueId = string.Empty;
            this._mockBalanceAdministrationService.Setup(mock => mock.GetUpdateStatus(bulkUpdateUniqueId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<CollectionAdministration>(new CollectionAdministration() { Status = CollectionAdministrationStatus.Succeeded }));

            //Act
            var result = await _controller.UpdateGhgBalancesPartial(bulkUpdateUniqueId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(PartialViewResult));
            Assert.AreEqual("_UpdateGhgBalanceConfirmationPartial", ((PartialViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests that the UpdateGhgBalancePartial Get method handles errors as expected
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalancesPartial_Handles_Error()
        {
            //Arrange
            var bulkUpdateUniqueId = string.Empty;
            this._mockBalanceAdministrationService.Setup(mock => mock.GetUpdateStatus(bulkUpdateUniqueId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<CollectionAdministration>(HttpStatusCode.Unauthorized));

            //Act
            var result = await _controller.UpdateGhgBalancesPartial(bulkUpdateUniqueId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(PartialViewResult));
            Assert.AreEqual("_UpdateGhgBalanceConfirmationPartial", ((PartialViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests that the UpdateGhgBalancePartial Get method handles a TaskCanceledException
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalancesPartial_Handles_TaskCanceledException()
        {
            //Arrange
            var bulkUpdateUniqueId = string.Empty;
            this._mockBalanceAdministrationService.Setup(mock => mock.GetUpdateStatus(bulkUpdateUniqueId, It.IsAny<string>()))
                .ThrowsAsync(new TaskCanceledException("test error"));

            //Act
            var result = await _controller.UpdateGhgBalancesPartial(bulkUpdateUniqueId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(PartialViewResult));
            Assert.AreEqual("_UpdateGhgBalanceConfirmationPartial", ((PartialViewResult)result).ViewName);
        }

        #endregion UpdateGhgBalanceConfirmationPartial Tests

        #endregion GHG Balance Tests

        #region GeneratePerformanceDataDownload

        /// <summary>
        /// Tests GeneratePerformanceDataDownload returns a file
        /// </summary>
        [TestMethod]
        public async Task GeneratePerformanceDataDownload_Returns_File()
        {
            //arrange
            var expectedName = "PerformanceData-";
            _mockApplicationsService.Setup(s => s.GetApplicationPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new List<ApplicationPerformanceData>()));

            _mockUsersService.Setup(s => s.RegistrationsPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new List<RegistrationsPerformanceData>()));

            _mockLedgerService.Setup(s => s.GetLedgerPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new List<LedgerPerformanceData>()));

            _mockRtfoPerformanceDataService.Setup(s => s.GetPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IEnumerable<RtfoPerformanceData>>(new List<RtfoPerformanceData>())));

            _mockPerformanceDataHelperService.Setup(s => s.GetConcatenatedPerformanceData(new List<PerformanceData>(), new List<PerformanceData>(), new List<PerformanceData>(), new List<PerformanceData>()))
                .Returns(new List<PerformanceData>{ new PerformanceData(new System.DateTime(2019, 1, 1), 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1) } );

            //act
            var result = await _controller.GeneratePerformanceDataDownload();
            FileContentResult file = (FileContentResult)result;

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(FileResult));
            StringAssert.Contains(file.FileDownloadName, expectedName);

        }

        /// <summary>
        /// Tests GeneratePerformanceDataDownload returns a file and logs a message event if
        /// the RTFO Application service (i.e. ROS connection) cannot be successfully made
        /// </summary>
        [TestMethod]
        public async Task GeneratePerformanceDataDownload_Handles_Rtfo_Service_Failure()
        {
            //arrange
            var expectedName = "PerformanceData-";
            _mockApplicationsService.Setup(s => s.GetApplicationPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new List<ApplicationPerformanceData>()));

            _mockUsersService.Setup(s => s.RegistrationsPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new List<RegistrationsPerformanceData>()));

            _mockLedgerService.Setup(s => s.GetLedgerPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new List<LedgerPerformanceData>()));

            _mockRtfoPerformanceDataService.Setup(s => s.GetPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IEnumerable<RtfoPerformanceData>>(HttpStatusCode.InternalServerError)));

            _mockPerformanceDataHelperService.Setup(s => s.GetConcatenatedPerformanceData(new List<PerformanceData>(), new List<PerformanceData>(), new List<PerformanceData>(), new List<PerformanceData>()))
                .Returns(new List<PerformanceData> { new PerformanceData(new System.DateTime(2019, 1, 1), 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0) });

            //act
            var result = await _controller.GeneratePerformanceDataDownload();
            FileContentResult file = (FileContentResult)result;

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(FileResult));
            StringAssert.Contains(file.FileDownloadName, expectedName);

            // - it logged the correct number of warnings
            _mockLogger.Verify(l => l.Log(LogLevel.Error,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((a, b) => true),
                It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Exactly(1));

        }


        #endregion GeneratePerformanceDataDownload

        #region DownloadAllPerformanceData

        /// <summary>
        /// Tests DownloadAllPerformanceData returns an IActionResult
        /// </summary>
        [TestMethod]
        public void DownloadAllPerformanceData_Returns_IActionResult()
        {
            //arrange
            _mockApplicationsService.Setup(s => s.GetApplicationPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new List<ApplicationPerformanceData>()));

            _mockUsersService.Setup(s => s.RegistrationsPerformanceData(It.IsAny<string>()))
                .Returns(Task.FromResult(new List<RegistrationsPerformanceData>()));

            //act
            var result = _controller.DownloadAllPerformanceData(null);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IActionResult));
        }

        #endregion DownloadAllPerformanceData
    }
}
