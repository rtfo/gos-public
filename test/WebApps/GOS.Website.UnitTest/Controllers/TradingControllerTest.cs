﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.GhgLedger.API.Client.Services;
using DfT.GOS.GhgLedger.Common.Exceptions;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Services;
using DfT.GOS.Web.Models;
using DfT.GOS.Website.Configuration;
using DfT.GOS.Website.Models.SuppliersViewModels;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
namespace DfT.GOS.Website.UnitTest.Controllers
{
    [TestClass]
    public class TradingControllerTest
    {
        #region private members
        private Mock<ILogger<TradingController>> _mockLogger;
        private Mock<IOrganisationsService> _mockOrganisationsService;
        private Mock<IReportingPeriodsService> _mockReportingPeriodsService;
        private Mock<IBalanceViewService> BalanceViewService;
        private Mock<ILedgerService> LedgerService;
        private Mock<ITokenService> _mockTokenService;
        private Mock<IOptions<AppSettings>> _mockAppSettings;
        private TradingController Controller;
        const int _userOrganisationId = 1000;
        #endregion

        #region Initialize
        [TestInitialize]
        public void Initialize()
        {
            _mockOrganisationsService = new Mock<IOrganisationsService>();
            _mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            _mockTokenService = new Mock<ITokenService>();
            _mockAppSettings = new Mock<IOptions<AppSettings>>();
            _mockLogger = new Mock<ILogger<TradingController>>();
            BalanceViewService = new Mock<IBalanceViewService>();
            LedgerService = new Mock<ILedgerService>();

            var appSettings = new AppSettings()
            {
                Pagination = new PaginationConfigurationSettings
                {
                    MaximumPagesToDisplay = 10,
                    ResultsPerPage = 5
                }
            };
            var options = Options.Create(appSettings);
            Controller = new TradingController(options, _mockOrganisationsService.Object
                , _mockLogger.Object, _mockTokenService.Object, LedgerService.Object, BalanceViewService.Object
                , _mockReportingPeriodsService.Object);
            // - user principal
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", Guid.NewGuid().ToString()));
            claims.Add(new Claim("OrganisationId", _userOrganisationId.ToString()));

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }
        #endregion

        #region Index
        [TestMethod]
        public async Task Index_Get_Returns_NotFound_Result_With_Invalid_OrganisationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            //act
            var result = await Controller.Index(33, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }
        [TestMethod]
        public async Task Index_Get_Returns_NotFound_Result_With_Invalid_ObligationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                 .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
               .Returns(Task.FromResult<ObligationPeriod>(null));
            //act
            var result = await Controller.Index(33, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }
        /// <summary>
        /// Tests Index returns view result
        /// </summary>
        [TestMethod]
        public async Task Index_Get_Returns_Result()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            _mockOrganisationsService.Setup(x => x.GetSuppliersAndTraders(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<Organisation>>(
                    new PagedResult<Organisation>()
                    {
                        Result = new List<Organisation>() { new Organisation(_userOrganisationId, "SupplierName1", 2, "", 1, "") }
                    })));

            //act
            var result = await Controller.Index(_userOrganisationId, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Tests Index returns forbid result when user tries to access other organisation
        /// </summary>
        [TestMethod]
        public async Task Index_Get_Returns_Forbid()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            _mockOrganisationsService.Setup(x => x.GetSuppliers(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<Organisation>>(
                    new PagedResult<Organisation>() { Result = new List<Organisation>() })));

            //act
            var result = await Controller.Index(111, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the Index with search text returns a redirecttoactionresult
        /// </summary>
        [TestMethod]
        public void Index_Post_Returns_RedirectResult()
        {
            //arrange

            //act
            var result = Controller.Index(_userOrganisationId, 13, "s", 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }
        #endregion

        #region TransferCredits
        /// <summary>
        /// Tests TransferCredits Post action return forbid result with invalid organisation
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Post_Returns_Forbid_With_Invalid_OrganisationId()
        {
            //arrange
            var invalidOrgId = 1221;

            //act
            var result = await Controller.TransferCredits(invalidOrgId, 22, 15, null);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests TransferCredits Post action return notfound result with invalid organisation
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Post_Returns_NotFound_With_Invalid_OrganisationId()
        {
            //arrange
            this._mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 22, 15, null);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests TransferCredits Post action return notfound result with invalid recipient organisation
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Post_Returns_NotFound_With_Invalid_Recipient_OrganisationId()
        {
            //arrange
            this._mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            this._mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 22, 15, null);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests TransferCredits Post action return notfound result with invalid ObligationId
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Post_Returns_NotFound_With_Invalid_ObligationId()
        {
            //arrange
            this._mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            this._mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
           .Returns(Task.FromResult<ObligationPeriod>(null));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 22, 15, null);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests TransferCredits Post action return ModelError with no balance summary
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Post_Returns_ModelError_With_No_BalanceSummary()
        {
            //arrange
            this._mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            this._mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
           .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                 It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                 , It.IsAny<int>(), new DateTime(2018, 11, 2), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                 , It.IsAny<int>(), It.IsAny<decimal>())));
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(HttpStatusCode.NotFound)));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 22, 15, new TransferCreditsViewModel() { NumberOfCreditsToTransfer = 99 });

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var errors = Controller.ModelState.Values
                .SelectMany(e => e.Errors)
                .Where(e => !string.IsNullOrEmpty(e.ErrorMessage))
                .Select(e => e.ErrorMessage).ToList();
            Assert.AreEqual(1, errors.Count);
            Assert.IsNotNull(((ViewResult)result).Model);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(TransferCreditsViewModel));
            Assert.AreEqual(0, ((TransferCreditsViewModel)(((ViewResult)result).Model)).AvailableCreditBalance);
        }

        /// <summary>
        /// Tests TransferCredits Post action return in valid view model state when the RedemptionDate is expired
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Post_Returns_ModelError_With_Expired_RedemptionDate()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
            .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                 It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                 , It.IsAny<int>(), new DateTime(2018, 11, 2), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                 , It.IsAny<int>(), It.IsAny<decimal>())));
            var balanceSummary = new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100);
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(balanceSummary)));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 22, 15, new TransferCreditsViewModel() { NumberOfCreditsToTransfer = 99 });

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.AreEqual(1, Controller.ModelState.ErrorCount);
            var errors = Controller.ModelState.Values
                .SelectMany(e => e.Errors)
                .Where(e => !string.IsNullOrEmpty(e.ErrorMessage))
                .Select(e => e.ErrorMessage).ToList()[0];
            Assert.AreEqual("Transfers are no longer allowed for the Obligation Period.", errors);
            Assert.AreEqual("TransferCredits", ((ViewResult)result).ViewName);
            Assert.IsNotNull(((ViewResult)result).Model);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(TransferCreditsViewModel));
            Assert.AreEqual(balanceSummary.BalanceCredits, ((TransferCreditsViewModel)(((ViewResult)result).Model)).AvailableCreditBalance);
        }

        /// <summary>
        /// Tests TransferCredits Post action return invalid view model state when there is insufficient balance
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Post_Returns_ModelError_With_Insufficient_Balance()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
            .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                 It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                 , It.IsAny<int>(), new DateTime(2019, 11, 2), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                 , It.IsAny<int>(), It.IsAny<decimal>())));
            var balanceSummary = new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100);
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(balanceSummary)));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 22, 15, new TransferCreditsViewModel() { NumberOfCreditsToTransfer = 111 });

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.AreEqual(1, Controller.ModelState.ErrorCount);
            var errors = Controller.ModelState.Values
                .SelectMany(e => e.Errors)
                .Where(e => !string.IsNullOrEmpty(e.ErrorMessage))
                .Select(e => e.ErrorMessage).ToList()[0];
            Assert.AreEqual("Number of credits to transfer exceeds the credits available for transfer.", errors);
            Assert.AreEqual("TransferCredits", ((ViewResult)result).ViewName);
            Assert.IsNotNull(((ViewResult)result).Model);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(TransferCreditsViewModel));
            Assert.AreEqual(balanceSummary.BalanceCredits, ((TransferCreditsViewModel)(((ViewResult)result).Model)).AvailableCreditBalance);
        }

        /// <summary>
        /// Tests TransferCredits Post action handles a Model State error (simulating logic when the user enters 0 credits or a negative number of credits to transfer
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Post_Handles_ModelState_Error()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
            .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                 It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                 , It.IsAny<int>(), new DateTime(2019, 11, 2), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                 , It.IsAny<int>(), It.IsAny<decimal>())));
            var balanceSummary = new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100);
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(balanceSummary)));
            this.Controller.ModelState.AddModelError("NumberOfCreditsToTransfer", "test error");

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 22, 15, new TransferCreditsViewModel() { NumberOfCreditsToTransfer = 0 });

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.AreEqual("TransferCredits", ((ViewResult)result).ViewName);
            Assert.IsNotNull(((ViewResult)result).Model);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(TransferCreditsViewModel));
            Assert.AreEqual(balanceSummary.BalanceCredits, ((TransferCreditsViewModel)(((ViewResult)result).Model)).AvailableCreditBalance);
        }

        #endregion TransferCredits

        #region EnterCredits
        /// <summary>
        /// Tests EnterCredits returns Forbid result with invalid OrgId
        /// </summary>
        [TestMethod]
        public async Task EnterCredits_Get_Returns_Forbid_Result_With_Invalid_OrganisationId()
        {
            //act
            var result = await Controller.TransferCredits(12, 12, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }
        /// <summary>
        /// Tests EnterCredits returns notfound result with invalid OrgId
        /// </summary>
        [TestMethod]
        public async Task EnterCreditst_Get_Returns_NotFound_With_Invalid_OrganisationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 12, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }
        /// <summary>
        /// Tests EnterCredits returns notfound result with invalid RecipientOrgId
        /// </summary>
        [TestMethod]
        public async Task EnterCredits_Get_Returns_NotFound_With_Invalid_Recipient_OrganisationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(12, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 12, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }
        /// <summary>
        /// Tests EnterCredits returns notfound result with invalid ObligationId
        /// </summary>
        [TestMethod]
        public async Task EnterCredits_Get_Returns_NotFound_With_Invalid_ObligationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(101, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
             .Returns(Task.FromResult<ObligationPeriod>(null));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 101, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }
        /// <summary>
        /// Tests the EnterCredits returns view result with the available balance
        /// </summary>
        [TestMethod]
        public async Task EnterCredits_Get_Returns_Result()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 12, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            _mockOrganisationsService.Verify(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()), Times.Exactly(2));
            BalanceViewService.Verify(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }
        /// <summary>
        /// Tests the EnterCredits returns view result with a warning when the available balance information is not available
        /// </summary>
        [TestMethod]
        public async Task EnterCredits_Get_Handles_No_Balance_Details()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<BalanceSummary>(HttpStatusCode.NotFound));

            //act
            var result = await Controller.TransferCredits(_userOrganisationId, 12, 13);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            _mockOrganisationsService.Verify(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()), Times.Exactly(2));
            BalanceViewService.Verify(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            Assert.IsFalse(Controller.ModelState.IsValid);
        }
        #endregion EnterCredits

        #region TransferConfirmation
        /// <summary>
        /// Tests TransferConfirmation returns invalid model state with invalid OrganisationId
        /// </summary>
        [TestMethod]
        public async Task TransferConfirmation_Post_Returns_Invalid_ModelState_Invalid_OrgId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferCredits", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests TransferConfirmation returns invalid model state with invalid RecipientOrgId
        /// </summary>
        [TestMethod]
        public async Task TransferConfirmation_Post_Returns_Invalid_ModelState_Invalid_RecipientOrgId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferCredits", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests TransferConfirmation returns invalid model state with invalid obligation period id 
        /// </summary>
        [TestMethod]
        public async Task TransferConfirmation_Post_Returns_Invalid_ModelState_Invalid_ObligationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
           .Returns(Task.FromResult<ObligationPeriod>(null));

            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferCredits", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests the TransferConfirmation returns a error
        /// </summary>
        [TestMethod]
        public async Task TransferConfirmation_Post_Returns_Error()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            LedgerService.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromException<int>(new Exception()));
            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferCredits", ((ViewResult)result).ViewName);
        }
        /// <summary>
        /// Tests the TransferConfirmation returns a Forbid when invalid supplier is passed
        /// </summary>
        [TestMethod]
        public async Task TransferConfirmation_Post_Returns_Forbid()
        {
            //arrange
            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = 12,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the TransferConfirmation return a confirmation view
        /// </summary>
        [TestMethod]
        public async Task TransferConfirmation_Post_Returns_Result()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
               , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
           .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            LedgerService.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));
            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferConfirmation", ((ViewResult)result).ViewName);
        }
        #endregion

        #region TransferredConfirmation
        /// <summary>
        /// Tests TransferredConfirmation returns invalid model state with invalid OrganisationId
        /// </summary>
        [TestMethod]
        public async Task TransferredConfirmation_Post_Returns_Invalid_ModelState_Invalid_OrgId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferredConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferConfirmation", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests TransferredConfirmation returns invalid model state with invalid RecipientOrgId
        /// </summary>
        [TestMethod]
        public async Task TransferredConfirmation_Post_Returns_Invalid_ModelState_Invalid_RecipientOrgId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferredConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferConfirmation", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests TransferredConfirmation returns invalid model state with invalid obligation period id 
        /// </summary>
        [TestMethod]
        public async Task TransferredConfirmation_Post_Returns_Invalid_ModelState_Invalid_ObligationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
           .Returns(Task.FromResult<ObligationPeriod>(null));

            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferredConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferConfirmation", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests TransferConfirmation returns invalid model state when a TrasactionThrottlingException is caught
        /// </summary>
        [TestMethod]
        public async Task TransferredConfirmation_Post_Returns_Invalid_ModelState_On_Transaction_Throttling()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
               , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
           .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            LedgerService.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));
            LedgerService.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Throws(new TransactionThrottlingException());
            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferredConfirmation(viewModel);

            //assert
            _mockLogger.Verify(x => x.Log(LogLevel.Error, It.IsAny<EventId>(), It.Is<It.IsAnyType>((a, b) => true), It.IsAny<Exception>(), It.Is<Func<object, Exception, string>>((a,b) => true)), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferConfirmation", ((ViewResult)result).ViewName);
            var view = (ViewResult)result;
            var model = (TransferCreditsViewModel)view.Model;
            Assert.IsNotNull(model.ObligationPeriod);
        }

        /// <summary>
        /// Tests the TransferredConfirmation returns a error
        /// </summary>
        [TestMethod]
        public async Task TransferredConfirmation_Post_Returns_Error()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
               , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            LedgerService.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromException<int>(new Exception()));
            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferredConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(Controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferConfirmation", ((ViewResult)result).ViewName);
        }
        /// <summary>
        /// Tests the TransferredConfirmation returns a Forbid when invalid supplier is passed
        /// </summary>
        [TestMethod]
        public async Task TransferredConfirmation_Post_Returns_Forbid()
        {
            //arrange
            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = 12,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferredConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the TransferredConfirmation return a confirmation view
        /// </summary>
        [TestMethod]
        public async Task TransferredConfirmation_Post_Returns_Result()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(22, It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
               , It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
           .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            LedgerService.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));
            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = _userOrganisationId,
                ObligationPeriodId = 13,
                ToSupplierId = 22,
                NumberOfCreditsToTransfer = 99
            };

            //act
            var result = await Controller.TransferredConfirmation(viewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("TransferredConfirmation", ((ViewResult)result).ViewName);
        }
        #endregion TransferredConfirmation
    }
}
