﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.API.Client.Models;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Services;
using DfT.GOS.SystemParameters.API.Client.Models;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.Web.Models;
using DfT.GOS.Website.Configuration;
using DfT.GOS.Website.Models.ApplicationsViewModels;
using GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the applications controller
    /// </summary>
    [TestClass]
    public class ApplicationsControllerTest
    {
        #region Constants

        private const int FakeOrganisationId = 101;
        private const int SomeOtherOrganisationId = 50;
        private const int FakeObligationPeriodId = 1;
        private const int ApplicationIdNotFound = 2;
        private const int SomeOrganisationIdThatDoesNotExist = 1000;

        #endregion Constants

        #region Private Members

        private Mock<IGhgFuelsService> _mockGhgFuelsService;
        private Mock<ILogger<ApplicationsController>> _mockLogger;
        private Mock<IApplicationsService> _mockApplicationsService;
        private Mock<IReviewApplicationsService> _mockReviewApplicationsService;
        private Mock<IOrganisationsService> _mockOrganisationsService;
        private Mock<IReportingPeriodsService> _mockReportingPeriodsService;
        private Mock<ITokenService> _mockTokenService;
        private Mock<ISystemParametersService> _mockSystemParametersService;
        private Mock<ICalculationsService> _mockCalculationsService;
        private Mock<IUsersService> _mockUsersService;
        private Mock<IOptions<AppSettings>> _mockAppSettings;
        private ApplicationsController _controller;
        const int _userOrganisationId = 1000;

        #endregion

        #region Test Initialize

        [TestInitialize]
        public void Initilize()
        {
            _mockGhgFuelsService = new Mock<IGhgFuelsService>();
            _mockLogger = new Mock<ILogger<ApplicationsController>>();
            _mockApplicationsService = new Mock<IApplicationsService>();
            _mockReviewApplicationsService = new Mock<IReviewApplicationsService>();
            _mockOrganisationsService = new Mock<IOrganisationsService>();
            _mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            _mockTokenService = new Mock<ITokenService>();
            _mockSystemParametersService = new Mock<ISystemParametersService>();
            _mockCalculationsService = new Mock<ICalculationsService>();
            _mockAppSettings = new Mock<IOptions<AppSettings>>();
            _mockUsersService = new Mock<IUsersService>();
            var appSettings = new AppSettings() { Pagination = new PaginationConfigurationSettings { MaximumPagesToDisplay = 10, ResultsPerPage = 5 } };
            var options = Options.Create(appSettings);

            _controller = new ApplicationsController(_mockApplicationsService.Object, _mockGhgFuelsService.Object
                , _mockOrganisationsService.Object, _mockReportingPeriodsService.Object
                , _mockSystemParametersService.Object, _mockCalculationsService.Object,_mockUsersService.Object, _mockLogger.Object
                , _mockTokenService.Object, options
                , _mockReviewApplicationsService.Object);

            // - user principal
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", Guid.NewGuid().ToString()));
            claims.Add(new Claim("OrganisationId", _userOrganisationId.ToString()));

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal

                }

            };
           
            var fakeFossilGasFuelTypes = new List<FuelType>();
            var fakeFossilGasFuelType = new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null);
            fakeFossilGasFuelTypes.Add(fakeFossilGasFuelType);
            _mockGhgFuelsService.Setup(x => x.GetFossilGasFuelTypes())
                .Returns(Task.FromResult<IList<FuelType>>(fakeFossilGasFuelTypes));

            var fakeElectricityFuelType = new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null);
            _mockGhgFuelsService.Setup(s => s.GetElectricityFuelType()).Returns(Task.FromResult(fakeElectricityFuelType));

            var fakeUerFuelType = new FuelType(2, 2, "Fake UER", "Fake UER", null, null);
            _mockGhgFuelsService.Setup(s => s.GetUerFuelType()).Returns(Task.FromResult(fakeUerFuelType));

            var allFuelTypes = new List<FuelType>();
            allFuelTypes.Add(fakeFossilGasFuelType);
            allFuelTypes.Add(fakeElectricityFuelType);
            allFuelTypes.Add(fakeUerFuelType);
            _mockGhgFuelsService.Setup(s => s.GetFuelTypes()).Returns(Task.FromResult<IList<FuelType>>(allFuelTypes));
        }

        #endregion Initialize

        #region Tests

        #region New Tests

        /// <summary>
        /// Tests that the New (Get) returns a 404 if an organisation does not exist for that application
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_New_Get_Returns_404_If_OrganisationId_does_not_exist()
        {
            //Arrange
            var controller = this.GetControllerToTest(SomeOrganisationIdThatDoesNotExist);
            //Act
            var actionResult = await controller.New(SomeOrganisationIdThatDoesNotExist, FakeObligationPeriodId);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the New (Get) returns a 404 if an organisation does not exist for that application
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_New_Get_Returns_404_If_ObligationPeriodId_does_not_exist()
        {
            //Arrange  
            var someObligationPeriodIdThatDoesNotExist = 30;
            var controller = this.GetControllerToTest(FakeOrganisationId);
            //Act
            var actionResult = await controller.New(FakeOrganisationId, someObligationPeriodIdThatDoesNotExist);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the New (Get) method returns a 403 if a non-admin user isn't assciated with the application's organisation
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_New_Get_Returns_403_If_Supplier_User_Not_Linked_To_Supplier()
        {
            //Arrange
            var controller = this.GetControllerToTest(SomeOtherOrganisationId);

            //Act
            var actionResult = await controller.New(FakeOrganisationId, FakeObligationPeriodId);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the correct view is returned on success get of New Application page
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_New_Get_Returns_Valid_View()
        {
            //Arrange
            var expectedViewName = "Details";
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(
                1000, "Fake Organisation", 
                1, "Fake Status", 
                2, "type"))));
            _mockGhgFuelsService.Setup(x => x.GetFossilGasFuelTypes())
                .Returns(Task.FromResult<IList<FuelType>>(new List<FuelType> {
                    new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null)
                }));
            _mockGhgFuelsService.Setup(x => x.GetElectricityFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null)));
            _mockGhgFuelsService.Setup(x => x.GetUerFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(2, 2, "Fake UER", "Fake UER", null, null)));
            //Act
            var actionResult = await _controller.New(_userOrganisationId, FakeObligationPeriodId);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, ((ViewResult)actionResult).ViewName);
        }

        /// <summary>
        /// Tests that the post method returns forbidden if the user doesn't have permissions for the organisation
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Post_Returns_403_If_User_Cannot_Access_Organisation()
        {
            //Arrange
            var controller = this.GetControllerToTest(SomeOtherOrganisationId);

            //Act
            var actionResult = await controller.New(FakeOrganisationId, FakeObligationPeriodId, new ApplicationDetailsViewModel());

            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the details view is returned if there are model state validation errors on the New Post method
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_New_Post_Returns_Details_View_On_ModelState_Validation_Errors()
        {
            //Arrange
            var expectedViewName = "Details";
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(FakeObligationPeriodId
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(
                1000, "Fake Organisation",
                1, "Fake Status",
                2, "type"))));
            _mockGhgFuelsService.Setup(x => x.GetFossilGasFuelTypes())
                .Returns(Task.FromResult<IList<FuelType>>(new List<FuelType> {
                    new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null)
                }));
            _mockGhgFuelsService.Setup(x => x.GetElectricityFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null)));
            _controller.ModelState.AddModelError("FakeKey", "Fake Validation Error");
            _mockGhgFuelsService.Setup(x => x.GetUerFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(1, 1, "Fake UER", "Fake UER", 81, null)));

            //Act
            var actionResult = await _controller.New(_userOrganisationId, FakeObligationPeriodId, new ApplicationDetailsViewModel());
            var viewModel = ((ViewResult)actionResult).Model as ApplicationDetailsViewModel;

            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, ((ViewResult)actionResult).ViewName);

            // - The model was rehydrated
            Assert.IsNotNull(viewModel);
            Assert.AreEqual(FakeObligationPeriodId, viewModel.ObligationPeriod.Id);
            Assert.IsTrue(viewModel.FossilGasFuelTypes.Count > 0);
        }

        /// <summary>
        /// Tests that the new POST method redirects to the upload documents page on success
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_New_Post_Returns_Upload_Documents_Page_On_Success()
        {
            //Arrange
            var expectedRedirectUrl = "/applications/1/SupportingDocuments";
            var controller = this.GetControllerToTest(FakeOrganisationId);

            var fakeViewModel = new ApplicationDetailsViewModel();
            fakeViewModel.FuelCategory = ApplicationDetailsViewModel.FuelCategories.Electricity;
            fakeViewModel.ElectricityAmountSupplied = 500;
            fakeViewModel.ElectricityFuelTypeId = 1;
            fakeViewModel.ElectricityGhgIntensity = 80;

            //Act
            var actionResult = await controller.New(FakeOrganisationId, FakeObligationPeriodId, fakeViewModel);

            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(RedirectResult));
            Assert.AreEqual(expectedRedirectUrl, ((RedirectResult)actionResult).Url);            
        }

        #endregion New Tests

        #region Submit Tests

        /// <summary>
        /// Tests that the Submit method returns a 403 error if try to login as invalid organization
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Get_Application_Items_Returns_403_If_User_Cannot_Access_Organisation()
        {
            //arrange
            var controller = this.GetControllerToTest(SomeOtherOrganisationId);

            //Act
            var actionResult = await controller.Submit(1);

            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Submit method returns a 404 error if try to fetach incorrect application id
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Submit_Returns_404_If_Application_Not_Found()
        {
            //arrange
            _mockApplicationsService.Setup(s => s.GetApplicationSummary(ApplicationIdNotFound, It.IsAny<string>()))
                                                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(HttpStatusCode.NotFound)));

            //Act
            var actionResult = await _controller.Submit(ApplicationIdNotFound);

            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the Submit method returns the expected view if a valid ApplicationId is supplied
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Submit_Returns_View()
        {
            //arrange
            var appItems = new List<ApplicationItemSummary>()
            {
                new ApplicationItemSummary(1,2,3,null,200,21,31),
                new ApplicationItemSummary(1,3,1,null,25,21,null)
            };
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(1, 1000, 3))));
            _mockApplicationsService.Setup(x => x.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpObjectResponse<IList<ApplicationItemSummary>>>(new HttpObjectResponse<IList<ApplicationItemSummary>>(appItems)));
            _mockOrganisationsService.Setup(c => c.GetOrganisation(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(1000, "Fake Organisation", 1, "Fake Status", 2, "type"))));

            this.SetupObligationPeriod();

            // - Calculation result
            var volumeCalculationItemResults = new List<VolumeCalculationItemResult>();
            volumeCalculationItemResults.Add(new VolumeCalculationItemResult(2.ToString(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>(), null, null, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()));
            var volumeCalculation = new VolumeCalculationResult(volumeCalculationItemResults, It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());
            
            var energyCalculationItemResults = new List<EnergyCalculationItemResult>();
            energyCalculationItemResults.Add(new EnergyCalculationItemResult(3.ToString(), It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()));
            var energyCalculation = new EnergyCalculationResult(energyCalculationItemResults, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());

            var emissionsReductionCalculationItemResults = new List<EmissionsReductionCalculationItemResult>();
            var emissionsReductionsCalculation = new EmissionsReductionCalculationResult(emissionsReductionCalculationItemResults, It.IsAny<decimal>(), It.IsAny<decimal>());

            var fakeCalculationResult = new CalculationResult(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>(), volumeCalculation, energyCalculation, emissionsReductionsCalculation, 200, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());
            _mockCalculationsService.Setup(s => s.GhgCalculationBreakdown(It.IsAny<CalculationCommand>())).Returns(Task.FromResult(fakeCalculationResult));

            var systemParameters = new SystemParameter[]
{
                new SystemParameter(3, "RtfoSupportTelephoneNumber", "12345"),
                new SystemParameter(4, "RtfoSupportEmailAddress", "support@rtfo.com")
};
            _mockSystemParametersService.Setup(mock => mock.GetSystemParameters())
                .ReturnsAsync(new GosSystemParameters(systemParameters));

            //Act
            var actionResult = await _controller.Submit(2);

            //Assert            
            Assert.IsNotNull(((ViewResult)actionResult).Model);
            Assert.IsInstanceOfType(((ViewResult)actionResult).Model, typeof(SubmitApplicationViewModel));
            var model = (SubmitApplicationViewModel) ((ViewResult)actionResult).Model;
            Assert.AreEqual(2, model.ApplicationItems.Count);
        }

        #endregion Submit Tests

        #region SubmitApplication Tests

        /// <summary>
        /// Tests that the SubmitApplication method returns the expected view when a valid ApplicationId is supplied
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_SubmitApplication_Returns_Valid_View_Model()
        {
            //arrange
            var viewApplicationModel = new SubmitApplicationViewModel(){ ApplicationId = 1 };
            var command = new SubmitApplicationCommand(viewApplicationModel.ApplicationId);
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(1, 1000, 3) { ApplicationStatusId = 1})));
            _mockApplicationsService.Setup(x => x.SubmitApplication(It.IsAny<SubmitApplicationCommand>(), It.IsAny<string>()))
                .Returns(Task.FromResult(""));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, true, DateTime.Now.AddDays(1), It.IsAny<int>(), It.IsAny<int>(), null, null, null, null, null, null)));
            var expectedRedirectUrl= "/applications/" + command.ApplicationId.ToString() + "/submitted";
            
            //Act
            var actionResult = await _controller.SubmitApplication(viewApplicationModel);

            //Assert
            Assert.IsNotNull(((RedirectResult)actionResult));
            Assert.AreEqual(expectedRedirectUrl, ((RedirectResult)actionResult).Url);
        }

        /// <summary>
        /// Tests that the SubmitApplication method returns the expected view when the application is late
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_SubmitApplication_Returns_Valid_View_Model_When_Late_Application()
        {
            //arrange
            var viewApplicationModel = new SubmitApplicationViewModel() { ApplicationId = 1, IsLateApplication = false };
            var command = new SubmitApplicationCommand(viewApplicationModel.ApplicationId);
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(1, 1000, 3) { ApplicationStatusId = 1 })));
            _mockApplicationsService.Setup(x => x.SubmitApplication(It.IsAny<SubmitApplicationCommand>(), It.IsAny<string>()))
                .Returns(Task.FromResult(""));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(1, null, null, true, DateTime.Now.AddDays(-1), It.IsAny<int>(), It.IsAny<int>(), null, null, null, null, null, null)));

            var applicationItems = new List<ApplicationItemSummary>();
            applicationItems.Add(new ApplicationItemSummary(1, 1, 1, null, 10, 10, 10));

            _mockApplicationsService.Setup(x => x.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<ApplicationItemSummary>>(applicationItems)));

            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(1, "Fake Organisation", 1, "Fake Status", 1, "Fake Type"))));

            var systemParameters = new SystemParameter[]
            {
                new SystemParameter(3, "RtfoSupportTelephoneNumber", "12345"),
                new SystemParameter(4, "RtfoSupportEmailAddress", "support@rtfo.com")
            };
            _mockSystemParametersService.Setup(mock => mock.GetSystemParameters())
                .ReturnsAsync(new GosSystemParameters(systemParameters));


            // - Calculation result
            var volumeCalculationItemResults = new List<VolumeCalculationItemResult>();
            volumeCalculationItemResults.Add(new VolumeCalculationItemResult(1.ToString(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>(), null, null, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()));
            var volumeCalculation = new VolumeCalculationResult(volumeCalculationItemResults, It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());

            var energyCalculationItemResults = new List<EnergyCalculationItemResult>();
            var energyCalculation = new EnergyCalculationResult(energyCalculationItemResults, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());
            var emissionsReductionCalculationItemResults = new List<EmissionsReductionCalculationItemResult>();
            var emissionsReductionsCalculation = new EmissionsReductionCalculationResult(emissionsReductionCalculationItemResults, It.IsAny<decimal>(), It.IsAny<decimal>());

            var fakeCalculationResult = new CalculationResult(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>(), volumeCalculation, energyCalculation, emissionsReductionsCalculation, 200, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());
            _mockCalculationsService.Setup(s => s.GhgCalculationBreakdown(It.IsAny<CalculationCommand>())).Returns(Task.FromResult(fakeCalculationResult));

            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , DateTime.Now.AddDays(-1)
                , 5000
                , 10000
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , 98
                , 10000
                , It.IsAny<decimal>())));

            //Act
            var actionResult = await _controller.SubmitApplication(viewApplicationModel);

            //Assert
            Assert.IsNotNull(((ViewResult)actionResult).Model);
            Assert.IsInstanceOfType(((ViewResult)actionResult).Model, typeof(SubmitApplicationViewModel));

            Assert.AreEqual(true, _controller.ModelState.IsValid);
        }

        /// <summary>
        /// Tests validation on the Submit Application View
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_SubmitApplication_Returns_InValid_View_Model()
        {
            //arrange
            var viewApplicationModel = new SubmitApplicationViewModel() { ApplicationId = 1 };
            var command = new SubmitApplicationCommand(viewApplicationModel.ApplicationId);
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(1, 1000, 3) { ApplicationStatusId = 1 })));

            var applicationItems = new List<ApplicationItemSummary>();
            applicationItems.Add(new ApplicationItemSummary(1, 1, 1, null, 10, 10, 10));

            _mockApplicationsService.Setup(x => x.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<ApplicationItemSummary>>(applicationItems)));

            _mockApplicationsService.Setup(x => x.SubmitApplication(It.IsAny<SubmitApplicationCommand>(), It.IsAny<string>()))
                .Returns(Task.FromResult("Error occured"));

            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(1, "Fake Organisation", 1, "Fake Status", 1, "Fake Type"))));

            var systemParameters = new SystemParameter[]
            {
                new SystemParameter(3, "RtfoSupportTelephoneNumber", "12345"),
                new SystemParameter(4, "RtfoSupportEmailAddress", "support@rtfo.com")
            };
            _mockSystemParametersService.Setup(mock => mock.GetSystemParameters())
                .ReturnsAsync(new GosSystemParameters(systemParameters));

            this.SetupObligationPeriod();

            // - Calculation result
            var volumeCalculationItemResults = new List<VolumeCalculationItemResult>();
            volumeCalculationItemResults.Add(new VolumeCalculationItemResult(1.ToString(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>(), null, null, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()));
            var volumeCalculation = new VolumeCalculationResult(volumeCalculationItemResults, It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());

            var energyCalculationItemResults = new List<EnergyCalculationItemResult>();
            var energyCalculation = new EnergyCalculationResult(energyCalculationItemResults, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());
            var emissionsReductionCalculationItemResults = new List<EmissionsReductionCalculationItemResult>();
            var emissionsReductionsCalculation = new EmissionsReductionCalculationResult(emissionsReductionCalculationItemResults,It.IsAny<decimal>(), It.IsAny<decimal>());

            var fakeCalculationResult = new CalculationResult(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>(), volumeCalculation, energyCalculation, emissionsReductionsCalculation, 200, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());
            _mockCalculationsService.Setup(s => s.GhgCalculationBreakdown(It.IsAny<CalculationCommand>())).Returns(Task.FromResult(fakeCalculationResult));

            //Act
            var actionResult = await _controller.SubmitApplication(viewApplicationModel);

            //Assert
            Assert.IsNotNull(((ViewResult)actionResult).Model);
            Assert.IsInstanceOfType(((ViewResult)actionResult).Model, typeof(SubmitApplicationViewModel));
           
            Assert.AreEqual(false, _controller.ModelState.IsValid);
        }

        #endregion Submit Application Tests

        #region SupportingDocuments Get Tests
        /// <summary>
        /// SupportingDocuments get action returns NotFound result with invalid application
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsController_SupportingDocuments_Get_Returns_NotFound()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(HttpStatusCode.NotFound)));
            //act
            var result = await _controller.SupportingDocuments(1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// SupportingDocuments get action returns Forbid result with invalid organisation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsController_SupportingDocuments_Get_Returns_Forbid()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , 122, It.IsAny<int>()))));
            //act
            var result = await _controller.SupportingDocuments(1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }
        /// <summary>
        /// SupportingDocuments get action returns Model with errors when invalid application status
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsController_SupportingDocuments_Get_Returns_Invalid_Model()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , _userOrganisationId, It.IsAny<int>())
                { ApplicationStatusId = 3})));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(_userOrganisationId, "test org", 1, "New", 1, "Supplier")));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                   new List<UploadedSupportingDocument>())));
            this._mockSystemParametersService.Setup(x => x.GetDocumentTypes())
                .Returns(Task.FromResult(new HttpObjectResponse<IList<DocumentType>>(new List<DocumentType>()
                { new DocumentType(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())})));
            //act
            var result = await _controller.SupportingDocuments(1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.AreEqual("SupportingDocumentError", ((ViewResult)result).ViewName);
        }
        /// <summary>
        /// SupportingDocuments get action returns ViewResult
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsController_SupportingDocuments_Get_Returns_ViewResult_Model()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , _userOrganisationId, It.IsAny<int>())
                { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(_userOrganisationId, "test org", 1, "New", 1, "Supplier")));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                   new List<UploadedSupportingDocument>())));
            this._mockSystemParametersService.Setup(x => x.GetDocumentTypes())
                .Returns(Task.FromResult(new HttpObjectResponse<IList<DocumentType>>(new List<DocumentType>()
                { new DocumentType(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())})));
            //act
            var result = await _controller.SupportingDocuments(1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsTrue(_controller.ModelState.IsValid);
            Assert.AreEqual("SupportingDocuments", ((ViewResult)result).ViewName);
        }
        #endregion SupportingDocuments Get Tests

        #region SupportingDocuments Post Tests
        /// <summary>
        /// SupportingDocuments Post action returns notfound result with invalid application
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsController_SupportingDocuments_Post_Returns_NotFound_Invalid_Application()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(HttpStatusCode.NotFound)));

            //act
            var result = await this._controller.SupportingDocuments(It.IsAny<List<IFormFile>>(), It.IsAny<int>());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result,typeof(NotFoundResult));
        }
        /// <summary>
        /// SupportingDocuments Post action returns notfound result with invalid organisation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsController_SupportingDocuments_Post_Returns_NotFound_Invalid_Organisation()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , 122, It.IsAny<int>())
                { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(122, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound));
           
            //act
            var result = await this._controller.SupportingDocuments(It.IsAny<List<IFormFile>>(), It.IsAny<int>());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// SupportingDocuments Post action returns Forbid result with invalid organisation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsController_SupportingDocuments_Post_Returns_Forbid()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , 122, It.IsAny<int>())
                { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(122, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            //act
            var result = await this._controller.SupportingDocuments(It.IsAny<List<IFormFile>>(), It.IsAny<int>());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }
        /// <summary>
        /// SupportingDocuments Post action returns ViewResult with invalid application status
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SupportingDocuments_Post_Returns_Model_Error_With_Invalid_Application_Status()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , _userOrganisationId, It.IsAny<int>())
                { ApplicationStatusId = 3 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(_userOrganisationId, "test org", 1, "New", 1, "Supplier")));
            //act
            var result = await this._controller.SupportingDocuments(It.IsAny<List<IFormFile>>(), It.IsAny<int>());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.AreEqual("SupportingDocumentError", ((ViewResult)result).ViewName);
        }
        /// <summary>
        /// SupportingDocuments Post action returns ViewResult with model error when no files to upload
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SupportingDocuments_Post_Returns_Model_Error_With_Null_File()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
               , _userOrganisationId, It.IsAny<int>())
               { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(_userOrganisationId, "test org", 1, "New", 1, "Supplier")));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
               .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
               , DateTime.Now.AddDays(-1)
               , DateTime.Now.AddDays(1)
               , It.IsAny<bool>()
               , It.IsAny<DateTime>()
               , It.IsAny<int>()
               , It.IsAny<int>()
               , It.IsAny<DateTime>()
               , It.IsAny<DateTime>()
               , It.IsAny<DateTime>()
               , It.IsAny<decimal>()
               , It.IsAny<int>()
               , It.IsAny<decimal>())));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                   new List<UploadedSupportingDocument>())));
            this._mockSystemParametersService.Setup(x => x.GetDocumentTypes())
                .Returns(Task.FromResult(new HttpObjectResponse<IList<DocumentType>>(new List<DocumentType>()
                { new DocumentType(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())})));
            //act
            var result = await this._controller.SupportingDocuments(null, It.IsAny<int>());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.AreEqual("SupportingDocuments", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// SupportingDocuments Post action returns ViewResult when no files to upload
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SupportingDocuments_Post_Returns_ViewResult_With_Empty_FileList()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
               , _userOrganisationId, It.IsAny<int>())
               { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(_userOrganisationId, "test org", 1, "New", 1, "Supplier")));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
               .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
               , DateTime.Now.AddDays(-1)
               , DateTime.Now.AddDays(1)
               , It.IsAny<bool>()
               , It.IsAny<DateTime>()
               , It.IsAny<int>()
               , It.IsAny<int>()
               , It.IsAny<DateTime>()
               , It.IsAny<DateTime>()
               , It.IsAny<DateTime>()
               , It.IsAny<decimal>()
               , It.IsAny<int>()
               , It.IsAny<decimal>())));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                   new List<UploadedSupportingDocument>())));
            this._mockSystemParametersService.Setup(x => x.GetDocumentTypes())
                .Returns(Task.FromResult(new HttpObjectResponse<IList<DocumentType>>(new List<DocumentType>()
                { new DocumentType(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())})));
            //act
            var result = await this._controller.SupportingDocuments(new List<IFormFile>(), It.IsAny<int>());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.IsTrue(_controller.ModelState.IsValid);
            
        }
        /// <summary>
        /// SupportingDocuments Post action returns ViewResult with model error when files fail validation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SupportingDocuments_Post_Returns_Model_Error_With_File_Validation_Error()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
               , _userOrganisationId, It.IsAny<int>())
               { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(_userOrganisationId, "test org", 1, "New", 1, "Supplier")));
            this._mockApplicationsService.Setup(x => x.ValidateFileMetadata(It.IsAny<FileMetadata>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<string>() { "Error" }));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
              .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
              , DateTime.Now.AddDays(-1)
              , DateTime.Now.AddDays(1)
              , It.IsAny<bool>()
              , It.IsAny<DateTime>()
              , It.IsAny<int>()
              , It.IsAny<int>()
              , It.IsAny<DateTime>()
              , It.IsAny<DateTime>()
              , It.IsAny<DateTime>()
              , It.IsAny<decimal>()
              , It.IsAny<int>()
              , It.IsAny<decimal>())));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                   new List<UploadedSupportingDocument>())));
            this._mockSystemParametersService.Setup(x => x.GetDocumentTypes())
                .Returns(Task.FromResult(new HttpObjectResponse<IList<DocumentType>>(new List<DocumentType>()
                { new DocumentType(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())})));
            var fileMock = new Mock<IFormFile>();
            fileMock.Setup(_ => _.FileName).Returns("File2.doc");
            fileMock.Setup(_ => _.Length).Returns(100);
            //act
            var result = await this._controller.SupportingDocuments(new List<IFormFile>() { fileMock.Object }, It.IsAny<int>());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.AreEqual("SupportingDocuments", ((ViewResult)result).ViewName);
        }
        /// <summary>
        /// SupportingDocuments Post action returns ViewResult with model error when upload fails
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SupportingDocuments_Post_Returns_Model_Error_When_Upload_Fails()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
               , _userOrganisationId, It.IsAny<int>())
               { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(_userOrganisationId, "test org", 1, "New", 1, "Supplier")));
            this._mockApplicationsService.Setup(x => x.ValidateFileMetadata(It.IsAny<FileMetadata>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<string>()));
            this._mockApplicationsService.Setup(x => x.UploadApplicationSupportingDocument(It.IsAny<SupportingDocument>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new SupportingDocumentUpload() { ScanResult = new ScanResult() { IsVirusFree = false, Message = "Error in the file"} }));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
              .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
              , DateTime.Now.AddDays(-1)
              , DateTime.Now.AddDays(1)
              , It.IsAny<bool>()
              , It.IsAny<DateTime>()
              , It.IsAny<int>()
              , It.IsAny<int>()
              , It.IsAny<DateTime>()
              , It.IsAny<DateTime>()
              , It.IsAny<DateTime>()
              , It.IsAny<decimal>()
              , It.IsAny<int>()
              , It.IsAny<decimal>())));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                   new List<UploadedSupportingDocument>())));
            this._mockSystemParametersService.Setup(x => x.GetDocumentTypes())
                .Returns(Task.FromResult(new HttpObjectResponse<IList<DocumentType>>(new List<DocumentType>()
                { new DocumentType(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())})));
            var fileMock = new Mock<IFormFile>();
            fileMock.Setup(_ => _.FileName).Returns("test.pdf");
            fileMock.Setup(_ => _.Length).Returns(100);
            fileMock.Setup(_ => _.ContentDisposition).Returns("ContentDisposition");
            fileMock.Setup(_ => _.ContentType).Returns("multipart/form-data");
            var content = "Hello World from a Fake File";
            
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
           
            //act 
            var result = await this._controller.SupportingDocuments(new List<IFormFile>() { fileMock.Object }, It.IsAny<int>());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.AreEqual("SupportingDocuments", ((ViewResult)result).ViewName);
        }
        /// <summary>
        /// SupportingDocuments Post action returns ViewResult with model error when Upload throws exception
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SupportingDocuments_Post_Returns_Throws_Exception()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
               , _userOrganisationId, It.IsAny<int>())
               { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            this._mockApplicationsService.Setup(x => x.ValidateFileMetadata(It.IsAny<FileMetadata>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<string>()));
            this._mockApplicationsService.Setup(x => x.UploadApplicationSupportingDocument(It.IsAny<SupportingDocument>(), It.IsAny<string>()))
               .Returns(Task.FromException<SupportingDocumentUpload>(new Exception()));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
              .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
              , DateTime.Now.AddDays(-1)
              , DateTime.Now.AddDays(1)
              , It.IsAny<bool>()
              , It.IsAny<DateTime>()
              , It.IsAny<int>()
              , It.IsAny<int>()
              , It.IsAny<DateTime>()
              , It.IsAny<DateTime>()
              , It.IsAny<DateTime>()
              , It.IsAny<decimal>()
              , It.IsAny<int>()
              , It.IsAny<decimal>())));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                   new List<UploadedSupportingDocument>())));
            this._mockSystemParametersService.Setup(x => x.GetDocumentTypes())
                .Returns(Task.FromResult(new HttpObjectResponse<IList<DocumentType>>(new List<DocumentType>()
                { new DocumentType(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())})));
            var fileMock = new Mock<IFormFile>();
            fileMock.Setup(_ => _.FileName).Returns("test.pdf");
            fileMock.Setup(_ => _.Length).Returns(100);
            fileMock.Setup(_ => _.ContentDisposition).Returns("ContentDisposition");
            fileMock.Setup(_ => _.ContentType).Returns("multipart/form-data");
            var content = "Hello World from a Fake File";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            //act 
            var result = await this._controller.SupportingDocuments(new List<IFormFile> { fileMock.Object}, It.IsAny<int>());
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.AreEqual("SupportingDocuments", ((ViewResult)result).ViewName);
        }
        /// <summary>
        /// SupportingDocuments Post action returns ViewResult
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SupportingDocuments_Post_Returns_ViewResult_Result()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
              , _userOrganisationId, It.IsAny<int>())
              { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            this._mockApplicationsService.Setup(x => x.ValidateFileMetadata(It.IsAny<FileMetadata>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<string>()));
            this._mockApplicationsService.Setup(x => x.UploadApplicationSupportingDocument(It.IsAny<SupportingDocument>(), It.IsAny<string>()))
               .Returns(Task.FromResult<SupportingDocumentUpload>(new SupportingDocumentUpload()
               { ScanResult = new ScanResult { IsVirusFree = true } }));
            this._mockSystemParametersService.Setup(x => x.GetDocumentTypes())
                .Returns(Task.FromResult(new HttpObjectResponse<IList<DocumentType>>(new List<DocumentType>()
                { new DocumentType(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())})));
            _mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                    new List<UploadedSupportingDocument>())));
            var fileMock = new Mock<IFormFile>();
            fileMock.Setup(_ => _.FileName).Returns("File2.doc");
            fileMock.Setup(_ => _.Length).Returns(100);
            fileMock.Setup(_ => _.ContentDisposition).Returns("ContentDisposition");
            fileMock.Setup(_ => _.ContentType).Returns("multipart/form-data");
            var content = "Hello World from a Fake File";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);

            //act
            var result = await this._controller.SupportingDocuments(new List<IFormFile> { fileMock.Object }, It.IsAny<int>());

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsTrue(_controller.ModelState.IsValid);
        }
        /// <summary>
        /// SupportingDocuments Post action returns RedirectResult after uploading the documents
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SupportingDocuments_Post_Returns_RedirectToResult()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
              , _userOrganisationId, It.IsAny<int>())
              { ApplicationStatusId = 1 })));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            this._mockApplicationsService.Setup(x => x.ValidateFileMetadata(It.IsAny<FileMetadata>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<string>()));
            this._mockApplicationsService.Setup(x => x.UploadApplicationSupportingDocument(It.IsAny<SupportingDocument>(), It.IsAny<string>()))
               .Returns(Task.FromResult<SupportingDocumentUpload>(new SupportingDocumentUpload()
               { ScanResult = new ScanResult { IsVirusFree = true } }));

            //act
            var result = await this._controller.SupportingDocuments(new List<IFormFile>(), It.IsAny<int>());

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.IsTrue(_controller.ModelState.IsValid);
        }
        #endregion SupportingDocuments Post Tests

        #region RemoveSupportingDocument Get
        /// <summary>
        /// Tests RemoveSupportingDocument Get action returns NotFoundResult woth incorrect application id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RemoveSupportingDocument_Get_Returns_NotFound_With_Incorrect_Application()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(HttpStatusCode.NotFound)));
            
            //act
            var result = await _controller.RemoveSupportingDocument(1, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests RemoveSupportingDocument Get action returns Forbidresult with incorrect organisation id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RemoveSupportingDocument_Get_Returns_Forbid_With_Incorrect_Organisation()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
               , 122, It.IsAny<int>()))));
            //act
            var result = await _controller.RemoveSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }
        /// <summary>
        /// Tests RemoveSupportingDocument Get action returns ViewResult with model errors with incorrect obligation period id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RemoveSupportingDocument_Get_Returns_ModelWithError_With_Incorrect_ObligationPeriod()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
               , _userOrganisationId, It.IsAny<int>()))));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(null));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocument(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<DownloadSupportingDocument>(HttpStatusCode.OK)));
            //act
            var result = await _controller.RemoveSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
        /// <summary>
        /// Tests RemoveSupportingDocument Get action returns ViewResult with model errors with incorrect document
        /// </summary>
        [TestMethod]
        public async Task RemoveSupportingDocument_Get_Returns_ModelWithError_With_No_Document()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
              , _userOrganisationId, It.IsAny<int>()))));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            var fakeElectricityFuelType = new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null);
            _mockGhgFuelsService.Setup(s => s.GetElectricityFuelType()).Returns(Task.FromResult(fakeElectricityFuelType));
            _mockGhgFuelsService.Setup(x => x.GetUerFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(1, 1, "Fake UER", "Fake UER", null, null)));
            var fakeFossilGasFuelTypes = new List<FuelType>();
            fakeFossilGasFuelTypes.Add(new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null));
            _mockGhgFuelsService.Setup(s => s.GetFossilGasFuelTypes()).Returns(Task.FromResult<IList<FuelType>>(fakeFossilGasFuelTypes));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocument(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<DownloadSupportingDocument>(HttpStatusCode.NotFound)));
            //act
            var result = await _controller.RemoveSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Tests RemoveSupportingDocument Get action returns ViewResult with model errors with filename is empty
        /// </summary>
        [TestMethod]
        public async Task RemoveSupportingDocument_Get_Returns_ModelWithError_When_FileName_Is_Null()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
              , _userOrganisationId, It.IsAny<int>()))));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            var fakeElectricityFuelType = new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null);
            _mockGhgFuelsService.Setup(s => s.GetElectricityFuelType()).Returns(Task.FromResult(fakeElectricityFuelType));

            var fakeFossilGasFuelTypes = new List<FuelType>();
            fakeFossilGasFuelTypes.Add(new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null));
            _mockGhgFuelsService.Setup(s => s.GetFossilGasFuelTypes()).Returns(Task.FromResult<IList<FuelType>>(fakeFossilGasFuelTypes));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocument(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<DownloadSupportingDocument>(new DownloadSupportingDocument())));
            _mockGhgFuelsService.Setup(x => x.GetUerFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(1, 1, "Fake UER", "Fake UER", null, null)));

            //act
            var result = await _controller.RemoveSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
        /// <summary>
        /// Tests RemoveSupportingDocument Get action returns ViewResult with model errors with invalid application status
        /// </summary>
        [TestMethod]
        public async Task RemoveSupportingDocument_Get_Returns_ModelWithError_With_Invalid_Application_Status()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
              , _userOrganisationId, It.IsAny<int>())
              {  ApplicationStatusId = (int)ApplicationStatus.Submitted})));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            var fakeElectricityFuelType = new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null);
            _mockGhgFuelsService.Setup(s => s.GetElectricityFuelType()).Returns(Task.FromResult(fakeElectricityFuelType));

            var fakeFossilGasFuelTypes = new List<FuelType>();
            fakeFossilGasFuelTypes.Add(new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null));
            _mockGhgFuelsService.Setup(s => s.GetFossilGasFuelTypes()).Returns(Task.FromResult<IList<FuelType>>(fakeFossilGasFuelTypes));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocument(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<DownloadSupportingDocument>(new DownloadSupportingDocument() { FileName = "File1.doc" })));
            _mockGhgFuelsService.Setup(x => x.GetUerFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(1, 1, "Fake UER", "Fake UER", null, null)));
            //act
            var result = await _controller.RemoveSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
        /// <summary>
        /// Tests RemoveSupportingDocument Get action returns ViewResult
        /// </summary>
        [TestMethod]
        public async Task RemoveSupportingDocument_Get_Returns_ViewResult()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
              , _userOrganisationId, It.IsAny<int>())
              { ApplicationStatusId = (int)ApplicationStatus.Open })));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            var fakeElectricityFuelType = new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null);
            _mockGhgFuelsService.Setup(s => s.GetElectricityFuelType()).Returns(Task.FromResult(fakeElectricityFuelType));
            _mockGhgFuelsService.Setup(x => x.GetUerFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(1, 1, "Fake UER", "Fake UER", null, null)));
            var fakeFossilGasFuelTypes = new List<FuelType>();
            fakeFossilGasFuelTypes.Add(new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null));
            _mockGhgFuelsService.Setup(s => s.GetFossilGasFuelTypes()).Returns(Task.FromResult<IList<FuelType>>(fakeFossilGasFuelTypes));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocument(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<DownloadSupportingDocument>(new DownloadSupportingDocument() { FileName = "File1.doc" })));

            //act
            var result = await _controller.RemoveSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("SupportingDocumentRemove", ((ViewResult)result).ViewName);
        }
        #endregion RemoveSupportingDocument Get

        #region RemoveSupportingDocument Post
        /// <summary>
        /// Tests RemoveSupportingDocument post action returns NotFoundResult with invalid application id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RemoveSupportingDocument_Post_Returns_NotFound_With_Invalid_Application()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(HttpStatusCode.NotFound)));

            //act
            var result = await _controller.RemoveSupportingDocumentPost(new ApplicationSupportingDocumentsViewModel(), 1, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests RemoveSupportingDocument post action returns ForBidResult with invalid organisation id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RemoveSupportingDocument_Post_Returns_Forbid_With_Invalid_Organisation()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
               , 122, It.IsAny<int>()))));
            //act
            var result = await _controller.RemoveSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }
        /// <summary>
        /// Tests RemoveSupportingDocument post action returns RedirectResult with modelstate erros with invalid application
        /// status
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RemoveSupportingDocument_Post_Returns_RedirectWithError_With_Invalid_Application_Status()
        {
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
              , _userOrganisationId, It.IsAny<int>())
              { ApplicationStatusId = (int)ApplicationStatus.Submitted })));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            var fakeElectricityFuelType = new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null);
            _mockGhgFuelsService.Setup(s => s.GetElectricityFuelType()).Returns(Task.FromResult(fakeElectricityFuelType));
            _mockGhgFuelsService.Setup(x => x.GetUerFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(1, 1, "Fake UER", "Fake UER", null, null)));
            var fakeFossilGasFuelTypes = new List<FuelType>();
            fakeFossilGasFuelTypes.Add(new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null));
            _mockGhgFuelsService.Setup(s => s.GetFossilGasFuelTypes()).Returns(Task.FromResult<IList<FuelType>>(fakeFossilGasFuelTypes));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocument(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<DownloadSupportingDocument>(new DownloadSupportingDocument() { FileName = "File1.doc" })));

            //act
            var result = await _controller.RemoveSupportingDocumentPost(new ApplicationSupportingDocumentsViewModel(),1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(RedirectResult));
        }
        /// <summary>
        /// Tests RemoveSupportingDocument post action returns RedirectResult 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RemoveSupportingDocument_Post_Returns_RedirectResult()
        {
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
              , _userOrganisationId, It.IsAny<int>())
              { ApplicationStatusId = (int)ApplicationStatus.Open })));
            this._mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(_userOrganisationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(122, "test org", 1, "New", 1, "Supplier")));
            var fakeElectricityFuelType = new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null);
            _mockGhgFuelsService.Setup(s => s.GetElectricityFuelType()).Returns(Task.FromResult(fakeElectricityFuelType));
            _mockGhgFuelsService.Setup(x => x.GetUerFuelType())
                .Returns(Task.FromResult<FuelType>(new FuelType(1, 1, "Fake UER", "Fake UER", null, null)));
            var fakeFossilGasFuelTypes = new List<FuelType>();
            fakeFossilGasFuelTypes.Add(new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null));
            _mockGhgFuelsService.Setup(s => s.GetFossilGasFuelTypes()).Returns(Task.FromResult<IList<FuelType>>(fakeFossilGasFuelTypes));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocument(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<DownloadSupportingDocument>(new DownloadSupportingDocument() { FileName = "File1.doc" })));

            //act
            var result = await _controller.RemoveSupportingDocumentPost(new ApplicationSupportingDocumentsViewModel(),1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(_controller.ModelState.IsValid);
            Assert.IsInstanceOfType(result, typeof(RedirectResult));
        }
        #endregion RemoveSupportingDocument Post

        #region DownloadSupportingDocument Tests
        /// <summary>
        /// Tests the DownloadSupportingDocument action returns notfound result with invalid application id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DownloadSupportingDocument_Returns_NotFound_With_Invalid_Application()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(HttpStatusCode.NotFound)));
            //act
            var result = await _controller.DownloadSupportingDocument(1,1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        /// <summary>
        /// Tests the DownloadSupportingDocument action returns Forbid result with invalid organisation id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DownloadSupportingDocument_Returns_ForbidResult_With_Invalid_Organisation()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , 122, It.IsAny<int>()))));
            //act
            var result = await _controller.DownloadSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the DownloadSupportingDocument action not found result with invalid document id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DownloadSupportingDocument_Returns_NotFound_With_Invalid_DocumentId()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , _userOrganisationId, It.IsAny<int>())
                { ApplicationStatusId = 3 })));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocument(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<DownloadSupportingDocument>(HttpStatusCode.NotFound)));
            //act
            var result = await _controller.DownloadSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests the DownloadSupportingDocument action returns the filecontent result 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DownloadSupportingDocument_Returns_FileResult()
        {
            //arrange
            this._mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , _userOrganisationId, It.IsAny<int>())
                { ApplicationStatusId = 3 })));
            this._mockApplicationsService.Setup(x => x.GetSupportingDocument(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<DownloadSupportingDocument>(new DownloadSupportingDocument()
                {FileName = "Doc1.docx", File= new byte[] { } })));
            //act
            var result = await _controller.DownloadSupportingDocument(1, 1);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(FileContentResult));
        }
        #endregion DownloadSupportingDocument Tests

        #region Submitted Tests

        /// <summary>
        /// Tests that the Submitted method returns the expected view when a valid ApplicationId is supplied
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Submitted_Returns_View()
        {
            //arrange
            var applicationId = 2;
            var supplierOrganisationId = _userOrganisationId;
            var obligationPeriodId = 15;
            var volumeCalculationItemResults = new List<VolumeCalculationItemResult>();
            volumeCalculationItemResults.Add(new VolumeCalculationItemResult(1.ToString(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>(), null, null, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()));
            var volumeCalculation = new VolumeCalculationResult(volumeCalculationItemResults, It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());

            var energyCalculationItemResults = new List<EnergyCalculationItemResult>();
            energyCalculationItemResults.Add(new EnergyCalculationItemResult(2.ToString(), It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()));
            var energyCalculation = new EnergyCalculationResult(energyCalculationItemResults, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());

            var emissionsReductionCalculationItemResults = new List<EmissionsReductionCalculationItemResult>();
            var emissionsReductionsCalculation = new EmissionsReductionCalculationResult(emissionsReductionCalculationItemResults, It.IsAny<decimal>(), It.IsAny<decimal>());
            this._mockApplicationsService.Setup(mock => mock.GetApplicationSummary(applicationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(applicationId, supplierOrganisationId, obligationPeriodId)));
            this._mockApplicationsService.Setup(mock => mock.GetApplicationItems(applicationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IList<ApplicationItemSummary>>(new List<ApplicationItemSummary>() { new ApplicationItemSummary(applicationId, 1, 1, null, 100, 100, 100) }));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(supplierOrganisationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(supplierOrganisationId, "test org", 1, "New", 1, "Supplier")));
            this._mockCalculationsService.Setup(x => x.GhgCalculationBreakdown(It.IsAny<CalculationCommand>()))
                .Returns(Task.FromResult(new CalculationResult(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>()
                , volumeCalculation, energyCalculation, emissionsReductionsCalculation
                , 200, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>())));

            this.SetupObligationPeriod();

            var systemParameters = new SystemParameter[]
{
                new SystemParameter(3, "RtfoSupportTelephoneNumber", "12345"),
                new SystemParameter(4, "RtfoSupportEmailAddress", "support@rtfo.com")
};
            _mockSystemParametersService.Setup(mock => mock.GetSystemParameters())
                .ReturnsAsync(new GosSystemParameters(systemParameters));

            //act
            var result = await _controller.Submitted(applicationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsNotNull(((ViewResult)result).Model);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(SubmitApplicationViewModel));
        }

        /// <summary>
        /// Tests that the Submitted method returns HTTP 403 (Forbidden) when the user attempts to view an Application for the wrong Supplier
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Submitted_Returns_Forbidden()
        {
            //arrange
            var applicationId = 2;
            var supplierOrganisationId = 243;
            var obligationPeriodId = 15;
            this._mockApplicationsService.Setup(mock => mock.GetApplicationSummary(applicationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(applicationId, supplierOrganisationId, obligationPeriodId)));
            this._mockApplicationsService.Setup(mock => mock.GetApplicationItems(applicationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IList<ApplicationItemSummary>>(new List<ApplicationItemSummary>() { new ApplicationItemSummary(applicationId, 1, 1, null, 100, 100, 100) }));
            this._mockOrganisationsService.Setup(mock => mock.GetOrganisation(supplierOrganisationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(supplierOrganisationId, "test org", 1, "New", 1, "Supplier")));

            //act
            var result = await _controller.Submitted(applicationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Submitted method returns HTTP 404 (NotFound) when an invalid ApplicationId is supplied
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Submitted_Returns_NotFound_For_Invalid_ApplicationId()
        {
            //arrange
            var applicationId = 2;
            this._mockApplicationsService.Setup(mock => mock.GetApplicationSummary(applicationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<ApplicationSummary>(HttpStatusCode.NotFound));

            //act
            var result = await _controller.Submitted(applicationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        #endregion Submitted Tests

        #region SupplierApplications Tests

        /// <summary>
        /// Tests that the SupplierApplications method does not allow the user to view Applications for the wrong Supplier
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_SupplierApplications_Returns_Forbid()
        {
            //act
            var result = await _controller.SupplierApplications(10);

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the SupplierApplications method returns HTTP 404 (NotFound) if an invalid OrganisationId is supplied
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_SupplierApplications_Return_NotFound_Invalid_OrgId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //act
            var result = await _controller.SupplierApplications(_userOrganisationId);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the SupplierApplications method returns HTTP 404 (NotFound) if an invalid Obligation Period Id is supplied
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_SupplierApplications_Return_NotFound_Invalid_ObligationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()))));

            //act
            var result = await _controller.SupplierApplications(_userOrganisationId, 1313);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the SupplierApplications method returns Applications when a valid OrganisationId and ObligationPeriodId are supplied
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_SupplierApplications_Returns_Applications()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            _mockApplicationsService.Setup(x => x.GetSupplierApplications(It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<ApplicationSummary>>(
                    new PagedResult<ApplicationSummary>() { Result = new List<ApplicationSummary>() })));

            //act
            var result = await _controller.SupplierApplications(_userOrganisationId);

            //assert
            _mockReportingPeriodsService.Verify(x => x.GetCurrentObligationPeriod(), Times.Once);
            _mockApplicationsService.Verify(x => x.GetSupplierApplications(It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<string>()), Times.Once);
        }

        #endregion SupplierApplications Tests

        #region ApplicationDetails Tests

        /// <summary>
        /// Tests that the ApplicationDetails method returns HTTP 404 (NotFound) if an invalid ApplicationId is supplied
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_ApplicationDetails_Returns_NotFoundResult_For_Invalid_ApplicationId()
        {
            //arrange
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(HttpStatusCode.NotFound)));

            //act
            var result = await _controller.ApplicationDetails(22);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests the ApplicationDetails method returns HTTP 403 (Forbidden) if the user tries to view an Application from the wrong Supplier
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_ApplicationDetails_Returns_Forbid_Invalid_OrganisationId()
        {
            //arrange
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<int>()))));

            //act
            var result = await _controller.ApplicationDetails(22);

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests the ApplicationDetails method returns RedirectToRoute if the user tries to view an Application in Open Status
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_ApplicationDetails_Returns_RedirectToRoute_Invalid_Status()
        {
            //arrange
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                ,_userOrganisationId
                , It.IsAny<int>())
                { ApplicationStatusId = (int)ApplicationStatus.Open})));

            //act
            var result = await _controller.ApplicationDetails(22);

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
        }

        /// <summary>
        /// Tests the ApplicationDetails method returns RedirectToRoute if the user tries to view an Application in Open Status
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_ApplicationDetails_Verify_ReviewSummary_IsCalled_For_Rejected_Application()
        {
            //arrange
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , _userOrganisationId
                , It.IsAny<int>())
                { ApplicationStatusId = (int)ApplicationStatus.Rejected })));
            _mockReviewApplicationsService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationReviewSummary>(new ApplicationReviewSummary())));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(FakeObligationPeriodId
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            _mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                    new List<UploadedSupportingDocument>())));
            _mockApplicationsService.Setup(x => x.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<ApplicationItemSummary>>(
                    new List<ApplicationItemSummary>())));
            _mockCalculationsService.Setup(x => x.GhgCalculationBreakdown(It.IsAny<CalculationCommand>()))
                .Returns(Task.FromResult(new CalculationResult(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<decimal>(), It.IsAny<VolumeCalculationResult>()
                , It.IsAny<EnergyCalculationResult>(), It.IsAny<EmissionsReductionCalculationResult>()
                , It.IsAny<decimal>(), It.IsAny<decimal>()
                , It.IsAny<decimal>(), It.IsAny<decimal>())));

            //act
            var result = await _controller.ApplicationDetails(22);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            _mockReviewApplicationsService.Verify(x => x.GetApplicationReviewSummary(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Tests ApplicationDetails method returns a valid view result
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_ApplicationDetails_Returns_View_Result()
        {
            //arrange
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , _userOrganisationId
                , It.IsAny<int>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(FakeObligationPeriodId
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            _mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                    new List<UploadedSupportingDocument>())));
            _mockApplicationsService.Setup(x => x.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<ApplicationItemSummary>>(
                    new List<ApplicationItemSummary>())));
            _mockCalculationsService.Setup(x => x.GhgCalculationBreakdown(It.IsAny<CalculationCommand>()))
                .Returns(Task.FromResult(new CalculationResult(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<decimal>(), It.IsAny<VolumeCalculationResult>()
                , It.IsAny<EnergyCalculationResult>(), It.IsAny<EmissionsReductionCalculationResult>()
                , It.IsAny<decimal>(), It.IsAny<decimal>()
                , It.IsAny<decimal>(), It.IsAny<decimal>())));

            //act
            var result = await _controller.ApplicationDetails(22);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        #endregion ApplicationDetails Tests

        #region RevokeApplication Tests

        /// <summary>
        /// Tests Revoke method returns a valid view result
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Revoke_Returns_View_Result()
        {
            //arrange
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , _userOrganisationId
                , It.IsAny<int>()))));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(FakeObligationPeriodId
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>())));
            _mockApplicationsService.Setup(x => x.GetSupportingDocuments(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<List<UploadedSupportingDocument>>(
                    new List<UploadedSupportingDocument>())));
            _mockApplicationsService.Setup(x => x.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<ApplicationItemSummary>>(
                    new List<ApplicationItemSummary>())));
            _mockCalculationsService.Setup(x => x.GhgCalculationBreakdown(It.IsAny<CalculationCommand>()))
                .Returns(Task.FromResult(new CalculationResult(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<decimal>(), It.IsAny<VolumeCalculationResult>()
                , It.IsAny<EnergyCalculationResult>(), It.IsAny<EmissionsReductionCalculationResult>()
                , It.IsAny<decimal>(), It.IsAny<decimal>()
                , It.IsAny<decimal>(), It.IsAny<decimal>())));

            //act
            var result = await _controller.Revoke(22);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        /// <summary>
        /// Tests the Revoke method returns HTTP 403 (Forbidden) if the user tries to view as the wrong Supplier
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Revoke_Returns_Forbid_Invalid_OrganisationId()
        {
            //arrange
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<int>()))));

            //act
            var result = await _controller.Revoke(22);

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Revoke method returns HTTP 404 (NotFound) if an invalid ApplicationId is supplied
        /// </summary>
        [TestMethod]
        public async Task ApplicationsController_Revoke_Returns_NotFoundResult_For_Invalid_ApplicationId()
        {
            //arrange
            _mockApplicationsService.Setup(x => x.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(HttpStatusCode.NotFound)));

            //act
            var result = await _controller.Revoke(22);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        #endregion Revoke Tests

        #endregion Tests

        #region Private Methods

        /// <summary>
        /// Sets up an instance of ApplicationsController with dependancies for testing against.
        /// </summary>
        /// <param name="userOrganisationId">The Organisation ID of the 'current user'</param>
        /// <returns>Returns the configured Application Controller</returns>
        private ApplicationsController GetControllerToTest(int userOrganisationId)
        {            
            var mockGhgFuelsService = new Mock<IGhgFuelsService>();
            var mockLogger = new Mock<ILogger<ApplicationsController>>();

            //Mock the Application Service
            var mockApplicationsService = new Mock<IApplicationsService>();
            var fakeApplicationId = 1;
            mockApplicationsService.Setup(s => s.CreateNewApplication(It.IsAny<CreateApplicationCommand>(), It.IsAny<string>()))
                                                .Returns(Task.FromResult(fakeApplicationId));
            mockApplicationsService.Setup(s => s.GetApplicationSummary(fakeApplicationId, It.IsAny<string>()))
                                                .Returns(Task.FromResult(new HttpObjectResponse<ApplicationSummary>(new ApplicationSummary(fakeApplicationId, 1,1))));
        
            //Mock the organisation
            var fakeOrganisation = new Organisation(FakeOrganisationId, "Fake Supplier", 1, "status", 1, "supplier");            

            var mockOrganisationsService = new Mock<IOrganisationsService>();            
            mockOrganisationsService.Setup(s => s.GetOrganisation(FakeOrganisationId,""))
                                                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(fakeOrganisation)));
           
            //Mock an organisation that doesn't exist
            mockOrganisationsService.Setup(s => s.GetOrganisation(SomeOrganisationIdThatDoesNotExist, ""))
                                                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));

            //Setup the obligation period
            var fakeObligationPeriod = new ObligationPeriod(FakeObligationPeriodId
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>());

            var mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            mockReportingPeriodsService.Setup(s => s.GetObligationPeriod(FakeObligationPeriodId))
                                                .Returns(Task.FromResult(fakeObligationPeriod));

            //Mock the Calculations Service
            var mockCalculationsService = new Mock<ICalculationsService>();

            //Mock the Token service
            var mockTokenService = new Mock<ITokenService>();
            mockTokenService.Setup(t => t.GetJwtToken()).ReturnsAsync("MockJwtToken");

            var mockSystemParametersService = new Mock<ISystemParametersService>();

            //Setup the Fuel Type
            var fakeElectricityFuelType = new FuelType(1, 1, "Fake Electricity", "Fake Electricity", 81, null);
            mockGhgFuelsService.Setup(s => s.GetElectricityFuelType()).Returns(Task.FromResult(fakeElectricityFuelType));

            var fakeFossilGasFuelTypes = new List<FuelType>();
            fakeFossilGasFuelTypes.Add(new FuelType(3, 3, "Fake Fossil Gas Category", "Fake Fossil Gas 1", null, null));
            mockGhgFuelsService.Setup(s => s.GetFossilGasFuelTypes()).Returns(Task.FromResult<IList<FuelType>>(fakeFossilGasFuelTypes));

            //Mock the current user
            var fakeUser = new GosApplicationUser()
            {
                OrganisationId = userOrganisationId
            };
                        
            var controllerUnderTest = new ApplicationsController(mockApplicationsService.Object,
                mockGhgFuelsService.Object,
                mockOrganisationsService.Object,                
                mockReportingPeriodsService.Object,
                mockSystemParametersService.Object,
                mockCalculationsService.Object,
                null,
                mockLogger.Object,
                mockTokenService.Object,
                null, null);

            // - user principal
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", Guid.NewGuid().ToString()));
            claims.Add(new Claim("OrganisationId", userOrganisationId.ToString()));

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            controllerUnderTest.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal                    
                }
            };

            return controllerUnderTest;
        }

        /// <summary>
        /// Sets up a fake obligation period from the reporting periods service
        /// </summary>
        private void SetupObligationPeriod()
        {
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>()
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , DateTime.Now.AddDays(1)
                , 5000
                , 10000
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , 98
                , 10000
                , It.IsAny<decimal>())));
        }

        #endregion Private Methods
    }
}
