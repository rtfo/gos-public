﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.SystemParameters.API.Client.Models;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DfT.GOS.Security.Claims;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Orchestrators;
using DfT.GOS.Website.Models.SuppliersViewModels;
using Microsoft.Extensions.Logging;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.Http;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        #region Properties
        private Mock<ILogger<HomeController>> MockLogger { get; set; }

        #endregion Properties

        #region Home Page Tests

        /// <summary>
        /// Tests the Home controller Home method returns the default landing page if no role is specified. 
        /// This occurs in the scenario that a user hasn't had their account linked to a company/role
        /// </summary>
        [TestMethod]
        public async Task HomeController_Returns_Default_Landing_Page_With_No_Role()
        {
            //arrange
            var controller = this.GetControllerInstance(true, null, false, false);
            var expectedViewName = "Home";

            //act
            var actionResult = await controller.Home();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, (((ViewResult)actionResult)).ViewName);
        }

        /// <summary>
        /// Tests the Home controller Home method returns the Admin landing page if the Administrator role is present
        /// </summary>
        [TestMethod]
        public async Task HomeController_Returns_Administrator_Landing_Page_With_Administrator_Role()
        {
            //arrange
            var controller = this.GetControllerInstance(true, Roles.Administrator, true, true);
            var expectedViewName = "AdministratorHome";

            //act
            var actionResult = await controller.Home();

            //assert
           Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
           Assert.AreEqual(expectedViewName, (((ViewResult)actionResult)).ViewName);
        }

        /// <summary>
        /// Tests the Home controller Home method returns the Admin landing page if BalanceViewService is unavailable
        /// </summary>
        [TestMethod]
        public async Task HomeController_Returns_Administrator_Landing_Page_When_BalanceViewService_NotAvailable()
        {
            //arrange
            var controller = this.GetControllerInstance(true, Roles.Administrator, false, false);
            var expectedViewName = "AdministratorHome";

            //act
            var actionResult = await controller.Home();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, (((ViewResult)actionResult)).ViewName);
            // Verify an error was logged indicating an error
            this.MockLogger.Verify(l => l.Log(LogLevel.Error,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((a,b) => true),
                It.IsAny<Exception>(),
                It.Is<Func<It.IsAnyType, Exception, string>>((a, b) => true)), Times.Once);
        }

        /// <summary>
        /// Tests the Home controller Home method returns the Admin landing page BalanceViewService returns a null object
        /// </summary>
        [TestMethod]
        public async Task HomeController_Returns_Administrator_Landing_Page_With_When_BalanceViewService_Returns_Null_Object()
        {
            //arrange
            var controller = this.GetControllerInstance(true, Roles.Administrator, false, true);
            var expectedViewName = "AdministratorHome";

            //act
            var actionResult = await controller.Home();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, (((ViewResult)actionResult)).ViewName);
        }

        /// <summary>
        /// Tests the Home controller Home method returns the Suppler landing page if the Supplier role is present
        /// </summary>
        [TestMethod]
        public async Task HomeController_Returns_Supplier_Landing_Page_With_Supplier_Role()
        {
            //arrange
            var controller = this.GetControllerInstance(true, Roles.Supplier, false, false);
            var expectedViewName = "SupplierHome";

            //act
            var actionResult = await controller.Home();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, ((ViewResult)actionResult).ViewName);
        }

        /// <summary>
        /// Tests the Home controller Home method returns the Verifier landing page if the Verifier role is present
        /// </summary>
        [TestMethod]
        public async Task HomeController_Returns_Verifier_Landing_Page_With_Verifier_Role()
        {
            //arrange
            var controller = this.GetControllerInstance(true, Roles.Verifier, false, false);
            var expectedViewName = "VerifierHome";

            //act
            var actionResult = await controller.Home();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, (((ViewResult)actionResult)).ViewName);
        }

        /// <summary>
        /// Tests the Home controller Home method returns the Trader landing page if the Trader role is present
        /// </summary>
        [TestMethod]
        public async Task HomeController_Returns_Trader_Landing_Page_With_Trader_Role()
        {
            //arrange
            var controller = this.GetControllerInstance(true, Roles.Trader, false, false);
            var expectedViewName = "TraderHome";

            //act
            var actionResult = await controller.Home();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, (((ViewResult)actionResult)).ViewName);
        }

        #endregion Home Page Tests

        #region Error Page Tests

        /// <summary>
        /// Tests that the error method returns the NotFound view whena 404 status is supplied
        /// </summary>
        [TestMethod]
        public async Task HomeController_Error_Returns_NotFound_With_404_Status()
        {
            //arrange
            var controller = this.GetControllerInstance(false, null, false, false);
            var expectedViewName = "NotFound";

            //act
            var result = await controller.Error(404);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests that the error method returns the main error page when no status is supplied
        /// </summary>
        [TestMethod]
        public async Task HomeController_Error_Returns_ErrorView_With_Null_Status()
        {
            //arrange
            var controller = this.GetControllerInstance(false, null, false, false);

            //act
            var result = await controller.Error(null);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(ErrorViewModel));            
        }

        #endregion Error Page Tests

        #region Feedback Page Tests

        /// <summary>
        /// Tests that the feedback method returns the feedback page when called
        /// </summary>
        [TestMethod]
        public async Task HomeController_Feedback_Returns_FeedbackView()
        {
            //arrange
            var controller = this.GetControllerInstance(false, null, false, false);

            //act
            var result = await controller.Feedback();

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(FeedbackViewModel));
        }

        #endregion Feedback Page Tests

        #region Cookies Page Tests

        /// <summary>
        /// Tests the Home controller Cookies method returns the Cookies page for an authenticated user
        /// </summary>
        [TestMethod]
        public void HomeController_Returns_Cookies_Page_For_Authenticated_User()
        {
            //arrange
            var controller = this.GetControllerInstance(true, null, false, false);

            //act
            var actionResult = controller.Cookies();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.IsNull(((ViewResult)actionResult).ViewName);
        }

        /// <summary>
        /// Tests the Home controller Cookies method returns the Cookies page for an unauthenticated user
        /// </summary>
        [TestMethod]
        public void HomeController_Returns_Cookies_Page_For_Unauthenticated_User()
        {
            //arrange
            var controller = this.GetControllerInstance(false, null, false, false);

            //act
            var actionResult = controller.Cookies();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.IsNull(((ViewResult)actionResult).ViewName);
        }

        #endregion Cookies Page Tests

        #region Privacy Policy Page Tests

        /// <summary>
        /// Tests the Home controller PrivacyPolicy method returns the Privacy Policy page for an authenticated user
        /// </summary>
        [TestMethod]
        public void HomeController_Returns_PrivacyPolicy_Page_For_Authenticated_User()
        {
            //arrange
            var controller = this.GetControllerInstance(true, null, false, false);

            //act
            var actionResult = controller.PrivacyPolicy();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.IsNull(((ViewResult)actionResult).ViewName);
        }

        /// <summary>
        /// Tests the Home controller PrivacyPolicy method returns the Privacy Policy page for an unauthenticated user
        /// </summary>
        [TestMethod]
        public void HomeController_Returns_PrivacyPolicy_Page_For_Unauthenticated_User()
        {
            //arrange
            var controller = this.GetControllerInstance(false, null, false, false);

            //act
            var actionResult = controller.PrivacyPolicy();

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.IsNull(((ViewResult)actionResult).ViewName);
        }

        #endregion Privacy Policy Page Tests

        #region Private Methods

        /// <summary>
        /// Gets an instance of the Account Controller using Mock objects for dependancies
        /// </summary>
        /// <returns>Returns an instance of the account controller</returns>
        private HomeController GetControllerInstance(bool isUserAuthenticated, string role, bool totalBalancySummaryExists, bool balanceViewServiceAvailable)
        {
            var fakeUser = new GosApplicationUser()
            {
                Id = "Fake User ID",
                OrganisationId = 1
            };

            var fakeObligationPeriod = new ObligationPeriod(1
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>());

            var mockIdentityService = new Mock<IUsersService>();
            mockIdentityService.Setup(s => s.FindUserByEmailAsync(It.IsAny<string>(), default(CancellationToken), It.IsAny<string>()))
                .Returns(Task.Run(()=> { return fakeUser; }));

            var mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            mockReportingPeriodsService.Setup(s => s.GetCurrentObligationPeriod()).Returns(Task.Run(() => { return fakeObligationPeriod; }));

            var mockSystemParametersService = new Mock<ISystemParametersService>();
            List<SystemParameter> fakeSystemParameterList = new List<SystemParameter>
            {
                new SystemParameter(3, "RTFO Support Telephone Number", "123"),
                new SystemParameter(4, "RTFO Support Email", "test@test.com")
            };
            var fakeSystemParameters = new GosSystemParameters(fakeSystemParameterList);
            mockSystemParametersService.Setup(s => s.GetSystemParameters()).Returns(Task.Run(() => { return fakeSystemParameters; }));
            var mockTokenService = new Mock<ITokenService>();
            mockTokenService.Setup(t => t.GetJwtToken()).ReturnsAsync("MockJwtToken");
            mockIdentityService.Setup(s => s.FindUserByIdAsync(It.IsAny<string>()
                , It.IsAny<string>()
                , default(CancellationToken)))
                .Returns(Task.Run(() => { return fakeUser; }));
            var mockOrchestrator = new Mock<ISupplierReportingSummaryOrchestrator>();
            mockOrchestrator.Setup(mock => mock.GetSupplierReportingSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<ClaimsPrincipal>()))
                .ReturnsAsync(new SupplierReportingSummaryViewModel());
            mockOrchestrator.Setup(mock => mock.GetTraderReportingSummary(It.IsAny<int>(), It.IsAny<ObligationPeriod>(), It.IsAny<ClaimsPrincipal>()))
               .ReturnsAsync(new Website.Models.TraderViewModels.TraderSummaryViewModel(It.IsAny<string>()));
            var mockBalanceService = new Mock<IBalanceViewService>();
            if (totalBalancySummaryExists)
            {
                mockBalanceService.Setup(mock => mock.GetSummary(It.IsAny<int>(), It.IsAny<string>()))
                    .Returns(Task.FromResult(new HttpObjectResponse<TotalBalanceSummary>(new TotalBalanceSummary(0.1m, 0.1m, 0.1m))));
            }
            else
            {
                if (balanceViewServiceAvailable)
                {
                    TotalBalanceSummary emptyTotalBalanceSummary = null;
                    mockBalanceService.Setup(mock => mock.GetSummary(It.IsAny<int>(), It.IsAny<string>()))
                        .Returns(Task.FromResult(new HttpObjectResponse<TotalBalanceSummary>(emptyTotalBalanceSummary)));
                }
            }
            MockLogger = new Mock<ILogger<HomeController>>();

            var controller = new HomeController(mockIdentityService.Object
                , mockReportingPeriodsService.Object
                , mockSystemParametersService.Object
                , mockTokenService.Object
                , mockOrchestrator.Object
                , mockBalanceService.Object
                , MockLogger.Object);

            // - user principal
            var mockUser = new Mock<ClaimsPrincipal>();
            mockUser.Setup(u => u.Identity.IsAuthenticated).Returns(isUserAuthenticated);
            mockUser.Setup(u => u.Identity.Name).Returns("Fake Name");
            var claims = new List<Claim>
            {
                new Claim("UserId", Guid.NewGuid().ToString())
            };
            if (!string.IsNullOrEmpty(role))
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }
            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
            
            if (role != null)
            {
                mockUser.Setup(u => u.IsInRole(role)).Returns(true);
            }

            // -Setup the controller
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };

            return controller;
        }

        #endregion Private Methods
    }
}
