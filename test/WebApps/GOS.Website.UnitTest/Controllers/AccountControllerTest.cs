﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Website.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Options;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Website.Models.AccountViewModels;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.SystemParameters.API.Client.Models;
using System.Collections.Generic;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Identity.API.Client.Commands;
using System.Net.Http;
using System.Net;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Configuration;
using System.Threading;
using System.Security.Principal;

namespace DfT.GOS.Website.UnitTest.Controllers
{
    [TestClass]
    public class AccountControllerTest
    {
        #region Properties

        Mock<ISystemParametersService> MockSystemParameterService { get; set; }
        Mock<IUsersService> MockUsersService { get; set; }
        private Mock<ILogger<AccountController>> MockLogger { get; set; }

        private Mock<UserManager<GosApplicationUser>> _mockUserManager;
        private Mock<ILogger<AccountController>> _mockLogger;
        private Mock<IOptions<AppSettings>> _mockAppSettings;
        private Mock<IUsersService> _mockUsersService;
        private Mock<IOrganisationsService> _mockOrganisationService;
        private Mock<IIdentityService> _mockIdentityService;
        private Mock<ITokenService> _mockTokenService;
        private Mock<ISystemParametersService> _mockSystemParametersService;
        private AppSettings _appSettings;
        private AccountController _controller;
        private int _userOrganisationId = 1000;

        #endregion Properties

        #region Constructors

        public AccountControllerTest()
        {
            var userStore = new Mock<IUserStore<GosApplicationUser>>();
            _mockUserManager = new Mock<UserManager<GosApplicationUser>>(
            userStore.Object, null, null, null, null, null, null, null, null);
            _mockLogger = new Mock<ILogger<AccountController>>();
            _mockAppSettings = new Mock<IOptions<AppSettings>>();
            _mockUsersService = new Mock<IUsersService>();
            _mockOrganisationService = new Mock<IOrganisationsService>();
            _mockIdentityService = new Mock<IIdentityService>();
            _mockTokenService = new Mock<ITokenService>();
            _mockSystemParametersService = new Mock<ISystemParametersService>();

            _appSettings = new AppSettings()
            {
                Pagination = new PaginationConfigurationSettings
                {
                    MaximumPagesToDisplay = 10,
                    ResultsPerPage = 5
                }
            };

            // - user principal
            var claims = new List<Claim>
            {
                new Claim("UserId", Guid.NewGuid().ToString()),
                new Claim("OrganisationId", _userOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            var httpContext = new DefaultHttpContext() { User = claimsPrincipal };

            _controller = GetControllerInstance(_mockUserManager.Object, _mockLogger, _appSettings, _mockUsersService, _mockOrganisationService, _mockIdentityService, _mockTokenService, _mockSystemParametersService, httpContext);
        }

        #endregion Constructors

        #region Tests

        #region Sign In Tests

        /// <summary>
        /// Tests that the SignIn method redirects to the landing page if the user is already authenticated
        /// </summary>
        [TestMethod]
        public void AccountController_SignIn_authenticated_user_redirects_to_landing_page()
        {
            //arrange
            var controller = this.GetControllerInstance(true);            

            //act
            var actionResult = controller.SignIn().Result;

            //assert
            // - We're redirected to the Home controller Home Action
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToActionResult));            
            Assert.AreEqual("Home", ((RedirectToActionResult)actionResult).ControllerName);
            Assert.AreEqual("Home", ((RedirectToActionResult)actionResult).ActionName);
        }

        /// <summary>
        /// Tests that the SignIn method displays the Sign in view if the user is not authenticated
        /// </summary>
        [TestMethod]
        public void AccountController_SignIn_non_authenticated_user_displays_SignIn()
        {
            //arrange
            var controller = this.GetControllerInstance(false);           

            //act
            var actionResult = controller.SignIn().Result;

            //assert
            // - We get a view result
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));        
        }

        /// <summary>
        /// Tests that the SignIn method displays the Sign in view if the user is not authenticated and a returnUrl is supplied
        /// </summary>
        [TestMethod]
        public async Task AccountController_SignIn_non_authenticated_user_with_returnUrl_displays_SignIn()
        {
            //arrange
            var returnUrl = "/Suppliers/14/FurtherActions";
            var controller = this.GetControllerInstance(false);

            //act
            var actionResult = await controller.SignIn(returnUrl);

            //assert
            // - We get a view result
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.IsNull(((ViewResult)actionResult).ViewName);
            Assert.AreEqual(returnUrl, controller.ViewData["returnUrl"]);
        }

        /// <summary>
        /// Tests that the SignIn method displays the Sign in view if the user is not authenticated and a returnUrl pointing to the SignOut action is supplied
        /// </summary>
        [TestMethod]
        public async Task AccountController_SignIn_non_authenticated_user_with_signout_returnUrl_displays_SignIn()
        {
            //arrange
            var returnUrl = "/Account/SignOut";
            var controller = this.GetControllerInstance(false);

            //act
            var actionResult = await controller.SignIn(returnUrl);

            //assert
            // - We get a view result
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.IsNull(((ViewResult)actionResult).ViewName);
            Assert.IsNull(controller.ViewData["returnUrl"]);
        }

        #endregion Sign in Tests

        #region Forgotten Password Tests

        /// <summary>
        /// Tests that the ForgottenPassword method displays the ForgottenPassword view
        /// </summary>
        [TestMethod]
        public void AccountController_ForgottenPassword_Returns_Correct_View()
        {
            //arrange
            var controller = this.GetControllerInstance(false);

            //act
            var actionResult = controller.ForgottenPassword().Result;

            //assert
            // - We get a view result
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual("ForgottenPassword", ((ViewResult)actionResult).ViewName);
            Assert.IsInstanceOfType(((ViewResult)actionResult).Model, typeof(ForgottenPasswordViewModel));
            // The system parameter service was called
            this.MockSystemParameterService.Verify(s => s.GetSystemParameters(), Times.Once);
        }

        /// <summary>
        /// Tests that the ForgottenPasswordConfirmation method displays the ForgottenPasswordConfirmation view
        /// </summary>
        [TestMethod]
        public void AccountController_ForgottenPasswordConfirmation_Returns_Correct_View()
        {
            //arrange
            var controller = this.GetControllerInstance(false);

            //act
            var actionResult = controller.ForgottenPasswordConfirmation();

            //assert
            // - We get a view result
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual("ForgottenPasswordConfirmation", ((ViewResult)actionResult).ViewName);
        }

        /// <summary>
        /// Tests that the Forgotten Password post method correctly handles invalid model state
        /// </summary>
        [TestMethod]
        public void AccountController_ForgottenPassword_Post_Handles_Invalid_ModelState()
        {
            //arrange
            var controller = this.GetControllerInstance(false);
            var model = new ForgottenPasswordViewModel();
            controller.ModelState.AddModelError("1", "Fake Error");

            //act
            var actionResult = controller.ForgottenPassword(model).Result;

            //assert
            // - The view is re-displayed
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual("ForgottenPassword", ((ViewResult)actionResult).ViewName);
            Assert.IsInstanceOfType(((ViewResult)actionResult).Model, typeof(ForgottenPasswordViewModel));
            // - the service method isn't called
            this.MockUsersService.Verify(s => s.IssueForgottenPasswordNotification(It.IsAny<IssueForgottenPasswordNotificationCommand>()), Times.Never);
        }

        /// <summary>
        /// Tests that the Forgotten Password post method still redirects to the confirmation screen 
        /// even if there was a validation issue (etc) in the API - this ensures security (preventing guesses of email address)
        /// </summary>
        [TestMethod]
        public void AccountController_ForgottenPassword_Post_Handles_API_Error()
        {
            //arrange
            var controller = this.GetControllerInstance(false);
            var model = new ForgottenPasswordViewModel();

            this.MockUsersService.Setup(s => s.IssueForgottenPasswordNotification(It.IsAny<IssueForgottenPasswordNotificationCommand>()))
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.BadRequest)));

            //act
            var actionResult = controller.ForgottenPassword(model).Result;

            //assert
            // - The view is re-displayed
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToActionResult));
            Assert.AreEqual("ForgottenPasswordConfirmation", ((RedirectToActionResult)actionResult).ActionName);       
            // - the service method is called once
            this.MockUsersService.Verify(s => s.IssueForgottenPasswordNotification(It.IsAny<IssueForgottenPasswordNotificationCommand>()), Times.Once);
        }

        /// <summary>
        /// Tests that the Forgotten Password post method correctly redirects to the confirmation page
        /// </summary>
        [TestMethod]
        public void AccountController_ForgottenPassword_Post_Redirects_To_Confirmation_Page()
        {
            //arrange
            var controller = this.GetControllerInstance(false);
            var model = new ForgottenPasswordViewModel();

            this.MockUsersService.Setup(s => s.IssueForgottenPasswordNotification(It.IsAny<IssueForgottenPasswordNotificationCommand>()))
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)));

            //act
            var actionResult = controller.ForgottenPassword(model).Result;

            //assert
            // - The view is re-displayed
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToActionResult));
            Assert.AreEqual("ForgottenPasswordConfirmation", ((RedirectToActionResult)actionResult).ActionName);
            // - the service method is called once
            this.MockUsersService.Verify(s => s.IssueForgottenPasswordNotification(It.IsAny<IssueForgottenPasswordNotificationCommand>()), Times.Once);
        }

        /// <summary>
        /// Tests that the reset password Get method handles a null password reset request code
        /// </summary>
        [TestMethod]
        public async Task AccountController_ResetPassword_Get_Handles_Null_code()
        {
            //arrange
            var controller = this.GetControllerInstance(false);

            this.MockUsersService.Setup(s => s.ValidateResetPasswordToken(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)));

            //act
            var result = await controller.ResetPassword(code: null);

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the reset password Get method returns the correct view and correctly populates the view model
        /// </summary>
        [TestMethod]
        public async Task AccountController_ResetPassword_Get_Returns_View()
        {
            //arrange
            var fakeCode = "Fake Code";
            var controller = this.GetControllerInstance(false);

            this.MockUsersService.Setup(s => s.ValidateResetPasswordToken(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)));

            //act
            var result = await controller.ResetPassword(fakeCode);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ResetPassword", ((ViewResult)result).ViewName);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(ResetPasswordViewModel));
            Assert.AreEqual(fakeCode, ((ResetPasswordViewModel)((ViewResult)result).Model).Code);
        }

        /// <summary>
        /// Tests that the reset password Get method returns the correct view if token does not exist or is expired
        /// </summary>
        [TestMethod]
        public async Task AccountController_ResetPassword_Get_Returns_Error_View_For_Expired_Null_Tokens()
        {
            //arrange
            var fakeCode = "Fake Code";
            var controller = this.GetControllerInstance(false);

            this.MockUsersService.Setup(s => s.ValidateResetPasswordToken(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.BadRequest)));

            //act
            var result = await controller.ResetPassword(fakeCode);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ResetPasswordError", ((ViewResult)result).ViewName);
        }

        /// <summary>
        /// Tests that the Reset Password (POST) method handles invalid model state
        /// </summary>
        [TestMethod]
        public async Task AccountController_ResetPassword_Post_Handles_Invalid_ModelState()
        {
            //arrange
            var viewModel = new ResetPasswordViewModel();
            var controller = this.GetControllerInstance(false);
            controller.ModelState.AddModelError("1", "Fake Error");

            //act
            var result = await controller.ResetPassword(viewModel);

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ResetPassword", ((ViewResult)result).ViewName);
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(ResetPasswordViewModel));
        }

        /// <summary>
        /// Tests that the Reset Password (POST) method handles a failure (e.g. incorrect code) from the API
        /// </summary>
        [TestMethod]
        public async Task AccountController_ResetPassword_Post_Handles_API_Failure()
        {
            //arrange
            var viewModel = new ResetPasswordViewModel();
            var controller = this.GetControllerInstance(false);

            this.MockUsersService.Setup(s => s.ResetPassword(It.IsAny<ResetPasswordCommand>()))
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.BadRequest)));
         
            //act
            var result = await controller.ResetPassword(viewModel);

            //assert
            // - The method might have failed, but for security reasons we should still be redirected to the confirmation screen
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("ResetPasswordConfirmation", ((RedirectToActionResult)result).ActionName);
            // - Test that a warning was logged
            this.MockLogger.Verify(l => l.Log(LogLevel.Warning, 0, It.IsAny<It.IsAnyType>(), It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
        }

        /// <summary>
        /// Tests that the Reset Password (POST) method handles a success result from the API
        /// </summary>
        [TestMethod]
        public async Task AccountController_ResetPassword_Post_Handles_API_Success()
        {
            //arrange
            var viewModel = new ResetPasswordViewModel();
            var controller = this.GetControllerInstance(false);

            this.MockUsersService.Setup(s => s.ResetPassword(It.IsAny<ResetPasswordCommand>()))
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)));

            //act
            var result = await controller.ResetPassword(viewModel);

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("ResetPasswordConfirmation", ((RedirectToActionResult)result).ActionName);
            //Test no warnings were logged
            this.MockLogger.Verify(l => l.Log(LogLevel.Warning, 0, It.IsAny<It.IsAnyType>(), It.IsAny<Exception>(),
                It.IsAny<Func<object, Exception, string>>()), Times.Never);
        }

        /// <summary>
        /// Tests that the reset password confirmation Get method returns the correct view 
        /// </summary>
        [TestMethod]
        public void AccountController_ResetPasswordConfirmation_Get_Returns_View()
        {
            //arrange
            var controller = this.GetControllerInstance(false);

            //act
            var result = controller.ResetPasswordConfirmation();

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual("ResetPasswordConfirmation", ((ViewResult)result).ViewName);
        }

        #endregion Forgotten Password Tests

        #region How to register

        /// <summary>
        /// Tests that the HowToRegister method returns the correct view model
        /// </summary>
        [TestMethod]
        public async Task AccountController_HowToRegister_Returns_View_Model()
        {
            //arrange
            var controller = this.GetControllerInstance(false);
            var expectedViewName = "HowToRegister";

            //act
            var result = await controller.HowToRegister();

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, ((ViewResult)result).ViewName);
        }

        #endregion How to register

        #region Invite Tests

        /// <summary>
        /// Tests that the registration form is pre-populated with the email when using a invite
        /// </summary>
        [TestMethod]
        public void AccountController_invite_prepopulates_registration_form()
        {
            //arrange
            var controller = this.GetControllerInstance(false);
            var expectedViewName = "Register";
            var expectedEmail = "johndoe@email.com";
            var expectedOrgId = 1;
            var expectedOrgName = "Supplier";
            var expectedRole = "Supplier";

            //act
            var actionResult = controller.Register(It.IsAny<string>()).Result;

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            Assert.AreEqual(expectedViewName, ((ViewResult)actionResult).ViewName);
            Assert.AreEqual(expectedEmail, ((RegisterViewModel)((ViewResult)actionResult).Model).Email);
            Assert.AreEqual(expectedOrgId, ((RegisterViewModel)((ViewResult)actionResult).Model).OrganisationId);
            Assert.AreEqual(expectedOrgName, ((RegisterViewModel)((ViewResult)actionResult).Model).Company);
            Assert.AreEqual(expectedRole, ((RegisterViewModel)((ViewResult)actionResult).Model).Role);
        }

        #endregion Invite Tests

        #region Account Details Tests

        /// <summary>
        /// Tests that the Index method displays the View User Account Details screen if the user is authenticated
        /// </summary>
        [TestMethod]
        public async Task AccountController_Index_Authenticated_User_Returns_ViewUserAccountDetails_When_Account_Exists()
        {
            //arrange (cannot use GetControllerInstance method because we need the UserManager class to be set-up to find users by email address)
            var email = "joe.bloggs@supplier.co.uk";
            var rtfoSupportEmail = "gos@dft.gov.uk";
            var rtfoSupportTelephone = "12345";
            var user = new GosApplicationUser() { FullName = "Joe Bloggs", Email = email };

            var systemParametersService = new Mock<ISystemParametersService>();
            var systemParameters= new SystemParameter[] 
            {
                new SystemParameter(3, "RtfoSupportTelephoneNumber", rtfoSupportTelephone),
                new SystemParameter(4, "RtfoSupportEmailAddress", rtfoSupportEmail)
            };
            systemParametersService.Setup(mock => mock.GetSystemParameters())
                .ReturnsAsync(new GosSystemParameters(systemParameters));
            var userStore = new Mock<IUserEmailStore<GosApplicationUser>>();
            userStore.Setup(mock => mock.FindByEmailAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(user);
            var userManager = new UserManager<GosApplicationUser>(userStore.Object, null, null, null, null, null, null, null, null);
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", Guid.NewGuid().ToString()));
            claims.Add(new Claim("UserName", email));
            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
            var principal = new Mock<IIdentity>();
            principal.Setup(mock => mock.Name)
                .Returns(email);
            var httpContext = new DefaultHttpContext() { User = claimsPrincipal };
            var controller = GetControllerInstance(userManager, _mockLogger, _appSettings, _mockUsersService, _mockOrganisationService, _mockIdentityService, _mockTokenService, systemParametersService, httpContext);

            //act
            var result = await controller.Index();

            //assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsNull(((ViewResult)result).ViewName, "Use the default View for Action Method");
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(AccountDetailsViewModel));
            Assert.AreEqual(user.FullName, ((AccountDetailsViewModel)((ViewResult)result).Model).FullName);
            Assert.AreEqual(user.Email, ((AccountDetailsViewModel)((ViewResult)result).Model).Email);
            Assert.AreEqual(rtfoSupportEmail, ((AccountDetailsViewModel)((ViewResult)result).Model).RtfoSupportEmailAddress);
            Assert.AreEqual(rtfoSupportTelephone, ((AccountDetailsViewModel)((ViewResult)result).Model).RtfoSupportTelephoneNumber);
        }

        /// <summary>
        /// Tests that the Index method displays the View User Account Details screen if the user is authenticated
        /// </summary>
        [TestMethod]
        public async Task AccountController_Index_Authenticated_User_Returns_ViewUserAccountDetails_When_Account_Does_Not_Exist()
        {
            //arrange (cannot use GetControllerInstance method because we need the UserManager class to be set-up to find users by email address)
            var email = "joe.bloggs@supplier.co.uk";

            var userStore = new Mock<IUserEmailStore<GosApplicationUser>>();
            var userManager = new UserManager<GosApplicationUser>(userStore.Object, null, null, null, null, null, null, null, null);
            var principal = new Mock<IIdentity>();
            principal.Setup(mock => mock.Name)
                .Returns(email);
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", Guid.NewGuid().ToString()));
            claims.Add(new Claim("UserName", email));
            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
            var httpContext = new DefaultHttpContext() { User = claimsPrincipal };
            var controller = GetControllerInstance(userManager, _mockLogger, _appSettings, _mockUsersService, _mockOrganisationService, _mockIdentityService, _mockTokenService, _mockSystemParametersService, httpContext);

            //act
            var result = await controller.Index();

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        #endregion Account Details Tests

        #endregion Tests

        #region Private Methods

        private AccountController GetControllerInstance(UserManager<GosApplicationUser> mockUserManager,
            Mock<ILogger<AccountController>> mockLogger,
            AppSettings appSettings,
            Mock<IUsersService> mockUsersService,
            Mock<IOrganisationsService> mockOrganisationService,
            Mock<IIdentityService> mockIdentityService,
            Mock<ITokenService> mockTokenService,
            Mock<ISystemParametersService> mockSystemParametersService,
            DefaultHttpContext httpContext)
        {
            var controller = new AccountController(mockUserManager, mockLogger.Object
                , Options.Create(appSettings), mockUsersService.Object
                , mockOrganisationService.Object, mockIdentityService.Object
                , mockTokenService.Object, mockSystemParametersService.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext
            };

            return controller;
        }

        /// <summary>
        /// Gets an instance of the Account Controller using Mock objects for dependancies
        /// </summary>
        /// <returns>Returns an instance of the account controller</returns>
        private AccountController GetControllerInstance(bool isUserAuthenticated)
        {
            // - user manager
            var userStore = new Mock<IUserStore<GosApplicationUser>>();
            var userManager = new UserManager<GosApplicationUser>(userStore.Object, null, null, null, null, null, null, null, null);

            // - sign in manager
            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var mockUserClaimsPrincipleFactory = new Mock<IUserClaimsPrincipalFactory<GosApplicationUser>>();

            // -system parameter service
            this.MockSystemParameterService = new Mock<ISystemParametersService>();
            var fakeSystemParameters = new List<SystemParameter>();
            fakeSystemParameters.Add(new SystemParameter(GosSystemParameters.SystemParameterId_RtfoSupportTelephoneNumber, null, null));
            fakeSystemParameters.Add(new SystemParameter(GosSystemParameters.SystemParameterId_RtfoSupportEmailAddress, null, null));
            this.MockSystemParameterService.Setup(s => s.GetSystemParameters()).Returns(Task.FromResult(new GosSystemParameters(fakeSystemParameters)));

            // - logging service
            this.MockLogger = new Mock<ILogger<AccountController>>();
            // - users service
            this.MockUsersService = new Mock<IUsersService>();

            // - users Service Mock
            this.MockUsersService.Setup(s => s.GetInviteDetailsByLinkAsync(It.IsAny<string>()))
                .ReturnsAsync(new GosInvitedUserAssociation { Email = "johndoe@email.com", OrganisationId = 1, Company = "Supplier", Role = "Supplier" });

            // - user principal
            var mockUser = new Mock<ClaimsPrincipal>();
            mockUser.Setup(u => u.Identity.IsAuthenticated).Returns(isUserAuthenticated);

            // - authentication service
            var mockAuthenticationService = new Mock<IAuthenticationService>();
            mockAuthenticationService.Setup(s => s.SignOutAsync(It.IsAny<HttpContext>(), It.IsAny<string>(), It.IsAny<AuthenticationProperties>()))
                .Returns(Task.FromResult((object)null));

            var mockServiceProvider = new Mock<IServiceProvider>();
            mockServiceProvider.Setup(s => s.GetService(typeof(IAuthenticationService)))
                .Returns(mockAuthenticationService.Object);

            var httpContext = new DefaultHttpContext()
            {
                User = mockUser.Object,
                RequestServices = mockServiceProvider.Object
            };

            var controller = GetControllerInstance(userManager, this.MockLogger, _appSettings, this.MockUsersService, _mockOrganisationService, _mockIdentityService, _mockTokenService, this.MockSystemParameterService, httpContext);

            //View Features (ITempDataDictionary Mock for calls to View() from the controller) - normally added in startup by calling app.UseMvc
            var mockTempData = new Mock<ITempDataDictionary>();
            controller.TempData = mockTempData.Object;

            //Url Helper Factory (to enable testing of redirect to action)
            var mockUrlHelper = new Mock<IUrlHelper>();            
            controller.Url = mockUrlHelper.Object;

            return controller;
        }

        #endregion Private Methods
    }
}
