﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT). See License file in the project root for license information.
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Website.Models.ApplicationsViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DfT.GOS.Website.UnitTest.Models.ApplicationsViewModels
{
    [TestClass]
    public class ApplicationDetailsViewModelTest
    {
        private readonly ObligationPeriod ObligationPeriod = new ObligationPeriod(1, DateTime.Now, null, true, null, 0, 0, null, null, null, null, null, null);             
        private readonly Organisation Organisation = new Organisation(1, "Test Org", 1, "Active", 1, "Test");

        /// <summary>
        /// Tests that the user can enter a CI of 0 for Electricity applications
        /// </summary>
        [TestMethod]
        public void ApplicationDetailsViewModel_ElectricityGhgIntensity_Allows_Zero_Value()
        {
            // Arrange
            var model = new ApplicationDetailsViewModel()
            {
                DefaultElectricityGhgIntensity = 0,
                ElectricityAmountSupplied = 1000,
                ElectricityFuelTypeId = 1,
                ElectricityGhgIntensity = 0,
                FuelCategory = ApplicationDetailsViewModel.FuelCategories.Electricity,
                ObligationPeriod = ObligationPeriod,
                Supplier = Organisation
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var result = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(0, validationResults.Count);
        }

        /// <summary>
        /// Tests that the user can enter a CI greater than 0 for Electricity applications
        /// </summary>
        [TestMethod]
        public void ApplicationDetailsViewModel_ElectricityGhgIntensity_Allows_Value_Greater_Than__Zero()
        {
            // Arrange
            var model = new ApplicationDetailsViewModel()
            {
                DefaultElectricityGhgIntensity = 0,
                ElectricityAmountSupplied = 1000,
                ElectricityFuelTypeId = 1,
                ElectricityGhgIntensity = 0.1m,
                FuelCategory = ApplicationDetailsViewModel.FuelCategories.Electricity,
                ObligationPeriod = ObligationPeriod,
                Supplier = Organisation
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var result = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(0, validationResults.Count);
        }

        /// <summary>
        /// Tests that the user can enter a CI less than 0 for Electricity applications
        /// </summary>
        [TestMethod]
        public void ApplicationDetailsViewModel_ElectricityGhgIntensity_Allows_Value_Below_Zero()
        {
            // Arrange
            var model = new ApplicationDetailsViewModel()
            {
                DefaultElectricityGhgIntensity = 0,
                ElectricityAmountSupplied = 1000,
                ElectricityFuelTypeId = 1,
                ElectricityGhgIntensity = -0.1m,
                FuelCategory = ApplicationDetailsViewModel.FuelCategories.Electricity,
                ObligationPeriod = ObligationPeriod,
                Supplier = Organisation
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var result = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(0, validationResults.Count);
        }

    }
}
