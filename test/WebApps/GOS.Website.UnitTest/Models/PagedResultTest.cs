﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Website.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DfT.GOS.Website.UnitTest.Models
{
    /// <summary>
    /// Tests the UI PageResult class
    /// </summary>
    [TestClass]
    public class PagedResultTest
    {
        #region Constructors

        public PagedResultTest()
        {
        }

        #endregion Constructors

        #region Tests
        
        /// <summary>
        /// Tests the UI paged result is setup for the following scenario:
        /// (1) 2 3 4 5 - results 1 to 20
        /// </summary>
        [TestMethod]
        public void PagedResult_Display5_FirstPage()
        {
            //arrange / act
            var pageResult = new UIPagedResult<Object>()
            {
                MaximumPagesToDisplay = 5,
                PageSize = 20,
                PageNumber = 1,
                TotalResults = 501
            };

            //assert
            TestPageResultSettings(pageResult, 1, 5, 1, 20, 26);
        }

        /// <summary>
        /// Tests the UI paged result is setup for the following scenario:
        /// 1 (2) 3 4 5 - results 21 to 40
        /// </summary>
        [TestMethod]
        public void PagedResult_Display5_SecondPage()
        {
            //arrange / act
            var pageResult = new UIPagedResult<Object>()
            {
                MaximumPagesToDisplay = 5,
                PageSize = 20,
                PageNumber = 2,
                TotalResults = 501
            };

            //assert
            TestPageResultSettings(pageResult, 1, 5, 21, 40, 26);
        }

        /// <summary>
        /// Tests the UI paged result is setup for the following scenario:
        /// 1 2 (3) 4 5 - results 41 to 60
        /// </summary>
        [TestMethod]
        public void PagedResult_Display5_ThirdPage()
        {
            //arrange / act
            var pageResult = new UIPagedResult<Object>()
            {
                MaximumPagesToDisplay = 5,
                PageSize = 20,
                PageNumber = 3,
                TotalResults = 501
            };

            //assert
            TestPageResultSettings(pageResult, 1, 5, 41, 60, 26);
        }

        /// <summary>
        /// Tests the UI paged result is setup for the following scenario:
        /// 2 3 (4) 5 6- results 61 to 80
        /// </summary>
        [TestMethod]
        public void PagedResult_Display5_FourthPage()
        {
            //arrange / act
            var pageResult = new UIPagedResult<Object>()
            {
                MaximumPagesToDisplay = 5,
                PageSize = 20,
                PageNumber = 4,
                TotalResults = 501
            };

            //assert
            TestPageResultSettings(pageResult, 2, 6, 61, 80, 26);
        }

        /// <summary>
        /// Tests the UI paged result is setup for the following scenario:
        /// 22 23 24 25 (26)- results 500 to 501
        /// </summary>
        [TestMethod]
        public void PagedResult_Display5_LastPage()
        {
            //arrange / act
            var pageResult = new UIPagedResult<Object>()
            {
                MaximumPagesToDisplay = 5,
                PageSize = 20,
                PageNumber = 26,
                TotalResults = 501
            };

            //assert
            TestPageResultSettings(pageResult, 22, 26, 501, 501, 26);
        }

        /// <summary>
        /// Tests the UI paged result is setup for the following scenario:
        /// (1) 2 3 4 5 6 - results 1 to 20
        /// </summary>
        [TestMethod]
        public void PagedResult_Display6_FirstPage()
        {
            //arrange / act
            var pageResult = new UIPagedResult<Object>()
            {
                MaximumPagesToDisplay = 6,
                PageSize = 20,
                PageNumber = 1,
                TotalResults = 501
            };

            //assert
            TestPageResultSettings(pageResult, 1, 6, 1, 20, 26);
        }

        /// <summary>
        /// Tests the UI paged result is setup for the following scenario:
        /// 1 2 3 (4) 5 6 - results 61 to 80
        /// </summary>
        [TestMethod]
        public void PagedResult_Display6_FourthPage()
        {
            //arrange / act
            var pageResult = new UIPagedResult<Object>()
            {
                MaximumPagesToDisplay = 6,
                PageSize = 20,
                PageNumber = 4,
                TotalResults = 501
            };

            //assert
            TestPageResultSettings(pageResult, 1, 6, 61, 80, 26);
        }

        /// <summary>
        /// Tests the UI paged result is setup for the following scenario:
        /// 1 2 3 4 (5) 6 - results 81 to 100
        /// </summary>
        [TestMethod]
        public void PagedResult_Display6_FifthPage()
        {
            //arrange / act
            var pageResult = new UIPagedResult<Object>()
            {
                MaximumPagesToDisplay = 6,
                PageSize = 20,
                PageNumber = 5,
                TotalResults = 501
            };

            //assert
            TestPageResultSettings(pageResult, 2, 7, 81, 100, 26);
        }

        #endregion Tests

        #region Methods

        /// <summary>
        /// Tests the page results settings against expected values
        /// </summary>
        /// <param name="pageResult">The paged result settings under test</param>
        /// <param name="expectedFirstPageToDisplay">Expected first page to display in the UI</param>
        /// <param name="lastPageToDisplay">Expected last page to display in the UI</param>
        /// <param name="firstResult">Expected first result within the page</param>
        /// <param name="lastResult">Expected last result within the page</param>
        /// <param name="pageCount">Expected page count</param>
        private static void TestPageResultSettings(UIPagedResult<object> pageResult
            , int expectedFirstPageToDisplay
            , int expectedLastPageToDisplay
            , int expectedFirstResult
            , int expectedLastResult
            , int expectedPageCount)
        {
            Assert.AreEqual(expectedFirstPageToDisplay, pageResult.FirstPageToDisplay);
            Assert.AreEqual(expectedLastPageToDisplay, pageResult.LastPageToDisplay);
            Assert.AreEqual(expectedFirstResult, pageResult.FirstResult);
            Assert.AreEqual(expectedLastResult, pageResult.LastResult);
            Assert.AreEqual(expectedPageCount, pageResult.PageCount);
        }

        #endregion Methods
    }
}
