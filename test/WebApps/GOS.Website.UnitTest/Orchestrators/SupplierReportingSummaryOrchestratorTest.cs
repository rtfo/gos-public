﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.Security.Services;
using DfT.GOS.Web.Models;
using DfT.GOS.Website.Configuration;
using DfT.GOS.Website.Orchestrators;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using RtfoAdminConsignmentSubmission = DfT.GOS.GhgBalanceView.Common.Models.RtfoAdminConsignmentSubmission;

namespace DfT.GOS.Website.UnitTest.Orchestrators
{
    /// <summary>
    /// Tests the UI SupplierReportingSummaryOrchestrator class
    /// </summary>
    [TestClass]
    public class SupplierReportingSummaryOrchestratorTest
    {
        #region Properties

        private Mock<IOptions<AppSettings>> _mockAppSettings { get; set; }
        private Mock<ClaimsPrincipal> _mockUser { get; set; }
        private Mock<IOrganisationsService> _mockOrganisationService { get; set; }
        private Mock<ITokenService> _mockTokenService { get; set; }
        private Mock<IBalanceViewService> _mockBalanceViewService { get; set; }
        private ISupplierReportingSummaryOrchestrator ReportingSummaryOrchestrator { get; set; }
        private ClaimsPrincipal _claimsPrincipal;
        private int _userOrganisationId = 1000;
        #endregion Properties

        #region Initialize
        [TestInitialize]
        public void Initialize()
        {
            _mockOrganisationService = new Mock<IOrganisationsService>();
            _mockTokenService = new Mock<ITokenService>();
            _mockBalanceViewService = new Mock<IBalanceViewService>();
            var appSettings = new AppSettings() { Pagination = new PaginationConfigurationSettings { MaximumPagesToDisplay = 10, ResultsPerPage = 5 } };
            var options = Options.Create(appSettings);
            ReportingSummaryOrchestrator = new SupplierReportingSummaryOrchestrator(_mockOrganisationService.Object
                , _mockBalanceViewService.Object, _mockTokenService.Object
                , options);
            // - user principal
            var claims = new List<Claim>
            {
                new Claim("UserId", Guid.NewGuid().ToString()),
                new Claim("OrganisationId", _userOrganisationId.ToString())
            };

            _claimsPrincipal = new ClaimsPrincipal();
            _claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
        }
        #endregion Initialize

        #region Tests

        #region GetSupplierReportingSummary Tests

        /// <summary>
        /// Tests that the SupplierReportingSummaryOrchestrator correctly sets DisplaySupplierContext to true for administrator users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierReportingSummary_DisplaySupplierContext_Is_True_For_Administrator()
        {
            //arrange 
            var fakeOrganisation = new Organisation(1, "Fake Organisation", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, true);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>())));

            //act
            var viewModel = await orchestrator.GetSupplierReportingSummary(It.IsAny<int>(), obligationPeriod, _mockUser.Object);

            //assert
            Assert.IsTrue(viewModel.DisplaySupplierContext);
        }

        /// <summary>
        /// Tests that the SupplierReportingSummaryOrchestrator correctly sets DisplaySupplierContext to false for non-administrator users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierReportingSummary_DisplaySupplierContext_Is_False_For_NonAdministrator()
        {
            //arrange 
            var fakeOrganisation = new Organisation(1, "Fake Organisation", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);

            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>())));

            //act
            var viewModel = await orchestrator.GetSupplierReportingSummary(It.IsAny<int>(), obligationPeriod, _mockUser.Object);

            //assert
            Assert.IsFalse(viewModel.DisplaySupplierContext);
        }

        /// <summary>
        /// Tests that the SupplierReportingSummaryOrchestrator GetSupplierReportingSummary method returns null 
        /// if no organisation is found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierReportingSummary_Returns_Null_Supplier_Not_Found()
        {
            //arrange 
            Organisation fakeOrganisation = null;
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>())));

            //act
            var viewModel = await orchestrator.GetSupplierReportingSummary(It.IsAny<int>(), obligationPeriod, _mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the SupplierReportingSummaryOrchestrator GetSupplierReportingSummary method returns null 
        /// if a non-supplier organisation is found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierReportingSummary_Returns_Null_Non_Supplier()
        {
            //arrange 
            var fakeOrganisation = new Organisation(1, "Fake Non-Supplier", 3, "Approved", 3, "Verifier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>())));

            //act
            var viewModel = await orchestrator.GetSupplierReportingSummary(It.IsAny<int>(), obligationPeriod, _mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the SupplierReportingSummaryOrchestrator GetSupplierReportingSummary method returns null 
        /// if the Obligation Period is not found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierReportingSummary_Returns_Null_If_ObligationPeriod_Not_Found()
        {
            //arrange 
            var fakeOrganisation = new Organisation(1, "Fake Non-Supplier", 3, "Approved", 2, "Trader");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            ObligationPeriod obligationPeriod = null;

            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>())));

            //act
            var viewModel = await orchestrator.GetSupplierReportingSummary(It.IsAny<int>(), obligationPeriod, _mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        #endregion GetSupplierReportingSummary Tests

        #region GetSupplierSubmissionsSummary Tests

        /// <summary>
        /// Tests that the GetSupplierSubmissionsSummary method sets the DisplaySupplierContext to true for Admin users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsSummary_DisplayContext_Is_True_For_Administrator()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, true);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<SubmissionBalanceSummary>(new SubmissionBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<LiquidGasSubmissionsSummary>()
                                , It.IsAny<ElectricitySubmissionsSummary>()
                                , It.IsAny<UpstreamEmissionsReductionSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsSummary(fakeOrganisation.Id, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNotNull(viewModel);
            Assert.IsTrue(viewModel.DisplaySupplierContext);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsSummary method sets the DisplaySupplierContext to false for non-Admin users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsSummary_DisplayContext_Is_False_For_Non_Administrator()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<SubmissionBalanceSummary>(new SubmissionBalanceSummary(It.IsAny<decimal>()
                    , It.IsAny<decimal>()
                    , It.IsAny<LiquidGasSubmissionsSummary>()
                    , It.IsAny<ElectricitySubmissionsSummary>()
                    , It.IsAny<UpstreamEmissionsReductionSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsSummary(fakeOrganisation.Id, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNotNull(viewModel);
            Assert.IsFalse(viewModel.DisplaySupplierContext);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsSummary method returns null if the Organisation cannot be found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsSummary_Returns_Null_If_Organisation_Not_Found()
        {
            //arrange
            Organisation fakeOrganisation = null;
            var fakeOrganisationId = 1;
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<SubmissionBalanceSummary>(new SubmissionBalanceSummary(It.IsAny<decimal>()
                    , It.IsAny<decimal>()
                    , It.IsAny<LiquidGasSubmissionsSummary>()
                    , It.IsAny<ElectricitySubmissionsSummary>()
                    , It.IsAny<UpstreamEmissionsReductionSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsSummary(fakeOrganisationId, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsSummary method returns null if the Organisation is not a Supplier
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsSummary_Returns_Null_If_Organisation_Not_Supplier()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 2, "Trader");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<SubmissionBalanceSummary>(new SubmissionBalanceSummary(It.IsAny<decimal>()
                    , It.IsAny<decimal>()
                    , It.IsAny<LiquidGasSubmissionsSummary>()
                    , It.IsAny<ElectricitySubmissionsSummary>()
                    , It.IsAny<UpstreamEmissionsReductionSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsSummary(fakeOrganisation.Id, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsSummary method returns null if the Obligation Period is not found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsSummary_Returns_Null_If_ObligationPeriod_Not_Found()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 2, "Trader");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            ObligationPeriod obligationPeriod = null;

            _mockBalanceViewService.Setup(s => s.GetSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<SubmissionBalanceSummary>(new SubmissionBalanceSummary(It.IsAny<decimal>()
                    , It.IsAny<decimal>()
                    , It.IsAny<LiquidGasSubmissionsSummary>()
                    , It.IsAny<ElectricitySubmissionsSummary>()
                    , It.IsAny<UpstreamEmissionsReductionSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsSummary(fakeOrganisation.Id, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        #endregion GetSupplierSubmissionsSummary Tests

        #region GetSupplierSubmissionsLiquidGasSummary Tests

        /// <summary>
        /// Tests that the GetSupplierSubmissionsLiquidGasSummary method sets the DisplaySupplierContext to true for Admin users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsLiquidGasSummary_DisplayContext_Is_True_For_Administrator()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, true);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsLiquidGasSummary(fakeOrganisation.Id, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNotNull(viewModel);
            Assert.IsTrue(viewModel.DisplaySupplierContext);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsLiquidGasSummary method sets the DisplaySupplierContext to false for non-Admin users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsLiquidGasSummary_DisplayContext_Is_False_For_Non_Administrator()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsLiquidGasSummary(fakeOrganisation.Id, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNotNull(viewModel);
            Assert.IsFalse(viewModel.DisplaySupplierContext);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsLiquidGasSummary method returns null if the Organisation cannot be found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsLiquidGasSummary_Returns_Null_If_Organisation_Not_Found()
        {
            //arrange
            Organisation fakeOrganisation = null;
            var fakeOrganisationId = 1;
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsLiquidGasSummary(fakeOrganisationId, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsLiquidGasSummary method returns null if the Organisation is not a Supplier
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsLiquidGasSummary_Returns_Null_If_Organisation_Not_Supplier()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 2, "Trader");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsLiquidGasSummary(fakeOrganisation.Id, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsLiquidGasSummary method returns null if the Obligation Period is not found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetSupplierSubmissionsLiquidGasSummary_Returns_Null_If_ObligationPeriod_Not_Found()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 2, "Trader");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            ObligationPeriod obligationPeriod = null;

            _mockBalanceViewService.Setup(s => s.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>())));

            //act
            var viewModel = await orchestrator.GetSupplierSubmissionsLiquidGasSummary(fakeOrganisation.Id, obligationPeriod, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        #endregion GetSupplierSubmissionsLiquidGasSummary Tests

        #region GetTradingSummary Tests

        /// <summary>
        /// Tests that the GetTradingSummary method sets the DisplaySupplierContext to true for Admin users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetTradingSummary_DisplayContext_Is_True_For_Administrator()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, true);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetTrading(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<TradingSummary>(new TradingSummary(It.IsAny<decimal>()
                                , new PagedResult<CreditEvent>())));

            //act
            var viewModel = await orchestrator.GetTradingSummary(fakeOrganisation.Id, obligationPeriod, 1, this._mockUser.Object);

            //assert
            Assert.IsNotNull(viewModel);
            Assert.IsTrue(viewModel.DisplayOrganisationContext);
        }

        /// <summary>
        /// Tests that the GetTradingSummary method sets the DisplaySupplierContext to false for non-Admin users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetTradingSummary_DisplayContext_Is_False_For_Non_Administrator()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetTrading(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<TradingSummary>(new TradingSummary(It.IsAny<decimal>()
                                , new PagedResult<CreditEvent>())));

            //act
            var viewModel = await orchestrator.GetTradingSummary(fakeOrganisation.Id, obligationPeriod, 1, this._mockUser.Object);

            //assert
            Assert.IsNotNull(viewModel);
            Assert.IsFalse(viewModel.DisplayOrganisationContext);
        }

        /// <summary>
        /// Tests that the GetTradingSummary method returns null if the Organisation cannot be found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetTradingSummary_Returns_Null_If_Organisation_Not_Found()
        {
            //arrange
            Organisation fakeOrganisation = null;
            var fakeOrganisationId = 1;
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetTrading(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<TradingSummary>(new TradingSummary(It.IsAny<decimal>()
                                , new PagedResult<CreditEvent>())));

            //act
            var viewModel = await orchestrator.GetTradingSummary(fakeOrganisationId, obligationPeriod, 1, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }


        /// <summary>
        /// Tests that the GetTradingSummary method returns null if the Obligation Period is not found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetTradingSummary_Returns_Null_If_ObligationPeriod_Not_Found()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 2, "Trader");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            ObligationPeriod obligationPeriod = null;

            _mockBalanceViewService.Setup(s => s.GetTrading(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<TradingSummary>(new TradingSummary(It.IsAny<decimal>()
                                , new PagedResult<CreditEvent>())));

            //act
            var viewModel = await orchestrator.GetTradingSummary(fakeOrganisation.Id, obligationPeriod, 1, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        #endregion GetTradingSummary Tests

        #region GetSupplierSubmissionsOtherRtfoVolumes
        /// <summary>
        /// Tests that the GetSupplierSubmissionsOtherRtfoVolumes returns null when obligation is null
        /// </summary>
        [TestMethod]
        public async Task GetSupplierSubmissionsOtherRtfoVolumes_Returns_Null_When_Obligation_IsNull()
        {
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetSupplierSubmissionsOtherRtfoVolumes(1, null, 1, 10, null);
            //
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsOtherRtfoVolumes returns null when Organisation api returns no result
        /// </summary>
        [TestMethod]
        public async Task GetSupplierSubmissionsOtherRtfoVolumes_Returns_Null_When_Organisation_NoFound_Or_Null()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<LiquidGasBalanceSummary>(HttpStatusCode.NotFound)));
            _mockBalanceViewService.Setup(x => x.GetOtherRtfoVolumes(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<OtherVolumesSubmission>>(HttpStatusCode.NotFound)));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetSupplierSubmissionsOtherRtfoVolumes(1, obligationPeriod, 1, 10, _claimsPrincipal);
            //
            Assert.IsNull(viewModel);
        }
        /// <summary>
        /// Tests that the GetSupplierSubmissionsOtherRtfoVolumes returns view model without other volumes
        /// </summary>
        [TestMethod]
        public async Task GetSupplierSubmissionsOtherRtfoVolumes_Returns_ViewModel_Without_AdminConssignment()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(1, "Fake Organisation", 3, "Approved", 1, "Supplier"))));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>()))));
            _mockBalanceViewService.Setup(x => x.GetOtherRtfoVolumes(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<OtherVolumesSubmission>>(HttpStatusCode.NotFound)));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetSupplierSubmissionsOtherRtfoVolumes(1, obligationPeriod, 1, 10, _claimsPrincipal);
            //
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.LiquidGasBalanceSummary);
            Assert.IsNotNull(viewModel.ObligationPeriod);
            Assert.IsNotNull(viewModel.SupplierId);
            Assert.IsNotNull(viewModel.SupplierName);
            Assert.IsNull(viewModel.OtherVolumesSubmission);
        }

        /// <summary>
        /// Tests that the GetSupplierSubmissionsOtherRtfoVolumes returns view model
        /// </summary>
        [TestMethod]
        public async Task GetSupplierSubmissionsOtherRtfoVolumes_Returns_ViewModel()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(1, "Fake Organisation", 3, "Approved", 1, "Supplier"))));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>()))));
            _mockBalanceViewService.Setup(x => x.GetOtherRtfoVolumes(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<PagedResult<OtherVolumesSubmission>>(
                        new PagedResult<OtherVolumesSubmission>() { Result = new List<OtherVolumesSubmission>() })));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetSupplierSubmissionsOtherRtfoVolumes(1, obligationPeriod, 1, 10, _claimsPrincipal);
            //
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.LiquidGasBalanceSummary);
            Assert.IsNotNull(viewModel.ObligationPeriod);
            Assert.IsNotNull(viewModel.SupplierId);
            Assert.IsNotNull(viewModel.SupplierName);
            Assert.IsNotNull(viewModel.OtherVolumesSubmission);
        }

        #endregion GetSupplierSubmissionsOtherRtfoVolumes

        #region GetSupplierRtfoAdminConsignment
        /// <summary>
        /// Tests that the GetSupplierRtfoAdminConsignment returns null when obligation is null
        /// </summary>
        [TestMethod]
        public async Task GetSupplierRtfoAdminConsignment_Returns_Null_When_Obligation_IsNull()
        {
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetSupplierRtfoAdminConsignment(1, null, 1, null);
            //
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the GetSupplierRtfoAdminConsignment returns null when Organisation api returns no result
        /// </summary>
        [TestMethod]
        public async Task GetSupplierRtfoAdminConsignment_Returns_Null_When_Organisation_NoFound_Or_Null()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<LiquidGasBalanceSummary>(HttpStatusCode.NotFound)));
            _mockBalanceViewService.Setup(x => x.GetRtfoAdminConsignment(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<RtfoAdminConsignmentSubmission>>(HttpStatusCode.NotFound)));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetSupplierRtfoAdminConsignment(1, obligationPeriod, 1, _claimsPrincipal);
            //
            Assert.IsNull(viewModel);
        }
        /// <summary>
        /// Tests that the GetSupplierRtfoAdminConsignment returns view model without admin consignment
        /// </summary>
        [TestMethod]
        public async Task GetSupplierRtfoAdminConsignment_Returns_ViewModel_Without_AdminConssignment()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(1, "Fake Organisation", 3, "Approved", 1, "Supplier"))));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>()))));
            _mockBalanceViewService.Setup(x => x.GetRtfoAdminConsignment(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<PagedResult<RtfoAdminConsignmentSubmission>>(HttpStatusCode.NotFound)));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetSupplierRtfoAdminConsignment(1, obligationPeriod, 1, _claimsPrincipal);
            //
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.LiquidGasBalanceSummary);
            Assert.IsNotNull(viewModel.ObligationPeriod);
            Assert.IsNotNull(viewModel.SupplierId);
            Assert.IsNotNull(viewModel.SupplierName);
            Assert.IsNull(viewModel.RtfoAdminConsignmentSubmissions);
        }

        /// <summary>
        /// Tests that the GetSupplierRtfoAdminConsignment returns view model
        /// </summary>
        [TestMethod]
        public async Task GetSupplierRtfoAdminConsignment_Returns_ViewModel()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(1, "Fake Organisation", 3, "Approved", 1, "Supplier"))));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>()))));
            _mockBalanceViewService.Setup(x => x.GetRtfoAdminConsignment(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<PagedResult<RtfoAdminConsignmentSubmission>>(
                        new PagedResult<RtfoAdminConsignmentSubmission>() { Result = new List<RtfoAdminConsignmentSubmission>()})));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetSupplierRtfoAdminConsignment(1, obligationPeriod, 1, _claimsPrincipal);
            //
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.LiquidGasBalanceSummary);
            Assert.IsNotNull(viewModel.ObligationPeriod);
            Assert.IsNotNull(viewModel.SupplierId);
            Assert.IsNotNull(viewModel.SupplierName);
            Assert.IsNotNull(viewModel.RtfoAdminConsignmentSubmissions);
        }

        #endregion GetSupplierRtfoAdminConsignment

        #region GetFossilFuelSubmissions
        /// <summary>
        /// Tests that the GetFossilFuelSubmissions returns null when obligation is null
        /// </summary>
        [TestMethod]
        public async Task GetFossilFuelSubmissions_Returns_Null_When_Obligation_IsNull()
        {
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetFossilFuelSubmissions(1, null, 1, null);
            //
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the GetFossilFuelSubmissions returns null when Organisation api returns no result
        /// </summary>
        [TestMethod]
        public async Task GetFossilFuelSubmissions_Returns_Null_When_Organisation_NoFound_Or_Null()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<LiquidGasBalanceSummary>(HttpStatusCode.NotFound)));
            _mockBalanceViewService.Setup(x => x.GetFossilFuelSubmissions(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<PagedResult<GhgBalanceView.Common.Models.FossilGasSubmission>>(HttpStatusCode.NotFound)));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetFossilFuelSubmissions(1, obligationPeriod, 1, _claimsPrincipal);
            //
            Assert.IsNull(viewModel);
        }
        /// <summary>
        /// Tests that the GetFossilFuelSubmissions returns view model without admin consignment
        /// </summary>
        [TestMethod]
        public async Task GetFossilFuelSubmissions_Returns_ViewModel_Without_FossilGas()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(1, "Fake Organisation", 3, "Approved", 1, "Supplier"))));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>()))));
            _mockBalanceViewService.Setup(x => x.GetFossilFuelSubmissions(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<PagedResult<GhgBalanceView.Common.Models.FossilGasSubmission>>(HttpStatusCode.NotFound)));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetFossilFuelSubmissions(1, obligationPeriod, 1, _claimsPrincipal);
            //
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.LiquidGasBalanceSummary);
            Assert.IsNotNull(viewModel.ObligationPeriod);
            Assert.IsNotNull(viewModel.SupplierId);
            Assert.IsNotNull(viewModel.SupplierName);
            Assert.IsNull(viewModel.FossilFuelSubmissions);
        }

        /// <summary>
        /// Tests that the GetFossilFuelSubmissions returns view model
        /// </summary>
        [TestMethod]
        public async Task GetFossilFuelSubmissions_Returns_ViewModel()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(
                    new Organisation(1, "Fake Organisation", 3, "Approved", 1, "Supplier"))));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<LiquidGasBalanceSummary>(new LiquidGasBalanceSummary(It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<decimal>()
                                , It.IsAny<RtfoAdminConsignmentsSubmissionsSummary>()
                                , It.IsAny<OtherVolumesSubmissionsSummary>()
                                , It.IsAny<FossilGasSubmissionsSummary>()))));
            _mockBalanceViewService.Setup(x => x.GetFossilFuelSubmissions(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<PagedResult<GhgBalanceView.Common.Models.FossilGasSubmission>>(
                        new PagedResult<GhgBalanceView.Common.Models.FossilGasSubmission>()
                        { Result = new List<GhgBalanceView.Common.Models.FossilGasSubmission>() })));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetFossilFuelSubmissions(1, obligationPeriod, 1, _claimsPrincipal);
            //
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.LiquidGasBalanceSummary);
            Assert.IsNotNull(viewModel.ObligationPeriod);
            Assert.IsNotNull(viewModel.SupplierId);
            Assert.IsNotNull(viewModel.SupplierName);
            Assert.IsNotNull(viewModel.FossilFuelSubmissions);
        }

        #endregion GetFossilFuelSubmissions

        #region GetElectricitySubmissions
        /// <summary>
        /// Tests that the GetElectricitySubmissions returns null when obligation is null
        /// </summary>
        [TestMethod]
        public async Task GetElectricitySubmissions_Returns_Null_When_Obligation_IsNull()
        {
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetElectricitySubmissions(1, null, 1, null);
            //
            Assert.IsNull(viewModel);
        }

        /// <summary>
        /// Tests that the GetElectricitySubmissions returns null when Organisation api returns no result
        /// </summary>
        [TestMethod]
        public async Task GetElectricitySubmissions_Returns_Null_When_Organisation_NoFound_Or_Null()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);
            _mockOrganisationService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            _mockBalanceViewService.Setup(x => x.GetElectricitySubmissions(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new HttpObjectResponse<PagedResult<GhgBalanceView.Common.Models.ElectricitySubmission>>(HttpStatusCode.NotFound)));
            //act
            var viewModel = await ReportingSummaryOrchestrator.GetElectricitySubmissions(1, obligationPeriod, 1, _claimsPrincipal);
            //
            Assert.IsNull(viewModel);
        }

        #endregion GetElectricitySubmissions

        #region GetUpstreamEmissionsReductionsSubmissions Tests

        /// <summary>
        /// Tests that the GetUpstreamEmissionsReductionsSubmissions method sets the DisplaySupplierContext to true for Admin users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetUpstreamEmissionsReductionsSubmissions_DisplayContext_Is_True_For_Administrator()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, true);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetUpstreamEmissionReductionSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<UpstreamEmissionsReductionsSummary>(new UpstreamEmissionsReductionsSummary(It.IsAny<decimal>()
                                , new PagedResult<UpstreamEmissionsReductionSubmission>())));

            //act
            var viewModel = await orchestrator.GetUpstreamEmissionsReductionsSubmissions(fakeOrganisation.Id, obligationPeriod, 1, this._mockUser.Object);

            //assert
            Assert.IsNotNull(viewModel);
            Assert.IsTrue(viewModel.DisplayOrganisationContext);
        }

        /// <summary>
        /// Tests that the GetUpstreamEmissionsReductionsSubmissions method sets the DisplaySupplierContext to false for non-Admin users
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetUpstreamEmissionsReductionsSubmissions_DisplayContext_Is_False_For_Non_Administrator()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 1, "Supplier");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetUpstreamEmissionReductionSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                 .ReturnsAsync(new HttpObjectResponse<UpstreamEmissionsReductionsSummary>(new UpstreamEmissionsReductionsSummary(It.IsAny<decimal>()
                                 , new PagedResult<UpstreamEmissionsReductionSubmission>())));

            //act
            var viewModel = await orchestrator.GetUpstreamEmissionsReductionsSubmissions(fakeOrganisation.Id, obligationPeriod, 1, this._mockUser.Object);

            //assert
            Assert.IsNotNull(viewModel);
            Assert.IsFalse(viewModel.DisplayOrganisationContext);
        }

        /// <summary>
        /// Tests that the GetUpstreamEmissionsReductionsSubmissions method returns null if the Organisation cannot be found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetUpstreamEmissionsReductionsSubmissions_Returns_Null_If_Organisation_Not_Found()
        {
            //arrange
            Organisation fakeOrganisation = null;
            var fakeOrganisationId = 1;
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 0, 0, null, null, null, null, null, null);

            _mockBalanceViewService.Setup(s => s.GetUpstreamEmissionReductionSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<UpstreamEmissionsReductionsSummary>(new UpstreamEmissionsReductionsSummary(It.IsAny<decimal>()
                                , new PagedResult<UpstreamEmissionsReductionSubmission>())));

            //act
            var viewModel = await orchestrator.GetUpstreamEmissionsReductionsSubmissions(fakeOrganisationId, obligationPeriod, 1, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }


        /// <summary>
        /// Tests that the GetUpstreamEmissionsReductionsSubmissions method returns null if the Obligation Period is not found
        /// </summary>
        [TestMethod]
        public async Task SupplierReportingSummaryOrchestrator_GetUpstreamEmissionsReductionsSubmissions_Returns_Null_If_ObligationPeriod_Not_Found()
        {
            //arrange
            var fakeOrganisation = new Organisation(1, "Fake Supplier", 3, "Approved", 2, "Trader");
            var orchestrator = this.GetOrchestrator(fakeOrganisation, false);
            ObligationPeriod obligationPeriod = null;

            _mockBalanceViewService.Setup(s => s.GetUpstreamEmissionReductionSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<UpstreamEmissionsReductionsSummary>(new UpstreamEmissionsReductionsSummary(It.IsAny<decimal>()
                                , new PagedResult<UpstreamEmissionsReductionSubmission>())));

            //act
            var viewModel = await orchestrator.GetTradingSummary(fakeOrganisation.Id, obligationPeriod, 1, this._mockUser.Object);

            //assert
            Assert.IsNull(viewModel);
        }

        #endregion GetUpstreamEmissionsReductionsSubmissions Tests

        #endregion Tests

        #region Methods

        /// <summary>
        /// Gets an instance of the SupplierReportingSummaryOrchestrator class for testing
        /// </summary>
        /// <param name="isAdministratorUser">Whether the user is an administrator</param>
        /// <returns>Returns an instanciated SupplierReportingSummaryOrchestrator for testing</returns>
        private SupplierReportingSummaryOrchestrator GetOrchestrator(Organisation fakeOrganisation, bool isAdministratorUser)
        {
            // - user principal
            _mockUser = new Mock<ClaimsPrincipal>();
            _mockUser.Setup(u => u.IsInRole(Roles.Administrator))
                .Returns(isAdministratorUser);

            //-organisation service
            _mockOrganisationService = new Mock<IOrganisationsService>();
            _mockOrganisationService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(fakeOrganisation));

            _mockTokenService = new Mock<ITokenService>();

            _mockBalanceViewService = new Mock<IBalanceViewService>();

            _mockTokenService.Setup(t => t.GetJwtToken()).ReturnsAsync("MockJwtToken");

            _mockAppSettings = new Mock<IOptions<AppSettings>>();
            _mockAppSettings.Setup(s => s.Value.Pagination.MaximumPagesToDisplay).Returns(5);
            _mockAppSettings.Setup(s => s.Value.Pagination.ResultsPerPage).Returns(10);

            var orchestrator = new SupplierReportingSummaryOrchestrator(_mockOrganisationService.Object, _mockBalanceViewService.Object, _mockTokenService.Object, _mockAppSettings.Object);

            return orchestrator;
        }

        #endregion Methods
    }
}
