﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DfT.GOS.Website.UnitTest.Extensions
{
    /// <summary>
    /// Unit tests for the GosEmailAddressAttribute class
    /// </summary>
    [TestClass]
    public class GosEmailAddressAttributeTest
    {
        #region Tests Methods

        /// <summary>
        /// Tests a combination of valid and invliad email addresses
        /// </summary>
        [TestMethod]
        public void GosEmailAddressAttribute_Tests()
        {
            //arrange
            var validator = new GosEmailAddressAttribute();

            //act / assert

            // - valid email addresses
            Assert.IsTrue(validator.IsValid("test@test.com"));
            Assert.IsTrue(validator.IsValid("test.test@test.com"));
            Assert.IsTrue(validator.IsValid("test.test@test-test.com"));
            Assert.IsTrue(validator.IsValid("qatest@qatest.com"));

            // - invalid email addresses
            Assert.IsFalse(validator.IsValid("test"));
            Assert.IsFalse(validator.IsValid("test@test"));
            Assert.IsFalse(validator.IsValid("test.test@test"));
            Assert.IsFalse(validator.IsValid("qatest@"));
            Assert.IsFalse(validator.IsValid("qatest"));
            Assert.IsFalse(validator.IsValid("qatest@qatest"));
            Assert.IsFalse(validator.IsValid("qatest@qatest."));
            Assert.IsFalse(validator.IsValid("qatest.com"));
            Assert.IsFalse(validator.IsValid("qatest@.com"));
            Assert.IsFalse(validator.IsValid("@.com"));
            Assert.IsFalse(validator.IsValid("qatest@qatest@qatest.com"));
            Assert.IsFalse(validator.IsValid("qatest@qatest.com."));
        }

        #endregion Tests Methods
    }
}
