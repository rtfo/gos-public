﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Website.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DfT.GOS.Website.UnitTest.Extensions
{
    /// <summary>
    /// Unit Tests for the ObligationPeriod extension methods
    /// </summary>
    [TestClass]
    public class ObligationPeriodExtensionsTest
    {
        #region Test Methods

        /// <summary>
        /// Check that the extension method handles a null ObligationPeriod
        /// </summary>
        [TestMethod]
        public void ObligationPeriodExtensions_GetDescription_Handles_Null_Values()
        {
            // Arrange
            ObligationPeriod obligationPeriod = null;

            // Act
            var result = obligationPeriod.GetDescription();

            // Assert
            Assert.AreEqual("No Obligation Period", result);
        }

        /// <summary>
        /// Check that the extension method handles an ObligationPeriod with a null Start Date
        /// </summary>
        [TestMethod]
        public void ObligationPeriodExtensions_GetDescription_Handles_Null_StartDate()
        {
            // Arrange
            var obligationPeriod = new ObligationPeriod(1, null, new DateTime(2020, 3, 1), false, null, 100, 1000, null, null, null, null, null, null);

            // Act
            var result = obligationPeriod.GetDescription();

            // Assert
            Assert.AreEqual("01/03/2020", result);
        }

        /// <summary>
        /// Checks that the extension method handles an ObligationPeriod with a null End Date
        /// </summary>
        [TestMethod]
        public void ObligationPeriodExtensions_GetDescription_Handles_Null_EndDate()
        {
            // Arrange
            var obligationPeriod = new ObligationPeriod(1, new DateTime(2020, 3, 1), null, false, null, 100, 1000, null, null, null, null, null, null);

            // Act
            var result = obligationPeriod.GetDescription();

            // Assert
            Assert.AreEqual("No Obligation Period", result);
        }

        /// <summary>
        /// Checks that the extension method handles an ObligationPeriod with a null StartDate and EndDate
        /// </summary>
        [TestMethod]
        public void ObligationPeriodExtensions_GetDescription_Handles_Null_StartDate_And_EndDate()
        {
            // Arrange
            var obligationPeriod = new ObligationPeriod(1, null, null, false, null, 100, 1000, null, null, null, null, null, null);

            // Act
            var result = obligationPeriod.GetDescription();

            // Assert
            Assert.AreEqual("No Obligation Period", result);
        }

        /// <summary>
        /// Checks that the extension method returns the expected description for an ObligationPeriod with a StartDate and EndDate
        /// </summary>
        [TestMethod]
        public void ObligationPeriodExtensions_GetDescription_Returns_Expected_Description()
        {
            // Arrange
            var obligationPeriod = new ObligationPeriod(1, new DateTime(2019, 2, 28), new DateTime(2020, 3, 1), false, null, 100, 1000, null, null, null, null, null, null);

            // Act
            var result = obligationPeriod.GetDescription();

            // Assert
            Assert.AreEqual("01/03/2020", result);
        }

        #endregion Test Methods
    }
}
