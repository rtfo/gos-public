﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgLedger.Common.Models;
using DfT.GOS.Identity.Common.Models;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Services;
using GOS.RtfoApplications.Common.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace DfT.GOS.Website.UnitTest.Services
{
    /// <summary>
    /// Unit tests for the PerformanceDataHelperServiceTest service
    /// </summary>
    [TestClass]
    public class PerformanceDataHelperServiceTest
    {
        #region Properties
        private PerformanceDataHelperService PerformanceDataHelperService { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Initializes the mock objects used for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.PerformanceDataHelperService = new PerformanceDataHelperService();
        }

        #endregion Initialize

        #region Tests

        /// <summary>
        /// Tests that the Get method can retrieve Transactions by Obligation Period Id and Organisation Id
        /// </summary>
        [TestMethod]
        public void PerformanceDataHelperService_Get_Concatenated_Performance_Data_Returns_Result()
        {
            //arrange
            var appPerformance = new List<PerformanceData> { new PerformanceData(new ApplicationPerformanceData(new System.DateTime(2019, 1, 1), 1, 2, 3, 4, 5, 6, 7)) };
            var regPerformance = new List<PerformanceData> { new PerformanceData(new RegistrationsPerformanceData(new System.DateTime(2019, 1, 1), 8, 9, 10)) };
            var ledPerformance = new List<PerformanceData> { new PerformanceData(new LedgerPerformanceData(new System.DateTime(2019, 1, 1), 11)) };
            var rtfoPerformance = new List<PerformanceData> { new PerformanceData(new RtfoPerformanceData(new System.DateTime(2019, 1, 1), 12)) };

            //assert
            var result = this.PerformanceDataHelperService.GetConcatenatedPerformanceData(appPerformance, regPerformance, ledPerformance, rtfoPerformance).ToList();

            //act
            Assert.IsNotNull(result);
            Assert.AreEqual(new System.DateTime(2019, 1, 1), result[0].Date);
            Assert.AreEqual(1, result[0].NoOfGosApplicationsWithCreditsIssued);
            Assert.AreEqual(2, result[0].NoOfIncompleteGosApplications);
            Assert.AreEqual(3, result[0].NoOfRejectedGosApplications);
            Assert.AreEqual(4, result[0].NoOfRevokedGosApplications);
            Assert.AreEqual(5, result[0].NoOfSubmittedGosApplications);
            Assert.AreEqual(6, result[0].NoOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue);
            Assert.AreEqual(7, result[0].TotalNoOfGosApplications);
            Assert.AreEqual(8, result[0].InvitationsWithoutAccount);
            Assert.AreEqual(9, result[0].Invitations);
            Assert.AreEqual(10, result[0].TotalUsers);
            Assert.AreEqual(11, result[0].NoOfTransferTransactions);
            Assert.AreEqual(12, result[0].AdminConsignmentCount);
            Assert.AreEqual(19, result[0].TotalNoOfGosAndRosApplications);
        }

        #endregion Tests
    }
}
