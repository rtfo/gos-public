﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Security.Claims;
using DfT.GOS.Website.Middleware;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Middleware
{
    [TestClass]
    public class CookieTokenAuthenticationEventsTest
    {
        #region Properties
        private Mock<IIdentityService> IdentityService;
        private CookieTokenAuthenticationEvents CookieTokenAuthenticationEvents;
        #endregion Properties


        #region Initialisation

        /// <summary>
        /// Performs initialisation before each test is run
        /// </summary>
        [TestInitialize]
        public void Initialise()
        {
            this.IdentityService = new Mock<IIdentityService>();
            var mockLogger = new Mock<ILogger<CookieTokenAuthenticationEvents>>();
            this.CookieTokenAuthenticationEvents = new CookieTokenAuthenticationEvents(this.IdentityService.Object, mockLogger.Object);
        }

        #endregion Initialisation

        #region ValidatePrincipal Tests
        [TestMethod]
        public async Task ValidatePrincipal_Rejects_When_Invalid_Security_Stamp()
        {
            // Arrange
            var httpContext = new Mock<HttpContext>();
            var handlerType = new Mock<IAuthenticationHandler>();
            var authenticationScheme = new AuthenticationScheme("scheme", "name", handlerType.Object.GetType());
            var options = new CookieAuthenticationOptions();
            var authTicket = new AuthenticationTicket(new ClaimsPrincipal(), "authorization");
            var context = new Mock<CookieValidatePrincipalContext>(httpContext.Object, authenticationScheme, options, authTicket);
            var mockPrincipal = new Mock<ClaimsPrincipal>();
            var claims = new List<Claim>()
            {
                new Claim(Claims.SecurityStamp, "abc123"),
                new Claim(Claims.UserId, "user1")
            };
            mockPrincipal.Setup(p => p.Claims).Returns(claims);
            context.Setup(c => c.Principal).Returns(mockPrincipal.Object);
            this.IdentityService.Setup(s => s.ValidateSecurityStamp("abc123", "user1")).Returns(Task.FromResult(false));

            // Act
            await this.CookieTokenAuthenticationEvents.ValidatePrincipal(context.Object);

            // Assert
            this.IdentityService.Verify(s => s.ValidateSecurityStamp("abc123", "user1"));
        }

        #endregion ValidatePrincipal Tests
    }
}
