﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Website.Middleware;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Middleware
{
    /// <summary>
    /// Unit tests for the GDS Cookie Policy middleware component
    /// </summary>
    [TestClass]
    public class GdsCookiePolicyTest
    {
        #region Constants

        private const int CookieWarningCookieExpiryInMonths = 3;

        #endregion Constants

        #region Properties

        private HttpContext HttpContext { get; set; }
        private GdsCookiePolicy Middleware { get; set; }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Performs initialisation before each test is run
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            var mockOptions = new Mock<IOptions<AppSettings>>();
            mockOptions.Setup(mock => mock.Value)
                .Returns(new AppSettings() { CookieWarningCookieExpirationInMonths = CookieWarningCookieExpiryInMonths });
            this.Middleware = new GdsCookiePolicy(context => Task.CompletedTask, mockOptions.Object);
            this.HttpContext = new DefaultHttpContext();
        }

        #endregion Initialization

        #region Tests

        /// <summary>
        /// Tests that the GDS Cookie Policy component detects the cookie that indicates that the user has been shown the cookie warning
        /// </summary>
        [TestMethod]
        public async Task GdsCookiePolicy_Invoke_Detects_CookieWarning_Cookie()
        {
            //arrange
            var mockCookieCollection = new Mock<IRequestCookieCollection>(MockBehavior.Strict);
            mockCookieCollection.Setup(mock => mock.Count)
                .Returns(1);
            var cookies = new List<KeyValuePair<string, string>>();
            cookies.Add(new KeyValuePair<string, string>(GdsCookiePolicy.CookieName, string.Empty));
            mockCookieCollection.Setup(mock => mock.GetEnumerator())
                .Returns(() => cookies.GetEnumerator());
            var builder = new CookieBuilder() { Name = GdsCookiePolicy.CookieName };
            this.HttpContext.Request.Cookies = mockCookieCollection.Object;

            //act
            await this.Middleware.Invoke(this.HttpContext);

            //assert
            Assert.IsNotNull(this.HttpContext);
            Assert.IsNotNull(this.HttpContext.Items[GdsCookiePolicy.ShowCookieWarning]);
            Assert.AreEqual(false, this.HttpContext.Items[GdsCookiePolicy.ShowCookieWarning]);
        }

        /// <summary>
        /// Tests that the GDS Cookie Policy component handles the absence of the cookie that indicates that the user has been shown the cookie warning
        /// </summary>
        [TestMethod]
        public async Task GdsCookiePolicy_Invoke_Detects_Absence_Of_CookieWarning_Cookie()
        {
            //arrange
            var expectedExpiryDate = DateTime.Now.AddMonths(CookieWarningCookieExpiryInMonths);

            //act
            await this.Middleware.Invoke(this.HttpContext);

            //assert
            Assert.IsNotNull(this.HttpContext);
            Assert.IsNotNull(this.HttpContext.Items[GdsCookiePolicy.ShowCookieWarning]);
            Assert.AreEqual(true, this.HttpContext.Items[GdsCookiePolicy.ShowCookieWarning]);
            //cookies will appear in response headers
            Assert.AreEqual(1, this.HttpContext.Response.Headers.Count);
            Assert.AreEqual("Set-Cookie", this.HttpContext.Response.Headers.First().Key);
            Assert.AreEqual(1, this.HttpContext.Response.Headers.First().Value.Count());
            Assert.IsNotNull(this.HttpContext.Response.Headers.First().Value.First());
            //don't check the time component of the cookie expiration date as this may be affected by the duration of the test
            var expected = $"{GdsCookiePolicy.CookieName}=true; expires={expectedExpiryDate.ToString("R").Substring(0, 16)}";
            var headerString = this.HttpContext.Response.Headers.First().Value.First();
            Assert.IsTrue(headerString.StartsWith(expected));
            Assert.IsTrue(headerString.EndsWith("GMT; path=/; samesite=lax; httponly"), headerString);
            Console.Out.WriteLine(headerString);
        }

        #endregion Tests
    }
}
