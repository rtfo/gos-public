﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Website.Extensions;
using DfT.GOS.Website.Middleware;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Middleware
{
    /// <summary>
    /// Unit tests for the HTTP Security Headers middleware component
    /// </summary>
    [TestClass]
    public class SecurityHeadersTest
    {
        #region Constants

        private const string ContentSecurityPolicyHeader = "Content-Security-Policy";
        private const string CrossSiteScriptingPolicyHeader = "X-XSS-Protection";
        private const string ContentTypeOptionsPolicyHeader = "X-Content-Type-Options";
        private const string ReferrerPolicyHeader = "Referrer-Policy";
        private const string CacheControlHeader = "Cache-control";
        private const string PragmaCacheControlHeader = "Pragma";
        private const string FeaturePolicyHeader = "Feature-Policy";

        #endregion Constants

        #region Properties

        private HttpContext HttpContext { get; set; }
        private SecurityHeaders SecurityHeadersMiddleware { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialisation before each test is run
        /// </summary>
        [TestInitializeAttribute]
        public void Initialise()
        {
            this.SecurityHeadersMiddleware = new SecurityHeaders(context => Task.CompletedTask);
            //Create a nonce service and add to the HttpContext
            var services = new ServiceCollection();
            services.AddScoped<CSPNonceService>(s => new CSPNonceService());
            this.HttpContext = new DefaultHttpContext 
            { 
                RequestServices = services.BuildServiceProvider()
            };
       }

        #endregion Initialisation

        #region Tests

        /// <summary>
        /// Tests that the Security Headers class adds the expected security headers
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SecurityHeadersTest_Invoke_Adds_All_Security_Headers()
        {
            //arrange
            var expectedHeadersCount = 7;

            //act
            await this.SecurityHeadersMiddleware.Invoke(this.HttpContext);

            //assert
            Assert.IsNotNull(this.HttpContext);
            Assert.AreEqual(this.HttpContext.Response.Headers.Count, expectedHeadersCount);
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(ContentSecurityPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(CrossSiteScriptingPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(ContentTypeOptionsPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(ReferrerPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(CacheControlHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(PragmaCacheControlHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(FeaturePolicyHeader));
        }

        /// <summary>
        /// Tests that the Security Headers class overwrites existing headers
        /// </summary>
        [TestMethod]
        public async Task SecurityHeadersTest_Invoke_Overwrites_All_Existing_Security_Headers()
        {
            //arrange
            var expectedHeadersCount = 7;
            string originalHeaderValue = "Original Fake Value";
            this.HttpContext.Response.Headers.Add(ContentSecurityPolicyHeader, originalHeaderValue);
            this.HttpContext.Response.Headers.Add(CrossSiteScriptingPolicyHeader, originalHeaderValue);
            this.HttpContext.Response.Headers.Add(ContentTypeOptionsPolicyHeader, originalHeaderValue);
            this.HttpContext.Response.Headers.Add(ReferrerPolicyHeader, originalHeaderValue);
            this.HttpContext.Response.Headers.Add(CacheControlHeader, originalHeaderValue);
            this.HttpContext.Response.Headers.Add(PragmaCacheControlHeader, originalHeaderValue);
            this.HttpContext.Response.Headers.Add(FeaturePolicyHeader, originalHeaderValue);

            //act
            await this.SecurityHeadersMiddleware.Invoke(this.HttpContext);

            //assert
            Assert.IsNotNull(this.HttpContext);
            Assert.AreEqual(this.HttpContext.Response.Headers.Count, expectedHeadersCount);
            Assert.AreNotEqual(originalHeaderValue, this.HttpContext.Response.Headers[ContentSecurityPolicyHeader][0]);
            Assert.AreNotEqual(originalHeaderValue, this.HttpContext.Response.Headers[CrossSiteScriptingPolicyHeader][0]);
            Assert.AreNotEqual(originalHeaderValue, this.HttpContext.Response.Headers[ContentTypeOptionsPolicyHeader][0]);
            Assert.AreNotEqual(originalHeaderValue, this.HttpContext.Response.Headers[ReferrerPolicyHeader][0]);
            Assert.AreNotEqual(originalHeaderValue, this.HttpContext.Response.Headers[CacheControlHeader][0]);
            Assert.AreNotEqual(originalHeaderValue, this.HttpContext.Response.Headers[PragmaCacheControlHeader][0]);
            Assert.AreNotEqual(originalHeaderValue, this.HttpContext.Response.Headers[FeaturePolicyHeader][0]);
        }

        /// <summary>
        /// Tests that the ApplySecurityHeaders method adds the expected security headers
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public void SecurityHeadersTest_ApplySecurityHeaders_Adds_All_Security_Headers_When_ApplyCachingPolicies_True()
        {
            //arrange
            var expectedHeadersCount = 7;

            //act
            SecurityHeaders.ApplySecurityHeaders(this.HttpContext, applyCachingPolicies:true);

            //assert
            Assert.IsNotNull(this.HttpContext);
            Assert.AreEqual(this.HttpContext.Response.Headers.Count, expectedHeadersCount);
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(ContentSecurityPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(CrossSiteScriptingPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(ContentTypeOptionsPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(ReferrerPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(CacheControlHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(PragmaCacheControlHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(FeaturePolicyHeader));
        }

        /// <summary>
        /// Tests that the ApplySecurityHeaders method adds the expected security headers
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public void SecurityHeadersTest_ApplySecurityHeaders_Adds_All_Security_Headers_When_ApplyCachingPolicies_False()
        {
            //arrange
            var expectedHeadersCount = 5;

            //act
            SecurityHeaders.ApplySecurityHeaders(this.HttpContext, applyCachingPolicies: false);

            //assert
            Assert.IsNotNull(this.HttpContext);
            Assert.AreEqual(this.HttpContext.Response.Headers.Count, expectedHeadersCount);
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(ContentSecurityPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(CrossSiteScriptingPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(ContentTypeOptionsPolicyHeader));
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(ReferrerPolicyHeader));            
            Assert.IsTrue(this.HttpContext.Response.Headers.ContainsKey(FeaturePolicyHeader));
            Assert.IsFalse(this.HttpContext.Response.Headers.ContainsKey(CacheControlHeader));
            Assert.IsFalse(this.HttpContext.Response.Headers.ContainsKey(PragmaCacheControlHeader));
        }

        #endregion Tests
    }
}
