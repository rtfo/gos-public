﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Website.UnitTest.Fakes
{
    public class FakeSignInManager<TUser> : SignInManager<TUser> where TUser : class
    {
        private Dictionary<string, SignInResult> Logins { get; set; }
        private Dictionary<string, ExternalLoginInfo> ExternalLogins { get; set; }
        public FakeSignInManager(UserManager<TUser> userManager,
            IHttpContextAccessor contextAccessor,
            IUserClaimsPrincipalFactory<TUser> claimsFactory,
            IOptions<Microsoft.AspNetCore.Identity.IdentityOptions> optionsAccessor,
            ILogger<Microsoft.AspNetCore.Identity.SignInManager<TUser>> logger,
            IAuthenticationSchemeProvider schemes,
            Dictionary<string, SignInResult> logins,
            IUserConfirmation<TUser> confirmation,
            Dictionary<string, ExternalLoginInfo> externalLogins) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes, confirmation)
        {
            this.Logins = logins;
            this.ExternalLogins = externalLogins;
        }

        public override Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)
        {
            if (this.Logins != null && this.Logins.ContainsKey(userName))
            {
                return Task.FromResult(this.Logins[userName]);
            }
            else
            {
                return Task.FromResult(SignInResult.Failed);
            }
        }

        public override Task SignInAsync(TUser user, AuthenticationProperties authenticationProperties, string authenticationMethod = null)
        {
            return Task.CompletedTask;
        }
        public override Task SignOutAsync()
        {
            return Task.CompletedTask;
        }

        public override Task<ExternalLoginInfo> GetExternalLoginInfoAsync(string userId = null)
        {
            if (this.ExternalLogins != null && this.ExternalLogins.ContainsKey(userId))
            {
                return Task.FromResult(this.ExternalLogins[userId]);
            }
            else
            {
                return Task.FromResult<ExternalLoginInfo>(null);
            }

        }
    }
}
