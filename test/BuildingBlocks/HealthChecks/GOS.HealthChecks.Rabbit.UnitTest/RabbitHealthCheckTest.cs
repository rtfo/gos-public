// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.HealthChecks.Rabbit;
using DfT.GOS.UnitTest.Common.Attributes;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.HealthChecks.UnitTest.Rabbit
{
    /// <summary>
    /// Unit tests for the RabbitHealthCheck class
    /// </summary>
    [TestClass]
    public class RabbitHealthCheckTest
    {
        #region Constants

        private const int ConnectionTimeoutMilliseconds = 100;

        #endregion Constants

        #region Properties

        private IHealthCheck HealthCheck { get; set; }
        private Mock<IModel> Channel { get; set; }
        private Mock<IConnection> Connection { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialisation required for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.Channel = new Mock<IModel>();
            this.Connection = new Mock<IConnection>();
            this.Connection.Setup(mock => mock.CreateModel())
                .Returns(this.Channel.Object);
            var mockConnectionFactory = new Mock<IConnectionFactory>();
            mockConnectionFactory.Setup(mock => mock.CreateConnection())
                .Returns(this.Connection.Object);
            var logger = new Mock<ILogger<RabbitHealthCheck>>();
            this.HealthCheck = new RabbitHealthCheck(mockConnectionFactory.Object, logger.Object);
        }

        #endregion Initialisation

        #region Tests

        #region Constructor Tests

        /// <summary>
        /// Tests that the RabbitHealthCheck constructor will throw an ArgumentNullException if a null value is passed for the messagingClient parameter
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "connectionFactory", true)]
        public void RabbitHealthCheck_Constructor_Throws_ArgumentNullException_If_MessagingClient_Parameter_Is_Null()
        {
            //arrange
            var mockLogger = new Mock<ILogger<RabbitHealthCheck>>();

            //act
            new RabbitHealthCheck(null, mockLogger.Object);

            //assert
        }

        /// <summary>
        /// Tests that the RabbitHealthCheck constructor will throw an ArgumentNullException if a null value is passed for the logger parameter
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "logger", true)]
        public void RabbitHealthCheck_Constructor_Throws_ArgumentNullException_If_Logger_Parameter_Is_Null()
        {
            //arrange
            var mockConnectionFactory = new Mock<IConnectionFactory>();

            //act
            new RabbitHealthCheck(mockConnectionFactory.Object, null);

            //assert
        }

        #endregion Constructor Tests

        #region CheckHealthAsync Tests

        /// <summary>
        /// Verify that the Health Check returns a Healthy status when RabbitMQ is running normally
        /// </summary>
        [TestMethod]
        public async Task RabbitHealthCheck_CheckHealthAsync_Returns_Healthy_When_RabbitMQ_Is_Responding()
        {
            //arrange
            this.Channel.Setup(mock => mock.QueueDeclare(It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<IDictionary<string, object>>()))
                .Returns(new QueueDeclareOk("test", 0, 0));
            var context = new HealthCheckContext();

            //act
            var result = await this.HealthCheck.CheckHealthAsync(context);

            //assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(HealthStatus.Healthy, result.Status);
        }

        /// <summary>
        /// Verify that the Health Check returns a Unhealthy status when RabbitMQ is running slower than expected
        /// </summary>
        [TestMethod]
        public async Task RabbitHealthCheck_CheckHealthAsync_Returns_Unhealthy_When_RabbitMQ_Is_Not_Responding()
        {
            //arrange
            this.Connection.Setup(mock => mock.CreateModel())
                .Returns(() => { Thread.Sleep(ConnectionTimeoutMilliseconds * 2); return new Mock<IModel>().Object; });

            var context = new HealthCheckContext();
            var source = new CancellationTokenSource(ConnectionTimeoutMilliseconds);

            //act
            var result = await this.HealthCheck.CheckHealthAsync(context, source.Token);

            //assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(HealthStatus.Unhealthy, result.Status);
        }

        /// <summary>
        /// Verify that the Health Check returns a Unhealthy status when RabbitMQ cannot be contacted
        /// </summary>
        [TestMethod]
        public async Task RabbitHealthCheck_CheckHealthAsync_Returns_Unhealthy_When_RabbitMQ_Is_Not_Available()
        {
            //arrange
            this.Connection.Setup(mock => mock.CreateModel())
               .Throws(new Exception("test exception"));

            var context = new HealthCheckContext();

            //act
            var result = await this.HealthCheck.CheckHealthAsync(context);

            //assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(HealthStatus.Unhealthy, result.Status);
        }

        #endregion CheckHealthAsync Tests

        #endregion Tests
    }
}
