﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.HealthChecks.Common;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace DfT.GOS.HealthChecks.Common.UnitTest
{
    /// <summary>
    /// Unit tests for the StartupHostedServiceHealthCheck class
    /// </summary>
    [TestClass]
    public class BackgroundServiceHealthCheckTest
    {
        #region Tests

        #region CheckHealthAsync

        /// <summary>
        /// Tests that the CheckHealthAsync returns a Unhealthy status if startup processing has not completed
        /// </summary>
        [TestMethod]
        public async Task StartupHostedServiceHealthCheck_CheckHealthAsync_Returns_Unhealthy_When_Startup_Processing_Not_Complete()
        {
            //arrange
            var healthCheck = new BackgroundServiceHealthCheck();
            var context = new HealthCheckContext();

            //act
            var result = await healthCheck.CheckHealthAsync(context);

            //assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(HealthStatus.Unhealthy, result.Status);
        }

        /// <summary>
        /// Tests that the CheckHealthAsync returns a Healthy status once startup processing has completed
        /// </summary>
        [TestMethod]
        public async Task StartupHostedServiceHealthCheck_CheckHealthAsync_Returns_Healthy_When_Startup_Processing_Complete()
        {
            //arrange
            var healthCheck = new BackgroundServiceHealthCheck
            {
                StartupTaskCompleted = true
            };
            var context = new HealthCheckContext();

            //act
            var result = await healthCheck.CheckHealthAsync(context);

            //assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(HealthStatus.Healthy, result.Status);
        }

        #endregion CheckHealthAsync

        #endregion Tests
    }
}
