// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.HealthChecks.Mongo;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Driver;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.HealthChecks.UnitTest.Mongo
{
    /// <summary>
    /// Unit tests for the MongoHealthCheck class
    /// </summary>
    [TestClass]
    public class MongoHealthCheckTest
    {
        #region Constants

        private const int ConnectionTimeoutMilliseconds = 500;

        #endregion Constants

        #region Properties

        private IHealthCheck HealthCheck { get; set; }
        private Mock<IMongoClient> MongoClient { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialisation required for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.MongoClient = new Mock<IMongoClient>();
            var options = new MongoHealthCheckOptions("testDB", ConnectionTimeoutMilliseconds);
            var mockLogger = new Mock<ILogger<MongoHealthCheck>>();
            this.HealthCheck = new MongoHealthCheck(this.MongoClient.Object, options, mockLogger.Object);
        }

        #endregion Initialisation

        #region Tests

        #region CheckHealthAsync Tests

        /// <summary>
        /// Verify that the Health Check returns a Healthy status when Mongo is running normally
        /// </summary>
        [TestMethod]
        public async Task MongoHealthCheck_CheckHealthAsync_Returns_Healthy_When_Mongo_Is_Responding()
        {
            //arrange
            var mockDatabase = new Mock<IMongoDatabase>();
            mockDatabase.Setup(mock => mock.RunCommandAsync(It.IsAny<Command<BsonDocument>>(), It.IsAny<ReadPreference>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new BsonDocument(), TimeSpan.FromMilliseconds(ConnectionTimeoutMilliseconds * 0.5));
            this.MongoClient.Setup(mock => mock.GetDatabase(It.IsAny<string>(), It.IsAny<MongoDatabaseSettings>()))
                .Returns(mockDatabase.Object);
            var context = new HealthCheckContext();

            //act
            var result = await this.HealthCheck.CheckHealthAsync(context);

            //assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(HealthStatus.Healthy, result.Status);
        }

        /// <summary>
        /// Verify that the Health Check returns a Unhealthy status when Mongo is running slower than expected
        /// </summary>
        [TestMethod]
        public async Task MongoHealthCheck_CheckHealthAsync_Returns_Unhealthy_When_Mongo_Is_Not_Responding()
        {
            //arrange
            var mockDatabase = new Mock<IMongoDatabase>();
            mockDatabase.Setup(mock => mock.RunCommandAsync(It.IsAny<Command<BsonDocument>>(), It.IsAny<ReadPreference>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new BsonDocument(), TimeSpan.FromMilliseconds(ConnectionTimeoutMilliseconds * 1.5));
            this.MongoClient.Setup(mock => mock.GetDatabase(It.IsAny<string>(), It.IsAny<MongoDatabaseSettings>()))
                .Returns(mockDatabase.Object);
            var context = new HealthCheckContext();

            //act
            var result = await this.HealthCheck.CheckHealthAsync(context);

            //assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(HealthStatus.Unhealthy, result.Status);
        }

        /// <summary>
        /// Verify that the Health Check returns a Unhealthy status when Mongo cannot be contacted
        /// </summary>
        [TestMethod]
        public async Task MongoHealthCheck_CheckHealthAsync_Returns_Unhealthy_When_Mongo_Is_Not_Available()
        {
            //arrange
            var mockDatabase = new Mock<IMongoDatabase>();
            mockDatabase.Setup(mock => mock.RunCommandAsync(It.IsAny<Command<BsonDocument>>(), It.IsAny<ReadPreference>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception("test exception"));
            this.MongoClient.Setup(mock => mock.GetDatabase(It.IsAny<string>(), It.IsAny<MongoDatabaseSettings>()))
                .Returns(mockDatabase.Object);
            var context = new HealthCheckContext();

            //act
            var result = await this.HealthCheck.CheckHealthAsync(context);

            //assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(HealthStatus.Unhealthy, result.Status);
        }

        #endregion CheckHealthAsync Tests

        #endregion Tests
    }
}
