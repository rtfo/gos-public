// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Messaging.Client;
using DfT.GOS.Messaging.Messages;
using DfT.GOS.Messaging.Models;
using DfT.GOS.UnitTest.Common.Attributes;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Messaging.Test
{
    /// <summary>
    /// Unit tests for the RabbitMQ implementation of the GHG Balance Message Queue
    /// </summary>
    [TestClass]
    public class RabbitMessagingClientTest
    {
        #region Constants

        private const string ExpectedQueueName= "DfT.GOS.Messaging.Messages.RosUpdatedMessage";

        #endregion Constants

        #region Properties

        private IMessagingClient MessagingClient { get; set; }
        private Mock<IConnectionFactory> MockConnectionFactory { get; set; }
        private Mock<IConnection> MockConnection { get; set; }
        private Mock<IModel> MockChannel { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Initialisation performed for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.MockChannel = new Mock<IModel>();
            this.MockConnection = new Mock<IConnection>();
            this.MockConnection.Setup(mock => mock.CreateModel())
                .Returns(this.MockChannel.Object);
            this.MockConnection.Setup(mock => mock.IsOpen)
                .Returns(true);
            this.MockConnectionFactory = new Mock<IConnectionFactory>();
            this.MockConnectionFactory.Setup(mock => mock.CreateConnection())
                .Returns(this.MockConnection.Object);
            var mockLogger = new Mock<ILogger<RabbitMessagingClient>>();
            this.MessagingClient = new RabbitMessagingClient(this.MockConnectionFactory.Object, mockLogger.Object);
        }

        #endregion Initialisation

        #region Tests

        #region Constructor Tests

        /// <summary>
        /// Tests that the RabbitMessagingClient constructor throws an exception if the connectionFactory parameter is null
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "connectionFactory", true)]
        public void RabbitMessagingClient_Constructor_Throws_ArgumentNullException_For_ConnectionFactory_Parameter()
        {
            //arrange
            var connectionFactory = (IConnectionFactory)null;
            var mockLogger = new Mock<ILogger<RabbitMessagingClient>>();

            //act
            var result = new RabbitMessagingClient(connectionFactory, mockLogger.Object);

            //assert
        }

        /// <summary>
        /// Tests that the RabbitMessagingClient constructor throws an exception if the logger parameter is null
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "logger", true)]
        public void RabbitMessagingClient_Constructor_Throws_ArgumentNullException_For_Logger_Parameter()
        {
            //arrange
            var mockConnectionFactory = new Mock<IConnectionFactory>();
            var logger = (ILogger<RabbitMessagingClient>)null;

            //act
            var result = new RabbitMessagingClient(mockConnectionFactory.Object, logger);

            //assert
        }

        /// <summary>
        /// Tests that the RabbitMessagingClient constructor throws an exception if the settings parameter is null
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "settings", true)]
        public void RabbitMessagingClient_Constructor_Throws_ArgumentNullException_For_Settings_Parameter()
        {
            //arrange
            var settings = (IRabbitMessagingClientSettings)null;
            var mockLogger = new Mock<ILogger<RabbitMessagingClient>>();

            //act
            var result = new RabbitMessagingClient(settings, mockLogger.Object);

            //assert
        }

        /// <summary>
        /// Tests that the RabbitMessagingClient constructor throws an exception if the logger parameter is null
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "logger", true)]
        public void RabbitMessagingClient_Constructor_Throws_ArgumentNullException_For_Logger_Parameter2()
        {
            //arrange
            var mockSettings = new Mock<IRabbitMessagingClientSettings>();
            var logger = (ILogger<RabbitMessagingClient>)null;

            //act
            var result = new RabbitMessagingClient(mockSettings.Object, logger);

            //assert
        }

        #endregion Constructor Tests

        #region BuildConnectionFactory Tests

        /// <summary>
        /// Tests that the BuildConnectionFactory method throws an exception if the settings parameter is null
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "settings", true)]
        public void RabbitMessagingClient_BuildConnectionFactory_Throws_ArgumentNullException()
        {
            //arrange

            //act
            var result = RabbitMessagingClient.BuildConnectionFactory(null);

            //assert
        }

        /// <summary>
        /// Tests that the BuildConnectionFactory method correctly populates a ConnectionFactory instance
        /// </summary>
        [TestMethod]
        public void RabbitMessagingClient_BuildConnectionFactory_Builds_ConnectionFactory()
        {
            //arrange
            var settings = new RabbitMessagingClientSettings()
            {
                HostName = "test.triad.co.uk",
                Port = 12345,
                UserName = "TestUser",
                Password = "TestPassword"
            };

            //act
            var result = RabbitMessagingClient.BuildConnectionFactory(settings);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ConnectionFactory));
            Assert.AreEqual(settings.HostName, ((ConnectionFactory)result).HostName);
            Assert.AreEqual(settings.Port, ((ConnectionFactory)result).Port);
            Assert.AreEqual(settings.UserName, result.UserName);
            Assert.AreEqual(settings.Password, result.Password);
        }

        #endregion BuildConnectionFactory Tests

        #region Publish Tests

        /// <summary>
        /// Tests that the Publish method publishes a message to the queue
        /// </summary>
        [TestMethod]
        public async Task RabbitMessagingClient_Publish_Sends_Message_Successfully()
        {
            //arrange
            var basicProperties = new Mock<IBasicProperties>();
            this.MockChannel.Setup(mock => mock.CreateBasicProperties())
                .Returns(basicProperties.Object);
            var message = new RosUpdatedMessage();

            //act
            await this.MessagingClient.Publish(message);

            //assert
            this.MockChannel.Verify(mock => mock.QueueDeclare(ExpectedQueueName, true, false, false, It.IsAny<IDictionary<string, object>>()), Times.Once);
            this.MockChannel.Verify(mock => mock.BasicPublish(string.Empty, ExpectedQueueName, false, It.IsAny<IBasicProperties>(), It.IsAny<byte[]>()), Times.Once);
        }

        #endregion Publish Tests

        #region Subscribe Tests

        /// <summary>
        /// Tests that the Subscribe method configures a consumer to receive messages from the queue
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RabbitMessagingClient_Subscribe_Retrieves_Message_Successfully()
        {
            //arrange
            var basicProperties = new Mock<IBasicProperties>();
            this.MockChannel.Setup(mock => mock.CreateBasicProperties())
                .Returns(basicProperties.Object);
            var message = new RosUpdatedMessage();
            await this.MessagingClient.Subscribe<RosUpdatedMessage>(m => Task.FromResult(true));

            //act
            await this.MessagingClient.Publish(message);

            //assert
            this.MockChannel.Verify(mock => mock.QueueDeclare(ExpectedQueueName, true, false, false, It.IsAny<IDictionary<string, object>>()), Times.Exactly(2));
            this.MockChannel.Verify(mock => mock.BasicPublish(string.Empty, ExpectedQueueName, false, It.IsAny<IBasicProperties>(), It.IsAny<byte[]>()), Times.Once);
        }

        #endregion Subscribe Tests

        #region Dispose Tests

        /// <summary>
        /// Tests that the Dispose method clears-up non-managed components
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RabbitMessagingClient_Dispose_Releases_Connection_And_Channel()
        {
            //arrange
            var basicProperties = new Mock<IBasicProperties>();
            this.MockChannel.Setup(mock => mock.CreateBasicProperties())
                .Returns(basicProperties.Object);
            var message = new RosUpdatedMessage();
            await this.MessagingClient.Publish(message);

            //act
            ((IDisposable)this.MessagingClient).Dispose();

            //assert
            this.MockConnection.Verify(mock => mock.Dispose(), Times.Once);
            this.MockChannel.Verify(mock => mock.Dispose(), Times.Once);
        }

        #endregion Dispose Tests

        #endregion Tests
    }
}
