﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Messaging.Client;
using DfT.GOS.UnitTest.Common.Attributes;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RabbitMQ.Client;
using System;
using System.Threading.Tasks;

namespace DfT.GOS.Messaging.Test
{
    /// <summary>
    /// Unit tests for the GosBasicConsumer class
    /// </summary>
    [TestClass]
    public class GosBasicConsumerTest
    {
        #region Tests

        #region Constructor Tests

        /// <summary>
        /// Tests that the GosBasicConsumer constructor throws an exception if the subscriberAsync parameter is null
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "subscriberAsync", true)]
        public void GosBasicConsumer_Constructor_Throws_ArgumentNullException_For_SubscriberAsync_Parameter()
        {
            //arrange
            var mockLogger = new Mock<ILogger<RabbitMessagingClient>>();

            //act
            var result = new GosBasicConsumer<string>(null, mockLogger.Object);

            //assert
        }

        /// <summary>
        /// Tests that the GosBasicConsumer constructor throws an exception if the logger parameter is null
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "logger", true)]
        public void GosBasicConsumer_Constructor_Throws_ArgumentNullException_For_Logger_Parameter()
        {
            //arrange

            //act
            var result = new GosBasicConsumer<string>(s => Task.FromResult(true), null);

            //assert
        }

        #endregion Constructor Tests

        /// <summary>
        /// Tests that the HandleBasicDeliver method handles a message with a null body
        /// </summary>
        [TestMethod]
        public async Task GosBasicConsumer_HandleBasicDeliver_Handles_Null_Body()
        {
            //arrange
            string consumerTag = null;
            var deliveryTag = 0ul;
            var redelivered = true;
            string exchange = null;
            string routingKey = null;
            IBasicProperties properties = null;
            byte[] body = null;
            var mockLogger = new Mock<ILogger>();
            var consumer = new GosBasicConsumer<string>(s => Task.FromResult(true), mockLogger.Object);

            //act
            await consumer.HandleBasicDeliver(consumerTag, deliveryTag, redelivered, exchange, routingKey, properties, body);

            //assert
        }

        /// <summary>
        /// Tests that the HandleBasicDeliver method handles an exception in the subscriber code
        /// </summary>
        [TestMethod]
        public async Task GosBasicConsumer_HandleBasicDeliver_Handles_Error_In_Subscriber_Delegate()
        {
            //arrange
            string consumerTag = null;
            var deliveryTag = 0ul;
            var redelivered = true;
            string exchange = null;
            string routingKey = null;
            IBasicProperties properties = null;
            byte[] body = new byte[0];
            var mockLogger = new Mock<ILogger>();
            var consumer = new GosBasicConsumer<string>(s => Task.FromException<bool>(new ArithmeticException()), mockLogger.Object);

            //act
            await consumer.HandleBasicDeliver(consumerTag, deliveryTag, redelivered, exchange, routingKey, properties, body);

            //assert
        }

        /// <summary>
        /// Tests that the HandleBasicDeliver method handles an exception when deserialising the message (body is empty)
        /// </summary>
        [TestMethod]
        public async Task GosBasicConsumer_HandleBasicDeliver_Handles_Error_In_Message_Deserialisation_Empty_Body()
        {
            //arrange
            string consumerTag = null;
            var deliveryTag = 0ul;
            var redelivered = true;
            string exchange = null;
            string routingKey = null;
            IBasicProperties properties = null;
            byte[] body = new byte[0];
            var mockLogger = new Mock<ILogger>();
            var consumer = new GosBasicConsumer<DateTime>(s => Task.FromResult(true), mockLogger.Object);

            //act
            await consumer.HandleBasicDeliver(consumerTag, deliveryTag, redelivered, exchange, routingKey, properties, body);

            //assert
        }

        /// <summary>
        /// Tests that the HandleBasicDeliver method handles an exception when deserialising the message (body is incorrect)
        /// </summary>
        [TestMethod]
        public async Task GosBasicConsumer_HandleBasicDeliver_Handles_Error_In_Message_Deserialisation_Incorrect_Body()
        {
            //arrange
            string consumerTag = null;
            var deliveryTag = 0ul;
            var redelivered = true;
            string exchange = null;
            string routingKey = null;
            IBasicProperties properties = null;
            byte[] body = new byte[] { 1, 2, 3 };
            var mockLogger = new Mock<ILogger>();
            var consumer = new GosBasicConsumer<DateTime>(s => Task.FromResult(true), mockLogger.Object);

            //act
            await consumer.HandleBasicDeliver(consumerTag, deliveryTag, redelivered, exchange, routingKey, properties, body);

            //assert
        }

        #endregion Tests
    }
}
