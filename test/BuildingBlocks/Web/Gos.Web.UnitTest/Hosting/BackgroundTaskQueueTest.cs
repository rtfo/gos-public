﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.Gos.Web.UnitTest.Hosting
{
    /// <summary>
    /// Unit tests for the BackgroundTaskQueue class
    /// </summary>
    [TestClass]
    public class BackgroundTaskQueueTest
    {
        #region Properties

        private IBackgroundTaskQueue BackgroundTaskQueue { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialization for unit tests
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.BackgroundTaskQueue = new BackgroundTaskQueue();
        }

        #endregion Initialisation

        #region Tests

        #region QueueBackgroundWorkItem Tests

        /// <summary>
        /// Tests that the QueueBackgroundWorkItem method queues a Task
        /// </summary>
        [TestMethod]
        public void BackgroundTaskQueue_QueueBackgroundWorkItem_Queues_Task()
        {
            //arrange
            var task = Task.CompletedTask;

            //act
            this.BackgroundTaskQueue.QueueBackgroundWorkItem(token => task);

            //assert
        }

        #endregion QueueBackgroundWorktItem Tests

        #region QueueBackgroundWorkItem Tests

        /// <summary>
        /// Tests that the DequeueAsync method retrieves a Task from the queue
        /// </summary>
        [TestMethod]
        public async Task BackgroundTaskQueue_DequeueAsync_Dequeues_Task()
        {
            //arrange
            Func<CancellationToken, Task> func = token => Task.CompletedTask;
            this.BackgroundTaskQueue.QueueBackgroundWorkItem(func);

            //act
            var result = await this.BackgroundTaskQueue.DequeueAsync(new CancellationToken());

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(func, result);
        }

        /// <summary>
        /// Tests that the DequeueAsync method obeys the FIFO rule
        /// </summary>
        [TestMethod]
        public async Task BackgroundTaskQueue_DequeueAsync_Obeys_FIFO()
        {
            //arrange
            Func<CancellationToken, Task> func1 = token => Task.CompletedTask;
            this.BackgroundTaskQueue.QueueBackgroundWorkItem(func1);
            Func<CancellationToken, Task> func2 = token => Task.CompletedTask;
            this.BackgroundTaskQueue.QueueBackgroundWorkItem(func2);
            Func<CancellationToken, Task> func3 = token => Task.CompletedTask;
            this.BackgroundTaskQueue.QueueBackgroundWorkItem(func3);

            //act
            var result = await this.BackgroundTaskQueue.DequeueAsync(new CancellationToken());

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(func1, result);

            //act
            result = await this.BackgroundTaskQueue.DequeueAsync(new CancellationToken());

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(func2, result);

            //act
            result = await this.BackgroundTaskQueue.DequeueAsync(new CancellationToken());

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(func3, result);
        }

        #endregion QueueBackgroundWorktItem Tests

        #endregion Tests
    }
}
