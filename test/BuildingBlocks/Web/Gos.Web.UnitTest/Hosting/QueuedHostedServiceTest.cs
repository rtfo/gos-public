﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.Gos.Web.UnitTest.Hosting
{
    [TestClass]
    public class QueuedHostedServiceTest
    {
        #region Properties

        private QueuedHostedService QueuedHostedService { get; set; }
        private Mock<IBackgroundTaskQueue> BackgroundTaskQueue { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialisation for tests
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.BackgroundTaskQueue = new Mock<IBackgroundTaskQueue>();
            var mockLogger = new Mock<ILogger<QueuedHostedService>>();
            this.QueuedHostedService = new QueuedHostedService(this.BackgroundTaskQueue.Object, mockLogger.Object);
        }

        #endregion Initialisation

        #region Tests

        #region ExecuteAsync Tests

        /// <summary>
        /// Tests that the ExecuteAsync method purges expired collections
        /// </summary>
        [TestMethod]
        public void BalanceCollectionAdministrationBackgroundService_ExecuteAsync_Runs_Task_On_Queue()
        {
            //arrange
            this.BackgroundTaskQueue.Setup(mock => mock.DequeueAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(token => Task.CompletedTask)
                .Verifiable();

            //act
            var backgroundTask = Task.Run(() => this.QueuedHostedService.StartAsync(new CancellationToken(false)));
            Thread.Sleep(500);
            this.QueuedHostedService.StopAsync(new CancellationToken(true));

            //assert
            this.BackgroundTaskQueue.Verify();
            Assert.IsNull(backgroundTask.Exception);
        }

        /// <summary>
        /// Tests that the ExecuteAsync method handles exceptions that occur when running a Task on the Queue
        /// </summary>
        [TestMethod]
        public void BalanceCollectionAdministrationBackgroundService_ExecuteAsync_Handles_Exceptions_Whilst_Running_A_Task_On_Queue()
        {
            //arrange
            this.BackgroundTaskQueue.Setup(mock => mock.DequeueAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(token => Task.FromException(new Exception("test exception")))
                .Verifiable();

            //act
            var backgroundTask = Task.Run(() => this.QueuedHostedService.StartAsync(new CancellationToken(false)));
            Thread.Sleep(500);
            this.QueuedHostedService.StopAsync(new CancellationToken(true));

            //assert
            this.BackgroundTaskQueue.Verify();
            Assert.IsNull(backgroundTask.Exception);
        }

        /// <summary>
        /// Tests that the ExecuteAsync method handles exceptions that occur when retrieving a Task from the Queue
        /// </summary>
        [TestMethod]
        public void BalanceCollectionAdministrationBackgroundService_ExecuteAsync_Handles_Exceptions_Whilst_Dequeuing_A_Task()
        {
            //arrange
            this.BackgroundTaskQueue.Setup(mock => mock.DequeueAsync(It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception("test exception"))
                .Verifiable();

            //act
            var backgroundTask = Task.Run(() => this.QueuedHostedService.StartAsync(new CancellationToken(false)));
            Thread.Sleep(500);
            this.QueuedHostedService.StopAsync(new CancellationToken(true));

            //assert
            this.BackgroundTaskQueue.Verify();
            Assert.IsNull(backgroundTask.Exception);
        }

        #endregion ExecuteAsync Tests

        #endregion Tests
    }
}