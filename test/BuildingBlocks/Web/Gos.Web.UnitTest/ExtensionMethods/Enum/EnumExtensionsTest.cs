﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.ExtensionMethods.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.DataAnnotations;

namespace DfT.Gos.Web.UnitTest.ExtensionMethods.Enum
{
    /// <summary>
    /// Tests the Enum Extensions
    /// </summary>
    [TestClass]
    public class EnumExtensionsTest
    {
        private enum TestEnum
        {
            [Display(Name = "Test One")]
            Test1 = 1,
            Test2 = 2
        }

        /// <summary>
        /// Tests the enum GetDescription method return the display name if the arribute exists
        /// </summary>
        [TestMethod]
        public void EnumExtensions_GetDescription_Display_Attribute_Exists()
        {
            //arrange
            var testEnum = TestEnum.Test1;

            //act
            var result = testEnum.GetDisplayName();

            //assert
            Assert.AreEqual("Test One", result);
        }


        /// <summary>
        /// Tests the enum GetDescription method return the display name if the arribute exists
        /// </summary>
        [TestMethod]
        public void EnumExtensions_GetDescription_Display_Attribute_Not_Exists()
        {
            //arrange
            var testEnum = TestEnum.Test2;

            //act
            var result = testEnum.GetDisplayName();

            //assert
            Assert.AreEqual("Test2", result);
        }
    }
}
