﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DfT.Gos.Web.UnitTest.Validation
{
    /// <summary>
    /// Unit tests for the Gos Password validation attribute
    /// </summary>
    [TestClass]
    public class GosPasswordAttributeTest
    {
        #region Tests Methods

        /// <summary>
        /// Tests a combination of valid and invalid passwords
        /// </summary>
        [TestMethod]
        public void GosPasswordAttributeTest_Tests()
        {
            //arrange
            var validator = new GosPasswordAttribute();

            //act / assert

            // - valid passwords
            Assert.IsTrue(validator.IsValid("P@ssw0rd"));

            // - test special chars: !@#$%^&*():;'£?_.,
            Assert.IsTrue(validator.IsValid("Password1!"));
            Assert.IsTrue(validator.IsValid("Password1@"));
            Assert.IsTrue(validator.IsValid("Password1#"));
            Assert.IsTrue(validator.IsValid("Password1$"));
            Assert.IsTrue(validator.IsValid("Password1%"));
            Assert.IsTrue(validator.IsValid("Password1^"));
            Assert.IsTrue(validator.IsValid("Password1&"));
            Assert.IsTrue(validator.IsValid("Password1*"));
            Assert.IsTrue(validator.IsValid("Password1("));
            Assert.IsTrue(validator.IsValid("Password1)"));
            Assert.IsTrue(validator.IsValid("Password1:"));
            Assert.IsTrue(validator.IsValid("Password1;"));
            Assert.IsTrue(validator.IsValid("Password1'"));
            Assert.IsTrue(validator.IsValid("Password1£"));
            Assert.IsTrue(validator.IsValid("Password1?"));
            Assert.IsTrue(validator.IsValid("Password1_"));
            Assert.IsTrue(validator.IsValid("Password1."));
            Assert.IsTrue(validator.IsValid("Password1,"));

            // - invalid passwords
            Assert.IsFalse(validator.IsValid("password"));
            Assert.IsFalse(validator.IsValid("password1"));
            Assert.IsFalse(validator.IsValid("Password1"));
            Assert.IsFalse(validator.IsValid("Password£"));
            Assert.IsFalse(validator.IsValid("password1£"));
            Assert.IsFalse(validator.IsValid("P@ss1"));
            Assert.IsFalse(validator.IsValid("123456789"));
            Assert.IsFalse(validator.IsValid("123456789a"));
            Assert.IsFalse(validator.IsValid("123456789aA")); 
        }

        #endregion Tests Methods
    }
}
