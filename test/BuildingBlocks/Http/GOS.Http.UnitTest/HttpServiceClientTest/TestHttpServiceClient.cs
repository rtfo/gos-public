﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Net.Http;

namespace DfT.GOS.Http.UnitTest.HttpServiceClientTest
{
    /// <summary>
    /// Test implementation of HttpServiceClient for test purposes
    /// </summary>
    public class TestHttpServiceClient: HttpServiceClient
    {
        #region Constructors

        public TestHttpServiceClient(HttpClient httpClient)
            :base(httpClient)
        {

        }

        #endregion Constructors

        #region Overriden Methods

        /// <summary>
        /// Sets some context information in the request for Polly to pick up and report on if there are connection issues
        /// </summary>
        /// <param name="request">The request to add context information to</param>
        /// <param name="requestUri">The URL of the resource we're requesting</param>
        public new HttpRequestMessage SetPolicyExecutionContext(HttpRequestMessage request, string requestUri)
        {
            return base.SetPolicyExecutionContext(request, requestUri);
        }

        #endregion Overriden Methods
    }
}
