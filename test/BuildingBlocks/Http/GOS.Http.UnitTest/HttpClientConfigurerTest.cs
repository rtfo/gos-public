﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DfT.GOS.Http.UnitTest
{
    /// <summary>
    /// Tests the HttpClientConfigurer class
    /// </summary>
    [TestClass]
    public class HttpClientConfigurerTest
    {
        #region Tests

        #region Configure Tests

        /// <summary>
        /// Tests the Configure method appropriately configures a HttpClientBuilder
        /// </summary>
        [TestMethod]
        public void HttpClientConfigurer_Configure_Configures_HttpClientBuilder()
        {
            //arrange
            var settings = new HttpClientAppSettings()
            {
                HttpClientLifetimeSeconds = 30
            };

            var mockLogger = new Mock<ILogger>();
            var httpClientConfigurer = new HttpClientConfigurer(settings, mockLogger.Object);
            var mockHttpClientBuilder = new Mock<IHttpClientBuilder>();

            var fakeServiceCollection = new ServiceCollection();
            mockHttpClientBuilder.Setup(b => b.Services).Returns(fakeServiceCollection);

            //act
            var configuredHttpClientBuilder = httpClientConfigurer.Configure(mockHttpClientBuilder.Object);

            //assert
            Assert.IsNotNull(configuredHttpClientBuilder);
            // - services were added with the configuration
            Assert.IsTrue(configuredHttpClientBuilder.Services.Count > 0);
        }

        #endregion Configure Tests

        #endregion Tests

    }
}
