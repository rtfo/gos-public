// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http.UnitTest.HttpServiceClientTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Polly;
using System;
using System.Net.Http;
using DfT.GOS.Http;
using Moq;

namespace GOS.Http.UnitTest
{
    /// <summary>
    /// Tests the HttpServiceClient class
    /// </summary>
    [TestClass]
    public class HttpServiceClientTest
    {
        #region Constants

        private const string BaseUri = "http://base";

        #endregion Constants

        #region Private variables

        private Mock<HttpMessageHandler> _mockHandler;
        private HttpClient _httpClient;
        private TestHttpServiceClient _httpServiceClient;

        #endregion Private variables

        #region Methods

        /// <summary>
        /// Initialises objects for test
        /// </summary>
        [TestInitialize]
        public void InitialiseTest()
        {
            _mockHandler = new Mock<HttpMessageHandler>();
            _httpClient = new HttpClient(_mockHandler.Object);            
            _httpClient.BaseAddress = new Uri(BaseUri);
            _httpServiceClient = new TestHttpServiceClient(_httpClient);
        }

        #endregion Methods

        #region Tests

        #region SetPolicyExecutionContext tests

        /// <summary>
        /// Tests that the SetPolicyExecutionContext correctly configures the request
        /// </summary>
        [TestMethod]
        public void HttpServiceClient_SetPolicyExecutionContext_Configures_Context()
        {
            //Arrange                      
            var fakeUri = "fake/uri";
            var request = new HttpRequestMessage(HttpMethod.Post, fakeUri);
            var expectedUri = BaseUri + "/" + fakeUri;
            var expectedRequestMethod = HttpMethod.Post.ToString();

            //Act
            var configuredRequest = _httpServiceClient.SetPolicyExecutionContext(request, fakeUri);
            var context = configuredRequest.GetPolicyExecutionContext();

            //Assert
            // - Assert the Request URI is as expected
            Assert.IsTrue(context.ContainsKey(HttpPolicyContext.RequestUriKey));
            Assert.AreEqual(expectedUri, context[HttpPolicyContext.RequestUriKey]);
            // - Assert the Request Method is as expected
            Assert.IsTrue(context.ContainsKey(HttpPolicyContext.RequestUriKey));
            Assert.AreEqual(expectedRequestMethod, context[HttpPolicyContext.RequestMethod]);
        }

        #endregion SetPolicyExecutionContext tests

        #endregion Tests
    }
}
