﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Polly;
using System;
using System.Linq;
using System.Net.Http;

namespace DfT.GOS.Http.UnitTest
{
    /// <summary>
    /// Unit tests for HttpClientRetryExceptionHandler class
    /// </summary>
    [TestClass]
    public class HttpClientRetryExceptionHandlerTest
    {
        #region Private variables

        private Mock<ILogger> _mockLogger;
        private HttpClientRetryExceptionHandler _exceptionHandler;

        #endregion Private Variables

        #region Methods

        /// <summary>
        /// Sets up each test
        /// </summary>
        [TestInitialize]
        public void InitialiseTest()
        {
            _mockLogger = new Mock<ILogger>();
            _exceptionHandler = new HttpClientRetryExceptionHandler(_mockLogger.Object);
        }

        #endregion Methods

        #region Tests

        /// <summary>
        /// Tests that the HandleException method handles null exceptions
        /// </summary>
        [TestMethod]
        public void HandleException_Handles_Null_Exception()
        {
            //arrange
            Exception ex = null;

            //act
            _exceptionHandler.HandleException(ex, TimeSpan.MinValue, 1, new Polly.Context(), 5);

            //assert
            // - The logger wasn't called
            _mockLogger.Verify(l => l.Log(It.IsAny<LogLevel>(), It.IsAny<EventId>(), It.IsAny<It.IsAnyType>(), It.IsAny<Exception>(),
                It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)), Times.Never);
        }

        /// <summary>
        /// Tests that the HandleException method handles null polly context items
        /// </summary>
        [TestMethod]
        public void HandleException_Handles_Null_ContextItems()
        {
            //arrange
            Exception ex = new Exception("Fake exception");

            //act
            _exceptionHandler.HandleException(ex, TimeSpan.MinValue, 1, new Context(), 5);

            //assert
            // - The logger was called
            _mockLogger.Verify(l => l.Log(LogLevel.Warning, It.IsAny<EventId>(), It.Is<It.IsAnyType>((v, t) => true), It.IsAny<Exception>(),
                It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)), Times.Once);
        }

        /// <summary>
        /// Tests that the HandleException method handles polly context items
        /// </summary>
        [TestMethod]
        public void HandleException_Handles_ContextItems()
        {
            //arrange
            Exception ex = new Exception("Fake exception");
            var fakeUri = "http://fakeApi";
            var context = new Context();
            context.Add(HttpPolicyContext.RequestUriKey, fakeUri);
            context.Add(HttpPolicyContext.RequestMethod, HttpMethod.Post.ToString());

            //act
            _exceptionHandler.HandleException(ex, TimeSpan.MinValue, 1, context, 5);

            //assert
            // - The logger was called
            _mockLogger.Verify(l => l.Log(LogLevel.Warning, It.IsAny<EventId>(), It.Is<It.IsAnyType>((v, p) => true), It.IsAny<Exception>(),
                It.Is<Func<It.IsAnyType, Exception, string>>((v, p) => true)), Times.Once);
            // - The URL was logged
            Assert.AreEqual(1, _mockLogger.Invocations.Count);
            Assert.IsTrue(_mockLogger.Invocations.Any(i => i.Arguments.Any(a => a.ToString().Contains(fakeUri))));
        }

        #endregion Tests
    }
}
