﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text.RegularExpressions;

namespace DfT.GOS.UnitTest.Common.Attributes
{
    /// <summary>
    /// Attribute used to test that a specific exception is thrown with a specific error message during a unit test
    /// </summary>
    public class ExpectedExceptionMessageAttribute : ExpectedExceptionBaseAttribute
    {
        #region Properties

        private Type ExpectedExceptionType { get; set; }
        private string ExpectedExceptionMessage { get; set; }
        private bool UseRegex { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="expectedExceptionType">Type of exception expected to be thrown</param>
        /// <param name="expectedExceptionMessage">Expected exception message</param>
        /// <param name="useRegex">Flag indicating whether to treat the expectedExceptionMessage parameter as a Regular Expression</param>
        public ExpectedExceptionMessageAttribute(Type expectedExceptionType, string expectedExceptionMessage, bool useRegex = false)
        {
            this.ExpectedExceptionType = expectedExceptionType ?? throw new ArgumentNullException("expectedExceptionType");
            this.ExpectedExceptionMessage = expectedExceptionMessage;
            this.UseRegex = useRegex;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Checks the given Exception matches the supplied criteria
        /// </summary>
        /// <param name="exception">Exception to check</param>
        protected override void Verify(Exception exception)
        {
            Assert.IsNotNull(exception);
            Assert.AreEqual(this.ExpectedExceptionType, exception.GetType());

            if (this.UseRegex)
            {
                Assert.IsTrue(new Regex(this.ExpectedExceptionMessage).IsMatch(exception.Message), $"Expected exception message to match '{this.ExpectedExceptionMessage}' but was '{exception.Message}'.");
            }
            else
            {
                Assert.AreEqual(this.ExpectedExceptionMessage, exception.Message, $"Expected exception message '{this.ExpectedExceptionMessage}' but was '{exception.Message}'.");
            }
        }

        #endregion Methods
    }
}
