// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace DfT.GOS.GhgMarketplace.API.UnitTest
{
    /// <summary>
    /// Tests the Startup class
    /// </summary>
    [TestClass]
    public class StartupTest
    {
        #region Tests
        [TestMethod]
        public void Startup_Validate_Connection_String_Is_Present()
        {
            //arrange

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_ConnectionString);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_ConnectionStringNotSupplied, ex.Message);

        }

        /// <summary>
        /// Tests that the JwtTokenIssuer variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_JwtTokenIssuer_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_JwtTokenIssuer);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_JwtTokenIssuer, ex.Message);
        }

        /// <summary>
        /// Tests that the JwtTokenKey variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_JwtTokenKey_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_JwtTokenKey);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_JwtTokenKey, ex.Message);
        }
        #endregion Tests

        #region Private Methods

        /// <summary>
        /// Gets a dictionary of all configuration settings for this service
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetAllConfigurationItems()
        {
            var configurationItems = new Dictionary<string, string>();
            configurationItems.Add(Startup.EnvironmentVariable_ConnectionString, "fake connection string");
            configurationItems.Add(Startup.EnvironmentVariable_JwtTokenAudience, "fake JwtTokenAudience");
            configurationItems.Add(Startup.EnvironmentVariable_JwtTokenIssuer, "fake JwtTokenIssuer");
            configurationItems.Add(Startup.EnvironmentVariable_JwtTokenKey, "fake JwtTokenKey");
            return configurationItems;
        }

        /// <summary>
        /// Gets an instance of the startup class for testing
        /// </summary>
        /// <returns></returns>
        private Startup GetStartupInstance(IDictionary<string, string> configurationItems)
        {
            var configuration = new ConfigurationBuilder();
            var mockConfiguration = new Mock<IConfiguration>();

            foreach (var configurationItem in configurationItems)
            {
                mockConfiguration.SetupGet(c => c[configurationItem.Key]).Returns(configurationItem.Value);
            }

            var mockLogger = new Mock<ILogger<Startup>>();
            return new Startup(mockConfiguration.Object, mockLogger.Object);
        }

        #endregion Private Methods
    }
}
