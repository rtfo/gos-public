﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.API.Controllers;
using DfT.GOS.GhgMarketplace.API.Data;
using DfT.GOS.GhgMarketplace.API.Repositories;
using DfT.GOS.GhgMarketplace.API.Services;
using DfT.GOS.GhgMarketplace.Common.Commands;
using DfT.GOS.GhgMarketplace.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.GhgMarketplace.API.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the Marketplace controller class
    /// </summary>
    [TestClass]
    public class MarketplaceControllerTest
    {
        #region Constants

        private const int FakeObligationPeriodId = 15;
        private const int UserOrganisationId = 1;

        #endregion Constants

        #region Properties

        private MarketplaceController Controller { get; set; }
        private Mock<IReportingPeriodsService> ReportingPeriodsService { get; set; }
        private Mock<IOrganisationsService> OrganisationsService { get; set; }
        private Mock<IMarketplaceService> MarketplaceService { get; set; }
        private Mock<ILogger<MarketplaceController>> Logger { get; set; }

        #endregion Properties

        #region Initialise

        /// <summary>
        /// Initializes the objects used for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.MarketplaceService = new Mock<IMarketplaceService>();

            //Setup a fake obligation period
            this.ReportingPeriodsService = new Mock<IReportingPeriodsService>();

            var fakeObligationPeriod = new ObligationPeriod(FakeObligationPeriodId,
                It.IsAny<DateTime>(), new DateTime(2019, 05, 01), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>());

            this.ReportingPeriodsService.Setup(s => s.GetObligationPeriod(FakeObligationPeriodId)).ReturnsAsync(fakeObligationPeriod);

            this.OrganisationsService = new Mock<IOrganisationsService>();
            this.OrganisationsService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(1, "Org 1", 1, "Status", 1, "supplier")));
            this.Logger = new Mock<ILogger<MarketplaceController>>();

            this.Controller = new MarketplaceController(this.MarketplaceService.Object
                , this.ReportingPeriodsService.Object
                , this.OrganisationsService.Object
                , this.Logger.Object);

            this.SetUserPrincipal();
        }

        #endregion Initialise

        #region GetMarketplaceAdverts Tests

        /// <summary>
        /// Tests that the Marketplace controller handles a non-existant obligation period
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task MarketplaceController_GetMarketplaceAdverts_Handles_Not_Found_ObligationPeriod()
        {
            //arrange
            var invalidObligationPeriodId = 1;

            //act
            var result = await this.Controller.GetMarketplaceAdverts(invalidObligationPeriodId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the Marketplace controller happy path
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task MarketplaceController_GetMarketplaceAdverts_Returns_Result()
        {
            //arrange           
            var fakeMarketplaceAdverts = new MarketplaceAdverts(FakeObligationPeriodId, new List<MarketplaceAdvert>(), new List<MarketplaceAdvert>());
            this.MarketplaceService.Setup(s => s.GetMarketplaceAdverts(FakeObligationPeriodId)).ReturnsAsync(fakeMarketplaceAdverts);

            //act
            var result = await this.Controller.GetMarketplaceAdverts(FakeObligationPeriodId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(MarketplaceAdverts));
        }

        #endregion GetMarketplaceAdverts Tests

        #region GetMarketplaceAdvert Tests

        ///<summary>
        /// Only users for the given organisation can access that organisations advert options
        ///</summary>
        [TestMethod]
        public async Task GetMarketplaceAdvert_Returns_Forbid_When_User_Belongs_To_Different_Organisation()
        {
            // Arrange 
            var organisationId = 2;
            var obligationPeriod = 1;

            // Act
            var result = await this.Controller.GetMarketplaceAdvert(organisationId, obligationPeriod);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Marketplace controller handles a non-existant obligation period for GetMarketplaceAdvert
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetMarketplaceAdvert_Handles_Not_Found_ObligationPeriod()
        {
            //arrange
            var invalidObligationPeriodId = 1;
            var organisationId = 1;

            //act
            var result = await this.Controller.GetMarketplaceAdvert(invalidObligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the Marketplace controller handles a non-existant organisation for GetMarketplaceAdvert
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetMarketplaceAdvert_Handles_Not_Found_Organisation()
        {
            //arrange
            var organisationId = 1;
            this.OrganisationsService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(null));

            //act
            var result = await this.Controller.GetMarketplaceAdvert(FakeObligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        ///<summary>
        /// GetMarketplaceAdvert returns the organisations advert settings for that obligation period
        ///</summary>
        [TestMethod]
        public async Task GetMarketplaceAdvert_Returns_Expected_Advert()
        {
            // Arrange 
            var organisationId = 1;
            var expectedAdvert = new MarketplaceAdvertCommand(true, true, null, null, null);

            IActionResult result;
            using (var context = this.GetNewInMemoryContext())
            {
                this.PopulateContext(context, new List<GhgCreditAdvert>
                {
                   new GhgCreditAdvert
                    {
                        OrganisationId = 1,
                        ObligationPeriodId = FakeObligationPeriodId,
                        AdvertiseToBuy = true,
                        AdvertiseToSell = true
                    }
                });

                this.Controller = new MarketplaceController(new MarketplaceService(new MarketplaceRepository(context), new Mock<ILogger<MarketplaceService>>().Object), this.ReportingPeriodsService.Object, this.OrganisationsService.Object, this.Logger.Object);
                this.SetUserPrincipal();

                // Act
                result = await this.Controller.GetMarketplaceAdvert(organisationId, FakeObligationPeriodId);
            }
            // Assert 
            var okObjectResult = result as OkObjectResult;
            Assert.IsNotNull(okObjectResult);

            var marketplaceAdvert = okObjectResult.Value as MarketplaceAdvert;
            Assert.IsNotNull(marketplaceAdvert);

            Assert.AreEqual(expectedAdvert.AdvertiseToBuy, marketplaceAdvert.AdvertiseToBuy);
            Assert.AreEqual(expectedAdvert.AdvertiseToSell, marketplaceAdvert.AdvertiseToSell);
        }


        ///<summary>
        /// GetMarketplaceAdvert handles when the organisation doesn't have one for the obligation period,
        /// it should return a default one with both values set to false
        ///</summary>
        [TestMethod]
        public async Task GetMarketplaceAdvert_Handles_New_Organisation_For_Obligation_Period()
        {
            // Arrange 
            var organisationId = 1;
            var expectedAdvert = new MarketplaceAdvertCommand(false, false, null, null, null);

            IActionResult result;
            using (var context = this.GetNewInMemoryContext())
            {

                this.PopulateContext(context, new List<GhgCreditAdvert>
                {
                   new GhgCreditAdvert
                    {
                        OrganisationId = 2,
                        ObligationPeriodId = FakeObligationPeriodId,
                        AdvertiseToBuy = true,
                        AdvertiseToSell = true
                    },
                   new GhgCreditAdvert
                    {
                        OrganisationId = 1,
                        ObligationPeriodId = FakeObligationPeriodId + 1,
                        AdvertiseToBuy = true,
                        AdvertiseToSell = true
                    }
                });

                this.Controller = new MarketplaceController(new MarketplaceService(new MarketplaceRepository(context), new Mock<ILogger<MarketplaceService>>().Object), this.ReportingPeriodsService.Object, this.OrganisationsService.Object, this.Logger.Object);
                this.SetUserPrincipal();

                // Act
                result = await this.Controller.GetMarketplaceAdvert(organisationId, FakeObligationPeriodId);
            }
            // Assert 
            var okObjectResult = result as OkObjectResult;
            Assert.IsNotNull(okObjectResult);

            var marketplaceAdvert = okObjectResult.Value as MarketplaceAdvert;
            Assert.IsNotNull(marketplaceAdvert);

            Assert.AreEqual(expectedAdvert.AdvertiseToBuy, marketplaceAdvert.AdvertiseToBuy);
            Assert.AreEqual(expectedAdvert.AdvertiseToSell, marketplaceAdvert.AdvertiseToSell);
        }
        #endregion GetMarketplaceAdvert Tests

        #region SetMarketplaceAdvert Tests

        ///<summary>
        /// Only users for the given organisation can set that organisations advert options
        ///</summary>
        [TestMethod]
        public async Task SetMarketplaceAdvert_Returns_Forbid_When_User_Belongs_To_Different_Organisation()
        {
            // Arrange 
            var organisationId = 2;
            var obligationPeriod = 1;
            var advert = new MarketplaceAdvertCommand(true, true, null, null, null);

            // Act
            var result = await this.Controller.SetMarketplaceAdvert(organisationId, obligationPeriod, advert);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Marketplace controller handles a non-existant organisation Id for SetMarketplaceAdvert
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SetMarketplaceAdvert_Handles_Not_Found_Organisation()
        {
            //arrange
            var organisationId = 1;
            var advert = new MarketplaceAdvertCommand(true, true, null, null, null);
            this.OrganisationsService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(null));

            //act
            var result = await this.Controller.SetMarketplaceAdvert(organisationId, FakeObligationPeriodId, advert);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the Marketplace controller handles a non-existant obligation period for SetMarketplaceAdvert
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SetMarketplaceAdvert_Handles_Not_Found_ObligationPeriod()
        {
            //arrange
            var invalidObligationPeriodId = 1;
            var organisationId = 1;
            var advert = new MarketplaceAdvertCommand(true, true, null, null, null);

            //act
            var result = await this.Controller.SetMarketplaceAdvert(organisationId, invalidObligationPeriodId, advert);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the Marketplace controller handles doesn't allow invalid email addresses
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SetMarketplaceAdvert_Returns_Bad_Request_When_Request_Is_Invalid()
        {
            //arrange
            var invalidObligationPeriodId = 1;
            var organisationId = 1;
            var advert = new MarketplaceAdvertCommand(true, true, "User", "notanemail", null);
            this.Controller.ModelState.AddModelError("user", "test error");

            //act
            var result = await this.Controller.SetMarketplaceAdvert(organisationId, invalidObligationPeriodId, advert);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        ///<summary>
        /// Tests that a new advert can be created for an organisation in a given obligation period
        ///</summary>
        [TestMethod]
        public async Task SetMarketplaceAdvert_Creates_A_New_Record_If_None_Exists()
        {
            // Arrange 
            var organisationId = 1;
            var advert = new MarketplaceAdvertCommand(true, true, null, null, null);

            IActionResult result;
            using (var context = this.GetNewInMemoryContext())
            {
                this.Controller = new MarketplaceController(new MarketplaceService(new MarketplaceRepository(context), new Mock<ILogger<MarketplaceService>>().Object), this.ReportingPeriodsService.Object, this.OrganisationsService.Object, this.Logger.Object);
                this.SetUserPrincipal();

                // Act
                result = await this.Controller.SetMarketplaceAdvert(organisationId, FakeObligationPeriodId, advert);

                // Assert 
                Assert.IsInstanceOfType(result, typeof(CreatedResult));
                var record = await context.FindAsync<GhgCreditAdvert>(1, FakeObligationPeriodId);
                Assert.IsTrue(record.AdvertiseToBuy);
                Assert.IsTrue(record.AdvertiseToSell);
                Assert.AreEqual(advert.FullName, record.FullName);
                Assert.AreEqual(advert.EmailAddress, record.EmailAddress);
                Assert.AreEqual(advert.PhoneNumber, record.PhoneNumber);
            }
        }

        ///<summary>
        /// Tests that a new advert can be created for an organisation in a given obligation period
        ///</summary>
        [TestMethod]
        public async Task SetMarketplaceAdvert_Creates_A_New_Record_With_Contact_Details_If_None_Exists()
        {
            // Arrange 
            var organisationId = 1;
            var advert = new MarketplaceAdvertCommand(true, true, "User", "user@triad.co.uk", "01234567891");

            IActionResult result;
            using (var context = this.GetNewInMemoryContext())
            {
                this.Controller = new MarketplaceController(new MarketplaceService(new MarketplaceRepository(context), new Mock<ILogger<MarketplaceService>>().Object), this.ReportingPeriodsService.Object, this.OrganisationsService.Object, this.Logger.Object);
                this.SetUserPrincipal();

                // Act
                result = await this.Controller.SetMarketplaceAdvert(organisationId, FakeObligationPeriodId, advert);

                // Assert 
                Assert.IsInstanceOfType(result, typeof(CreatedResult));
                var record = await context.FindAsync<GhgCreditAdvert>(1, FakeObligationPeriodId);
                Assert.IsTrue(record.AdvertiseToBuy);
                Assert.IsTrue(record.AdvertiseToSell);
                Assert.AreEqual(advert.FullName, record.FullName);
                Assert.AreEqual(advert.EmailAddress, record.EmailAddress);
                Assert.AreEqual(advert.PhoneNumber, record.PhoneNumber);
            }
        }

        ///<summary>
        /// Tests that an existing advert can be updated for an organisation in a given obligation period
        ///</summary>
        [TestMethod]
        public async Task SetMarketplaceAdvert_Updates_A_Record_If_One_Exists()
        {
            // Arrange 
            var organisationId = 1;
            var advert = new MarketplaceAdvertCommand(false, true, "User", null, null);

            IActionResult result;
            using (var context = this.GetNewInMemoryContext())
            {

                this.PopulateContext(context, new List<GhgCreditAdvert>
                {
                   new GhgCreditAdvert
                    {
                        OrganisationId = 1,
                        ObligationPeriodId = FakeObligationPeriodId,
                        AdvertiseToBuy = true,
                        AdvertiseToSell = true
                    },
                   new GhgCreditAdvert
                    {
                        OrganisationId = 1,
                        ObligationPeriodId = FakeObligationPeriodId + 1,
                        AdvertiseToBuy = true,
                        AdvertiseToSell = true
                    }
                });

                this.Controller = new MarketplaceController(new MarketplaceService(new MarketplaceRepository(context), new Mock<ILogger<MarketplaceService>>().Object), this.ReportingPeriodsService.Object, this.OrganisationsService.Object, this.Logger.Object);
                this.SetUserPrincipal();

                // Act
                result = await this.Controller.SetMarketplaceAdvert(organisationId, FakeObligationPeriodId, advert);

                // Assert 
                Assert.IsInstanceOfType(result, typeof(OkResult));
                var record = await context.FindAsync<GhgCreditAdvert>(1, FakeObligationPeriodId);
                Assert.IsFalse(record.AdvertiseToBuy);
                Assert.IsTrue(record.AdvertiseToSell);
                Assert.AreEqual(advert.FullName, record.FullName);
            }
        }

        ///<summary>
        /// Tests that an if the buy and sell options are both false the contact details are cleared
        ///</summary>
        [TestMethod]
        public async Task SetMarketplaceAdvert_Removes_Contact_Details_When_Not_Advertising()
        {
            // Arrange 
            var organisationId = 1;
            var advert = new MarketplaceAdvertCommand(false, false, "User", "email@email.com", "01293456789");

            IActionResult result;
            using (var context = this.GetNewInMemoryContext())
            {

                this.PopulateContext(context, new List<GhgCreditAdvert>
                {
                   new GhgCreditAdvert
                    {
                        OrganisationId = 1,
                        ObligationPeriodId = FakeObligationPeriodId,
                        AdvertiseToBuy = true,
                        AdvertiseToSell = true,
                        FullName = "User",
                        EmailAddress = "email@email.com",
                        PhoneNumber = "01293456789"
                    },
                   new GhgCreditAdvert
                    {
                        OrganisationId = 1,
                        ObligationPeriodId = FakeObligationPeriodId + 1,
                        AdvertiseToBuy = true,
                        AdvertiseToSell = true
                    }
                });

                this.Controller = new MarketplaceController(new MarketplaceService(new MarketplaceRepository(context), new Mock<ILogger<MarketplaceService>>().Object), this.ReportingPeriodsService.Object, this.OrganisationsService.Object, this.Logger.Object);
                this.SetUserPrincipal();

                // Act
                result = await this.Controller.SetMarketplaceAdvert(organisationId, FakeObligationPeriodId, advert);

                // Assert 
                Assert.IsInstanceOfType(result, typeof(OkResult));
                var record = await context.FindAsync<GhgCreditAdvert>(1, FakeObligationPeriodId);
                Assert.IsFalse(record.AdvertiseToBuy);
                Assert.IsFalse(record.AdvertiseToSell);
                Assert.IsNull(record.FullName);
                Assert.IsNull(record.EmailAddress);
                Assert.IsNull(record.PhoneNumber);
            }
        }

        #endregion SetMarketplaceAdvert Tests

        #region Private Methods
        private void SetUserPrincipal()
        {
            // - user principal
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", Guid.NewGuid().ToString()));
            claims.Add(new Claim("OrganisationId", UserOrganisationId.ToString()));

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }

            };
        }

        private GosMarketplaceDataContext GetNewInMemoryContext()
        {
            var options = new DbContextOptionsBuilder<GosMarketplaceDataContext>()
                .UseInMemoryDatabase(databaseName: "MarketplaceControllerTestDatabase_" + Guid.NewGuid())
                .Options;
            var context = new GosMarketplaceDataContext(options);
            context.Database.EnsureDeleted();
            return context;
        }

        /// <summary>
        /// Populated a db context with adverts
        /// </summary>
        /// <param name="context">The db context to populate</param>
        /// <param name="ghgCreditAdverts">The list of adverts to populate the context with</param>
        private void PopulateContext(GosMarketplaceDataContext context, List<GhgCreditAdvert> ghgCreditAdverts)
        {
            context.AddRange(ghgCreditAdverts);

            context.SaveChanges();
        }

        #endregion Private Methods
    }
}
