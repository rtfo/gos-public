﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.API.Data;
using DfT.GOS.GhgMarketplace.API.Repositories;
using DfT.GOS.GhgMarketplace.API.Services;
using DfT.GOS.GhgMarketplace.Common.Models;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgMarketplace.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests for the marketplace service
    /// </summary>
    [TestClass]
    public class MarketplaceServiceTest
    {
        #region Constructors

        private int FakeObligationPeriodId = 15;
        private int FakeOrganisationId = 1;
        private int AnotherFakeOrganisationId = 2;

        #endregion Constructors

        #region Properties

        private IMarketplaceService Service { get; set; }
        private Mock<IMarketplaceRepository> MockMarketplaceRepository { get; set; }
        private Mock<ILogger<MarketplaceService>> MockLogger { get; set; }

        #endregion Properties

        #region Initialise

        /// <summary>
        /// Initialises the objects used for each test
        /// </summary>
        [TestInitialize]
        public void Initialise()
        {
            this.MockMarketplaceRepository = new Mock<IMarketplaceRepository>();
            this.MockLogger = new Mock<ILogger<MarketplaceService>>();

            this.Service = new MarketplaceService(this.MockMarketplaceRepository.Object, this.MockLogger.Object);
        }

        #endregion Initialise

        #region GetMarketplaceAdverts Tests

        /// <summary>
        /// Tests that the GetMarketplaceAdverts correctly constructs and returns a MarketplaceAdverts object
        /// </summary>
        [TestMethod]
        public async Task MarketplaceService_GetMarketplaceAdverts_Returns_Expected_MarketplaceAdverts()
        {
            //arrange
            var expectedName = "Name";
            var expectedEmail = "name@email.com";
            var expectedPhone = "+44123456789";
            var adverts = new List<GhgCreditAdvert>();
            adverts.Add(new GhgCreditAdvert()
            {
                ObligationPeriodId = FakeObligationPeriodId,
                OrganisationId = FakeOrganisationId,
                AdvertiseToBuy = true,
                AdvertiseToSell = false,
                FullName = "Name",
                EmailAddress = "name@email.com",
                PhoneNumber = "+44123456789"
            });
            adverts.Add(new GhgCreditAdvert()
            {
                ObligationPeriodId = FakeObligationPeriodId,
                OrganisationId = AnotherFakeOrganisationId,
                AdvertiseToBuy = true,
                AdvertiseToSell = true
            });

            this.MockMarketplaceRepository.Setup(r => r.GetMarketplaceAdverts(FakeObligationPeriodId)).ReturnsAsync(adverts);

            //act
            var result = await this.Service.GetMarketplaceAdverts(FakeObligationPeriodId);

            //assert
            Assert.IsInstanceOfType(result, typeof(MarketplaceAdverts));
            Assert.AreEqual(FakeObligationPeriodId, result.ObligationPeriodId);
            // - assert the lists were correctly populated
            Assert.AreEqual(FakeOrganisationId, result.OrganisationsAdvertisingToBuy[0].OrganisationId);
            Assert.AreEqual(expectedName, result.OrganisationsAdvertisingToBuy[0].FullName);
            Assert.AreEqual(expectedEmail, result.OrganisationsAdvertisingToBuy[0].EmailAddress);
            Assert.AreEqual(expectedPhone, result.OrganisationsAdvertisingToBuy[0].PhoneNumber);
            Assert.AreEqual(AnotherFakeOrganisationId, result.OrganisationsAdvertisingToBuy[1].OrganisationId);
            Assert.AreEqual(AnotherFakeOrganisationId, result.OrganisationsAdvertisingToSell[0].OrganisationId);
            Assert.AreEqual(1, result.OrganisationsAdvertisingToSell.Count);
        }

        #endregion GetMarketplaceAdverts Tests
    }
}
