﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.API.Data;
using DfT.GOS.GhgMarketplace.API.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgMarketplace.API.UnitTest.Repositories
{
    /// <summary>
    /// Unit tests for the marketplace repository
    /// </summary>
    [TestClass]
    public class MarketplaceRepositoryTest
    {
        #region Constants

        private const int FakeObligationPeriodId = 15;
        private const int AnotherFakeObligationPeriodId = 14;
        private const int FakeOrganisationId = 1;
        private const int AnotherFakeOrganisationId = 2;

        #endregion Constants

        #region GetMarketplaceAdverts Tests

        /// <summary>
        /// Tests that the GetMarketplaceAdverts method correctly filters by obligation period
        /// </summary>
        [TestMethod]
        public async Task MarketplaceRepository_GetMarketplaceAdverts_Filters_By_ObligationPeriod()
        {
            //Arrange
            var expectedCount = 2;
            var adverts = new List<GhgCreditAdvert>();
            adverts.Add(new GhgCreditAdvert()
            {
                ObligationPeriodId = FakeObligationPeriodId,
                OrganisationId = FakeOrganisationId,
                AdvertiseToBuy = true,
                AdvertiseToSell = true
            });
            adverts.Add(new GhgCreditAdvert()
            {
                ObligationPeriodId = FakeObligationPeriodId,
                OrganisationId = AnotherFakeOrganisationId,
                AdvertiseToBuy = true,
                AdvertiseToSell = true
            });
            adverts.Add(new GhgCreditAdvert()
            {
                ObligationPeriodId = AnotherFakeObligationPeriodId,
                OrganisationId = FakeOrganisationId,
                AdvertiseToBuy = true,
                AdvertiseToSell = true
            });

            IEnumerable<GhgCreditAdvert> result;

            using (var context = this.GetNewInMemoryContext())
            {
                
                this.PopulateContext(context, adverts);
                IMarketplaceRepository repository = new MarketplaceRepository(context);

                //Act
                result = await repository.GetMarketplaceAdverts(FakeObligationPeriodId);                
            }

            var actualAdverts = result.ToList();

            //Assert
            Assert.IsInstanceOfType(result, typeof(IEnumerable<GhgCreditAdvert>));
            Assert.AreEqual(expectedCount, actualAdverts.Count);
            Assert.AreEqual(FakeObligationPeriodId, actualAdverts[0].ObligationPeriodId);
            Assert.AreEqual(FakeObligationPeriodId, actualAdverts[1].ObligationPeriodId);
        }

        #endregion GetMarketplaceAdverts Tests

        #region Private Methods

        /// <summary>
        /// Gets a new instance of the an in-memory db context
        /// </summary>
        private GosMarketplaceDataContext GetNewInMemoryContext()
        {
            var options = new DbContextOptionsBuilder<GosMarketplaceDataContext>()
               .UseInMemoryDatabase(databaseName: "MarketplaceRepositoryTestDatabase_" + Guid.NewGuid())
               .Options;
            var context = new GosMarketplaceDataContext(options);
            context.Database.EnsureDeleted();
            return context;
        }

        /// <summary>
        /// Populated a db context with adverts
        /// </summary>
        /// <param name="context">The db context to populate</param>
        /// <param name="ghgCreditAdverts">The list of adverts to populate the context with</param>
        private void PopulateContext(GosMarketplaceDataContext context, List<GhgCreditAdvert> ghgCreditAdverts)
        {
            context.AddRange(ghgCreditAdverts);
            context.SaveChanges();
        }

        #endregion Private Methods
    }
}
