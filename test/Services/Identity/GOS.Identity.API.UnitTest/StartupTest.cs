﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace DfT.GOS.Identity.API.UnitTest
{
    /// <summary>
    /// Tests the Startup class
    /// </summary>
    [TestClass]
    public class StartupTest
    {
        #region Tests

        /// <summary>
        /// Tests that the CONNECTION_STRING variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_connection_string_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_ConnectionString);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_ConnectionStringNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the ADMIN_EMAIL_ADDRESS variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_admin_email_address_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_AdminEmailAddress);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_AdminEmailAddressNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the ADMIN_PASSWORD variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_admin_password_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_AdminPassword);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_AdminPasswordNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the GOV_NOTIFY_URL variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_govuk_notify_url_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovUkNotifyUrl);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_GovUkNotifyUrlNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the GOV_NOTIFY_API_KEY variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_govuk_notify_api_key_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovUkNotifyApiKey);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_GovUkApiKeyNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the GOV_NOTIFY_NEW_USER_INVITE_EMAIL_TEMPLATEID variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_govuk_notify_new_user_invite_email_template_id_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovUkNewUserInviteEmailTemplateId);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_GovUkNewUserInviteEmailTemplateIdNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the GovUkUserLinkedToCompanyEmailTemplateId variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_govuk_user_linked_to_company_email_template_id_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovUkUserLinkedToCompanyEmailTemplateId);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_GovUkUserLinkedToCompanyEmailTemplateIdNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the GovUkAdminUserEmailTemplateId variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_gov_uk_admin_user_email_template_id_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovUkAdminUserEmailTemplateId);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_GovUkAdminUserEmailTemplateIdNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the GovUkNewUserCreatedEmailTemplateId variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_govuk_new_user_created_email_template_id_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovUkNewUserCreatedEmailTemplateId);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_GovUkNewUserCreatedEmailTemplateIdNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the GovNotifyPasswordResetRequestEmailTemplateId variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_GOV_NOTIFY_PASSWORD_RESET_REQUEST_EMAIL_TEMPLATEID_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovNotifyPasswordResetRequestEmailTemplateId);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_GovNotifyPasswordResetRequestEmailTemplateIdNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the GovNotifyPasswordResetRequestEmailTemplateId variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_GOV_NOTIFY_PASSWORD_RESET_CONFIRMATION_EMAIL_TEMPLATEID_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovNotifyPasswordResetConfirmationEmailTemplateId);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_GovNotifyPasswordResetConfirmationEmailTemplateIdNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the GovNotifyUserLockedOutEmailTemplateId variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_GOV_NOTIFY_USER_LOCKED_OUT_EMAIL_TEMPLATEID_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovNotifyUserLockedOutEmailTemplateId);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_GovNotifyUserLockedOutEmailTemplateIdNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        /// <summary>
        /// Tests that the NEW_USER_INVITE_LINK_BASE_URL variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_new_user_invite_link_base_url_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_NewUserInviteLinkBaseUrl);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch (NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_NewUserInviteLinkBaseUrlNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        #endregion Tests

        #region Private Methods

        /// <summary>
        /// Gets a dictionary of all configuration settings for this service
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetAllConfigurationItems()
        {
            var configurationItems = new Dictionary<string, string>();
            configurationItems.Add(Startup.EnvironmentVariable_ConnectionString, "fake connection string");
            configurationItems.Add(Startup.EnvironmentVariable_AdminEmailAddress, "fake admin email address");
            configurationItems.Add(Startup.EnvironmentVariable_AdminPassword, "fake admin password");
            configurationItems.Add(Startup.EnvironmentVariable_GovUkNotifyUrl, "fake URL");
            configurationItems.Add(Startup.EnvironmentVariable_GovUkNotifyApiKey, "fake API Key");
            configurationItems.Add(Startup.EnvironmentVariable_GovUkNewUserInviteEmailTemplateId, "fake email template ID");
            configurationItems.Add(Startup.EnvironmentVariable_GovUkNewUserCreatedEmailTemplateId, "fake email template ID");
            configurationItems.Add(Startup.EnvironmentVariable_GovUkUserLinkedToCompanyEmailTemplateId, "fake email template ID");
            configurationItems.Add(Startup.EnvironmentVariable_GovUkAdminUserEmailTemplateId, "fake email template ID");
            configurationItems.Add(Startup.EnvironmentVariable_GovNotifyPasswordResetRequestEmailTemplateId, "fake email template ID");
            configurationItems.Add(Startup.EnvironmentVariable_GovNotifyPasswordResetConfirmationEmailTemplateId, "fake email template ID");
            configurationItems.Add(Startup.EnvironmentVariable_NewUserInviteLinkBaseUrl, "fake base URL");
            configurationItems.Add(Startup.EnvironmentVariable_GovNotifyUserLockedOutEmailTemplateId, "email template ID");
            return configurationItems;
        }

        /// <summary>
        /// Gets an instance of the startup class for testing
        /// </summary>
        /// <returns></returns>
        private Startup GetStartupInstance(IDictionary<string, string> configurationItems)
        {
            var configuration = new ConfigurationBuilder();
            var mockConfiguration = new Mock<IConfiguration>();

            foreach (var configurationItem in configurationItems)
            {
                mockConfiguration.SetupGet(c => c[configurationItem.Key]).Returns(configurationItem.Value);
            }

            var mockLogger = new Mock<ILogger<Startup>>();
            return new Startup(mockConfiguration.Object, mockLogger.Object);
        }

        #endregion Private Methods
    }
}
