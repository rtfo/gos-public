﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DfT.GOS.Identity.API.UnitTest.Models
{
    /// <summary>
    /// Tests the attribute-based validation rules configured for the GosApplicationUserInvite class
    /// </summary>
    [TestClass]
    public class GosApplicationUserInviteValidationTest : BaseValidationTest
    {
        /// <summary>
        /// Tests that the validation configured for the GosApplicationUserInvite object validates a valid object successfully
        /// </summary>
        [TestMethod]
        public void GosApplicationUserInvite_Validation_Success()
        {
            //arrange
            var model = new GosApplicationUserInvite()
            {
                Company = "Test Company",
                Email = "jbloggs@triad.co.uk",
                FullName = "Test Name",
                OrganisationId = 123,
                Password = "password",
                Role = "Test Role"
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count());
        }

        /// <summary>
        /// Tests that the validation configured for the GosApplicationUserInvite object requires that the Email property is populated
        /// </summary>
        [TestMethod]
        public void GosApplicationUserInvite_Validation_Email_Is_Required()
        {
            //arrange
            var model = new GosApplicationUserInvite()
            {
                Company = "Test Company",
                Email = null,
                FullName = "Test Name",
                OrganisationId = 123,
                Password = "password",
                Role = "Test Role"
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result.First().MemberNames);
            Assert.AreEqual(1, result.First().MemberNames.Count());
            Assert.AreEqual("Email", result.First().MemberNames.First());
        }

        /// <summary>
        /// Tests that the validation configured for the GosApplicationUserInvite object requires that a valid value is specified for the Email property
        /// </summary>
        [TestMethod]
        public void GosApplicationUserInvite_Validation_Email_Is_Invalid()
        {
            //arrange
            var model = new GosApplicationUserInvite()
            {
                Company = "Test Company",
                Email = "invalid.email.address",
                FullName = "Test Name",
                OrganisationId = 123,
                Password = "password",
                Role = "Test Role"
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result.First().MemberNames);
            Assert.AreEqual(1, result.First().MemberNames.Count());
            Assert.AreEqual("Email", result.First().MemberNames.First());
        }
    }
}
