﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DfT.GOS.Identity.API.UnitTest.Models
{
    /// <summary>
    /// Tests the attribute-based validation rules configured for the GosApplicationNewUser class
    /// </summary>    
    [TestClass]
    public class GosApplicationUserValidationTest : BaseValidationTest
    {
        /// <summary>
        /// Tests that the validation configured for the GosApplicationUser object ensures that an email address is supplied
        /// </summary>       
        [TestMethod]
        public void GosApplicationUser_Validation_Email_Is_Required()
        {
            //arrange
            var model = new GosApplicationUser()
            {
                AccessFailedCount = 0,
                ConcurrencyStamp = string.Empty,
                CreatedDate = DateTime.Now,
                Email = null,
                EmailConfirmed = false,
                FullName = "Test Name",
                Id = "",
                LockoutEnabled = true,
                LockoutEnd = DateTime.MinValue,
                NormalizedEmail = null,
                NormalizedUserName = "",
                OrganisationAssociationBy = "",
                OrganisationAssociationDate = null,
                OrganisationId = null,
                PasswordHash = "",
                PhoneNumber = "",
                PhoneNumberConfirmed = false,
                Role = "",
                SecurityStamp = "",
                TwoFactorEnabled = false,
                UserName = "Test Username"
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result.First().MemberNames);
            Assert.AreEqual(1, result.First().MemberNames.Count());
            Assert.AreEqual("Email", result.First().MemberNames.First());
        }

        /// <summary>
        /// Tests that the validation configured for the GosApplicationUser object ensures that a valid email address is supplied
        /// </summary>       
        [TestMethod]
        public void GosApplicationUser_Validation_Email_Is_Valid()
        {
            //arrange
            var model = new GosApplicationUser()
            {
                AccessFailedCount = 0,
                ConcurrencyStamp = string.Empty,
                CreatedDate = DateTime.Now,
                Email = "invalid.email.address",
                EmailConfirmed = false,
                FullName = "Test Name",
                Id = "",
                LockoutEnabled = true,
                LockoutEnd = DateTime.MinValue,
                NormalizedEmail = null,
                NormalizedUserName = "",
                OrganisationAssociationBy = "",
                OrganisationAssociationDate = null,
                OrganisationId = null,
                PasswordHash = "",
                PhoneNumber = "",
                PhoneNumberConfirmed = false,
                Role = "",
                SecurityStamp = "",
                TwoFactorEnabled = false,
                UserName = "Test Username"
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result.First().MemberNames);
            Assert.AreEqual(1, result.First().MemberNames.Count());
            Assert.AreEqual("Email", result.First().MemberNames.First());
        }

        /// <summary>
        /// Tests that the validation configured for the GosApplicationUser object ensures that the user's full name is supplied
        /// </summary>       
        [TestMethod]
        public void GosApplicationUser_Validation_FullName_Is_Required()
        {
            //arrange
            var model = new GosApplicationUser()
            {
                AccessFailedCount = 0,
                ConcurrencyStamp = string.Empty,
                CreatedDate = DateTime.Now,
                Email = "joe.bloggs@triad.co.uk",
                EmailConfirmed = false,
                FullName = null,
                Id = "",
                LockoutEnabled = true,
                LockoutEnd = DateTime.MinValue,
                NormalizedEmail = null,
                NormalizedUserName = "",
                OrganisationAssociationBy = "",
                OrganisationAssociationDate = null,
                OrganisationId = null,
                PasswordHash = "",
                PhoneNumber = "",
                PhoneNumberConfirmed = false,
                Role = "",
                SecurityStamp = "",
                TwoFactorEnabled = false,
                UserName = "Test Username"
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result.First().MemberNames);
            Assert.AreEqual(1, result.First().MemberNames.Count());
            Assert.AreEqual("FullName", result.First().MemberNames.First());
        }

        /// <summary>
        /// Tests that the validation configured for the GosApplicationUser object ensures that a user name is supplied
        /// </summary>       
        [TestMethod]
        public void GosApplicationUser_Validation_UserName_Is_Required()
        {
            //arrange
            var model = new GosApplicationUser()
            {
                AccessFailedCount = 0,
                ConcurrencyStamp = string.Empty,
                CreatedDate = DateTime.Now,
                Email = "jbloggs@triad.co.uk",
                EmailConfirmed = false,
                FullName = "Test Name",
                Id = "",
                LockoutEnabled = true,
                LockoutEnd = DateTime.MinValue,
                NormalizedEmail = null,
                NormalizedUserName = "",
                OrganisationAssociationBy = "",
                OrganisationAssociationDate = null,
                OrganisationId = null,
                PasswordHash = "",
                PhoneNumber = "",
                PhoneNumberConfirmed = false,
                Role = "",
                SecurityStamp = "",
                TwoFactorEnabled = false,
                UserName = null
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result.First().MemberNames);
            Assert.AreEqual(1, result.First().MemberNames.Count());
            Assert.AreEqual("UserName", result.First().MemberNames.First());
        }
    }
}
