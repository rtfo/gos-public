﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DfT.GOS.Identity.API.UnitTest.Models
{
    /// <summary>
    /// Tests the attribute-based validation rules configured for the GosApplicationNewUser class
    /// </summary>    
    [TestClass]
    public class SetAdminRoleCommandValidationTest : BaseValidationTest
    {
        /// <summary>
        /// Tests that the validation configured for the SetAdminRoleCommand object ensures that an email address is supplied
        /// </summary>       
        [TestMethod]
        public void SetAdminRoleCommand_Validation_Email_Is_Required()
        {
            //arrange
            var model = new SetAdminRoleCommand()
            {
                 EmailAddress = null
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result.First().MemberNames);
            Assert.AreEqual(1, result.First().MemberNames.Count());
            Assert.AreEqual("EmailAddress", result.First().MemberNames.First());
        }

        /// <summary>
        /// Tests that the validation configured for the SetAdminRoleCommand object ensures that a valid email address is supplied
        /// </summary>       
        [TestMethod]
        public void SetAdminRoleCommand_Validation_Email_Is_Valid()
        {
            //arrange
            var model = new SetAdminRoleCommand()
            {
                EmailAddress = "invalid.email.address"
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result.First().MemberNames);
            Assert.AreEqual(1, result.First().MemberNames.Count());
            Assert.AreEqual("EmailAddress", result.First().MemberNames.First());
        }

        /// <summary>
        /// Tests that the validation configured for the SetAdminRoleCommand object ensures that a valid object is validated successfully
        /// </summary>       
        [TestMethod]
        public void SetAdminRoleCommand_Validation_Success()
        {
            //arrange
            var model = new SetAdminRoleCommand()
            {
                EmailAddress = "jbloggs@triad.co.uk"
            };

            //act
            var result = Validate(model);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count());
        }
    }
}
