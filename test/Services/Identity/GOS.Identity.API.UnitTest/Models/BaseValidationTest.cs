﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DfT.GOS.Identity.API.UnitTest.Models
{
    public class BaseValidationTest
    {
        /// <summary>
        /// Performs attribute-based validation on the given object
        /// </summary>
        /// <param name="model">Object to be validated</param>
        /// <returns>Validation errors</returns>
        protected IEnumerable<ValidationResult> Validate(object model)
        {
            var results = new List<ValidationResult>();
            var validationContext = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, validationContext, results, true);
            return results;
        }
    }
}
