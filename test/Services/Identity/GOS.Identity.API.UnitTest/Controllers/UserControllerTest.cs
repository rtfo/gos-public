﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Commands;
using DfT.GOS.Identity.API.Controllers;
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests for the User Controller
    /// </summary>
    [TestClass]
    public class UserControllerTest
    {
        #region Constants

        private const string Username = "jbloggs";
        private const string UserId = "123";
        private const string UserEmail = "jbloggs@triad.co.uk";

        private const string OrganisationId = "xyz";

        #endregion Constants

        #region Properties

        private UsersController Controller { get; set; }
        private Mock<IUsersService> MockUsersService { get; set; }
        private Mock<INotify> MockNotificationService { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Sets up the controller for tests before each test is run
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.MockUsersService = new Mock<IUsersService>();
            this.MockNotificationService = new Mock<INotify>();
            this.Controller = new UsersController(this.MockUsersService.Object, MockNotificationService.Object);
            var claims = new List<Claim>
            {
                new Claim(Claims.UserId, UserId),
                new Claim(Claims.OrganisationId, OrganisationId)
            };
            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialize

        #region Tests

        #region Get By Username tests

        /// <summary>
        /// Tests that the GetByUsername method returns Ok if the user exists
        /// </summary>
        [TestMethod]
        public async Task UserController_GetByUsername_Returns_Ok_If_User_Exists()
        {
            //arrange
            this.MockUsersService.Setup(mock => mock.GetByUserName(Username))
                .ReturnsAsync(new GosApplicationUser() { UserName = Username });

            //act
            var result = await this.Controller.GetByUserName(Username);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(GosApplicationUser));
        }

        /// <summary>
        /// Tests that the GetByUsername method returns Ok if the user does not exist
        /// </summary>
        [TestMethod]
        public async Task UserController_GetByUsername_Returns_Ok_If_User_Does_Not_Exist()
        {
            //arrange
            this.MockUsersService.Setup(mock => mock.GetByUserName(Username))
                .ReturnsAsync((GosApplicationUser)null);

            //act
            var result = await this.Controller.GetByUserName(Username);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNull(((OkObjectResult)result).Value);
        }

        #endregion Get By Username tests

        #region Get By Email tests

        /// <summary>
        /// Tests that the GetByEmail method returns Ok if the user exists
        /// </summary>
        [TestMethod]
        public async Task UserController_GetByEmail_Returns_Ok_If_User_Exists()
        {
            //arrange
            this.MockUsersService.Setup(mock => mock.GetByEmail(UserEmail))
                .ReturnsAsync(new GosApplicationUser() { Email = UserEmail });

            //act
            var result = await this.Controller.GetByEmail(UserEmail);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(GosApplicationUser));
        }

        /// <summary>
        /// Tests that the GetByEmail method returns Ok if the user does not exist
        /// </summary>
        [TestMethod]
        public async Task UserController_GetByEmail_Returns_Ok_If_User_Does_Not_Exist()
        {
            //arrange
            this.MockUsersService.Setup(mock => mock.GetByEmail(UserEmail))
                .ReturnsAsync((GosApplicationUser)null);

            //act
            var result = await this.Controller.GetByUserName(UserEmail);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNull(((OkObjectResult)result).Value);
        }

        #endregion Get By Email tests

        #region Get By Id tests

        /// <summary>
        /// Tests that the GetById method returns Ok if the user exists
        /// </summary>
        [TestMethod]
        public async Task UserController_GetById_Returns_Ok_If_User_Exists()
        {
            //arrange
            this.MockUsersService.Setup(mock => mock.GetUserById(UserId))
                .ReturnsAsync(new GosApplicationUser() { Id = UserId });

            //act
            var result = await this.Controller.GetById(UserId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(GosApplicationUser));
        }

        /// <summary>
        /// Tests that the GetById method returns Ok if the user exists
        /// </summary>
        [TestMethod]
        public async Task UserController_GetById_Returns_Ok_If_User_Does_Not_Exist()
        {
            //arrange
            this.MockUsersService.Setup(mock => mock.GetByUserName(UserId))
                .ReturnsAsync((GosApplicationUser)null);

            //act
            var result = await this.Controller.GetById(UserId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNull(((OkObjectResult)result).Value);
        }

        #endregion Get By Id tests

        #region Get By Organisation Id tests

        /// <summary>
        /// Tests that the Organisation method returns Ok if users exist for the given organisation
        /// </summary>
        [TestMethod]
        public async Task UserController_Organisation_Returns_Ok_If_Users_Exist()
        {
            //arrange
            var organisationId = 123;
            this.MockUsersService.Setup(mock => mock.GetUsersByOrganisationId(It.IsAny<int>()))
                .ReturnsAsync(new List<GosApplicationUser>());

            //act
            var result = await this.Controller.Organisation(organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(List<GosApplicationUser>));
        }

        /// <summary>
        /// Tests that the Organisation method returns BadRequest if no users exist for the given organisation
        /// </summary>
        [TestMethod]
        public async Task UserController_Organisation_Returns_BadRequest_If_Users_Do_Not_Exist()
        {
            //arrange
            var organisationId = 123;
            this.MockUsersService.Setup(mock => mock.GetUsersByOrganisationId(It.IsAny<int>()))
                .ReturnsAsync((List<GosApplicationUser>)null);

            //act
            var result = await this.Controller.Organisation(organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        #endregion Get By Organisation Id tests

        #region Get Admin Users tests

        /// <summary>
        /// Tests that the admin users method returns OK
        /// </summary>
        [TestMethod]
        public async Task UserController_AdminUsers_Returns_Ok()
        {
            //arrange
            this.MockUsersService.Setup(mock => mock.GetAdminUsers())
                .ReturnsAsync(new GosApplicationUser[] { });

            //act
            var result = await this.Controller.AdminUsers();

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IList<GosApplicationUser>));
        }

        #endregion Get Admin Users tests

        #region Add User tests

        /// <summary>
        /// Tests that the AddUser method returns BadRequest if the ModelState is invalid (this can happen if a malformed request is submittted)
        /// </summary>
        [TestMethod]
        public async Task UserController_AddUser_Returns_BadRequest_If_ModelState_Error()
        {
            //arrange
            var user = (AddNewGosApplicationUser)null;
            this.Controller.ModelState.AddModelError("user", "test error");

            //act
            var result = await this.Controller.AddUser(user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the AddUser method returns BadRequest if the user cannot be added
        /// </summary>
        [TestMethod]
        public async Task UserController_AddUser_Returns_BadRequest_If_User_Not_Added()
        {
            //arrange
            var user = new AddNewGosApplicationUser();
            this.MockUsersService.Setup(mock => mock.AddUser(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync((AddNewGosApplicationUserResult)null);

            //act
            var result = await this.Controller.AddUser(user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the AddUser method returns Ok if the user is added successfully
        /// </summary>
        [TestMethod]
        public async Task UserController_AddUser_Returns_Ok_If_User_Added()
        {
            //arrange
            var user = new AddNewGosApplicationUser();
            this.MockUsersService.Setup(mock => mock.AddUser(It.IsAny<AddNewGosApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(new AddNewGosApplicationUserResult());

            //act
            var result = await this.Controller.AddUser(user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(AddNewGosApplicationUserResult));
        }

        #endregion Add User tests       

        #region Update User tests

        /// <summary>
        /// Tests that the UpdateUser method returns BadRequest if the ModelState is invalid (this can happen if a malformed request is submittted)
        /// </summary>
        [TestMethod]
        public async Task UserController_UpdateUser_Returns_BadRequest_If_ModelState_Error()
        {
            //arrange
            var user = (GosApplicationUser)null;
            this.Controller.ModelState.AddModelError("user", "test error");

            //act
            var result = await this.Controller.UpdateUser(UserId, user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the UpdateUser method returns BadRequest if the Id on the query string and the Id in the supplied user object are different
        /// </summary>
        [TestMethod]
        public async Task UserController_UpdateUser_Returns_BadRequest_If_Ids_Different()
        {
            //arrange
            var user = new GosApplicationUser() { Id = "dummy" };

            //act
            var result = await this.Controller.UpdateUser(UserId, user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the UpdateUser method returns BadRequest if the update fails
        /// </summary>
        [TestMethod]
        public async Task UserController_UpdateUser_Returns_BadRequest_If_Update_Fails()
        {
            //arrange
            var user = new GosApplicationUser() { Id = UserId };
            this.MockUsersService.Setup(mock => mock.UpdateUser(It.IsAny<GosApplicationUser>()))
                .ReturnsAsync((GosApplicationUser)null);

            //act
            var result = await this.Controller.UpdateUser(UserId, user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the UpdateUser method returns OK if the update succeeds
        /// </summary>
        [TestMethod]
        public async Task UserController_UpdateUser_Returns_Ok_If_Update_Succeeds()
        {
            //arrange
            var user = new GosApplicationUser() { Id = UserId };
            this.MockUsersService.Setup(mock => mock.UpdateUser(It.IsAny<GosApplicationUser>()))
                .ReturnsAsync(new GosApplicationUser());

            //act
            var result = await this.Controller.UpdateUser(UserId, user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(GosApplicationUser));
        }

        #endregion Update User tests

        #region Remove User tests

        /// <summary>
        /// Tests that the RemoveUser method returns BadRequest if the ModelState is invalid (this can happen if a malformed request is submittted)
        /// </summary>
        [TestMethod]
        public async Task UserController_RemoveUser_Returns_BadRequest_If_ModelState_Error()
        {
            //arrange
            var userId = (string)null;
            this.Controller.ModelState.AddModelError("userId", "test error");

            //act
            var result = await this.Controller.RemoveUser(userId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the RemoveUser method returns BadRequest if the user cannot be removed
        /// </summary>
        [TestMethod]
        public async Task UserController_RemoveUser_Returns_BadRequest_If_User_Not_Removed()
        {
            //arrange
            var userId = UserId;
            this.MockUsersService.Setup(mock => mock.RemoveUser(It.IsAny<string>()))
                .ReturnsAsync(false);

            //act
            var result = await this.Controller.RemoveUser(userId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the RemoveUser method returns OK if the user has been removed successfully
        /// </summary>
        [TestMethod]
        public async Task UserController_RemoveUser_Returns_Ok_If_User_Removed()
        {
            //arrange
            var userId = UserId;
            this.MockUsersService.Setup(mock => mock.RemoveUser(It.IsAny<string>()))
                .ReturnsAsync(true);

            //act
            var result = await this.Controller.RemoveUser(userId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }

        #endregion Remove User tests

        #region Invite tests

        /// <summary>
        /// Tests that the Invite method returns BadRequest if the ModelState is invalid (this can happen if a malformed request is submittted)
        /// </summary>
        [TestMethod]
        public async Task UserController_Invite_Returns_BadRequest_If_ModelState_Error()
        {
            //arrange
            var invitation = (GosApplicationUserInvite)null;
            this.Controller.ModelState.AddModelError("user", "test error");

            //act
            var result = await this.Controller.Invite(invitation);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the Invite method returns BadRequest if the invitation process fails
        /// </summary>
        [TestMethod]
        public async Task UserController_Invite_Returns_BadRequest_If_Invitation_Fails()
        {
            //arrange
            var invitation = new GosApplicationUserInvite() { Email = UserEmail };
            this.MockUsersService.Setup(mock => mock.InviteUserToRegisterAccount(It.IsAny<GosApplicationUserInvite>(), It.IsAny<string>()))
                .ReturnsAsync(false);

            //act
            var result = await this.Controller.Invite(invitation);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the Invite method returns BadRequest if the invitation process fails
        /// </summary>
        [TestMethod]
        public async Task UserController_Invite_Returns_Ok_If_Invitation_Succeeds()
        {
            //arrange
            var invitation = new GosApplicationUserInvite() { Email = UserEmail };
            this.MockUsersService.Setup(mock => mock.InviteUserToRegisterAccount(It.IsAny<GosApplicationUserInvite>(), It.IsAny<string>()))
                .ReturnsAsync(true);

            //act
            var result = await this.Controller.Invite(invitation);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(bool));
            Assert.IsTrue((bool)((OkObjectResult)result).Value);
        }

        #endregion Invite tests

        #region Get Email By Link tests

        /// <summary>
        /// Tests that the GetEmailByLink method returns BadRequest if the supplied link is null
        /// </summary>
        [TestMethod]
        public async Task UserController_GetEmailByLink_Returns_BadRequest_If_Link_Null()
        {
            //arrange
            var link = (string)null;

            //act
            var result = await this.Controller.GetEmailByLink(link);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the GetEmailByLink method returns BadRequest if the supplied link is not found
        /// </summary>
        [TestMethod]
        public async Task UserController_GetEmailByLink_Returns_BadRequest_If_Link_Not_Found()
        {
            //arrange
            var link = "test link";
            this.MockUsersService.Setup(mock => mock.LinkAsync(It.IsAny<string>()))
                .ReturnsAsync((GosInvitedUserAssociation)null);

            //act
            var result = await this.Controller.GetEmailByLink(link);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the GetEmailByLink method returns Ok if the supplied link is found
        /// </summary>
        [TestMethod]
        public async Task UserController_GetEmailByLink_Returns_Ok_If_Link_Found()
        {
            //arrange
            var link = "test link";
            this.MockUsersService.Setup(mock => mock.LinkAsync(It.IsAny<string>()))
                .ReturnsAsync(new GosInvitedUserAssociation());

            //act
            var result = await this.Controller.GetEmailByLink(link);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(GosInvitedUserAssociation));
        }

        #endregion Get Email By Link tests

        #region New User Email tests

        /// <summary>
        /// Tests that the NewUserEmail method returns BadRequest if the ModelState is invalid (this can happen if a malformed request is received)
        /// </summary>
        [TestMethod]
        public async Task UserController_NewUserEmail_Returns_BadRequest_If_ModelState_Invalid()
        {
            //arrange
            var user = (GosApplicationNewUser)null;
            this.Controller.ModelState.AddModelError("user", "test error");

            //act
            var result = await this.Controller.NewUserEmail(user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the NewUserEmail method returns BadRequest if the email is not sent
        /// </summary>
        [TestMethod]
        public async Task UserController_NewUserEmail_Returns_BadRequest_If_Email_Not_Sent()
        {
            //arrange
            var user = new GosApplicationNewUser() { Email = UserEmail };
            this.MockUsersService.Setup(mock => mock.NewUserEmailAsync(It.IsAny<GosApplicationNewUser>()))
                .ReturnsAsync(false);

            //act
            var result = await this.Controller.NewUserEmail(user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the NewUserEmail method returns OK if the email is sent
        /// </summary>
        [TestMethod]
        public async Task UserController_NewUserEmail_Returns_Ok_If_Email_Sent()
        {
            //arrange
            var user = new GosApplicationNewUser() { Email = UserEmail };
            this.MockUsersService.Setup(mock => mock.NewUserEmailAsync(It.IsAny<GosApplicationNewUser>()))
                .ReturnsAsync(true);

            //act
            var result = await this.Controller.NewUserEmail(user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(bool));
            Assert.IsTrue((bool)((OkObjectResult)result).Value);
        }

        #endregion New User Email tests

        #region New User Linked Email tests

        /// <summary>
        /// Tests that the NewUserLinkedEmail method returns BadRequest if the ModelState is invalid (this can happen if a malformed request is received)
        /// </summary>
        [TestMethod]
        public async Task UserController_NewUserLinkedEmail_Returns_BadRequest_If_ModelState_Invalid()
        {
            //arrange
            var user = (GosApplicationNewUser)null;
            var company = "dummy";
            this.Controller.ModelState.AddModelError("user", "test error");

            //act
            var result = await this.Controller.NewUserLinkedEmail(user, company);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the NewUserLinkedEmail method returns BadRequest if the email is not sent
        /// </summary>
        [TestMethod]
        public async Task UserController_NewUserLinkedEmail_Returns_BadRequest_If_Email_Not_Sent()
        {
            //arrange
            var user = new GosApplicationNewUser() { Email = UserEmail };
            var company = "dummy";
            this.MockUsersService.Setup(mock => mock.NewUserLinkedEmailAsync(It.IsAny<GosApplicationNewUser>()))
                .ReturnsAsync(false);

            //act
            var result = await this.Controller.NewUserLinkedEmail(user, company);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the NewUserLinkedEmail method returns OK if the email is sent
        /// </summary>
        [TestMethod]
        public async Task UserController_NewUserLinkedEmail_Returns_Ok_If_Email_Sent()
        {
            //arrange
            var user = new GosApplicationNewUser() { Email = UserEmail };
            var company = "dummy";
            this.MockUsersService.Setup(mock => mock.NewUserLinkedEmailAsync(It.IsAny<GosApplicationNewUser>()))
                .ReturnsAsync(true);

            //act
            var result = await this.Controller.NewUserLinkedEmail(user, company);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(bool));
            Assert.IsTrue((bool)((OkObjectResult)result).Value);
        }

        #endregion New User Linked Email tests

        #region Issue Forgotten Password Notification tests

        /// <summary>
        /// Tests that the IssueForgottenPasswordNotification correctly handles a modelstate error
        /// </summary>
        [TestMethod]
        public void UserController_IssueForgottenPasswordNotification_Handles_ModelState_Error()
        {
            //arrange
            this.Controller.ModelState.AddModelError("1", "Fake ModelState Error");

            //act
            var result = this.Controller.IssueForgottenPasswordNotification(null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the IssueForgottenPasswordNotification correctly handles an issue from the service class
        /// </summary>
        [TestMethod]
        public void UserController_IssueForgottenPasswordNotification_Handles_ServiceClass_Error()
        {
            //arrange
            this.MockUsersService.Setup(s => s.SendUserResetPasswordNotification(It.IsAny<IssueForgottenPasswordNotificationCommand>()))
                .Returns(Task.FromResult(false));

            //act
            var result = this.Controller.IssueForgottenPasswordNotification(It.IsAny<IssueForgottenPasswordNotificationCommand>()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the IssueForgottenPasswordNotification correctly handles the happy path
        /// </summary>
        [TestMethod]
        public void UserController_IssueForgottenPasswordNotification_Sucess()
        {
            //arrange
            this.MockUsersService.Setup(s => s.SendUserResetPasswordNotification(It.IsAny<IssueForgottenPasswordNotificationCommand>()))
                .Returns(Task.FromResult(true));

            //act
            var result = this.Controller.IssueForgottenPasswordNotification(It.IsAny<IssueForgottenPasswordNotificationCommand>()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }

        #endregion Issue Forgotten Password Notification tests

        #region Reset Password tests

        /// <summary>
        /// Tests that the Reset Password correctly handles a modelstate error
        /// </summary>
        [TestMethod]
        public void UserController_ResetPassword_Handles_ModelState_Error()
        {
            //arrange
            this.Controller.ModelState.AddModelError("1", "Fake ModelState Error");

            //act
            var result = this.Controller.ResetPassword(null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the Reset Password correctly handles an issue from the service class
        /// </summary>
        [TestMethod]
        public void UserController_ResetPassword_Handles_ServiceClass_Error()
        {
            //arrange
            this.MockUsersService.Setup(s => s.ResetPassword(It.IsAny<ResetPasswordCommand>()))
                .Returns(Task.FromResult(false));

            //act
            var result = this.Controller.ResetPassword(It.IsAny<ResetPasswordCommand>()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the ResetPassword correctly handles the happy path
        /// </summary>
        [TestMethod]
        public void UserController_ResetPassword_Sucess()
        {
            //arrange
            this.MockUsersService.Setup(s => s.ResetPassword(It.IsAny<ResetPasswordCommand>()))
                .Returns(Task.FromResult(true));

            //act
            var result = this.Controller.ResetPassword(It.IsAny<ResetPasswordCommand>()).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }

        #endregion Reset Password Tests

        #region Assign Role To User tests

        /// <summary>
        /// Tests that the AssignRoleToUser method returns BadRequest if the ModelState is invalid (this can happen if a malformed request is received)
        /// </summary>
        [TestMethod]
        public async Task UserController_AssignRoleToUser_Returns_BadRequest_If_ModelState_Invalid()
        {
            //arrange
            var user = (GosApplicationUser)null;
            this.Controller.ModelState.AddModelError("user", "test error");

            //act
            var result = await this.Controller.AssignRoleToUser(user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the AssignRoleToUser method returns BadRequest if the role could not be assigned
        /// </summary>
        [TestMethod]
        public async Task UserController_AssignRoleToUser_Returns_BadRequest_If_Role_Not_Assigned()
        {
            //arrange
            var user = new GosApplicationUser();
            this.MockUsersService.Setup(mock => mock.AssignRoleToUserAsync(It.IsAny<GosApplicationUser>()))
                .ReturnsAsync((GosApplicationUser)null);

            //act
            var result = await this.Controller.AssignRoleToUser(user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the AssignRoleToUser method returns Ok if the role has been assigned
        /// </summary>
        [TestMethod]
        public async Task UserController_AssignRoleToUser_Returns_Ok_If_Role_Assigned()
        {
            //arrange
            var user = new GosApplicationUser();
            this.MockUsersService.Setup(mock => mock.AssignRoleToUserAsync(It.IsAny<GosApplicationUser>()))
                .ReturnsAsync(new GosApplicationUser());

            //act
            var result = await this.Controller.AssignRoleToUser(user);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }

        #endregion Assign Role To User tests

        #region Set Admin role tests

        /// <summary>
        /// Tests that the AddAdminUser method returns BadRequest if the ModelState is invalid (this can happen if a malformed request is submittted)
        /// </summary>
        [TestMethod]
        public void UserController_AddAdminUser_Returns_BadRequest_If_ModelState_Invalid()
        {
            //arrange
            this.Controller.ModelState.AddModelError("userEmail", "test error");
            //act
            var result = this.Controller.AddAdminUser(null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the AddAdminUser method returns BadRequest if an invalid user id is supplied
        /// </summary>        [TestMethod]
        public void UserController_AddAdminUser_Returns_BadRequest_If_UserId_Invalid()
        {
            //arrange
            var email = "user@123.com";
            var outputMessage = email + " is not a registered user";
            this.MockUsersService.Setup(x => x.AddAdminUser(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new AddNewAdminUserResult
                {
                    Message = email + " is not a registered user",
                    IsNewAdministrator = false
                }));
            //act
            var result = this.Controller.AddAdminUser(new SetAdminRoleCommand { EmailAddress = email }).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual(outputMessage, ((BadRequestObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the AddAdminUser method returns BadRequest if the user request object is wrong
        /// </summary>        
        [TestMethod]
        public void UserController_AddAdminUser_Returns_BadRequest_If_User_Command_Is_Bad()
        {
            //arrange
            var email = "invalidEmail.com";
            this.Controller.ModelState.AddModelError("user", "test error");

            //act
            var result = this.Controller.AddAdminUser(new SetAdminRoleCommand { EmailAddress = email }).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the AddAdminUser method returns BadRequest if the user request object is wrong
        /// </summary>        
        [TestMethod]
        public void UserController_AddAdminUser_Returns_BadRequest_If_User_Service_Fails()
        {
            //arrange
            var email = "user@123.com";
            var outputMessage = email + " cannot be added as administrator. User is associated with a supplier.";
            this.MockUsersService.Setup(x => x.AddAdminUser(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new AddNewAdminUserResult
                {
                    Message = outputMessage,
                    IsNewAdministrator = false,
                    UserInvited = false,
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                }));

            //act
            var result = this.Controller.AddAdminUser(new SetAdminRoleCommand { EmailAddress = email }).Result;

            //assert
            AddNewAdminUserResult badResultValue = (AddNewAdminUserResult)((BadRequestObjectResult)result).Value;
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual(outputMessage, badResultValue.Message);

        }

        /// <summary>
        /// Tests that the AddAdminUser method returns OK if the user is assigned the admin role successfully
        /// </summary>
        [TestMethod]
        public void UserController_AddAdminUser_Returns_Ok_If_Sets_AdminRole_To_User()
        {
            //arrange
            this.MockUsersService.Setup(x => x.AddAdminUser(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new AddNewAdminUserResult
                {
                    Message = "",
                    IsNewAdministrator = true
                }));

            //act
            var result = this.Controller.AddAdminUser(new SetAdminRoleCommand { EmailAddress = "123@ww.com" }).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

        }

        #endregion

        #region Remove Admin Role tests

        /// <summary>
        /// Tests that the RemoveAdminRole method returns BadRequest when the ModelState is invalid (this can happen if a malformed request is received)
        /// </summary>
        [TestMethod]
        public void UserController_RemoveAdminRole_Returns_BadRequest_If_ModelState_Invalid()
        {
            //arrange
            this.Controller.ModelState.AddModelError("userId", "test error");

            //act
            var result = this.Controller.RemoveAdminRole(null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the RemoveAdminRole method returns BadRequest if given an empty user id
        /// </summary>
        [TestMethod]
        public void UserController_RemoveAdminRole_Returns_BadRequest_If_Empty_UserId()
        {
            //act
            var result = this.Controller.RemoveAdminRole("").Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the RemoveAdminRole method returns BadRequest if given an invalid user id
        /// </summary>
        [TestMethod]
        public void UserController_RemoveAdminRole_Returns_BadRequest_If_Invalid_UserId()
        {
            //arrange
            this.MockUsersService.Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(null));

            //act
            var result = this.Controller.RemoveAdminRole("22424").Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the RemoveAdminRole method returns BadRequest if given the user id for a user who is already linked to an organisation
        /// </summary>
        [TestMethod]
        public void UserController_RemoveAdminRole_Returns_BadRequest_If_Organisation_User()
        {
            //arrange
            this.MockUsersService.Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(Task.FromResult(new GosApplicationUser { OrganisationId = 131, Role = Roles.Supplier }));
            //act
            var result = this.Controller.RemoveAdminRole("12212121").Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the RemoveAdminRole method returns BadRequest if the given user id does not identify an admin user
        /// </summary>
        [TestMethod]
        public void UserController_RemoveAdminRole_Returns_BadRequest_If_Is_Not_Admin_User()
        {
            //arrange
            this.MockUsersService.Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser() { UserName = "123" }));
            this.MockUsersService.Setup(x => x.GetAdminUsers())
                .Returns(Task.FromResult<IList<GosApplicationUser>>(new List<GosApplicationUser>()
                {
                    new GosApplicationUser { Id = "2211" }
                }));
            //act
            var result = this.Controller.RemoveAdminRole("12212121").Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the RemoveAdminRole method returns OK if the admin role is removed successfully
        /// </summary>
        [TestMethod]
        public void UserController_RemoveAdminRole_Returns_Ok_If_Admin_Role_Removed_Successfully()
        {
            //arrange
            var mockUser = new GosApplicationUser { Id = "12212121", UserName = "123", Role = Roles.Administrator };
            this.MockUsersService.Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(mockUser));
            this.MockUsersService.Setup(x => x.GetAdminUsers())
                .Returns(Task.FromResult<IList<GosApplicationUser>>(new List<GosApplicationUser>
                {
                    mockUser
                }));
            this.MockUsersService.Setup(x => x.RemoveAdminRole(It.IsAny<string>()))
                .Returns(Task.FromResult<bool>(true));

            //act
            var result = this.Controller.RemoveAdminRole("12212121").Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }

        /// <summary>
        /// Tests that the RemoveAdminRole method returns BadRequest if it fails to remove the admin role
        /// </summary>
        [TestMethod]
        public void UserController_RemoveAdminRole_Returns_BadRequest_If_Admin_Role_Is_Not_Removed()
        {
            //arrange
            var mockUser = new GosApplicationUser { Id = "12212121", UserName = "123", Role = Roles.Administrator };
            this.MockUsersService.Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(mockUser));
            this.MockUsersService.Setup(x => x.GetAdminUsers())
                .Returns(Task.FromResult<IList<GosApplicationUser>>(new List<GosApplicationUser>
                {
                    mockUser
                }));
            this.MockUsersService.Setup(x => x.RemoveAdminRole(It.IsAny<string>()))
                .Returns(Task.FromResult<bool>(false));

            //act
            var result = this.Controller.RemoveAdminRole("12212121").Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        #endregion

        #endregion Tests
    }
}
