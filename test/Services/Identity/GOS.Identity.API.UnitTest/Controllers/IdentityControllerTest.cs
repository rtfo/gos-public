﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Controllers;
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.API.Models.Configuration;
using DfT.GOS.Identity.API.Services;
using DfT.GOS.Identity.API.UnitTest.Fakes;
using DfT.GOS.Identity.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.UnitTest.Controllers
{
    [TestClass]
    public class IdentityControllerTest
    {
        #region Constants

        private const string ValidUserName = "validuser";
        private const string ValidPassword = "validpassword";

        private const string InvalidUserName = "invaliduser";
        private const string InvalidPassword = "invalidpassword";

        private const string LockedUserName = "lockeduser";
        private const string LockedPassword = "lockedpassword";

        private const string SecurityToken = "SecurityToken";

        private const string ClaimKey = "Test Claim";
        private const string ClaimValue = "Test Claim Value";

        private readonly AppSettings AppSettings = new AppSettings()
        {
            Authentication = new AuthenticationConfiguration()
            {
                DefaultLockoutTimeSpan = 5,
                MaxFailedAccessAttempts = 5
            },
            InviteExpiryHours = 3,
            JwtTokenExpiryMinutes = 60,
            PasswordResetNotificationExpiryHours = 2
        };

        #endregion Constants

        #region Properties

        private IdentityController Controller { get; set; }
        private Mock<ISecurityTokenService> SecurityTokenService { get; set; }
        private Mock<IUserStore<GosApplicationUser>> UserStore { get; set; }
        private Mock<UserManager<GosApplicationUser>> UserManager { get; set; }
        #endregion Properties

        #region Initialize

        [TestInitialize]
        public void Initialize()
        {
            var store = new Mock<IUserStore<GosApplicationUser>>();
            UserManager = new Mock<UserManager<GosApplicationUser>>(store.Object, null, null, null, null, null, null, null, null);
            var contextAccessor = new Mock<IHttpContextAccessor>();
            var claimsFactory = new Mock<IUserClaimsPrincipalFactory<GosApplicationUser>>();
            var optionsAccessor = new Mock<IOptions<IdentityOptions>>();
            var simLogger = new Mock<ILogger<SignInManager<GosApplicationUser>>>();
            var schemes = new Mock<IAuthenticationSchemeProvider>();
            var confirmation = new Mock<IUserConfirmation<GosApplicationUser>>();
            var logins = new Dictionary<string, Microsoft.AspNetCore.Identity.SignInResult>()
            {
                { ValidUserName, Microsoft.AspNetCore.Identity.SignInResult.Success  },
                { InvalidUserName, Microsoft.AspNetCore.Identity.SignInResult.Failed },
                { LockedUserName, Microsoft.AspNetCore.Identity.SignInResult.LockedOut }
            };
            var signInManager = new FakeSignInManager<GosApplicationUser>(
                UserManager.Object, contextAccessor.Object, claimsFactory.Object, optionsAccessor.Object, simLogger.Object, schemes.Object, confirmation.Object, logins
                );

            UserStore = new Mock<IUserStore<GosApplicationUser>>();
            var userRoleStore = new Mock<IUserRoleStore<GosApplicationUser>>();
            var securityTokenService = new Mock<ISecurityTokenService>();
            securityTokenService.Setup(mock => mock.GetClaimsForUser(It.IsAny<GosApplicationUser>(), It.IsAny<IList<string>>()))
                .Returns(new Dictionary<string, string>() { { ClaimKey, ClaimValue } });
            securityTokenService.Setup(mock => mock.CreateToken(It.IsAny<GosApplicationUser>(), It.IsAny<Dictionary<string, string>>()))
                .Returns(SecurityToken);
            SecurityTokenService = securityTokenService;
            var appSettings = new Mock<IOptions<AppSettings>>();
            appSettings.Setup(mock => mock.Value)
                .Returns(this.AppSettings);
            this.Controller = new IdentityController(UserStore.Object, userRoleStore.Object, signInManager,
                securityTokenService.Object, appSettings.Object);
        }

        #endregion Initialize

        #region Tests

        #region Authenticate User tests

        /// <summary>
        /// Tests that the authenticate user method returns BadRequest if the Model State is invalid (this can happen if a malformed request is sent)
        /// </summary>
        [TestMethod]
        public async Task IdentityController_AuthenticateUser_Returns_BadRequest_On_ModelState_Error()
        {
            //arrange
            var request = (AuthenticationRequest)null;
            this.Controller.ModelState.AddModelError("request", "test error");

            //act
            var result = await this.Controller.AuthenticateUser(request);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the authenticate user method returns OK if the login succeeds
        /// </summary>
        [TestMethod]
        public async Task IdentityController_AuthenticateUser_Returns_Ok_On_Login_Success()
        {
            //arrange
            var request = new AuthenticationRequest(ValidUserName, ValidPassword);

            //act
            var result = await this.Controller.AuthenticateUser(request);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(AuthenticationResult));
            var authenticationResult = (AuthenticationResult)((OkObjectResult)result).Value;
            Assert.IsTrue(authenticationResult.Succeeded);
            Assert.IsFalse(authenticationResult.IsLockedOut);
            Assert.IsNotNull(authenticationResult.Claims);
            Assert.AreEqual(1, authenticationResult.Claims.Count);
            Assert.AreEqual(ClaimKey, authenticationResult.Claims.First().Key);
            Assert.AreEqual(ClaimValue, authenticationResult.Claims.First().Value);
            Assert.IsNotNull(authenticationResult.Token);
            Assert.AreEqual(SecurityToken, authenticationResult.Token);
        }

        /// <summary>
        /// Tests that the authenticate user method returns OK if the login succeeds
        /// </summary>
        [TestMethod]
        public async Task IdentityController_AuthenticateUser_Returns_Ok_On_Login_Failure()
        {
            //arrange
            var request = new AuthenticationRequest(InvalidUserName, InvalidPassword);

            //act
            var result = await this.Controller.AuthenticateUser(request);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(AuthenticationResult));
            var authenticationResult = (AuthenticationResult)((OkObjectResult)result).Value;
            Assert.IsFalse(authenticationResult.Succeeded);
            Assert.IsFalse(authenticationResult.IsLockedOut);
            Assert.IsNull(authenticationResult.Claims);
            Assert.IsNull(authenticationResult.Token);
        }

        /// <summary>
        /// Tests that the authenticate user method returns OK if the login succeeds
        /// </summary>
        [TestMethod]
        public async Task IdentityController_AuthenticateUser_Returns_Ok_On_Lockout()
        {
            //arrange
            var request = new AuthenticationRequest(LockedUserName, LockedPassword);

            //act
            var result = await this.Controller.AuthenticateUser(request);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(AuthenticationResult));
            var authenticationResult = (AuthenticationResult)((OkObjectResult)result).Value;
            Assert.IsFalse(authenticationResult.Succeeded);
            Assert.IsTrue(authenticationResult.IsLockedOut);
            Assert.IsNull(authenticationResult.Claims);
            Assert.IsNull(authenticationResult.Token);
        }

        #endregion Authenticate User tests

        #region AuthenticateService User tests
        /// <summary>
        ///  Tests that the authenticate service method returns BadRequest if the Model State is invalid
        /// </summary>
        [TestMethod]
        public void IdentityController_AuthenticateService_Returns_BadRequest_On_ModelState_Error()
        {
            //arrange
            var request = new AuthenticateServiceRequest();
            this.Controller.ModelState.AddModelError("request", "test error");
            //act
            var result = this.Controller.AuthenticateService(request);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        ///  Tests that the authenticate service method returns BadRequest if the service id is invalid
        /// </summary>
        [TestMethod]
        public void IdentityController_AuthenticateService_Returns_BadRequest_On_Invalid_ServiceId()
        {
            //arrange
            var request = new AuthenticateServiceRequest { ServiceId = "111313" };
            SecurityTokenService.Setup(x => x.GetServiceAccounts())
                .Returns(new List<AuthorisedService>() {
                    new AuthorisedService{Name = "GOS.GhgBalanceOrchestrator", Id = "ABCD" } });

            //act
            var result = this.Controller.AuthenticateService(request);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the authenticate service method returns OK if the login succeeds
        /// </summary>
        [TestMethod]
        public void IdentityController_AuthenticateService_Returns_Ok()
        {
            //arrange
            var request = new AuthenticateServiceRequest { ServiceId = "1234" };
            SecurityTokenService.Setup(x => x.GetServiceAccounts())
               .Returns(new List<AuthorisedService>() {
                    new AuthorisedService{Name = "GOS.GhgBalanceOrchestrator", Id = "1234" } });
            //act
            var result = this.Controller.AuthenticateService(request);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
        #endregion

        #region Get Authentication Settings tests

        /// <summary>
        /// Tests that the get authentication settings method returns OK
        /// </summary>
        [TestMethod]
        public void IdentityController_GetAuthenticationSettings_Returns_Ok()
        {
            //arrange

            //act
            var result = this.Controller.GetAuthenticationSettings();

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(AuthenticationConfiguration));
            Assert.AreEqual(this.AppSettings.Authentication, result);
        }

        #endregion Get Authentication Settings tests

        #region ValidateSecurityStamp tests
        /// <summary>
        /// Tests that an "Unauthorized" response is returned when the provided and users security stamps don't match
        /// </summary>
        /// <returns>Completed Task</returns>
        [TestMethod]
        public async Task IdentityController_ValidateSecurityStamp_Returns_Unauthorized_When_Stamp_Does_Not_Match()
        {
            //arrange
            var mockUser = new GosApplicationUser
            {
                SecurityStamp = "abc123"
            };
            UserStore.Setup(s => s.FindByIdAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).Returns(Task.FromResult(mockUser));

            //act
            var result = await this.Controller.ValidateSecurityStamp(new ValidateSecurityStampRequest("def321", "user1"));

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(UnauthorizedResult));
        }

        /// <summary>
        /// Tests that an "OK" response is returned when the provided and users security stamps do match
        /// </summary>
        /// <returns>Completed Task</returns>
        [TestMethod]
        public async Task IdentityController_ValidateSecurityStamp_Returns_Ok_When_Stamp_Match()
        {
            //arrange
            var mockUser = new GosApplicationUser
            {
                SecurityStamp = "abc123"
            };
            UserStore.Setup(s => s.FindByIdAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).Returns(Task.FromResult(mockUser));

            //act
            var result = await this.Controller.ValidateSecurityStamp(new ValidateSecurityStampRequest("abc123", "user1"));

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }
        #endregion ValidateSecurityStamp tests

        #region SignOut tests
        /// <summary>
        /// Tests that the signout method updates the security stamp which invalidates further requests using the same token
        /// </summary>
        /// <returns>Completed Task</returns>
        [TestMethod]
        public async Task SignOut_Updates_The_Security_Stamp()
        {
            //arrange
            UserStore.Setup(s => s.FindByIdAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).Returns(Task.FromResult(new GosApplicationUser()));

            //act
            var result = await this.Controller.SignOut(new SignOutUserRequest("user1"));

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkResult));
            UserManager.Verify(s => s.UpdateSecurityStampAsync(It.IsAny<GosApplicationUser>()), Times.Once);
        }

        /// <summary>
        /// Tests once signed out a user cannot use the same security stamp as previously
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task SignOut_Followed_By_Validate_Returns_Unauthorized()
        {
            //arrange
            var mockUser = new GosApplicationUser
            {
                Id = "user1",
                SecurityStamp = "abc123"
            };
            UserStore.Setup(s => s.FindByIdAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).Returns(Task.FromResult(mockUser));
            UserManager.Setup(s => s.UpdateSecurityStampAsync(mockUser)).Callback(() => mockUser.SecurityStamp = "def123").Returns(Task.FromResult(new IdentityResult()));

            //act
            await this.Controller.SignOut(new SignOutUserRequest(mockUser.Id));
            var result = await this.Controller.ValidateSecurityStamp(new ValidateSecurityStampRequest("abc123", mockUser.Id));

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(UnauthorizedResult));
        }
        #endregion SignOut tests

        #endregion Tests
    }
}
