﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.Common.Models;
using DfT.GOS.Identity.API.Services;
using DfT.GOS.Security.Claims;
using GOS.Identity.API.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests for the Performance Data Controller
    /// </summary>
    [TestClass]
    public class PerformanceDataControllerTest
    {

        #region Constants
        private const string UserId = "123";
        private const string OrganisationId = "xyz";
        #endregion Constants

        #region Properties
        private PerformanceDataController Controller { get; set; }
        private Mock<IUsersService> MockUsersService { get; set; }
        #endregion Properties

        #region Initialize
        /// <summary>
        /// Sets up the controller for tests before each test is run
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.MockUsersService = new Mock<IUsersService>();
            this.Controller = new PerformanceDataController(this.MockUsersService.Object);
            var claims = new List<Claim>
            {
                new Claim(Claims.UserId, UserId),
                new Claim(Claims.OrganisationId, OrganisationId)
            };
            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }
        #endregion Initialize

        #region RegistrationsData tests

        ///<summary>
        /// Registrations Data forwards on the list of data
        ///</summary>
        [TestMethod]
        public async Task RegistrationsData_Returns_The_Registrations_Data()
        {
            // Arrange
            this.MockUsersService.Setup(s => s.GetRegistrationPerformanceData())
                .Returns(Task.FromResult(new List<RegistrationsPerformanceData>
                {
                    new RegistrationsPerformanceData(DateTime.Now, 1, 1, 1),
                    new RegistrationsPerformanceData(DateTime.Now.AddMonths(-1), 5, 4, 3)
                }));

            // Act
            var result = await this.Controller.RegistrationsData();

            // Assert 
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(result);

        }
        #endregion RegistrationsData tests
    }
}
