﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Controllers;
using DfT.GOS.Identity.API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.UnitTest.Controllers
{
    [TestClass]
    public class RolesControllerTest
    {
        #region Constants

        private const string UserId = "123";

        #endregion Constants

        #region Properties

        private RolesController Controller { get; set; }

        #endregion Properties

        #region Initialize

        [TestInitialize]
        public void Initialize()
        {
            var dataService = new Mock<IRolesService>();
            dataService.Setup(mock => mock.GetByUserId(UserId))
                .ReturnsAsync(new string[] { "Role1" });
            this.Controller = new RolesController(dataService.Object);
        }

        #endregion Initialize

        #region Tests

        #region Get By UserId tests

        /// <summary>
        /// Tests that the get by userid method returns OK
        /// </summary>
        [TestMethod]
        public async Task RolesController_GetByUserId_Returns_Ok()
        {
            //arrange

            //act
            var result = await this.Controller.GetByUserId(UserId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IList<string>));
        }

        #endregion Get By UserId tests

        #endregion Tests
    }
}
