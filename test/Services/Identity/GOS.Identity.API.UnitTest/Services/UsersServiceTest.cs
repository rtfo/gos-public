﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using DfT.GOS.Identity.API.Commands;
using DfT.GOS.Identity.API.Data;
using DfT.GOS.Identity.API.Exceptions;
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.API.Models.Configuration;
using DfT.GOS.Identity.API.Repositories;
using DfT.GOS.Identity.API.Services;
using DfT.GOS.Security.Claims;
using DfT.GOS.SystemParameters.API.Client.Models;
using DfT.GOS.SystemParameters.API.Client.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Notify.Models.Responses;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests for the Users Service
    /// </summary>
    [TestClass]
    public class UsersServiceTest
    {
        #region Properties       

        private IUsersService Service { get; set; }
        private Mock<IUsersRepository> MockUsersRepository { get; set; }
        private Mock<IRolesRepository> MockRolesRepository { get; set; }
        private Mock<IUsersRolesRepository> MockUserRolesRepository { get; set; }
        private Mock<IInvitesRepository> MockInviteRepository { get; set; }
        private Mock<IDbContext> MockDbContext { get; set; }
        private Mock<IPasswordResetRequestRepository> MockPasswordResetRepository { get; set; }
        private Mock<ISystemParametersService> MockSystemParametersService { get; set; }
        private Mock<INotify> MockNotifications { get; set; }
        private Mock<ILogger<Startup>> MockLogger { get; set; }
        private Mock<IOptions<AppSettings>> AppSettings { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Sets up the service for tests before each test is run
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.MockUsersRepository = new Mock<IUsersRepository>();
            this.MockRolesRepository = new Mock<IRolesRepository>();
            this.MockUserRolesRepository = new Mock<IUsersRolesRepository>();
            this.MockInviteRepository = new Mock<IInvitesRepository>();
            this.MockDbContext = new Mock<IDbContext>();
            this.MockPasswordResetRepository = new Mock<IPasswordResetRequestRepository>();
            this.MockNotifications = new Mock<INotify>();
            this.MockSystemParametersService = new Mock<ISystemParametersService>();
            this.MockLogger = new Mock<ILogger<Startup>>();

            this.AppSettings = new Mock<IOptions<AppSettings>>();
            var settings = new AppSettings();
            settings.Authentication = new AuthenticationConfiguration();
            settings.Authentication.DefaultLockoutTimeSpan = 5;
            this.AppSettings.Setup(s => s.Value).Returns(settings);

            this.Service = new UsersService(this.MockDbContext.Object
                , this.MockUsersRepository.Object
                , this.MockRolesRepository.Object
                , this.MockUserRolesRepository.Object
                , this.MockInviteRepository.Object
                , this.MockPasswordResetRepository.Object
                , this.MockNotifications.Object
                , this.MockSystemParametersService.Object
                , this.AppSettings.Object
                , this.MockLogger.Object);
        }

        #endregion Initialize

        #region Tests

        #region Invites

        /// <summary>
        /// Tests that the GetInviteDetailsByLinkAsync method returns result only when invite has not expired
        /// </summary>
        [TestMethod]
        public void UsersService_GetInviteDetailsByLinkAsync_Invite_Not_Expired()
        {
            //Arrange
            this.MockInviteRepository.Setup(s => s.GetEmailByLinkAsync(It.IsAny<string>())).Returns(Task.FromResult(new GosInvitedUserAssociation { ExpireInviteLink = DateTime.Now.AddHours(-1) }));

            //Act
            var result = this.Service.LinkAsync(It.IsAny<string>()).Result;

            //Assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// Tests that the GetInviteDetailsByLinkAsync method returns a NULL invite result when a user account
        /// already exits for the invite's email address
        /// </summary>
        [TestMethod]
        public void UsersService_GetInviteDetailsByLinkAsync_Invite_Handles_User_Account_Already_Exists()
        {
            //Arrange
            var emailAddress = "fake@email.com";

            this.MockInviteRepository.Setup(s => s.GetEmailByLinkAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(new GosInvitedUserAssociation 
                                                { ExpireInviteLink = DateTime.Now.AddHours(1),
                                                    Email = emailAddress}));

            this.MockUsersRepository.Setup(u => u.GetByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(new GosApplicationUser()));

            //Act
            var result = this.Service.LinkAsync("fakeLink").Result;

            //Assert
            Assert.IsNull(result);
        }

        #endregion Invites

        #region Create user tests
        /// <summary>
        /// Check when users are created that LockoutEnabled is set to true. 
        /// The actual locking out is handled by Microsoft.AspNetCore.Identity.SignInManager so that part shouldn't need us to test additionally once we've set this.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Created_Users_Will_Be_Locked_Out_After_Failed_Logins()
        {
            //Arrange
            // Setup everything else so the user would be created
            var mockUser = new GosApplicationUserInvite
            {
                Email = "jbloggs@email.com",
                OrganisationId = 1,
                Company = "Company A",
                Role = Roles.Supplier,
            };
            this.MockInviteRepository.Setup(i => i.GetEmailByLinkAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(new GosInvitedUserAssociation
                {
                    ExpireInviteLink = DateTime.Now.AddHours(1),
                    Email = mockUser.Email,
                    OrganisationId = mockUser.OrganisationId,
                    Company = mockUser.Company,
                    Role = mockUser.Role
                }));
            this.MockRolesRepository.Setup(r => r.GetRoleIdByNameAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<string>("Supplier"));
            this.MockUsersRepository.Setup(u => u.GetUserIdByUsernameAsync(It.IsAny<string>()))
                .Returns(Task.FromResult("123456"));
            this.MockUserRolesRepository.Setup(ur => ur.AddAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult("OK"));
            this.MockNotifications.Setup(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, dynamic>>(), null, null))
                .ReturnsAsync(new EmailNotificationResponse());

            //Act
            var userId = await this.Service.CreateUser(mockUser, "password", "link");

            //Assert
            Assert.IsNotNull(userId);
            this.MockUsersRepository.Verify(r => r.CreateAsync(It.Is<GosApplicationUser>(u => u.LockoutEnabled)));
        }
        #endregion Create user tests

        #region Reset Password Notifications

        /// <summary>
        /// Tests that the SendUserResetPasswordNotification method handles a null user from the supplied email address
        /// </summary>
        [TestMethod]
        public void UsersService_SendUserResetPasswordNotification_Email_Address_Not_Found()
        {
            //Arrange
            var command = new IssueForgottenPasswordNotificationCommand("fakeEmail@Address.com");
            this.MockUsersRepository.Setup(s => s.GetByEmailAsync(command.EmailAddress)).Returns(Task.FromResult<GosApplicationUser>(null));

            //Act
            var result = this.Service.SendUserResetPasswordNotification(command).Result;

            //Assert
            Assert.IsFalse(result);
        }

        /// <summary>
        /// Tests that the SendUserResetPasswordNotification method handles failure in the repository to create a password reset request
        /// </summary>
        [TestMethod]
        public void UsersService_SendUserResetPasswordNotification_Repository_Error()
        {
            //Arrange
            var command = new IssueForgottenPasswordNotificationCommand("fakeEmail@Address.com");
            var fakeUser = new GosApplicationUser
            {
                Id = Guid.NewGuid().ToString()
            };

            this.MockUsersRepository.Setup(s => s.GetByEmailAsync(It.IsAny<string>())).Returns(Task.FromResult<GosApplicationUser>(fakeUser));
            this.MockDbContext.Setup(c => c.SaveChangesAsync(It.IsAny<CancellationToken>())).Returns(Task.FromResult(0));

            //Act
            var result = this.Service.SendUserResetPasswordNotification(command).Result;
            //Assert
            Assert.IsFalse(result);
        }

        /// <summary>
        /// Tests that the SendUserResetPasswordNotification method handles failure in sending the notification
        /// </summary>
        [TestMethod]
        public async Task UsersService_SendUserResetPasswordNotification_Notification_Error()
        {
            //Arrange
            var command = new IssueForgottenPasswordNotificationCommand("fakeEmail@Address.com");
            var fakeUser = new GosApplicationUser
            {
                Id = Guid.NewGuid().ToString()
            };

            this.MockUsersRepository.Setup(s => s.GetByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(fakeUser);
            this.MockDbContext.Setup(c => c.SaveChangesAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(1);
            this.MockNotifications.Setup(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, dynamic>>(), null, null))
                .ReturnsAsync((EmailNotificationResponse)null);

            //Act
            var result = await this.Service.SendUserResetPasswordNotification(command);

            //Assert
            Assert.IsFalse(result);
        }

        /// <summary>
        /// Tests that the SendUserResetPasswordNotification happy path
        /// </summary>
        [TestMethod]
        public void UsersService_SendUserResetPasswordNotification_Success()
        {
            //Arrange
            var command = new IssueForgottenPasswordNotificationCommand("fakeEmail@Address.com");
            var fakeUser = new GosApplicationUser
            {
                Id = Guid.NewGuid().ToString()
            };

            this.MockUsersRepository.Setup(s => s.GetByEmailAsync(It.IsAny<string>())).Returns(Task.FromResult<GosApplicationUser>(fakeUser));
            this.MockDbContext.Setup(c => c.SaveChangesAsync(It.IsAny<CancellationToken>())).Returns(Task.FromResult(1));
            this.MockNotifications.Setup(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, dynamic>>(), null, null))
                .ReturnsAsync(new EmailNotificationResponse());

            //Act
            var result = this.Service.SendUserResetPasswordNotification(command).Result;

            //Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Tests that the validate reset password method handles a non existing password reset token (i.e. one isn't found)
        /// </summary>
        [TestMethod]
        public async Task UsersService_ValidateResetPasswordToken_handles_none_existing_PasswordResetRequest()
        {
            //Arrange
            this.MockPasswordResetRepository.Setup(r => r.Get(It.IsAny<string>())).Returns(Task.FromResult<PasswordResetRequest>(null));

            //Act
            var result = await this.Service.ValidateResetPasswordToken("token");

            //Assert
            // - A warning was logged
            this.MockLogger.Verify(l => l.Log(LogLevel.Warning, 0, It.IsAny<It.IsAnyType>(), It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
            // - The method returns unsuccessful
            Assert.IsFalse(result);
            // - The repository wasn't updated
            this.MockUsersRepository.Verify(r => r.UpdateAsync(It.IsAny<GosApplicationUser>()), Times.Never);
            // - No emails were sent
            this.MockNotifications.Verify(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<String, dynamic>>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Tests that the reset password method handles a null password reset request (i.e. one isn't found)
        /// </summary>
        [TestMethod]
        public void UsersService_ResetPassword_handles_null_PasswordResetRequest()
        {
            //Arrange
            var fakeCommand = new ResetPasswordCommand("123", It.IsAny<string>(), It.IsAny<string>());
            this.MockPasswordResetRepository.Setup(r => r.Get(It.IsAny<string>())).Returns(Task.FromResult<PasswordResetRequest>(null));

            //Act
            var result = this.Service.ResetPassword(fakeCommand).Result;

            //Assert
            // - A warning was logged
            this.MockLogger.Verify(l => l.Log(LogLevel.Warning, 0, It.IsAny<It.IsAnyType>(), It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a,b) => true)), Times.Once);
            // - The method returns unsuccessful
            Assert.IsFalse(result);
            // - The repository wasn't updated
            this.MockUsersRepository.Verify(r => r.UpdateAsync(It.IsAny<GosApplicationUser>()), Times.Never);
            // - No emails were sent
            this.MockNotifications.Verify(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<String, dynamic>>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Tests that the validate reset password token handles an expired reset request
        /// </summary>
        [TestMethod]
        public async Task UsersService_ValidateResetPasswordTokend_handles_expired_PasswordResetRequest()
        {
            //Arrange
            this.MockPasswordResetRepository.Setup(r => r.Get(It.IsAny<string>()))
                .Returns(Task.FromResult(new PasswordResetRequest { Expires = DateTime.Now.AddDays(-1) }));

            //Act
            var result = await this.Service.ValidateResetPasswordToken("token");

            //Assert
            // - A warning was logged
            this.MockLogger.Verify(l => l.Log(LogLevel.Warning, 0, It.IsAny<It.IsAnyType>(), It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
            // - The method returns unsuccessful
            Assert.IsFalse(result);
            // - The repository wasn't updated
            this.MockUsersRepository.Verify(r => r.UpdateAsync(It.IsAny<GosApplicationUser>()), Times.Never);
            // - No emails were sent
            this.MockNotifications.Verify(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<String, dynamic>>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Tests that the reset password method handles an expired reset request
        /// </summary>
        [TestMethod]
        public void UsersService_ResetPassword_handles_expired_PasswordResetRequest()
        {
            //Arrange
            var fakeCommand = new ResetPasswordCommand("123", "fake email", "fake password");

            this.MockPasswordResetRepository.Setup(r => r.Get(It.IsAny<string>()))
                .Returns(Task.FromResult(new PasswordResetRequest { Expires = DateTime.Now.AddDays(-1) }));

            //Act
            var result = this.Service.ResetPassword(fakeCommand).Result;

            //Assert
            // - A warning was logged
            this.MockLogger.Verify(l => l.Log(LogLevel.Warning, 0, It.Is<object>((a,b) => true), It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
            // - The method returns unsuccessful
            Assert.IsFalse(result);
            // - The repository wasn't updated
            this.MockUsersRepository.Verify(r => r.UpdateAsync(It.IsAny<GosApplicationUser>()), Times.Never);
            // - No emails were sent
            this.MockNotifications.Verify(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<String, dynamic>>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Tests that the reset password method handles a request that doesn't relate to an existing user
        /// </summary>
        [TestMethod]
        public void UsersService_ResetPassword_handles_null_user_PasswordResetRequest()
        {
            //Arrange
            var fakeCommand = new ResetPasswordCommand("123", "fake email", "fake password");

            this.MockPasswordResetRepository.Setup(r => r.Get(It.IsAny<string>()))
                .Returns(Task.FromResult(new PasswordResetRequest { Expires = DateTime.Now.AddDays(1) }));

            this.MockUsersRepository.Setup(r => r.GetAsync(It.IsAny<string>())).Returns(Task.FromResult<GosApplicationUser>(null));

            //Act
            var result = this.Service.ResetPassword(fakeCommand).Result;

            //Assert
            // - A warning was logged
            this.MockLogger.Verify(l => l.Log(LogLevel.Warning, 0, It.IsAny<It.IsAnyType>(), It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
            // - The method returns unsuccessful
            Assert.IsFalse(result);
            // - The repository wasn't updated
            this.MockUsersRepository.Verify(r => r.UpdateAsync(It.IsAny<GosApplicationUser>()), Times.Never);
            // - No emails were sent
            this.MockNotifications.Verify(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<String, dynamic>>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Tests that the reset password method handles a request that realates to a different user
        /// </summary>
        [TestMethod]
        public void UsersService_ResetPassword_handles_incorrect_user_PasswordResetRequest()
        {
            //Arrange
            var fakeCommand = new ResetPasswordCommand("123", "fake email", "fake password");

            this.MockPasswordResetRepository.Setup(r => r.Get(It.IsAny<string>()))
                .Returns(Task.FromResult(new PasswordResetRequest { Expires = DateTime.Now.AddDays(1) }));

            this.MockUsersRepository.Setup(r => r.GetAsync(It.IsAny<string>())).Returns(Task.FromResult(new GosApplicationUser { NormalizedEmail = "Some Other Email" }));

            //Act
            var result = this.Service.ResetPassword(fakeCommand).Result;

            //Assert
            // - A warning was logged
            this.MockLogger.Verify(l => l.Log(LogLevel.Warning, 0, It.IsAny<object>(), It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
            // - The method returns unsuccessful
            Assert.IsFalse(result);
            // - The repository wasn't updated
            this.MockUsersRepository.Verify(r => r.UpdateAsync(It.IsAny<GosApplicationUser>()), Times.Never);
            // - No emails were sent
            this.MockNotifications.Verify(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<String, dynamic>>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Tests that the reset password method happy path :)
        /// </summary>
        [TestMethod]
        public void UsersService_ResetPassword_Success()
        {
            //Arrange
            var emailAddress = "FAKE EMAIL";
            var fakeCommand = new ResetPasswordCommand("123", emailAddress, "fake password");

            this.MockPasswordResetRepository.Setup(r => r.Get(It.IsAny<string>()))
                .Returns(Task.FromResult(new PasswordResetRequest { Expires = DateTime.Now.AddDays(1) }));

            this.MockUsersRepository.Setup(r => r.GetAsync(It.IsAny<string>())).Returns(Task.FromResult(new GosApplicationUser { NormalizedEmail = emailAddress }));
            this.MockDbContext.Setup(db => db.SaveChangesAsync(It.IsAny<CancellationToken>())).Returns(Task.FromResult(1));

            this.MockNotifications.Setup(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<String, dynamic>>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new EmailNotificationResponse());

            //Act
            var result = this.Service.ResetPassword(fakeCommand).Result;

            //Assert
            // - The method returns successful
            Assert.IsTrue(result);
            // - The repository was updated
            this.MockUsersRepository.Verify(r => r.UpdateAsync(It.IsAny<GosApplicationUser>()), Times.Once);
            // - An email was sent
            this.MockNotifications.Verify(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<String, dynamic>>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }


        #endregion Reset Password Notifications

        #region Assign Role To User

        [TestMethod]
        public async Task UsersService_AssignRoleToUser_Returns_Null_If_Role_Does_Not_Exist()
        {
            //arrange
            var user = new GosApplicationUser { Role = null, Id = "jbloggs" };
            this.MockRolesRepository.Setup(mock => mock.GetRoleIdByNameAsync(It.IsAny<string>()))
                .ReturnsAsync((string)null);
            this.MockUserRolesRepository.Setup(mock => mock.AddAsync(It.IsAny<string>(), null))
                .ThrowsAsync(new Exception("Role Id cannot be null"));

            //act
            var result = await this.Service.AssignRoleToUserAsync(user);

            //assert
            Assert.IsNull(result);
        }

        #endregion Assign Role To User

        #region Add Admin user tests

        /// <summary>
        /// A new user being set as an administrator creates them an invite and emails them
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task AddAdminUser_New_User_Gets_An_Invite()
        {
            //arrange
            this.MockUsersRepository.Setup(u => u.GetByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(null));
            this.MockNotifications.Setup(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, dynamic>>(), null, null))
                .ReturnsAsync(new EmailNotificationResponse());
            this.MockInviteRepository.Setup(i => i.Add(It.IsAny<Invites>()))
                .Returns(Task.FromResult(new GosInvitedUserAssociation { ExpireInviteLink = DateTime.Now.AddHours(-1) }));

            //act
            var response = await this.Service.AddAdminUser("jbloggs@email.com", "123");

            //assert
            this.MockInviteRepository.Verify(r => r.Add(It.IsAny<Invites>()));
            this.MockNotifications.Verify(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, dynamic>>(), null, null), Times.Once);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }


        /// <summary>
        /// Check a user already associated with a organisation can't be made an administrator
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Can_Not_Add_Admin_If_Already_Associated_To_Organisation()
        {
            //arrange
            this.MockUsersRepository.Setup(u => u.GetByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser
                {
                    OrganisationId = 1
                }));

            //act
            var response = await this.Service.AddAdminUser("jbloggs@email.com", "123");

            //assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        /// <summary>
        /// A user who is already an Administrator can't be updated to be one, the user should be notified (bad request)
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Existing_Administrator_Can_Not_Be_Made_Admin()
        {
            //arrange
            this.MockUsersRepository.Setup(u => u.GetByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser
                {
                    OrganisationId = null
                }));
            this.MockRolesRepository.Setup(r => r.GetByUserIdAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<IList<string>>(new List<string>
                {
                    Roles.Administrator
                }));

            //act
            var response = await this.Service.AddAdminUser("jbloggs@email.com", "123");

            //assert
            this.MockRolesRepository.Verify(r => r.GetByUserIdAsync(It.IsAny<string>()), Times.Once);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        /// <summary>
        /// Existing users can be given the administrator role
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task An_Existing_User_can_Be_Made_An_Admin()
        {
            //arrange
            this.MockUsersRepository.Setup(u => u.GetByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser
                {
                    OrganisationId = null
                }));
            this.MockRolesRepository.Setup(r => r.GetByUserIdAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<IList<string>>(new List<string> { }));
            this.MockRolesRepository.Setup(r => r.GetRoleIdByNameAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<string>("Administrator"));

            //act
            var response = await this.Service.AddAdminUser("jbloggs@email.com", "123");

            //assert
            this.MockUsersRepository.Verify(u => u.UpdateAsync(It.IsAny<GosApplicationUser>()), Times.Once);
            this.MockUserRolesRepository.Verify(r => r.AddAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        /// <summary>
        /// If the admin role isn't present an internal server error status is returned
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task When_Admin_Role_Is_Not_Found_Return_Internal_Server_error()
        {
            //arrange
            this.MockUsersRepository.Setup(u => u.GetByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser
                {
                    OrganisationId = null
                }));
            this.MockRolesRepository.Setup(r => r.GetByUserIdAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<IList<string>>(new List<string> { }));
            this.MockRolesRepository.Setup(r => r.GetRoleIdByNameAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<string>(null));

            //act
            var response = await this.Service.AddAdminUser("jbloggs@email.com", "123");

            //assert
            this.MockUsersRepository.Verify(u => u.UpdateAsync(It.IsAny<GosApplicationUser>()), Times.Never);
            this.MockUserRolesRepository.Verify(r => r.AddAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        /// <summary>
        /// Existing users are emailed when they're made an administrator
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task An_Existing_User_Is_Notified_When_They_Are_Made_An_Admin()
        {
            //arrange
            this.MockUsersRepository.Setup(u => u.GetByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<GosApplicationUser>(new GosApplicationUser
                {
                    OrganisationId = null,
                    Email = "jbloggs@mail.com"
                }));
            this.MockRolesRepository.Setup(r => r.GetByUserIdAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<IList<string>>(new List<string> { }));
            this.MockRolesRepository.Setup(r => r.GetRoleIdByNameAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<string>("Administrator"));

            //act
            await this.Service.AddAdminUser("jbloggs@email.com", "123");

            //assert
            this.MockNotifications.Verify(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, dynamic>>(), null, null), Times.Once);
        }
        #endregion Add admin user tests

        #region GetRegistrationPerformanceData tests

        ///<summary>
        /// GetRegistrationPerformanceData returns registrations performance data for each month since the first user registered
        /// This includes:
        ///  - Number of invitations without an account, per month
        ///  - Number of invitations, per month
        ///  - Total number of users, per month
        ///</summary>
        [TestMethod]
        public async Task GetRegistrationPerformanceData_Returns_All_Registration_Performance_Data_For_Each_Month_Since_First_Registration()
        {
            // Arrange 
            var baseDate = new DateTime(2019, 04, 05);
            var expectedNumberOfMonths =((DateTime.Now.Year - baseDate.Year) * 12) + (DateTime.Now.Month - baseDate.Month) + 1;
            var expectedUsersInMonthOne = 2;
            var expectedUsersInMonthTwo = 1;
            var expectedInvitationsMonthOne = 3;
            var expectedInvitationsMonthTwo = 4;
            var expectedInvitationsWithoutAccountMonthOne = 2;
            var expectedInvitationsWithoutAccountMonthTwo = 3;

            using (var context = this.GetNewInMemoryContext())
            {
                this.SetupRegistrationPerformanceData(context, baseDate);
                this.Service = new UsersService(context, this.MockLogger.Object, this.MockNotifications.Object, this.MockSystemParametersService.Object, this.AppSettings.Object);

                // Act
                var result = await this.Service.GetRegistrationPerformanceData();

                // Assert 
                // Number of months that values have been created for
                Assert.AreEqual(expectedNumberOfMonths, result.Count);
                // Number of new users in the system for a month
                Assert.AreEqual(expectedUsersInMonthOne, result[0].TotalUsers);
                Assert.AreEqual(expectedUsersInMonthTwo, result[1].TotalUsers);
                //Number of invitations sent each month
                Assert.AreEqual(expectedInvitationsMonthOne, result[0].Invitations);
                Assert.AreEqual(expectedInvitationsMonthTwo, result[1].Invitations);
                // Number of users who received invitations but didn't create accounts
                Assert.AreEqual(expectedInvitationsWithoutAccountMonthOne, result[0].InvitationsWithoutAccount);
                Assert.AreEqual(expectedInvitationsWithoutAccountMonthTwo, result[1].InvitationsWithoutAccount);
            }
        }

        ///<summary>
        /// GetRegistrationPerformanceData returns registrations performance data for each month since the first user registered over multiple years
        /// This includes:
        ///  - Number of invitations without an account, per month
        ///  - Number of invitations, per month
        ///  - Total number of users, per month
        ///</summary>
        [TestMethod]
        public async Task GetRegistrationPerformanceData_Returns_All_Registration_Performance_Data_For_Each_Month_Since_First_Registration_Multiple_Years()
        {
            // Arrange 
            var baseDate = new DateTime(2018, 04, 05);
            var expectedNumberOfMonths = (DateTime.Now.Month - baseDate.Month) + 1 + (12 * (DateTime.Now.Year - baseDate.Year));
            var expectedUsersInMonthOne = 2;
            var expectedUsersInMonthTwo = 1;
            var expectedInvitationsMonthOne = 3;
            var expectedInvitationsMonthTwo = 4;
            var expectedInvitationsWithoutAccountMonthOne = 2;
            var expectedInvitationsWithoutAccountMonthTwo = 3;
            var expectedInvitationsThisMonthLastYear = 0;
            var expectedInvitationsThisMonth = 1;

            using (var context = this.GetNewInMemoryContext())
            {
                this.SetupRegistrationPerformanceData(context, baseDate);
                //Add one more invite recently
                PopulateContextWithInvites(context, new List<Invites>
                {
                    new Invites
                    {
                        Role = Roles.Supplier,
                        OrganisationName = "Organisation",
                        Email = "FredAbc@triad.co.uk",
                        Uri = Guid.NewGuid().ToString(),
                        ExpireInviteLink = DateTime.Now.AddDays(1),
                        InvitedBy = "AminUserId",
                        InvitedDate = DateTime.Now.AddHours(-1)
                    }
                });

                this.Service = new UsersService(context, this.MockLogger.Object, this.MockNotifications.Object, this.MockSystemParametersService.Object, this.AppSettings.Object);

                // Act
                var result = await this.Service.GetRegistrationPerformanceData();

                // Assert 
                // Number of months that values have been created for
                Assert.AreEqual(expectedNumberOfMonths, result.Count);
                // Number of new users in the system for a month
                Assert.AreEqual(expectedUsersInMonthOne, result[0].TotalUsers);
                Assert.AreEqual(expectedUsersInMonthTwo, result[1].TotalUsers);
                //Number of invitations sent each month
                Assert.AreEqual(expectedInvitationsMonthOne, result[0].Invitations);
                Assert.AreEqual(expectedInvitationsMonthTwo, result[1].Invitations);
                Assert.AreEqual(expectedInvitationsThisMonth, result[result.Count - 1].Invitations);
                // Number of users who received invitations but didn't create accounts
                Assert.AreEqual(expectedInvitationsWithoutAccountMonthOne, result[0].InvitationsWithoutAccount);
                Assert.AreEqual(expectedInvitationsWithoutAccountMonthTwo, result[1].InvitationsWithoutAccount);
                Assert.AreEqual(expectedInvitationsThisMonth, result[result.Count - 1].InvitationsWithoutAccount);

                var thisMonthInPreviousYear = result.FindLast(r => r.Date.Month == DateTime.Now.Month && r.Date.Year == DateTime.Now.Year - 1);
                Assert.AreEqual(expectedInvitationsThisMonthLastYear, thisMonthInPreviousYear.Invitations);
            }
        }


        ///<summary>
        /// Tests that if there is an invitation sent before the first user is registered that date is used to return data from. This would happen
        /// if the GosAdmin user was entirely deleted from the database.
        ///</summary>
        [TestMethod]
        public async Task GetRegistrationPerformanceData_Returns_Correct_Data_If_Invite_Is_Earliest_date()
        {
            // Arrange 
            var baseDate = new DateTime(2019, 04, 05);
            var expectedInvitations = 4;
            var expectedInvitationsWithoutAccount = 3;


            using (var context = this.GetNewInMemoryContext())
            {
                this.SetupRegistrationPerformanceData(context, baseDate);
                //Add one more invite before the admin user was created
                PopulateContextWithInvites(context, new List<Invites>
                {
                    new Invites
                    {
                        Role = Roles.Supplier,
                        OrganisationName = "Organisation",
                        Email = "FredAbc@triad.co.uk",
                        Uri = Guid.NewGuid().ToString(),
                        ExpireInviteLink = DateTime.Now.AddDays(1),
                        InvitedBy = "AminUserId",
                        InvitedDate = baseDate.AddMinutes(-10)
                    }
                });

                this.Service = new UsersService(context, this.MockLogger.Object, this.MockNotifications.Object, this.MockSystemParametersService.Object, this.AppSettings.Object);

                // Act
                var result = await this.Service.GetRegistrationPerformanceData();

                // Assert
                //Number of invitations sent each month
                Assert.AreEqual(expectedInvitations, result[0].Invitations);
                // Number of users who received invitations but didn't create accounts
                Assert.AreEqual(expectedInvitationsWithoutAccount, result[0].InvitationsWithoutAccount);
            }
        }


        ///<summary>
        /// An exception is thrown when no invitations or users can be found
        ///</summary>
        [TestMethod]
        public async Task GetRegistrationPerformanceData_Throws_A_ValidDateNotFoundException_When_No_Date_Is_Found()
        {
            // Arrange 
            using (var context = this.GetNewInMemoryContext())
            {
                this.Service = new UsersService(context, this.MockLogger.Object, this.MockNotifications.Object, this.MockSystemParametersService.Object, this.AppSettings.Object);

                // Act and Assert
                await Assert.ThrowsExceptionAsync<ValidDateNotFoundException>(() => this.Service.GetRegistrationPerformanceData());
            }
        }

        ///<summary>
        /// Tests that unclaimed invites gets the correct number over multiple months. It should count all invites that don't have an account created from them
        /// regardless of if an account was created from a subsequent invite.
        ///</summary>
        [TestMethod]
        public async Task GetRegistrationPerformanceData_Returns_Correct_Uncalimed_Invitation_Count()
        {
            // Arrange 
            var baseDate = new DateTime(2019, 04, 05);
            var expectedInvitationsWithoutAccountOne = 3;
            var expectedInvitationsWithoutAccountTwo = 5;
            var expectedInvitationsWithoutAccountThree = 0;

            using (var context = this.GetNewInMemoryContext())
            {
                //Month One -> invites + 3 Users + 2 invitesWithoutAccount + 2
                //Month Two -> invites + 4 Users + 1 invitesWithoutAccount + 3
                this.SetupRegistrationPerformanceData(context, baseDate);
                // Add a more invites and claim some, 
                // also add Invite one month, claim the next
                // Month one -> invites + 1 Users + 0 invitesWithoutaccount 1
                // Month Two -> invites + 4 Users + 3 invitesWithoutAccount + 2
                var invites = new List<Invites>
                {
                    new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation",
                    Email = "FredAbc@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMonths(1).AddMinutes(-10).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMonths(1).AddMinutes(-10)
                },
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation",
                    Email = "FredAbc@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMonths(1).AddMinutes(-10).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMonths(1).AddMinutes(-5)
                },
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation",
                    Email = "FredAbc@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMonths(1).AddMinutes(-10).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMonths(1).AddMinutes(-3)
                },
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation",
                    Email = "MaryDef@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMonths(1).AddMinutes(-10).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMonths(1).AddMinutes(-5)
                },
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation",
                    Email = "Batman@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMonths(1).AddMinutes(-10).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMonths(1).AddDays(-5).AddHours(7)
                }
                };
                PopulateContextWithInvites(context, invites);

                var users = new List<GosApplicationUser>{
                    new GosApplicationUser
                    {
                        FullName = "Fred Abc",
                        Email = "FredAbc@triad.co.uk",
                        UserName = "FredAbc",
                        NormalizedEmail = "FredAbc@triad.co.uk".ToUpper(),
                        CreatedDate = baseDate.AddMonths(1)
                    },
                    new GosApplicationUser
                {
                    FullName = "Mary Def",
                    Email = "MaryDef@triad.co.uk",
                    UserName = "MaryDef",
                    NormalizedEmail = "MaryDef@triad.co.uk".ToUpper(),
                    CreatedDate = baseDate.AddMonths(1)
                },
                new GosApplicationUser
                {
                    FullName = "Bruce Wayne",
                    Email = "Batman@triad.co.uk",
                    UserName = "BWayne",
                    NormalizedEmail = "Batman@triad.co.uk".ToUpper(),
                    CreatedDate = baseDate.AddMonths(1).AddDays(-3).AddHours(-6)
                }
                };
                PopulateContextWithUsers(context, users);

                this.Service = new UsersService(context, this.MockLogger.Object, this.MockNotifications.Object, this.MockSystemParametersService.Object, this.AppSettings.Object);

                // Act
                var result = await this.Service.GetRegistrationPerformanceData();

                // Assert 
                Assert.AreEqual(expectedInvitationsWithoutAccountOne, result[0].InvitationsWithoutAccount);
                Assert.AreEqual(expectedInvitationsWithoutAccountTwo, result[1].InvitationsWithoutAccount);
                Assert.AreEqual(expectedInvitationsWithoutAccountThree, result[2].InvitationsWithoutAccount);
            }
        }
        #endregion  GetRegistrations tests

        #region User Locked out notification tests
        /// <summary>
        /// UserLockedOutNotification should return false if the command passed in is invalid
        /// </summary>
        [TestMethod]
        public async Task UserLockedOutNotification_Handles_Empty_Email()
        {
            // Act
            var result = await this.Service.UserLockedOutNotification(new UserLockedOutCommand("", "user"));

            // Assert
            Assert.IsFalse(result);
        }

        /// <summary>
        /// UserLockedOutNotification should return true when it succeeds
        /// </summary>
        [TestMethod]
        public async Task UserLockedOutNotification_Returns_Success()
        {
            //Arrange
            this.MockNotifications.Setup(n => n.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, dynamic>>(), null, null))
                .ReturnsAsync(new EmailNotificationResponse());
            this.MockSystemParametersService.Setup(s => s.GetSystemParameters()).ReturnsAsync(new GosSystemParameters(new List<SystemParameter>
            {
                new SystemParameter(4, "RTFO Support Email", "email@address.com" ),
                new SystemParameter(3, "RTFO Support Telephone Number", "12312321" ),
            }));

            // Act
            var result = await this.Service.UserLockedOutNotification(new UserLockedOutCommand("fake@email.com", "user"));

            // Assert
            Assert.IsTrue(result);
        }
        #endregion User Locked out notification tests

        #endregion Tests

        #region DataSetup

        /// <summary>
        /// Create some data in an in-memory database for use by the registrations test
        /// </summary>
        /// <param name="options">Options for the database context</param>
        /// <param name="baseDate">Date the tests are based from</param>
        private void SetupRegistrationPerformanceData(GosIdentityDataContext context, DateTime baseDate)
        {
            // Create three users
            var users = new List<GosApplicationUser>
            {
                new GosApplicationUser
                {
                    FullName = "Gos Admin",
                    Email = "gosadmin@triad.co.uk",
                    UserName = "gosadmin",
                    NormalizedEmail = "gosadmin@triad.co.uk".ToUpper(),
                    CreatedDate =baseDate
                },
                new GosApplicationUser
                {
                    FullName = "Jane Smith",
                    Email = "JaSmith@triad.co.uk",
                    UserName = "Jane",
                    NormalizedEmail = "JaSmith@triad.co.uk".ToUpper(),
                    CreatedDate =baseDate.AddMinutes(5)
                },
                new GosApplicationUser
                {
                    FullName = "John Smith",
                    Email = "JSmith@triad.co.uk",
                    UserName = "jsmith",
                    NormalizedEmail = "JSmith@triad.co.uk".ToUpper(),
                    CreatedDate =baseDate.AddMonths(1)
                }
            };

            // Create entries for a number of invites, some for created users, some where users received multiple and then created their accounts
            // and some where no user account was then created.
            var invites = new List<Invites>
            {
                // Three used invites
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation 1",
                    Email = "JaSmith@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMinutes(1).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMinutes(1)
                },
                // These two were both sent to the same  user
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation 2",
                    Email = "JSmith@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMonths(1).AddMinutes(-3).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMonths(1).AddMinutes(-3)
                },
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation 2",
                    Email = "JSmith@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMonths(1).AddMinutes(-2).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMonths(1).AddMinutes(-2)
                },
                // Four unused invites - two in the first month for the same user
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation 3",
                    Email = "unclaimed@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddDays(3).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddDays(3)
                },
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation 3",
                    Email = "unclaimed@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddDays(7).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddDays(7)
                },
                // Two unused in the second month for different users
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation 4",
                    Email = "unclaimedtwo@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMonths(1).AddDays(7).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMonths(1).AddDays(7)
                },
                new Invites
                {
                    Role = Roles.Supplier,
                    OrganisationName = "Organisation 5",
                    Email = "BDoeUnclaimed@triad.co.uk",
                    Uri = Guid.NewGuid().ToString(),
                    ExpireInviteLink = baseDate.AddMonths(1).AddMinutes(-3).AddDays(1),
                    InvitedBy = "AminUserId",
                    InvitedDate = baseDate.AddMonths(1).AddMinutes(-3)
                }
            };

            // Add all the users and invites to the in-memory database
            PopulateContextWithInvites(context, invites);
            PopulateContextWithUsers(context, users);
        }

        #endregion DataSetup

        #region Private methods
        /// <summary>
        /// Gets a new instance of the an in-memory db context
        /// </summary>
        private GosIdentityDataContext GetNewInMemoryContext()
        {
            var options = new DbContextOptionsBuilder<GosIdentityDataContext>()
               .UseInMemoryDatabase(databaseName: "UserServiceTestDatabase_" + Guid.NewGuid())
               .Options;
            var context = new GosIdentityDataContext(options);
            context.Database.EnsureDeleted();
            return context;
        }

        /// <summary>
        /// Puts the given list of invites in the database context
        /// </summary>
        /// <param name="context">Database context</param>
        /// <param name="invites">List of invites to store</param>
        private static void PopulateContextWithInvites(GosIdentityDataContext context, List<Invites> invites)
        {
            invites.ForEach(i => context.Invites.Add(i));
            context.SaveChanges();
        }

        /// <summary>
        /// Puts the given list of users in the database context
        /// </summary>
        /// <param name="context">Database context</param>
        /// <param name="invites">List of users to store</param>
        private static void PopulateContextWithUsers(GosIdentityDataContext context, List<GosApplicationUser> invites)
        {
            invites.ForEach(u => context.Users.Add(u));
            context.SaveChanges();
        }

        #endregion Private Methods
    }
}
