﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.UnitTest.Fakes
{
    public class FakeSignInManager<TUser> : SignInManager<TUser> where TUser : class
    {
        private Dictionary<string, SignInResult> Logins { get; set; }

        public FakeSignInManager(UserManager<TUser> userManager,
            IHttpContextAccessor contextAccessor,
            IUserClaimsPrincipalFactory<TUser> claimsFactory,
            IOptions<Microsoft.AspNetCore.Identity.IdentityOptions> optionsAccessor,
            ILogger<Microsoft.AspNetCore.Identity.SignInManager<TUser>> logger,
            IAuthenticationSchemeProvider schemes,
            IUserConfirmation<TUser> confirmation,
            Dictionary<string, SignInResult> logins) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes, confirmation)
        {
            this.Logins = logins;
        }

        public override Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)
        {
            if (this.Logins != null && this.Logins.ContainsKey(userName))
            {
                return Task.FromResult(this.Logins[userName]);
            }
            else
            {
                return Task.FromResult(SignInResult.Failed);
            }
        }
    }
}
