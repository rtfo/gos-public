﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgLedger.API.Data;
using DfT.GOS.GhgLedger.API.Models;
using DfT.GOS.GhgLedger.API.Repositories;
using DfT.GOS.GhgLedger.API.Services;
using DfT.GOS.GhgLedger.Common.Exceptions;
using DfT.GOS.Messaging.Client;
using DfT.GOS.Messaging.Messages;
using DfT.GOS.UnitTest.Common.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgLedger.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests for the LedgerService class
    /// </summary>
    [TestClass]
    public class LedgerServiceTest
    {
        #region Properties

        private Mock<ILedgerRepository> LedgerRepository { get; set; }
        private Mock<ITransactionRepository> TransactionRepository { get; set; }
        private Mock<IMessagingClient> MessagingClient { get; set; }
        private Mock<ILogger<LedgerService>> Logger { get; set; }
        private Mock<IOptions<AppSettings>> AppSettings { get; set; }
        private ILedgerService Service { get; set; }

        #endregion Properties

        #region Constants
        private const int THROTTLING_DURATION = 60;
        private const int OUTSIDE_THROTTLING_DURATION = 70;
        private const int INSIDE_THROTTLING_DURATION = 40;
        #endregion Constants

        #region Initialize

        /// <summary>
        /// Initializes the mock objects used for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.LedgerRepository = new Mock<ILedgerRepository>();
            this.TransactionRepository = new Mock<ITransactionRepository>();
            this.MessagingClient = new Mock<IMessagingClient>();
            this.Logger = new Mock<ILogger<LedgerService>>();
            this.AppSettings = new Mock<IOptions<AppSettings>>();
            this.AppSettings.Setup(s => s.Value).Returns(new AppSettings() { TransactionThrottlingDurationSeconds = THROTTLING_DURATION });

            this.Service = new LedgerService(this.LedgerRepository.Object, this.TransactionRepository.Object, this.MessagingClient.Object, this.Logger.Object, this.AppSettings.Object);
        }

        #endregion Initialize

        #region Tests

        #region Constructor tests

        /// <summary>
        /// Tests the constructor throws the expected exception when a null value is supplied for the ledgerRepository parameter
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "ledgerRepository", true)]
        public void LedgerService_Constructor_Throws_Exception_When_LedgerRepository_Parameter_Is_Null()
        {
            //act
            new LedgerService(null, this.TransactionRepository.Object, this.MessagingClient.Object, this.Logger.Object, this.AppSettings.Object);
        }

        /// <summary>
        /// Tests the constructor throws the expected exception when a null value is supplied for the messagingClient parameter
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "messagingClient", true)]
        public void LedgerService_Constructor_Throws_Exception_When_MessagingClient_Parameter_Is_Null()
        {
            //act
            new LedgerService(this.LedgerRepository.Object, this.TransactionRepository.Object, null, this.Logger.Object, this.AppSettings.Object);
        }

        /// <summary>
        /// Tests the constructor throws the expected exception when a null value is supplied for the logger parameter
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "logger", true)]
        public void LedgerService_Constructor_Throws_Exception_When_Logger_Parameter_Is_Null()
        {
            //act
            new LedgerService(this.LedgerRepository.Object, this.TransactionRepository.Object, this.MessagingClient.Object, null, this.AppSettings.Object);
        }

        /// <summary>
        /// Tests the constructor throws the expected exception when a null value is supplied for the app settings parameter
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentNullException), "appSettings", true)]
        public void LedgerService_Constructor_Throws_Exception_When_AppSettings_Parameter_Is_Null()
        {
            //act
            new LedgerService(this.LedgerRepository.Object, this.TransactionRepository.Object, this.MessagingClient.Object, this.Logger.Object, null);
        }

        #endregion Constructor tests

        #region Get tests

        /// <summary>
        /// Tests that the Get method can retrieve Transactions by Obligation Period Id and Organisation Id
        /// </summary>
        [TestMethod]
        public async Task LedgerService_Get_Retrieves_Ledger_By_ObligationPeriodId_And_OrganisationId()
        {
            //arrange
            var obligationPeriodId = 14;
            var organisationId = 24;
            this.LedgerRepository.Setup(mock => mock.Get(obligationPeriodId, organisationId))
                .ReturnsAsync(new TransactionSummary[]
                {
                    new TransactionSummary(0, 0, 0, 0, null, null, 0, 0, 0, 0.0m, new DateTime(), null, null)
                });

            //assert
            var result = await this.Service.Get(obligationPeriodId, organisationId);

            //act
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IEnumerable<TransactionSummary>));
            Assert.AreEqual(1, result.Count());
        }

        #endregion Get tests

        #region TransferCredits tests
        /// <summary>
        /// Tests the transaction repository and transaction item repository methods invoked when calling transfercredits
        /// Doesn't throttle when there have been no previous transactions
        /// </summary>
        [TestMethod]
        public async Task LedgerService_TransferCredits()
        {
            //arrange
            var orgId = 13;
            var recipientOrgId = 22;
            var obligationPeriodId = 15;
            decimal value = 2000;
            var user = "userid";
            var additionalReference = "aRef";

            this.LedgerRepository.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));

            //act
            var result = await this.Service.TransferCredits(obligationPeriodId, orgId
                , recipientOrgId, value, user, additionalReference);

            //assert
            Assert.AreEqual(1, result);
            this.LedgerRepository.Verify(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            this.MessagingClient.Verify(mock => mock.Publish(It.IsAny<LedgerUpdatedMessage>()), Times.Exactly(2));
            Assert.AreEqual(2, this.MessagingClient.Invocations.Count);
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.Count == 1));
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.First() != null));
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.First().GetType() == typeof(LedgerUpdatedMessage)));
            Assert.IsTrue(this.MessagingClient.Invocations.Any(inv => inv.Method.Name == "Publish"
                && inv.Arguments.Count == 1
                && (inv.Arguments.First() as LedgerUpdatedMessage).ObligationPeriodId == obligationPeriodId
                && (inv.Arguments.First() as LedgerUpdatedMessage).OrganisationId == orgId));
            Assert.IsTrue(this.MessagingClient.Invocations.Any(inv => inv.Method.Name == "Publish"
                && inv.Arguments.Count == 1
                && (inv.Arguments.First() as LedgerUpdatedMessage).ObligationPeriodId == obligationPeriodId
                && (inv.Arguments.First() as LedgerUpdatedMessage).OrganisationId == recipientOrgId));
        }

        /// <summary>
        /// Tests that the TransferCredits method completes successfully even when publishing the LedgerUpdatedMessages fails
        /// </summary>
        [TestMethod]
        public async Task LedgerService_TransferCredits_Handles_Errors_When_Publishing_Messages()
        {
            //arrange
            var orgId = 13;
            var recipientOrgId = 22;
            var obligationPeriodId = 15;
            decimal value = 2000;
            var user = "userid";
            var additionalReference = "aRef";

            this.LedgerRepository.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));
            this.MessagingClient.Setup(mock => mock.Publish(It.IsAny<LedgerUpdatedMessage>()))
                .ThrowsAsync(new Exception("test"));

            //act
            var result = await this.Service.TransferCredits(obligationPeriodId, orgId
                , recipientOrgId, value, user, additionalReference);

            //assert
            Assert.AreEqual(1, result);
            this.LedgerRepository.Verify(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            this.MessagingClient.Verify(mock => mock.Publish(It.IsAny<LedgerUpdatedMessage>()), Times.Once);
            Assert.AreEqual(1, this.MessagingClient.Invocations.Count);
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.Count == 1));
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.First() != null));
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.First().GetType() == typeof(LedgerUpdatedMessage)));
            Assert.IsTrue(this.MessagingClient.Invocations.Any(inv => inv.Method.Name == "Publish"
                && inv.Arguments.Count == 1
                && (inv.Arguments.First() as LedgerUpdatedMessage).ObligationPeriodId == obligationPeriodId
                && (inv.Arguments.First() as LedgerUpdatedMessage).OrganisationId == orgId));
        }


        ///<summary>
        /// Tests that TransferCredits throws a TransactionThrottlingException when the last transfer was within the time set by AppSettings
        ///</summary>
        [TestMethod]
        public async Task LedgerService_TransferCredits_Throws_Exception_When_It_Should_Be_Throttling()
        {
            // Arrange 
            var orgId = 13;
            var recipientOrgId = 22;
            var obligationPeriodId = 15;
            decimal value = 2000;
            var user = "userid";
            var additionalReference = "aRef";

            this.LedgerRepository.Setup(r => r.GetMostRecent(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(new TransactionSummary(
                    It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(),
                    It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(),
                    It.IsAny<decimal>(), DateTime.Now.AddSeconds(-INSIDE_THROTTLING_DURATION),
                    It.IsAny<string>(), It.IsAny<string>()
                    )));

            // Act and Assert 
            await Assert.ThrowsExceptionAsync<TransactionThrottlingException>(() => this.Service.TransferCredits(obligationPeriodId, orgId
                , recipientOrgId, value, user, additionalReference));
        }

        /// <summary>
        /// Tests the transaction is not throttled when an previous transaction has been carried out for the organisation and obligation period
        /// but is outside the throttling limit
        /// </summary>
        [TestMethod]
        public async Task LedgerService_TransferCredits_Does_Not_Throttle_When_It_Should_Not()
        {
            //arrange
            var orgId = 13;
            var recipientOrgId = 22;
            var obligationPeriodId = 15;
            decimal value = 2000;
            var user = "userid";
            var additionalReference = "aRef";

            this.LedgerRepository.Setup(r => r.GetMostRecent(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(new TransactionSummary(
                    It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(),
                    It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(),
                    It.IsAny<decimal>(), DateTime.Now.AddSeconds(-OUTSIDE_THROTTLING_DURATION),
                    It.IsAny<string>(), It.IsAny<string>()
                    )));
            this.LedgerRepository.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));

            //act
            var result = await this.Service.TransferCredits(obligationPeriodId, orgId
                , recipientOrgId, value, user, additionalReference);

            //assert
            Assert.AreEqual(1, result);
            this.LedgerRepository.Verify(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            this.MessagingClient.Verify(mock => mock.Publish(It.IsAny<LedgerUpdatedMessage>()), Times.Exactly(2));
            Assert.AreEqual(2, this.MessagingClient.Invocations.Count);
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.Count == 1));
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.First() != null));
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.First().GetType() == typeof(LedgerUpdatedMessage)));
            Assert.IsTrue(this.MessagingClient.Invocations.Any(inv => inv.Method.Name == "Publish"
                && inv.Arguments.Count == 1
                && (inv.Arguments.First() as LedgerUpdatedMessage).ObligationPeriodId == obligationPeriodId
                && (inv.Arguments.First() as LedgerUpdatedMessage).OrganisationId == orgId));
            Assert.IsTrue(this.MessagingClient.Invocations.Any(inv => inv.Method.Name == "Publish"
                && inv.Arguments.Count == 1
                && (inv.Arguments.First() as LedgerUpdatedMessage).ObligationPeriodId == obligationPeriodId
                && (inv.Arguments.First() as LedgerUpdatedMessage).OrganisationId == recipientOrgId));
        }
        #endregion TransferCredits tests

        #region Performance Data tests

        ///<summary>
        /// Tests the expected values are returned for the ledger metrics
        ///</summary>
        [TestMethod]
        public async Task GetLedgerPerformanceData_Returns_Correct_Values_For_The_Ledger_Metrics()
        {
            // Arrange
            var expectedNoOfTransferTransactionsMonthThree = 0;
            var expectedNoOfTransferTransactionsMonthFour = 0;
            var expectedNoOfTransferTransactionsCurrentMonth = 1;

            var options = new DbContextOptionsBuilder<GosLedgerDataContext>()
                .UseInMemoryDatabase(databaseName: "Transaction")
                .Options;
            this.SetupApplicationsPerformanceData(options);

            using (var context = new GosLedgerDataContext(options))
            {
                // Act
                this.Service = new LedgerService(this.LedgerRepository.Object, new TransactionRepository(context), this.MessagingClient.Object, this.Logger.Object, this.AppSettings.Object);
                var result = await this.Service.GetLedgerPerformanceData();

                // Assert 
                Assert.AreEqual(expectedNoOfTransferTransactionsMonthThree, result[2].NoOfTransferTransactions);
                Assert.AreEqual(expectedNoOfTransferTransactionsMonthFour, result[3].NoOfTransferTransactions);
                Assert.AreEqual(expectedNoOfTransferTransactionsCurrentMonth, result.Last().NoOfTransferTransactions);
            }

        }
        #endregion Performance Data tests

        #endregion Tests

        #region Private Methods

        private void SetupApplicationsPerformanceData(DbContextOptions<GosLedgerDataContext> options)
        {
            var transactionTypes = new List<Data.TransactionType>();

            Data.TransactionType getTransactionType(int transactionTypeId, string type, string description)
            {
                var transactionType = transactionTypes.SingleOrDefault(t => t.TransactionTypeId == transactionTypeId);
                if (transactionType == null)
                {
                    transactionType = new Data.TransactionType(transactionTypeId, type, description);
                    transactionTypes.Add(transactionType);
                }

                return transactionType;
            };

            var transactions = new List<Transaction>
            {
                new Transaction
                {
                    TransactionId = 1,
                    ObligationPeriodId = 15,
                    TransactionTypeId = (int)Models.TransactionType.Transfer,
                    TransactionType = getTransactionType((int)Models.TransactionType.Transfer, "A", "BC"),
                    CreatedDate = new DateTime(2019, 01, 05),
                    CreatedBy = "A"
                },
                new Transaction
                {
                    TransactionId = 2,
                    ObligationPeriodId = 15,
                    TransactionTypeId = (int)Models.TransactionType.Transfer,
                    TransactionType = getTransactionType((int)Models.TransactionType.Transfer, "A", "BC"),
                    CreatedDate = new DateTime(2019, 02, 05),
                    CreatedBy = "A"
                },
                new Transaction
                {
                    TransactionId = 3,
                    ObligationPeriodId = 15,
                    TransactionTypeId = (int)Models.TransactionType.Transfer,
                    TransactionType = getTransactionType((int)Models.TransactionType.Transfer, "A", "BC"),
                    CreatedDate = new DateTime(2019, 02, 10),
                    CreatedBy = "A"
                },
                new Transaction
                {
                    TransactionId = 4,
                    ObligationPeriodId = 15,
                    TransactionTypeId = (int)Models.TransactionType.Redeem,
                    TransactionType = getTransactionType((int)Models.TransactionType.Redeem, "A", "BC"),
                    CreatedDate = new DateTime(2019, 03, 05),
                    CreatedBy = "A"
                },
                new Transaction
                {
                    TransactionId = 5,
                    ObligationPeriodId = 15,
                    TransactionTypeId = (int)Models.TransactionType.Revoke,
                    TransactionType = getTransactionType((int)Models.TransactionType.Revoke, "A", "BC"),
                    CreatedDate = new DateTime(2019, 04, 05),
                    CreatedBy = "A"
                },
                new Transaction
                {
                    TransactionId = 6,
                    ObligationPeriodId = 15,
                    TransactionTypeId = (int)Models.TransactionType.Transfer,
                    TransactionType = getTransactionType((int)Models.TransactionType.Transfer, "A", "BC"),
                    CreatedDate = new DateTime(2019, 05, 05),
                    CreatedBy = "A"
                },
                new Transaction
                {
                    TransactionId = 7,
                    ObligationPeriodId = 15,
                    TransactionTypeId = (int)Models.TransactionType.Transfer,
                    TransactionType = getTransactionType((int)Models.TransactionType.Transfer, "A", "BC"),
                    CreatedDate = DateTime.Now,
                    CreatedBy = "A"
                }
            };

            using (var context = new GosLedgerDataContext(options))
            {

                transactions.ForEach(a => context.Transaction.Add(a));
                context.SaveChanges();
            }

        }
        #endregion Private Methods
    }
}
