﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgLedger.Common.Models;
using DfT.GOS.GhgLedger.API.Services;
using DfT.GOS.Security.Claims;
using Dft.GOS.GhgLedger.API.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.GhgLedger.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests for the Performance Data Controller
    /// </summary>
    [TestClass]
    public class PerformanceDataControllerTest
    {

        #region Constants
        private const string UserId = "123";
        private const string OrganisationId = "xyz";
        #endregion Constants

        #region Properties
        private PerformanceDataController Controller { get; set; }
        private Mock<ILedgerService> MockLedgerService { get; set; }
        #endregion Properties

        #region Initialize
        /// <summary>
        /// Sets up the controller for tests before each test is run
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.MockLedgerService = new Mock<ILedgerService>();
            this.Controller = new PerformanceDataController(this.MockLedgerService.Object);
            var claims = new List<Claim>
            {
                new Claim(Claims.UserId, UserId),
                new Claim(Claims.OrganisationId, OrganisationId)
            };
            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }
        #endregion Initialize

        #region LedgerPerformanceData tests

        ///<summary>
        /// Registrations Data forwards on the list of data
        ///</summary>
        [TestMethod]
        public async Task LedgerPerformanceData_Returns_The_Ledger_Performance_Data()
        {
            // Arrange
            this.MockLedgerService.Setup(s => s.GetLedgerPerformanceData())
                .Returns(Task.FromResult(new List<LedgerPerformanceData>
                {
                    new LedgerPerformanceData(DateTime.Now, 1),
                    new LedgerPerformanceData(DateTime.Now.AddMonths(-1),1)
                }));

            // Act
            var result = await this.Controller.LedgerPerformanceData();

            // Assert 
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(result);

        }
        #endregion LedgerPerformanceData tests
    }
}
