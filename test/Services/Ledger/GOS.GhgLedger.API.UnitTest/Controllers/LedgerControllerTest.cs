﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Dft.GOS.GhgLedger.API.Controllers;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.GhgLedger.API.Commands;
using DfT.GOS.GhgLedger.API.Models;
using DfT.GOS.GhgLedger.API.Services;
using DfT.GOS.GhgLedger.Common.Exceptions;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.GhgLedger.API.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the Ledger Controller
    /// </summary>
    [TestClass]
    public class LedgerControllerTest
    {
        #region Constants

        private const int DefaultOrganisationId = 13;

        #endregion Constants

        #region Properties

        private Mock<ILedgerService> LedgerService { get; set; }
        private Mock<IBalanceViewService> BalanceViewService { get; set; }
        private Mock<IOrganisationsService> OrganisationsService { get; set; }
        private Mock<IReportingPeriodsService> ReportingPeriodsService { get; set; }
        private Mock<IIdentityService> IdentityService { get; set; }
        private Mock<IOptions<AppSettings>> AppSettings { get; set; }
        private LedgerController Controller { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Initializes the objects used for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.LedgerService = new Mock<ILedgerService>();
            this.OrganisationsService = new Mock<IOrganisationsService>();
            this.ReportingPeriodsService = new Mock<IReportingPeriodsService>();
            this.BalanceViewService = new Mock<IBalanceViewService>();
            this.IdentityService = new Mock<IIdentityService>();
            var claims = new Claim[]
            {
                new Claim(Claims.UserId, Guid.NewGuid().ToString()),
                new Claim(Claims.OrganisationId, DefaultOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
            var appSettings = new AppSettings() { SERVICE_IDENTIFIER = "1234" };
            var options = Options.Create(appSettings);
            this.Controller = new LedgerController(this.LedgerService.Object, this.BalanceViewService.Object
                , OrganisationsService.Object, ReportingPeriodsService.Object, this.IdentityService.Object, options);
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
            this.IdentityService.Setup(x => x.AuthenticateService(It.IsAny<string>()))
                .Returns(Task.FromResult(new AuthenticationResult() { Token = "ServiceToken" }));
        }

        #endregion Initialize

        #region Tests

        #region Get by Obligation Period and Organisation tests
        /// <summary>
        /// Tests that the Get method handles the case where the specified Supplier does not exist
        /// </summary>
        [TestMethod]
        public async Task LedgerController_Get_Returns_NotFound_To_Wrong_Supplier()
        {
            //arrange
            var obligationPeriodId = 140;
            var organisationId = 2400;
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            //act
            var result = await this.Controller.Get(obligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the Get method handles the case where the specified obligation period id does not exist
        /// </summary>
        [TestMethod]
        public async Task LedgerController_Get_Returns_NotFound_To_ObligationPeriod()
        {
            //arrange
            var obligationPeriodId = 140;
            var organisationId = 2400;
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(null));
            //act
            var result = await this.Controller.Get(obligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the Get method retrieves Tranactions by Obligation Period Id and Supplier Id
        /// </summary>
        [TestMethod]
        public async Task LedgerController_Get_Retrieves_Transactions_By_ObligationPeriodId_And_SupplierId()
        {
            //arrange
            var obligationPeriodId = 14;
            var organisationId = DefaultOrganisationId;
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            this.LedgerService.Setup(mock => mock.Get(obligationPeriodId, organisationId))
            .ReturnsAsync(new TransactionSummary[]
                {
                    new TransactionSummary(0, 0, 0, 0, null, null, 0, 0, 0, 0.0m, new DateTime(), null, null)
                });

            //act
            var result = await this.Controller.Get(obligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IEnumerable<TransactionSummary>));
            Assert.AreEqual(1, ((IEnumerable<TransactionSummary>)(((OkObjectResult)result).Value)).Count());
        }

        /// <summary>
        /// Tests that the Get method handles the case where the user does not have access to the specified Supplier
        /// </summary>
        [TestMethod]
        public async Task LedgerController_Get_Forbids_Access_To_Wrong_Supplier()
        {
            //arrange
            var obligationPeriodId = 140;
            var organisationId = 2400;
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            //act
            var result = await this.Controller.Get(obligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Get method allows Admin users to access data
        /// </summary>
        [TestMethod]
        public async Task LedgerController_Get_Allows_Admins_Access_To_Any_Supplier()
        {
            //arrange
            var obligationPeriodId = 14;
            var organisationId = DefaultOrganisationId;
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            this.LedgerService.Setup(mock => mock.Get(obligationPeriodId, organisationId))
            .ReturnsAsync(new TransactionSummary[]
                {
                    new TransactionSummary(0, 0, 0, 0, null, null, 0, 0, 0, 0.0m, new DateTime(), null, null)
                });
            var claims = new Claim[]
            {
                new Claim(Claims.UserId, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, Roles.Administrator)
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };

            //act
            var result = await this.Controller.Get(obligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IEnumerable<TransactionSummary>));
            Assert.AreEqual(1, ((IEnumerable<TransactionSummary>)(((OkObjectResult)result).Value)).Count());
        }

        #endregion Get by Obligation Period and Organisation tests

        #region Transfer Credits
        /// <summary>
        /// Tests that user from other organisation not able tot access the transfer credit api
        /// </summary>
        [TestMethod]
        public async Task LedgerController_TransferCredits_Returns_Forbid()
        {
            //arrange
            var cmd = new TransferCreditsCommand()
            {
                OrganisationId = 222,
                ObligationPeriodId = 12
            };
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
               .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>(), It.IsAny<int>(),
               It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            //act
            var result = await this.Controller.TransferCredits(cmd);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }
        /// <summary>
        /// Tests the transfer credit value is within the available credits
        /// </summary>
        [TestMethod]
        public async Task LedgerController_TransferCredits_Returns_BadRequest_If_Transfer_Value_Exceeds_Balance()
        {
            //arrange
            var cmd = new TransferCreditsCommand()
            {
                OrganisationId = DefaultOrganisationId,
                ObligationPeriodId = 12,
                Value = 1000
            };
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            this.BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            //act
            var result = await this.Controller.TransferCredits(cmd);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }
        /// <summary>
        /// Tests a supplier cannot create a transfer to themselves
        /// </summary>
        [TestMethod]
        public async Task LedgerController_TransferCredits_Returns_BadRequest_If_OrganisationId_Matches_Current_Supplier()
        {
            //arrange
            var cmd = new TransferCreditsCommand()
            {
                OrganisationId = DefaultOrganisationId,
                ObligationPeriodId = 12,
                RecipientOrganisationId = DefaultOrganisationId,
                Value = 0
            };
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            this.BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            //act
            var result = await this.Controller.TransferCredits(cmd);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }
        /// <summary>
        /// Tests a transfer cannot be made outside the Transfer Window for the Obligation Period
        /// </summary>
        [TestMethod]
        public async Task LedgerController_TransferCredits_Returns_BadRequest_If_Outside_Transfer_Window()
        {
            //arrange
            var cmd = new TransferCreditsCommand()
            {
                OrganisationId = DefaultOrganisationId,
                ObligationPeriodId = 12,
                RecipientOrganisationId = 14,
                Value = 10
            };
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), DateTime.Now.AddDays(-1), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            this.BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            //act
            var result = await this.Controller.TransferCredits(cmd);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }
        /// <summary>
        /// Tests the api returns transaction id after successful credit transfer
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task LedgerController_TransferCredits_Returns_TransactionId()
        {
            //arrange
            var cmd = new TransferCreditsCommand()
            {
                OrganisationId = DefaultOrganisationId,
                Value = 90,
                RecipientOrganisationId = 11,
                ObligationPeriodId = 13,

            };
            this.OrganisationsService.Setup(x => x.GetOrganisation(DefaultOrganisationId, It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>()))));
            this.OrganisationsService.Setup(x => x.GetOrganisation(11, It.IsAny<string>()))
             .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
             It.IsAny<string>(), It.IsAny<int>(),
             It.IsAny<string>(), It.IsAny<int>(),
             It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), DateTime.Now.AddDays(1), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            this.BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            this.LedgerService.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));

            //act
            var result = await this.Controller.TransferCredits(cmd);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            this.LedgerService.Verify(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }
        /// <summary>
        /// Tests the api returns a notfound result when invalid organisation is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task LedgerController_TransferCredits_Returns_NotFound_Invalid_Organisation()
        {
            //arrange
            var cmd = new TransferCreditsCommand()
            {
                OrganisationId = 222
            };
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            //act
            var result = await this.Controller.TransferCredits(cmd);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }
        /// <summary>
        /// Tests the api returns a notfound result when invalid recipient organisation is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task LedgerController_TransferCredits_Returns_NotFound_Invalid_Recipient_Organisation()
        {
            //arrange
            var cmd = new TransferCreditsCommand()
            {
                OrganisationId = DefaultOrganisationId,
                RecipientOrganisationId = 331,
                ObligationPeriodId = 22
            };
            this.OrganisationsService.Setup(x => x.GetOrganisation(DefaultOrganisationId, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>()))));
            this.OrganisationsService.Setup(x => x.GetOrganisation(331, It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound)));
            //act
            var result = await this.Controller.TransferCredits(cmd);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }
        /// <summary>
        /// Tests the api returns a notfound result when invalid obligation period is passed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task LedgerController_TransferCredits_Returns_NotFound_Invalid_ObligationPeriod()
        {
            //arrange
            var cmd = new TransferCreditsCommand()
            {
                OrganisationId = 222,
                ObligationPeriodId = 13131
            };
            this.OrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
                It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(null));
            //act
            var result = await this.Controller.TransferCredits(cmd);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }


        ///<summary>
        /// Tests the API returns a Too Many Requests status when the service is being throttled
        ///</summary>
        [TestMethod]
        public async Task LedgerController_TransferCredits_Rerturns_TooManyRequests_When_Service_Is_Throttling()
        {
            // Arrange 
            var cmd = new TransferCreditsCommand()
            {
                OrganisationId = DefaultOrganisationId,
                Value = 90,
                RecipientOrganisationId = 11,
                ObligationPeriodId = 13,

            };
            this.OrganisationsService.Setup(x => x.GetOrganisation(DefaultOrganisationId, It.IsAny<string>()))
              .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>(), It.IsAny<int>(),
              It.IsAny<string>()))));
            this.OrganisationsService.Setup(x => x.GetOrganisation(11, It.IsAny<string>()))
             .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
             It.IsAny<string>(), It.IsAny<int>(),
             It.IsAny<string>(), It.IsAny<int>(),
             It.IsAny<string>()))));
            this.ReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), DateTime.Now.AddDays(1), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            this.BalanceViewService.Setup(x => x.GetSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<BalanceSummary>(new BalanceSummary(100, 100, 100, 100, 100, 100, 100, 100))));
            this.LedgerService.Setup(x => x.TransferCredits(It.IsAny<int>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>()))
                .Throws(new TransactionThrottlingException());

            // Act
            var result = await this.Controller.TransferCredits(cmd);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(ObjectResult));
            Assert.AreEqual((int)HttpStatusCode.TooManyRequests, ((ObjectResult)result).StatusCode);
        }
        #endregion Transfer Credits

        #endregion Tests
    }
}
