// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

using DfT.GOS.GhgLedger.API.Data;
using DfT.GOS.GhgLedger.API.Repositories;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System;

namespace DfT.GOS.GhgLedger.API.UnitTest.Repositories
{
    /// <summary>
    /// Unit tests for the LedgerRepository class
    /// </summary>
    [TestClass]
    public class LedgerRepositoryTest
    {
        #region Properties
        private Mock<ITransactionRepository> TransactionRepository { get; set; }
        private Mock<ITransactionItemRepository> TransactionItemRepository { get; set; }
        private Mock<DatabaseFacade> MockDatabase { get; set; }
        private ILedgerRepository LedgerRepository { get; set; }
        #endregion Properties

        #region Initialize
        [TestInitialize]
        public void Initialize()
        {
            this.TransactionRepository = new Mock<ITransactionRepository>();
            this.TransactionItemRepository = new Mock<ITransactionItemRepository>();
            this.MockDatabase = new Mock<DatabaseFacade>();

        }
        #endregion Initialize

        #region Get tests
        /// <summary>
        /// Tests that the Get method can retrieve Transactions by Obligation Period Id and Organisation Id
        /// </summary>
        [TestMethod]
        public async Task LedgerRepository_Get_Retrieves_Ledger_By_OblicationPeriodId_And_OrganisationId()
        {
            //arrange
            var obligationPeriodId = 14;
            var secondOblicationPeriod = 15;
            var organisationId = 24;
            var secondOrgId = 25;

            var options = new DbContextOptionsBuilder<GosLedgerDataContext>()
                .UseInMemoryDatabase(databaseName: "Test_Get")
                .Options;

            var transactionItems = CreateTransactionItems(
                new TransactionItemArgs(obligationPeriodId, organisationId, 1, 1, "user1", (int)Models.TransactionType.Transfer, "Redeem", "description", "additional ref"),
                new TransactionItemArgs(obligationPeriodId, secondOrgId, 1, 2, "user2", (int)Models.TransactionType.Redeem, "Redeem", "description", "additional ref"),
                new TransactionItemArgs(obligationPeriodId, organisationId, 2, 5, "user1", (int)Models.TransactionType.Transfer, "Redeem", "description2", null),
                new TransactionItemArgs(obligationPeriodId, secondOrgId, 2, 6, "user2", (int)Models.TransactionType.Redeem, "Redeem", "description2", null),
                new TransactionItemArgs(obligationPeriodId, secondOrgId, 3, 3, "user7", (int)Models.TransactionType.Transfer, "Transfer", "Second transaction type", "additional ref"),
                new TransactionItemArgs(secondOblicationPeriod, organisationId, 4, 4, "user2", (int)Models.TransactionType.Redeem, "Redeem", "description", null));
            
            PopulateInMemoryDB(options, transactionItems);

            using (var context = new GosLedgerDataContext(options))
            {
                this.LedgerRepository = new LedgerRepository(context, this.TransactionRepository.Object, this.TransactionItemRepository.Object);

                //act         
                var results = await this.LedgerRepository.Get(obligationPeriodId, organisationId);

                //assert
                Assert.AreEqual(2, results.Count());
                Assert.AreEqual(organisationId, results.First().OrganisationId);
            }
        }
        #endregion Get tests

        #region GetMostRecent tests

        ///<summary>
        /// Tests the most recent transaction for the given obligation period and organisation is returned
        ///</summary>
        [TestMethod]
        public async Task LedgerRepository_GetMostRecent_Returns_The_Most_Recent_Transaction()
        {
            // Arrange 
            var obligationPeriodId = 14;
            var secondOblicationPeriod = 15;
            var organisationId = 24;
            var secondOrgId = 25;
            var expectedTransactionId = 2;

            var options = new DbContextOptionsBuilder<GosLedgerDataContext>()
                .UseInMemoryDatabase(databaseName: "Test_GetMostRecent")
                .Options;

            var transactionItems = CreateTransactionItems(
                new TransactionItemArgs(obligationPeriodId, organisationId, 1, 1, "user1", (int)Models.TransactionType.Redeem, "Redeem", "description", "additional ref"),
                new TransactionItemArgs(obligationPeriodId, secondOrgId, 1, 2, "user2", (int)Models.TransactionType.Redeem, "Redeem", "description", null),
                new TransactionItemArgs(obligationPeriodId, organisationId, 2, 5, "user1", (int)Models.TransactionType.Transfer, "Redeem", "description2", null),
                new TransactionItemArgs(obligationPeriodId, secondOrgId, 2, 6, "user2", (int)Models.TransactionType.Redeem, "Redeem", "description2", null),
                new TransactionItemArgs(obligationPeriodId, secondOrgId, 3, 3, "user7", (int)Models.TransactionType.Transfer, "Transfer", "Second transaction type", "additional ref"),
                new TransactionItemArgs(secondOblicationPeriod, organisationId, 4, 4, "user2", (int)Models.TransactionType.Redeem, "Redeem", "description", "additional ref"));

            PopulateInMemoryDB(options, transactionItems);

            using (var context = new GosLedgerDataContext(options))
            {
                this.LedgerRepository = new LedgerRepository(context, this.TransactionRepository.Object, this.TransactionItemRepository.Object);

                // Act
                var result = await this.LedgerRepository.GetMostRecent(obligationPeriodId, organisationId);

                // Assert
                Assert.AreEqual(expectedTransactionId, result.TransactionId);
            }
        }

        ///<summary>
        /// Tests that Null is returned if there are no transactions for the given obligation period and organisation
        ///</summary>
        [TestMethod]
        public async Task LedgerRepository_GetMostRecent_Returns_Null_When_No_Transactions_Are_Found()
        {
            // Arrange 
            var obligationPeriodId = 14;
            var organisationId = 24;
            var options = new DbContextOptionsBuilder<GosLedgerDataContext>()
                .UseInMemoryDatabase(databaseName: "Test_GetMostRecent")
                .Options;

            using (var context = new GosLedgerDataContext(options))
            {
                context.Database.EnsureDeleted();
                this.LedgerRepository = new LedgerRepository(context, this.TransactionRepository.Object, this.TransactionItemRepository.Object);


                // Act
                var result = await this.LedgerRepository.GetMostRecent(obligationPeriodId, organisationId);

                // Assert 
                Assert.IsNull(result);
            }
        }
        #endregion GetMostRecent tests

        #region Transfer credits tests
        /// <summary>
        /// Tests that the TransferCredits method creates a transaction
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Creates_A_Transaction()
        {
            //arrange
            var obligationPeriodId = 15;
            var fromOrganisationId = 1;
            var toOrganisationId = 2;
            var value = 1.7m;
            var userId = "User1";
            var additionalReference = "aRef";

            var options = new DbContextOptionsBuilder<GosLedgerDataContext>()
                .UseInMemoryDatabase(databaseName: "Test_Transfer")
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;
            var context = new GosLedgerDataContext(options);

            this.TransactionRepository.Setup(r => r.CreateTransaction(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));
            this.LedgerRepository = new LedgerRepository(context, this.TransactionRepository.Object, this.TransactionItemRepository.Object);

            //act
            var transactionId = await this.LedgerRepository.TransferCredits(obligationPeriodId, fromOrganisationId, toOrganisationId, value, userId, additionalReference);

            //assert
            Assert.AreNotEqual(0, transactionId);
            this.TransactionRepository.Verify(r => r.CreateTransaction(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

        }

        /// <summary>
        /// Tests that the TransferCredits method creates an in and an out transaction
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Creates_In_And_Out_Transactions()
        {
            //arrange
            var obligationPeriodId = 15;
            var fromOrganisationId = 1;
            var toOrganisationId = 2;
            var value = 1.7m;
            var userId = "User1";
            var additionalReference = "aRef";

            var options = new DbContextOptionsBuilder<GosLedgerDataContext>()
                .UseInMemoryDatabase(databaseName: "Test_Transfer")
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;
            var context = new GosLedgerDataContext(options);

            this.TransactionRepository.Setup(r => r.CreateTransaction(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));
            this.LedgerRepository = new LedgerRepository(context, this.TransactionRepository.Object, this.TransactionItemRepository.Object);

            //act
            var transactionId = await this.LedgerRepository.TransferCredits(obligationPeriodId, fromOrganisationId, toOrganisationId, value, userId, additionalReference);

            //assert
            Assert.AreNotEqual(0, transactionId);
            this.TransactionItemRepository.Verify(ir => ir.CreateTransactionItem(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>()), Times.Exactly(2));

        }

        /// <summary>
        /// Tests that when an exception is thrown a transaction id of 0 is returned
        /// </summary>
        [TestMethod]
        public async Task TransferCredits_Returns_A_Zero_TransactionId_On_Exceptions()
        {
            //arrange
            var obligationPeriodId = 15;
            var fromOrganisationId = 1;
            var toOrganisationId = 2;
            var value = 1.7m;
            var userId = "User1";
            var additionalReference = "aRef";

            var options = new DbContextOptionsBuilder<GosLedgerDataContext>()
                .UseInMemoryDatabase(databaseName: "Test_Transfer")
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;
            var context = new GosLedgerDataContext(options);

            this.TransactionRepository.Setup(r => r.CreateTransaction(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));

            this.TransactionItemRepository.Setup(r => r.CreateTransactionItem(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>()))
                .Throws(new DbUpdateException("Database exception", new Exception()));
            this.LedgerRepository = new LedgerRepository(context, this.TransactionRepository.Object, this.TransactionItemRepository.Object);

            //act
            var transactionId = await this.LedgerRepository.TransferCredits(obligationPeriodId, fromOrganisationId, toOrganisationId, value, userId, additionalReference);

            //assert
            Assert.AreEqual(0, transactionId);
            this.TransactionItemRepository.Verify(ir => ir.CreateTransactionItem(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>()), Times.Once());

        }
        #endregion Transfer credits tests

        #region Test data helpers
        /// <summary>
        /// Writes the provided list of transaction items to the in-memory database using the provided options.
        /// <param name="options">Options that define the database context</param>
        /// <param name="transactionItems">List of transaction items to store</param>
        /// </summary>
        private static void PopulateInMemoryDB(DbContextOptions<GosLedgerDataContext> options, List<TransactionItem> transactionItems)
        {
            using (var context = new GosLedgerDataContext(options))
            {
                foreach (var transactionItem in transactionItems)
                {
                    context.TransactionItem.Add(transactionItem);
                }

                context.SaveChanges();
            }
        }

        /// <summary>
        /// Creates a transaction item with a transaction.
        /// <param name="obligationPeriodId">Id of the Obligation Period of the transaction</param>
        /// <param name="organisationId">Id of the Organisation involved in the transaction</param>
        /// <param name="transactionId">Id of the transaction</param>
        /// <param name="userId">Id of the user who carried out the transaction</param>
        /// <param name="transactionTypeId">Id of the transaction type</param>
        /// <param name="type">Type of transaction</param>
        /// <param name="description">Description of the transaction</param>
        /// </summary>
        private static List<TransactionItem> CreateTransactionItems(params TransactionItemArgs[] transactionItemArgs)
        {
            List<Transaction> transactions = new List<Transaction>();
            List<TransactionType> transactionTypes = new List<TransactionType>();
            List<TransactionItemType> transactionItemTypes = new List<TransactionItemType>();

            var rand = new Random();

            TransactionType getTransactionType(int transactionTypeId, string type, string description)
            {
                var transactionType = transactionTypes.SingleOrDefault(t => t.TransactionTypeId == transactionTypeId);
                if (transactionType == null)
                {
                    transactionType = new TransactionType(transactionTypeId, type, description);
                    transactionTypes.Add(transactionType);
                }

                return transactionType;
            };

            TransactionItemType getTransactionItemType(int transactionitemTypeId, string type, string description)
            {
                var transactionItemType = transactionItemTypes.SingleOrDefault(t => t.TransactionItemTypeId == transactionitemTypeId);

                if (transactionItemType == null)
                {
                    transactionItemType = new TransactionItemType(transactionitemTypeId, type, description);
                    transactionItemTypes.Add(transactionItemType);
                }
                return transactionItemType;
            }

            Transaction getTransaction(TransactionItemArgs args)
            {
                var transaction = transactions.SingleOrDefault(t => t.TransactionId == args.TransactionId);

                if (transaction == null)
                {
                    transaction = new Transaction
                    {
                        TransactionId = args.TransactionId,
                        ObligationPeriodId = args.ObligationPeriodId,
                        CreatedDate = DateTime.Now,
                        CreatedBy = args.UserId,
                        TransactionType = getTransactionType(args.TransactionTypeId, args.Type, args.Description),
                        AdditionalReference = args.AdditionalReference
                    };
                    transactions.Add(transaction);
                }

                return transaction;
            };

            return transactionItemArgs.Select(t => new TransactionItem
            {
                Transaction = getTransaction(t),
                TransactionItemId = t.TransactionItemId,
                TransactionId = t.TransactionId,
                TransactionItemType = getTransactionItemType(t.TransactionItemId, t.Type, t.Description),
                TransactionItemTypeId = t.TransactionTypeId,
                OrganisationId = t.OrganisationId,
                Value = (Decimal)((rand.NextDouble() * Math.Abs(10 - 0.1)) + 0.1)
            }).ToList();
        }
        #endregion Test data helpers

        class TransactionItemArgs
        {
            public TransactionItemArgs(int obligationPeriodId, int organisationId, int transactionId, int transactionItemId, string userId, int transactionTypeId, string type, string description, string additionalReference)
            {
                ObligationPeriodId = obligationPeriodId;
                OrganisationId = organisationId;
                TransactionId = transactionId;
                TransactionItemId = transactionItemId;
                UserId = userId;
                TransactionTypeId = transactionTypeId;
                Type = type;
                Description = description;
                AdditionalReference = additionalReference;
            }

            public int ObligationPeriodId;
            public int OrganisationId;
            public int TransactionId;
            public int TransactionItemId;
            public string UserId;
            public int TransactionTypeId;
            public string Type;
            public string Description;
            public string AdditionalReference;
        }
    }
}