﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgLedger.API.Data;
using DfT.GOS.GhgLedger.API.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DfT.GOS.GhgLedger.API.UnitTest.Repositories
{
    /// <summary>
    /// Unit tests for the TransactionItemRepository
    /// </summary>
    [TestClass]
    public class TransactionItemRepositoryTest
    {

        #region Properties
        private ITransactionItemRepository TransactionItemRepository { get; set; }
        #endregion Properties

        #region Create transaction item tests
        [TestMethod]
        public async Task CreateTransactionItem_Writes_A_TransactionItem()
        {
            //arrange
            var transactionTypeId = (int)Models.TransactionType.Redeem;
            var organisationId = 3;
            var transactionItem = new TransactionItem
            {
                TransactionId = 1,
                OrganisationId = 3,
                TransactionItemTypeId = (int)Models.TransactionType.Redeem,
                Value = 0.7m
            };

            var options = new DbContextOptionsBuilder<GosLedgerDataContext>()
                .UseInMemoryDatabase(databaseName: "Test_Create_Transaction_Item")
                .Options;

            var context = new GosLedgerDataContext(options);
            this.TransactionItemRepository = new TransactionItemRepository(context);

            //act
            await this.TransactionItemRepository.CreateTransactionItem(transactionItem.TransactionId, transactionItem.TransactionItemTypeId, transactionItem.OrganisationId, transactionItem.Value);
            var transactionId = await this.TransactionItemRepository.CreateTransactionItem(2, transactionTypeId, organisationId, 1.4m);
            var transactionItems = await context.TransactionItem.ToListAsync();

            //assert
            Assert.AreEqual(transactionItem.TransactionId, transactionItems[0].TransactionId);
            Assert.AreEqual(transactionItem.OrganisationId, transactionItems[0].OrganisationId);
            Assert.AreEqual(transactionItem.Value, transactionItems[0].Value);
            Assert.AreEqual(2, transactionId);
        }
        #endregion Create transaction item tests
    }
}
