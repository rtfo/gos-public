﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgLedger.API.Data;
using DfT.GOS.GhgLedger.API.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace DfT.GOS.GhgLedger.API.UnitTest.Repositories
{
    /// <summary>
    /// Unit tests for the TransactionRepository
    /// </summary>
    [TestClass]
    public class TransactionRepositoryTest
    {
        #region Properties
        private ITransactionRepository TransactionRepository { get; set; }
        #endregion Properties


        #region Create transaction tests
        /// <summary>
        /// Tests that CreateTransaction writes a transaction to the database
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateTransaction_Writes_A_Transaction_To_The_Database_Context()
        {
            //arrange
            var obligationPeriodId = 15;
            var transactionTypeId = (int)Models.TransactionType.Redeem;
            var userId = "User1";
            var additionalReference = "addtional ref";
            
            var options = new DbContextOptionsBuilder<GosLedgerDataContext>()
                .UseInMemoryDatabase(databaseName: "Test_Create_Transaction")
                .Options;

            var context = new GosLedgerDataContext(options);
            this.TransactionRepository = new TransactionRepository(context);

            //act
            await this.TransactionRepository.CreateTransaction(obligationPeriodId, transactionTypeId, userId, additionalReference);
            var transactionId = await this.TransactionRepository.CreateTransaction(obligationPeriodId, transactionTypeId, userId, additionalReference);
            var transactions = await context.Transaction.ToListAsync();

            //assert
            Assert.AreEqual(userId, transactions[0].CreatedBy);
            Assert.AreEqual(additionalReference, transactions[0].AdditionalReference);
            Assert.AreEqual(obligationPeriodId, transactions[0].ObligationPeriodId);
            Assert.AreEqual(2, transactionId);
        }

        #endregion Create transaction tests


    }
}
