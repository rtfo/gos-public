﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.GhgFuels.Common.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace GOS.Organisations.API.Client.UnitTest
{
    [TestClass]
    public class GhgClientFuelsServiceTest
    {
        #region Private Members
        private const int CacheExpiryInSeconds = 1;
        private Mock<HttpMessageHandler> HttpHandler;
        private IGhgFuelsService ghgFuelsService;
        private IMemoryCache _memoryCache;
        #endregion

        #region Initialize
        [TestInitialize]
        public void Initialize()
        {
            HttpHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var httpClient = new HttpClient(HttpHandler.Object) { BaseAddress = new System.Uri("http://test.com/") };
            _memoryCache = _memoryCache ?? new MemoryCache(new MemoryCacheOptions());
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions();
            memoryCacheEntryOptions = memoryCacheEntryOptions.SetAbsoluteExpiration(TimeSpan.FromSeconds(CacheExpiryInSeconds));
            ghgFuelsService = new GhgFuelsService(httpClient, _memoryCache, memoryCacheEntryOptions);
        }
        #endregion

        #region tests
        /// <summary>
        /// Test the subsequest calls to service methods fetches from the cache instad of hitting http
        /// </summary>
        [TestMethod]
        public void GetFuelTypes_Stores_In_Cache()
        {
            //arrange
            AddToCache();

            //act
            var result = ghgFuelsService.GetFuelTypes();

            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(_memoryCache.TryGetValue("AllFuelTypes", out IList<FuelType> list));
            Assert.IsNotNull(list);
            Assert.AreEqual(6, list.Count);
        }
        /// <summary>
        /// Tests the data in cache gets deleted once cache expired
        /// </summary>
        [TestMethod]
        public void GetFuelTypes_Cache_Expires()
        {
            //arrange
            AddToCache();
            Initialize();
            //act
            Thread.Sleep(TimeSpan.FromSeconds(CacheExpiryInSeconds * 1.5)); // wait till the cache expires
            var result = ghgFuelsService.GetFuelTypes();

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsFalse(_memoryCache.TryGetValue("AllSuppliers", out IList<FuelType> list));
        }
        /// <summary>
        /// Tests the single element retreival from cache
        /// </summary>
        [TestMethod]
        public void GetFuelType_Retrieves_From_Cache()
        {
            //arrange
            AddToCache();
            Initialize();
            
            //act
            var result = ghgFuelsService.GetFuelType(3);

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/FuelTypes"
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Adds the organisation list to the cache
        /// </summary>
        private void AddToCache()
        {
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'fuelTypeId': 1,'fuelCategoryId': 1,'fuelCategory': 'Electricity','name': 'Electricity','carbonIntensity': 81.11,'energyDensity': null}," +
                        "{'fuelTypeId': 2,'fuelCategoryId': 2,'fuelCategory': 'Upstream Emission Reduction (UERs)','name': 'Upstream Emission Reductions (UERs)','carbonIntensity': null,'energyDensity': null}," +
                        "{'fuelTypeId': 3,'fuelCategoryId': 3,'fuelCategory': 'Fossil Gas','name': 'Liquid Petroleum Gas','carbonIntensity': 73.6,'energyDensity': 46}," +
                        "{'fuelTypeId': 4,'fuelCategoryId': 3,'fuelCategory': 'Fossil Gas','name': 'Compressed Natural Gas EU-Mix (in spark ignition engine)','carbonIntensity': 69.3,'energyDensity': 45.1}," +
                        "{'fuelTypeId': 5,'fuelCategoryId': 3,'fuelCategory': 'Fossil Gas','name': 'Liquefied Natural Gas EU-Mix (in spark ignition engine)','carbonIntensity': 74.5,'energyDensity': 45.1}," +
                        "{'fuelTypeId': 6,'fuelCategoryId': 3,'fuelCategory': 'Fossil Gas','name': 'Compressed Natural Gas Russia-Mix (in spark ignition engine)','carbonIntensity': 69.3,'energyDensity': 49.2}]"), //Json for PagedResult<Oranaisation>
                    }))
                   .Verifiable();

            var result = ghgFuelsService.GetFuelTypes();

            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/FuelTypes"
                            ),
                            ItExpr.IsAny<CancellationToken>());
        }
        #endregion
    }
}
