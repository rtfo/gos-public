﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace GOS.Organisations.API.Client.UnitTest
{
    [TestClass]
    public class ReportingPeriodsClientServiceTest
    {
        #region Private Members
        private const int CacheExpiryInSeconds = 1;
        private Mock<HttpMessageHandler> HttpHandler;
        private IReportingPeriodsService reportingPeriodsService;
        private IMemoryCache _memoryCache;
        #endregion

        #region Initialize
        [TestInitialize]
        public void Initialize()
        {
            HttpHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var httpClient = new HttpClient(HttpHandler.Object) { BaseAddress = new System.Uri("http://test.com/") };
            _memoryCache = _memoryCache ?? new MemoryCache(new MemoryCacheOptions());
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions();
            memoryCacheEntryOptions = memoryCacheEntryOptions.SetAbsoluteExpiration(TimeSpan.FromSeconds(CacheExpiryInSeconds));
            reportingPeriodsService = new ReportingPeriodsService(httpClient, _memoryCache, memoryCacheEntryOptions);
        }
        #endregion

        #region tests
        /// <summary>
        /// Test the subsequest calls to service methods fetches from the cache instad of hitting http
        /// </summary>
        [TestMethod]
        public void GetObligationPeriods_Stores_In_Cache()
        {
            //arrange
            AddToCache();

            //act
            var result = reportingPeriodsService.GetObligationPeriods();

            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(_memoryCache.TryGetValue("AllObligationPeriods", out IList<ObligationPeriod> list));
            Assert.IsNotNull(list);
            Assert.AreEqual(3, list.Count);
        }
        /// <summary>
        /// Tests the data in cache gets deleted once cache expired
        /// </summary>
        [TestMethod]
        public void GetFuelTypes_Cache_Expires()
        {
            //arrange
            AddToCache();
            Initialize();
            //act
            Thread.Sleep(TimeSpan.FromSeconds(CacheExpiryInSeconds * 1.5)); // wait till the cache expires
            var result = reportingPeriodsService.GetObligationPeriods();

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsFalse(_memoryCache.TryGetValue("AllObligationPeriods", out IList<ObligationPeriod> list));
        }
        /// <summary>
        /// Tests the single element retreival from cache
        /// </summary>
        [TestMethod]
        public void GetFuelType_Retrieves_From_Cache()
        {
            //arrange
            AddToCache();
            Initialize();

            //act
            var result = reportingPeriodsService.GetObligationPeriod(3);

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/obligationPeriods"
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Adds the Reporting period list to the cache
        /// </summary>
        private void AddToCache()
        {
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{  'id': 3,  'startDate': '2008-04-15T00:00:00',  'endDate': '2009-04-14T00:00:00',  " +
                        "'isActivated': true,  'latestDateForApplicationsFromSupplier': null,  'deMinimisLitres': 450000,  " +
                        "'noDeMinimisLitres': 10000000,  'latestDateForRedemption': '2009-10-05T00:00:00',  'latestDateForRevocation': null,  " +
                        "'latestDateToNotifyRevocation': '2009-07-31T00:00:00',  'ghgThresholdGrammesCO2PerMJ': null,  " +
                        "'ghgLowVolumeSupplierDeductionKGCO2': null,  'ghgBuyoutPencePerKGCO2': null}," +
                        "{  'id': 4,  'startDate': '2009-04-15T00:00:00',  'endDate': '2010-04-14T00:00:00',  'isActivated': true,  " +
                        "'latestDateForApplicationsFromSupplier': null,  'deMinimisLitres': 450000,  'noDeMinimisLitres': 10000000,  " +
                        "'latestDateForRedemption': '2010-10-06T00:00:00',  'latestDateForRevocation': null,  " +
                        "'latestDateToNotifyRevocation': '2010-07-31T00:00:00',  'ghgThresholdGrammesCO2PerMJ': null,  " +
                        "'ghgLowVolumeSupplierDeductionKGCO2': null,  'ghgBuyoutPencePerKGCO2': null}," +
                        "{  'id': 6,  'startDate': '2010-04-15T00:00:00',  'endDate': '2011-04-14T00:00:00',  " +
                        "'isActivated': true,  'latestDateForApplicationsFromSupplier': null,  'deMinimisLitres': 450000,  " +
                        "'noDeMinimisLitres': 10000000,  'latestDateForRedemption': '2011-10-05T00:00:00',  'latestDateForRevocation': null,  " +
                        "'latestDateToNotifyRevocation': '2011-08-01T00:00:00',  'ghgThresholdGrammesCO2PerMJ': null,  " +
                        "'ghgLowVolumeSupplierDeductionKGCO2': null,  'ghgBuyoutPencePerKGCO2': null}]"), //Json for IList<ObligationPeriod>
                    }))
                   .Verifiable();

            var result = reportingPeriodsService.GetObligationPeriods();

            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/obligationPeriods"
                            ),
                            ItExpr.IsAny<CancellationToken>());
        }
        #endregion
    }
}
