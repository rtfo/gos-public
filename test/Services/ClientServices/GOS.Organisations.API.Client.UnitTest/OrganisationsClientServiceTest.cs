// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using Moq;
using DfT.GOS.Organisations.API.Client.Services;
using Microsoft.Extensions.Caching.Memory;
using System;
using Moq.Protected;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using DfT.GOS.Organisations.API.Client.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GOS.Organisations.API.Client.UnitTest
{
    [TestClass]
    public class OrganisationsClientServiceTest
    {
        #region Private Members
        private const int CacheExpiryInSeconds = 1;
        private Mock<HttpMessageHandler> HttpHandler;
        private IOrganisationsService _organisationsService;
        private IMemoryCache _memoryCache;
        #endregion

        #region Initialize
        [TestInitialize]
        public void Initialize()
        {
            HttpHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var httpClient = new HttpClient(HttpHandler.Object) { BaseAddress = new System.Uri("http://test.com/") };
            _memoryCache = _memoryCache ?? new MemoryCache(new MemoryCacheOptions());
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions();
            memoryCacheEntryOptions = memoryCacheEntryOptions.SetAbsoluteExpiration(TimeSpan.FromSeconds(CacheExpiryInSeconds));
            _organisationsService = new OrganisationsService(httpClient, _memoryCache, memoryCacheEntryOptions);
        }
        #endregion

        #region Tests

        #region GetSuppliers Tests

        /// <summary>
        /// Tests the GetSuppliers service methods gets the data from api and adds to the cache 
        /// </summary>
        [TestMethod]
        public async Task GetSuppliers_Stores_In_Cache()
        {
            //arrange
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'id':101,'name':'Not Submitted Supplier','statusId':2," +
                        "'status':'Not Submitted','typeId':1,'type':'Supplier'}]"), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();

            //act
            var result = await _organisationsService.GetSuppliers("validToken");

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(1, result.Result.Count);
            Assert.IsTrue(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(1, pagedResult.Count);
        }

        /// <summary>
        /// Tests the data in cache gets deleted once cache expired
        /// </summary>
        [TestMethod]
        public async Task GetSuppliers_Cache_Expires()
        {
            //arrange
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'id':101,'name':'Not Submitted Supplier','statusId':2," +
                        "'status':'Not Submitted','typeId':1,'type':'Supplier'}]"), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();

            //act
            var result = await _organisationsService.GetSuppliers("validToken");
            Thread.Sleep(TimeSpan.FromSeconds(CacheExpiryInSeconds * 1.5)); // wait till the cache expires

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(1, result.Result.Count);
            Assert.IsFalse(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
        }

        /// <summary>
        /// Test the subsequest calls to service methods fetches from the cache instad of hitting http
        /// </summary>
        [TestMethod]
        public async Task GetSuppliers_Retrieves_From_Cache()
        {
            //arrange
            await AddToCache(); // populate the cache with 2 Suppliers

            //act
            var result = await _organisationsService.GetSuppliers("validtoken");

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we do not expect a request to the API service
                            ItExpr.IsNull<HttpRequestMessage>(),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(2, result.Result.Count);
            Assert.IsTrue(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(2, pagedResult.Count);
        }

        #endregion GetSuppliers Tests

        #region GetSuppliers (Paged) Tests

        /// <summary>
        /// Tests the GetSuppliers service methods gets the data from api and adds to the cache 
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersPaged_Stores_In_Cache()
        {
            //arrange
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'id':101,'name':'Not Submitted Supplier','statusId':2," +
                        "'status':'Not Submitted','typeId':1,'type':'Supplier'}]"), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();
            var pageNo = 1;
            var pageSize = 5;

            //act
            var result = await _organisationsService.GetSuppliers(pageNo, pageSize, "validToken", string.Empty);

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(1, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(1, result.Result.Result.Count);
            Assert.IsTrue(result.Result.Result.All(sup => sup.Id == 101));
            Assert.IsTrue(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(1, pagedResult.Count);
        }

        /// <summary>
        /// Tests the data in cache gets deleted once cache expired
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersPaged_Cache_Expires()
        {
            //arrange
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'id':101,'name':'Not Submitted Supplier','statusId':2," +
                        "'status':'Not Submitted','typeId':1,'type':'Supplier'}]"), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();
            var pageNo = 1;
            var pageSize = 5;

            //act
            var result = await _organisationsService.GetSuppliers(pageNo, pageSize, "validToken", string.Empty);
            Thread.Sleep(TimeSpan.FromSeconds(CacheExpiryInSeconds * 1.5)); // wait till the cache expires

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(1, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(1, result.Result.Result.Count);
            Assert.IsTrue(result.Result.Result.All(sup => sup.Id == 101));
            Assert.IsFalse(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
        }

        /// <summary>
        /// Test the subsequent calls to service methods fetches from the cache instad of hitting http
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersPaged_Retrieves_From_Cache()
        {
            //arrange
            await AddToCache(); // populate the cache with 2 Suppliers
            var pageNo = 1;
            var pageSize = 5;

            //act
            var result = await _organisationsService.GetSuppliers(pageNo, pageSize, "validToken", string.Empty);

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we do not expect a request to the API service
                            ItExpr.IsNull<HttpRequestMessage>(),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(2, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(2, result.Result.Result.Count);
            Assert.IsTrue(result.Result.Result.All(sup => sup.IsSupplier));
            Assert.IsTrue(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(2, pagedResult.Count);
        }

        /// <summary>
        /// Test that Suppliers are filtered by the given string
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersPaged_Filters_By_Given_String()
        {
            //arrange
            await AddToCache(); // populate the cache with 2 Suppliers
            var pageNo = 1;
            var pageSize = 5;

            //act
            var result = await _organisationsService.GetSuppliers(pageNo, pageSize, "validToken", "Retired");

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we do not expect a request to the API service
                            ItExpr.IsNull<HttpRequestMessage>(),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(1, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(1, result.Result.Result.Count);
            Assert.IsTrue(result.Result.Result.All(sup => sup.Id == 102));
            Assert.IsTrue(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(2, pagedResult.Count);
        }

        /// <summary>
        /// Test that Suppliers are paged by the given parameters
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersPaged_Pages_By_Given_Parameters()
        {
            //arrange
            await AddToCache(); // populate the cache with 2 Suppliers
            var pageNo = 1;
            var pageSize = 1;

            //act
            var result = await _organisationsService.GetSuppliers(pageNo, pageSize, "validToken", string.Empty);

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we do not expect a request to the API service
                            ItExpr.IsNull<HttpRequestMessage>(),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(2, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(1, result.Result.Result.Count);
            Assert.IsTrue(result.Result.Result.All(sup => sup.Id == 101));
            Assert.IsTrue(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(2, pagedResult.Count);
        }

        #endregion GetSuppliers (Paged) Tests

        #region GetOrganisation Tests

        /// <summary>
        /// Tests the GetSuppliers service methods gets the data from api and adds to the cache 
        /// </summary>
        [TestMethod]
        public async Task GetOrganisation_Stores_In_Cache()
        {
            //arrange
            var organisationId = 102;
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'id':102,'name':'Organisation 102','statusId':4," +
                        "'status':'Approved','typeId':2,'type':'Trader'}]"), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();

            //act
            var result = await _organisationsService.GetOrganisation(organisationId, "validToken");

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/organisations"
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(organisationId, result.Result.Id);
            Assert.IsTrue(_memoryCache.TryGetValue("AllOrganisations", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(1, pagedResult.Count);
            Assert.IsTrue(pagedResult.All(o => o.Id == organisationId));
        }

        /// <summary>
        /// Tests the data in cache gets deleted once cache expires after calling GetOrganisation
        /// </summary>
        [TestMethod]
        public async Task GetOrganisation_Cache_Expires()
        {
            //arrange
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'id':101,'name':'Not Submitted Supplier','statusId':2," +
                        "'status':'Not Submitted','typeId':1,'type':'Supplier'}]"), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();
            var organisationId = 101;

            //act
            var result = await _organisationsService.GetOrganisation(organisationId, "validToken");
            Thread.Sleep(TimeSpan.FromSeconds(CacheExpiryInSeconds * 1.5)); // wait till the cache expires

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(organisationId, result.Result.Id);
            Assert.IsFalse(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
        }

        /// <summary>
        /// Tests the single element retreival from cache
        /// </summary>
        [TestMethod]
        public async Task GetOrganisation_Retrieves_From_Cache()
        {
            //arrange
            await AddToCache();
            var organisationId = 103;

            //act
            var result = await _organisationsService.GetOrganisation(organisationId, "validtoken");

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we do not expect any HTTP requests to be made
                            ItExpr.IsNull<HttpRequestMessage>(),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(organisationId, result.Result.Id);
        }

        #endregion GetOrganisation Tests

        #region GetSuppliersAndTraders Tests

        /// <summary>
        /// Tests the GetSuppliersAndTraders service methods gets the data from api and adds to the cache 
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersAndTraders_Stores_In_Cache()
        {
            //arrange
            var orgJson = new StringBuilder("[{'id':1,'name':'fake supplier','statusId':1,'status':'fake status','typeId':1,'type':'supplier'},");
            orgJson.Append("{'id':2,'name':'fake trader','statusId':1,'status':'fake status','typeId':2,'type':'trader'},");
            orgJson.Append("{'id':3,'name':'fake verifier','statusId':1,'status':'fake status','typeId':3,'type':'verifier'}]");

            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent(orgJson.ToString()), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();

            //act
            var result = await _organisationsService.GetSuppliersAndTraders("validToken");

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/organisations"
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(2, result.Result.Count);
            Assert.IsTrue(result.Result.All(org => org.IsSupplier || org.IsTrader));
            Assert.IsTrue(_memoryCache.TryGetValue("AllOrganisations", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(3, pagedResult.Count);
        }

        /// <summary>
        /// Tests the data in cache gets deleted once cache expires after calling GetSuppliersAndTraders
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersAndTraders_Cache_Expires()
        {
            //arrange
            var orgJson = new StringBuilder("[{'id':1,'name':'fake supplier','statusId':1,'status':'fake status','typeId':1,'type':'supplier'},");
            orgJson.Append("{'id':2,'name':'fake trader','statusId':1,'status':'fake status','typeId':2,'type':'trader'},");
            orgJson.Append("{'id':3,'name':'fake verifier','statusId':1,'status':'fake status','typeId':3,'type':'verifier'}]");

            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent(orgJson.ToString()), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();

            //act
            var result = await _organisationsService.GetSuppliersAndTraders("validToken");
            Thread.Sleep(TimeSpan.FromSeconds(CacheExpiryInSeconds * 1.5)); // wait till the cache expires

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(2, result.Result.Count);
            Assert.IsTrue(result.Result.All(org => org.IsSupplier || org.IsTrader));
            Assert.IsFalse(_memoryCache.TryGetValue("AllSuppliers", out IList<Organisation> pagedResult));
        }

        /// <summary>
        /// Tests the single element retreival from cache
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersAndTraders_Retrieves_From_Cache()
        {
            //arrange
            await AddToCache();

            //act
            var result = await _organisationsService.GetSuppliersAndTraders("validtoken");

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we do not expect any HTTP requests to be made
                            ItExpr.IsNull<HttpRequestMessage>(),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(2, result.Result.Count);
        }

        /// <summary>
        /// Tests that the GetSuppliersAndTraders method gets traders and suppliers from the cache
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersAndTraders_Retrieves_Returns_Only_Suppliers_And_Traders()
        {
            //arrange
            var organisations = new List<Organisation>
            {
                new Organisation(1, "fake supplier", 1, "fake status", 1, "supplier"),
                new Organisation(2, "fake trader", 1, "fake status", 2, "trader"),
                new Organisation(3, "fake verifier", 1, "fake status", 3, "verifier")
            };
            _memoryCache.Set("AllOrganisations", organisations);

            //act
            var result = await _organisationsService.GetSuppliersAndTraders("validtoken");
            var tradersAndSuppliers = result.Result;

            //assert
            Assert.IsNotNull(tradersAndSuppliers);
            Assert.AreEqual(2, tradersAndSuppliers.Count);
            Assert.IsTrue(tradersAndSuppliers.All(ts => ts.IsSupplier || ts.IsTrader));
        }

        #endregion GetSuppliersAndTraders Tests

        #region GetSuppliersAndTraders (Paged) Tests

        /// <summary>
        /// Tests the GetSuppliersAndTraders service methods gets the data from api and adds to the cache 
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersAndTradersPaged_Stores_In_Cache()
        {
            //arrange
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'id':101,'name':'Not Submitted Supplier','statusId':2," +
                        "'status':'Not Submitted','typeId':1,'type':'Supplier'}]"), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();
            var pageNo = 1;
            var pageSize = 5;

            //act
            var result = await _organisationsService.GetSuppliersAndTraders(pageNo, pageSize, "validToken", false, string.Empty);

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(1, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(1, result.Result.Result.Count);
            Assert.IsTrue(result.Result.Result.All(sup => sup.Id == 101));
            Assert.IsTrue(_memoryCache.TryGetValue("AllOrganisations", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(1, pagedResult.Count);
        }

        /// <summary>
        /// Tests the data in cache gets deleted once cache expired
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersAndTradersPaged_Cache_Expires()
        {
            //arrange
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'id':101,'name':'Not Submitted Supplier','statusId':2," +
                        "'status':'Not Submitted','typeId':1,'type':'Supplier'}]"), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();
            var pageNo = 1;
            var pageSize = 5;

            //act
            var result = await _organisationsService.GetSuppliersAndTraders(pageNo, pageSize, "validToken", false, string.Empty);
            Thread.Sleep(TimeSpan.FromSeconds(CacheExpiryInSeconds * 1.5)); // wait till the cache expires

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(1, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(1, result.Result.Result.Count);
            Assert.IsTrue(result.Result.Result.All(sup => sup.Id == 101));
            Assert.IsFalse(_memoryCache.TryGetValue("AllOrganisations", out IList<Organisation> pagedResult));
        }

        /// <summary>
        /// Test the subsequent calls to service methods fetches from the cache instad of hitting http
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersAndTradersPaged_Retrieves_From_Cache()
        {
            //arrange
            await AddToCache(); // populate the cache with 2 Suppliers
            var pageNo = 1;
            var pageSize = 5;

            //act
            var result = await _organisationsService.GetSuppliersAndTraders(pageNo, pageSize, "validToken", false, string.Empty);

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we do not expect a request to the API service
                            ItExpr.IsNull<HttpRequestMessage>(),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(2, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(2, result.Result.Result.Count);
            Assert.IsTrue(result.Result.Result.All(sup => sup.IsSupplier || sup.IsTrader));
            Assert.IsTrue(_memoryCache.TryGetValue("AllOrganisations", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(3, pagedResult.Count);
        }

        /// <summary>
        /// Test that Suppliers and Traders are filtered by the given string
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersAndTradersPaged_Filters_By_Given_String()
        {
            //arrange
            await AddToCache(); // populate the cache with 2 Suppliers
            var pageNo = 1;
            var pageSize = 5;

            //act
            var result = await _organisationsService.GetSuppliersAndTraders(pageNo, pageSize, "validToken", false, "Retired");

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we do not expect a request to the API service
                            ItExpr.IsNull<HttpRequestMessage>(),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(0, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(0, result.Result.Result.Count);
            Assert.IsTrue(_memoryCache.TryGetValue("AllOrganisations", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(3, pagedResult.Count);
        }

        /// <summary>
        /// Test that Suppliers and Traders are paged by the given parameters
        /// </summary>
        [TestMethod]
        public async Task GetSuppliersAndTradersPaged_Pages_By_Given_Parameters()
        {
            //arrange
            await AddToCache(); // populate the cache with 2 Suppliers
            var pageNo = 1;
            var pageSize = 1;

            //act
            var result = await _organisationsService.GetSuppliers(pageNo, pageSize, "validToken", string.Empty);

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we do not expect a request to the API service
                            ItExpr.IsNull<HttpRequestMessage>(),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(pageNo, result.Result.PageNumber);
            Assert.AreEqual(pageSize, result.Result.PageSize);
            Assert.AreEqual(2, result.Result.TotalResults);
            Assert.IsNotNull(result.Result.Result);
            Assert.AreEqual(1, result.Result.Result.Count);
            Assert.IsTrue(result.Result.Result.All(sup => sup.Id == 101));
            Assert.IsTrue(_memoryCache.TryGetValue("AllOrganisations", out IList<Organisation> pagedResult));
            Assert.IsNotNull(pagedResult);
            Assert.AreEqual(3, pagedResult.Count);
        }

        #endregion GetSuppliersAndTraders (Paged) Tests

        #endregion Tests

        #region Private Methods
        /// <summary>
        /// Adds the organisation list to the cache
        /// </summary>
        private async Task AddToCache()
        {
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{'id':101,'name':'Not Submitted Supplier','statusId':2," +
                        "'status':'Not Submitted','typeId':1,'type':'Supplier'}," +
                        "{'id': 102,'name': 'Retired Supplier','statusId': 4,'status': 'Retired',"+
                        "'typeId': 1,'type': 'Supplier'}]"), //Json for PagedResult<Organisation>
                    }))
                   .Verifiable();

            await _organisationsService.GetSuppliers("validToken");

            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/suppliers"
                            ),
                            ItExpr.IsAny<CancellationToken>());

            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                .Returns(Task.FromResult(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content =
                    new StringContent("[{'id':103,'name':'Trader 102','statusId':3," +
                    "'status':'Approved','typeId':2,'type':'Trader'}," +
                    "{'id': 104,'name': 'Verifier 104','statusId': 3,'status': 'Approved'," +
                    "'typeId': 3,'type': 'Verifier'}," +
                    "{'id': 105,'name': 'Supplier 105','statusId': 3,'status': 'Approved'," +
                    "'typeId': 1,'type': 'Supplier'}]"), //Json for PagedResult<Organisation>
                }))
                .Verifiable();

            await _organisationsService.GetOrganisation(103, "validToken");

            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/organisations"
                            ),
                            ItExpr.IsAny<CancellationToken>());
        }
        #endregion
    }
}
