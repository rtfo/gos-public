﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.SystemParameters.API.Client.Models;

namespace GOS.Organisations.API.Client.UnitTest
{
    [TestClass]
    public class SystemParametersClientServiceTest
    {
        #region Private Members
        private const int CacheExpirationInSeconds= 1;
        private Mock<HttpMessageHandler> HttpHandler;
        private ISystemParametersService systemParametersService;
        private IMemoryCache _memoryCache;
        #endregion

        #region Initialize
        [TestInitialize]
        public void Initialize()
        {
            HttpHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var httpClient = new HttpClient(HttpHandler.Object) { BaseAddress = new System.Uri("http://test.com/") };
            _memoryCache = _memoryCache ?? new MemoryCache(new MemoryCacheOptions());
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions();
            memoryCacheEntryOptions = memoryCacheEntryOptions.SetAbsoluteExpiration(TimeSpan.FromSeconds(CacheExpirationInSeconds));
            systemParametersService = new SystemParametersService(httpClient, _memoryCache, memoryCacheEntryOptions);
        }
        #endregion

        #region tests
        /// <summary>
        /// Test the subsequest calls to service methods fetches from the cache instad of hitting http
        /// </summary>
        [TestMethod]
        public void GetSystemParameters_Stores_In_Cache()
        {
            //arrange
            AddToCacheSystemParams();

            //act
            var result = systemParametersService.GetSystemParameters();

            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(_memoryCache.TryGetValue("AllSystemParameters", out GosSystemParameters gosSystemParameters));
            Assert.IsNotNull(gosSystemParameters);
        }

        /// <summary>
        /// Tests the data in cache gets deleted once cache expired
        /// </summary>
        [TestMethod]
        public void GetSystemParameters_Cache_Expires()
        {
            //arrange
            AddToCacheSystemParams();
            Initialize();
            //act
            Thread.Sleep(TimeSpan.FromSeconds(CacheExpirationInSeconds * 1.5)); // wait till the cache expires
            var result = systemParametersService.GetSystemParameters();

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsFalse(_memoryCache.TryGetValue("AllSystemParameters", out GosSystemParameters gosSystemParameters));
        }

        /// <summary>
        /// Tests the single element retreival from cache
        /// </summary>
        [TestMethod]
        public void GetSystemParameters_Retreives_From_Cache()
        {
            //arrange
            AddToCacheSystemParams();
            Initialize();

            //act
            var result = systemParametersService.GetSystemParameters();

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/SystemParameters"
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test the subsequest calls to service methods fetches from the cache instad of hitting http
        /// </summary>
        [TestMethod]
        public void GetDocumentTypes_Stores_In_Cache()
        {
            //arrange
            AddToCacheDocTypes();

            //act
            var result = systemParametersService.GetDocumentTypes();

            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(_memoryCache.TryGetValue("AllDocumentTypes", out IList<DocumentType> docTypeList));
            Assert.IsNotNull(docTypeList);
        }

        /// <summary>
        /// Tests the data in cache gets deleted once cache expired
        /// </summary>
        [TestMethod]
        public void GetDocumentTypes_Cache_Expires()
        {
            //arrange
            AddToCacheDocTypes();
            Initialize();
            //act
            Thread.Sleep(TimeSpan.FromSeconds(CacheExpirationInSeconds * 1.5)); // wait till the cache expires
            var result = systemParametersService.GetSystemParameters();

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsFalse(_memoryCache.TryGetValue("AllDocumentTypes", out IList<DocumentType> docTypeList));
        }

        /// <summary>
        /// Tests the single element retreival from cache
        /// </summary>
        [TestMethod]
        public void GetDocumentTypes_Retrieves_From_Cache()
        {
            //arrange
            AddToCacheDocTypes();
            Initialize();

            //act
            var result = systemParametersService.GetDocumentTypes();

            //assert
            HttpHandler.Protected().Verify("SendAsync",
                            Times.Never(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/SystemParameters/documenttypes"
                            ),
                            ItExpr.IsAny<CancellationToken>());
            Assert.IsNotNull(result);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Adds the SystemParams list to the cache
        /// </summary>
        private void AddToCacheSystemParams()
        {
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{  'id': 3,  'parameterName': 'RTFO Support Telephone Number',  'value': '020 7944 8555'}," +
                        "{  'id': 4,  'parameterName': 'RTFO Support Email',  'value': 'rtfo-compliance@dft.gsi.gov.uk'}," +
                        "{  'id': 15,  'parameterName': 'RTFO Address Line 1',  'value': 'Low Carbon Fuels - Greener Transport & International Directorate - Department for Transport'}," +
                        "{  'id': 16,  'parameterName': 'RTFO Address Line 2',  'value': 'D/06 Ashdown House'}," +
                        "{  'id': 17,  'parameterName': 'RTFO Address Line 3',  'value': 'Sedlescombe Road North'}]"), //Json for GosSystemParameter
                    }))
                   .Verifiable();

            var result = systemParametersService.GetSystemParameters();

            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/SystemParameters"
                            ),
                            ItExpr.IsAny<CancellationToken>());
        }
        /// <summary>
        /// Add document types to cache
        /// </summary>
        private void AddToCacheDocTypes()
        {
            HttpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                        "SendAsync",
                        ItExpr.IsAny<HttpRequestMessage>(),
                        ItExpr.IsAny<CancellationToken>())// prepare the expected response of the mocked http call
                    .Returns(Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content =
                        new StringContent("[{  'id': 7,  'contentType': 'application/msword',  'fileExtension': 'doc',  'magicNumber': 'D0CF11E0A1B11AE1'}," +
                        "{  'id': 8,  'contentType': 'application/pdf',  'fileExtension': 'pdf',  'magicNumber': '25504446'}," +
                        "{  'id': 9,  'contentType': 'application/msword',  'fileExtension': 'docx',  'magicNumber': '504B030414'}," +
                        "{  'id': 10,  'contentType': 'Email',  'fileExtension': 'msg',  'magicNumber': 'NULL'}," +
                        "{  'id': 11,  'contentType': 'application/vnd.ms-powerpoint',  'fileExtension': 'pptx',  'magicNumber': '504B0304'}]"), //Json for IList<DocumentType>
                    }))
                   .Verifiable();

            var result = systemParametersService.GetDocumentTypes();

            HttpHandler.Protected().Verify("SendAsync",
                            Times.Once(), // we expected a single external request
                            ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get  // we expected a GET request
                            && req.RequestUri.AbsoluteUri == "http://test.com/api/v1/SystemParameters/documenttypes"
                            ),
                            ItExpr.IsAny<CancellationToken>());
        }
        #endregion

    }
}
