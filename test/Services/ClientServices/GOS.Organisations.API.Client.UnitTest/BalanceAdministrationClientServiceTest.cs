﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceOrchestrator.API.Client.Models;
using DfT.GOS.GhgBalanceOrchestrator.API.Client.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GOS.Organisations.API.Client.UnitTest
{
    /// <summary>
    /// Unit Tests for the Balance Administration API Client
    /// </summary>
    [TestClass]
    public class BalanceAdministrationClientServiceTest
    {
        #region Properties

        private IBalanceAdministrationService BalanceAdministrationService { get; set; }
        private Mock<HttpMessageHandler> HttpHandler;

        #endregion Properties

        #region Initialise

        /// <summary>
        /// Performs initialisation required for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            HttpHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var httpClient = new HttpClient(HttpHandler.Object) { BaseAddress = new System.Uri("http://test.com/") };
            this.BalanceAdministrationService = new BalanceAdministrationService(httpClient);
        }

        #endregion Initialise

        #region Tests

        #region UpdateCollections tests

        /// <summary>
        /// Tests that the Update Collections method handles an HTTP 200 (OK) response
        /// </summary>
        [TestMethod]
        public async Task BalanceAdministrationService_UpdateCollections_Returns_BulkUpdateUniqueId()
        {
            //arrange
            var jwtToken = string.Empty;
            var bulkUpdateUniqueId = "3f604f22-840b-4f76-9f54-dff7dbce7b74";
            this.HttpHandler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage()
                {
                    Content = new StringContent($"\"{bulkUpdateUniqueId}\""),
                    StatusCode = HttpStatusCode.OK
                });

            //act
            var result = await this.BalanceAdministrationService.UpdateCollections(jwtToken);

            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.AreEqual(bulkUpdateUniqueId, result.Result);
        }

        /// <summary>
        /// Tests that the Update Collections method handles an HTTP 401 (Unauthorized) response
        /// </summary>
        [TestMethod]
        public async Task BalanceAdministrationService_UpdateCollections_Handles_Error()
        {
            //arrange
            var jwtToken = string.Empty;
            this.HttpHandler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized
                });

            //act
            var result = await this.BalanceAdministrationService.UpdateCollections(jwtToken);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasResult);
            Assert.IsNull(result.Result);
        }

        #endregion UpdateCollections tests

        #region GetUpdateStatus tests

        /// <summary>
        /// Tests that the Get Update Status method handles an HTTP 200 (OK) response
        /// </summary>
        [TestMethod]
        public async Task BalanceAdministrationService_GetUpdateStatus_Returns_CollectionAdministration()
        {
            //arrange
            var bulkUpdateUniqueId = "3f604f22-840b-4f76-9f54-dff7dbce7b74";
            var jwtToken = string.Empty;
            this.HttpHandler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage()
                {
                    Content = new StringContent($"{{ \"id\": \"{bulkUpdateUniqueId}\", \"loadStarted\": \"2019-02-14T15:13:57.238Z\", \"loadSuccessfullyCompleted\": \"2019-02-14T15:14:19.435Z\", \"errors\": [], \"hasErrors\": false, \"hasTimedOut\": false, \"status\": 1 }}"),
                    StatusCode = HttpStatusCode.OK
                });

            //act
            var result = await this.BalanceAdministrationService.GetUpdateStatus(bulkUpdateUniqueId, jwtToken);

            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasResult);
            Assert.IsNotNull(result.Result);
            Assert.IsNotNull(result.Result.Errors);
            Assert.AreEqual(0, result.Result.Errors.Count);
            Assert.IsFalse(result.Result.HasErrors);
            Assert.IsFalse(result.Result.HasTimedOut);
            Assert.AreEqual(new Guid(bulkUpdateUniqueId), result.Result.Id);
            Assert.AreEqual(new DateTime(2019, 2, 14, 15, 13, 57, 238, DateTimeKind.Utc), result.Result.LoadStarted);
            Assert.AreEqual(new DateTime(2019, 2, 14, 15, 14, 19, 435, DateTimeKind.Utc), result.Result.LoadSuccessfullyCompleted);
            Assert.AreEqual(CollectionAdministrationStatus.Succeeded, result.Result.Status);

        }

        /// <summary>
        /// Tests that the Get Update Status method handles an HTTP 401 (Unauthorized) response
        /// </summary>
        [TestMethod]
        public async Task BalanceAdministrationService_GetUpdateStatus_Handles_Error()
        {
            //arrange
            var bulkUpdateUniqueId = string.Empty;
            var jwtToken = string.Empty;
            this.HttpHandler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized
                });

            //act
            var result = await this.BalanceAdministrationService.GetUpdateStatus(bulkUpdateUniqueId, jwtToken);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasResult);
            Assert.IsNull(result.Result);
        }

        #endregion GetUpdateStatus tests

        #endregion Tests
    }
}
