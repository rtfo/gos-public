﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgCalculations.Orchestration.UnitTest
{
    /// <summary>
    /// Tests the ApplicationsCalculationOrchestratorTest class
    /// </summary>
    [TestClass]
    public class ApplicationsCalculationOrchestratorTest
    {
        #region Constants

        private int FakeApplicationId = 1;
        private int FakeSupplierOrganisationId = 2;
        private int FakeObligationPeriodId = 3;
        private decimal FakeGhgThreshold = 90;
        private int FakeDeminimis = 5000;
        private int FakeNoDeminimis = 50000;
        private int FakeLowVolumeDeduction = 10000;

        #endregion Constants

        #region Properties

        private CalculationCommand LastCalculationCommand { get; set; }

        #endregion Properties

        #region Tests

        /// <summary>
        /// Tests that the GetCalculationSummaryForApplication issues the correct command to the calculation service
        /// </summary>
        [TestMethod]
        public async Task GetCalculationSummaryForApplication_IssuesCorrectCommand()
        {
            //arrange                    
            var orchestrator = this.GetOrchestratorInstance();

            //act
            var result = await orchestrator.GetCalculationSummaryForApplication(FakeApplicationId
                , FakeSupplierOrganisationId
                , FakeObligationPeriodId
                , FakeGhgThreshold
                , FakeDeminimis
                , FakeNoDeminimis
                , FakeLowVolumeDeduction);

            var command = this.LastCalculationCommand;

            //assert
            Assert.AreEqual(FakeApplicationId, result.ApplicationId);
            Assert.AreEqual(FakeObligationPeriodId, command.ObligationPeriodId);
            Assert.AreEqual(1, command.VolumeItems.Count);
            Assert.AreEqual(1, command.EnergyItems.Count);
            Assert.AreEqual(1, command.EmissionsReductionItems.Count);
        }

        /// <summary>
        /// Tests that the GetCalculationBreakdownForApplicationItems issues the correct command to the calculation service
        /// </summary>
        [TestMethod]
        public async Task GetCalculationBreakdownForApplicationItems_IssuesCorrectCommand()
        {
            //arrange                    
            var orchestrator = this.GetOrchestratorInstance();

            //act
            await orchestrator.GetCalculationBreakdownForApplicationItems(this.GetFakeApplicationItems()
                , FakeSupplierOrganisationId
                , FakeObligationPeriodId
                , FakeGhgThreshold
                , FakeDeminimis
                , FakeNoDeminimis
                , FakeLowVolumeDeduction);

            var command = this.LastCalculationCommand;

            //assert
            Assert.AreEqual(FakeSupplierOrganisationId, command.SupplierOrganisationId);
            Assert.AreEqual(FakeObligationPeriodId, command.ObligationPeriodId);
            Assert.AreEqual(1, command.VolumeItems.Count);
            Assert.AreEqual(1, command.EnergyItems.Count);
            Assert.AreEqual(1, command.EmissionsReductionItems.Count);
        }

        #endregion Tests

        #region Private Methods

        /// <summary>
        /// Gets a list of fake application items
        /// </summary>
        /// <returns>Returns a list of fake application items</returns>
        private IList<ApplicationItemSummary> GetFakeApplicationItems()
        {
            IList<ApplicationItemSummary> applicationItems = new List<ApplicationItemSummary>();

            applicationItems.Add(new ApplicationItemSummary(FakeApplicationId, 1, 1, null, null, null, null));
            applicationItems.Add(new ApplicationItemSummary(FakeApplicationId, 1, 2, null, null, null, null));
            applicationItems.Add(new ApplicationItemSummary(FakeApplicationId, 1, 3, null, null, null, null));

            return applicationItems;
        }

        /// <summary>
        /// Gets an instance of the orchestrator for testing
        /// </summary>
        /// <returns>Returns a setup instance of the orchestrator</returns>
        private ApplicationsCalculationOrchestrator GetOrchestratorInstance()
        {
            // - the applications service
            var mockApplicationsService = new Mock<IApplicationsService>();            

            mockApplicationsService.Setup(ap => ap.GetApplicationItems(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<IList<ApplicationItemSummary>>(this.GetFakeApplicationItems())));

            // - the fuels types
            IList<FuelType> fuelTypes = new List<FuelType>();
            fuelTypes.Add(new FuelType(1, (int)ApplicationsCalculationOrchestrator.FuelCategories.FossilGas, "Fossil Gas", "Fake Fossil Gas", null, null));
            fuelTypes.Add(new FuelType(2, (int)ApplicationsCalculationOrchestrator.FuelCategories.Electricity, "Electricity", "Fake Electricity", null, null));
            fuelTypes.Add(new FuelType(3, (int)ApplicationsCalculationOrchestrator.FuelCategories.UER, "UER", "Fake UERs", null, null));

            // - the calculation service
            var mockCalculationService = new Mock<ICalculationsService>();

            mockCalculationService.Setup(cs => cs.GhgCalculationSummary(It.IsAny<CalculationCommand>()))
                .Returns(Task.FromResult<CalculationSummaryResult>(null))
                .Callback<CalculationCommand>(c => this.LastCalculationCommand = c);

            mockCalculationService.Setup(cs => cs.GhgCalculationBreakdown(It.IsAny<CalculationCommand>()))
               .Returns(Task.FromResult<CalculationResult>(null))
               .Callback<CalculationCommand>(c => this.LastCalculationCommand = c);

            return new ApplicationsCalculationOrchestrator(mockApplicationsService.Object
                , mockCalculationService.Object
                , fuelTypes
                , It.IsAny<string>());
        }

        #endregion Private Methods
    }
}
