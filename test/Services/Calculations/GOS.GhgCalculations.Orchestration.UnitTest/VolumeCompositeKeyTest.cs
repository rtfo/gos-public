﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DfT.GOS.GhgCalculations.Orchestration.UnitTest
{
    /// <summary>
    /// Tests the Volume Composite Key Class
    /// </summary>
    [TestClass]
    public class VolumeCompositeKeyTest
    {
        #region Tests

        /// <summary>
        /// Tests the contructor taking a composite key returns the correct individual properties
        /// </summary>
        [TestMethod]
        public void VolumeCompositeKeyTest_CompositeKey_Construtor()
        {
            //Arrange
            var organisationId = 1;
            var obligationPeriodId = 2;
            var reportedMonthEndDate = DateTime.Now;
            var fuelTypeId = 3;

            var suppliedCompositeKey = string.Format("{0},{1},{2},{3}", organisationId
                , obligationPeriodId
                , reportedMonthEndDate
                , fuelTypeId);

            var expectedCompositeKey = string.Format("{0},{1},{2},{3}", organisationId
                , obligationPeriodId
                , reportedMonthEndDate.Date
                , fuelTypeId);

            //Act
            var key = new VolumeCompositeKey(suppliedCompositeKey);

            //Assert
            Assert.AreEqual(organisationId, key.OrganisationId);
            Assert.AreEqual(obligationPeriodId, key.ObligationPeriodId);
            Assert.AreEqual(reportedMonthEndDate.Date, key.ReportedMonthEndDate);           
            Assert.AreEqual(fuelTypeId, key.FuelTypeId);
            Assert.AreEqual(expectedCompositeKey, key.CompositeKey);
        }

        /// <summary>
        /// Tests the constructor taking components constructor returns the correct details
        /// </summary>
        [TestMethod]
        public void VolumeCompositeKeyTest_ComponentKey_Construtor()
        {
            //Arrange
            var organisationId = 1;
            var obligationPeriodId = 2;
            var reportedMonthEndDate = DateTime.Now;
            var fuelTypeId = 3;

            var expectedCompositeKey = string.Format("{0},{1},{2},{3}", organisationId
               , obligationPeriodId
               , reportedMonthEndDate.Date
               , fuelTypeId);

            //Act
            var key = new VolumeCompositeKey(organisationId, obligationPeriodId, reportedMonthEndDate, fuelTypeId);

            //Assert
            Assert.AreEqual(organisationId, key.OrganisationId);
            Assert.AreEqual(obligationPeriodId, key.ObligationPeriodId);
            Assert.AreEqual(reportedMonthEndDate.Date, key.ReportedMonthEndDate);
            Assert.AreEqual(fuelTypeId, key.FuelTypeId);
            Assert.AreEqual(expectedCompositeKey, key.CompositeKey);
        }

        #endregion Tests
    }
}
