﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgCalculations.API.Calculations;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dft.GOS.GhgCalculations.API.UnitTest.Calculations
{
    /// <summary>
    /// Unit tests for the calculation version 1
    /// </summary>
    [TestClass]
    public class Calculation_v1Test
    {
        #region General Tests

        /// <summary>
        /// Tests that the PerformCalculation method returns correct result when no items are supplied
        /// </summary>
        [TestMethod]
        public async Task Calculation_v1_PerformCalculation_NoItems()
        {
            //arrange
            var ghgTarget = (decimal)90.34;
            var deminimis = 450000;
            var noDeminimis = 10000000;
            var lowVolumeDeduction = 59352;
            var obligationPeriodId = 1;
            var organisationId = 30;           

            var calculation_v1 = new Calculation_v1();

            var volumeCalculationItems = new List<VolumeCalculationItem>();
            var energyCalculationItems = new List<EnergyCalculationItem>();
            var emissionsReductionsItem = new List<EmissionsReductionCalculationItem>();

            var command = new CalculationCommand(organisationId
                                                , obligationPeriodId
                                                , ghgTarget
                                                , deminimis
                                                , noDeminimis
                                                , lowVolumeDeduction
                                                , volumeCalculationItems
                                                , energyCalculationItems
                                                , emissionsReductionsItem);

            //act
            var result = await calculation_v1.PerformCalculation(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(CalculationResult));
            Assert.AreEqual(obligationPeriodId, result.ObligationPeriodId);
            Assert.AreEqual(organisationId, result.SupplierOrganisationId);

            Assert.AreEqual(0, result.VolumeCalculation.Items.Count);
            Assert.AreEqual(0, result.VolumeCalculation.NetTotalVolumeObligation);
            Assert.AreEqual(0, result.VolumeCalculation.TotalVolumeObligationWithObligationPeriodAdjustments);
            Assert.AreEqual(0, result.VolumeCalculation.TotalVolumeCredits);
            Assert.AreEqual(0, result.VolumeCalculation.TotalVolumeAboveGhgTarget);
            Assert.AreEqual(0, result.VolumeCalculation.TotalVolumeSupplied);
            Assert.AreEqual(ghgTarget, result.GhgTargetLevel);
        }

        /// <summary>
        /// Tests that the PerformCalculation method returns correct result when a mix of items supplied
        /// </summary>
        [TestMethod]
        public async Task Calculation_v1_PerformCalculation_MixedItems()
        {
            //arrange
            var ghgTarget = (decimal)90.34;
            var obligationPeriodId = 1;
            var deminimis = 450000;
            var noDeminimis = 10000000;
            var lowVolumeDeduction = 59352;
            var organisationId = 30;

            var calculation_v1 = new Calculation_v1();

            var fakeVolumeCalucationItems = this.GetFakeVolumeCalculationItems();
            var fakeEnergyCalucationItems = this.GetFakeEnergyCalculationItems();
            var fakeEmissionsReductionCalculationItems = this.GetFakeEmissionsReductionCalculationItems();

            var command = new CalculationCommand(organisationId
                                                , obligationPeriodId
                                                , ghgTarget
                                                , deminimis
                                                , noDeminimis
                                                , lowVolumeDeduction
                                                , fakeVolumeCalucationItems
                                                , fakeEnergyCalucationItems
                                                , fakeEmissionsReductionCalculationItems);

            //act
            var result = await calculation_v1.PerformCalculation(command);

            //assert 
            // - general checks
            Assert.IsInstanceOfType(result, typeof(CalculationResult));
            Assert.AreEqual(obligationPeriodId, result.ObligationPeriodId);
            Assert.AreEqual(organisationId, result.SupplierOrganisationId);
            Assert.AreEqual(ghgTarget, result.GhgTargetLevel);

            // - check item calculation counts
            Assert.AreEqual(fakeVolumeCalucationItems.Count, result.VolumeCalculation.Items.Count);
            Assert.AreEqual(fakeEnergyCalucationItems.Count, result.EnergyCalculation.Items.Count);
            Assert.AreEqual(fakeEmissionsReductionCalculationItems.Count, result.EmissionsReductionCalculation.Items.Count);

            // - check volume calculations
            Assert.AreEqual((decimal) 228626382 , result.VolumeCalculation.TotalVolumeSupplied);
            Assert.AreEqual((decimal) 34461447.09 , result.VolumeCalculation.NetTotalVolumeObligation);
            Assert.AreEqual((decimal) 0 , result.VolumeCalculation.ObligationPeriodLowVolumeSupplierDeduction);
            Assert.AreEqual((decimal) 4372469.47 , result.VolumeCalculation.TotalVolumeCredits);
            Assert.AreEqual((decimal) 226564134 , result.VolumeCalculation.TotalVolumeAboveGhgTarget);

            // - check energy calculations
            Assert.AreEqual((decimal) 25000, result.EnergyCalculation.TotalEnergySupplied);
            Assert.AreEqual((decimal) 301.70, result.EnergyCalculation.TotalEnergyCredits);
            Assert.AreEqual((decimal) 193.20, result.EnergyCalculation.TotalEnergyObligation);

            // - check emissions reduction calculations
            Assert.AreEqual((decimal) 1300, result.EmissionsReductionCalculation.TotalEmissionsReductionCredits);
            Assert.AreEqual((decimal) 0, result.EmissionsReductionCalculation.TotalEmissionsReductionObligation);

            // - check aggregated calculations.
            Assert.AreEqual((decimal) 4374071.17, result.NetTotalCredits);
            Assert.AreEqual((decimal) 34461640.29, result.NetTotalObligation);
            Assert.AreEqual((decimal) 4374071.17, result.TotalCreditsWithObligationPeriodAdjustments);
            Assert.AreEqual((decimal) 34461640.29, result.TotalObligationWithObligationPeriodAdjustments);
        }

        #endregion General Tests

        #region Low Volume Supplier Deduction Tests

        /// <summary>
        /// Tests that no volume obligation is generated when below the deminimis.
        /// (If you supply less than or equal to the DeMinimis Total fuel (e.g. 450,000 Units), there is no obligation)
        /// </summary>
        [TestMethod]
        public async Task Calculation_v1_PerformCalculation_LowVolumeSupplierDeduction_No_Obligation_Below_Deminimis()
        {
            //arrange
            var ghgTarget = (decimal)90.34;
            var deminimis = 450000;
            var noDeminimis = 10000000;
            var lowVolumeDeduction = 59352;
            var obligationPeriodId = 1;
            var organisationId = 30;

            // - volume to create an obligation below the deminimis
            var itemWithVolumeAboveNoDeminimis = new VolumeCalculationItem(It.IsAny<string>()
                , It.IsAny<string>()
                , It.IsAny<string>()
                , 10000001
                , (decimal)91.34
                , 25);

            var volumeCalculationItems = new List<VolumeCalculationItem>();
            volumeCalculationItems.Add(itemWithVolumeAboveNoDeminimis);
            var energyCalculationItems = new List<EnergyCalculationItem>();
            var emissionsReductionsItem = new List<EmissionsReductionCalculationItem>();

            var calculation_v1 = new Calculation_v1();

            var command = new CalculationCommand(organisationId
                                                , obligationPeriodId
                                                , ghgTarget
                                                , deminimis
                                                , noDeminimis
                                                , lowVolumeDeduction
                                                , volumeCalculationItems
                                                , energyCalculationItems
                                                , emissionsReductionsItem);

            //act
            var result = await calculation_v1.PerformCalculation(command);

            //assert
            // - A net obligation should have been produced
            Assert.IsTrue(result.VolumeCalculation.NetTotalVolumeObligation > 0);
            // - There should be no volume deduction
            Assert.AreEqual(0, result.VolumeCalculation.ObligationPeriodLowVolumeSupplierDeduction);
            // - The total net obligation and adjusted obligation should be the same
            Assert.AreEqual(result.NetTotalObligation, result.TotalObligationWithObligationPeriodAdjustments);
            // - The volume net obligation and volume net obligation should be the same
            Assert.AreEqual(result.VolumeCalculation.NetTotalVolumeObligation, result.VolumeCalculation.TotalVolumeObligationWithObligationPeriodAdjustments);
        }

        /// <summary>
        /// Tests that no deductions are made to the obligation if we're over the no-deminimis
        /// (if you supply more than the NoDeMinimis (e.g. 10,000,000) units of 'fuel above the GHG Target' (regardless of the 
        ///   total fuel supplied), there is no low volume supplier reduction)
        /// </summary>
        [TestMethod]
        public async Task Calculation_v1_PerformCalculation_LowVolumeSupplierDeduction_No_Deduction_Above_NoDeminimis()
        {
            //arrange
            var ghgTarget = (decimal)90.34;
            var deminimis = 450000;
            var noDeminimis = 10000000;
            var lowVolumeDeduction = 59352;
            var obligationPeriodId = 1;
            var organisationId = 30;

            // - volume to create an obligation below the deminimis
            var itemWithVolumeLessThanDeminimis = new VolumeCalculationItem(It.IsAny<string>()
                , It.IsAny<string>()
                , It.IsAny<string>()
                , 1000
                , (decimal)91.34
                , 25);

            var volumeCalculationItems = new List<VolumeCalculationItem>();
            volumeCalculationItems.Add(itemWithVolumeLessThanDeminimis);
            var energyCalculationItems = new List<EnergyCalculationItem>();
            var emissionsReductionsItem = new List<EmissionsReductionCalculationItem>();

            var calculation_v1 = new Calculation_v1();

            var command = new CalculationCommand(organisationId
                                                , obligationPeriodId
                                                , ghgTarget
                                                , deminimis
                                                , noDeminimis
                                                , lowVolumeDeduction
                                                , volumeCalculationItems
                                                , energyCalculationItems
                                                , emissionsReductionsItem);

            //act
            var result = await calculation_v1.PerformCalculation(command);

            //assert
            // - A net obligation should have been produced
            Assert.IsTrue(result.VolumeCalculation.NetTotalVolumeObligation > 0);
            // - There shouldn't be any volume obligation - so the deduction should be deduced by the Net Total Volume Obligation
            Assert.AreEqual(result.VolumeCalculation.NetTotalVolumeObligation, result.VolumeCalculation.ObligationPeriodLowVolumeSupplierDeduction);
            // - There shouldn't be any volume obligation
            Assert.AreEqual(0, result.VolumeCalculation.TotalVolumeObligationWithObligationPeriodAdjustments);
            // - There shouldn't be any total obligation
            Assert.AreEqual(0, result.TotalObligationWithObligationPeriodAdjustments);
        }

        /// <summary>
        /// Tests that a deduction of amount ghgLowVolumeSupplierDeductionKGCO2 is made if the net obligation is over the  ghgLowVolumeSupplierDeductionKGCO2
        /// when we are between the deminimis and the no-deminimis
        /// 
        /// (If you supply > DeMinimis (e.g. 450,000) units (total fuel) but the amount of 'fuel above the GHG target' 
        /// is smaller than NoDeMinimis (e.g. 10,000,000) there is a low volume supplier reduction applied to the obligation)
        /// </summary>
        [TestMethod]
        public async Task Calculation_v1_PerformCalculation_HighVolumeSupplierDeduction_equals_ghgLowVolumeSupplierDeductionKGCO2()
        {
            //arrange
            var ghgTarget = (decimal)90.34;
            var deminimis = 450000;
            var noDeminimis = 10000000;
            var lowVolumeDeduction = 59352;
            var obligationPeriodId = 1;
            var organisationId = 30;

            // - volume to create an obligation below the deminimis
            var itemWithHiVolumeBetweenDeminimisAndNoDeminimis = new VolumeCalculationItem(It.IsAny<string>()
                , It.IsAny<string>()
                , It.IsAny<string>()
                , 9999999
                , (decimal)91.34
                , 25);

            var volumeCalculationItems = new List<VolumeCalculationItem>();
            volumeCalculationItems.Add(itemWithHiVolumeBetweenDeminimisAndNoDeminimis);
            var energyCalculationItems = new List<EnergyCalculationItem>();
            var emissionsReductionsItem = new List<EmissionsReductionCalculationItem>();

            var calculation_v1 = new Calculation_v1();

            var command = new CalculationCommand(organisationId
                                                , obligationPeriodId
                                                , ghgTarget
                                                , deminimis
                                                , noDeminimis
                                                , lowVolumeDeduction
                                                , volumeCalculationItems
                                                , energyCalculationItems
                                                , emissionsReductionsItem);

            //act
            var result = await calculation_v1.PerformCalculation(command);

            //assert
            // - A net obligation should have been produced
            Assert.IsTrue(result.VolumeCalculation.NetTotalVolumeObligation > 0);
            // - The net obligation was larger than the ghgLowVolumeSupplierDeductionKGCO2
            Assert.IsTrue(result.VolumeCalculation.NetTotalVolumeObligation > result.VolumeCalculation.GhgLowVolumeSupplierDeductionKGCO2);
            // - The deduction made was the same as the ghgLowVolumeSupplierDeductionKGCO2
            Assert.AreEqual(result.VolumeCalculation.GhgLowVolumeSupplierDeductionKGCO2, result.VolumeCalculation.ObligationPeriodLowVolumeSupplierDeduction);
        }

        /// <summary>
        /// Tests that a deduction of amount the deduction made is never below the Net Total Obligation
        /// when we are between the deminimis and the no-deminimis
        /// 
        /// (If the obligation would become negative due to the low volume supplier deduction 
        /// being applied, it should be 0.)
        /// </summary>
        [TestMethod]
        public async Task Calculation_v1_PerformCalculation_LowVolumeSupplierDeduction_less_than_Net_Obligation()
        {
            //arrange
            var ghgTarget = (decimal)90.34;
            var deminimis = 450000;
            var noDeminimis = 10000000;
            var lowVolumeDeduction = 59352;
            var obligationPeriodId = 1;
            var organisationId = 30;

            // - volume to create an obligation below the deminimis
            var itemWithHiVolumeBetweenDeminimisAndNoDeminimis = new VolumeCalculationItem(It.IsAny<string>()
                , It.IsAny<string>()
                , It.IsAny<string>()
                , 450001
                , (decimal)91.34
                , 25);

            var volumeCalculationItems = new List<VolumeCalculationItem>();
            volumeCalculationItems.Add(itemWithHiVolumeBetweenDeminimisAndNoDeminimis);
            var energyCalculationItems = new List<EnergyCalculationItem>();
            var emissionsReductionsItem = new List<EmissionsReductionCalculationItem>();

            var calculation_v1 = new Calculation_v1();

            var command = new CalculationCommand(organisationId
                                                , obligationPeriodId
                                                , ghgTarget
                                                , deminimis
                                                , noDeminimis
                                                , lowVolumeDeduction
                                                , volumeCalculationItems
                                                , energyCalculationItems
                                                , emissionsReductionsItem);

            //act
            var result = await calculation_v1.PerformCalculation(command);

            //assert
            // - A net obligation should have been produced
            Assert.IsTrue(result.VolumeCalculation.NetTotalVolumeObligation > 0);
            // - The total obligation after deductions is zero
            Assert.AreEqual(0, result.TotalObligationWithObligationPeriodAdjustments);
            Assert.AreEqual(0, result.VolumeCalculation.TotalVolumeObligationWithObligationPeriodAdjustments);
            // - The deduction made equals the Net Obligation
            Assert.AreEqual(result.VolumeCalculation.NetTotalVolumeObligation, result.VolumeCalculation.ObligationPeriodLowVolumeSupplierDeduction);
        }

        /// <summary>
        /// Tests that a deduction of amount the deduction made is never below the Net Total Obligation
        /// when we are between the deminimis and the no-deminimis
        /// 
        /// (If the obligation would become negative due to the low volume supplier deduction 
        /// being applied, it should be 0.)
        /// </summary>
        [TestMethod]
        public async Task Calculation_v1_PerformCalculation_LowVolumeSupplierDeduction_less_than_Net_Obligation_When_Volume_Negative_Value()
        {
            //arrange
            var ghgTarget = (decimal)90.34;
            var deminimis = 450000;
            var noDeminimis = 10000000;
            var lowVolumeDeduction = 59352;
            var obligationPeriodId = 1;
            var organisationId = 30;

            // - volume to create an obligation below the deminimis
            var itemWithHiVolumeBetweenDeminimisAndNoDeminimis = new VolumeCalculationItem(It.IsAny<string>()
                , It.IsAny<string>()
                , It.IsAny<string>()
                , -450001
                , (decimal)91.34
                , 25);

            var volumeCalculationItems = new List<VolumeCalculationItem>();
            volumeCalculationItems.Add(itemWithHiVolumeBetweenDeminimisAndNoDeminimis);
            var energyCalculationItems = new List<EnergyCalculationItem>();
            var emissionsReductionsItem = new List<EmissionsReductionCalculationItem>();

            var calculation_v1 = new Calculation_v1();

            var command = new CalculationCommand(organisationId
                                                , obligationPeriodId
                                                , ghgTarget
                                                , deminimis
                                                , noDeminimis
                                                , lowVolumeDeduction
                                                , volumeCalculationItems
                                                , energyCalculationItems
                                                , emissionsReductionsItem);

            //act
            var result = await calculation_v1.PerformCalculation(command);

            //assert
            // - A net obligation should have been produced
            Assert.IsTrue(result.VolumeCalculation.NetTotalVolumeObligation < 0);
            // - The total obligation after deductions is zero
            Assert.AreEqual(0, result.TotalObligationWithObligationPeriodAdjustments);
            Assert.AreEqual(0, result.VolumeCalculation.TotalVolumeObligationWithObligationPeriodAdjustments);
            // - The deduction made equals the Net Obligation
            Assert.AreEqual(0, result.VolumeCalculation.ObligationPeriodLowVolumeSupplierDeduction);
        }

        /// <summary>
        /// Tests that aggregated volumes where one volume is negative produces no obligation
        /// 
        /// </summary>
        [TestMethod]
        public async Task Calculation_v1_PerformCalculation_Aggregated_Volumes_Net_Zero_Produce_No_Obligation_When_One_Volume_Is_Negative()
        {
            //arrange
            var ghgTarget = (decimal)90.34;
            var deminimis = 450000;
            var noDeminimis = 10000000;
            var lowVolumeDeduction = 59352;
            var obligationPeriodId = 1;
            var organisationId = 30;

            // - volume to create an obligation below the deminimis
            var negativeValueFuelCategory1 = new VolumeCalculationItem(It.IsAny<string>()
                , It.IsAny<string>()
                , It.IsAny<string>()
                , -450001
                , (decimal)91.34
                , 25);

            var positiveValueFuelCategory2 = new VolumeCalculationItem(It.IsAny<string>()
                , It.IsAny<string>()
                , It.IsAny<string>()
                , 450001
                , (decimal)91.34
                , 25);

            var volumeCalculationItems = new List<VolumeCalculationItem>();
            volumeCalculationItems.Add(negativeValueFuelCategory1);
            volumeCalculationItems.Add(positiveValueFuelCategory2);
            var energyCalculationItems = new List<EnergyCalculationItem>();
            var emissionsReductionsItem = new List<EmissionsReductionCalculationItem>();

            var calculation_v1 = new Calculation_v1();

            var command = new CalculationCommand(organisationId
                                                , obligationPeriodId
                                                , ghgTarget
                                                , deminimis
                                                , noDeminimis
                                                , lowVolumeDeduction
                                                , volumeCalculationItems
                                                , energyCalculationItems
                                                , emissionsReductionsItem);

            //act
            var result = await calculation_v1.PerformCalculation(command);

            //assert
            // - A net obligation should have been produced
            Assert.IsTrue(result.VolumeCalculation.NetTotalVolumeObligation == 0);
            // - The total obligation after deductions is zero
            Assert.AreEqual(0, result.TotalObligationWithObligationPeriodAdjustments);
            Assert.AreEqual(0, result.VolumeCalculation.TotalVolumeObligationWithObligationPeriodAdjustments);
            // - The deduction made equals the Net Obligation
            Assert.AreEqual(result.VolumeCalculation.NetTotalVolumeObligation, result.VolumeCalculation.ObligationPeriodLowVolumeSupplierDeduction);
        }

        #endregion Low Volume Supplier Deduction Tests   

        #region Private Methods

        /// <summary>
        /// Gets a mixed list of fake energy calculation items for testing with
        /// </summary>
        /// <returns>Returns a list of fake energy calculation items</returns>
        private List<EnergyCalculationItem> GetFakeEnergyCalculationItems()
        {
            var calculationItems = new List<EnergyCalculationItem>();

            // - example 1 
            var energy1 = "Energy 1";
            var energy1_description = "Low GHG Electricity";
            var energy1_energySupplied = 5000;
            var energy1_ghgIntensity = 30; //GHG below threshold
            calculationItems.Add(new EnergyCalculationItem(energy1, energy1_description, energy1_energySupplied, energy1_ghgIntensity));

            // - example 2 
            var energy2 = "Energy 2";
            var energy2_description = "Low GHG Electricity";
            var energy2_energySupplied = 20000;
            var energy2_ghgIntensity = 100; //GHG above threshold
            calculationItems.Add(new EnergyCalculationItem(energy2, energy2_description, energy2_energySupplied, energy2_ghgIntensity));

            return calculationItems;
        }

        /// <summary>
        /// Gets a mixed list of fake emissions reduction calculation items for testing with
        /// </summary>
        /// <returns>Returns a list of fake emissions reduction calculation items</returns>
        private List<EmissionsReductionCalculationItem> GetFakeEmissionsReductionCalculationItems()
        {
            var calculationItems = new List<EmissionsReductionCalculationItem>();

            // - example 1
            calculationItems.Add(new EmissionsReductionCalculationItem("UER1", "UER 1", 1300, "UER"));

            return calculationItems;
        }

        /// <summary>
        /// Gets a mixed list of fake volume calculation items for testing with
        /// </summary>
        /// <returns>Returns a list fake volume calculation items</returns>
        private List<VolumeCalculationItem> GetFakeVolumeCalculationItems()
        {
            var calculationItems = new List<VolumeCalculationItem>();

            // - example 1 - Accounting unit test data (below target)
            var ac52667 = "AC 52667";
            var ac52667_description = "Bioethanol - Sugar beet";
            var ac52667_volume = 47576;
            var ac52667_ci = 27;
            var ac52667_energyDensity = 21;
            calculationItems.Add(new VolumeCalculationItem(ac52667, It.IsAny<string>(), ac52667_description, ac52667_volume, ac52667_ci, ac52667_energyDensity));

            // - example 2 - Accounting unit test data (below target)
            var ac52668 = "AC 52668";
            var ac52668_description = "Bioethanol - Sugar beet";
            var ac52668_volume = 893336;
            var ac52668_ci = 27;
            var ac52668_energyDensity = 21;
            calculationItems.Add(new VolumeCalculationItem(ac52668, It.IsAny<string>(), ac52668_description, ac52668_volume, ac52668_ci, ac52668_energyDensity));

            // - example 3 - Accounting unit test data (below target)
            var ac64412 = "AC 64412";
            var ac64412_description = "Biodiesel ME - Used cooking oil";
            var ac64412_volume = 1121336;
            var ac64412_ci = 6;
            var ac64412_energyDensity = 33;
            calculationItems.Add(new VolumeCalculationItem(ac64412, It.IsAny<string>(), ac64412_description, ac64412_volume, ac64412_ci, ac64412_energyDensity));

            // - example 4 - Volume unit test data (above target)
            var vol1 = "Vol 1";
            var vol1_description = "Petrol(fossil)";
            var vol1_volume = 55826563;
            var vol1_ci = (decimal)93.3;
            var vol1_energyDensity = (decimal)32.18;
            calculationItems.Add(new VolumeCalculationItem(vol1, It.IsAny<string>(), vol1_description, vol1_volume, vol1_ci, vol1_energyDensity));

            // - example 5 - Accounting unit test data (above target)
            var vol2 = "Vol 2";
            var vol2_description = "Diesel (fossil)";
            var vol2_volume = 170737571;
            var vol2_ci = (decimal)95.1;
            var vol2_energyDensity = (decimal)35.86;
            calculationItems.Add(new VolumeCalculationItem(vol2, It.IsAny<string>(), vol2_description, vol2_volume, vol2_ci, vol2_energyDensity));

            return calculationItems;
        }

        #endregion Private Methods
    }
}
