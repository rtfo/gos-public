// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Dft.GOS.GhgCalculations.API.Controllers;
using DfT.GOS.GhgCalculations.API.Services;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dft.GOS.GhgCalculations.API.UnitTest
{
    /// <summary>
    /// Unit tests for the calculations controller
    /// </summary>
    [TestClass]
    public class CalculationsControllerTest
    {
        #region Constants

        private const int SupplierOrganisationId = 15;
        private const int NonGhgObligationPeriodId = 1;
        private const int ObligationPeriodId = 13;
        private const int NotFoundObligationPeriodId = 1000;

        private decimal FakeGhgThreshold = 90;
        private int FakeDeminimis = 5000;
        private int FakeNoDeminimis = 50000;
        private int FakeLowVolumeDeduction = 10000;

        #endregion Constants

        #region Properties

        private CalculationResult FakeCalculationResult { get; set; }

        #endregion Properties

        #region GhgCalculationBreakdown Tests

        /// <summary>
        /// Tests that the GhgCalculationBreakdown method returns OK result on success
        /// </summary>
        [TestMethod]
        public async Task GhgCalculationBreakdown_Returns_OK_Result()
        {
            //arrange
            var controller = this.GetControllerInstance();
            var command = new CalculationCommand(SupplierOrganisationId
                , ObligationPeriodId
                , FakeGhgThreshold
                , FakeDeminimis
                , FakeNoDeminimis
                , FakeLowVolumeDeduction
                , new List<VolumeCalculationItem>()
                , new List<EnergyCalculationItem>()
                , new List<EmissionsReductionCalculationItem>());
            
            //act
            var result = await controller.GhgCalculationBreakdown(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(CalculationResult));
        }        

        /// <summary>
        /// Tests that the GhgCalculationBreakdown returns a bad request if the Model State is invalid (this can happen if a malformed request is sent)
        /// </summary>
        [TestMethod]
        public async Task GhgCalculationBreakdown_Returns_BadRequest_ModelState_Invalid()
        {
            //arrange
            var controller = this.GetControllerInstance();
            controller.ModelState.AddModelError("command", "test error");
            var command = (CalculationCommand)null;

            //act
            var result = await controller.GhgCalculationBreakdown(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the GhgCalculationBreakdown returns a bad request if the calculation results in an overflow
        /// </summary>
        [TestMethod]
        public async Task GhgCalculationBreakdown_Returns_BadRequest_On_Overflow()
        {
            //arrange
            var calculationsService = new Mock<ICalculationsService>();
            calculationsService.Setup(mock => mock.Calculate(It.IsAny<CalculationCommand>()))
                .ThrowsAsync(new OverflowException());

            var controller = new CalculationsController(calculationsService.Object);
            var command = new CalculationCommand(SupplierOrganisationId
                , NonGhgObligationPeriodId
                , FakeGhgThreshold
                , FakeDeminimis
                , FakeNoDeminimis
                , FakeLowVolumeDeduction
                , new List<VolumeCalculationItem>()
                , new List<EnergyCalculationItem>()
                , new List<EmissionsReductionCalculationItem>());

            //act
            var result = await controller.GhgCalculationBreakdown(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        #endregion GhgCalculationBreakdown Tests

        #region GhgCalculationSummary Tests

        /// <summary>
        /// Tests that the GhgCalculationSummary method returns OK result on success
        /// </summary>
        [TestMethod]
        public async Task GhgCalculationSummary_Returns_OK_Result()
        {
            //arrange
            var controller = this.GetControllerInstance();
            var command = new CalculationCommand(SupplierOrganisationId
                , ObligationPeriodId
                , FakeGhgThreshold
                , FakeDeminimis
                , FakeNoDeminimis
                , FakeLowVolumeDeduction
                , new List<VolumeCalculationItem>()
                , new List<EnergyCalculationItem>()
                , new List<EmissionsReductionCalculationItem>());

            //act
            var result = await controller.GhgCalculationSummary(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(CalculationSummaryResult));

            var summaryResult = (CalculationSummaryResult)((OkObjectResult)result).Value;
            Assert.AreEqual(this.FakeCalculationResult.SupplierOrganisationId, summaryResult.SupplierOrganisationId);
            Assert.AreEqual(this.FakeCalculationResult.ObligationPeriodId, summaryResult.ObligationPeriodId);
            Assert.AreEqual(this.FakeCalculationResult.GhgTargetLevel, summaryResult.GhgTargetLevel);
            Assert.AreEqual(this.FakeCalculationResult.NetTotalCredits, summaryResult.NetTotalCredits);
            Assert.AreEqual(this.FakeCalculationResult.NetTotalObligation, summaryResult.NetTotalObligation);
            Assert.AreEqual(this.FakeCalculationResult.TotalCreditsWithObligationPeriodAdjustments, summaryResult.TotalCreditsWithObligationPeriodAdjustments);
            Assert.AreEqual(this.FakeCalculationResult.TotalObligationWithObligationPeriodAdjustments, summaryResult.TotalObligationWithObligationPeriodAdjustments);
        }       

        /// <summary>
        /// Tests that the GhgCalculationSummary returns a bad request if the Model State is invalid (this can happen if a malformed request is sent)
        /// </summary>
        [TestMethod]
        public async Task GhgCalculationSummary_Returns_BadRequest_ModelState_Invalid()
        {
            //arrange
            var controller = this.GetControllerInstance();
            controller.ModelState.AddModelError("command", "test error");
            var command = (CalculationCommand)null;

            //act
            var result = await controller.GhgCalculationSummary(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the GhgCalculationSummary returns a bad request if the calculation results in an overflow
        /// </summary>
        [TestMethod]
        public async Task GhgCalculationSummary_Returns_BadRequest_On_Overflow()
        {
            //arrange
            var calculationsService = new Mock<ICalculationsService>();
            calculationsService.Setup(mock => mock.Calculate(It.IsAny<CalculationCommand>()))
                .ThrowsAsync(new OverflowException());

            var controller = new CalculationsController(calculationsService.Object);
            var command = new CalculationCommand(SupplierOrganisationId
                , NonGhgObligationPeriodId
                , FakeGhgThreshold
                , FakeDeminimis
                , FakeNoDeminimis
                , FakeLowVolumeDeduction
                , new List<VolumeCalculationItem>()
                , new List<EnergyCalculationItem>()
                , new List<EmissionsReductionCalculationItem>());

            //act
            var result = await controller.GhgCalculationSummary(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        #endregion GhgCalculationSummary Tests

        #region Private Methods

        /// <summary>
        /// Gets an instance of the calculations controller for testing
        /// </summary>
        /// <returns>Returns a configured calculations controller</returns>
        private CalculationsController GetControllerInstance()
        {
            //Calculations Service
            var volumeCalculationResult = new VolumeCalculationResult(new List<VolumeCalculationItemResult>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>());

            var energyCalculationResult = new EnergyCalculationResult(new List<EnergyCalculationItemResult>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>());

            var emissionsReductionCalculationResult = new EmissionsReductionCalculationResult(new List<EmissionsReductionCalculationItemResult>()
                , It.IsAny<decimal>()
                , It.IsAny<decimal>());

            this.FakeCalculationResult = new CalculationResult(SupplierOrganisationId
                                                                , 1
                                                                , 90
                                                                , volumeCalculationResult
                                                                , energyCalculationResult
                                                                , emissionsReductionCalculationResult
                                                                , It.IsAny<decimal>()
                                                                , It.IsAny<decimal>()
                                                                , It.IsAny<decimal>()
                                                                , It.IsAny<decimal>());

            var calculationsService = new Mock<ICalculationsService>();
            calculationsService.Setup(s => s.Calculate(It.IsAny<CalculationCommand>()))
                                .Returns(Task.FromResult(this.FakeCalculationResult));           

            var controller = new CalculationsController(calculationsService.Object);
            return controller;
        }

        #endregion Private Methods
    }
}
