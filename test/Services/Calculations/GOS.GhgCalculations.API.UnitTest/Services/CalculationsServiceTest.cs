﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgCalculations.API.Services;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dft.GOS.GhgCalculations.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests for the calculations service
    /// </summary>
    [TestClass]
    public class CalculationsServiceTest
    {
        /// <summary>
        /// Tests that the calculateNetBreakdown method returns result on success
        /// </summary>
        [TestMethod]
        public async Task CalculateNetBreakdown_Returns_Result()
        {
            //arrange
            ICalculationsService service = new CalculationsService();
            var command = new CalculationCommand(15
                , 1
                , 90
                , 5000
                , 10000
                , 100000
                , new List<VolumeCalculationItem>()
                , new List<EnergyCalculationItem>()
                , new List<EmissionsReductionCalculationItem>());           

            //act
            var result = await service.Calculate(command);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(CalculationResult));
        }
    }
}
