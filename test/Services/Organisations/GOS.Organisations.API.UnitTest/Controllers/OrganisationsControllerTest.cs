﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Controllers;
using DfT.GOS.Organisations.API.Models;
using DfT.GOS.Organisations.API.Services;
using DfT.GOS.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Organisations.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests the Organisations controller
    /// </summary>
    [TestClass]
    public class OrganisationsControllerTest
    {
        #region Properties

        private OrganisationsController ControllerUnderTest { get; set; }
        private Mock<IOrganisationsService> MockOrganisationService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public OrganisationsControllerTest()
        {
        }

        #endregion Constructors

        [TestInitialize]
        public void Initialize()
        {
            this.MockOrganisationService = new Mock<IOrganisationsService>();
            this.MockOrganisationService.Setup(s => s.GetById(1))
                .Returns(Task.FromResult(new Organisation(1, "name", 1, "status", 1, "type")));
            this.MockOrganisationService.Setup(s => s.Get())
                .ReturnsAsync(new Organisation[] 
                {
                    new Organisation(1, "Org1", 1, "New", 1, "Supplier"),
                    new Organisation(2, "Org2", 1, "New", 2, "Verifier")
                });
            this.MockOrganisationService.Setup(s => s.GetSuppliers())
               .ReturnsAsync(new Organisation[]
               {
                    new Organisation(1, "Org1", 1, "New", 1, "Supplier")
               });

            this.ControllerUnderTest = new OrganisationsController(this.MockOrganisationService.Object);
        }
        #region Tests

        /// <summary>
        /// Tests that the GetAll method calls the appropriate Service method
        /// </summary>
        [TestMethod]
        public void OrganisationsController_GetAll_Returns_Organisations()
        {
            //arrange          

            //act
            var result = this.ControllerUnderTest.GetAll().Result;

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            // - We get the expected organisation details
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IList<Organisation>));
            var organisations = (IList<Organisation>)((OkObjectResult)result).Value;
            Assert.AreEqual(2, organisations.Count);
            // - Check that the appropriate Service method has been called
            this.MockOrganisationService.Verify(mock => mock.Get(), Times.Once);
            this.MockOrganisationService.Verify(mock => mock.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Tests that the GetSuppliers method calls the appropriate Service method
        /// </summary>
        [TestMethod]
        public void OrganisationsController_GetSuppliers_Returns_Suppliers()
        {
            //arrange          

            //act
            var result = this.ControllerUnderTest.GetSuppliers().Result;

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            // - We get the expected organisation details
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IList<Organisation>));
            var suppliers = (IList<Organisation>)((OkObjectResult)result).Value;
            Assert.AreEqual(1, suppliers.Count);
            // - Check that the appropriate Service method has been called
            this.MockOrganisationService.Verify(mock => mock.GetSuppliers(), Times.Once);
        }

        /// <summary>
        /// Tests that the GetById method returns valid Organisation
        /// </summary>
        [TestMethod]
        public void OrganisationsController_GetById_Returns_Organisation()
        {
            //arrange          
            var existingSupplierId = 1;

            //act
            var result = this.ControllerUnderTest.GetById(existingSupplierId).Result;

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            // - We get the expected organisation details
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(Organisation));
            var organisation = ((Organisation)((OkObjectResult)result).Value);
            Assert.AreEqual(existingSupplierId, organisation.Id);
        }
        
        /// <summary>
        /// Tests that the GetById method returns 404 if no organisation
        /// </summary>
        [TestMethod]
        public void OrganisationsController_GetById_Returns_404_Invalid_Organisation()
        {
            //arrange         
            var notExistingSupplierId = 100;

            //act
            var result = this.ControllerUnderTest.GetById(notExistingSupplierId).Result;

            //assert
            // - We get a 404 response
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));         
        }

        /// <summary>
        /// Tests that the GetByType method returns 404 if no organisation type
        /// </summary>
        [TestMethod]
        public void OrganisationsController_GetByType_Returns_400_Null_Organisation_Type()
        {
            //act
            var result = this.ControllerUnderTest.GetByType(null,"").Result;

            //assert
            // - We get a 400 response
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the GetByType method returns 404 if no organisation type
        /// </summary>
        [TestMethod]
        public void OrganisationsController_GetByType_Returns_400_Empty_Organisation_Type()
        {
            //act
            var result = this.ControllerUnderTest.GetByType(string.Empty, string.Empty).Result;

            //assert
            // - We get a 400 response
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the GetByType method returns 404 if invalid organisation type
        /// </summary>
        [TestMethod]
        public void OrganisationsController_GetByType_Returns_404_Invalid_Organisation_Type()
        {
            //arrange         
            var invalidOrganisationType = "InvalidType";

            //act
            var result = this.ControllerUnderTest.GetByType(invalidOrganisationType, string.Empty).Result;

            //assert
            // - We get a 404 response
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the GetByType method calls the repository's filtered get method
        /// </summary>
        [TestMethod]
        public void OrganisationsController_GetByType_Calls_FilteredGetServiceMethod()
        {
            //arrange
            var defaultPageSize = 20;
            var defaultPageNumber = 1;

            //act
            var result = this.ControllerUnderTest.GetByType("Supplier", string.Empty).Result;

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            // - The Filtered service method was called exactly once using the default page size and page number
            this.MockOrganisationService.Verify(s => s.Get(It.IsAny<string>(), It.IsAny<string>(), defaultPageSize, defaultPageNumber), Times.Once());
            // - The Get All service method was not called
            this.MockOrganisationService.Verify(s => s.Get(), Times.Never());
        }

        /// <summary>
        /// Tests that the GetByType method respects the supplied Page Size and Page Number parameters
        /// </summary>
        [TestMethod]
        public void OrganisationsController_GetByType_Respects_PageSize_And_PageNumber()
        {
            //arrange
            var pageSize = 5;
            var pageNumber = 2;

            //act
            var result = this.ControllerUnderTest.GetByType("Supplier", "*", pageSize, pageNumber).Result;

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            // - The Filtered service method was called exactly once using the default page size and page number
            this.MockOrganisationService.Verify(s => s.Get(It.IsAny<string>(), It.IsAny<string>(), pageSize, pageNumber), Times.Once());
            // - The Get All service method was not called
            this.MockOrganisationService.Verify(s => s.Get(), Times.Never());
        }


        [TestMethod]
        public void OrganisationsController_GetByType_Calls_WithSearchText()
        {
            //arrage
            this.MockOrganisationService.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(new PagedResult<Organisation>() { Result = new List<Organisation>() }));
            //act
            var result = this.ControllerUnderTest.GetByType("Supplier", "Supplier One").Result;

            //assert
            MockOrganisationService.Verify(x => x.Get("Supplier One", "supplier", 20, 1), Times.Once);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
        #endregion Tests

        #region Private Methods

        /// <summary>
        /// Sets up a configured instance of the controller under test
        /// </summary>
        /// <returns></returns>
        private void SetupControllerInstance()
        {
            this.MockOrganisationService = new Mock<IOrganisationsService>();
            this.MockOrganisationService.Setup(s => s.GetById(1))
                .ReturnsAsync(new Organisation(1, "name", 1, "status", 1, "type"));
            this.MockOrganisationService.Setup(s => s.Get())
                .ReturnsAsync(new Organisation[] { new Organisation(1, "Org1", 1, "Status1", 1, "Type1"), new Organisation(2, "Org2", 2, "Status2", 2, "Type2") });

            this.ControllerUnderTest = new OrganisationsController(this.MockOrganisationService.Object);
        }

        #endregion Private Methods
    }
}
