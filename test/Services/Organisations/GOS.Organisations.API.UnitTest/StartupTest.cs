﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using GOS.Organisations.API;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Moq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace DfT.GOS.Organisations.API.UnitTest
{
    /// <summary>
    /// Tests the Startup class
    /// </summary>
    [TestClass]
    public class StartupTest
    {
        #region Tests

        /// <summary>
        /// Tests that the CONNECTION_STRING variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_environmental_validates_connection_string_is_present()
        {
            //arrange
            bool nullExceptionThrown = false;

            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_ConnectionString);
            var startup = GetStartupInstance(configurationItems);

            //act
            try
            {
                startup.ConfigureServices(new ServiceCollection());
            }
            catch(NullReferenceException ex)
            {
                nullExceptionThrown = ex.Message == Startup.ErrorMessage_ConnectionStringNotSupplied;
            }

            //assert
            Assert.IsTrue(nullExceptionThrown);
        }

        #endregion Tests

        #region Private Methods

        /// <summary>
        /// Gets a dictionary of all configuration settings for this service
        /// </summary>
        /// <returns></returns>
        private Dictionary<string,string> GetAllConfigurationItems()
        {
            var configurationItems = new Dictionary<string, string>();
            configurationItems.Add(Startup.EnvironmentVariable_ConnectionString, "fake connection string");
            return configurationItems;
        }

        /// <summary>
        /// Gets an instance of the startup class for testing
        /// </summary>
        /// <returns></returns>
        private Startup GetStartupInstance(IDictionary<string,string> configurationItems)
        {
            var configuration = new ConfigurationBuilder();
            var mockConfiguration = new Mock<IConfiguration>();

            foreach (var configurationItem in configurationItems)
            {
                mockConfiguration.SetupGet(c => c[configurationItem.Key]).Returns(configurationItem.Value);
            }

            var mockLogger = new Mock<ILogger<Startup>>();
            return new Startup(mockConfiguration.Object, mockLogger.Object);
        }

        #endregion Private Methods

    }
}
