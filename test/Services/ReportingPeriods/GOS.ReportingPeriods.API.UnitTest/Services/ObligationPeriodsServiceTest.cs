﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.API.Repositories;
using DfT.GOS.ReportingPeriods.API.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests for Obligation Periods Service
    /// </summary>
    [TestClass]
    public class ObligationPeriodsServiceTest
    {
        #region Properties

        private Mock<IObligationPeriodsRepository> ObligationPeriodsRepository { get; set; }

        private ObligationPeriodsService ObligationPeriodsService { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Initialisation performed for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.ObligationPeriodsRepository = new Mock<IObligationPeriodsRepository>();
            this.ObligationPeriodsService = new ObligationPeriodsService(this.ObligationPeriodsRepository.Object);
        }

        #endregion Initialize

        #region Tests

        #region Get All tests

        /// <summary>
        /// Tests that the Get All method returns a collection of Obligation Periods
        /// </summary>
        [TestMethod]
        public async Task ObligationPeriodsService_Get_Returns_All_Obligation_Periods()
        {
            //arrange
            this.ObligationPeriodsRepository.Setup(mock => mock.Get())
                .ReturnsAsync(new ObligationPeriod[] { new ObligationPeriod(14, null, null, false, null, 0, 0, null, null, null, null, null, null) });

            //act
            var result = await this.ObligationPeriodsService.Get();

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IList<ObligationPeriod>));
        }

        #endregion Get All tests

        #region Get Current tests

        /// <summary>
        /// Tests that the Get Current method returns the current Obligation Period
        /// </summary>
        [TestMethod]
        public async Task ObligationPeriodsService_GetCurrent_Returns_Current_Obligation_Periods()
        {
            //arrange
            var obligationPeriod = new ObligationPeriod(14, null, null, false, null, 0, 0, null, null, null, null, null, null);
            this.ObligationPeriodsRepository.Setup(mock => mock.GetCurrent())
                .ReturnsAsync(obligationPeriod);

            //act
            var result = await this.ObligationPeriodsService.GetCurrent();

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ObligationPeriod));
            Assert.AreEqual(obligationPeriod, result);
        }

        #endregion Get Current tests

        #region Get By Id tests

        /// <summary>
        /// Tests that the Get By Id method returns the requested Obligation Period
        /// </summary>
        [TestMethod]
        public async Task ObligationPeriodsService_GetById_Returns_Requested_Obligation_Periods()
        {
            //arrange
            var obligationPeriodId = 14;
            var obligationPeriod = new ObligationPeriod(obligationPeriodId, null, null, false, null, 0, 0, null, null, null, null, null, null);
            this.ObligationPeriodsRepository.Setup(mock => mock.Get(obligationPeriodId))
                .ReturnsAsync(obligationPeriod);

            //act
            var result = await this.ObligationPeriodsService.Get(obligationPeriodId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ObligationPeriod));
            Assert.AreEqual(obligationPeriod, result);
        }

        #endregion Get By Id tests

        #endregion Tests
    }
}
