// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.API.Controllers;
using DfT.GOS.ReportingPeriods.API.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests the Reporting Periods controller
    /// </summary>
    [TestClass]
    public class ObligationPeriodsControllerTest
    {
        #region Constants

        private const decimal DEFAULT_GHG_THRESHOLD = (decimal)90.34;
        private const int DEFAULT_GHG_LOW_VOLUME_DEDUCTION = 59352;
        private const decimal DEFAULT_GHG_BUYOUT_PENCE = (decimal)7.4;

        #endregion Constants

        #region Properties

        private Mock<IObligationPeriodsService> ObligationPeriodsService { get; set; }

        private ObligationPeriodsController Controller { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ObligationPeriodsControllerTest()
        {
        }

        #endregion Constructors

        #region Initialize

        /// <summary>
        /// Performs initialisation before each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.ObligationPeriodsService = new Mock<IObligationPeriodsService>();
            this.Controller = new ObligationPeriodsController(this.ObligationPeriodsService.Object);
        }

        #endregion Initialize

        #region Private Methods

        /// <summary>
        /// Gets a configured instance of the controller under test
        /// </summary>
        /// <param name="includeCurrentObligationPeriod">Whether the current obligation peirod should be included or not</param>
        /// <returns>Returns a list of obligation periods</returns>
        private void SetupControllerInstance(bool includeCurrentObligationPeriod)
        {
            var currentObligationPeriodStartDate = DateTime.Now.AddDays(-1);
            var currentObligationPeriodEndDate = DateTime.Now.AddDays(1);

            var previousObligationPeriod = new ObligationPeriod(1
                    , currentObligationPeriodStartDate.AddYears(-1)
                    , currentObligationPeriodStartDate.AddDays(-1)
                    , true
                    , null
                    , 10
                    , 100
                    , null
                    , null
                    , null
                    , DEFAULT_GHG_THRESHOLD
                    , DEFAULT_GHG_LOW_VOLUME_DEDUCTION
                    , DEFAULT_GHG_BUYOUT_PENCE);

            var currentObligationPeriod = new ObligationPeriod(2
                , currentObligationPeriodStartDate
                , currentObligationPeriodEndDate
                , true
                , null
                , 10
                , 100
                , null
                , null
                , null
                , DEFAULT_GHG_THRESHOLD
                , DEFAULT_GHG_LOW_VOLUME_DEDUCTION
                , DEFAULT_GHG_BUYOUT_PENCE);

            var futureObligationPeriod = new ObligationPeriod(3
               , currentObligationPeriodEndDate.AddDays(1)
               , currentObligationPeriodEndDate.AddYears(1)
               , true
               , null
               , 10
               , 100
               , null
               , null
               , null
               , DEFAULT_GHG_THRESHOLD
               , DEFAULT_GHG_LOW_VOLUME_DEDUCTION
               , DEFAULT_GHG_BUYOUT_PENCE);


            //Setup the Get All
            this.ObligationPeriodsService.Setup(s => s.Get())
                .Returns(Task.Run<IList<ObligationPeriod>>(() =>
                {
                    var obligationPeriods = new List<ObligationPeriod>();
                    
                    obligationPeriods.Add(previousObligationPeriod);
                    if (includeCurrentObligationPeriod)
                    {
                        obligationPeriods.Add(currentObligationPeriod);
                    }
                    obligationPeriods.Add(futureObligationPeriod);

                    return obligationPeriods;
                }));

            //Setup the Get Current
            this.ObligationPeriodsService.Setup(s => s.GetCurrent())
                .Returns(Task.Run<ObligationPeriod>(() =>
                {
                    ObligationPeriod obligationPeriod = null;
                    if(includeCurrentObligationPeriod)
                    {
                        obligationPeriod = currentObligationPeriod;
                    }
                    return obligationPeriod;
                }));
        }

        #endregion Private Methods

        #region Tests

        #region GetAll tests

        /// <summary>
        /// Tests that the Get method returns a result
        /// </summary>
        [TestMethod]
        public async Task ObligationPeriodsController_GetAll_Returns_Result()
        {
            //arrange
            this.SetupControllerInstance(true);

            //act
            var result = await this.Controller.GetAll();

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            // - We get a list of obligation periods back
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IList<ObligationPeriod>));
        }

        #endregion GetAll Tests

        #region GetCurrent tests

        /// <summary>
        /// Tests that the GetCurrent method returns the correct obligation period
        /// </summary>
        [TestMethod]
        public async Task ObligationPeriodsController_GetCurrent_Returns_CurrentObligationPeriod()
        {
            //arrange
            this.SetupControllerInstance(true);

            //act
            var result = await this.Controller.GetCurrent();

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            // - We get a single obligation period back
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(ObligationPeriod));
        }

        /// <summary>
        /// Tests that the GetCurrent method returns 404 if there is no current obligation period
        /// </summary>
        [TestMethod]
        public async Task ObligationPeriodsController_GetCurrent_Returns_404_where_no_CurrentObligationPeriod()
        {
            //arrange
            this.SetupControllerInstance(false);

            //act
            var result = await this.Controller.GetCurrent();

            //assert
            // - We get a 404 response
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));         
        }

        #endregion GetCurrent tests

        #region Get By Id tests

        /// <summary>
        /// Tests that the GetById method returns the correct obligation period
        /// </summary>
        [TestMethod]
        public async Task ObligationPeriodsController_GetById_Returns_Reuqested_ObligationPeriod()
        {
            //arrange
            var obligationPeriodId = 14;
            this.ObligationPeriodsService.Setup(mock => mock.Get(obligationPeriodId))
                .ReturnsAsync(new ObligationPeriod(obligationPeriodId, null, null, false, null, 0, 0, null, null, null, null, null, null));

            //act
            var result = await this.Controller.Get(obligationPeriodId);

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            // - We get a single obligation period back
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(ObligationPeriod));
        }

        /// <summary>
        /// Tests that the GetCurrent method returns 404 if there is no current obligation period
        /// </summary>
        [TestMethod]
        public async Task ObligationPeriodsController_GetById_Returns_Not_Found()
        {
            //arrange
            var obligationPeriodId = 14;

            //act
            var result = await this.Controller.Get(obligationPeriodId);

            //assert
            // - We get a 404 response
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        #endregion Get By Id tests

        #endregion Tests
    }
}
