﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgFuels.API.Services;
using DfT.GOS.GhgFuels.Common.Models;
using GOS.GhgFuels.API.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GOS.GhgFuels.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests the Fuel Types controller
    /// </summary>
    [TestClass]
    public class FuelTypesControllerTest
    {
        #region Properties

        private FuelTypesController ControllerUnderTest { get; set; }
        private Mock<IFuelTypesService> MockFuelTypesService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public FuelTypesControllerTest()
        {
            this.SetupControllerInstance();
        }

        #endregion Constructors


        #region Private Methods

        /// <summary>
        /// Sets up a configured instance of the controller under test
        /// </summary>
        /// <returns></returns>
        private void SetupControllerInstance()
        {
            this.MockFuelTypesService = new Mock<IFuelTypesService>();
            this.MockFuelTypesService.Setup(s => s.Get())
                .Returns(Task.Run<IList<FuelType>>(() =>
                {
                    var fuelTypes = new List<FuelType>();

                    var fuelType = new FuelType(1, 1, "Electricity", "Electricity", null, null);
                    fuelTypes.Add(fuelType);

                    return fuelTypes;
                }));

            this.ControllerUnderTest = new FuelTypesController(this.MockFuelTypesService.Object);
        }

        #endregion Private Methods

        #region Tests

        /// <summary>
        /// Tests that the Get method returns a result
        /// </summary>
        [TestMethod]
        public void ObligationPeriodsController_Get_Returns_Result()
        {
            //act
            var result = this.ControllerUnderTest.GetAll().Result;

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            // - We get a list of fuel types back
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IList<FuelType>));
        }

        #endregion Tests
    }
}
