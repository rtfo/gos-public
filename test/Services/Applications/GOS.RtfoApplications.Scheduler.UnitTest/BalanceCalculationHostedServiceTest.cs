// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Messaging.Client;
using DfT.GOS.Messaging.Messages;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.Scheduler.UnitTest
{
    /// <summary>
    /// Tests for the BalanceCalculationHostedService
    /// </summary>
    [TestClass]
    public class BalanceCalculationHostedServiceTest
    {
        /// <summary>
        /// Tests that the BalanceCalculationHostedService publish the message to rabbit mq
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task StartAsyncTest()
        {
            //Arrange
            IServiceCollection services = new ServiceCollection();
            
            //Inject all dependencies
            //Logger
            var _mockLogger = new Mock<ILogger<BalanceCalculationHostedService>>();
            services.AddSingleton<ILogger<BalanceCalculationHostedService>>(_mockLogger.Object);
            //MessageClient
            var mockIMessageClient = new Mock<IMessagingClient>();
            services.AddSingleton<IMessagingClient>(mockIMessageClient.Object);
            //Appsettings
            var appSettings = new AppSettings() { TimeIntervalInMinutes = 0.1 };
            var options = Options.Create(appSettings);
            services.AddSingleton(options);
            //Add the hosted service
            services.AddHostedService<BalanceCalculationHostedService>();
            var serviceProvider = services.BuildServiceProvider();

            var service = serviceProvider.GetService<IHostedService>();
            mockIMessageClient.Setup(x => x.Publish(It.IsAny<RosUpdatedMessage>()));
            
            //act    
            await service.StartAsync(CancellationToken.None);
            //delay by .1 minute so that the timer call back kicks in
            await Task.Delay(150);
            
            //assert
            mockIMessageClient.Verify(x => x.Publish(It.IsAny<RosUpdatedMessage>()), Times.Once);

            
            await service.StopAsync(CancellationToken.None);
        }
    }
}
