﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT). See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace GOS.GhgApplications.Common.UnitTest.Commands
{
    [TestClass]
    public class CreateApplicationCommandTest
    {
        /// <summary>
        /// Tests that the user can enter a CI of 0 when creating Electricity applications
        /// </summary>
        [TestMethod]
        public void CreateApplicationCommand_GhgIntensity_Allows_Zero_Value()
        {
            // Arrange
            var command = new CreateApplicationCommand(7, 15, 1000, 51, 0, "testing");
            var validationResults = new List<ValidationResult>();

            // Act
            var result = Validator.TryValidateObject(command, new ValidationContext(command), validationResults, true);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(0, validationResults.Count);
        }

        /// <summary>
        /// Tests that the user can enter a CI greater than 0 when creating Electricity applications
        /// </summary>
        [TestMethod]
        public void CreateApplicationCommand_GhgIntensity_Allows_Value_Greater_Than_Zero()
        {
            // Arrange
            var command = new CreateApplicationCommand(7, 15, 1000, 51, 0.1m, "testing");
            var validationResults = new List<ValidationResult>();

            // Act
            var result = Validator.TryValidateObject(command, new ValidationContext(command), validationResults, true);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(0, validationResults.Count);
        }

        /// <summary>
        /// Tests that the user can enter a CI less than 0 when creating Electricity applications
        /// </summary>
        [TestMethod]
        public void CreateApplicationCommand_GhgIntensity_Allows_Value_Less_Than_Zero()
        {
            // Arrange
            var command = new CreateApplicationCommand(7, 15, 1000, 51, -0.1m, "testing");
            var validationResults = new List<ValidationResult>();

            // Act
            var result = Validator.TryValidateObject(command, new ValidationContext(command), validationResults, true);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(0, validationResults.Count);
        }
    }
}
