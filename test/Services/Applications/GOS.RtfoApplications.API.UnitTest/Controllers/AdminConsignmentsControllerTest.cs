﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.RtfoApplications.API.Controllers;
using DfT.GOS.RtfoApplications.API.Models;
using DfT.GOS.RtfoApplications.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the Admin Consignments Controller
    /// </summary>
    [TestClass]
    public class AdminConsignmentsControllerTest
    {
        #region Constants

        private const int DefaultOrganisationId = 13;

        #endregion Constants

        #region Properties

        private Mock<IAdminConsignmentsService> AdminConsignmentsService { get; set; }
        private Mock<IReportingPeriodsService> ReportingPeriodsService { get; set; }
        private Mock<IOrganisationsService> OrganisationsService { get; set; }
        private AdminConsignmentsController Controller { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Initializes the objects used for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.AdminConsignmentsService = new Mock<IAdminConsignmentsService>();
            this.ReportingPeriodsService = new Mock<IReportingPeriodsService>();
            this.OrganisationsService = new Mock<IOrganisationsService>();

            var claims = new Claim[]
            {
                new Claim(Claims.UserId, Guid.NewGuid().ToString()),
                new Claim(Claims.OrganisationId, DefaultOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            this.Controller = new AdminConsignmentsController(this.AdminConsignmentsService.Object, this.ReportingPeriodsService.Object, this.OrganisationsService.Object);
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialize

        #region Tests

        #region Get by Obligation Period tests

        /// <summary>
        /// Tests that the Get method retrieves Admin Consignments by Obligation Period Id
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsController_Get_Retrieves_AdminConsignments_By_ObligationPeriodId()
        {
            //arrange
            var obligationPeriodId = 14;
            this.ReportingPeriodsService.Setup(mock => mock.GetObligationPeriod(obligationPeriodId))
                .ReturnsAsync(new ObligationPeriod(obligationPeriodId, null, null, false, null, 0, 0, null, null, null, null, null, null));
            this.AdminConsignmentsService.Setup(mock => mock.Get(obligationPeriodId))
                .ReturnsAsync(new AdminConsignment[]
                {
                    new AdminConsignment()
                    {
                         AdminConsignmentId = 1,
                          CarbonIntensity = 456,
                          EnergyDensity = 5678.34M,
                          FeedStockName = "Test Feedstock",
                          FuelTypeName = "Test Fuel Type",
                          ObligationPeriodId = obligationPeriodId,
                          OrganisationId = DefaultOrganisationId,
                          Reference = "ABC123",
                          ReportedMonthEndDate = DateTime.Now,
                          Volume = 45679
                    }
                });

            //act
            var result = await this.Controller.GetAdminConsignments(obligationPeriodId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IEnumerable<AdminConsignment>));
            Assert.AreEqual(1, ((IEnumerable<AdminConsignment>)(((OkObjectResult)result).Value)).Count());
        }

        /// <summary>
        /// Tests that the Get method handles the case where the Obligation Period Id does not exist
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsController_Get_Handles_Invalid_ObligationPeriodId()
        {
            //arrange
            var obligationPeriodId = 140;
            this.ReportingPeriodsService.Setup(mock => mock.GetObligationPeriod(obligationPeriodId))
                .ReturnsAsync((ObligationPeriod)null);

            //act
            var result = await this.Controller.GetAdminConsignments(obligationPeriodId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.IsNotNull(((BadRequestObjectResult)result).Value);
            Assert.IsInstanceOfType(((BadRequestObjectResult)result).Value, typeof(string));
        }

        /// <summary>
        /// Tests that the Get method (for Obligation Periods) forbids access to non-administrators
        /// </summary>
        [TestMethod]
        public void AdminConsignmentsController_Get_Forbids_Access_To_Non_Administrators()
        {
            //arrange

            //act
            var type = this.Controller.GetType();
            var method = type.GetMethod("GetAdminConsignments", new Type[] { typeof(int) });
            var authorizeAttribute = method.CustomAttributes
                .Where(attr => attr.AttributeType == typeof(AuthorizeAttribute))
                .SingleOrDefault();

            //assert
            Assert.IsNotNull(authorizeAttribute);
            Assert.IsNotNull(authorizeAttribute.NamedArguments);
            Assert.AreEqual(1, authorizeAttribute.NamedArguments.Count());
            Assert.IsNotNull(authorizeAttribute.NamedArguments.First());
            Assert.AreEqual("Roles", authorizeAttribute.NamedArguments.First().MemberName);
            Assert.AreEqual(Roles.Administrator, authorizeAttribute.NamedArguments.First().TypedValue.Value);
        }

        #endregion Get by Obligation Period tests

        #region Get by Obligation Period and Supplier tests

        /// <summary>
        /// Tests that the Get method retrieves Admin Consignments by Obligation Period Id
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsController_Get_Retrieves_AdminConsignments_By_ObligationPeriodId_And_SupplierId()
        {
            //arrange
            var obligationPeriodId = DefaultOrganisationId;
            var organisationId = DefaultOrganisationId;
            this.ReportingPeriodsService.Setup(mock => mock.GetObligationPeriod(obligationPeriodId))
                .ReturnsAsync(new ObligationPeriod(obligationPeriodId, null, null, false, null, 0, 0, null, null, null, null, null, null));
            this.OrganisationsService.Setup(mock => mock.GetOrganisation(organisationId, string.Empty))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(organisationId, "test supplier", 1, "New", 1, "New")));
            this.AdminConsignmentsService.Setup(mock => mock.Get(obligationPeriodId, organisationId))
                .ReturnsAsync(new AdminConsignment[]
                {
                    new AdminConsignment()
                    {
                         AdminConsignmentId = 1,
                          CarbonIntensity = 456,
                          EnergyDensity = 5678.34M,
                          FeedStockName = "Test Feedstock",
                          FuelTypeName = "Test Fuel Type",
                          ObligationPeriodId = obligationPeriodId,
                          OrganisationId = DefaultOrganisationId,
                          Reference = "ABC123",
                          ReportedMonthEndDate = DateTime.Now,
                          Volume = 45679
                    }
                });

            //act
            var result = await this.Controller.GetAdminConsignments(obligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IEnumerable<AdminConsignment>));
            Assert.AreEqual(1, ((IEnumerable<AdminConsignment>)(((OkObjectResult)result).Value)).Count());
        }

        /// <summary>
        /// Tests that the Get method handles the case where the Obligation Period Id does not exist
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsController_Get_Handles_Invalid_ObligationPeriodId_And_Valid_SupplierId()
        {
            //arrange
            var obligationPeriodId = 140;
            var organisationId = DefaultOrganisationId;
            this.ReportingPeriodsService.Setup(mock => mock.GetObligationPeriod(obligationPeriodId))
                .ReturnsAsync((ObligationPeriod)null);
            this.OrganisationsService.Setup(mock => mock.GetOrganisation(organisationId, string.Empty))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(HttpStatusCode.OK));

            //act
            var result = await this.Controller.GetAdminConsignments(obligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.IsNotNull(((BadRequestObjectResult)result).Value);
            Assert.IsInstanceOfType(((BadRequestObjectResult)result).Value, typeof(string));
        }

        /// <summary>
        /// Tests that the Get method handles the case where the Organisation Id does not exist
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsController_Get_Handles_Valid_ObligationPeriodId_And_Invalid_SupplierId()
        {
            //arrange
            var obligationPeriodId = 140;
            var organisationId = DefaultOrganisationId;
            this.ReportingPeriodsService.Setup(mock => mock.GetObligationPeriod(obligationPeriodId))
                .ReturnsAsync(new ObligationPeriod(obligationPeriodId, null, null, false, null, 0, 0, null, null, null, null, null, null));
            this.OrganisationsService.Setup(mock => mock.GetOrganisation(organisationId, string.Empty))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound));

            //act
            var result = await this.Controller.GetAdminConsignments(obligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.IsNotNull(((BadRequestObjectResult)result).Value);
            Assert.IsInstanceOfType(((BadRequestObjectResult)result).Value, typeof(string));
        }

        /// <summary>
        /// Tests that the Get method handles the case where the user does not have access to the specified Supplier
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsController_Get_Forbids_Access_To_Wrong_Supplier()
        {
            //arrange
            var obligationPeriodId = 140;
            var supplierId = 2400;

            //act
            var result = await this.Controller.GetAdminConsignments(obligationPeriodId, supplierId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Get method allows Admin users to access any Supplier
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsController_Get_Allows_Admin_Access_To_Any_Supplier()
        {
            //arrange
            var obligationPeriodId = 140;
            var organisationId = 2400;
            this.ReportingPeriodsService.Setup(mock => mock.GetObligationPeriod(obligationPeriodId))
                .ReturnsAsync(new ObligationPeriod(obligationPeriodId, null, null, false, null, 0, 0, null, null, null, null, null, null));
            this.OrganisationsService.Setup(mock => mock.GetOrganisation(organisationId, string.Empty))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(organisationId, "test supplier", 1, "New", 1, "New")));
            var claims = new Claim[]
            {
                new Claim(Claims.UserId, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, Roles.Administrator)
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };

            //act
            var result = await this.Controller.GetAdminConsignments(obligationPeriodId, organisationId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IEnumerable<AdminConsignment>));
            Assert.AreEqual(0, ((IEnumerable<AdminConsignment>)(((OkObjectResult)result).Value)).Count());
        }

        #endregion Get by Obligation Period and Supplier tests

        #endregion Tests
    }
}
