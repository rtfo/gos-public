﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DfT.GOS.RtfoApplications.API.Controllers;
using DfT.GOS.RtfoApplications.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DfT.GOS.RtfoApplications.API.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the Performance Data Controller
    /// </summary>
    [TestClass]
    public class PerformanceDataControllerTest
    {
        #region Constants

        private const int DefaultOrganisationId = 13;

        #endregion Constants

        #region Properties

        private Mock<IAdminConsignmentsService> AdminConsignmentsService { get; set; }
        private PerformanceDataController Controller { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Initializes the objects used for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.AdminConsignmentsService = new Mock<IAdminConsignmentsService>();

            var claims = new Claim[]
            {
                new Claim(Claims.UserId, Guid.NewGuid().ToString()),
                new Claim(Claims.OrganisationId, DefaultOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            this.Controller = new PerformanceDataController(this.AdminConsignmentsService.Object);
            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialize

        #region Tests

        /// <summary>
        /// Tests the RtfoPerformanceData happy path
        /// </summary>
        [TestMethod]
        public async Task PerformanceDataController_RtfoPerformanceData_Retrieves_PerformanceData()
        {
            //arrange

            //act
            var result = await this.Controller.RtfoPerformanceData();

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }

        /// <summary>
        /// Tests that the Get method (for Obligation Periods) forbids access to non-administrators
        /// </summary>
        [TestMethod]
        public void PerformanceDataController_RtfoPerformanceData_Forbids_Access_To_Non_Administrators()
        {
            //arrange

            //act
            var type = this.Controller.GetType();
            var method = type.GetMethod("RtfoPerformanceData");
            var authorizeAttribute = method.CustomAttributes
                .Where(attr => attr.AttributeType == typeof(AuthorizeAttribute))
                .SingleOrDefault();

            //assert
            Assert.IsNotNull(authorizeAttribute);
            Assert.IsNotNull(authorizeAttribute.NamedArguments);
            Assert.AreEqual(1, authorizeAttribute.NamedArguments.Count());
            Assert.IsNotNull(authorizeAttribute.NamedArguments.First());
            Assert.AreEqual("Roles", authorizeAttribute.NamedArguments.First().MemberName);
            Assert.AreEqual(Roles.Administrator, authorizeAttribute.NamedArguments.First().TypedValue.Value);
        }

        #endregion Tests
    }
}
