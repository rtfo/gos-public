﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using AutoMapper;
using DfT.GOS.RtfoApplications.API.Repositories;
using DfT.GOS.RtfoApplications.API.Repositories.ROS;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.UnitTest.Repositories
{
    /// <summary>
    /// Provides unit tests for the Ros Admin Consignment Repository
    /// </summary>
    [TestClass]
    public class RosAdminConsignmentsRepositoryTest
    {
        #region Properties
        private IAdminConsignmentsRepository AdminConsignmentsRepository { get; set; }

        #endregion Properties

        #region GetAdminConsignmentCounts Tests

        /// <summary>
        /// Tests that GetAdminConsignmentCounts method groups admin consignments by month (and year).
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task RosAdminConsignmentsRepository_GetAdminConsignmentCounts_Groups_By_Month()
        {
            //arrange
            var options = new DbContextOptionsBuilder<RosDataContext>()
                .UseInMemoryDatabase(databaseName: "GetAdminConsignmentCounts" + Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();
            var context = new RosDataContext(options);
            context.GosAdminConsignments.Add(new GosAdminConsignments() { ReportedMonthEndDate = new DateTime(2019, 11, 1) });
            context.GosAdminConsignments.Add(new GosAdminConsignments() { ReportedMonthEndDate = new DateTime(2019, 12, 5)});
            context.GosAdminConsignments.Add(new GosAdminConsignments() { ReportedMonthEndDate = new DateTime(2019, 12, 10) });
            context.GosAdminConsignments.Add(new GosAdminConsignments() { ReportedMonthEndDate = new DateTime(2020, 12, 5) });
            context.SaveChanges();

            this.AdminConsignmentsRepository = new RosAdminConsignmentsRepository(context, mapper.Object);

            //act
            var results = await this.AdminConsignmentsRepository.GetAdminConsignmentCounts();

            //assert
            Assert.AreEqual(3, results.Count());
            Assert.AreEqual(1, results.Single(ac => ac.Date == new DateTime(2019, 11, 1)).AdminConsignmentCount);
            Assert.AreEqual(2, results.Single(ac => ac.Date == new DateTime(2019, 12, 1)).AdminConsignmentCount);
            Assert.AreEqual(1, results.Single(ac => ac.Date == new DateTime(2020, 12, 1)).AdminConsignmentCount);
        }

        #endregion GetAdminConsignmentCounts Tests
    }
}
