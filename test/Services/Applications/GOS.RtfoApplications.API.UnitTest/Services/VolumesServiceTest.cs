﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.RtfoApplications.API.Models;
using DfT.GOS.RtfoApplications.API.Repositories;
using DfT.GOS.RtfoApplications.API.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests for the VolumesService class
    /// </summary>
    [TestClass]
    public class VolumesServiceTest
    {
        #region Properties

        private Mock<IVolumesRepository> Repository { get; set; }
        private IVolumesService Service { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Initializes the mock objects used for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.Repository = new Mock<IVolumesRepository>();
            this.Service = new VolumesService(this.Repository.Object);
        }

        #endregion Initialize

        #region Tests

        #region Get tests

        /// <summary>
        /// Tests that the Get method can retrieve Volumnes by Obligation Period
        /// </summary>
        [TestMethod]
        public async Task VolumesService_Get_Retrieves_Volumes_By_ObligationPeriodId()
        {
            //arrange
            var obligationPeriodId = 14;
            this.Repository.Setup(mock => mock.Get(obligationPeriodId))
                .ReturnsAsync(new NonRenewableVolume[] 
                {
                    new NonRenewableVolume()
                    {
                         CarbonIntensity = 1,
                         EnergyDensity = 2,
                         FuelTypeName = "Diesel",
                         ObligationPeriodId = obligationPeriodId,
                         OrganisationId = 34,
                         ReportedMonthEndDate = DateTime.Now,
                         Volume = 10345
                    }
                });

            //assert
            var result = await this.Service.Get(obligationPeriodId);

            //act
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IEnumerable<NonRenewableVolume>));
            Assert.AreEqual(1, result.Count());
        }

        /// <summary>
        /// Tests that the Get method can retrieve Volumnes by Obligation Period and Supplier
        /// </summary>
        [TestMethod]
        public async Task VolumesService_Get_Retrieves_Volumes_By_ObligationPeriodId_And_OrganisationId()
        {
            //arrange
            var obligationPeriodId = 14;
            var organisationId = 24;
            this.Repository.Setup(mock => mock.Get(obligationPeriodId, organisationId))
                .ReturnsAsync(new NonRenewableVolume[]
                {
                    new NonRenewableVolume()
                    {
                         CarbonIntensity = 1,
                         EnergyDensity = 2,
                         FuelTypeName = "Diesel",
                         ObligationPeriodId = obligationPeriodId,
                         OrganisationId = organisationId,
                         ReportedMonthEndDate = DateTime.Now,
                         Volume = 10345
                    }
                });

            //assert
            var result = await this.Service.Get(obligationPeriodId, organisationId);

            //act
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IEnumerable<NonRenewableVolume>));
            Assert.AreEqual(1, result.Count());
        }

        #endregion Get tests

        #endregion Tests
    }
}
