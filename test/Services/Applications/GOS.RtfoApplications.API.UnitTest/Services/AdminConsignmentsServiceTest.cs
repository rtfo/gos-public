﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.RtfoApplications.API.Models;
using DfT.GOS.RtfoApplications.API.Repositories;
using DfT.GOS.RtfoApplications.API.Services;
using GOS.RtfoApplications.Common.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests for the AdminConsignmentsService class
    /// </summary>
    [TestClass]
    public class AdminConsignmentsServiceTest
    {
        #region Properties

        private Mock<IAdminConsignmentsRepository> Repository { get; set; }
        private IAdminConsignmentsService Service { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Initializes the mock objects used for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.Repository = new Mock<IAdminConsignmentsRepository>();
            this.Service = new AdminConsignmentsService(this.Repository.Object);
        }

        #endregion Initialize

        #region Tests

        #region Get tests

        /// <summary>
        /// Tests that the Get method can retrieve Admin Consignments by Obligation Period
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsService_Get_Retrieves_AdminConsignments_By_ObligationPeriodId()
        {
            //arrange
            var obligationPeriodId = 14;
            this.Repository.Setup(mock => mock.Get(obligationPeriodId))
                .ReturnsAsync(new AdminConsignment[]
                {
                    new AdminConsignment()
                    {
                        AdminConsignmentId = 567,
                        CarbonIntensity = 1,
                        EnergyDensity = 2,
                        FeedStockName = "Sugar beet",
                        FuelTypeName = "Bio-ethanol",
                        ObligationPeriodId = obligationPeriodId,
                        OrganisationId = 34,
                        Reference = "AC123",
                        ReportedMonthEndDate = DateTime.Now,
                        Volume = 10345
                    }
                });

            //assert
            var result = await this.Service.Get(obligationPeriodId);

            //act
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IEnumerable<AdminConsignment>));
            Assert.AreEqual(1, result.Count());
        }

        /// <summary>
        /// Tests that the Get method can retrieve Admin Consignments by Obligation Period and Supplier
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsService_Get_Retrieves_AdminConsignments_By_ObligationPeriodId_And_OrganisationId()
        {
            //arrange
            var obligationPeriodId = 14;
            var organisationId = 24;
            this.Repository.Setup(mock => mock.Get(obligationPeriodId, organisationId))
                .ReturnsAsync(new AdminConsignment[]
                {
                    new AdminConsignment()
                    {
                        AdminConsignmentId = 567,
                        CarbonIntensity = 1,
                        EnergyDensity = 2,
                        FeedStockName = "Sugar beet",
                        FuelTypeName = "Bio-ethanol",
                        ObligationPeriodId = obligationPeriodId,
                        OrganisationId = 34,
                        Reference = "AC123",
                        ReportedMonthEndDate = DateTime.Now,
                        Volume = 10345
                    }
                });

            //assert
            var result = await this.Service.Get(obligationPeriodId, organisationId);

            //act
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IEnumerable<AdminConsignment>));
            Assert.AreEqual(1, result.Count());
        }

        #endregion Get tests

        #region Performance Data tests

        /// <summary>
        /// Tests that the GetPerformanceData method happy path
        /// </summary>
        [TestMethod]
        public async Task AdminConsignmentsService_GetPerformanceData_Retieves_PerformanceData()
        {
            //arrange            
            this.Repository.Setup(mock => mock.GetAdminConsignmentCounts())
                .ReturnsAsync(new RtfoPerformanceData[]
                {
                    new RtfoPerformanceData(DateTime.Now, 1)
                });

            //assert
            var result = await this.Service.GetPerformanceData();

            //act
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IEnumerable<RtfoPerformanceData>));
            Assert.AreEqual(1, result.Count());
        }

        #endregion Performance Data tests

        #endregion Tests
    }
}
