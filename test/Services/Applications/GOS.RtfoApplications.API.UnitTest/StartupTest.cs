﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.UnitTest.Common.Attributes;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace DfT.GOS.RtfoApplications.API.UnitTest
{
    /// <summary>
    /// Unit tests for the Startup configuration
    /// </summary>
    [TestClass]
    public class StartupTest
    {
        #region Properties

        private Startup Startup { get; set; }
        private Mock<IConfiguration> Configuration { get; set; }
        private Mock<ILogger<Startup>> Logger { get; set; }

        #endregion Properties

        #region Initialize

        /// <summary>
        /// Performs initialization for each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.Configuration = new Mock<IConfiguration>();
            this.Logger = new Mock<ILogger<Startup>>();
            this.Startup = new Startup(this.Configuration.Object, this.Logger.Object);
        }

        #endregion Initialize

        #region Tests

        #region ConfigureServices tests

        /// <summary>
        /// Tests that the validation checks that the CONNECTION_STRING environment variable exists
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(NullReferenceException), Startup.ErrorMessage_ConnectionStringNotSupplied)]
        public void Startup_ConfigureServices_Identifies_Missing_ConnectionString()
        {
            //arrange
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_JwtTokenAudience])
                .Returns("dummy JwTTokenAudience");
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_JwtTokenIssuer])
                .Returns("dummy JwtTokenIssuer");
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_JwtTokenKey])
                .Returns("dummy JwtTokenKey");

            //act
            this.Startup.ConfigureServices(new ServiceCollection());

            //assert
        }

        /// <summary>
        /// Tests that the validation checks that the JWT_TOKEN_AUDIENCE environment variable exists
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(NullReferenceException), Startup.ErrorMessage_JwtTokenAudience)]
        public void Startup_ConfigureServices_Identifies_Missing_JwtTokenAudience()
        {
            //arrange
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_ConnectionString])
                .Returns("dummy connection string");
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_JwtTokenIssuer])
                .Returns("dummy JwtTokenIssuer");
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_JwtTokenKey])
                .Returns("dummy JwtTokenKey");

            //act
            this.Startup.ConfigureServices(new ServiceCollection());

            //assert
        }

        /// <summary>
        /// Tests that the validation checks that the JWT_TOKEN_ISSUER environment variable exists
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(NullReferenceException), Startup.ErrorMessage_JwtTokenIssuer)]
        public void Startup_ConfigureServices_Identifies_Missing_JwtTokenIssuer()
        {
            //arrange
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_ConnectionString])
                .Returns("dummy connection string");
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_JwtTokenAudience])
                .Returns("dummy JwtTokenAudience");
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_JwtTokenKey])
                .Returns("dummy JwtTokenKey");

            //act
            this.Startup.ConfigureServices(new ServiceCollection());

            //assert
        }

        /// <summary>
        /// Tests that the validation checks that the JWT_TOKEN_KEY environment variable exists
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(NullReferenceException), Startup.ErrorMessage_JwtTokenKey)]
        public void Startup_ConfigureServices_Identifies_Missing_JwtTokenKey()
        {
            //arrange
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_ConnectionString])
                .Returns("dummy connection string");
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_JwtTokenAudience])
                .Returns("dummy JwtTokenAudience");
            this.Configuration.SetupGet(mock => mock[Startup.EnvironmentVariable_JwtTokenIssuer])
                .Returns("dummy JwtTokenIssuer");

            //act
            this.Startup.ConfigureServices(new ServiceCollection());

            //assert
        }

        #endregion ConfigureServices tests

        #endregion Tests
    }
}
