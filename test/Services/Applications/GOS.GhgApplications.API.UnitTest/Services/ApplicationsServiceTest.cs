// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using AutoMapper;
using DfT.GOS.GhgApplications.API.Commands;
using DfT.GOS.GhgApplications.API.Data;
using DfT.GOS.GhgApplications.API.Repositories;
using DfT.GOS.GhgApplications.API.Services;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.Common.Exceptions;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.Models;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Messaging.Client;
using DfT.GOS.Messaging.Messages;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.SystemParameters.API.Client.Models;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using static DfT.GOS.GhgApplications.API.Data.ApplicationStatus;
using ApplicationStatus = DfT.GOS.GhgApplications.Common.Models.ApplicationStatus;
using ReviewStatus = DfT.GOS.GhgApplications.API.Data.ReviewStatus;

namespace DfT.GOS.GhgApplications.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests the applications service class
    /// </summary>
    [TestClass]
    public class ApplicationsServiceTest
    {
        #region Constants

        private const int FakeInitialReviewStatusId = 50;
        private const int FakeInitialApplicationStatusId = 51;
        private const decimal FakeEnergyDensity = 2000;
        private const decimal FakeCarbonIntensity = 3000;
        private const int FakeElectricityFuelTypeId = 1;
        private const int FakeElectricityFuelCategoryId = 1;

        private const int ValidObligationPeriodId = 1;
        private const int ValidSupplierOrganisationId = 2;
        private const int ValidFuelTypeId = 3;
        private const int ValidAmountSupplied = 500;

        private const int InvalidFuelTypeId = 1001;
        private const int InvalidObligationPeriodId = 1002;
        private Data.ApplicationStatus statusSubmitted = new Data.ApplicationStatus(3, "Submitted");
        private Data.ApplicationStatus statusApproved = new Data.ApplicationStatus(4, "Approved");
        private Data.ApplicationStatus statusIssued = new Data.ApplicationStatus(6, "Issued");
        private Data.ApplicationStatus statusRejected = new Data.ApplicationStatus(7, "Rejected");
        private Data.ApplicationStatus statusRecommended = new Data.ApplicationStatus(8, "Recommended");
        private Data.ApplicationStatus statusRevoked = new Data.ApplicationStatus(9, "Revoked");

        #endregion Constants

        #region Properties

        private Application LastApplicationPassedToRepository { get; set; }
        private ApplicationItem LastApplicationItemPassedToRepository { get; set; }

        private IApplicationsService _applicationsService;
        private Mock<IScanVirusService> _mockVirusScannerService;
        private Mock<IGhgFuelsService> _mockGhgFuelsService;
        private Mock<IReportingPeriodsService> _mockReportingPeriodsService;
        private Mock<ISystemParametersService> _mockSystemParametersService;
        private Mock<ISupportingDocumentParametersService> _mockDocumentParameterService;
        private Mock<IApplicationsRepository> _mockApplicationsRepository;
        private Mock<IApplicationItemsRepository> _mockApplicationItemsRepository;
        private Mock<IApplicationStatusRepository> _mockApplicationStatusRepository;
        private Mock<IReviewStatusRepository> _mockReviewStatusRepository;
        private Mock<ISupportingDocumentRepository> _mockSupportingDocumentRepository;
        private Mock<GosApplicationsDataContext> _mockDbContext;
        private Mock<ILogger<ApplicationsService>> _mockLogger;
        private Mock<DbSet<Application>> mockApplicationDbSet;
        private Mock<DbSet<Data.ApplicationStatus>> mockApplicationStatusDbSet;
        private Mock<DbSet<ApplicationItem>> mockApplicationItemDbSet;
        private Mock<DbSet<SupportingDocument>> mockSupportingDocumentDbSet;
        private Mock<DbSet<ReviewStatus>> mockReviewStatusDbSet;
        private Mock<IMessagingClient> MessagingClient;
        private IMapper _mapper;

        #endregion Properties

        /// <summary>
        /// Initialises the service with mock objects
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile<AutoMapperProfile>();
                config.CreateMap<ApplicationItem, ApplicationItemSummary>();
            });
            _mapper = Mapper.Configuration.CreateMapper();
            _mockVirusScannerService = new Mock<IScanVirusService>();
            _mockGhgFuelsService = new Mock<IGhgFuelsService>();
            _mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            _mockSystemParametersService = new Mock<ISystemParametersService>();
            _mockDocumentParameterService = new Mock<ISupportingDocumentParametersService>();
            _mockApplicationsRepository = new Mock<IApplicationsRepository>();
            _mockApplicationItemsRepository = new Mock<IApplicationItemsRepository>();
            _mockApplicationStatusRepository = new Mock<IApplicationStatusRepository>();
            _mockReviewStatusRepository = new Mock<IReviewStatusRepository>();
            _mockSupportingDocumentRepository = new Mock<ISupportingDocumentRepository>();
            _mockLogger = new Mock<ILogger<ApplicationsService>>();
            MessagingClient = new Mock<IMessagingClient>();
            _mockDbContext = new Mock<GosApplicationsDataContext>();
            mockApplicationDbSet = new Mock<DbSet<Application>>();
            mockApplicationStatusDbSet = new Mock<DbSet<Data.ApplicationStatus>>();
            mockApplicationItemDbSet = new Mock<DbSet<ApplicationItem>>();
            mockSupportingDocumentDbSet = new Mock<DbSet<SupportingDocument>>();
            mockReviewStatusDbSet = new Mock<DbSet<ReviewStatus>>();
            _mockDbContext.Setup(x => x.Applications).Returns(mockApplicationDbSet.Object);
            _mockDbContext.Setup(x => x.ApplicationStatus).Returns(mockApplicationStatusDbSet.Object);
            _mockDbContext.Setup(x => x.ApplicationItem).Returns(mockApplicationItemDbSet.Object);
            _mockDbContext.Setup(x => x.SupportingDocuments).Returns(mockSupportingDocumentDbSet.Object);
            _mockDbContext.Setup(x => x.ReviewStatus).Returns(mockReviewStatusDbSet.Object);
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(1
                , DateTime.Now.AddDays(1)
                , It.IsAny<DateTime>()
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , DateTime.Now.AddDays(50)
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>()
                )));


            _applicationsService = new ApplicationsService(_mapper
                , _mockDbContext.Object
                , _mockVirusScannerService.Object
                , _mockGhgFuelsService.Object
                , _mockReportingPeriodsService.Object
                , _mockApplicationsRepository.Object
                , _mockApplicationItemsRepository.Object
                , _mockApplicationStatusRepository.Object
                , _mockReviewStatusRepository.Object
                , _mockSupportingDocumentRepository.Object
                , _mockSystemParametersService.Object
                , _mockDocumentParameterService.Object
                , _mockLogger.Object
                , MessagingClient.Object
                , 2);
        }

        /// <summary>
        /// Tears down the tests, cleaning up Automapper etc
        /// </summary>
        [TestCleanup]
        public void Cleanup()
        {
            Mapper.Reset();
        }

        #region Tests

        #region Create Application Tests

        /// <summary>
        /// Tests that the Create Application method validates that a Fuel Type exists before persisting
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task ApplicationsService_CreateApplication_Validates_FuelType_ExistsAsync()
        {
            //arrange
            var command = new CreateApplicationCommand(ValidObligationPeriodId, ValidSupplierOrganisationId, ValidAmountSupplied, InvalidFuelTypeId, null, null);
            _mockGhgFuelsService.Setup(x => x.GetFuelType(It.IsAny<int>()))
                .Returns(Task.FromResult<FuelType>(null));

            //act
            var result = await _applicationsService.CreateApplication(command, It.IsAny<string>());
        }

        /// <summary>
        /// Tests that the Create Application method validates that an Obligation Period exists before persisting
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task ApplicationsService_CreateApplication_Validates_ObligationPeriod_ExistsAsync()
        {
            //arrange
            var command = new CreateApplicationCommand(InvalidObligationPeriodId, ValidSupplierOrganisationId
                , ValidAmountSupplied, ValidFuelTypeId, null, null);
            _mockGhgFuelsService.Setup(x => x.GetFuelType(ValidFuelTypeId))
               .Returns(Task.FromResult<FuelType>(new FuelType(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()
               , It.IsAny<string>(), It.IsAny<decimal?>(), It.IsAny<decimal?>())));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(InvalidObligationPeriodId))
                .Returns(Task.FromResult<ObligationPeriod>(null));

            //act
            var result = await _applicationsService.CreateApplication(command, It.IsAny<string>());

        }
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task ApplicationsService_CreateApplication_Creates_Application_ThrowsException()
        {

            //arrange
            var command = new CreateApplicationCommand(ValidObligationPeriodId, ValidSupplierOrganisationId
                , ValidAmountSupplied, ValidFuelTypeId, null, null);
            _mockGhgFuelsService.Setup(x => x.GetFuelType(ValidFuelTypeId))
               .Returns(Task.FromResult<FuelType>(new FuelType(ValidFuelTypeId, It.IsAny<int>(), It.IsAny<string>()
               , It.IsAny<string>(), 1212, 12)));
            _mockGhgFuelsService.Setup(x => x.GetElectricityFuelType())
                .Returns(Task.FromResult(new FuelType(FakeElectricityFuelTypeId, FakeElectricityFuelCategoryId
                , "Fake Electricity Fuel Category", "Fake Electricity Fuel Type", 1, 1)));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(ValidObligationPeriodId))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(1
                , DateTime.Now.AddDays(1)
                , It.IsAny<DateTime>()
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>()
                )));
            _mockReviewStatusRepository.Setup(x => x.GetInitialReviewStatus())
                .Returns(Task.FromResult(new ReviewStatus() { Name = "Approved", ReviewStatusId = 1 }));
            _mockApplicationStatusRepository.Setup(x => x.GetOpenApplicationStatus())
                .Returns(Task.FromResult(new Data.ApplicationStatus()));
            _mockDbContext.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>())).Returns(Task.FromException<int>(new Exception()));

            //act
            var result = await _applicationsService.CreateApplication(command, It.IsAny<string>());

        }
        /// <summary>
        /// Validates that the application and application items are correctly created when valid input is given
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_CreateApplication_Creates_Application_and_ApplicationItem()
        {
            //arrange
            var command = new CreateApplicationCommand(ValidObligationPeriodId, ValidSupplierOrganisationId, ValidAmountSupplied, ValidFuelTypeId, null, null);
            _mockGhgFuelsService.Setup(x => x.GetFuelType(ValidFuelTypeId))
               .Returns(Task.FromResult<FuelType>(new FuelType(ValidFuelTypeId, It.IsAny<int>(), It.IsAny<string>()
               , It.IsAny<string>(), FakeCarbonIntensity, FakeEnergyDensity)));
            _mockGhgFuelsService.Setup(x => x.GetElectricityFuelType())
                .Returns(Task.FromResult(new FuelType(FakeElectricityFuelTypeId, FakeElectricityFuelCategoryId
                , "Fake Electricity Fuel Category", "Fake Electricity Fuel Type", 1, 1)));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(ValidObligationPeriodId))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(1
                , DateTime.Now.AddDays(1)
                , It.IsAny<DateTime>()
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>()
                )));

            _mockReviewStatusRepository.Setup(x => x.GetInitialReviewStatus())
                .Returns(Task.FromResult(new ReviewStatus() { Name = "Approved", ReviewStatusId = FakeInitialReviewStatusId }));
            _mockApplicationStatusRepository.Setup(x => x.GetOpenApplicationStatus())
                .Returns(Task.FromResult(new Data.ApplicationStatus() { ApplicationStatusId = FakeInitialApplicationStatusId }));
            _mockDbContext.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>())).Returns(Task.FromResult<int>(It.IsAny<int>()));
            _mockApplicationsRepository.Setup(r => r.CreateAsync(It.IsAny<Application>()))
                .Returns(Task.CompletedTask)
                .Callback<Application>(a => LastApplicationPassedToRepository = a);
            _mockApplicationItemsRepository.Setup(r => r.CreateAsync(It.IsAny<ApplicationItem>()))
                .Returns(Task.CompletedTask)
                .Callback<ApplicationItem>(i => LastApplicationItemPassedToRepository = i);
            //act
            var result = await _applicationsService.CreateApplication(command, It.IsAny<string>());

            //assert
            // - Application Created
            var application = LastApplicationPassedToRepository;
            Assert.AreEqual(application.ObligationPeriodId, ValidObligationPeriodId);
            Assert.AreEqual(application.ApplicationStatus.ApplicationStatusId, FakeInitialApplicationStatusId);
            Assert.AreEqual(application.ApplicationVolumesReviewStatus.ReviewStatusId, FakeInitialReviewStatusId);
            Assert.IsNull(application.ApplicationVolumesRejectionReason);
            Assert.AreEqual(application.SupportingDocumentsReviewStatus.ReviewStatusId, FakeInitialReviewStatusId);
            Assert.IsNull(application.SupportingDocumentsRejectionReason);
            Assert.AreEqual(application.CalculationReviewStatus.ReviewStatusId, FakeInitialReviewStatusId);
            Assert.IsNull(application.CalculationRejectionReason);

            // - Application Item Created
            var applicationItem = LastApplicationItemPassedToRepository;
            Assert.AreEqual(applicationItem.Application, application);
            Assert.AreEqual(applicationItem.FuelTypeId, ValidFuelTypeId);
            Assert.AreEqual(applicationItem.AmountSupplied, ValidAmountSupplied);
            Assert.AreEqual(applicationItem.EnergyDensity, FakeEnergyDensity);
            Assert.AreEqual(applicationItem.GhgIntensity, FakeCarbonIntensity);
        }

        /// <summary>
        /// Validates that the the Application Item that is created has it's GHG Intensity overriden by the command (user input) for Electricty Fuel Category
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_CreateApplication_GhgIntensityOverridenForElectricityFuelCategory()
        {
            //arrange
            decimal fakeGhGIntensity = 123;
            var service = this.GetServiceInstance();
            var command = new CreateApplicationCommand(ValidObligationPeriodId, ValidSupplierOrganisationId, ValidAmountSupplied, FakeElectricityFuelTypeId, fakeGhGIntensity, null);

            //act
            var result = await service.CreateApplication(command, It.IsAny<string>());

            //assert         
            var applicationItem = LastApplicationItemPassedToRepository;
            Assert.AreEqual(applicationItem.GhgIntensity, fakeGhGIntensity);
        }


        /// <summary>
        /// Submit an application
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_CreateApplication()
        {
            //arrange
            decimal fakeGhGIntensity = 123;
            var service = this.GetServiceInstance();
            var command = new CreateApplicationCommand(ValidObligationPeriodId, ValidSupplierOrganisationId, ValidAmountSupplied, FakeElectricityFuelTypeId, fakeGhGIntensity, null);

            //act
            var result = await service.CreateApplication(command, It.IsAny<string>());

            //assert         
            var applicationItem = LastApplicationItemPassedToRepository;
            Assert.AreEqual(applicationItem.GhgIntensity, fakeGhGIntensity);

            Assert.IsTrue(true);
        }
        #endregion Create Application Tests

        #region Upload Supporting Documents Tests

        /// <summary>
        /// Tests that the Validate File Metadata method returns a success result
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_ValidateFileMetadata_SuccessResult()
        {
            //arrange
            var service = this.GetServiceInstance();
            var fileMetadata = new FileMetadata()
            {
                FileExtension = ".docx",
                FileName = "fakeFile.docx",
                FileSizeBytes = 2000
            };

            //act 
            var result = await service.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.AreEqual("Validated", result.UploadStatus);
            Assert.AreEqual(0, result.Messages.Count);
        }

        /// <summary>
        /// Tests that the Validate File Metadata method returns a "No file is attached" message for NULL File Meatadata
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_ValidateFileMetadata_Null_FileMetadata()
        {
            //arrange
            var service = this.GetServiceInstance();

            //act 
            var result = await service.ValidateFileMetadata(null);

            //assert
            Assert.AreEqual("No File is attached", result.UploadStatus);
            Assert.AreEqual(1, result.Messages.Count);
            Assert.AreEqual("No File is attached", result.Messages[0]);
        }

        /// <summary>
        /// Tests that the Validate File Metadata method returns a "No file is attached" message for null filename
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_ValidateFileMetadata_Null_Filename()
        {
            //arrange
            var service = this.GetServiceInstance();
            var fileMetadata = new FileMetadata()
            {
                FileExtension = ".docx",
                FileName = null,
                FileSizeBytes = 2000
            };

            //act 
            var result = await service.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.AreEqual("No File is attached", result.UploadStatus);
            Assert.AreEqual(1, result.Messages.Count);
            Assert.AreEqual("No File is attached", result.Messages[0]);
        }

        /// <summary>
        /// Tests that the Validate File Metadata method returns a "File extension is missing" message for null file extension
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_ValidateFileMetadata_Null_FileExtension()
        {
            //arrange
            var service = this.GetServiceInstance();
            var fileMetadata = new FileMetadata()
            {
                FileExtension = null,
                FileName = "fakeFile.docx",
                FileSizeBytes = 2000
            };

            //act 
            var result = await service.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.AreEqual("File extension is missing", result.UploadStatus);
            Assert.AreEqual(1, result.Messages.Count);
            Assert.AreEqual("File extension is missing", result.Messages[0]);
        }

        /// <summary>
        /// Tests that the Validate File Metadata method returns a "File extension is missing" message for file extension without period
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_ValidateFileMetadata_FileExtension_No_Period()
        {
            //arrange
            var service = this.GetServiceInstance();
            var fileMetadata = new FileMetadata()
            {
                FileExtension = "noPeriod",
                FileName = "fakeFile.docx",
                FileSizeBytes = 2000
            };

            //act 
            var result = await service.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.AreEqual("File extension is missing", result.UploadStatus);
            Assert.AreEqual(1, result.Messages.Count);
            Assert.AreEqual("File extension is missing", result.Messages[0]);
        }

        /// <summary>
        /// Tests that the Validate File Metadata method returns a "No file is attached" message for null filename
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_ValidateFileMetadata_Zero_FileSize()
        {
            //arrange
            var service = this.GetServiceInstance();
            var fileMetadata = new FileMetadata()
            {
                FileExtension = ".docx",
                FileName = "fakeFile.docx",
                FileSizeBytes = 0
            };

            //act 
            var result = await service.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.AreEqual("File is Empty", result.UploadStatus);
            Assert.AreEqual(1, result.Messages.Count);
            Assert.AreEqual("File fakeFile.docx is empty", result.Messages[0]);
        }

        /// <summary>
        /// Tests that the Validate File Metadata method returns a "File Type Not Supported" message for noon-supported file extension
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_ValidateFileMetadata_Non_Supported_File_Extension()
        {
            //arrange
            var service = this.GetServiceInstance();
            var fileMetadata = new FileMetadata()
            {
                FileExtension = ".no",
                FileName = "fakeFile.no",
                FileSizeBytes = 2000
            };

            //act 
            var result = await service.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.AreEqual("File Type Not Supported", result.UploadStatus);
            Assert.AreEqual(1, result.Messages.Count);
            Assert.AreEqual("The .no file type is not supported. Please use one of .docx", result.Messages[0]);
        }

        /// <summary>
        /// Tests that the Validate File Metadata method returns a "No file is attached" message for null filename
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_ValidateFileMetadata_FileSize_Exceeded_Maximum()
        {
            //arrange
            var service = this.GetServiceInstance();
            var fileMetadata = new FileMetadata()
            {
                FileExtension = ".docx",
                FileName = "bigFakeFile.docx",
                FileSizeBytes = 5000000
            };

            //act 
            var result = await service.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.AreEqual("MaxFilesize exceeded", result.UploadStatus);
            Assert.AreEqual(1, result.Messages.Count);
            Assert.AreEqual("File bigFakeFile.docx exceeds the maximum file size of 4.0 MB", result.Messages[0]);
        }

        #endregion Upload Supporting Documents Tests

        #region Application Item Tests

        [TestMethod]
        public async Task ApplicationsService_GetApplicationItems()
        {
            //arrange
            decimal fakeGhGIntensity = 123;
            int fuelTypeId = 2;
            _mockApplicationsRepository.Setup(r => r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application() { ApplicationId = 1 }));

            _mockApplicationItemsRepository.Setup(r => r.FindAll(It.IsAny<Expression<Func<ApplicationItem, bool>>>()))
                .Returns(Task.FromResult<IList<ApplicationItem>>(new List<ApplicationItem>() {
                    new ApplicationItem {
                        AmountSupplied = ValidAmountSupplied,
                        GhgIntensity = fakeGhGIntensity,
                        FuelTypeId = fuelTypeId
                    }
                }));
            //act
            var appItems = await _applicationsService.GetApplicationItems(1);

            //assert
            Assert.IsNotNull(appItems);
            _mockApplicationsRepository.Verify(x => x.GetAsync(It.IsAny<int>()), Times.Once);
            _mockApplicationItemsRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<ApplicationItem, bool>>>()), Times.Once);
            Assert.AreEqual(ValidAmountSupplied, appItems[0].AmountSupplied);
            Assert.AreEqual(fakeGhGIntensity, appItems[0].GhgIntensity);
            Assert.AreEqual(fuelTypeId, appItems[0].FuelTypeId);
        }



        #endregion Application Item Tests

        #region Pending application tests

        private List<Application> getApplications()
        {
            var baseDate = DateTime.Now;
            return new List<Application>
            {
                new Application
                {
                    ApplicationId = 1,
                    ApplicationStatusId = 3,
                    ApplicationStatus = statusSubmitted,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                },
                new Application
                {
                    ApplicationId = 2,
                    ApplicationStatusId = 6,
                    ApplicationStatus = statusIssued,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                    IssuedDate = baseDate.AddMonths(2).AddDays(3)
                },
                new Application
                {
                    ApplicationId = 3,
                    ApplicationStatusId = 7,
                    ApplicationStatus = statusRejected,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    RejectedDate = baseDate.AddMonths(1),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                },
                new Application
                {
                    ApplicationId = 4,
                    ApplicationStatusId = 9,
                    ApplicationStatus = statusApproved,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    RevokedDate = baseDate.AddMonths(1),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                    IssuedDate = baseDate.AddMonths(1).AddDays(-1)
                },
                new Application
                {
                    ApplicationId = 5,
                    ApplicationStatusId = 9,
                    ApplicationStatus = statusRevoked,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    RevokedDate = baseDate.AddMonths(3).AddDays(3),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                    IssuedDate = baseDate.AddMonths(3).AddDays(2)
                },
                new Application
                {
                    ApplicationId = 6,
                    ApplicationStatusId = 3,
                    ApplicationStatus = statusSubmitted,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  DateTime.Now.AddMonths(-2).AddMinutes(-15),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User"
                },
                new Application
                {
                    ApplicationId = 7,
                    ApplicationStatusId = 3,
                    ApplicationStatus = statusRecommended,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  DateTime.Now.AddMonths(-2).AddMinutes(15),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User"
                }
            };
        }

        /// <summary>
        /// Gets the pending applications and checks they are sorted by date descending by default
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetPendingApplicationPagedResult_Default_Sorts_By_Date_Descending()
        {
            //arrange
            var applications = getApplications();

            var options = new DbContextOptionsBuilder<GosApplicationsDataContext>()
                .UseInMemoryDatabase(databaseName: "Applications" + Guid.NewGuid())
                .Options;
            this.SetupApplications(options, applications);

            using (var context = new GosApplicationsDataContext(options))
            {
                this._applicationsService = new ApplicationsService(
                this._mapper
                , context
                , this._mockVirusScannerService.Object
                , this._mockGhgFuelsService.Object
                , this._mockReportingPeriodsService.Object
                , new ApplicationsRepository(context)
                , this._mockApplicationItemsRepository.Object
                , this._mockApplicationStatusRepository.Object
                , this._mockReviewStatusRepository.Object
                , this._mockSupportingDocumentRepository.Object
                , this._mockSystemParametersService.Object
                , this._mockDocumentParameterService.Object
                , this._mockLogger.Object
                , this.MessagingClient.Object
                , 2);

                //act
                var result = await _applicationsService.GetPendingApplicationPagedResult(new PendingApplicationParameters { PageSize = 5, Page = 1 });

                //assert
                var applicationResults = result.Result;
                Assert.AreEqual(4, applicationResults.Count);
                applicationResults.ToList().ForEach(a => Assert.IsTrue(a.ApplicationStatusId == statusApproved.ApplicationStatusId
                    || a.ApplicationStatusId == statusRecommended.ApplicationStatusId
                    || a.ApplicationStatusId == statusSubmitted.ApplicationStatusId));

                var firstDate = applicationResults.First().SubmittedDate;
                applicationResults.Remove(applicationResults.First());
                applicationResults.ToList().ForEach(a => { Assert.IsTrue(firstDate >= a.SubmittedDate); firstDate = a.SubmittedDate; });
            }
        }

        /// <summary>
        /// Gets the pending applications and checks they can be sorted by date ascending
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetPendingApplicationPagedResult_Can_Sort_By_Date_Ascending()
        {
            //arrange
            var applications = getApplications();

            var options = new DbContextOptionsBuilder<GosApplicationsDataContext>()
                .UseInMemoryDatabase(databaseName: "Applications" + Guid.NewGuid())
                .Options;
            this.SetupApplications(options, applications);

            using (var context = new GosApplicationsDataContext(options))
            {
                this._applicationsService = new ApplicationsService(
                this._mapper
                , context
                , this._mockVirusScannerService.Object
                , this._mockGhgFuelsService.Object
                , this._mockReportingPeriodsService.Object
                , new ApplicationsRepository(context)
                , this._mockApplicationItemsRepository.Object
                , this._mockApplicationStatusRepository.Object
                , this._mockReviewStatusRepository.Object
                , this._mockSupportingDocumentRepository.Object
                , this._mockSystemParametersService.Object
                , this._mockDocumentParameterService.Object
                , this._mockLogger.Object
                , this.MessagingClient.Object
                , 2);

                //act
                var result = await _applicationsService.GetPendingApplicationPagedResult(new PendingApplicationParameters { PageSize = 5, Page = 1, Order = SortOrder.Ascending });

                //assert
                var applicationResults = result.Result;
                Assert.AreEqual(4, applicationResults.Count);
                applicationResults.ToList().ForEach(a => Assert.IsTrue(a.ApplicationStatusId == statusApproved.ApplicationStatusId
                    || a.ApplicationStatusId == statusRecommended.ApplicationStatusId
                    || a.ApplicationStatusId == statusSubmitted.ApplicationStatusId));

                var firstDate = applicationResults.First().SubmittedDate;
                applicationResults.Remove(applicationResults.First());
                applicationResults.ToList().ForEach(a => { Assert.IsTrue(firstDate <= a.SubmittedDate); firstDate = a.SubmittedDate; });
            }
        }

        /// <summary>
        /// Test that applications can be filtered to only return those that match the provided application status filter
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetPendingApplicationPagedResult_Can_Filter_By_Status()
        {
            //arrange
            var applications = getApplications();

            var options = new DbContextOptionsBuilder<GosApplicationsDataContext>()
                .UseInMemoryDatabase(databaseName: "Applications" + Guid.NewGuid())
                .Options;
            this.SetupApplications(options, applications);

            using (var context = new GosApplicationsDataContext(options))
            {
                this._applicationsService = new ApplicationsService(
                this._mapper
                , context
                , this._mockVirusScannerService.Object
                , this._mockGhgFuelsService.Object
                , this._mockReportingPeriodsService.Object
                , new ApplicationsRepository(context)
                , this._mockApplicationItemsRepository.Object
                , this._mockApplicationStatusRepository.Object
                , this._mockReviewStatusRepository.Object
                , this._mockSupportingDocumentRepository.Object
                , this._mockSystemParametersService.Object
                , this._mockDocumentParameterService.Object
                , this._mockLogger.Object
                , this.MessagingClient.Object
                , 2);

                //act
                var resultSubmitted = await _applicationsService.GetPendingApplicationPagedResult(new PendingApplicationParameters { PageSize = 5, Page = 1, ApplicationStatus = ApplicationStatus.Submitted });
                var resultRecommended = await _applicationsService.GetPendingApplicationPagedResult(new PendingApplicationParameters { PageSize = 5, Page = 1, ApplicationStatus = ApplicationStatus.RecommendApproval });
                var resultApproved = await _applicationsService.GetPendingApplicationPagedResult(new PendingApplicationParameters { PageSize = 5, Page = 1, ApplicationStatus = ApplicationStatus.Approved });

                //assert
                //Submitted
                var submittedResults = resultSubmitted.Result;
                Assert.AreEqual(2, submittedResults.Count);
                submittedResults.ToList().ForEach(a => Assert.IsTrue(a.ApplicationStatusId == statusSubmitted.ApplicationStatusId));

                //Recommended
                var recommendedResults = resultRecommended.Result;
                Assert.AreEqual(1, recommendedResults.Count);
                recommendedResults.ToList().ForEach(a => Assert.IsTrue(a.ApplicationStatusId == statusRecommended.ApplicationStatusId));
                //Approved
                var approvedResults = resultApproved.Result;
                Assert.AreEqual(1, approvedResults.Count);
                approvedResults.ToList().ForEach(a => Assert.IsTrue(a.ApplicationStatusId == statusApproved.ApplicationStatusId));
            }
        }

        /// <summary>
        /// Gets the pending applications and makes sure applications with Submitted, RecommendApproval or approved status are added to result to be displayed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetPendingApplicationPagedResult_Returns_Approved_Submitted_Or_Recommended_Applications()
        {
            //arrange
            var applications = getApplications();

            var options = new DbContextOptionsBuilder<GosApplicationsDataContext>()
                .UseInMemoryDatabase(databaseName: "Applications" + Guid.NewGuid())
                .Options;
            this.SetupApplications(options, applications);

            using (var context = new GosApplicationsDataContext(options))
            {
                this._applicationsService = new ApplicationsService(
                this._mapper
                , context
                , this._mockVirusScannerService.Object
                , this._mockGhgFuelsService.Object
                , this._mockReportingPeriodsService.Object
                , new ApplicationsRepository(context)
                , this._mockApplicationItemsRepository.Object
                , this._mockApplicationStatusRepository.Object
                , this._mockReviewStatusRepository.Object
                , this._mockSupportingDocumentRepository.Object
                , this._mockSystemParametersService.Object
                , this._mockDocumentParameterService.Object
                , this._mockLogger.Object
                , this.MessagingClient.Object
                , 2);


                //act
                var result = await _applicationsService.GetPendingApplicationPagedResult(new PendingApplicationParameters { PageSize = 5, Page = 1 });

                //assert
                var applicationResults = result.Result;
                Assert.AreEqual(4, applicationResults.Count);
                applicationResults.ToList().ForEach(a => Assert.IsTrue(a.ApplicationStatusId == statusApproved.ApplicationStatusId
                    || a.ApplicationStatusId == statusRecommended.ApplicationStatusId
                    || a.ApplicationStatusId == statusSubmitted.ApplicationStatusId));
            }

        }

        #endregion

        #region GetApplicationItemsWithIssuedCredits
        /// <summary>
        /// Gets the Application items that have had GHG credits issued
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetApplicationItemsWithIssuedCredits()
        {
            //arrange
            _mockApplicationItemsRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<ApplicationItem, bool>>>()))
                .Returns(Task.FromResult<IList<ApplicationItem>>(new List<ApplicationItem>() { new ApplicationItem() }));
            //act
            var result = await this._applicationsService.GetApplicationItemsWithIssuedCredits(It.IsAny<int>(), It.IsAny<int>());

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<ApplicationItemSummary>));
            _mockApplicationItemsRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<ApplicationItem, bool>>>()), Times.Once);
        }
        #endregion GetApplicationItemsWithIssuedCredits

        #region UploadSupportingDocument
        /// <summary>
        /// Tests UploadSupportingDocument uploads the file.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_UploadSupportingDocument()
        {
            //arrange
            this._mockSupportingDocumentRepository.Setup(x => x.CreateAsync(It.IsAny<SupportingDocument>()))
                .Returns(Task.CompletedTask);
            this._mockVirusScannerService.Setup(x => x.ScanBytes(It.IsAny<byte[]>()))
                .Returns(Task.FromResult(new ScanResult() { IsVirusFree = true }));
            var command = new AddSupportingDocumentCommand
            {
                File = new System.Text.ASCIIEncoding().GetBytes("test"),
                FileExtension = "docx",
                FileName = "doc1.docx"
            };
            //act
            var result = await this._applicationsService.UploadSupportingDocument(command, 1, 1, "user");

            //assert
            Assert.IsNotNull(result);
        }
        #endregion UploadSupportingDocument

        #region GetUploadedSupportingDocuments
        /// <summary>
        /// Tests GetUploadedSupportingDocuments returns List of UploadedSupportingDocument
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetUploadedSupportingDocuments()
        {
            //arrange
            this._mockSupportingDocumentRepository.Setup(x => x.FindAll(It.IsAny<int>()))
                .Returns(Task.FromResult(new List<SupportingDocument>()
                { new SupportingDocument() { ApplicationId = 1, SupportingDocumentId = 1 }
                , new SupportingDocument() { ApplicationId = 1, SupportingDocumentId = 2 }}));

            //act
            var result = await this._applicationsService.GetUploadedSupportingDocuments(1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<UploadedSupportingDocument>));
            Assert.AreEqual(2, result.Count);
        }
        /// <summary>
        /// Tests GetUploadedSupportingDocuments returns a empty collection when exception is thrown
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetUploadedSupportingDocuments_ThrowsException_ReturnsEmptyCollection()
        {
            //arrange
            this._mockSupportingDocumentRepository.Setup(x => x.FindAll(It.IsAny<int>()))
                .Returns(Task.FromException<List<SupportingDocument>>(new Exception()));

            //act
            var result = await this._applicationsService.GetUploadedSupportingDocuments(1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<UploadedSupportingDocument>));
            Assert.AreEqual(0, result.Count);
        }

        /// <summary>
        /// Tests GetUploadedSupportingDocument returns DownloadSupportingDocument
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetUploadedSupportingDocument()
        {
            //arrange
            this._mockSupportingDocumentRepository.Setup(x => x.GetFile(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(new SupportingDocument() { ApplicationId = 1, SupportingDocumentId = 1 }));

            //act
            var result = await this._applicationsService.GetUploadedSupportingDocument(1, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(DownloadSupportingDocument));
        }

        /// <summary>
        /// Tests GetUploadedSupportingDocument returns DownloadSupportingDocument
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetUploadedSupportingDocument_ThrowsException()
        {
            //arrange
            this._mockSupportingDocumentRepository.Setup(x => x.GetFile(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromException<SupportingDocument>(new Exception()));

            //act
            var result = await this._applicationsService.GetUploadedSupportingDocument(1, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(DownloadSupportingDocument));
            Assert.IsNull(result.File);
            Assert.IsNull(result.FileName);
        }
        #endregion GetUploadedSupportingDocuments

        #region RemoveSupportingDocument
        /// <summary>
        /// Test RemoveSupportingDocument returns false on exception
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_RemoveSupportingDocument_ThrowsException()
        {
            //arrange
            this._mockSupportingDocumentRepository.Setup(x => x.DeleteAsync(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromException<bool>(new Exception()));

            //act
            var result = await this._applicationsService.RemoveSupportingDocument(1, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result);
        }

        /// <summary>
        /// Test RemoveSupportingDocument returns true
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_RemoveSupportingDocument()
        {
            //arrange
            this._mockSupportingDocumentRepository.Setup(x => x.DeleteAsync(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<bool>(true));

            //act
            var result = await this._applicationsService.RemoveSupportingDocument(1, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result);
        }
        #endregion RemoveSupportingDocument

        #region SubmitApplication
        /// <summary>
        /// Test RemoveSupportingDocument returns empty string when no application is found
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_SubmitApplication_With_No_Application()
        {
            //arrange
            this._mockApplicationsRepository.Setup(x => x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Application>(null));
            //act
            var result = await this._applicationsService.SubmitApplication(1, "user");
            //assert
            Assert.AreEqual(string.Empty, result);
        }

        /// <summary>
        /// Test RemoveSupportingDocument returns error message when application status is submitted
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_SubmitApplication_With_Submitted_Status()
        {
            //arrange
            this._mockApplicationsRepository.Setup(x => x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application
                { ApplicationId = 1, ApplicationStatusId = (int)ApplicationStatus.Submitted }));
            //act
            var result = await this._applicationsService.SubmitApplication(1, "user");
            //assert
            Assert.AreEqual("Application has already been submitted.", result);
        }

        /// <summary>
        /// Test RemoveSupportingDocument returns error message when application status is approved
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_SubmitApplication_With_Approved_Status()
        {
            //arrange
            this._mockApplicationsRepository.Setup(x => x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application
                { ApplicationId = 1, ApplicationStatusId = (int)ApplicationStatus.Approved }));
            //act
            var result = await this._applicationsService.SubmitApplication(1, "user");
            //assert
            Assert.AreEqual("Invalid application status. Application was not submitted.", result);
        }

        /// <summary>
        /// Test RemoveSupportingDocument returns error message when an exception is thrown
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_SubmitApplication_ThrowsException()
        {
            //arrange
            this._mockApplicationsRepository.Setup(x => x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromException<Application>(new Exception()));
            //act
            var result = await this._applicationsService.SubmitApplication(1, "user");
            //assert
            Assert.AreEqual("Application Submission Error. Application was not submitted.", result);
        }

        /// <summary>
        /// Test RemoveSupportingDocument returns error message when an exception is thrown
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_SubmitApplication_UpdateAsync_ThrowsException()
        {
            //arrange
            this._mockApplicationsRepository.Setup(x => x.GetAsync(It.IsAny<int>()))
                 .Returns(Task.FromResult(new Application
                 { ApplicationId = 1, ApplicationStatusId = (int)ApplicationStatus.Open }));
            this._mockApplicationsRepository.Setup(x => x.UpdateAsync(It.IsAny<Application>()))
                 .Returns(Task.FromException(new Exception()));
            //act
            var result = await this._applicationsService.SubmitApplication(1, "user");
            //assert
            Assert.AreEqual("Saving Application Submission Error. Application was not submitted.", result);
        }

        /// <summary>
        /// Test RemoveSupportingDocument returns error message when an exception is thrown
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_SubmitApplication_SaveChangesAsync_ThrowsException()
        {
            //arrange
            this._mockApplicationsRepository.Setup(x => x.GetAsync(It.IsAny<int>()))
                 .Returns(Task.FromResult(new Application
                 { ApplicationId = 1, ApplicationStatusId = (int)ApplicationStatus.Open }));
            this._mockApplicationsRepository.Setup(x => x.UpdateAsync(It.IsAny<Application>()));
            this._mockDbContext.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>()))
                 .Returns(Task.FromException<int>(new Exception()));
            //act
            var result = await this._applicationsService.SubmitApplication(1, "user");
            //assert
            Assert.AreEqual("Saving Application Submission Error. Application was not submitted.", result);
        }

        /// <summary>
        /// Test RemoveSupportingDocument returns empty string on successful submissions
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_SubmitApplication_Sucessfully_Submits_Application()
        {
            //arrange
            var application = new Application
            { ApplicationId = 1, ApplicationStatusId = (int)ApplicationStatus.Open };
            this._mockApplicationsRepository.Setup(x => x.GetAsync(It.IsAny<int>()))
                 .Returns(Task.FromResult(application));
            this._mockApplicationsRepository.Setup(x => x.UpdateAsync(application)).Returns(Task.CompletedTask);
            this._mockDbContext.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>()))
                 .Returns(Task.FromResult<int>(1));
            //act
            var result = await this._applicationsService.SubmitApplication(1, "user");
            //assert
            Assert.AreEqual(string.Empty, result);
        }
        #endregion SubmitApplication

        #region ReviewApplication Tests
        /// <summary>
        /// Test ReviewApplication returns true for the first level approval
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_ReviewApplication_First_level()
        {
            //arrange
            this._mockApplicationsRepository.Setup(x => x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application
                {
                    ApplicationId = 1,
                    ApplicationStatus = new Data.ApplicationStatus(3, "Submitted")
                ,
                    ApplicationStatusId = (int)ApplicationStatus.Submitted
                }));
            this._mockApplicationStatusRepository.Setup(x => x.Get(It.IsAny<int>()))
                .Returns(Task.FromResult(new Data.ApplicationStatus(3, "Submitted")));
            this._mockDbContext.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>()))
                 .Returns(Task.FromResult<int>(1));

            //act
            var result = await this._applicationsService.ReviewApplication(new ReviewApplicationCommand(1, 3));

            //assert
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Test ReviewApplication returns true for the second level approval
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_ReviewApplication_Second_level()
        {
            //arrange
            this._mockApplicationsRepository.Setup(x => x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application
                {
                    ApplicationId = 1,
                    ApplicationStatus = new Data.ApplicationStatus(8, "RecommendApproval")
                ,
                    ApplicationStatusId = (int)ApplicationStatus.RecommendApproval
                }));
            this._mockApplicationStatusRepository.Setup(x => x.Get(It.IsAny<int>()))
                .Returns(Task.FromResult(new Data.ApplicationStatus(8, "RecommendApproval")));
            this._mockDbContext.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>()))
                 .Returns(Task.FromResult<int>(1));

            //act
            var result = await this._applicationsService.ReviewApplication(new ReviewApplicationCommand(1, 8));

            //assert
            Assert.IsTrue(result);
        }
        #endregion ReviewApplication Tests

        #region IssueCredits
        /// <summary>
        /// Test IssueCredits returns true 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_IssueCredits()
        {
            //arrange
            this._mockApplicationStatusRepository.Setup(x => x.Get(It.IsAny<int>()))
                .Returns(Task.FromResult(new Data.ApplicationStatus(8, "RecommendApproval")));
            this._mockApplicationsRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Application, bool>>>()))
                .Returns(Task.FromResult<IList<Application>>(new List<Application>()
                { new Application {
                    ApplicationId = 1, ApplicationStatus = new Data.ApplicationStatus(4, "Approved"), ObligationPeriodId = 15
                    , SupplierOrganisationId = 12
                },
                  new Application {
                      ApplicationId = 2, ApplicationStatus = new Data.ApplicationStatus(4, "Approved"), ObligationPeriodId = 15
                      , SupplierOrganisationId = 12
                  } }));
            this._mockDbContext.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>()))
                 .Returns(Task.FromResult<int>(1));

            //act
            var result = await this._applicationsService.IssueCredits(new IssueCreditCommand() { });

            //assert
            Assert.IsTrue(result);
            this.MessagingClient.Verify(mock => mock.Publish(It.IsAny<CreditsIssuedMessage>()), Times.Exactly(2));
            Assert.AreEqual(2, this.MessagingClient.Invocations.Count);
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.Count == 1));
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.First() != null));
            Assert.IsTrue(this.MessagingClient.Invocations.All(inv => inv.Arguments.First().GetType() == typeof(CreditsIssuedMessage)));
            Assert.IsTrue(this.MessagingClient.Invocations.Any(inv => inv.Method.Name == "Publish"
                && inv.Arguments.Count == 1
                && (inv.Arguments.First() as CreditsIssuedMessage).ObligationPeriodId == 15
                && (inv.Arguments.First() as CreditsIssuedMessage).OrganisationId == 12));
        }
        #endregion IssueCredits

        #region Applications ready for issue tests

        /// <summary>
        /// Gets the applications ready for issue
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationsService_GetApplicationsReadyForIssuePagedResult()
        {
            //arrange
            var pagedResult = new PagedResult<Application>();
            _mockApplicationsRepository.Setup(x => x.GetApplicationsPagedResult(
                It.IsAny<Expression<Func<Application, bool>>>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<SortOrder>()))
                .Returns(Task.FromResult(pagedResult));
            //act
            var result = await _applicationsService.GetApplicationsReadyForIssuePagedResult(5, 2);

            //assert
            _mockApplicationsRepository.Verify(m => m.GetApplicationsPagedResult(It.IsAny<Expression<Func<Application, bool>>>(), 5, 2, SortOrder.Decending), Times.Once);
        }

        #endregion Applications ready for issue tests

        #region Get Application Review Summary tests

        [TestMethod]
        public async Task ApplicationsService_GetApplicationReviewSummary_Handles_Invalid_ApplicationId()
        {
            //arrange
            var invalidApplicationId = 1001;
            _mockApplicationsRepository.Setup(mock => mock.GetAsync(invalidApplicationId))
                .ReturnsAsync((Application)null);

            //act
            var result = await _applicationsService.GetApplicationReviewSummary(invalidApplicationId);

            //assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public async Task ApplicationsService_GetApplicationReviewSummary()
        {
            //arrange
            var applicationId = 101;
            var application = new Application()
            {
                ApplicationId = applicationId,
                ApplicationItems = new List<ApplicationItem>() { new ApplicationItem() { AmountSupplied = 3000, ApplicationItemId = 1, CreatedBy = "testuser", CreatedDate = DateTime.Now, EnergyDensity = 1 } },
                ApplicationStatus = new Data.ApplicationStatus() { ApplicationStatusId = (int)ApplicationStatuses.Submitted, Name = "Submitted" },
                ApplicationStatusId = (int)ApplicationStatuses.Submitted,
                ApplicationVolumesRejectionReason = "",
                ApplicationVolumesReviewStatus = new Data.ReviewStatus() { ReviewStatusId = 1, Name = "Not Reviewed" },
                ApprovedBy = null,
                ApprovedDate = null,
                CalculationRejectionReason = null,
                CalculationReviewStatus = new Data.ReviewStatus() { ReviewStatusId = 1, Name = "Not Reviewed" },
                CreatedBy = "testuser",
                CreatedDate = DateTime.Now,
                IssuedBy = null,
                IssuedDate = null,
                ObligationPeriodId = 13,
                RecommendedBy = null,
                RecommendedDate = null,
                SubmittedBy = "testuser",
                SubmittedDate = DateTime.Now,
                SupplierOrganisationId = ValidSupplierOrganisationId,
                SupportingDocumentsRejectionReason = null,
                SupportingDocumentsReviewStatus = new Data.ReviewStatus() { ReviewStatusId = 1, Name = "Not Reviewed" }
            };
            _mockApplicationsRepository.Setup(mock => mock.GetAsync(applicationId))
                .ReturnsAsync(application);

            //act
            var result = await _applicationsService.GetApplicationReviewSummary(applicationId);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(application.ApplicationId, result.ApplicationId);
            Assert.AreEqual(ApplicationStatus.Submitted, result.ApplicationStatus);
            Assert.AreEqual(application.ApplicationStatusId, result.ApplicationStatusId);
            Assert.AreEqual(application.ApplicationVolumesRejectionReason, result.ApplicationVolumesRejectionReason);
            Assert.AreEqual(application.ApplicationVolumesReviewStatus.Name, result.ApplicationVolumesReviewStatus.Name);
            Assert.AreEqual(application.ApplicationVolumesReviewStatus.ReviewStatusId, result.ApplicationVolumesReviewStatus.ReviewStatusId);
            Assert.AreEqual(application.ApprovedBy, result.ApprovedBy);
            Assert.AreEqual(application.ApprovedDate, result.ApprovedDate);
            Assert.AreEqual(application.CalculationRejectionReason, result.CalculationRejectionReason);
            Assert.AreEqual(application.CalculationReviewStatus.Name, result.CalculationReviewStatus.Name);
            Assert.AreEqual(application.CalculationReviewStatus.ReviewStatusId, result.CalculationReviewStatus.ReviewStatusId);
            Assert.AreEqual(application.ObligationPeriodId, result.ObligationPeriodId);
            Assert.AreEqual(application.RecommendedBy, result.RecommendedBy);
            //Assert.AreEqual(application.RecommendedDate, result.RecommendedDate);
            Assert.AreEqual(application.SubmittedBy, result.SubmittedBy);
            Assert.AreEqual(application.SubmittedDate, result.SubmittedDate);
            Assert.AreEqual(application.SupplierOrganisationId, result.SupplierOrganisationId);
            Assert.AreEqual(application.SupportingDocumentsRejectionReason, result.SupportingDocumentsRejectionReason);
            Assert.AreEqual(application.SupportingDocumentsReviewStatus.Name, result.SupportingDocumentsReviewStatus.Name);
            Assert.AreEqual(application.SupportingDocumentsReviewStatus.ReviewStatusId, result.SupportingDocumentsReviewStatus.ReviewStatusId);
        }

        #endregion Get Application Review Summary tests

        #region Review Application Volumes tests

        /// <summary>
        /// Tests the Review Application Volumes method 
        /// </summary>
        [TestMethod]
        public void ApplicationsService_ReviewApplicationVolumes()
        {
            //arrange
            var command = new ReviewApplicationVolumesCommand(1, 1, null);

            _mockApplicationsRepository.Setup(r => r.GetAsync(It.IsAny<int>()))
               .Returns(Task.FromResult(new Application() { ApplicationId = 1 }));

            _mockReviewStatusRepository.Setup(r => r.GetInitialReviewStatus()).Returns(
                Task.FromResult(new Data.ReviewStatus(FakeInitialReviewStatusId, "Fake initial Review Status")));

            //act
            var result = _applicationsService.ReviewApplicationVolumes(command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(bool));
        }

        #endregion Review Application Volumes tests

        #region Review Application Supporting Documents tests

        /// <summary>
        /// Tests the Review Application Supporting Documents method 
        /// </summary>
        [TestMethod]
        public void ApplicationsService_ReviewApplicationSupportingDocuments()
        {
            //arrange
            var command = new ReviewApplicationSupportingDocumentsCommand(1, 1, null);

            _mockApplicationsRepository.Setup(r => r.GetAsync(It.IsAny<int>()))
               .Returns(Task.FromResult(new Application() { ApplicationId = 1 }));

            _mockReviewStatusRepository.Setup(r => r.GetInitialReviewStatus()).Returns(
                Task.FromResult(new Data.ReviewStatus(FakeInitialReviewStatusId, "Fake initial Review Status")));

            //act
            var result = _applicationsService.ReviewApplicationSupportingDocuments(command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(bool));
        }

        #endregion Review Application Volumes tests

        #region Review Application Calculation tests

        /// <summary>
        /// Tests the Review Application Calculation method 
        /// </summary>
        [TestMethod]
        public void ApplicationsService_ReviewApplicationCalculation()
        {
            //arrange
            var command = new ReviewApplicationCalculationCommand(1, 1, null);

            _mockApplicationsRepository.Setup(r => r.GetAsync(It.IsAny<int>()))
               .Returns(Task.FromResult(new Application() { ApplicationId = 1 }));

            _mockReviewStatusRepository.Setup(r => r.GetInitialReviewStatus()).Returns(
                Task.FromResult(new Data.ReviewStatus(FakeInitialReviewStatusId, "Fake initial Review Status")));

            //act
            var result = _applicationsService.ReviewApplicationCalculation(command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(bool));
        }

        #endregion Review Application Calculation tests

        #region Approve Application tests

        /// <summary>
        /// Tests the Approve Application method 
        /// </summary>
        [TestMethod]
        public void ApplicationsService_ApproveApplication()
        {
            //arrange
            var command = new ApproveApplicationCommand(1);

            _mockApplicationsRepository.Setup(r => r.GetAsync(It.IsAny<int>()))
               .Returns(Task.FromResult(new Application() { ApplicationId = 1 }));

            _mockReviewStatusRepository.Setup(r => r.Get((int)ApplicationStatuses.Approved)).Returns(
                Task.FromResult(new Data.ReviewStatus((int)ApplicationStatuses.Approved, "Fake approved Status")));

            //act
            var result = _applicationsService.ApproveApplication(command, It.IsAny<string>()).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(bool));
        }

        #endregion Approve Application tests

        #region Reject Application tests

        /// <summary>
        /// Tests the Reject Application method 
        /// </summary>
        [TestMethod]
        public void ApplicationsService_RejectApplication()
        {
            //arrange
            var command = new RejectApplicationCommand(1);

            _mockApplicationsRepository.Setup(r => r.GetAsync(It.IsAny<int>()))
               .Returns(Task.FromResult(new Application() { ApplicationId = 1 }));

            _mockReviewStatusRepository.Setup(r => r.Get((int)ApplicationStatuses.Approved)).Returns(
                Task.FromResult(new Data.ReviewStatus((int)ApplicationStatuses.Rejected, "Fake rejected Status")));

            //act
            var result = _applicationsService.RejectApplication(command, It.IsAny<string>()).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(bool));
        }

        #endregion Approve Application tests

        #region Supplier Application
        /// <summary>
        /// Test the application repo method gets called when GetSupplierApplications is called
        /// </summary>
        [TestMethod]
        public async Task ApplicationsService_GetSupplierApplications()
        {
            //arrange
            var pagedResult = new PagedResult<Application>();
            _mockApplicationsRepository.Setup(x => x.GetApplicationsPagedResult(
                It.IsAny<Expression<Func<Application, bool>>>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<SortOrder>()))
                .Returns(Task.FromResult(pagedResult));

            //act
            await _applicationsService.GetSupplierApplications(10, 120, 5, 2);

            //assert
            _mockApplicationsRepository.Verify(m => m.GetApplicationsPagedResult(
            It.IsAny<Expression<Func<Application, bool>>>(), 5, 2, SortOrder.Decending), Times.Once);
        }
        #endregion Supplier Application

        #region Revoke Application tests

        ///<summary>
        /// Revoke returns a bad request response when the application state is not Credits Issued
        ///</summary>
        [TestMethod]
        public async Task ApplicationsService_Revoke_Application_Fails_When_Application_Not_In_Credits_Issued_State()
        {
            // Arrange 
            var revokedBy = "user1";
            var command = new RevokeApplicationCommand(1, "ABC");
            _mockApplicationsRepository.Setup(a => a.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application
                {
                    ApplicationId = 1,
                    ApplicationStatusId = (int)ApplicationStatuses.Open
                }));


            // Act and Assert 
            await Assert.ThrowsExceptionAsync<InvalidApplicationRevokeException>(() => this._applicationsService.RevokeApplication(command, revokedBy));
        }

        ///<summary>
        /// Revoke returns a Not Found response when the application can't be found
        ///</summary>
        [TestMethod]
        public async Task ApplicationsService_Revoke_Application_Fails_When_Application_Can_Not_Be_Found()
        {
            // Arrange 
            var revokedBy = "user1";
            var command = new RevokeApplicationCommand(1, "ABC");
            _mockApplicationsRepository.Setup(a => a.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Application>(null));

            // Act and Assert 
            await Assert.ThrowsExceptionAsync<ApplicationNotFoundException>(() => this._applicationsService.RevokeApplication(command, revokedBy));

        }

        ///<summary>
        /// Revoke application adds the revocation reason, date, and user to the application as well as changing its status.
        ///</summary>
        [TestMethod]
        public async Task ApplicationsService_Revoke_Applications_Adds_Revoke_Fields_To_Application()
        {
            // Arrange 
            var revokedBy = "user1";
            var revocationReason = "No reason";
            var command = new RevokeApplicationCommand(1, revocationReason);
            _mockApplicationsRepository.Setup(a => a.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application
                {
                    ApplicationId = 1,
                    ApplicationStatusId = (int)ApplicationStatuses.GHGCreditsIssued
                }));

            // Act
            await this._applicationsService.RevokeApplication(command, revokedBy);

            // Assert
            _mockApplicationsRepository.Verify(r => r.UpdateAsync(
                It.Is<Application>(a =>
                    a.RevokedBy.Equals(revokedBy) && a.RevokedDate != null && a.RevocationReason == revocationReason && a.ApplicationStatusId == (int)ApplicationStatuses.Revoked
                )), Times.Once);
        }

        ///<summary>
        /// Check an application can't be revoked after the last possible revocation date
        ///</summary>
        [TestMethod]
        public async Task ApplicationsService_Revoke_Application_Fails_If_Date_Is_Past_Last_Revocation_Date()
        {
            // Arrange 
            var revokedBy = "user1";
            var command = new RevokeApplicationCommand(1, "ABC");
            _mockApplicationsRepository.Setup(a => a.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application
                {
                    ApplicationId = 1,
                    ApplicationStatusId = (int)ApplicationStatuses.GHGCreditsIssued
                }));
            _mockReportingPeriodsService.Setup(rp => rp.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(15, null, null, false, null, 123, 123, null, DateTime.Now.AddDays(-1), null, null, null, null)));

            // Act and Assert 
            await Assert.ThrowsExceptionAsync<InvalidApplicationRevokeException>(() => this._applicationsService.RevokeApplication(command, revokedBy));
        }

        ///<summary>
        /// Successful revocation returns a success response
        ///</summary>
        [TestMethod]
        public async Task ApplicationsService_Revoke_Application_Returns_Success_When_It_Is_Successful()
        {
            // Arrange 
            var command = new RevokeApplicationCommand(1, null);
            _mockApplicationsRepository.Setup(a => a.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application
                {
                    ApplicationId = 1,
                    ApplicationStatusId = (int)ApplicationStatuses.GHGCreditsIssued
                }));

            // Act
            var result = await this._applicationsService.RevokeApplication(command, "12345");

            // Assert 
            Assert.IsTrue(result);
        }

        ///<summary>
        /// Revoke sends a message to the messaging service containing the application Id, obligation period Id and organisation Id
        ///</summary>
        [TestMethod]
        public async Task ApplicationsService_Revoke_Application_Sends_A_Message_To_The_Messaging_service()
        {
            // Arrange 
            var applicationId = 1;
            var obligationPeriodId = 15;
            var organisationId = 1;
            var command = new RevokeApplicationCommand(1, "ABC");
            _mockApplicationsRepository.Setup(a => a.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application
                {
                    ApplicationId = applicationId,
                    ObligationPeriodId = obligationPeriodId,
                    SupplierOrganisationId = organisationId,
                    ApplicationStatusId = (int)ApplicationStatuses.GHGCreditsIssued
                }));
            _mockReportingPeriodsService.Setup(r => r.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult(new ObligationPeriod(obligationPeriodId, null, null, true, null, 0, 0, null, DateTime.Now.AddDays(50), null, null, null, null)));


            // Act
            await this._applicationsService.RevokeApplication(command, "12345");

            // Assert
            this.MessagingClient.Verify(c => c.Publish(It.Is<ApplicationRevokedMessage>(m =>
                m.ApplicationId == applicationId && m.ObligationPeriodId == obligationPeriodId && m.OrganisationId == organisationId
            )), Times.Once);
        }
        #endregion Revoke Application tests

        #region Performance Data tests

        ///<summary>
        /// Tests the expected values are returned for the number of applications metrics
        ///</summary>
        [TestMethod]
        public async Task GetPerformanceData_Returns_Correct_Values_For_The_Applications_Metrics()
        {
            // Arrange 
            var baseDate = new DateTime(2019, 01, 05);
            var expectedApplicationsMonthThree = 4;
            var expectedApplicationsMonthFour = 3;
            var expectedApplicationsCurrentMonth = 2;
            var expectedNoOfApplicationsWithCreditsIssuedMonthThree = 1;
            var expectedNoOfApplicationsWithCreditsIssuedMonthFour = 2;
            var expectedNoOfApplicationsWithCreditsIssuedCurrentMonth = 0;
            var expectedNoOfIncompleteApplicationsThree = 0;
            var expectedNoOfIncompleteApplicationsMonthFour = 0;
            var expectedNoOfIncompleteApplicationsCurrentMonth = 0;
            var expectedNoOfRejectedApplicationsMonthThree = 0;
            var expectedNoOfRejectedApplicationsMonthFour = 0;
            var expectedNoOfRejectedApplicationsMonthCurrentMonth = 0;
            var expectedNoOfRevokedApplicationsMonthThree = 0;
            var expectedNoOfRevokedApplicationsMonthFour = 1;
            var expectedNoOfRevokedApplicationsMonthCurrentMonth = 0;
            var expectedNoOfSubmittedApplicationsMonthThree = 0;
            var expectedNoOfSubmittedApplicationsMonthFour = 0;
            var expectedNoOfSubmittedApplicationsMonthCurrentMonth = 0;
            var expectedTotalNoOfApplicationsMonthThree = 0;
            var expectedTotalNoOfApplicationsMonthFour = 0;
            var expectedTotalNoOfApplicationsCurrentMonth = 0;

            var options = new DbContextOptionsBuilder<GosApplicationsDataContext>()
                .UseInMemoryDatabase(databaseName: "Applications" + Guid.NewGuid())
                .Options;
            this.SetupApplicationsPerformanceData(options, baseDate);

            using (var context = new GosApplicationsDataContext(options))
            {
                this._applicationsService = new ApplicationsService(
                this._mapper
                , context
                , this._mockVirusScannerService.Object
                , this._mockGhgFuelsService.Object
                , this._mockReportingPeriodsService.Object
                , new ApplicationsRepository(context)
                , this._mockApplicationItemsRepository.Object
                , this._mockApplicationStatusRepository.Object
                , this._mockReviewStatusRepository.Object
                , this._mockSupportingDocumentRepository.Object
                , this._mockSystemParametersService.Object
                , this._mockDocumentParameterService.Object
                , this._mockLogger.Object
                , this.MessagingClient.Object
                , 2);


                // Act
                var result = await this._applicationsService.GetApplicationPerformanceData();

                // Assert 
                Assert.AreEqual(expectedApplicationsMonthThree, result[2].NoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue);
                Assert.AreEqual(expectedApplicationsMonthFour, result[3].NoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue);
                Assert.AreEqual(expectedApplicationsCurrentMonth, result.Last().NoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue);
                Assert.AreEqual(expectedNoOfApplicationsWithCreditsIssuedMonthThree, result[2].NoOfApplicationsWithCreditsIssued);
                Assert.AreEqual(expectedNoOfApplicationsWithCreditsIssuedMonthFour, result[3].NoOfApplicationsWithCreditsIssued);
                Assert.AreEqual(expectedNoOfApplicationsWithCreditsIssuedCurrentMonth, result.Last().NoOfApplicationsWithCreditsIssued);
                Assert.AreEqual(expectedNoOfIncompleteApplicationsThree, result[2].NoOfIncompleteApplications);
                Assert.AreEqual(expectedNoOfIncompleteApplicationsMonthFour, result[3].NoOfIncompleteApplications);
                Assert.AreEqual(expectedNoOfIncompleteApplicationsCurrentMonth, result.Last().NoOfIncompleteApplications);
                Assert.AreEqual(expectedNoOfRejectedApplicationsMonthThree, result[2].NoOfRejectedApplications);
                Assert.AreEqual(expectedNoOfRejectedApplicationsMonthFour, result[3].NoOfRejectedApplications);
                Assert.AreEqual(expectedNoOfRejectedApplicationsMonthCurrentMonth, result.Last().NoOfRejectedApplications);
                Assert.AreEqual(expectedNoOfRevokedApplicationsMonthThree, result[2].NoOfRevokedApplications);
                Assert.AreEqual(expectedNoOfRevokedApplicationsMonthFour, result[3].NoOfRevokedApplications);
                Assert.AreEqual(expectedNoOfRevokedApplicationsMonthCurrentMonth, result.Last().NoOfRevokedApplications);
                Assert.AreEqual(expectedNoOfSubmittedApplicationsMonthThree, result[2].NoOfSubmittedApplications);
                Assert.AreEqual(expectedNoOfSubmittedApplicationsMonthFour, result[3].NoOfSubmittedApplications);
                Assert.AreEqual(expectedNoOfSubmittedApplicationsMonthCurrentMonth, result.Last().NoOfSubmittedApplications);
                Assert.AreEqual(expectedTotalNoOfApplicationsMonthThree, result[2].TotalNoOfApplications);
                Assert.AreEqual(expectedTotalNoOfApplicationsMonthFour, result[3].TotalNoOfApplications);
                Assert.AreEqual(expectedTotalNoOfApplicationsCurrentMonth, result.Last().TotalNoOfApplications);
            }

        }

        ///<summary>
        /// Test that the data is correct in the same month as currently but last year. (i.e. if it's june now the data will also be correct in june last year)
        ///</summary>
        [TestMethod]
        public async Task GetPerformanceData_Produces_Correct_Data_In_Matching_Month_Last_Year()
        {
            // Arrange 
            var baseDate = new DateTime(2015, 04, 05);
            var expectedApplicationsAYearAgo = 2;
            var expectedApplicationsThisMonth = 1;
            var options = new DbContextOptionsBuilder<GosApplicationsDataContext>()
                .UseInMemoryDatabase(databaseName: "Applications_" + Guid.NewGuid())
                .Options;
            this.SetupApplicationsPerformanceData(options, baseDate);

            using (var context = new GosApplicationsDataContext(options))
            {
                this._applicationsService = new ApplicationsService(
                this._mapper
                , context
                , this._mockVirusScannerService.Object
                , this._mockGhgFuelsService.Object
                , this._mockReportingPeriodsService.Object
                , new ApplicationsRepository(context)
                , this._mockApplicationItemsRepository.Object
                , this._mockApplicationStatusRepository.Object
                , this._mockReviewStatusRepository.Object
                , this._mockSupportingDocumentRepository.Object
                , this._mockSystemParametersService.Object
                , this._mockDocumentParameterService.Object
                , this._mockLogger.Object
                , this.MessagingClient.Object
                , 2);

                //Add two applications last year
                context.Applications.Add(
                    new Application
                    {
                        ApplicationId = 20,
                        ObligationPeriodId = 15,
                        SupplierOrganisationId = 1,
                        SubmittedDate = baseDate,
                        CreatedDate = DateTime.Now.AddYears(-1),
                        CreatedBy = "A User",
                    }
                );
                context.Applications.Add(
                    new Application
                    {
                        ApplicationId = 21,
                        ObligationPeriodId = 15,
                        SupplierOrganisationId = 1,
                        SubmittedDate = baseDate,
                        CreatedDate = DateTime.Now.AddYears(-1).AddSeconds(10),
                        CreatedBy = "A User",
                    }
                );
                // Add one this year
                context.Applications.Add(
                    new Application
                    {
                        ApplicationId = 22,
                        ObligationPeriodId = 15,
                        SupplierOrganisationId = 1,
                        SubmittedDate = baseDate,
                        CreatedDate = DateTime.Now.AddMinutes(-15),
                        CreatedBy = "A User",
                    }
                );
                context.SaveChanges();

                // Act
                var result = await this._applicationsService.GetApplicationPerformanceData();

                // Assert
                var thisMonthInPreviousYear = result.FindLast(r => r.Date.Month == DateTime.Now.Month && r.Date.Year == DateTime.Now.Year - 1);
                Assert.AreEqual(expectedApplicationsAYearAgo, thisMonthInPreviousYear.TotalNoOfApplications);
                Assert.AreEqual(expectedApplicationsThisMonth, result.Last().TotalNoOfApplications);
            }
        }

        #endregion Performance Data tests

        #endregion Tests

        #region Private Methods

        /// <summary>
        /// Gets an instance of the applications service class for testing
        /// </summary>
        /// <returns></returns>
        private IApplicationsService GetServiceInstance()
        {
            //Setup the Fuel Service Values
            var fakeElectricityFuelType = new FuelType(FakeElectricityFuelTypeId, FakeElectricityFuelCategoryId, "Fake Electricity Fuel Category", "Fake Electricity Fuel Type", 1, 1);
            var mockGhgFuelsService = new Mock<IGhgFuelsService>();
            mockGhgFuelsService.Setup(s => s.GetFuelType(InvalidFuelTypeId)).Returns(Task.FromResult<FuelType>(null));
            mockGhgFuelsService.Setup(s => s.GetFuelType(ValidFuelTypeId)).Returns(Task.FromResult<FuelType>(new FuelType(2, 2, "Fake Fuel Category", "Fake Fuel Type", FakeCarbonIntensity, FakeEnergyDensity)));
            mockGhgFuelsService.Setup(s => s.GetFuelType(FakeElectricityFuelTypeId)).Returns(Task.FromResult<FuelType>(fakeElectricityFuelType));
            mockGhgFuelsService.Setup(s => s.GetElectricityFuelType()).Returns(Task.FromResult<FuelType>(fakeElectricityFuelType));

            //Setup Reporting Periods Service Values
            var mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            mockReportingPeriodsService.Setup(s => s.GetObligationPeriod(InvalidObligationPeriodId)).Returns(
                Task.FromResult<ObligationPeriod>(null));

            mockReportingPeriodsService.Setup(s => s.GetObligationPeriod(ValidObligationPeriodId)).Returns(
                Task.FromResult(new ObligationPeriod(1
                , DateTime.Now.AddDays(1)
                , It.IsAny<DateTime>()
                , It.IsAny<bool>()
                , It.IsAny<DateTime>()
                , It.IsAny<int>()
                , It.IsAny<int>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<DateTime>()
                , It.IsAny<decimal>()
                , It.IsAny<int>()
                , It.IsAny<decimal>()
                )));

            //Setup Review Status Repository methods
            var mockReviewStatusRepository = new Mock<IReviewStatusRepository>();
            mockReviewStatusRepository.Setup(r => r.GetInitialReviewStatus()).Returns(
                Task.FromResult(new Data.ReviewStatus(FakeInitialReviewStatusId, "Fake initial Review Status")));

            //Setup Application Status Repository methods
            var mockApplicationStatusRepository = new Mock<IApplicationStatusRepository>();
            mockApplicationStatusRepository.Setup(r => r.GetOpenApplicationStatus()).Returns(
                Task.FromResult(new Data.ApplicationStatus(FakeInitialApplicationStatusId, "Fake initial Application Status")));

            //Setup Applications Repository
            var mockApplicationsRepository = new Mock<IApplicationsRepository>();
            mockApplicationsRepository.Setup(r => r.CreateAsync(It.IsAny<Application>()))
                .Returns(Task.CompletedTask)
                .Callback<Application>(a => LastApplicationPassedToRepository = a);
            mockApplicationsRepository.Setup(r => r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Application() { ApplicationId = 1, ApplicationStatusId = FakeInitialApplicationStatusId }));

            //Setup Application Items Repository
            var mockApplicationItemsRepository = new Mock<IApplicationItemsRepository>();
            mockApplicationItemsRepository.Setup(r => r.CreateAsync(It.IsAny<ApplicationItem>()))
                .Returns(Task.CompletedTask)
                .Callback<ApplicationItem>(i => LastApplicationItemPassedToRepository = i);

            var mockVirusScannerService = new Mock<IScanVirusService>();

            //Setup System Parameter Service
            var mockSystemParametersService = new Mock<ISystemParametersService>();

            IList<DocumentType> supportedDocumentTypes = new List<DocumentType>();
            supportedDocumentTypes.Add(new DocumentType(1, "fakeContext", "docx", "123"));
            mockSystemParametersService.Setup(s => s.GetDocumentTypes()).Returns(Task.FromResult(new HttpObjectResponse<IList<DocumentType>>(supportedDocumentTypes)));

            // Supporting Document parameter service
            var mockSupportingDocumentParameterService = new Mock<ISupportingDocumentParametersService>();
            mockSupportingDocumentParameterService.Setup(s => s.GetUploadedSupportingDocumentMaxSizeBytes()).Returns(4194304); //4MB

            var service = new ApplicationsService(_mapper
                , _mockDbContext.Object
                , mockVirusScannerService.Object
                , mockGhgFuelsService.Object
                , mockReportingPeriodsService.Object
                , mockApplicationsRepository.Object
                , mockApplicationItemsRepository.Object
                , mockApplicationStatusRepository.Object
                , mockReviewStatusRepository.Object
                , _mockSupportingDocumentRepository.Object
                , mockSystemParametersService.Object
                , mockSupportingDocumentParameterService.Object
                , _mockLogger.Object, null
                , 2);

            return service;
        }

        private void SetupApplications(DbContextOptions<GosApplicationsDataContext> options, List<Application> applications)
        {
            using (var context = new GosApplicationsDataContext(options))
            {
                applications.ForEach(a => context.Applications.Add(a));
                context.SaveChanges();
            }
        }

        private void SetupApplicationsPerformanceData(DbContextOptions<GosApplicationsDataContext> options, DateTime baseDate)
        {
            var applications = new List<Application>
            {
                // Application submitted month 1, credits not issued
                new Application
                {
                    ApplicationId = 1,
                    ApplicationStatusId = 3,
                    ApplicationStatus = statusSubmitted,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                },
                // Application submitted month 1, credits issued after threshold
                new Application
                {
                    ApplicationId = 2,
                    ApplicationStatusId = 6,
                    ApplicationStatus = statusIssued,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                    IssuedDate = baseDate.AddMonths(2).AddDays(3)
                },
                // Application submitted month 1, rejected one month later (inside issue threshold)
                new Application
                {
                    ApplicationId = 3,
                    ApplicationStatusId = 7,
                    ApplicationStatus = statusRejected,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    RejectedDate = baseDate.AddMonths(1),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                },
                // Application submitted month 1, credits issued in threshold, then revoked later
                new Application
                {
                    ApplicationId = 4,
                    ApplicationStatusId = 9,
                    ApplicationStatus = statusRevoked,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    RevokedDate = baseDate.AddMonths(1),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                    IssuedDate = baseDate.AddMonths(1).AddDays(-1)
                },
                // Application submitted month 1, credits issued late, and subsequently revoked
                new Application
                {
                    ApplicationId = 5,
                    ApplicationStatusId = 9,
                    ApplicationStatus = statusRevoked,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    RevokedDate = baseDate.AddMonths(3).AddDays(3),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                    IssuedDate = baseDate.AddMonths(3).AddDays(2)
                },
                // Application submitted just over two months ago credits not issued, so is late
                new Application
                {
                    ApplicationId = 6,
                    ApplicationStatusId = 3,
                    ApplicationStatus = statusSubmitted,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  DateTime.Now.AddMonths(-2).AddMinutes(-15),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User"
                },
                // Application submitted just under two months ago credits not issued, so is OK
                new Application
                {
                    ApplicationId = 7,
                    ApplicationStatusId = 3,
                    ApplicationStatus = statusSubmitted,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  DateTime.Now.AddMonths(-2).AddMinutes(15),
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User"
                },
                // Application submitted month 1, credits issued after threshold
                new Application
                {
                    ApplicationId = 9,
                    ApplicationStatusId = 6,
                    ApplicationStatus = statusIssued,
                    ObligationPeriodId = 15,
                    SupplierOrganisationId = 1,
                    SubmittedDate =  baseDate,
                    CreatedDate = baseDate.AddMinutes(-5),
                    CreatedBy = "A User",
                    IssuedDate = baseDate.AddMonths(3)
                }
            };

            SetupApplications(options, applications);
        }

        #endregion Private Methods
    }
}
