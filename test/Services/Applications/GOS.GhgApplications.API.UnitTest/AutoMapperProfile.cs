﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using AutoMapper;

namespace DfT.GOS.GhgApplications.API.UnitTest
{
    /// <summary>
    /// Profile for automapper that is used by tests
    /// </summary>
    public class AutoMapperProfile : Profile
    {

    }
}
