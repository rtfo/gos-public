﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DfT.GOS.GhgApplications.API.UnitTest.Helpers
{
    /// <summary>
    /// Provides unit tests for the FileHelper class
    /// </summary>
    [TestClass]
    public class FileHelperTest
    {
        /// <summary>
        /// Validates that the GetMaxFileSizeDisplay correctly handles bytes, kb and mb when passed a number of bytes
        /// </summary>
        [TestMethod]
        public void ApplicationService_GetMaxFileSizeDisplay_Handles_bytes_kb_and_mb()
        {
            //act
            var zeroResult = FileHelper.GetMaxFileSizeDisplay(0);
            var bytesResult = FileHelper.GetMaxFileSizeDisplay(125);
            var kbResult = FileHelper.GetMaxFileSizeDisplay(1601);
            var mbResult = FileHelper.GetMaxFileSizeDisplay(2726298);
            var fourMbResult = FileHelper.GetMaxFileSizeDisplay(4194304);

            //assert
            Assert.AreEqual(zeroResult, "0 bytes");
            Assert.AreEqual(bytesResult, "125 bytes");
            Assert.AreEqual(kbResult, "1.6 KB");
            Assert.AreEqual(mbResult, "2.6 MB");
            Assert.AreEqual(fourMbResult, "4.0 MB");
        }
    }
}
