﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Commands;
using DfT.GOS.GhgApplications.API.Models;
using DfT.GOS.GhgApplications.API.Services;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.Common.Exceptions;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.Models;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.Web.Models;
using GOS.GhgApplications.API.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests for the Application Controller
    /// </summary>
    [TestClass]
    public class ApplicationControllerTest
    {
        #region Constants

        private const int ValidSupplierOrganisationId = 101;
        private const int InvalidSupplierOrganisationId = 102;
        private const int NonSupplierOrganisationId = 202;

        private const int ValidApplicationId = 20;
        private const int InvalidApplicationId = 50;
        private const int ApprovedApplicationId = 60;
        private const int SubmittedApplicationId = 70;

        private const int ValidSupportingDocumentId = 10;
        private const int InvalidSupportingDocumentId = 20;

        #endregion Constants

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ApplicationControllerTest()
        {
        }
        #endregion Constructors

        #region Private Members
        private ApplicationsController _controller;
        private Mock<IApplicationsService> _mockApplicationService;
        private Mock<IScanVirusService> _mockScanVirusService;
        private Mock<ISystemParametersService> _mockSystemParametersService;
        private Mock<ISupportingDocumentParametersService> _mockSupportingDocumentParametersService;
        private Mock<IOrganisationsService> _mockOrganisationsService;
        private Mock<IGhgFuelsService> _mockGhgFuelsService;
        private Mock<IReportingPeriodsService> _mockReportingPeriodsService;
        private readonly int userOrganisationId = 13;
        #endregion Private Members

        #region Initialize
        [TestInitialize]
        public void Initialize()
        {
            _mockApplicationService = new Mock<IApplicationsService>();
            _mockScanVirusService = new Mock<IScanVirusService>();
            _mockSystemParametersService = new Mock<ISystemParametersService>();
            _mockSupportingDocumentParametersService = new Mock<ISupportingDocumentParametersService>();
            _mockOrganisationsService = new Mock<IOrganisationsService>();
            _mockGhgFuelsService = new Mock<IGhgFuelsService>();
            _mockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            _controller = GetControllerInstance(userOrganisationId, _mockApplicationService, _mockScanVirusService, _mockSystemParametersService, _mockSupportingDocumentParametersService, _mockOrganisationsService, _mockGhgFuelsService, _mockReportingPeriodsService);
        }

        #endregion Initialize

        #region Tests

        #region Create Application Tests

        /// <summary>
        /// Tests that the create (POST) method returns a created result on success
        /// </summary>
        [TestMethod]
        public void Create_Returns_Created_Result_On_Success()
        {
            //arrange
            _mockGhgFuelsService.Setup(x => x.GetFuelType(It.IsAny<int>()))
                .Returns(Task.FromResult<FuelType>(new FuelType(It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<decimal>(),
                It.IsAny<decimal>())));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpObjectResponse<Organisation>>(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
                It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<string>(), 1,
                It.IsAny<string>()))));
            var command = new CreateApplicationCommand(1, 13, 10, 1, 50, null);

            //act
            var result = _controller.Create(command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(CreatedResult));
        }

        /// <summary>
        /// Tests that the create (POST) method returns a bad request result if validation fails success
        /// </summary>
        [TestMethod]
        public void Create_Returns_Created_Result_On_Invalid_Model()
        {
            //arrange
            _mockGhgFuelsService.Setup(x => x.GetFuelType(It.IsAny<int>()))
                .Returns(Task.FromResult<FuelType>(new FuelType(It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<decimal>(),
                It.IsAny<decimal>())));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpObjectResponse<Organisation>>(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
                It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<string>(), 1,
                It.IsAny<string>()))));
            _controller.ModelState.AddModelError("FakeKey", "Fake Error Message");
            var command = new CreateApplicationCommand(1, 13, 10, 1, 50, null);

            //act
            var result = _controller.Create(command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the create (POST) method forbids a user creating an application for an organisation they don't
        /// has access to
        /// </summary>
        [TestMethod]
        public void Create_Return_Forbid_User_Without_Permission()
        {
            //arrange
            _mockGhgFuelsService.Setup(x => x.GetFuelType(It.IsAny<int>()))
                .Returns(Task.FromResult<FuelType>(new FuelType(It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<decimal>(),
                It.IsAny<decimal>())));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpObjectResponse<Organisation>>(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
                It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<string>()))));
            var command = new CreateApplicationCommand(1, InvalidSupplierOrganisationId, 10, 1, 50, null);

            //act
            var result = _controller.Create(command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the create (POST) doesn't allow a user to create an application for a non-supplier (e.g. verifier or trader organisation)
        /// </summary>
        [TestMethod]
        public void Create_Return_Forbids_Creation_For_Non_Supplier_Organisation()
        {
            //arrange
            _mockGhgFuelsService.Setup(x => x.GetFuelType(It.IsAny<int>()))
                .Returns(Task.FromResult<FuelType>(new FuelType(It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<decimal>(),
                It.IsAny<decimal>())));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpObjectResponse<Organisation>>(new HttpObjectResponse<Organisation>(
                    new Organisation(It.IsAny<int>(),
                It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<string>(), 2,
                It.IsAny<string>()))));
            var command = new CreateApplicationCommand(1, 13, 10, 1, 50, null);

            //act
            var result = _controller.Create(command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that create doesn't allow invalid fuel type
        /// </summary>
        [TestMethod]
        public async Task Create_Return_BadRequest_For_Invalid_Fuel_Type()
        {
            //arrange
            _mockGhgFuelsService.Setup(x => x.GetFuelType(It.IsAny<int>()))
                .Returns(Task.FromResult<FuelType>(null));
            var command = new CreateApplicationCommand(1, 101, 10, 131, 50, null);

            //act
            var result = await _controller.Create(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that create doesn't allow invalid organisation
        /// </summary>
        [TestMethod]
        public async Task Create_Return_BadRequest_For_Invalid_Organisation_Id()
        {
            //arrange
            _mockGhgFuelsService.Setup(x => x.GetFuelType(It.IsAny<int>()))
                .Returns(Task.FromResult<FuelType>(new FuelType(It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<decimal>(),
                It.IsAny<decimal>())));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(null));
            var command = new CreateApplicationCommand(1, 222, 10, 1, 50, null);

            //act
            var result = await _controller.Create(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that create doesn't allow invalid obligation period
        /// </summary>
        [TestMethod]
        public async Task Create_Return_BadRequest_For_Invalid_ObligationPeriod_Id()
        {
            //arrange
            _mockGhgFuelsService.Setup(x => x.GetFuelType(It.IsAny<int>()))
                .Returns(Task.FromResult<FuelType>(new FuelType(It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<decimal>(),
                It.IsAny<decimal>())));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(null));
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<HttpObjectResponse<Organisation>>(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(),
                It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<string>()))));
            var command = new CreateApplicationCommand(6694, 13, 10, 1, 50, null);

            //act
            var result = await _controller.Create(command);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        #endregion Create Application Tests

        #region Get Application Summary Tests

        /// <summary>
        /// Tests that the get application summary method returns an application summary
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetApplicationSummary_Returns_Summary()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = controller.GetApplicationSummary(ValidApplicationId).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(ApplicationSummary));
        }

        /// <summary>
        /// Tests that the get application summary method returns a 404 if no application is found
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetApplicationSummary_Returns_404_Null_Application()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = controller.GetApplicationSummary(InvalidApplicationId).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the get application summary method returns a Forbid if the application doesn't belong to the current user's organisation
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetApplicationSummary_Returns_Forbid_Incorrect_Organisation()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, InvalidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = controller.GetApplicationSummary(ValidApplicationId).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        #endregion Get Application Summary Tests

        #region Get Application Items Tests

        /// <summary>
        /// Tests that the get application items method returns application items
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetApplicationItems_Returns_Items()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = controller.GetApplicationItems(ValidApplicationId).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IList<ApplicationItemSummary>));
        }

        /// <summary>
        /// Tests that the get application items method returns a 404 if no application is found
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetApplicationItems_Returns_404_Null_Application()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = controller.GetApplicationItems(InvalidApplicationId).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the get application items method returns a Forbid if the application doesn't belong to the current user's organisation
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetApplicationItems_Returns_Forbid_Incorrect_Organisation()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, InvalidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = controller.GetApplicationItems(ValidApplicationId).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        #endregion Get Application Items Tests

        #region Upload Supporting Document Tests

        /// <summary>
        /// Test for empty file
        /// </summary>
        [TestMethod]
        public void ApplicationController_UploadFile_EmptyFile()
        {
            //arrange
            var supportingDocument = new AddSupportingDocumentCommand()
            {
                File = Encoding.ASCII.GetBytes(""),
                FileExtension = "docx",
                FileName = "test.docx"
            };
            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(new ApplicationSummary(ValidApplicationId, 13, 13) { ApplicationStatusId = 1 }));
            _mockApplicationService.Setup(s => s.ValidateFileMetadata(It.IsAny<FileMetadata>()))
               .ReturnsAsync(() =>
               {
                   FileValidationResult filevalidationResult = new FileValidationResult();
                   filevalidationResult.Messages.Add("Error");
                   return filevalidationResult;
               });
            int applicationId = 1;
            string createdBy = string.Empty;

            //act
            var response = _controller.SupportingDocuments(supportingDocument, applicationId);

            //assert
            Assert.IsInstanceOfType(response.Result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Test that errors in Model State from parameter binding are handled correctly
        /// </summary>
        [TestMethod]
        public void ApplicationController_UploadFile_ModelStateError()
        {
            //arrange
            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(ValidApplicationId, 13, 13) { ApplicationStatusId = 1 }));
            _mockApplicationService.Setup(s => s.ValidateFileMetadata(It.IsAny<FileMetadata>()))
               .ReturnsAsync(() =>
               {
                   FileValidationResult filevalidationResult = new FileValidationResult();
                   return filevalidationResult;
               });
            _mockApplicationService.Setup(s => s.UploadSupportingDocument(It.IsAny<AddSupportingDocumentCommand>(),
                It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<string>()))
               .ReturnsAsync(() =>
               {
                   FileValidationResult filevalidationResult = new FileValidationResult();
                   ScanResult scanResult = new ScanResult
                   {
                       Message = "",
                       IsVirusFree = true
                   };
                   filevalidationResult.ScanResult = scanResult;

                   return filevalidationResult;
               });
            var supportingDocument = new AddSupportingDocumentCommand()
            {
                File = Encoding.ASCII.GetBytes("Test data in file"),
                FileExtension = "docx",
                FileName = "test.docx"
            };
            int applicationId = 1;
            string createdBy = string.Empty;
            var controller = GetControllerInstance("Success", true, ValidSupplierOrganisationId, applicationId);
            controller.ModelState.AddModelError("supportingDocument", "test error");

            //act
            var response = controller.SupportingDocuments(supportingDocument, applicationId);

            //assert
            Assert.IsInstanceOfType(response.Result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Test not empty file size
        /// </summary>
        [TestMethod]
        public void ApplicationController_UploadFile_NonEmptyFile()
        {
            //arrange
            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(ValidApplicationId, 13, 13) { ApplicationStatusId = 1 }));
            _mockApplicationService.Setup(s => s.ValidateFileMetadata(It.IsAny<FileMetadata>()))
               .ReturnsAsync(() =>
               {
                   FileValidationResult filevalidationResult = new FileValidationResult();
                   return filevalidationResult;
               });
            _mockApplicationService.Setup(s => s.UploadSupportingDocument(It.IsAny<AddSupportingDocumentCommand>(),
                It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<string>()))
               .ReturnsAsync(() =>
               {
                   FileValidationResult filevalidationResult = new FileValidationResult();
                   ScanResult scanResult = new ScanResult
                   {
                       Message = "",
                       IsVirusFree = true
                   };
                   filevalidationResult.ScanResult = scanResult;

                   return filevalidationResult;
               });
            var supportingDocument = new AddSupportingDocumentCommand()
            {
                File = Encoding.ASCII.GetBytes("Test data in file"),
                FileExtension = "docx",
                FileName = "test.docx"
            };
            int applicationId = 1;
            string createdBy = string.Empty;

            //act
            var response = _controller.SupportingDocuments(supportingDocument, applicationId);

            //assert
            Assert.IsInstanceOfType(response.Result, typeof(OkObjectResult));
        }

        /// <summary>
        /// Test unsupported file extension
        /// </summary>
        [TestMethod]
        public void ApplicationController_UploadFile_NonEmptyFile_With_Unsupported_Extension()
        {
            //arrange
            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
              .Returns(Task.FromResult(new ApplicationSummary(ValidApplicationId, 13, 13) { ApplicationStatusId = 1 }));
            _mockApplicationService.Setup(s => s.ValidateFileMetadata(It.IsAny<FileMetadata>()))
               .ReturnsAsync(() =>
               {
                   FileValidationResult filevalidationResult = new FileValidationResult();
                   filevalidationResult.Messages.Add("Error");
                   return filevalidationResult;
               });
            var supportingDocument = new AddSupportingDocumentCommand()
            {
                File = Encoding.ASCII.GetBytes("Test data in file"),
                FileExtension = "ini",
                FileName = "test.ini"
            };
            int applicationId = 1;
            string createdBy = string.Empty;

            //act
            var response = _controller.SupportingDocuments(supportingDocument, applicationId);

            //assert
            Assert.IsInstanceOfType(response.Result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Test uploading against an application that does not have status Open
        /// </summary>
        [TestMethod]
        public void ApplicationController_UploadFile_NonEmptyFile_With_Approved_Application()
        {
            //arrange
            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
              .ReturnsAsync(new ApplicationSummary(ValidApplicationId, 13, 13) { ApplicationStatusId = (int)ApplicationStatus.Approved });
            _mockApplicationService.Setup(s => s.ValidateFileMetadata(It.IsAny<FileMetadata>()))
               .ReturnsAsync(new FileValidationResult());
            var supportingDocument = new AddSupportingDocumentCommand()
            {
                File = Encoding.ASCII.GetBytes("Test data in file"),
                FileExtension = ".doc",
                FileName = "test.doc"
            };
            int applicationId = 1;
            string createdBy = string.Empty;

            //act
            var response = _controller.SupportingDocuments(supportingDocument, applicationId);

            //assert
            Assert.IsInstanceOfType(response.Result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Test uploading against an supplier to which the user does not have access
        /// </summary>
        [TestMethod]
        public void ApplicationController_UploadFile_NonEmptyFile_Against_Invalid_Supplier()
        {
            //arrange
            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
              .ReturnsAsync(new ApplicationSummary(ValidApplicationId, 13, 13) { ApplicationStatusId = (int)ApplicationStatus.Open });
            _mockApplicationService.Setup(s => s.ValidateFileMetadata(It.IsAny<FileMetadata>()))
               .ReturnsAsync(new FileValidationResult());
            var supportingDocument = (AddSupportingDocumentCommand)null;
            int applicationId = 1;
            string createdBy = string.Empty;
            var controller = GetControllerInstance("Success", true, InvalidSupplierOrganisationId, applicationId);

            //act
            var response = controller.SupportingDocuments(supportingDocument, ValidApplicationId);

            //assert
            Assert.IsInstanceOfType(response.Result, typeof(ForbidResult));
        }

        #endregion Upload Supporting Documents tests

        #region Supporting Documents tests

        /// <summary>
        /// Tests that the supporting documents method returns supporting documents
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SupportingDocuments_Returns_Documents()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.SupportingDocuments(ValidApplicationId);

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(List<UploadedSupportingDocument>));
        }

        /// <summary>
        /// Tests that the supporting documents method returns a 404 if no application is found
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SupportingDocuments_Returns_404_Null_Application()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.SupportingDocuments(InvalidApplicationId);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the supporting documents method returns a Forbid if the application doesn't belong to the current user's organisation
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SupportingDocuments_Returns_Forbid_Incorrect_Organisation()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, InvalidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.SupportingDocuments(ValidApplicationId);

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        #endregion Supporting Documents tests

        #region Supporting Document tests

        /// <summary>
        /// Tests that the supporting document method returns BadReqeust if the ModelState is invalid
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SupportingDocument_Returns_BadRequest_For_Invalid_ModelState()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = ValidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, InvalidSupplierOrganisationId, fakeApplicationId);
            controller.ModelState.AddModelError("applicationId", "test error");

            //act
            var result = await controller.SupportingDocument(ValidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the supporting document method returns NotFound if the application doesn't exist
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SupportingDocument_Returns_NotFound_Invalid_ApplicationId()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = ValidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.SupportingDocument(InvalidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the supporting document method returns a Forbid if the application doesn't belong to the current user's organisation
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SupportingDocument_Returns_Forbid_Incorrect_Organisation()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = ValidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, InvalidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.SupportingDocument(ValidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the supporting document method returns a OK with a null contents if the document doesn't exist
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SupportingDocument_Returns_OkResult_Nonexistant_Document()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = InvalidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.SupportingDocument(ValidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNull(((OkObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the supporting document method returns a OK with the required document if it exists
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SupportingDocument_Returns_OkResult_Existing_Document()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = ValidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.SupportingDocument(ValidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(DownloadSupportingDocument));
        }

        #endregion Supporting Document tests       

        #region Remove Supporting Document tests

        /// <summary>
        /// Tests that the remove supporting document method returns BadReqeust if the ModelState is invalid
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_RemoveSupportingDocument_Returns_BadRequest_For_Invalid_ModelState()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = ValidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, InvalidSupplierOrganisationId, fakeApplicationId);
            controller.ModelState.AddModelError("applicationId", "test error");

            //act
            var result = await controller.RemoveSupportingDocument(ValidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the remove supporting document method returns NotFound if the application doesn't exist
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_RemoveSupportingDocument_Returns_NotFound_Invalid_ApplicationId()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = ValidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.RemoveSupportingDocument(InvalidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the remove supporting document method returns a Forbid if the application doesn't belong to the current user's organisation
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_RemoveSupportingDocument_Returns_Forbid_Incorrect_Organisation()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = ValidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, InvalidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.RemoveSupportingDocument(ValidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the remove supporting document method returns BadRequest if the application status is not Open
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_RemoveSupportingDocument_Returns_BadRequest_Application_Status_Not_Open()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = ValidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.RemoveSupportingDocument(ApprovedApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the remove supporting document method returns NotFound if the file for the given Supporting Document is null
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_RemoveSupportingDocument_Returns_NotFound_If_SupportingDocument_Not_Found()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = InvalidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.RemoveSupportingDocument(ValidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the remove supporting document method returns OK if the document is removed
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_RemoveSupportingDocument_Returns_OkResult_On_Success()
        {
            //arrange
            var fakeApplicationId = 1;
            var supportingDocumentId = ValidSupportingDocumentId;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);

            //act
            var result = await controller.RemoveSupportingDocument(ValidApplicationId, supportingDocumentId);

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsTrue((bool)((OkObjectResult)result).Value);
        }

        #endregion Remove Supporting Document tests       

        #region SubmitApplication tests
        /// <summary>
        /// Submit an application
        /// </summary>
        [TestMethod]
        public void ApplicationController_SubmitApplication_Post()
        {
            //arrange
            int fakeApplicationId = 3;
            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
             .Returns(Task.FromResult(new ApplicationSummary(fakeApplicationId, userOrganisationId, 13) { ApplicationStatusId = 1 }));
            string createdBy = string.Empty;

            SubmitApplicationCommand command = new SubmitApplicationCommand(fakeApplicationId);

            //act
            var response = _controller.SubmitApplication(command);

            //assert
            Assert.IsInstanceOfType(response.Result, typeof(OkObjectResult));
        }

        /// <summary>
        /// Submit an application
        /// </summary>
        [TestMethod]
        public void ApplicationController_SubmitApplication_Post_Already_Submitted()
        {
            //arrange
            int fakeApplicationId = 3;
            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
             .Returns(Task.FromResult(new ApplicationSummary(fakeApplicationId, userOrganisationId, 13) { ApplicationStatusId = 3 }));
            string createdBy = string.Empty;

            SubmitApplicationCommand command = new SubmitApplicationCommand(fakeApplicationId);

            //act
            var response = _controller.SubmitApplication(command);

            //assert
            Assert.IsInstanceOfType(response.Result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Submit an application for a correct application
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SubmitApplication_For_Correct_Application_Post()
        {
            //arrange
            int fakeApplicationId = 4;
            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
             .Returns(Task.FromResult(new ApplicationSummary(fakeApplicationId, userOrganisationId, 13) { ApplicationStatusId = 1 }));
            SubmitApplicationCommand command = new SubmitApplicationCommand(fakeApplicationId);

            //act
            var response = await _controller.SubmitApplication(command);

            //assert
            Assert.IsInstanceOfType(response, typeof(OkObjectResult));
        }

        /// <summary>
        /// Submit an application for an incorrect application
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SubmitApplication_For_Incorrect_Application_Post()
        {
            //arrange
            int fakeApplicationId = 2;
            var result = this.GetControllerInstance("Submit", false, 1, fakeApplicationId);
            SubmitApplicationCommand command = new SubmitApplicationCommand(fakeApplicationId);

            //act
            var response = await result.SubmitApplication(command);

            //assert
            Assert.IsInstanceOfType(response, typeof(NotFoundResult));
        }

        /// <summary>
        /// Submit an application with an invalid ModelState (this occurs when a malformed request is submitted)
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SubmitApplication_For_Invalid_ModelState()
        {
            //arrange
            int fakeApplicationId = 2;
            var controller = this.GetControllerInstance("Submit", false, 1, fakeApplicationId);
            controller.ModelState.AddModelError("command", "test error");
            var command = (SubmitApplicationCommand)null;

            //act
            var response = await controller.SubmitApplication(command);

            //assert
            Assert.IsInstanceOfType(response, typeof(BadRequestResult));
        }

        /// <summary>
        /// Submit an application that doesn't belong to your organisation
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_SubmitApplication_For_Wrong_Organisation()
        {
            //arrange
            int fakeApplicationId = 3;
            var controller = this.GetControllerInstance("Submit", false, ValidSupplierOrganisationId, fakeApplicationId);
            var command = new SubmitApplicationCommand(fakeApplicationId);

            //act
            var response = await controller.SubmitApplication(command);

            //assert
            Assert.IsInstanceOfType(response, typeof(ForbidResult));
        }
        #endregion SubmitApplication tests

        #region Pending applications tests
        /// <summary>
        /// Gets the pending applications
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ApplicationController_GetPendingApplicationPagedResult()
        {
            //act
            var result = await _controller.GetPendingApplicationPagedResult(new PendingApplicationParameters());

            //assert
            _mockApplicationService.Verify(x => x.GetPendingApplicationPagedResult(It.IsAny<PendingApplicationParameters>()), Times.Once);
        }
        #endregion Pending applications tests

        #region Applications ready for issue tests

        /// <summary>
        /// Tests retrieving applications ready for issue
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_GetApplicationsReadyForIssuePagedResult()
        {
            //arrange
            _mockApplicationService.Setup(mock => mock.GetApplicationsReadyForIssuePagedResult(10, 3))
                .ReturnsAsync(new PagedResult<ApplicationSummary>() { PageNumber = 3, PageSize = 10, TotalResults = 0, Result = new ApplicationSummary[] { } });

            //act
            var result = await _controller.GetApplicationsReadyForIssuePagedResult(10, 3);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(PagedResult<ApplicationSummary>));
            _mockApplicationService.Verify(x => x.GetApplicationsReadyForIssuePagedResult(10, 3), Times.Once);
        }

        #endregion Applications ready for issue tests

        #region Supplier Application tests
        /// <summary>
        /// test with invalid organisation id returns ForbidResult
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetSupplierApplicationPagedResult_Invalid_UserOrganisationId()
        {
            //act
            var result = _controller.GetSupplierApplicationPagedResult(2121, 13, 10, 1).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// test with organisation id with do not exist
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetSupplierApplicationPagedResult_Invalid_OrganisationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(System.Net.HttpStatusCode.NotFound)));

            //act
            var result = _controller.GetSupplierApplicationPagedResult(13, 12, 10, 1).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Test that current obligation period is set if not provided with obligation id
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetSupplierApplicationPagedResult_No_ObligationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .Returns(Task.FromResult(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));

            //act
            var result = _controller.GetSupplierApplicationPagedResult(13, 0, 10, 1).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            _mockReportingPeriodsService.Verify(x => x.GetCurrentObligationPeriod(), Times.Once);
        }

        /// <summary>
        /// Test case where current obligation period does not exist and no obligation id provided
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetSupplierApplicationPagedResult_No_ObligationId_And_No_Current_Obligation_Period()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>())));
            _mockReportingPeriodsService.Setup(x => x.GetCurrentObligationPeriod())
                .ReturnsAsync((ObligationPeriod)null);

            //act
            var result = _controller.GetSupplierApplicationPagedResult(13, 0, 10, 1).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
            _mockReportingPeriodsService.Verify(x => x.GetCurrentObligationPeriod(), Times.Once);
        }

        /// <summary>
        /// test invalid obligation period returns NotFound
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetSupplierApplicationPagedResult_Invalid_ObligationId()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(null));

            //act
            var result = _controller.GetSupplierApplicationPagedResult(13, 200, 10, 1).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// test GetSupplierApplication is invoked with given parameters
        /// </summary>
        [TestMethod]
        public void ApplicationController_GetSupplierApplicationPagedResult_Returns_Ok()
        {
            //arrange
            _mockOrganisationsService.Setup(x => x.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpObjectResponse<Organisation>(new Organisation(It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()
                , It.IsAny<int>(), It.IsAny<string>()))));
            _mockReportingPeriodsService.Setup(x => x.GetObligationPeriod(It.IsAny<int>()))
                .Returns(Task.FromResult<ObligationPeriod>(new ObligationPeriod(It.IsAny<int>(),
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<bool>(), It.IsAny<DateTime>(), It.IsAny<int>()
                , It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<decimal>()
                , It.IsAny<int>(), It.IsAny<decimal>())));
            _mockApplicationService.Setup(mock => mock.GetSupplierApplications(13, 200, 10, 1))
                .ReturnsAsync(new PagedResult<ApplicationSummary>() { PageNumber = 1, PageSize = 10, TotalResults = 1, Result = new ApplicationSummary[] { new ApplicationSummary(1, 13, 1) } });

            //act
            var result = _controller.GetSupplierApplicationPagedResult(13, 200, 10, 1).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(PagedResult<ApplicationSummary>));
            _mockApplicationService.Verify(x => x.GetSupplierApplications(13, 200, 10, 1), Times.Once);
        }
        #endregion Supplier Application tests

        #region ValidateFileMetadata tests

        /// <summary>
        /// Tests that the validate file metadata method handles the case where an invalid request is made
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_ValidateFileMetadata_Returns_BadResult_For_Invalid_Request()
        {
            //arrange
            int fakeApplicationId = 1;
            var controller = this.GetControllerInstance("Success", true, ValidSupplierOrganisationId, fakeApplicationId);
            controller.ModelState.AddModelError("fileMetadata", "test error");
            var fileMetadata = (FileMetadata)null;

            //act
            var result = await controller.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the validate file metadata method returns BadResult for invalid data
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_ValidateFileMetadata_Returns_BadResult_For_Invalid_FileMetadata()
        {
            //arrange
            _mockApplicationService.Setup(mock => mock.ValidateFileMetadata(It.IsAny<FileMetadata>()))
                .ReturnsAsync(new FileValidationResult() { Messages = new List<string>() { "Test error" } });
            var fileMetadata = new FileMetadata()
            {
                FileExtension = "",
                FileName = null,
                FileSizeBytes = 0
            };

            //act
            var result = await _controller.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(FileValidationResult));
            Assert.IsTrue(((FileValidationResult)((OkObjectResult)result).Value).Messages.Count > 0);
        }

        /// <summary>
        /// Tests that the validate file metadata method returns OkResult for invalid data
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_ValidateFileMetadata_Returns_OkResult_For_Valid_FileMetadata()
        {
            //arrange
            _mockApplicationService.Setup(mock => mock.ValidateFileMetadata(It.IsAny<FileMetadata>()))
                .ReturnsAsync(new FileValidationResult() { UploadStatus = "Validated" });
            var fileMetadata = new FileMetadata()
            {
                FileExtension = ".doc",
                FileName = "report.doc",
                FileSizeBytes = 246785
            };

            //act
            var result = await _controller.ValidateFileMetadata(fileMetadata);

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(FileValidationResult));
            Assert.AreEqual(0, ((FileValidationResult)((OkObjectResult)result).Value).Messages.Count);
        }

        #endregion ValidateFileMetadata tests

        #region ReviewApplicationVolumes tests

        /// <summary>
        /// Tests that the review application volumes method returns BadRequest if the ModelState is invalid (this can happen when a malformed request is received)
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_ReviewApplicationVolumes_Returns_BadRequest_For_Invalid_ModelState()
        {
            //arrange
            var command = (ReviewApplicationVolumesCommand)null;
            var controller = GetControllerInstance("Success", true, ValidSupplierOrganisationId, ValidApplicationId);
            controller.ModelState.AddModelError("command", "test error");

            //act
            var result = await controller.ReviewApplicationVolumes(command);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the review application volumes method returns NotFound if the supplied Application does not exist
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_ReviewApplicationVolumes_Returns_NotFound_For_Invalid_ApplicationId()
        {
            //arrange
            var command = new ReviewApplicationVolumesCommand(InvalidApplicationId, 1, "test");

            //act
            var result = await _controller.ReviewApplicationVolumes(command);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the review application volumes method returns Forbid if the Application belongs to a different Supplier
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_ReviewApplicationVolumes_Returns_Forbid_For_Wrong_Supplier()
        {
            //arrange
            var command = new ReviewApplicationVolumesCommand(SubmittedApplicationId, 1, "test");
            _mockApplicationService.Setup(mock => mock.GetApplicationSummary(SubmittedApplicationId))
                .ReturnsAsync(new ApplicationSummary(SubmittedApplicationId, InvalidSupplierOrganisationId, 13) { ApplicationStatusId = (int)ApplicationStatus.Submitted });

            //act
            var result = await _controller.ReviewApplicationVolumes(command);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the review application volumes method returns BadRequest if the Application is not at Submitted status
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_ReviewApplicationVolumes_Returns_BadRequest_If_Application_Not_Submitted()
        {
            //arrange
            var command = new ReviewApplicationVolumesCommand(ApprovedApplicationId, 1, "test");
            var controller = GetControllerInstance("Success", true, ValidSupplierOrganisationId, ApprovedApplicationId);

            //act
            var result = await controller.ReviewApplicationVolumes(command);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the review application volumes method returns OK if the Application is reviewed successfully
        /// </summary>
        [TestMethod]
        public async Task ApplicationController_ReviewApplicationVolumes_Returns_Ok_If_Application_Reviewed_Successfully()
        {
            //arrange
            var command = new ReviewApplicationVolumesCommand(SubmittedApplicationId, 1, "test");
            _mockApplicationService.Setup(mock => mock.GetApplicationSummary(SubmittedApplicationId))
                .ReturnsAsync(new ApplicationSummary(SubmittedApplicationId, userOrganisationId, 13) { ApplicationStatusId = (int)ApplicationStatus.Submitted });

            //act
            var result = await _controller.ReviewApplicationVolumes(command);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }

        #endregion ReviewApplicationVolumes tests

        #region Revoke Application tests
        ///<summary>
        /// Checks a Bad Request response is returned when no Application Id is provided
        ///</summary>
        [TestMethod]
        public async Task ApplicationController_RevokeApplications_Returns_Bad_Request_When_Application_Id_Is_Not_Provided()
        {
            // Arrange 
            RevokeApplicationCommand revocation = null;
            _controller.ModelState.AddModelError("Incorrect Body", "test error");

            // Act
            var result = await _controller.RevokeApplication(revocation);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        ///<summary>
        /// Ensures revoke application method returns an OK response when the application is revoked successfully
        ///</summary>
        [TestMethod]
        public async Task ApplicationController_RevokeApplications_Returns_OK_When_Application_Is_Revoked()
        {
            // Arrange 
            var revocation = new RevokeApplicationCommand(1, null);
            _mockApplicationService.Setup(a => a.GetApplicationSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(new ApplicationSummary(1, 1, 15)
                {
                    ApplicationStatusId = (int)ApplicationStatus.GHGCreditsIssued
                }));
            _mockApplicationService.Setup(mock => mock.RevokeApplication(It.IsAny<RevokeApplicationCommand>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            // Act
            var result = await _controller.RevokeApplication(revocation);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            _mockApplicationService.Verify(a => a.RevokeApplication(It.IsAny<RevokeApplicationCommand>(), It.IsAny<string>()), Times.Once);
        }

        ///<summary>
        /// Ensures revoke application method returns an InternalServerError response when the application fails to revoke for an unknown reason
        ///</summary>
        [TestMethod]
        public async Task ApplicationController_RevokeApplications_Returns_InternalServerError_When_Application_Fails_To_Revoke_For_Unknown_Reason()
        {
            // Arrange 
            var revocation = new RevokeApplicationCommand(1, null);
            _mockApplicationService.Setup(a => a.GetApplicationSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(new ApplicationSummary(1, 1, 15)
                {
                    ApplicationStatusId = (int)ApplicationStatus.GHGCreditsIssued
                }));
            _mockApplicationService.Setup(mock => mock.RevokeApplication(It.IsAny<RevokeApplicationCommand>(), It.IsAny<string>()))
                .Returns(Task.FromResult(false));

            // Act
            StatusCodeResult result = (StatusCodeResult)await _controller.RevokeApplication(revocation);

            // Assert 
            Assert.AreEqual(500, result.StatusCode);
            _mockApplicationService.Verify(a => a.RevokeApplication(It.IsAny<RevokeApplicationCommand>(), It.IsAny<string>()), Times.Once);
        }

        ///<summary>
        /// Returns Bad Request when revocation fails due to a bad request.
        ///</summary>
        [TestMethod]
        public async Task ApplicationController_RevokeApplication_Returns_Bad_Request_When_Revocation_Fails_Due_To_A_Bad_Request()
        {
            // Arrange
            var revocation = new RevokeApplicationCommand(1, null);
            _mockApplicationService.Setup(a => a.GetApplicationSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(new ApplicationSummary(1, 1, 15)
                {
                    ApplicationStatusId = (int)ApplicationStatus.GHGCreditsIssued
                }));
            _mockApplicationService.Setup(mock => mock.RevokeApplication(It.IsAny<RevokeApplicationCommand>(), It.IsAny<string>()))
                .Throws(new InvalidApplicationRevokeException());

            // Act
            var result = await _controller.RevokeApplication(revocation);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            _mockApplicationService.Verify(a => a.RevokeApplication(It.IsAny<RevokeApplicationCommand>(), It.IsAny<string>()), Times.Once);
        }

        ///<summary>
        /// Returns Not Found when the application cannot be found 
        ///</summary>
        [TestMethod]
        public async Task ApplicationController_RevokeApplication_Returns_Not_found_When_The_Application_Is_Not_Found()
        {
            // Arrange 
            var revocation = new RevokeApplicationCommand(1, null);
            _mockApplicationService.Setup(a => a.RevokeApplication(It.IsAny<RevokeApplicationCommand>(), It.IsAny<string>()))
                .Throws(new ApplicationNotFoundException());

            // Act
            var result = await _controller.RevokeApplication(revocation);

            // Assert 
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }
        #endregion Revoke Application tests

        #endregion Tests

        #region Private Methods
        /// <summary>
        /// Get the controller instance using the supplier mock objects
        /// </summary>
        /// <param name="userOrganisationId">Id of Organisation to which user belongs</param>
        /// <param name="mockApplicationsService">Mock ApplicationsService</param>
        /// <param name="mockScanVirusService">Mock ScanVirusService</param>
        /// <param name="mockSystemParametersService">Mock SystemParametersService</param>
        /// <param name="mockSupportingDocumentsParametersService">Mock SupportDocumentsParametersService</param>
        /// <param name="mockOrganisationsService">Mock OrganisationsService</param>
        /// <returns></returns>
        private ApplicationsController GetControllerInstance(int userOrganisationId,
            Mock<IApplicationsService> mockApplicationsService,
            Mock<IScanVirusService> mockScanVirusService,
            Mock<ISystemParametersService> mockSystemParametersService,
            Mock<ISupportingDocumentParametersService> mockSupportingDocumentsParametersService,
            Mock<IOrganisationsService> mockOrganisationsService,
            Mock<IGhgFuelsService> mockGhgFuelsService,
            Mock<IReportingPeriodsService> mockReportingPeriodsService)
        {
            var controllerUnderTest = new ApplicationsController(mockApplicationsService.Object
                , mockScanVirusService.Object
                , mockSystemParametersService.Object
                , mockSupportingDocumentsParametersService.Object
                , mockOrganisationsService.Object
                , mockGhgFuelsService.Object
                , mockReportingPeriodsService.Object);

            // - user principal
            var claims = new List<Claim>
            {
                new Claim(Claims.UserId, Guid.NewGuid().ToString()),
                new Claim(Claims.OrganisationId, userOrganisationId.ToString())
            };

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            controllerUnderTest.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };

            return controllerUnderTest;
        }

        /// <summary>
        /// Get the controller instance and setup methods  with message, extension, virus check
        /// </summary>
        /// <param name="message"> message for test case</param>
        /// <param name="virusFree">file size</param>
        /// <param name="userOrganisationId">The organisation ID of the user's organisation</param>
        /// <returns></returns>
        private ApplicationsController GetControllerInstance(string message, bool virusFree, int userOrganisationId, int applicationId)
        {
            var mockApplicationsService = new Mock<IApplicationsService>();
            mockApplicationsService.Setup(s => s.ValidateFileMetadata(It.IsAny<FileMetadata>()))
                .ReturnsAsync(() =>
                {
                    FileValidationResult filevalidationResult = new FileValidationResult();

                    if (message.Length > 0)
                    {
                        filevalidationResult.Messages.Add(message);
                    }
                    return filevalidationResult;
                });

            mockApplicationsService.Setup(s => s.UploadSupportingDocument(It.IsAny<AddSupportingDocumentCommand>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(() =>
                {
                    FileValidationResult filevalidationResult = new FileValidationResult();

                    if (message.Length > 0)
                    {
                        filevalidationResult.Messages.Add(message);
                    }
                    return filevalidationResult;
                });

            mockApplicationsService.Setup(s => s.UploadSupportingDocument(It.IsAny<AddSupportingDocumentCommand>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(() =>
                {
                    FileValidationResult filevalidationResult = new FileValidationResult();
                    ScanResult scanResult = new ScanResult();

                    if (virusFree)
                    {
                        scanResult.Message = "";
                    }
                    scanResult.IsVirusFree = virusFree;
                    filevalidationResult.ScanResult = scanResult;

                    return filevalidationResult;
                });

            mockApplicationsService.Setup(s => s.SubmitApplication(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(() =>
                {
                    int fakeApplicationId = applicationId;
                    int fakeSupplierOrganisationId = 1;
                    int fakeObligationPeriodId = 1;
                    ApplicationSummary applicationSummary = new ApplicationSummary(fakeApplicationId, fakeSupplierOrganisationId, fakeObligationPeriodId);
                    applicationSummary.ApplicationId = applicationId;

                    return "";
                });
            if (applicationId == 4)
            {
                mockApplicationsService.Setup(s => s.GetApplicationSummary(It.IsAny<int>()))
                .ReturnsAsync(() =>
                {
                    ApplicationSummary applicationSummary = new ApplicationSummary(1, 1, 1);
                    applicationSummary.ApplicationId = applicationId;

                    return applicationSummary;
                });
            }
            else if (applicationId == 3)
            {
                mockApplicationsService.Setup(s => s.GetApplicationSummary(It.IsAny<int>()))
                .ReturnsAsync(() =>
                {
                    int fakeApplicationId = applicationId;
                    int fakeSupplierOrganisationId = 1;
                    int fakeObligationPeriodId = 1;
                    ApplicationSummary applicationSummary = new ApplicationSummary(fakeApplicationId, fakeSupplierOrganisationId, fakeObligationPeriodId);
                    applicationSummary.ApplicationId = applicationId;
                    return applicationSummary;
                });
            }
            else if (applicationId == 2)
            {
                mockApplicationsService.Setup(s => s.GetApplicationSummary(It.IsAny<int>()))
                .ReturnsAsync(() =>
                {
                    return null;
                });
            }
            else if (applicationId == 4)
            {
                mockApplicationsService.Setup(s => s.GetApplicationSummary(It.IsAny<int>()))
                .ReturnsAsync(() =>
                {
                    return null;
                });
            }
            else
            {
                mockApplicationsService.Setup(s => s.GetApplicationSummary(ValidApplicationId))
                    .ReturnsAsync(new ApplicationSummary(ValidApplicationId, ValidSupplierOrganisationId, 13) { ApplicationStatusId = (int)ApplicationStatus.Open });
                mockApplicationsService.Setup(s => s.GetApplicationItems(ValidApplicationId))
                    .ReturnsAsync(new ApplicationItemSummary[] { new ApplicationItemSummary(ValidApplicationId, It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), null, null, null) });
                mockApplicationsService.Setup(s => s.GetApplicationSummary(InvalidApplicationId))
                    .ReturnsAsync((ApplicationSummary)null);
                mockApplicationsService.Setup(s => s.GetApplicationSummary(ApprovedApplicationId))
                    .ReturnsAsync(new ApplicationSummary(ApprovedApplicationId, ValidSupplierOrganisationId, 13) { ApplicationStatusId = (int)ApplicationStatus.Approved });
                mockApplicationsService.Setup(s => s.GetUploadedSupportingDocuments(ValidApplicationId))
                    .ReturnsAsync(new List<UploadedSupportingDocument>() { new UploadedSupportingDocument() { SupportingDocumentId = 1, FileName = "test.doc" } });
                mockApplicationsService.Setup(s => s.GetUploadedSupportingDocument(ValidApplicationId, ValidSupportingDocumentId))
                    .ReturnsAsync(new DownloadSupportingDocument() { FileName = "test.doc", File = Encoding.UTF8.GetBytes("Test text") });
                mockApplicationsService.Setup(s => s.RemoveSupportingDocument(ValidApplicationId, ValidSupportingDocumentId))
                    .ReturnsAsync(true);
            }

            var mockScanVirusService = new Mock<IScanVirusService>();
            var mockSystemParametersService = new Mock<ISystemParametersService>();
            var mockSupportingDocumentsParametersService = new Mock<ISupportingDocumentParametersService>();

            //Setup organisations for security checks
            var nonSupplierOrganisation = new Organisation(NonSupplierOrganisationId, "Non Supplier Organisation", 1, "Fake Status", 2, "");
            var validSupplier = new Organisation(ValidSupplierOrganisationId, "Valid Supplier", 1, "Fake Status", 1, "");

            var mockOrganisationsService = new Mock<IOrganisationsService>();
            mockOrganisationsService.Setup(o => o.GetOrganisation(NonSupplierOrganisationId, "")).Returns(Task.FromResult(new HttpObjectResponse<Organisation>(nonSupplierOrganisation)));
            mockOrganisationsService.Setup(o => o.GetOrganisation(ValidSupplierOrganisationId, "")).Returns(Task.FromResult(new HttpObjectResponse<Organisation>(validSupplier)));

            return GetControllerInstance(userOrganisationId, mockApplicationsService, mockScanVirusService, mockSystemParametersService, mockSupportingDocumentsParametersService, mockOrganisationsService, new Mock<IGhgFuelsService>(), new Mock<IReportingPeriodsService>());
        }

        #endregion Private Methods
    }
}
