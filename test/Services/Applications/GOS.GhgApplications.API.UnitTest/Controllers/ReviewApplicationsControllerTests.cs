﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Commands;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.API.Services;
using DfT.GOS.Security.Claims;
using GOS.GhgApplications.API.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using static DfT.GOS.GhgApplications.API.Data.ApplicationStatus;

namespace DfT.GOS.GhgApplications.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests the review applications controller class
    /// </summary>
    [TestClass]
    public class ReviewApplicationsControllerTests
    {
        #region Private Members

        private ReviewApplicationsController _controller;
        private Mock<IApplicationsService> _mockApplicationService;

        private const int UserOrganisationId = 13;
        private const int SomeOtherOrganisationId = 14;
        private const string CurrentUserId = "60de7427-bc6e-4327-854e-70805ff16773";

        #endregion Private Members

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ReviewApplicationsControllerTests()
        {
        }
        #endregion Constructors

        #region Initialize
        [TestInitialize]
        public void Initialize()
        {
           _mockApplicationService = new Mock<IApplicationsService>();
            _controller = new ReviewApplicationsController(_mockApplicationService.Object);
            var claims = new List<Claim>();
            claims.Add(new Claim(Claims.UserId, CurrentUserId));
            claims.Add(new Claim(Claims.OrganisationId, UserOrganisationId.ToString()));

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            // -Setup the controller
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }
        #endregion 

        #region GetApplicationReviewSummary tests
        /// <summary>
        /// Throws a object not found result for invalid application id
        /// </summary>
        [TestMethod]
        public void GetApplicationReviewSummary_Returns_NotFoundResult()
        {
            //arrange
            _mockApplicationService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationReviewSummary>(null));
            //act
            var result = _controller.GetApplicationReviewSummary(2);
            //assert
            _mockApplicationService.Verify(x => x.GetApplicationReviewSummary(It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result.Result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Returns a valid Ok object result with Application review summary
        /// </summary>
        [TestMethod]
        public void GetApplicationReviewSummary_Returns_Application_Review_Summary()
        {
            //arrange
            _mockApplicationService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(new ApplicationReviewSummary() { ApplicationId = 2, ApplicationStatus = ApplicationStatus.Submitted }));
            //act
            var result = _controller.GetApplicationReviewSummary(2);
            //assert
            _mockApplicationService.Verify(x => x.GetApplicationReviewSummary(It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
            Assert.AreEqual(2, ((ApplicationReviewSummary)((OkObjectResult)result.Result).Value).ApplicationId);
        }
        #endregion GetApplicationReviewSummary tests

        #region ReviewApplication tests
        /// <summary>
        /// Returns a BadRequestObjectResult
        /// </summary>
        [TestMethod]
        public void ReviewApplication_Returns_BadRequest_With_Different_Application_Id()
        {
            //arrange
            var reviewApplication = new ReviewApplicationCommand(1, 1);

            //act
            var result = _controller.ReviewApplication(2, reviewApplication);

            //assert
            Assert.IsInstanceOfType(result.Result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Returns a NotFoundObjectResult
        /// </summary>
        [TestMethod]
        public void ReviewApplication_Returns_NotFound_Invalid_Application_Id()
        {
            //arrange
            var reviewApplication = new ReviewApplicationCommand(1, 1);
            _mockApplicationService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationReviewSummary>(null));

            //act
            var result = _controller.ReviewApplication(1, reviewApplication);

            //assert
            Assert.IsInstanceOfType(result.Result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Returns a BadRequestResult
        /// </summary>
        [TestMethod]
        public void ReviewApplication_Returns_BadRequest_Invalid_ApplicationStatus()
        {
            //arrange
            var reviewApplication = new ReviewApplicationCommand(1, 1);
            _mockApplicationService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(new ApplicationReviewSummary { ApplicationId = 1, ApplicationStatusId = 1 }));

            //act
            var result = _controller.ReviewApplication(1, reviewApplication);

            //assert
            Assert.IsInstanceOfType(result.Result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Returns a BadRequestResult if this ModelState is invalid (this can happen when a malformed request is submitted
        /// </summary>
        [TestMethod]
        public void ReviewApplication_Returns_BadRequest_Invalid_ReviewApplicationCommand()
        {
            //arrange
            _mockApplicationService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(new ApplicationReviewSummary { ApplicationId = 1, ApplicationStatusId = 3, SupplierOrganisationId = UserOrganisationId }));
            _controller.ModelState.AddModelError("", "Invalid command");

            //act
            var result = _controller.ReviewApplication(1, null);

            //assert
            Assert.IsInstanceOfType(result.Result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Returns a ForbidResult
        /// </summary>
        [TestMethod]
        public void ReviewApplication_Returns_ForbidResult_Invalid_OrganisationId()
        {
            //arrange
            var reviewApplication = new ReviewApplicationCommand(1, 1);
            _mockApplicationService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(new ApplicationReviewSummary { ApplicationId = 1, ApplicationStatusId = 3, SupplierOrganisationId = 2121212 }));

            //act
            var result = _controller.ReviewApplication(1, reviewApplication);

            //assert
            Assert.IsInstanceOfType(result.Result, typeof(ForbidResult));
        }

        /// <summary>
        /// Returns a OkObjectResult
        /// </summary>
        [TestMethod]
        public void ReviewApplication_Returns_OkResult()
        {
            //arrange
            var reviewApplication = new ReviewApplicationCommand(1, 1);
            _mockApplicationService.Setup(x => x.GetApplicationReviewSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(new ApplicationReviewSummary { ApplicationId = 1, ApplicationStatusId = 3, SupplierOrganisationId = UserOrganisationId }));
            _mockApplicationService.Setup(x => x.ReviewApplication(It.IsAny<ReviewApplicationCommand>()))
                .Returns(Task.FromResult(true));
            //act
            var result = _controller.ReviewApplication(1, reviewApplication);

            //assert
            Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
        }
        #endregion ReviewApplication tests

        #region Review Application Volumes Tests

        /// <summary>
        /// ReviewApplicationVolumes - Returns a bad request if the application ids doesn't match that of the command
        /// </summary>
        [TestMethod]
        public void ReviewApplicationVolumes_Returns_BadRequest_When_Application_Ids_Do_Not_Match()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationVolumesCommand(2, It.IsAny<int>(), It.IsAny<string>());
            //act
            var result = _controller.ReviewApplicationVolumes(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationVolumes(It.IsAny<ReviewApplicationVolumesCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationVolumes - Returns an object not found result for invalid application id
        /// </summary>
        [TestMethod]
        public void ReviewApplicationVolumes_Returns_NotFound_With_No_Application()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationVolumesCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult<ApplicationSummary>(null));

            //act
            var result = _controller.ReviewApplicationVolumes(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationVolumes(It.IsAny<ReviewApplicationVolumesCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationVolumes - Returns a bad request if the application is not in submitted state
        /// </summary>
        [TestMethod]
        public void ReviewApplicationVolumes_Returns_BadRequest_When_Application_Not_Submitted_State()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationVolumesCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()) { ApplicationStatusId = (int)ApplicationStatus.Open }));

            //act
            var result = _controller.ReviewApplicationVolumes(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationVolumes(It.IsAny<ReviewApplicationVolumesCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationVolumes - Returns a forbid if the user doesn't have permission to view the application
        /// </summary>
        [TestMethod]
        public void ReviewApplicationVolumes_Returns_Forbid_Incorrect_Permission()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationVolumesCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
               {
                   ApplicationStatusId = (int)ApplicationStatus.Submitted,
                   SupplierOrganisationId = SomeOtherOrganisationId
               }));

            //act
            var result = _controller.ReviewApplicationVolumes(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationVolumes(It.IsAny<ReviewApplicationVolumesCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationVolumes - Returns a Bad Request if there are model state errors - this can happen
        /// </summary>
        [TestMethod]
        public void ReviewApplicationVolumes_Returns_BadRequest_On_Invalid_Model()
        {
            //arrange
            var applicationId = 1;

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
               {
                   ApplicationStatusId = (int)ApplicationStatus.Submitted,
                   SupplierOrganisationId = UserOrganisationId
               }));

            _controller.ModelState.AddModelError("1", "Fake model state error");

            //act
            var result = _controller.ReviewApplicationVolumes(applicationId, null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationVolumes(It.IsAny<ReviewApplicationVolumesCommand>()), Times.Never());
        }

        /// <summary>
        /// Tests the ReviewApplicationVolumes happy path
        /// </summary>
        [TestMethod]
        public void ReviewApplicationVolumes_Returns_Ok_On_Success()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationVolumesCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
               {
                   ApplicationStatusId = (int)ApplicationStatus.Submitted,
                   SupplierOrganisationId = UserOrganisationId
               }));         

            //act
            var result = _controller.ReviewApplicationVolumes(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationVolumes(It.IsAny<ReviewApplicationVolumesCommand>()), Times.Once());
        }

        #endregion Review Application Volumes Tests

        #region Review Application Supporting Documents Tests

        /// <summary>
        /// ReviewApplicationSupportingDocuments - Returns a bad request if the application ids doesn't match that of the command
        /// </summary>
        [TestMethod]
        public void ReviewApplicationSupportingDocuments_Returns_BadRequest_When_Application_Ids_Do_Not_Match()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationSupportingDocumentsCommand(2, It.IsAny<int>(), It.IsAny<string>());
            //act
            var result = _controller.ReviewApplicationSupportingDocuments(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationSupportingDocuments(It.IsAny<ReviewApplicationSupportingDocumentsCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationSupportingDocuments - Returns an object not found result for invalid application id
        /// </summary>
        [TestMethod]
        public void ReviewApplicationSupportingDocuments_Returns_NotFound_With_No_Application()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationSupportingDocumentsCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult<ApplicationSummary>(null));

            //act
            var result = _controller.ReviewApplicationSupportingDocuments(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationSupportingDocuments(It.IsAny<ReviewApplicationSupportingDocumentsCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationSupportingDocuments - Returns a bad request if the application is not in submitted state
        /// </summary>
        [TestMethod]
        public void ReviewApplicationSupportingDocuments_Returns_BadRequest_When_Application_Not_Submitted_State()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationSupportingDocumentsCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()) { ApplicationStatusId = (int)ApplicationStatus.Open }));

            //act
            var result = _controller.ReviewApplicationSupportingDocuments(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationSupportingDocuments(It.IsAny<ReviewApplicationSupportingDocumentsCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationSupportingDocuments - Returns a forbid if the user doesn't have permission to view the application
        /// </summary>
        [TestMethod]
        public void ReviewApplicationSupportingDocuments_Returns_Forbid_Incorrect_Permission()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationSupportingDocumentsCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
               {
                   ApplicationStatusId = (int)ApplicationStatus.Submitted,
                   SupplierOrganisationId = SomeOtherOrganisationId
               }));

            //act
            var result = _controller.ReviewApplicationSupportingDocuments(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationSupportingDocuments(It.IsAny<ReviewApplicationSupportingDocumentsCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationSupportingDocuments - Returns a Bad Request if there are model state errors
        /// </summary>
        [TestMethod]
        public void ReviewApplicationSupportingDocuments_Returns_BadRequest_On_Invalid_Model()
        {
            //arrange
            var applicationId = 1;

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
               {
                   ApplicationStatusId = (int)ApplicationStatus.Submitted,
                   SupplierOrganisationId = UserOrganisationId
               }));

            _controller.ModelState.AddModelError("1", "Fake model state error");

            //act
            var result = _controller.ReviewApplicationSupportingDocuments(applicationId, null).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationSupportingDocuments(It.IsAny<ReviewApplicationSupportingDocumentsCommand>()), Times.Never());
        }

        /// <summary>
        /// Tests the ReviewApplicationSupportingDocuments happy path
        /// </summary>
        [TestMethod]
        public void ReviewApplicationSupportingDocuments_Returns_Ok_On_Success()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationSupportingDocumentsCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
               {
                   ApplicationStatusId = (int)ApplicationStatus.Submitted,
                   SupplierOrganisationId = UserOrganisationId
               }));

            //act
            var result = _controller.ReviewApplicationSupportingDocuments(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationSupportingDocuments(It.IsAny<ReviewApplicationSupportingDocumentsCommand>()), Times.Once());
        }

        #endregion Review Application Supporting Documents Tests

        #region Review Application Calculation Tests

        /// <summary>
        /// ReviewApplicationCalculation - Returns a bad request if the application ids doesn't match that of the command
        /// </summary>
        [TestMethod]
        public void ReviewApplicationCalculation_Returns_BadRequest_If_Application_Ids_Do_Not_Match()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationCalculationCommand(2, It.IsAny<int>(), It.IsAny<string>());
            //act
            var result = _controller.ReviewApplicationCalculation(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationCalculation(It.IsAny<ReviewApplicationCalculationCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationCalculation - Returns an object not found result for invalid application id
        /// </summary>
        [TestMethod]
        public void ReviewApplicationCalculation_Returns_NotFound_With_No_Application()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationCalculationCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult<ApplicationSummary>(null));

            //act
            var result = _controller.ReviewApplicationCalculation(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationCalculation(It.IsAny<ReviewApplicationCalculationCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationCalculation - Returns a bad request if the application is not in submitted state
        /// </summary>
        [TestMethod]
        public void ReviewApplicationCalculation_Returns_BadRequest_When_Application_Not_Submitted_State()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationCalculationCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()) { ApplicationStatusId = (int)ApplicationStatus.Open }));

            //act
            var result = _controller.ReviewApplicationCalculation(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationCalculation(It.IsAny<ReviewApplicationCalculationCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationCalculation - Returns a forbid if the user doesn't have permission to view the application
        /// </summary>
        [TestMethod]
        public void ReviewApplicationCalculation_Returns_Forbid_Incorrect_Permission()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationCalculationCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
               {
                   ApplicationStatusId = (int)ApplicationStatus.Submitted,
                   SupplierOrganisationId = SomeOtherOrganisationId
               }));

            //act
            var result = _controller.ReviewApplicationCalculation(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationCalculation(It.IsAny<ReviewApplicationCalculationCommand>()), Times.Never());
        }

        /// <summary>
        /// ReviewApplicationCalculation - Returns a Bad Request if there are model state errors
        /// </summary>
        [TestMethod]
        public void ReviewApplicationCalculation_Returns_BadRequest_On_Invalid_Model()
        {
            //arrange
            var applicationId = 1;

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
               {
                   ApplicationStatusId = (int)ApplicationStatus.Submitted,
                   SupplierOrganisationId = UserOrganisationId
               }));

            _controller.ModelState.AddModelError("1", "Fake model state error");

            //act
            var result = _controller.ReviewApplicationCalculation(applicationId, null).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationCalculation(It.IsAny<ReviewApplicationCalculationCommand>()), Times.Never());
        }

        /// <summary>
        /// Tests the ReviewApplicationCalculation happy path
        /// </summary>
        [TestMethod]
        public void ReviewApplicationCalculation_Returns_Ok_On_Success()
        {
            //arrange
            var applicationId = 1;
            var command = new ReviewApplicationCalculationCommand(applicationId, It.IsAny<int>(), It.IsAny<string>());

            _mockApplicationService.Setup(x => x.GetApplicationSummary(It.IsAny<int>()))
               .Returns(Task.FromResult(new ApplicationSummary(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
               {
                   ApplicationStatusId = (int)ApplicationStatus.Submitted,
                   SupplierOrganisationId = UserOrganisationId
               }));

            //act
            var result = _controller.ReviewApplicationCalculation(applicationId, command).Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            _mockApplicationService.Verify(s => s.ReviewApplicationCalculation(It.IsAny<ReviewApplicationCalculationCommand>()), Times.Once());
        }

        #endregion Review Application Supporting Documents Tests

        #region Approve Application Tests

        /// <summary>
        /// Tests that the application IDs in the command and the URL match
        /// </summary>
        [TestMethod]
        public void ApproveApplication_Application_Ids_Match()
        {
            //arrange
            var command = new ApproveApplicationCommand(1);

            //act
            var result = _controller.ApproveApplication(2, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual("Application ID parameters in command and URL do not match", ((BadRequestObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the method checks for a valid application
        /// </summary>
        [TestMethod]
        public void ApproveApplication_Application_Not_Found()
        {
            //arrange
            int applicationId = 1;
            var command = new ApproveApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(null));

            //act
            var result = _controller.ApproveApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
            Assert.AreEqual("Application not found", ((NotFoundObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the application cannot be approved in the incorrect state
        /// </summary>
        [TestMethod]
        public void ApproveApplication_Application_Cannot_Be_Approved_In_Incorrect_State()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary() { ApplicationStatusId = (int)ApplicationStatuses.Open };

            var command = new ApproveApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));

            //act
            var result = _controller.ApproveApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual("The application is in the incorrect state for approval", ((BadRequestObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the application cannot be approved by the same person who recommended it for approval
        /// </summary>
        [TestMethod]
        public void ApproveApplication_Application_Cannot_Be_Approved_By_Recommender()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary()
            {
                ApplicationStatusId = (int)ApplicationStatuses.RecommendApproval,
                RecommendedBy = CurrentUserId
            };

            var command = new ApproveApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));

            //act
            var result = _controller.ApproveApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual("The application can't be approved by the same administrator who recommended it for approval", ((BadRequestObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the application cannot be approved by the same person who recommended it for approval
        /// </summary>
        [TestMethod]
        public void ApproveApplication_Application_Must_Have_Permission_To_Access_The_Application_Organisation()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary()
            {
                ApplicationStatusId = (int)ApplicationStatuses.RecommendApproval,
                RecommendedBy = Guid.NewGuid().ToString(),
                SupplierOrganisationId = SomeOtherOrganisationId
            };

            var command = new ApproveApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));

            //act
            var result = _controller.ApproveApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the application cannot be approved with invalid model state
        /// </summary>
        [TestMethod]
        public void ApproveApplication_Application_Must_Have_Valid_Model_State()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary()
            {
                ApplicationStatusId = (int)ApplicationStatuses.RecommendApproval,
                RecommendedBy = Guid.NewGuid().ToString(),
                SupplierOrganisationId = UserOrganisationId
            };

            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));
            _controller.ModelState.AddModelError("1", "Fake Error");

            //act
            var result = _controller.ApproveApplication(applicationId, null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the application successfully calls the service method
        /// </summary>
        [TestMethod]
        public void ApproveApplication_Application_Success()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary()
            {
                ApplicationStatusId = (int)ApplicationStatuses.RecommendApproval,
                RecommendedBy = Guid.NewGuid().ToString(),
                SupplierOrganisationId = UserOrganisationId
            };

            var command = new ApproveApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));
            
            //act
            var result = _controller.ApproveApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            _mockApplicationService.Verify(s => s.ApproveApplication(It.IsAny<ApproveApplicationCommand>(), It.IsAny<string>()), Times.Once());
        }

        #endregion Approve Application Tests

        #region Reject Application Tests

        /// <summary>
        /// Tests that the application IDs in the command and the URL match
        /// </summary>
        [TestMethod]
        public void RejectApplication_Application_Ids_Match()
        {
            //arrange
            var command = new RejectApplicationCommand(1);

            //act
            var result = _controller.RejectApplication(2, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual("Application ID parameters in command and URL do not match", ((BadRequestObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the method checks for a valid application
        /// </summary>
        [TestMethod]
        public void RejectApplication_Application_Not_Found()
        {
            //arrange
            int applicationId = 1;
            var command = new RejectApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(null));

            //act
            var result = _controller.RejectApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
            Assert.AreEqual("Application not found", ((NotFoundObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the application cannot be rejected in the incorrect state
        /// </summary>
        [TestMethod]
        public void RejectApplication_Application_Cannot_Be_Approved_In_Incorrect_State()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary() { ApplicationStatusId = (int)ApplicationStatuses.Open };

            var command = new RejectApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));

            //act
            var result = _controller.RejectApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual("The application is in the incorrect state for rejection", ((BadRequestObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the application cannot be rejected by the same person who recommended it for approval
        /// </summary>
        [TestMethod]
        public void RejectApplication_Application_Cannot_Be_Approved_By_Recommender()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary()
            {
                ApplicationStatusId = (int)ApplicationStatuses.RecommendApproval,
                RecommendedBy = CurrentUserId
            };

            var command = new RejectApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));

            //act
            var result = _controller.RejectApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual("The application can't be rejected by the same administrator who recommended it for approval", ((BadRequestObjectResult)result).Value);
        }

        /// <summary>
        /// Tests that the application cannot be rejected by the same person who recommended it for approval
        /// </summary>
        [TestMethod]
        public void RejectApplication_Application_Must_Have_Permission_To_Access_The_Application_Organisation()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary()
            {
                ApplicationStatusId = (int)ApplicationStatuses.RecommendApproval,
                RecommendedBy = Guid.NewGuid().ToString(),
                SupplierOrganisationId = SomeOtherOrganisationId
            };

            var command = new RejectApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));

            //act
            var result = _controller.RejectApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the application cannot be rejected with invalid model state
        /// </summary>
        [TestMethod]
        public void RejectApplication_Application_Must_Have_Valid_Model_State()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary()
            {
                ApplicationStatusId = (int)ApplicationStatuses.RecommendApproval,
                RecommendedBy = Guid.NewGuid().ToString(),
                SupplierOrganisationId = UserOrganisationId
            };

            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));
            _controller.ModelState.AddModelError("1", "Fake Error");

            //act
            var result = _controller.RejectApplication(applicationId, null).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        /// <summary>
        /// Tests that the reject application successfully calls the service method
        /// </summary>
        [TestMethod]
        public void RejectApplication_Application_Success()
        {
            //arrange
            int applicationId = 1;
            var fakeApplication = new ApplicationReviewSummary()
            {
                ApplicationStatusId = (int)ApplicationStatuses.RecommendApproval,
                RecommendedBy = Guid.NewGuid().ToString(),
                SupplierOrganisationId = UserOrganisationId
            };

            var command = new RejectApplicationCommand(applicationId);
            _mockApplicationService.Setup(s => s.GetApplicationReviewSummary(applicationId)).Returns(Task.FromResult<ApplicationReviewSummary>(fakeApplication));

            //act
            var result = _controller.RejectApplication(applicationId, command).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            _mockApplicationService.Verify(s => s.RejectApplication(It.IsAny<RejectApplicationCommand>(), It.IsAny<string>()), Times.Once());
        }

        #endregion Reject Application Tests

        #region Issue Credits tests
        /// <summary>
        /// Tests when no application to issue
        /// </summary>
        [TestMethod]
        public void IssueCredits_WithNo_Applications_To_Process()
        {
            //arrange
            _mockApplicationService.Setup(s => s.IssueCredits(It.IsAny<IssueCreditCommand>()))
                .Returns(Task.FromResult(false));

            //act
            var result = _controller.IssueCredits().Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsFalse((bool)((OkObjectResult)result).Value);
            _mockApplicationService.Verify(s => s.IssueCredits(It.IsAny<IssueCreditCommand>()), Times.Once());
        }

        /// <summary>
        /// Test a successful credit issue
        /// </summary>
        [TestMethod]
        public void IssueCredits_Updates_Application_Status()
        {
            //arrange
            _mockApplicationService.Setup(s => s.IssueCredits(It.IsAny<IssueCreditCommand>()))
                .Returns(Task.FromResult(true));

            //act
            var result = _controller.IssueCredits().Result;
            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsTrue((bool)((OkObjectResult)result).Value);
            _mockApplicationService.Verify(s => s.IssueCredits(It.IsAny<IssueCreditCommand>()), Times.Once());
        }
        #endregion Issue Credits tests
    }
}
