﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.API.Services;
using DfT.GOS.Security.Claims;
using GOS.GhgApplications.API.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests for the Performance Data Controller
    /// </summary>
    [TestClass]
    public class PerformanceDataControllerTest
    {

        #region Constants
        private const string UserId = "123";
        private const string OrganisationId = "xyz";
        #endregion Constants

        #region Properties
        private PerformanceDataController Controller { get; set; }
        private Mock<IApplicationsService> _mockApplicationService { get; set; }
        #endregion Properties

        #region Initialize
        /// <summary>
        /// Sets up the controller for tests before each test is run
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this._mockApplicationService = new Mock<IApplicationsService>();
            this.Controller = new PerformanceDataController(this._mockApplicationService.Object);
        }
        #endregion Initialize

        #region RegistrationsData tests

        ///<summary>
        /// ApplicationPerformanceDataa forwards on the list of data
        ///</summary>
        [TestMethod]
        public async Task ApplicationPerformanceData_Returns_The_Application_Performance_Data()
        {
            // Arrange
            this._mockApplicationService.Setup(s => s.GetApplicationPerformanceData())
                .Returns(Task.FromResult(new List<ApplicationPerformanceData>
                {
                    new ApplicationPerformanceData(DateTime.Now, It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()),
                    new ApplicationPerformanceData(DateTime.Now, It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())
                }));

            // Act
            var result = await this.Controller.ApplicationPerformanceData();

            // Assert 
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(result);

        }
        #endregion ApplicationPerformanceData tests
    }
}
