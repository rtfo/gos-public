﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.SystemParameters.API.Controllers;
using DfT.GOS.SystemParameters.API.Models;
using DfT.GOS.SystemParameters.API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.SystemParameters.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests the System Parameters Controller
    /// </summary>
    [TestClass]
    public class SystemParametersControllerTest
    {
        #region Properties

        private SystemParametersController ControllerUnderTest { get; set; }
        private Mock<ISystemParametersService> MockSystemParametersService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public SystemParametersControllerTest()
        {
            this.SetupControllerInstance();
        }

        #endregion Constructors


        #region Private Methods

        /// <summary>
        /// Sets up a configured instance of the controller under test
        /// </summary>
        /// <returns></returns>
        private void SetupControllerInstance()
        {
            this.MockSystemParametersService = new Mock<ISystemParametersService>();
            this.MockSystemParametersService.Setup(s => s.GetSystemParameters())
                .ReturnsAsync(new SystemParameter[] { new SystemParameter(1, "FakeName", "Fake Value") });
            this.MockSystemParametersService.Setup(s => s.GetDocumentTypes())
                .ReturnsAsync(new DocumentType[] { new DocumentType(1, "ContentType", ".ext", null) });

            this.ControllerUnderTest = new SystemParametersController(this.MockSystemParametersService.Object);
        }

        #endregion Private Methods

        #region Tests

        /// <summary>
        /// Tests that the Get method returns a result
        /// </summary>
        [TestMethod]
        public async Task SystemParametersController_Get_Returns_Result()
        {
            //act
            var result = await this.ControllerUnderTest.GetAll();

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            // - We get a list of system parameters back
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IList<SystemParameter>));
        }

        /// <summary>
        /// Tests that the GetDocumentTypes method returns a result
        /// </summary>
        [TestMethod]
        public async Task SystemParametersController_GetDocumentTypes_Returns_Result()
        {
            //act
            var result = await this.ControllerUnderTest.GetDocumentTypes();

            //assert
            // - We get a 200 response
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            // - We get a list of document types back
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(IList<DocumentType>));
        }

        #endregion Tests
    }
}
