﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.GhgBalanceView.API.Controllers;
using DfT.GOS.GhgBalanceView.API.Services;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceView.API.UnitTest.Controllers
{
    /// <summary>
    /// Tests the Balance Controller
    /// </summary>
    [TestClass]
    public class BalanceControllerTest
    {
        #region Constants

        private const int UserOrganisationId = 13;
        private const int SomeOtherOrganisationId = 14;
        private const int FakeObligationPeriodId = 15;
        private const string CurrentUserId = "60de7427-bc6e-4327-854e-70805ff16773";

        #endregion Constants

        #region Private variables

        private BalanceController _controller;
        private Mock<IBalanceViewService> _mockBalanceViewService;
        private Mock<ILogger<BalanceController>> _mockLogger;

        #endregion Private variables

        #region Initialise

        /// <summary>
        /// Test initialisation run at the beginning of each test
        /// </summary>
        [TestInitialize]
        public void Initialise()
        {
            _mockBalanceViewService = new Mock<IBalanceViewService>();
            _mockLogger = new Mock<ILogger<BalanceController>>();
            _controller = new BalanceController(_mockBalanceViewService.Object, _mockLogger.Object);

            // -Setup the controller Auth
            var claims = new List<Claim>();
            claims.Add(new Claim(Claims.UserId, CurrentUserId));
            claims.Add(new Claim(Claims.OrganisationId, UserOrganisationId.ToString()));

            var claimsPrincipal = new ClaimsPrincipal();
            claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal
                }
            };
        }

        #endregion Initialise

        #region Tests

        #region GetSummary Tests     

        /// <summary>
        /// Tests that the Get Summary method handles a Forbid result for the summary
        /// </summary>
        [TestMethod]
        public async Task GetSummary_Returns_ForbidResult()
        {
            //act
            var result = await _controller.GetSummary(SomeOtherOrganisationId, 2);

            //assert
            _mockBalanceViewService.Verify(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>()), Times.Never);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Get Summary method handles a not found result for the summary
        /// </summary>
        [TestMethod]
        public async Task GetSummary_Returns_NotFoundResult()
        {
            //arrange
            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<BalanceSummary>(null));

            //act
            var result = await _controller.GetSummary(UserOrganisationId, 2);

            //assert
            _mockBalanceViewService.Verify(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the Get Summary method returns a summary result
        /// </summary>
        [TestMethod]
        public async Task GetSummary_Returns_ValidResult()
        {
            //arrange
            decimal submissionsCredits = 10;
            decimal submissionsObligation = 20;
            decimal tradingCredits = 30;
            decimal balanceCredits = 40;
            decimal balanceObligation = 50;
            decimal netPosition = 60;
            decimal buyoutCost = 70;
            decimal buyOutPer_tCO2e = 80;

            var balanceSummary = new BalanceSummary(submissionsCredits
                , submissionsObligation
                , tradingCredits
                , balanceCredits
                , balanceObligation
                , netPosition
                , buyoutCost
                , buyOutPer_tCO2e);

            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<BalanceSummary>(balanceSummary));

            //act
            var result = await _controller.GetSummary(UserOrganisationId, 2);

            //assert
            _mockBalanceViewService.Verify(s => s.GetSummary(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(BalanceSummary));

            var actual = (BalanceSummary)((OkObjectResult)result).Value;
            Assert.AreEqual(submissionsCredits, actual.SubmissionsCredits);
            Assert.AreEqual(submissionsObligation, actual.SubmissionsObligation);
            Assert.AreEqual(tradingCredits, actual.TradingCredits);
            Assert.AreEqual(balanceCredits, actual.BalanceCredits);
            Assert.AreEqual(balanceObligation, actual.BalanceObligation);
            Assert.AreEqual(netPosition, actual.NetPosition);
            Assert.AreEqual(buyoutCost, actual.BuyoutCost);
            Assert.AreEqual(buyOutPer_tCO2e, actual.BuyOutPer_tCO2e);
        }

        #endregion GetSummary Tests

        #region GetSummary (all) tests
        /// <summary>
        /// Tests that the Get Summary method handles a not found result for the summary
        /// </summary>
        [TestMethod]
        public async Task GetSummary_All_Returns_NotFoundResult()
        {
            //arrange
            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>()))
                .Returns(Task.FromResult<TotalBalanceSummary>(null));

            //act
            var result = await _controller.GetSummary(FakeObligationPeriodId);

            //assert
            _mockBalanceViewService.Verify(s => s.GetSummary(It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the Get Summary method returns a summary result
        /// </summary>
        [TestMethod]
        public async Task GetSummary_All_Returns_ValidResult()
        {
            //arrange
            decimal balanceCredits = 40;
            decimal balanceObligation = 50;
            decimal netPosition = -60;

            var balanceSummary = new TotalBalanceSummary(
                 balanceCredits
                , balanceObligation
                , netPosition);

            _mockBalanceViewService.Setup(s => s.GetSummary(It.IsAny<int>()))
                .Returns(Task.FromResult(balanceSummary));

            //act
            var result = await _controller.GetSummary(FakeObligationPeriodId);

            //assert
            _mockBalanceViewService.Verify(s => s.GetSummary(It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(TotalBalanceSummary));

            var actual = (TotalBalanceSummary)((OkObjectResult)result).Value;
            Assert.AreEqual(balanceCredits, actual.BalanceCredits);
            Assert.AreEqual(balanceObligation, actual.BalanceObligation);
            Assert.AreEqual(netPosition, actual.NetPosition);
        }
        #endregion GetSummary (all) tests

        #region GetSubmissions Tests

        /// <summary>
        /// Tests that the GetSubmissions method handles a Forbid result for the summary
        /// </summary>
        [TestMethod]
        public async Task GetSubmissions_Returns_ForbidResult()
        {
            //act
            var result = await _controller.GetSubmissions(SomeOtherOrganisationId, 2);

            //assert
            _mockBalanceViewService.Verify(s => s.GetSummary(It.IsAny<int>()
                , It.IsAny<int>()), Times.Never);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that Getsubmissions returns notfound result when there is no submissions record
        /// </summary>
        [TestMethod]
        public async Task GetSubmissions_Returns_Notfound()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetSubmissions(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<SubmissionBalanceSummary>(null));

            //act
            var result = await _controller.GetSubmissions(UserOrganisationId, 13);

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that Getsubmissions returns successful result
        /// </summary>
        [TestMethod]
        public async Task GetSubmissions_Returns_Successful_Result()
        {
            //arrange
            var submission = new SubmissionBalanceSummary(1000, 800
                , new LiquidGasSubmissionsSummary(500, 300, 1000)
                , new ElectricitySubmissionsSummary(500, 500, 1000)
                , new UpstreamEmissionsReductionSubmissionsSummary(0));
            _mockBalanceViewService.Setup(x => x.GetSubmissions(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<SubmissionBalanceSummary>(submission));

            //act
            var result = await _controller.GetSubmissions(UserOrganisationId, 13);

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(SubmissionBalanceSummary));

            var actual = (SubmissionBalanceSummary)((OkObjectResult)result).Value;
            Assert.AreEqual(submission.TotalCredits, actual.TotalCredits);
            Assert.AreEqual(submission.TotalObligation, actual.TotalObligation);
        }
        #endregion GetSubmissions tests

        #region GetLiquidGasSummary tests
        /// <summary>
        /// Tests that the GetLiquidGasSummary method handles a Forbid result for the summary
        /// </summary>
        [TestMethod]
        public void GetLiquidGasSummary_Returns_ForbidResult()
        {
            //act
            var result = _controller.GetLiquidGasSummary(SomeOtherOrganisationId, 2).Result;

            //assert
            _mockBalanceViewService.Verify(s => s.GetSummary(It.IsAny<int>()
                , It.IsAny<int>()), Times.Never);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that GetLiquidGasSummary returns notfound result when there is no submissions record
        /// </summary>
        [TestMethod]
        public void GetLiquidGasSummary_Returns_Notfound()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<LiquidGasBalanceSummary>(null));

            //act
            var result = _controller.GetLiquidGasSummary(UserOrganisationId, 13).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that GetLiquidGasSummary returns successful result
        /// </summary>
        [TestMethod]
        public void GetLiquidGasSummary_Returns_Successful_Result()
        {
            //arrange
            var summary = new LiquidGasBalanceSummary(0, 0, 0, 0, 0
                , new RtfoAdminConsignmentsSubmissionsSummary(0, 0, 0)
                , new OtherVolumesSubmissionsSummary(0, 0, 0)
                , new FossilGasSubmissionsSummary(0, 0, 0));
            _mockBalanceViewService.Setup(x => x.GetLiquidGasSummary(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<LiquidGasBalanceSummary>(summary));

            //act
            var result = _controller.GetLiquidGasSummary(UserOrganisationId, 13).Result;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(LiquidGasBalanceSummary));

            var actual = (LiquidGasBalanceSummary)((OkObjectResult)result).Value;
            Assert.AreEqual(summary.TotalCredits, actual.TotalCredits);
            Assert.AreEqual(summary.TotalObligation, actual.TotalObligation);
        }

        #endregion GetLiquidGasSummary tests

        #region GetTrading tests

        /// <summary>
        /// Tests that the Get Trading method handles a Forbid result for the summary
        /// </summary>
        [TestMethod]
        public async Task GetTrading_Returns_ForbidResult()
        {
            //act
            var result = await _controller.GetTrading(SomeOtherOrganisationId, 2, 1, 10);

            //assert
            _mockBalanceViewService.Verify(s => s.GetTrading(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.Never);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that the Get Trading method handles a not found result for the summary
        /// </summary>
        [TestMethod]
        public async Task GetTrading_Returns_NotFoundResult()
        {
            //arrange
            _mockBalanceViewService.Setup(s => s.GetTrading(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<TradingSummary>(null));

            //act
            var result = await _controller.GetTrading(UserOrganisationId, 2, 1, 10);

            //assert
            _mockBalanceViewService.Verify(s => s.GetTrading(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that the Get Trading method returns a summary result
        /// </summary>
        [TestMethod]
        public async Task GetTading_Returns_ValidResult()
        {
            //arrange
            decimal balanceCredits = 100;
            int reference = 2;
            DateTime date = DateTime.Now;
            int eventTypeId = 3;
            string eventType = "fake event type";
            int tradingOrganisationId = 4;
            string tradingOrganisation = "fake trading Organisation";
            decimal credits = 50;
            string additionalReference = "additional reference";

            var creditEvent = new CreditEvent(reference, date, eventTypeId, eventType, tradingOrganisationId, tradingOrganisation, credits, additionalReference);
            var creditEvents = new List<CreditEvent>();
            creditEvents.Add(creditEvent);

            var tradingSummary = new TradingSummary(balanceCredits, new Web.Models.PagedResult<CreditEvent>()
            {
                Result = creditEvents
            });

            _mockBalanceViewService.Setup(s => s.GetTrading(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<TradingSummary>(tradingSummary));

            //act
            var result = await _controller.GetTrading(UserOrganisationId, 2, 1, 10);

            //assert
            _mockBalanceViewService.Verify(s => s.GetTrading(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(TradingSummary));

            var actual = (TradingSummary)((OkObjectResult)result).Value;
            Assert.AreEqual(balanceCredits, actual.BalanceCredits);
            Assert.AreEqual(reference, actual.CreditEvents.Result[0].Reference);
            Assert.AreEqual(date, actual.CreditEvents.Result[0].Date);
            Assert.AreEqual(eventTypeId, actual.CreditEvents.Result[0].EventTypeId);
            Assert.AreEqual(eventType, actual.CreditEvents.Result[0].EventType);
            Assert.AreEqual(tradingOrganisationId, actual.CreditEvents.Result[0].TradingOrganisationId);
            Assert.AreEqual(tradingOrganisation, actual.CreditEvents.Result[0].TradingOrganisation);
            Assert.AreEqual(credits, actual.CreditEvents.Result[0].Credits);
            Assert.AreEqual(additionalReference, actual.CreditEvents.Result[0].AdditionalReference);
        }

        #endregion GetTrading tests

        #region GetRftoAdminConsignment Tests
        /// <summary>
        /// Tests that GetRftoAdminConsignment returns forbid result when incorrect organisation id supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetRftoAdminConsignment_Returns_Forbid_When_Incorrect_Organisation_Id_Supplied()
        {
            //act
            var result = await this._controller.GetRftoAdminConsignment(12, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that GetRftoAdminConsignment returns notfound result when balance api returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetRftoAdminConsignment_Returns_NotFound_Result_When_BalanceAPI_Returns_Null()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetRftoAdminConsignment(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<PagedResult<GhgBalanceView.Common.Models.RtfoAdminConsignmentSubmission>>(null));

            //act
            var result = await this._controller.GetRftoAdminConsignment(UserOrganisationId, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that GetRftoAdminConsignment returns OK result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetRftoAdminConsignment_Returns_Ok_Result()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetRftoAdminConsignment(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<PagedResult<GhgBalanceView.Common.Models.RtfoAdminConsignmentSubmission>>(
                    new PagedResult<Common.Models.RtfoAdminConsignmentSubmission>()
                    { Result = new List<Common.Models.RtfoAdminConsignmentSubmission>()}));

            //act
            var result = await this._controller.GetRftoAdminConsignment(UserOrganisationId, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
        #endregion GetRftoAdminConsignment tests
        
        #region GetOtherRtfoVolumes Tests
        /// <summary>
        /// Tests that GetOtherRtfoVolumes returns forbid result when incorrect organisation id supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetOtherRtfoVolumes_Returns_Forbid_When_Incorrect_Organisation_Id_Supplied()
        {
            //act
            var result = await this._controller.GetOtherRtfoVolumes(12, 15, 1, 10);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that GetOtherRtfoVolumes returns notfound result when balance api returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetOtherRtfoVolumes_Returns_NotFound_Result_When_BalanceAPI_Returns_Null()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetOtherRtfoVolumes(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<PagedResult<GhgBalanceView.Common.Models.OtherVolumesSubmission>>(null));

            //act
            var result = await this._controller.GetOtherRtfoVolumes(UserOrganisationId, 15, 1, 10);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that GetOtherRtfoVolumes returns OK result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetOtherRtfoVolumes_Returns_Ok_Result()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetOtherRtfoVolumes(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<PagedResult<GhgBalanceView.Common.Models.OtherVolumesSubmission>>(
                    new PagedResult<Common.Models.OtherVolumesSubmission>()
                    { Result = new List<Common.Models.OtherVolumesSubmission>() }));

            //act
            var result = await this._controller.GetOtherRtfoVolumes(UserOrganisationId, 15, 1, 10);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
        #endregion GetOtherRtfoVolumes tests

        #region GetFossilFuelSubmissions Tests
        /// <summary>
        /// Tests that GetFossilFuelSubmissions returns forbid result when incorrect organisation id supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetFossilFuelSubmissions_Returns_Forbid_When_Incorrect_Organisation_Id_Supplied()
        {
            //act
            var result = await this._controller.GetFossilFuelSubmissions(12, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that GetFossilFuelSubmissions returns notfound result when balance api returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetFossilFuelSubmissions_Returns_NotFound_Result_When_BalanceAPI_Returns_Null()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetFossilFuelSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<PagedResult<GhgBalanceView.Common.Models.FossilGasSubmission>>(null));

            //act
            var result = await this._controller.GetFossilFuelSubmissions(UserOrganisationId, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that GetFossilFuelSubmissions returns OK result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetFossilFuelSubmissions_Returns_Ok_Result()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetFossilFuelSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<PagedResult<GhgBalanceView.Common.Models.FossilGasSubmission>>(
                    new PagedResult<Common.Models.FossilGasSubmission>()
                    { Result = new List<Common.Models.FossilGasSubmission>() }));

            //act
            var result = await this._controller.GetFossilFuelSubmissions(UserOrganisationId, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
        #endregion GetFossilFuelSubmissions Tests

        #region GetElectricitySubmissions Tests
        /// <summary>
        /// Tests that GetElectricitySubmissions returns forbid result when incorrect organisation id supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetElectricitySubmissions_Returns_Forbid_When_Incorrect_Organisation_Id_Supplied()
        {
            //act
            var result = await this._controller.GetElectricitySubmissions(12, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that GetElectricitySubmissions returns notfound result when balance api returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetElectricitySubmissions_Returns_NotFound_Result_When_BalanceAPI_Returns_Null()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetElectricitySubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<PagedResult<GhgBalanceView.Common.Models.ElectricitySubmission>>(null));

            //act
            var result = await this._controller.GetElectricitySubmissions(UserOrganisationId, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that GetElectricitySubmissions returns OK result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetElectricitySubmissions_Returns_Ok_Result()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetElectricitySubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<PagedResult<GhgBalanceView.Common.Models.ElectricitySubmission>>(
                    new PagedResult<Common.Models.ElectricitySubmission>()
                    { Result = new List<Common.Models.ElectricitySubmission>() }));

            //act
            var result = await this._controller.GetElectricitySubmissions(UserOrganisationId, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
        #endregion GetElectricitySubmissions Tests

        #region GetUpstreamEmissionReductionSubmissions Tests

        /// <summary>
        /// Tests that GetUpstreamEmissionReductionSubmissions returns forbid result when incorrect organisation id supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetUpstreamEmissionReductionSubmissions_Returns_Forbid_When_Incorrect_Organisation_Id_Supplied()
        {
            //act
            var result = await this._controller.GetUpstreamEmissionReductionSubmissions(12, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that GetUpstreamEmissionReductionSubmissions returns not found result when balance api returns null
        /// </summary>
        [TestMethod]
        public async Task GetUpstreamEmissionReductionSubmissions_Returns_NotFound_Result_When_BalanceAPI_Returns_Null()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetUpstreamEmissionReductionSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<UpstreamEmissionsReductionsSummary>(null));

            //act
            var result = await this._controller.GetUpstreamEmissionReductionSubmissions(UserOrganisationId, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that GetUpstreamEmissionReductionSubmissions returns OK result
        /// </summary>
        [TestMethod]
        public async Task GetUpstreamEmissionReductionSubmissions_Returns_Ok_Result()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GetUpstreamEmissionReductionSubmissions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<UpstreamEmissionsReductionsSummary>(
                    new UpstreamEmissionsReductionsSummary(1000, null)));

            //act
            var result = await this._controller.GetUpstreamEmissionReductionSubmissions(UserOrganisationId, 15, 1);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
        #endregion GetUpstreamEmissionReductionSubmissions Tests

        #region GenerateSubmissionsExport Tests

        /// <summary>
        /// Tests that GenerateSubmissionsExport returns forbid result when incorrect organisation id supplied
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GenerateSubmissionsExport_Returns_Forbid_When_Incorrect_Organisation_Id_Supplied()
        {
            //arrange
            var incorrectOrgId = 12;

            //act
            var result = await this._controller.GenerateSubmissionsExport(incorrectOrgId, obligationPeriodId: 15);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ForbidResult));
        }

        /// <summary>
        /// Tests that GenerateSubmissionsExport returns not found result when balance api returns null
        /// </summary>
        [TestMethod]
        public async Task GenerateSubmissionsExport_Returns_NotFound_Result_When_BalanceAPI_Returns_Null()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GenerateExportForOrganisation(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<string>(null));

            //act
            var result = await this._controller.GenerateSubmissionsExport(UserOrganisationId, obligationPeriodId: 15);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that GenerateSubmissionsExport returns OK result
        /// </summary>
        [TestMethod]
        public async Task GenerateSubmissionsExport_Returns_Ok_Result()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GenerateExportForOrganisation(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<string>(""));

            //act
            var result = await this._controller.GenerateSubmissionsExport(UserOrganisationId, obligationPeriodId: 15);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
        #endregion GenerateSubmissionsExport Tests

        #region DownloadAllApplications Tests

        /// <summary>
        /// Tests that DownloadAllApplications returns not found result when balance api returns null
        /// </summary>
        [TestMethod]
        public async Task DownloadAllApplications_Returns_NotFound_Result_When_BalanceAPI_Returns_Null()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GenerateExportForAllOrganisations(It.IsAny<int>()))
                .Returns(Task.FromResult<string>(null));

            //act
            var result = await this._controller.DownloadAllApplications(obligationPeriodId: 15);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that DownloadAllApplications returns OK result
        /// </summary>
        [TestMethod]
        public async Task DownloadAllApplications_Returns_Ok_Result()
        {
            //arrange
            _mockBalanceViewService.Setup(x => x.GenerateExportForAllOrganisations(It.IsAny<int>()))
                .Returns(Task.FromResult<string>(""));

            //act
            var result = await this._controller.DownloadAllApplications(obligationPeriodId: 15);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
        #endregion DownloadAllApplications Tests

        #region GetCreditTransferCsv Tests

        /// <summary>
        /// Tests that GetCreditTransfersCsv returns Not Found if no credit transfer records exist
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetCreditTransfersCsv_Returns_NotFound_When_No_CreditTransfers()
        {
            //arrange
            _mockBalanceViewService.Setup(s => s.GetCreditTransfers(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<IList<CreditTransferSummary>>(null));

            //act
            var result = await this._controller.GetCreditTransfersCsv(1, "");

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
        }

        /// <summary>
        /// Tests that GetCreditTransfersCsv returns file byte array
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetCreditTransfersCsv_Returns_File_When_CreditTransfers_Exist()
        {
            //arrange
            var creditSummaries = new List<CreditTransferSummary>();

            _mockBalanceViewService.Setup(s => s.GetCreditTransfers(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult<IList<CreditTransferSummary>>(creditSummaries));

            //act
            var result = await this._controller.GetCreditTransfersCsv(1, "");

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(byte[]));
        }

        #endregion GetCreditTransferCsv Tests

        #endregion Tests
    }
}
