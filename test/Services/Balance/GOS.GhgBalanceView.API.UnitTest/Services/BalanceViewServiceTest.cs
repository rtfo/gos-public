﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using AutoMapper;
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.GhgBalance.Common.Repositories;
using DfT.GOS.GhgBalanceView.API.Services;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FossilGasSubmission = DfT.GOS.GhgBalanceView.Common.Models.FossilGasSubmission;
using RtfoAdminConsignmentSubmission = DfT.GOS.GhgBalanceView.Common.Models.RtfoAdminConsignmentSubmission;

namespace DfT.GOS.GhgBalanceView.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests for the Balance View Service
    /// </summary>
    [TestClass]
    public class BalanceViewServiceTest
    {
        #region private variables

        private Mock<IBalanceRepository> _mockBalanceRepository;
        private IBalanceViewService _service;
        private Mock<IMapper> Mapper;

        #endregion private variables

        #region Test Initialise

        /// <summary>
        /// Test initialisation run at the beginning of each test
        /// </summary>
        [TestInitialize]
        public void Initialise()
        {
            _mockBalanceRepository = new Mock<IBalanceRepository>();
            Mapper = new Mock<IMapper>();
            _service = new BalanceViewService(_mockBalanceRepository.Object, Mapper.Object);
        }

        #endregion Test Initialise

        #region Tests

        #region GetSummary Tests

        /// <summary>
        /// Tests that the GetSummary method returns a balance summary correctly
        /// </summary>
        [TestMethod]
        public async Task GetSummary_Returns_BalanceSummary()
        {
            var mockSubmissions = new Mock<Submissions>();
            var mockTrading = new Mock<Trading>();

            var mockOrganisationBalance = new Mock<OrganisationBalance>();
            mockOrganisationBalance.Setup(b => b.Submissions).Returns(mockSubmissions.Object);
            mockOrganisationBalance.Setup(b => b.Trading).Returns(mockTrading.Object);

            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(mockOrganisationBalance.Object));

            //act
            var result = await _service.GetSummary(1, 2);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(BalanceSummary));
        }

        /// <summary>
        /// Tests that the GetSummary method correctly handles a summary not being found in the repository
        /// </summary>
        [TestMethod]
        public async Task GetSummary_Handles_Repository_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(null));

            //act
            var result = await _service.GetSummary(1, 2);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsNull(result);
        }

        #endregion GetSummary Tests

        #region GetSubmissions Tests

        /// <summary>
        /// Tests that the GetSubmissions method returns submissions data correctly
        /// </summary>
        [TestMethod]
        public async Task GetSubmissions_Returns_BalanceSummary()
        {
            var mockSubmissions = new Mock<Submissions>();
            var mockTrading = new Mock<Trading>();

            var mockOrganisationBalance = new Mock<OrganisationBalance>();
            mockOrganisationBalance.Setup(b => b.Submissions).Returns(mockSubmissions.Object);
            mockOrganisationBalance.Setup(b => b.Trading).Returns(mockTrading.Object);

            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(mockOrganisationBalance.Object));

            //act
            var result = await _service.GetSubmissions(1, 2);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(SubmissionBalanceSummary));
        }

        /// <summary>
        /// Tests that the GetSubmissions method correctly handles submission data not being found in the repository
        /// </summary>
        [TestMethod]
        public async Task GetSubmissions_Handles_Repository_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(null));

            //act
            var result = await _service.GetSubmissions(1, 2);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsNull(result);
        }

        #endregion GetSubmissions Tests

        #region GetTrading Tests

        /// <summary>
        /// Tests that the GetTrading method returns a trading summary, with pagination, correctly
        /// </summary>
        [TestMethod]
        public async Task GetTrading_Returns_TradingSummary()
        {
            var mockTrading = new Mock<Trading>();
            var creditEvents = new List<CreditEvent>();
            mockTrading.Setup(t => t.CreditEvents).Returns(creditEvents);

            var mockOrganisationBalance = new Mock<OrganisationBalance>();
            mockOrganisationBalance.Setup(b => b.Trading).Returns(mockTrading.Object);

            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(mockOrganisationBalance.Object));

            //act
            var result = await _service.GetTrading(1, 2, 1, 10);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(TradingSummary));
        }

        /// <summary>
        /// Tests that the GetTrading method correctly handles a summary not being found in the repository
        /// </summary>
        [TestMethod]
        public async Task GetTrading_Handles_Repository_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(null));

            //act
            var result = await _service.GetTrading(1, 2, 1, 10);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsNull(result);
        }

        #endregion GetTrading Tests

        #region GetRftoAdminConsignment Test
        /// <summary>
        /// Tests that GetRftoAdminConsignment returns null when balance repository returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetRftoAdminConsignment_Returns_Null_When_BalanceSummary_Is_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(x => x.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(null));

            //act
            var result = await this._service.GetRftoAdminConsignment(12, 12, 1, 10);

            //assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// Tests that GetRftoAdminConsignment returns PagedResult<RtfoAdminConsignmentSubmission>
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetRftoAdminConsignment_Returns_PagedResult_Of_RtfoAdminConsignmentSubmission()
        {
            //arrange
            var orgBalance = new OrganisationBalance(It.IsAny<int>(), It.IsAny<DateTime?>()
                , It.IsAny<int>(), It.IsAny<string>(), new Submissions(new LiquidGasSubmissions(
                    new RtfoAdminConsignmentSubmissions(new List<GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission>()
                    , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>())
            , It.IsAny<RtfoVolumeSubmissions>(), It.IsAny<FossilGasSubmissions>()
            , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()
            , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()
            , It.IsAny<decimal>()), It.IsAny<ElectricitySubmissions>(), It.IsAny<UpstreamEmissionsReductionSubmissions>()
            , It.IsAny<decimal>(), It.IsAny<decimal>()), It.IsAny<Trading>()
            , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());

            Mapper.Setup(x => x.Map<IList<GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission>
            , IList<RtfoAdminConsignmentSubmission>>(It.IsAny<List<GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission>>()))
                .Returns(new List<RtfoAdminConsignmentSubmission>());
            _mockBalanceRepository.Setup(x => x.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(orgBalance));

            //act
            var result = await this._service.GetRftoAdminConsignment(12, 12, 1, 10);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(PagedResult<RtfoAdminConsignmentSubmission>));
        }
        #endregion GetRftoAdminConsignment Test

        #region GetFossilFuelSubmissions Tests
        /// <summary>
        /// Tests that GetFossilFuelSubmissions returns null when balance repository returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetFossilFuelSubmissions_Returns_Null_When_BalanceSummary_Is_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(x => x.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(null));

            //act
            var result = await this._service.GetFossilFuelSubmissions(12, 12, 1, 10);

            //assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// Tests that GetFossilFuelSubmissions returns PagedResult<FossilGasSubmission>
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetFossilFuelSubmissions_Returns_PagedResult_Of_FossilGasSubmission()
        {
            //arrange
            var orgBalance = new OrganisationBalance(It.IsAny<int>(), It.IsAny<DateTime?>()
                , It.IsAny<int>(), It.IsAny<string>(), new Submissions(new LiquidGasSubmissions(
                    new RtfoAdminConsignmentSubmissions(new List<GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission>()
                    , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>())
            , It.IsAny<RtfoVolumeSubmissions>(), new FossilGasSubmissions(new List<GhgBalance.Common.Data.Balance.FossilGasSubmission>()
            , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>())
            , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()
            , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()
            , It.IsAny<decimal>()), It.IsAny<ElectricitySubmissions>(), It.IsAny<UpstreamEmissionsReductionSubmissions>()
            , It.IsAny<decimal>(), It.IsAny<decimal>()), It.IsAny<Trading>()
            , It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());

            Mapper.Setup(x => x.Map<IList<GhgBalance.Common.Data.Balance.FossilGasSubmission>
            , IList<FossilGasSubmission>>(It.IsAny<List<GhgBalance.Common.Data.Balance.FossilGasSubmission>>()))
                .Returns(new List<FossilGasSubmission>());
            _mockBalanceRepository.Setup(x => x.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(orgBalance));

            //act
            var result = await this._service.GetFossilFuelSubmissions(12, 12, 1, 10);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(PagedResult<FossilGasSubmission>));
        }
        #endregion GetFossilFuelSubmissions Tests

        #region GetElectricitySubmissions Tests
        /// <summary>
        /// Tests that GetElectricitySubmissions returns null when balance repository returns null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetElectricitySubmissions_Returns_Null_When_BalanceSummary_Is_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(x => x.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(null));

            //act
            var result = await this._service.GetElectricitySubmissions(12, 12, 1, 10);

            //assert
            Assert.IsNull(result);
        }


        #endregion GetElectricitySubmissions Tests

        #region GetUpstreamEmissionReductionSubmissions Tests

        /// <summary>
        /// Tests that the GetUpstreamEmissionReductionSubmissions method returns a UER summary, with pagination, correctly
        /// </summary>
        [TestMethod]
        public async Task GetUpstreamEmissionReductionSubmissions_Returns_UpstreamEmissionsReductionSummary()
        {
            //arrange
            var mockOrganisationBalance = new Mock<OrganisationBalance>();
            var fakeUerSubmissions = new UpstreamEmissionsReductionSubmissions(new List<UpstreamEmissionsReductionSubmission>(), 1000);
            var fakeSubmissions = new Submissions(null, null, fakeUerSubmissions, 1000, 1000);
            mockOrganisationBalance.Setup(b => b.Submissions).Returns(fakeSubmissions);

            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult(mockOrganisationBalance.Object));

            //act
            var result = await _service.GetUpstreamEmissionReductionSubmissions(1, 2, 1, 10);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsInstanceOfType(result, typeof(UpstreamEmissionsReductionsSummary));
        }

        /// <summary>
        /// Tests that the GetUpstreamEmissionReductionSubmissions method correctly handles a summary not being found in the repository
        /// </summary>
        [TestMethod]
        public async Task GetUpstreamEmissionReductionSubmissions_Handles_Repository_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(null));

            //act
            var result = await _service.GetUpstreamEmissionReductionSubmissions(1, 2, 1, 10);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsNull(result);
        }

        #endregion GetUpstreamEmissionReductionSubmissions Tests

        #region GenerateExportForOrganisation Tests

        /// <summary>
        /// Tests that the GenerateExportForOrganisation method correctly not being found in the repository
        /// </summary>
        [TestMethod]
        public async Task GenerateExportForOrganisation_Handles_Repository_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(null));

            //act
            var result = await _service.GenerateExportForOrganisation(1, 15);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsNull(result);
        }

        /// <summary>
        /// Tests that the GenerateExportForOrganisation method correctly generates CSV format
        /// </summary>
        [TestMethod]
        public async Task GenerateExportForOrganisation_Generates_Correct_CSV()
        {
            var orgBalance = new OrganisationBalance(It.IsAny<int>(), new DateTime(2019, 1, 1)
                , 1, "Org name"
                , new Submissions(new LiquidGasSubmissions(
                    new RtfoAdminConsignmentSubmissions(
                        new List<GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission> {
                            new GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission(new DateTime(2019, 1, 10), 23, "int ref", 10, "type", 5, "feedstock", 1.2m, 0.7m, 0.3m, 0.2m, 1.2m, 0.3m, 0.2m)
                        }, 0.1m, 0.2m, 0.3m)
                      , new RtfoVolumeSubmissions(
                          new List<GhgBalance.Common.Data.Balance.RtfoVolumeSubmission> {
                              new GhgBalance.Common.Data.Balance.RtfoVolumeSubmission(new DateTime(2019, 1, 10), 23, "type", 0.1m, 1.2m, 0.3m, 0.2m, 1.2m, 0.3m, 0.2m)
                          }, 0.1m, 0.2m, 0.3m)
                      , new FossilGasSubmissions(
                          new List<GhgBalance.Common.Data.Balance.FossilGasSubmission> {
                              new GhgBalance.Common.Data.Balance.FossilGasSubmission(123, new DateTime(2019, 1, 10), 23, 1, "type", 0.1m, 1.2m, 0.3m, 0.2m, 1.2m, 0.3m, 0.2m)
                          }, 0.1m, 0.2m, 0.3m)
                      , 0.1m, 0.2m, 0.3m, 0.1m, 0.2m, 0.3m, 0.1m)
                , new ElectricitySubmissions(
                    new List<GhgBalance.Common.Data.Balance.ElectricitySubmission> {
                        new GhgBalance.Common.Data.Balance.ElectricitySubmission(123, new DateTime(2019, 1, 10), 23, 1.5m, 0.75m, 0.1m, 1.2m, 0.3m, 0.2m)
                    }, 0.1m, 0.2m, 0.3m)
                , new UpstreamEmissionsReductionSubmissions(
                    new List<GhgBalance.Common.Data.Balance.UpstreamEmissionsReductionSubmission> {
                        new GhgBalance.Common.Data.Balance.UpstreamEmissionsReductionSubmission(1, new DateTime(2019, 1, 1), "A", 0.1m, "B")
                    }, 0.1m)
            , 0.1m, 0.2m), It.IsAny<Trading>()
            , 0.2m, 0.3m, 0.7m, 1.2m, 5.3m);

            //arrange
            _mockBalanceRepository.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Task.FromResult<OrganisationBalance>(orgBalance));

            //act
            var result = await _service.GenerateExportForOrganisation(1, 15);

            //assert
            _mockBalanceRepository.Verify(s => s.Get(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            StringAssert.Contains(result, ("10/01/2019,RTFO Admin Consignments,Org name,1,2019,,23,int ref,type,feedstock,1.2,0.7,0.3,0.2,1.2,0.3,0.2\r\n"));
            StringAssert.Contains(result, ("10/01/2019,RTFO Fossil Fuels,Org name,1,2019,,,,type,,0.1,1.2,0.3,0.2,1.2,0.3,0.2\r\n"));
            StringAssert.Contains(result, ("10/01/2019,Fossil Gas,Org name,1,2019,123,23,,type,,0.1,1.2,0.3,0.2,1.2,0.3,0.2\r\n"));
            StringAssert.Contains(result, ("10/01/2019,Electricity,Org name,1,2019,123,23,,Electricity,,1.5,,0.75,0.1,1.2,0.3,0.2\r\n"));
            StringAssert.Contains(result, ("01/01/2019,UER,Org name,1,2019,1,,B,UER,,0.1,,,,,0.1,0\r\n"));
            Assert.IsNotNull(result);
        }

        #endregion GenerateExportForOrganisation Tests

        #region GenerateExportForAllOrganisations Tests

        /// <summary>
        /// Tests that the GenerateExportForAllOrganisations method correctly not being found in the repository
        /// </summary>
        [TestMethod]
        public async Task GenerateExportForAllOrganisations_Handles_Repository_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(r => r.GetAll(It.IsAny<int>()))
                .Returns(Task.FromResult<List<OrganisationBalance>>(null));

            //act
            var result = await _service.GenerateExportForAllOrganisations(15);

            //assert
            _mockBalanceRepository.Verify(s => s.GetAll(It.IsAny<int>()), Times.Once);
            Assert.IsNull(result);
        }

        /// <summary>
        /// Tests that the GenerateExportForAllOrganisations method correctly not being found in the repository
        /// </summary>
        [TestMethod]
        public async Task GenerateExportForAllOrganisations_Handles_Repository_Zero()
        {
            //arrange
            _mockBalanceRepository.Setup(r => r.GetAll(It.IsAny<int>()))
                .Returns(Task.FromResult(new List<OrganisationBalance>()));

            //act
            var result = await _service.GenerateExportForAllOrganisations(15);

            //assert
            _mockBalanceRepository.Verify(s => s.GetAll(It.IsAny<int>()), Times.Once);
            Assert.IsNull(result);
        }

        /// <summary>
        /// Tests that the GenerateExportForAllOrganisations method correctly generates CSV format
        /// </summary>
        [TestMethod]
        public async Task GenerateExportForAllOrganisations_Generates_Correct_CSV()
        {
            var orgBalance = new OrganisationBalance(It.IsAny<int>(), new DateTime(2019, 1, 1)
                , 1, "Org name"
                , new Submissions(new LiquidGasSubmissions(
                    new RtfoAdminConsignmentSubmissions(
                        new List<GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission> {
                            new GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission(new DateTime(2019, 1, 10), 23, "int ref", 10, "type", 5, "feedstock", 1.2m, 0.7m, 0.3m, 0.2m, 1.2m, 0.3m, 0.2m)
                        }, 0.1m, 0.2m, 0.3m)
                      , new RtfoVolumeSubmissions(
                          new List<GhgBalance.Common.Data.Balance.RtfoVolumeSubmission> {
                              new GhgBalance.Common.Data.Balance.RtfoVolumeSubmission(new DateTime(2019, 1, 10), 23, "type", 0.1m, 1.2m, 0.3m, 0.2m, 1.2m, 0.3m, 0.2m)
                          }, 0.1m, 0.2m, 0.3m)
                      , new FossilGasSubmissions(
                          new List<GhgBalance.Common.Data.Balance.FossilGasSubmission> {
                              new GhgBalance.Common.Data.Balance.FossilGasSubmission(123, new DateTime(2019, 1, 10), 23, 1, "type", 0.1m, 1.2m, 0.3m, 0.2m, 1.2m, 0.3m, 0.2m)
                          }, 0.1m, 0.2m, 0.3m)
                      , 0.1m, 0.2m, 0.3m, 0.1m, 0.2m, 0.3m, 0.1m)
                , new ElectricitySubmissions(
                    new List<GhgBalance.Common.Data.Balance.ElectricitySubmission> {
                        new GhgBalance.Common.Data.Balance.ElectricitySubmission(123, new DateTime(2019, 1, 10), 23, 1.5m, 0.75m, 0.1m, 1.2m, 0.3m, 0.2m)
                    }, 0.1m, 0.2m, 0.3m)
                , new UpstreamEmissionsReductionSubmissions(
                    new List<GhgBalance.Common.Data.Balance.UpstreamEmissionsReductionSubmission> {
                        new GhgBalance.Common.Data.Balance.UpstreamEmissionsReductionSubmission(1, new DateTime(2019, 1, 1), "A", 0.1m, "B")
                    }, 0.1m)
            , 0.1m, 0.2m), It.IsAny<Trading>()
            , 0.2m, 0.3m, 0.7m, 1.2m, 5.3m);


            List<OrganisationBalance> balances = new List<OrganisationBalance>();
            balances.Add(orgBalance);

            //arrange
            _mockBalanceRepository.Setup(r => r.GetAll(It.IsAny<int>()))
                .Returns(Task.FromResult<List<OrganisationBalance>>(balances));

            //act
            var result = await _service.GenerateExportForAllOrganisations(15);

            //assert
            _mockBalanceRepository.Verify(s => s.GetAll(It.IsAny<int>()), Times.Once);
            StringAssert.Contains(result, ("10/01/2019,RTFO Admin Consignments,Org name,1,2019,,23,int ref,type,feedstock,1.2,0.7,0.3,0.2,1.2,0.3,0.2\r\n"));
            StringAssert.Contains(result, ("10/01/2019,RTFO Fossil Fuels,Org name,1,2019,,,,type,,0.1,1.2,0.3,0.2,1.2,0.3,0.2\r\n"));
            StringAssert.Contains(result, ("10/01/2019,Fossil Gas,Org name,1,2019,123,23,,type,,0.1,1.2,0.3,0.2,1.2,0.3,0.2\r\n"));
            StringAssert.Contains(result, ("10/01/2019,Electricity,Org name,1,2019,123,23,,Electricity,,1.5,,0.75,0.1,1.2,0.3,0.2\r\n"));
            StringAssert.Contains(result, ("01/01/2019,UER,Org name,1,2019,1,,B,UER,,0.1,,,,,0.1,0\r\n"));
            Assert.IsNotNull(result);
        }

        #endregion GenerateExportForAllOrganisations Tests

        #region GetCreditTransfers Tests

        /// <summary>
        /// Tests that the GetCreditTransfers method correctly generates a result
        /// </summary>
        [TestMethod]
        public async Task GetCreditTransfers_Returns_Result()
        {
            //arrange
            _mockBalanceRepository.Setup(r => r.GetCreditTransfers(It.IsAny<int>(), "", It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Task.FromResult(new ResultSet<CreditTransferSummary>()));

            //act
            var result = await _service.GetCreditTransfers(15, "", 1, 5);
            //assert
            _mockBalanceRepository.Verify(s => s.GetCreditTransfers(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Tests that the GetCreditTransfers method correctly handles null response
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetCreditTransfers_Handles_Repository_Null()
        {
            //arrange
            _mockBalanceRepository.Setup(r => r.GetCreditTransfers(It.IsAny<int>(), "", It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Task.FromResult<ResultSet<CreditTransferSummary>>(null));

            //act
            var result = await _service.GetCreditTransfers(15, "", 1, 5);

            //assert
            _mockBalanceRepository.Verify(s => s.GetCreditTransfers(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsNull(result);
        }

        [TestMethod]
        public async Task GetCreditTransfers_Handles_Invalid_Page_Size_And_Number()
        {
            //arrange
            var defaultStartPage = 1;
            var defaultPageSize = 10;
            _mockBalanceRepository.Setup(r => r.GetCreditTransfers(It.IsAny<int>(), "", It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Task.FromResult(new ResultSet<CreditTransferSummary>()));

            //act
            await _service.GetCreditTransfers(15, "", 0, -1);

            //assert
            _mockBalanceRepository.Verify(s => s.GetCreditTransfers(It.IsAny<int>(), It.IsAny<string>(), defaultStartPage, defaultPageSize), Times.Once);
        }

        #endregion GetCreditTransfers Tests

        #endregion Tests
    }
}