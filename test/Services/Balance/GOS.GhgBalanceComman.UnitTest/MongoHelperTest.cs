using DfT.GOS.GhgBalance.Common.Data;
using DfT.GOS.GhgBalance.Common.Repositories;
using DfT.GOS.GhgBalance.Common.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using System.Collections.Generic;

namespace GOS.GhgBalanceCommon.UnitTest
{
    [TestClass]
    public class MongoHelperTest
    {
        #region private variables

        #endregion private variables

        #region Test Initialise

        [TestInitialize]
        public void TestInitialize()
        {
        }

        #endregion Test Initialise

        #region Mongo Pipeline Tests

        /// <summary>
        /// Tests that the Mongohelper Match returns a Bson Document correctly
        /// </summary>
        [TestMethod]
        public void Match_Builds_CorrectDocument()
        {
            var obligationPeriodProperty = "ObligationPeriodId";
            var obligationPeriodId = 5;

            var target = MongoHelper.Match(obligationPeriodProperty, obligationPeriodId);

            var expectedResult = new BsonDocument("$match", new BsonDocument()
                    .Add(obligationPeriodProperty, obligationPeriodId));

            Assert.IsTrue(BsonDocumentsAreEquivalent(target, expectedResult));
        }

        /// <summary>
        /// Tests that the Mongohelper Unwind returns a Bson Document correctly
        /// </summary>
        [TestMethod]
        public void Unwind_Builds_CorrectDocument()
        {
            var pathToTarget = "$Trading.CreditEvents";
            var preserveNullAndEmptyArrays = false;

            var target = MongoHelper.Unwind(pathToTarget, preserveNullAndEmptyArrays);

            var expectedResult = new BsonDocument("$unwind", new BsonDocument()
                    .Add("path", "$Trading.CreditEvents")
                    .Add("preserveNullAndEmptyArrays", new BsonBoolean(false)));

            Assert.IsTrue(BsonDocumentsAreEquivalent(target, expectedResult));
        }

        /// <summary>
        /// Tests that the Mongohelper Sort returns a Bson Document correctly
        /// </summary>
        [TestMethod]
        public void Sort_Builds_CorrectDocument()
        {
            var orderDescending = -1;

            var target = MongoHelper.Sort(
                        new KeyValuePair<string, int>("Date", orderDescending)
                        , new KeyValuePair<string, int>("Reference", orderDescending));

            var expectedResult = new BsonDocument("$sort", new BsonDocument()
                        .Add("Date", -1)
                        .Add("Reference", -1));

            Assert.IsTrue(BsonDocumentsAreEquivalent(target, expectedResult));
        }

        /// <summary>
        /// Tests that the Mongohelper FirstInEachGroup returns a Bson Document correctly
        /// </summary>
        [TestMethod]
        public void FirstInEachGroup_Builds_CorrectDocument()
        {
            var target = MongoHelper.FirstInEachGroup("$Trading.CreditEvents.Reference", "Reference");

            var expectedResult = new BsonDocument("$group", new BsonDocument() //Group by reference then take first element of grouping. Should be higher value of credits exchanged for one transaction
                    .Add("_id", new BsonDocument()
                        .Add("Reference", "$Trading.CreditEvents.Reference")
                    )
                    .Add("doc", new BsonDocument()
                        .Add("$first", "$$ROOT")
                    ));

            Assert.IsTrue(BsonDocumentsAreEquivalent(target, expectedResult));
        }

        /// <summary>
        /// Tests that the Mongohelper FirstInEachGroup returns a Bson Document correctly
        /// </summary>
        [TestMethod]
        public void Project_Builds_CorrectDocument()
        {
            var dontInclude = 0;

            var target = MongoHelper.Project(new Dictionary<string, BsonValue>()
            {
                ["_id"] = dontInclude,
                ["OrganisationName"] = "$doc.OrganisationName",
                ["OrganisationId"] = "$doc._id.OrganisationId"
            });

            var expectedResult = new BsonDocument("$project", new BsonDocument()
                    .Add("_id", 0)
                    .Add("OrganisationName", "$doc.OrganisationName")
                    .Add("OrganisationId", "$doc._id.OrganisationId"));

            Assert.IsTrue(BsonDocumentsAreEquivalent(target, expectedResult));
        }

        /// <summary>
        /// Tests that the Mongohelper FirstInEachGroup returns a Bson Document correctly
        /// </summary>
        [TestMethod]
        public void PageCountStages_Builds_CorrectDocument()
        {
            var skipRecords = 5;
            var pageSize = 5;

            var target = MongoHelper.PageCountStages(skipRecords, pageSize);

            var expectedResult = new BsonDocument[]{
                new BsonDocument("$facet", new BsonDocument() //Split to two pipelines, one for desired results, one for counting all results
                    .Add("Results", new BsonArray()
                        .Add(new BsonDocument()
                            .Add("$skip", skipRecords)
                        )
                        .Add(new BsonDocument()
                            .Add("$limit", pageSize)
                        )
                    )
                    .Add("Count", new BsonArray()
                        .Add(new BsonDocument()
                            .Add("$count", "count")
                        )
                    )),
                new BsonDocument("$replaceRoot", new BsonDocument() //Tidy up document, handle no results found
                    .Add("newRoot", new BsonDocument()
                        .Add("Count", new BsonDocument()
                            .Add("$cond", new BsonDocument()
                                .Add("if", new BsonDocument()
                                    .Add("$eq", new BsonArray()
                                        .Add(new BsonDocument()
                                            .Add("$size", "$Count")
                                        )
                                        .Add(0)
                                    )
                                )
                                .Add("then", new BsonDocument()
                                    .Add("$literal", 0)
                                )
                                .Add("else", new BsonDocument()
                                    .Add("$map", new BsonDocument()
                                        .Add("input", "$Count")
                                        .Add("as", "num")
                                        .Add("in", "$$num.count")
                                    )
                                )
                            )
                        )
                        .Add("Results", "$Results")
                )),
                new BsonDocument("$unwind", new BsonDocument()
                        .Add("path", "$Count"))
            };

            Assert.IsTrue(BsonDocumentsAreEquivalent(target, expectedResult));
        }

        #endregion Mongo Pipeline Tests

        #region Private methods

        private bool BsonDocumentsAreEquivalent(BsonDocument[] a, BsonDocument[] b)
        {
            return a.ToJson().Equals(b.ToJson());
        }

        private bool BsonDocumentsAreEquivalent(BsonDocument a, BsonDocument b)
        {
            return a.ToJson().Equals(b.ToJson());
        }

        #endregion Private methods
    }
}
