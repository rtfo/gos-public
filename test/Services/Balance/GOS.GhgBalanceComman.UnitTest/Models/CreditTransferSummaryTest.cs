﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

using DfT.GOS.GhgBalance.Common.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GOS.GhgBalanceCommon.UnitTest.Models
{
    /// <summary>
    /// Tests the CreditsTransferSummary model
    /// </summary>
    [TestClass]
    public class CreditTransferSummaryTest
    {
        /// <summary>
        /// Tests the ToString method of the CreditsTransferSummary class
        /// </summary>
        [TestMethod]
        public void CreditsTransferSummary_ToString()
        {
            //arrange
            var organisationName = "fakeName's";
            var date = new DateTime(1999, 5, 3);
            var tradingOrganistationName = "trading, name";
            var credits = 5000;
            var additionalReference = "123";
            var reference = 101;

            var expectedResult = $"{reference.ToString()},03/05/1999,{organisationName},\"{tradingOrganistationName}\",{additionalReference},{credits.ToString()}";

            var creditTransferSummary = new CreditTransferSummary(organisationName, 1, 2, reference, date, 4, "fakeEventType", 5, tradingOrganistationName, credits, additionalReference);

            //act
            var result = creditTransferSummary.ToString();

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedResult, result);
        }
    }
}
