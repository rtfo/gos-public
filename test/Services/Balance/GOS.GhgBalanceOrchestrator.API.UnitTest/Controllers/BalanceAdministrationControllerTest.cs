﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Administration;
using DfT.GOS.GhgBalanceOrchestrator.API.Exceptions;
using DfT.GOS.GhgBalanceOrchestrator.API.Services;
using DfT.GOS.Security.Claims;
using DfT.GOS.Web.Hosting;
using GOS.GhgBalanceOrchestrator.API.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.UnitTest.Controllers
{
    /// <summary>
    /// Unit tests for the balance administration controller
    /// </summary>
    [TestClass]
    public class BalanceAdministrationControllerTest
    {
        #region Properties

        private BalanceAdministrationController Controller { get; set; }
        private Mock<IBalanceService> BalanceService { get; set; }
        private Mock<IBackgroundTaskQueue> BackgroundTaskQueue { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialisation required before each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.BackgroundTaskQueue = new Mock<IBackgroundTaskQueue>(MockBehavior.Strict);
            this.BalanceService = new Mock<IBalanceService>(MockBehavior.Strict);
            this.Controller = new BalanceAdministrationController(this.BackgroundTaskQueue.Object, this.BalanceService.Object);
        }

        #endregion Initialisation

        #region Helper Methods

        /// <summary>
        /// Asserts that the given method is decorated with an Authorize attribute that requires a member of the Sys Admin role
        /// </summary>
        /// <param name="methodInfo">Method to check</param>
        private void RequiresSysAdmin(MethodInfo methodInfo)
        {
            //arrange

            //act
            var result = methodInfo.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Length);
            Assert.IsNotNull(result[0]);
            Assert.IsInstanceOfType(result[0], typeof(AuthorizeAttribute));
            Assert.AreEqual(Roles.Administrator, ((AuthorizeAttribute)result[0]).Roles);
        }

        #endregion Helper Methods

        #region Tests

        #region GetBulkUpdateStatus tests

        /// <summary>
        /// Tests that the GetBulkUpdateStatus method returns HTTP 200 (OK) if the given bulk update id exists
        /// </summary>
        [TestMethod]
        public async Task BalanceAdministrationController_GetBulkUpdateStatus_Returns_OK_When_CollectionAdministration_Exists()
        {
            //arrange
            var bulkUpdateUniqueId = Guid.Empty;
            var collectionAdmin = new CollectionAdministration(bulkUpdateUniqueId, DateTime.Now.AddMinutes(-2), DateTime.Now.AddMinutes(5));
            this.BalanceService.Setup(mock => mock.GetBulkUpdateStatus(bulkUpdateUniqueId))
                .ReturnsAsync(collectionAdmin);

            //act
            var result = await this.Controller.GetBulkUpdateStatus(bulkUpdateUniqueId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsNotNull(((OkObjectResult)result).Value);
            Assert.IsInstanceOfType(((OkObjectResult)result).Value, typeof(CollectionAdministration));
            Assert.AreEqual(collectionAdmin.Errors, ((CollectionAdministration)((OkObjectResult)result).Value).Errors);
            Assert.AreEqual(collectionAdmin.HasErrors, ((CollectionAdministration)((OkObjectResult)result).Value).HasErrors);
            Assert.AreEqual(collectionAdmin.HasTimedOut, ((CollectionAdministration)((OkObjectResult)result).Value).HasTimedOut);
            Assert.AreEqual(collectionAdmin.Id, ((CollectionAdministration)((OkObjectResult)result).Value).Id);
            Assert.AreEqual(collectionAdmin.LoadStarted, ((CollectionAdministration)((OkObjectResult)result).Value).LoadStarted);
            Assert.AreEqual(collectionAdmin.LoadSuccessfullyCompleted, ((CollectionAdministration)((OkObjectResult)result).Value).LoadSuccessfullyCompleted);
            Assert.AreEqual(collectionAdmin.Status, ((CollectionAdministration)((OkObjectResult)result).Value).Status);
            Assert.AreEqual(collectionAdmin.StatusDescription, ((CollectionAdministration)((OkObjectResult)result).Value).StatusDescription);
            Assert.AreEqual(collectionAdmin.TimeoutCancellation, ((CollectionAdministration)((OkObjectResult)result).Value).TimeoutCancellation);
        }

        /// <summary>
        /// Tests that the GetBulkUpdateStatus method returns HTTP 404 (NotFound) if the given bulk update id does not exist
        /// </summary>
        [TestMethod]
        public async Task BalanceAdministrationController_GetBulkUpdateStatus_Returns_NotFound_When_CollectionAdministration_Does_Not_Exist()
        {
            //arrange
            var bulkUpdateUniqueId = Guid.Empty;
            var collectionAdmin = new CollectionAdministration(bulkUpdateUniqueId, DateTime.Now.AddMinutes(-2), DateTime.Now.AddMinutes(5));
            this.BalanceService.Setup(mock => mock.GetBulkUpdateStatus(bulkUpdateUniqueId))
                .ReturnsAsync((CollectionAdministration)null);

            //act
            var result = await this.Controller.GetBulkUpdateStatus(bulkUpdateUniqueId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        /// <summary>
        /// Tests that the GetBulkUpdateStatus method is only available to users in the Admin role
        /// </summary>
        [TestMethod]
        public void BalanceAdministrationController_GetBulkUpdateStatus_Requires_Admin_Permissions()
        {
            //arrange

            //act
            var methodInfo = this.Controller.GetType().GetMethod("GetBulkUpdateStatus");

            //assert
            RequiresSysAdmin(methodInfo);
        }

        #endregion GetBulkUpdateStatus tests

        #region UpdateCollection tests

        /// <summary>
        /// Tests that the UpdateCollection method returns HTTP 400 (Bad Reqeust) if invalid arguments are detected by the service
        /// </summary>
        [TestMethod]
        public async Task BalanceAdministrationController_UpdateCollection_Returns_Bad_Request_On_Service_Argument_Exception()
        {
            //arrange
            var organisationId = 250;
            var obligationPeriodId = 14;
            this.BalanceService.Setup(mock => mock.UpdateGhgBalance(organisationId, obligationPeriodId))
                .Throws(new ArgumentException("Fake Arg exception", "fake param name"));

            //act
            var result = await this.Controller.UpdateCollection(organisationId, obligationPeriodId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        /// <summary>
        /// Tests that the UpdateCollection method returns HTTP 200 (OK) if the update completes sucessfully
        /// </summary>
        [TestMethod]
        public async Task BalanceAdministrationController_UpdateCollection_Returns_OK_When_Bulk_Update_Completes_Successfully()
        {
            //arrange
            var organisationId = 250;
            var obligationPeriodId = 14;
            this.BalanceService.Setup(mock => mock.UpdateGhgBalance(organisationId, obligationPeriodId))
                .ReturnsAsync(true);

            //act
            var result = await this.Controller.UpdateCollection(organisationId, obligationPeriodId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }

        /// <summary>
        /// Tests that the UpdateCollection method throws an exception if the bulk update fails
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(BalanceOrchestrationException))]
        public async Task BalanceAdministrationController_UpdateCollection_Throws_Exception_When_Bulk_Update_Fails()
        {
            //arrange
            var organisationId = 250;
            var obligationPeriodId = 14;
            this.BalanceService.Setup(mock => mock.UpdateGhgBalance(organisationId, obligationPeriodId))
                .ReturnsAsync(false);

            //act
            var result = await this.Controller.UpdateCollection(organisationId, obligationPeriodId);

            //assert
        }

        /// <summary>
        /// Tests that the UpdateCollection method is only available to users in the Admin role
        /// </summary>
        [TestMethod]
        public void BalanceAdministrationController_UpdateCollection_Requires_Admin_Permissions()
        {
            //arrange

            //act
            var methodInfo = this.Controller.GetType().GetMethod("UpdateCollection");

            //assert
            RequiresSysAdmin(methodInfo);
        }

        #endregion UpdateCollection tests

        #region UpdateCollections tests

        /// <summary>
        /// Tests that the UpdateCollections method throws an exception if the bulk update fails
        /// </summary>
        [TestMethod]
        public async Task BalanceAdministrationController_UpdateCollections_Returns_Accepted_When_Request_Queued()
        {
            //arrange
            var collectionAdmin = new CollectionAdministration(Guid.Empty, DateTime.Now.AddMinutes(-2), DateTime.Now.AddMinutes(5));
            this.BalanceService.Setup(mock => mock.CreateCollectionAdministration())
                .ReturnsAsync(collectionAdmin);
            this.BackgroundTaskQueue.Setup(mock => mock.QueueBackgroundWorkItem(It.IsAny<Func<CancellationToken, Task>>()))
                .Verifiable();              

            //act
            var result = await this.Controller.UpdateCollections();

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(AcceptedResult));
            Assert.IsNotNull(((AcceptedResult)result).Value);
            Assert.IsInstanceOfType(((AcceptedResult)result).Value, typeof(Guid));
            Assert.AreEqual(collectionAdmin.Id, ((AcceptedResult)result).Value);
            this.BackgroundTaskQueue.Verify();
        }

        /// <summary>
        /// Tests that the UpdateCollections method is only available to users in the Admin role
        /// </summary>
        [TestMethod]
        public void BalanceAdministrationController_UpdateCollections_Requires_Admin_Permissions()
        {
            //arrange

            //act
            var methodInfo = this.Controller.GetType().GetMethod("UpdateCollections");

            //assert
            RequiresSysAdmin(methodInfo);
        }

        #endregion UpdateCollections tests

        #endregion Tests
    }
}
