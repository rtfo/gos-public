﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalanceOrchestrator.API.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using DfT.GOS.GhgCalculations.Orchestration;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.GhgLedger.API.Client.Models;
using DfT.GOS.GhgLedger.API.Client.Services;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.RtfoApplications.API.Client.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DfT.GOS.GhgCalculations.Orchestration.ApplicationsCalculationOrchestrator;

namespace DfT.GOS.GhgBalanceOrchestrator.API.UnitTest.Services
{
    /// <summary>
    /// Unit tests for the Balance Calculation Service
    /// </summary>
    [TestClass]
    public class BalanceCalculationServiceTest
    {
        #region Constants

        private int FakeAdminConsignmentId = 2;
        private int FakeElectricityApplicationItemId = 3;
        private int FakeFossilGasApplicationItemId = 6;
        private int FakeOrganisationId = 20;
        private int FakeObligationPeriodId = 13;
        private decimal NumberOfCreditsToAwardPerVolume = 50;
        private decimal ObligationPerAdminConsignment = 500;
        private decimal TotalCreditsWithAdjustments = 50000;
        private decimal TotalObligationWithAdjustments = 60000;
        private decimal FakeBuyoutPoundsPer_tCO2e = 10;

        #endregion Constants

        #region Properties

        private Mock<IApplicationsService> MockApplicationsService { get; set; }
        private Mock<ILedgerService> MockLedgerService { get; set; }
        private Mock<ICalculationsService> MockCalculationsService { get; set; }
        private Mock<IOrganisationsService> MockOrganisationService { get; set; }
        private IBalanceCalculationService BalanceCalculationService { get; set; }
        private CalculationCommand LastCalculationCommandPassed { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Initialises the service / dependencies before each test
        /// </summary>
        [TestInitialize]
        public void TestInitialise()
        {
            this.MockApplicationsService = new Mock<IApplicationsService>();
            this.MockLedgerService = new Mock<ILedgerService>();
            this.MockCalculationsService = new Mock<ICalculationsService>();
            this.MockOrganisationService = new Mock<IOrganisationsService>();
            this.BalanceCalculationService = new BalanceCalculationService(this.MockLedgerService.Object, this.MockApplicationsService.Object, this.MockOrganisationService.Object);
        }

        /// <summary>
        /// Gets a fake Obligation Period to test with
        /// </summary>
        /// <returns></returns>
        private ObligationPeriod GetFakeObligationPeriod()
        {
            var obligationPeriod = new ObligationPeriod(FakeObligationPeriodId
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , true
                , DateTime.Now
                , 5000
                , 10000
                , null
                , null
                , null
                , 50
                , 200
                , FakeBuyoutPoundsPer_tCO2e);

            return obligationPeriod;
        }

        /// <summary>
        /// Gets a fake Supplier for testing with
        /// </summary>
        private Organisation GetFakeSupplier()
        {
            var supplier = new Organisation(FakeOrganisationId, "Fake Org Name", 1, "Fake Status", 1, "Fake Type");
            return supplier;
        }

        /// <summary>
        /// Gets a list of fake fuel types for testing with
        /// </summary>
        private IList<FuelType> GetFakeFuelTypes()
        {
            var fuelTypes = new List<FuelType>();
            // - Fake Electricity
            fuelTypes.Add(new FuelType(1, (int)FuelCategories.Electricity, null, "Fake Electricity", null, null));
            return fuelTypes;
        }

        /// <summary>
        /// Gets a list of fake admin consignments
        /// </summary>
        private IEnumerable<AdminConsignment> GetFakeAdminConsignments()
        {
            var adminConsignments = new List<AdminConsignment>();
            adminConsignments.Add(new AdminConsignment() { AdminConsignmentId = FakeAdminConsignmentId });
            return adminConsignments;
        }

        /// <summary>
        /// Gets a list of fake volumes
        /// </summary>
        private IEnumerable<NonRenewableVolume> GetFakeVolumes()
        {
            var volumes = new List<NonRenewableVolume>();
            volumes.Add(new NonRenewableVolume()
            {
                OrganisationId = FakeOrganisationId,
                ObligationPeriodId = FakeObligationPeriodId,
                FuelTypeId = 33,
                FuelTypeName = "Fake Fuel Type",
                CarbonIntensity = 50,
                EnergyDensity = 20,
                Volume = 5000,
                ReportedMonthEndDate = DateTime.Now.Date
            });

            return volumes;
        }

        /// <summary>
        /// Sets up Application Items mock
        /// </summary>
        private void SetupMockApplicationItems()
        {
            var applicationItems = new List<ApplicationItemSummary>();
            applicationItems.Add(new ApplicationItemSummary(1, FakeElectricityApplicationItemId, 1, null, 5000, 50, 20));
            applicationItems.Add(new ApplicationItemSummary(1, FakeFossilGasApplicationItemId, 1, null, 2000, 50, 20));

            this.MockApplicationsService.Setup(s => s.GetApplicationItemsWithIssuedCredits(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IList<ApplicationItemSummary>>(applicationItems));
        }

        /// <summary>
        /// Sets up Applications mock
        /// </summary>
        private void SetupMockApplications()
        {
            var application = new ApplicationSummary(1, 1, 13);

            this.MockApplicationsService.Setup(s => s.GetApplicationSummary(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<ApplicationSummary>(application));
        }

        /// <summary>
        /// Sets up mock calculation service call
        /// </summary>
        private void SetupMockCalculation(IEnumerable<AdminConsignment> adminConsignments)
        {
            // - Admin Consignments
            var adminConsignmentCalculationItemResults = new List<VolumeCalculationItemResult>();

            foreach (var adminConsignment in adminConsignments)
            {
                adminConsignmentCalculationItemResults.Add(new VolumeCalculationItemResult(adminConsignment.AdminConsignmentId.ToString()
                    , ApplicationsCalculationOrchestrator.VolumeFuelCategory_AdminConsignment
                    , It.IsAny<string>()
                    , It.IsAny<decimal>()
                    , null
                    , null
                    , It.IsAny<decimal>()
                    , It.IsAny<decimal>()
                    , ObligationPerAdminConsignment
                    , It.IsAny<decimal>()));
            }

            // - Volumes
            var volumeCalculation = new VolumeCalculationResult(adminConsignmentCalculationItemResults, It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());

            foreach (var volume in this.GetFakeVolumes())
            {
                var volumeKey = new VolumeCompositeKey(volume.OrganisationId, volume.ObligationPeriodId, volume.ReportedMonthEndDate, volume.FuelTypeId);

                volumeCalculation.Items.Add(new VolumeCalculationItemResult(volumeKey.CompositeKey
                    , ApplicationsCalculationOrchestrator.VolumeFuelCategory_Volume
                    , It.IsAny<string>()
                    , volume.Volume
                    , null
                    , It.IsAny<decimal>()
                    , It.IsAny<decimal>()
                    , NumberOfCreditsToAwardPerVolume // Add a fixed credit for each volume
                    , It.IsAny<decimal>()
                    , It.IsAny<decimal>()));
            }

            // - Fossil Gas
            volumeCalculation.Items.Add(new VolumeCalculationItemResult(FakeFossilGasApplicationItemId.ToString(), ApplicationsCalculationOrchestrator.VolumeFuelCategory_FossilGas, It.IsAny<string>(), It.IsAny<decimal>(), null, null, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()));

            // - Electricity
            var energyCalculationItemResults = new List<EnergyCalculationItemResult>();
            energyCalculationItemResults.Add(new EnergyCalculationItemResult(FakeElectricityApplicationItemId.ToString(), It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>()));
            var energyCalculation = new EnergyCalculationResult(energyCalculationItemResults, It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<decimal>());


            // - UERs
            var emissionsReductionCalculationItemResults = new List<EmissionsReductionCalculationItemResult>();
            var emissionsReductionsCalculation = new EmissionsReductionCalculationResult(emissionsReductionCalculationItemResults, It.IsAny<decimal>(), It.IsAny<decimal>());

            var fakeCalculationResult = new CalculationResult(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<decimal>(), volumeCalculation, energyCalculation, emissionsReductionsCalculation, 200, TotalCreditsWithAdjustments, It.IsAny<decimal>(), TotalObligationWithAdjustments);
            this.MockCalculationsService.Setup(s => s.GhgCalculationBreakdown(It.IsAny<CalculationCommand>()))
                .Returns(Task.FromResult(fakeCalculationResult))                
                .Callback<CalculationCommand>((command) => this.LastCalculationCommandPassed = command);
        }

        /// <summary>
        /// Sets up mock ledger service calls
        /// </summary>
        private void SetupMockLedger()
        {
            var transactions = new List<TransactionSummary>();

            this.MockLedgerService.Setup(s => s.Get(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IEnumerable<TransactionSummary>>(transactions));
        }

        #endregion Methods

        #region Tests

        /// <summary>
        /// Tests the CalculateGhgBalance method's happy path
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CalculateGhgBalance_Success()
        {
            //arrange
            var fakeJwtToken = "Fake JWT Token";

            var applicationsCalculationOrchestrator = new ApplicationsCalculationOrchestrator(this.MockApplicationsService.Object
                , this.MockCalculationsService.Object
                , this.GetFakeFuelTypes()
                , fakeJwtToken);

            this.SetupMockApplicationItems();
            this.SetupMockApplications();
            var adminConsignments = this.GetFakeAdminConsignments();
            this.SetupMockCalculation(adminConsignments);
            this.SetupMockLedger();

            var expectedVolumeCredits = this.GetFakeVolumes().Count() * NumberOfCreditsToAwardPerVolume;
            var expectedAdminConsignmentObligation = this.GetFakeAdminConsignments().Count() * ObligationPerAdminConsignment;
            var expectedNetBalance = TotalCreditsWithAdjustments - TotalObligationWithAdjustments;
            var expectedBuyoutCost = (expectedNetBalance / 1000) * (FakeBuyoutPoundsPer_tCO2e * 1000 / 100) * -1;

            //act
            var result = await this.BalanceCalculationService.CalculateGhgBalance(this.GetFakeObligationPeriod()
                , this.GetFakeSupplier()
                , this.GetFakeFuelTypes()
                , adminConsignments
                , this.GetFakeVolumes()
                , applicationsCalculationOrchestrator
                , fakeJwtToken);

            //assert
            Assert.IsInstanceOfType(result, typeof(OrganisationBalance));
            Assert.AreEqual(TotalCreditsWithAdjustments, result.TotalCredits);
            Assert.AreEqual(TotalObligationWithAdjustments, result.TotalObligation);
            Assert.AreEqual(expectedNetBalance, result.NetBalance);
            Assert.AreEqual(expectedBuyoutCost, result.BuyOutCost);
            Assert.AreEqual(FakeObligationPeriodId, result.ObligationPeriodId);
            Assert.AreEqual(FakeOrganisationId, result.OrganisationId);
        }        

        /// <summary>
        /// Tests that the CalculateGhgBalance method filters out non-renewable volumes with no volume prior to sending to the calculation
        /// </summary>
        [TestMethod]
        public async Task CalculateGhgBalance_Filters_Out_Zero_Non_Renewable_Volumes()
        {
            //arrange
            var fakeJwtToken = "Fake JWT Token";

            var applicationsCalculationOrchestrator = new ApplicationsCalculationOrchestrator(this.MockApplicationsService.Object
                , this.MockCalculationsService.Object
                , this.GetFakeFuelTypes()
                , fakeJwtToken);

            this.SetupMockApplicationItems();
            this.SetupMockApplications();
            var adminConsignments = new List<AdminConsignment>();
            this.SetupMockCalculation(adminConsignments);
            this.SetupMockLedger();

            var volumes = new List<NonRenewableVolume>();
            var expectedVolume = 5000;

            // - add a volume with a non-zero value
            volumes.Add(new NonRenewableVolume()
            {
                OrganisationId = FakeOrganisationId,
                ObligationPeriodId = FakeObligationPeriodId,
                FuelTypeId = 33,
                FuelTypeName = "Fake Fuel Type",
                CarbonIntensity = 50,
                EnergyDensity = 20,
                Volume = expectedVolume,
                ReportedMonthEndDate = DateTime.Now.Date
            });

            // - add a volume with zero volume value
            volumes.Add(new NonRenewableVolume()
            {
                OrganisationId = FakeOrganisationId,
                ObligationPeriodId = FakeObligationPeriodId,
                FuelTypeId = 34,
                FuelTypeName = "Another Fake Fuel Type",
                CarbonIntensity = 20,
                EnergyDensity = 30,
                Volume = 0,
                ReportedMonthEndDate = DateTime.Now.Date
            });


            //act
            await this.BalanceCalculationService.CalculateGhgBalance(this.GetFakeObligationPeriod()
                , this.GetFakeSupplier()
                , this.GetFakeFuelTypes()
                , adminConsignments
                , volumes
                , applicationsCalculationOrchestrator
                , fakeJwtToken);

            //assert
            // - only one volume was passed to the calculation service
            Assert.AreEqual(1, this.LastCalculationCommandPassed.VolumeItems.Count);
            // - the one that was passed was the one with a non-zero volume
            Assert.AreEqual(expectedVolume, this.LastCalculationCommandPassed.VolumeItems[0].Volume);
        }        

        #endregion Tests
    }
}
