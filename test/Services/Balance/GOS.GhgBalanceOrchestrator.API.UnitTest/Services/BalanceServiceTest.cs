﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgBalance.Common.Data.Administration;
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalance.Common.Repositories;
using DfT.GOS.GhgBalanceOrchestrator.API.Exceptions;
using DfT.GOS.GhgBalanceOrchestrator.API.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgCalculations.Orchestration;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.RtfoApplications.API.Client.Models;
using DfT.GOS.RtfoApplications.API.Client.Services;
using DfT.GOS.UnitTest.Common.Attributes;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.UnitTest.Services
{
    /// <summary>
    /// Provides unit tests for the balance service
    /// </summary>
    [TestClass]
    public class BalanceServiceTest
    {
        #region Private Constants

        private const string ValidServiceAuthenticationId = "ValidServiceAuthenticationId";

        #endregion Private Constants

        #region Properties

        private IBalanceService BalanceService { get; set; }
        private Mock<IOptions<AppSettings>> MockAppSettings { get; set; }
        private Mock<ILogger<Startup>> MockLogger { get; set; }
        private Mock<IBalanceCollectionAdministrationRepository> MockBalanceCollectionAdministrationRepository { get; set; }
        private Mock<IBalanceRepositoryFactory> MockTemporaryBalanceRepositoryFactory { get; set; }
        private Mock<IReportingPeriodsService> MockReportingPeriodsService { get; set; }
        private Mock<IOrganisationsService> MockOrganisationsService { get; set; }
        private Mock<IGhgFuelsService> MockGhgFuelsService { get; set; }
        private Mock<IIdentityService> MockIdentityService { get; set; }
        private Mock<IApplicationsService> MockApplicationsService { get; set; }
        private Mock<ICalculationsService> MockCalculationsService { get; set; }
        private Mock<IAdminConsignmentsService> MockAdminConsignmentsService { get; set; }
        private Mock<IVolumesService> MockVolumesService { get; set; }
        private Mock<IBalanceCalculationService> MockBalanceCalculationService { get; set; }
        private AppSettings AppSettings { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Initialises the service / dependancies before each test
        /// </summary>
        [TestInitialize]
        public void TestInitialise()
        {
            this.MockAppSettings = new Mock<IOptions<AppSettings>>();
            this.MockLogger = new Mock<ILogger<Startup>>();
            this.MockBalanceCollectionAdministrationRepository = new Mock<IBalanceCollectionAdministrationRepository>();
            this.MockTemporaryBalanceRepositoryFactory = new Mock<IBalanceRepositoryFactory>();
            this.MockReportingPeriodsService = new Mock<IReportingPeriodsService>();
            this.MockOrganisationsService = new Mock<IOrganisationsService>();
            this.MockGhgFuelsService = new Mock<IGhgFuelsService>();
            this.MockIdentityService = new Mock<IIdentityService>();
            this.MockApplicationsService = new Mock<IApplicationsService>();
            this.MockCalculationsService = new Mock<ICalculationsService>();
            this.MockAdminConsignmentsService = new Mock<IAdminConsignmentsService>();
            this.MockVolumesService = new Mock<IVolumesService>();
            this.MockBalanceCalculationService = new Mock<IBalanceCalculationService>();

            // Setup Authentication
            this.AppSettings = new AppSettings();
            this.AppSettings.SERVICE_IDENTIFIER = ValidServiceAuthenticationId;
            this.MockAppSettings.Setup(s => s.Value).Returns(this.AppSettings);
            this.MockIdentityService.Setup(s => s.AuthenticateService(ValidServiceAuthenticationId)).ReturnsAsync(new AuthenticationResult() { Succeeded = true, Token = "Fake Token" });

            //Setup the service
            this.BalanceService = new BalanceService(this.MockAppSettings.Object
                , this.MockLogger.Object
                , this.MockBalanceCollectionAdministrationRepository.Object
                , this.MockTemporaryBalanceRepositoryFactory.Object
                , this.MockReportingPeriodsService.Object
                , this.MockOrganisationsService.Object
                , this.MockGhgFuelsService.Object
                , this.MockIdentityService.Object
                , this.MockApplicationsService.Object
                , this.MockCalculationsService.Object
                , this.MockAdminConsignmentsService.Object
                , this.MockVolumesService.Object
                , this.MockBalanceCalculationService.Object);
        }

        /// <summary>
        /// Sets up mock Fuel Types
        /// </summary>
        private void SetupMockFuelTypes()
        {
            this.MockGhgFuelsService.Setup(s => s.GetFuelTypes()).ReturnsAsync(new List<FuelType>());
        }

        /// <summary>
        /// Sets up a mock obligation period
        /// </summary>
        private void SetupMockObligationPeriods()
        {
            var obligationPeriod = new ObligationPeriod(13
                , DateTime.Now.AddDays(-1)
                , DateTime.Now.AddDays(1)
                , true
                , DateTime.Now
                , 5000
                , 10000
                , null
                , null
                , null
                , 50
                , 200
                , 70);

            var obligationPeriods = new List<ObligationPeriod>(0);
            obligationPeriods.Add(obligationPeriod);

            this.MockReportingPeriodsService.Setup(s => s.GetObligationPeriod(It.IsAny<int>())).ReturnsAsync(obligationPeriod);
            this.MockReportingPeriodsService.Setup(s => s.GetObligationPeriods()).ReturnsAsync(obligationPeriods);
        }

        /// <summary>
        /// Sets up a mock organisation
        /// </summary>
        private void SetupMockOrganisations()
        {
            var supplier = new Organisation(20, "Fake Org Name", 1, "Fake Status", 1, "Fake Type");

            this.MockOrganisationsService.Setup(s => s.GetOrganisation(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(supplier));

            var suppliers = new List<Organisation>();
            suppliers.Add(supplier);
            this.MockOrganisationsService.Setup(s => s.GetSuppliersAndTraders(It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IList<Organisation>>(suppliers));
        }

        /// <summary>
        /// Sets up mock admin consignment data
        /// </summary>
        private void SetupMockAdminConsignments()
        {
            var adminConsignments = new List<AdminConsignment>();

            this.MockAdminConsignmentsService.Setup(s => s.Get(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IEnumerable<AdminConsignment>>(adminConsignments));

            this.MockAdminConsignmentsService.Setup(s => s.Get(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IEnumerable<AdminConsignment>>(adminConsignments));
        }

        /// <summary>
        /// Sets up mock volume data
        /// </summary>
        private void SetupMockVolumes()
        {
            var volumes = new List<NonRenewableVolume>();

            this.MockVolumesService.Setup(s => s.Get(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IEnumerable<NonRenewableVolume>>(volumes));

            this.MockVolumesService.Setup(s => s.Get(It.IsAny<int>(), It.IsAny<string>()))
               .ReturnsAsync(new HttpObjectResponse<IEnumerable<NonRenewableVolume>>(volumes));
        }

        /// <summary>
        /// Sets up a mock balance calculation
        /// </summary>
        private void SetupMockBalanceCalculation()
        {
            var organisationBalance = new OrganisationBalance();

            this.MockBalanceCalculationService.Setup(s => s.CalculateGhgBalance(It.IsAny<ObligationPeriod>()
                , It.IsAny<Organisation>()
                , It.IsAny<IList<FuelType>>()
                , It.IsAny<IEnumerable<AdminConsignment>>()
                , It.IsAny<IEnumerable<NonRenewableVolume>>()
                , It.IsAny<ApplicationsCalculationOrchestrator>()
                , It.IsAny<string>())).ReturnsAsync(organisationBalance);
        }

        #endregion Methods

        #region Tests

        #region CreateCollectionAdministration Tests

        /// <summary>
        /// Tests that the CreateCollectionAdministration method creates a new CollectionAdministration object
        /// </summary>
        [TestMethod]
        public async Task BalanceService_CreateCollectionAdministration_Creates_A_New_CollectionAdministration_Record()
        {
            //arrange
            var appSettings = new AppSettings() { BulkUpdateTimeoutMinutes = 5 };
            this.MockBalanceCollectionAdministrationRepository.Setup(mock => mock.Get(It.IsAny<Guid>()))
                .ReturnsAsync((CollectionAdministration)null);
            this.MockBalanceCollectionAdministrationRepository.Setup(mock => mock.Create(It.IsAny<CollectionAdministration>()))
                .ReturnsAsync(true);
            this.MockAppSettings.Setup(mock => mock.Value)
                .Returns(appSettings);

            //act
            var result = await this.BalanceService.CreateCollectionAdministration();

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(0, result.Errors.Count);
            Assert.IsFalse(result.HasErrors);
            Assert.IsFalse(result.HasTimedOut);
            Assert.IsNull(result.LoadSuccessfullyCompleted);
            Assert.AreEqual(CollectionAdministrationStatus.InProgress, result.Status);
            Assert.AreEqual(CollectionAdministrationStatus.InProgress.ToString(), result.StatusDescription);
        }

        /// <summary>
        /// Tests that the CreateCollectionAdministration method throws an exception if it cannot generate a Guid that has not been used before
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(BalanceOrchestrationException), "The generated bulk update unique id, [a-z0-9-]{36}, has already been used.", true)]
        public async Task BalanceService_CreateCollectionAdministration_Throws_Exception_If_Guid_Already_User()
        {
            //arrange
            var appSettings = new AppSettings() { BulkUpdateTimeoutMinutes = 5 };
            this.MockBalanceCollectionAdministrationRepository.Setup(mock => mock.Get(It.IsAny<Guid>()))
                .ReturnsAsync(new CollectionAdministration(Guid.Empty, DateTime.Now.AddMinutes(-2), DateTime.Now.AddMinutes(5)));

            //act
            var result = await this.BalanceService.CreateCollectionAdministration();

            //assert
        }

        /// <summary>
        /// Tests that the CreateCollectionAdministration method throws an exception if it cannot create the CollectionAdministration record
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(BalanceOrchestrationException), "Unable to create a new balance administration record.")]
        public async Task BalanceService_CreateCollectionAdmistration_Throws_Exception_If_Create_CollectionAdministration_Record_Fails()
        {
            //arrange
            var appSettings = new AppSettings() { BulkUpdateTimeoutMinutes = 5 };
            this.MockBalanceCollectionAdministrationRepository.Setup(mock => mock.Get(It.IsAny<Guid>()))
                .ReturnsAsync((CollectionAdministration)null);
            this.MockBalanceCollectionAdministrationRepository.Setup(mock => mock.Create(It.IsAny<CollectionAdministration>()))
                .ReturnsAsync(false);
            this.MockAppSettings.Setup(mock => mock.Value)
                .Returns(appSettings);

            //act
            var result = await this.BalanceService.CreateCollectionAdministration();

            //assert
        }

        #endregion CreateCollectionAdministration Tests

        #region GetBulkUpdateStatus Tests

        /// <summary>
        /// Tests that the GetBulkUpdateStatus method calls the correct repository method
        /// </summary>
        [TestMethod]
        public async Task GetBulkUpdateStatus_Call_Repository_Method()
        {
            //arrange
            var bulkUpdateUniqueID = Guid.NewGuid();
            var collectionAdmin = new CollectionAdministration(bulkUpdateUniqueID, DateTime.Now, DateTime.Now.AddHours(1));
            this.MockBalanceCollectionAdministrationRepository.Setup(r => r.Get(bulkUpdateUniqueID)).ReturnsAsync(collectionAdmin);

            //act
            var result = await this.BalanceService.GetBulkUpdateStatus(bulkUpdateUniqueID);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(CollectionAdministration));
            this.MockBalanceCollectionAdministrationRepository.Verify(r => r.Get(It.IsAny<Guid>()), Times.Once);
        }

        #endregion GetBulkUpdateStatus Tests

        #region PurgeExpiredCollections Tests

        /// <summary>
        /// Tests that the PurgeExpiredCollection method truncates only expired collections
        /// </summary>
        [TestMethod]
        public async Task PurgeExpiredCollections_Truncates_Expired_Collections()
        {
            //arrange
            var expectedDeleteCount = 2;
            var collectionAdministrationRecords = new List<CollectionAdministration>();

            // - add two expired records
            collectionAdministrationRecords.Add(new CollectionAdministration(Guid.NewGuid(), DateTime.Now.AddHours(-2), DateTime.Now.AddHours(-1)));
            collectionAdministrationRecords.Add(new CollectionAdministration(Guid.NewGuid(), DateTime.Now.AddHours(-3), DateTime.Now.AddHours(-2)));
            // - add a non-expired record
            collectionAdministrationRecords.Add(new CollectionAdministration(Guid.NewGuid(), DateTime.Now, DateTime.Now.AddHours(1)));
            this.MockBalanceCollectionAdministrationRepository.Setup(r => r.GetTemporaryCollectionAdministrationRecords()).ReturnsAsync(collectionAdministrationRecords);

            var mockTemporaryBalanceRepository = new Mock<IBalanceRepository>();
            this.MockTemporaryBalanceRepositoryFactory.Setup(f => f.GetTemporaryRepository(It.IsAny<Guid>())).Returns(mockTemporaryBalanceRepository.Object);

            //act
            var result = await this.BalanceService.PurgeExpiredCollections();

            //assert
            // - it reported the correct number of collections deleted
            Assert.AreEqual(expectedDeleteCount, result);
            // - it called the repository method the correct number of times
            mockTemporaryBalanceRepository.Verify(r => r.Drop(), Times.Exactly(expectedDeleteCount));
            // - it logged the correct number of warnings
            this.MockLogger.Verify(l => l.Log(LogLevel.Warning,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((a,b) => true),
                It.IsAny<Exception>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Exactly(expectedDeleteCount));
        }

        /// <summary>
        /// Tests that the PurgeExpiredCollection handles null temporary collections being returned by the repository
        /// </summary>
        [TestMethod]
        public async Task PurgeExpiredCollections_Handles_Null_Collection_Records()
        {
            //arrange
            var expectedDeleteCount = 0;
            List<CollectionAdministration> collectionAdministrationRecords = null;
            this.MockBalanceCollectionAdministrationRepository.Setup(r => r.GetTemporaryCollectionAdministrationRecords()).ReturnsAsync(collectionAdministrationRecords);

            //act
            var result = await this.BalanceService.PurgeExpiredCollections();

            //assert
            Assert.AreEqual(expectedDeleteCount, result);
        }

        #endregion PurgeExpiredCollections Tests

        #region UpdateGhgBalance Tests

        /// <summary>
        /// Checks that an out of range exception is thrown for zero or negative organisation ID
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentOutOfRangeException), "organisationId", true)]
        public async Task UpdateGhgBalance_Checks_OutOfRange_OrganisationId()
        {
            //Arrange

            //Act
            await this.BalanceService.UpdateGhgBalance(0, 1);

            //Assert
        }

        /// <summary>
        /// Checks that an out of range exception is thrown for zero or negative obligation period ID
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentOutOfRangeException), "obligationPeriodId", true)]
        public async Task UpdateGhgBalance_Checks_OutOfRange_ObligationPeriodId()
        {
            //Arrange

            //Act
            await this.BalanceService.UpdateGhgBalance(1, 0);

            //Assert
        }

        /// <summary> 
        /// Tests that the UpdateGhgBalance method handles an invalid service account
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(BalanceOrchestrationAuthenticationException), "Unable to authenticate the balance orchestrator against the identity service.")]
        public async Task UpdateGhgBalance_Handles_Invalid_Service_Account_Auth_Request()
        {
            //arrange
            var invalidServiceId = "invalidServiceId";

            var appSettings = new AppSettings();
            appSettings.SERVICE_IDENTIFIER = "invalidServiceId";

            this.MockAppSettings.Setup(s => s.Value).Returns(appSettings);
            this.MockIdentityService.Setup(s => s.AuthenticateService(invalidServiceId)).ReturnsAsync(new AuthenticationResult() { Succeeded = false });

            //act
            await this.BalanceService.UpdateGhgBalance(1, 1);

            //assert
        }

        /// <summary>
        /// Tests that UpdateGhgBalance method handles null Fuel Types being returned by the Fuels service
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(BalanceOrchestrationException), "No fuel types found")]
        public async Task UpdateGhgBalance_Handles_Fuel_Type_API_Null_Error()
        {
            //arrange
            IList<FuelType> fuelTypes = null;
            this.MockGhgFuelsService.Setup(s => s.GetFuelTypes()).ReturnsAsync(fuelTypes);

            //act
            await this.BalanceService.UpdateGhgBalance(1, 1);

            //assert
        }

        /// <summary>
        /// Tests that UpdateGhgBalance method handles null Obligation Period
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentException), "No obligation period found with ID 13", true)]
        public async Task UpdateGhgBalance_Handles_Null_Obligation_Period()
        {
            //arrange
            var obligationPeriodId = 13;
            this.SetupMockFuelTypes();

            //act
            await this.BalanceService.UpdateGhgBalance(1, obligationPeriodId);

            //assert
        }

        /// <summary>
        /// Tests that UpdateGhgBalance method handles null organisation response
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(ArgumentException), "No organisation found with ID 20", true)]
        public async Task UpdateGhgBalance_Handles_Null_Organisation()
        {
            //arrange
            var organisationId = 20;
            this.SetupMockFuelTypes();
            this.SetupMockObligationPeriods();

            this.MockOrganisationsService.Setup(s => s.GetOrganisation(organisationId, It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<Organisation>(HttpStatusCode.NotFound));

            //act
            await this.BalanceService.UpdateGhgBalance(organisationId, 1);

            //assert
        }

        /// <summary>
        /// Tests that UpdateGhgBalance method handles failed admin consignment response
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(BalanceOrchestrationException), "No admin consignment data could be retrieved.")]
        public async Task UpdateGhgBalance_Handles_failed_admin_consignment_response()
        {
            //arrange
            this.SetupMockFuelTypes();
            this.SetupMockObligationPeriods();
            this.SetupMockOrganisations();

            this.MockAdminConsignmentsService.Setup(s => s.Get(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IEnumerable<AdminConsignment>>(HttpStatusCode.NotFound));

            //act
            await this.BalanceService.UpdateGhgBalance(1, 1);

            //assert
        }

        /// <summary>
        /// Tests that UpdateGhgBalance method handles failed volume response
        /// </summary>
        [TestMethod]
        [ExpectedExceptionMessage(typeof(BalanceOrchestrationException), "No volume data could be retrieved.")]
        public async Task UpdateGhgBalance_Handles_failed_volume_response()
        {
            //arrange
            this.SetupMockFuelTypes();
            this.SetupMockObligationPeriods();
            this.SetupMockOrganisations();
            this.SetupMockAdminConsignments();

            this.MockVolumesService.Setup(s => s.Get(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IEnumerable<NonRenewableVolume>>(HttpStatusCode.NotFound));

            //act
            await this.BalanceService.UpdateGhgBalance(1, 1);

            //assert
        }

        /// <summary>
        /// Tests that UpdateGhgBalance method happy path
        /// </summary>
        [TestMethod]
        public async Task UpdateGhgBalance_Success()
        {
            //arrange
            this.SetupMockFuelTypes();
            this.SetupMockObligationPeriods();
            this.SetupMockOrganisations();
            this.SetupMockAdminConsignments();
            this.SetupMockVolumes();
            this.SetupMockBalanceCalculation();

            // - setup the Master Collection
            var mockMasterCollectionRepository = new Mock<IBalanceRepository>();
            this.MockTemporaryBalanceRepositoryFactory.Setup(s => s.GetMasterRepository()).Returns(mockMasterCollectionRepository.Object);
            mockMasterCollectionRepository.Setup(r => r.Save(It.IsAny<OrganisationBalance>())).ReturnsAsync(true);

            // - Setup the temporary collections
            var adminCollectionRecords = new List<CollectionAdministration>();
            adminCollectionRecords.Add(new CollectionAdministration(Guid.NewGuid(), DateTime.Now, DateTime.Now.AddHours(1)));
            adminCollectionRecords.Add(new CollectionAdministration(Guid.NewGuid(), DateTime.Now, DateTime.Now.AddHours(1)));

            this.MockBalanceCollectionAdministrationRepository.Setup(r => r.GetTemporaryCollectionAdministrationRecords())
                .ReturnsAsync(adminCollectionRecords);

            var mockTemporaryCollectionRepository = new Mock<IBalanceRepository>();
            this.MockTemporaryBalanceRepositoryFactory.Setup(s => s.GetTemporaryRepository(It.IsAny<Guid>())).Returns(mockTemporaryCollectionRepository.Object);

            //act
            var result = await this.BalanceService.UpdateGhgBalance(1, 1);

            //assert
            Assert.IsTrue(result);
            // - Master Repository save called correct number of times
            mockMasterCollectionRepository.Verify(r => r.Save(It.IsAny<OrganisationBalance>()), Times.Once);
            // - Temporary Repository called correct number of times
            mockTemporaryCollectionRepository.Verify(r => r.Save(It.IsAny<OrganisationBalance>()), Times.Exactly(adminCollectionRecords.Count));
        }

        #endregion UpdateGhgBalance Tests

        #region UpdateAllGhgBalances Tests

        /// <summary>
        /// Tests that UpdateAllGhgBalances method handles null Fuel Types being returned by the Fuels service
        /// </summary>
        [TestMethod]
        public async Task UpdateAllGhgBalances_Handles_Fuel_Type_API_Null_Error()
        {
            //Arrange
            IList<FuelType> fuelTypes = null;
            this.MockGhgFuelsService.Setup(s => s.GetFuelTypes()).ReturnsAsync(fuelTypes);

            var tempBalanceRepository = new Mock<IBalanceRepository>();
            this.MockTemporaryBalanceRepositoryFactory.Setup(f => f.GetTemporaryRepository(It.IsAny<Guid>())).Returns(tempBalanceRepository.Object);

            var collectionAdmin = new CollectionAdministration(Guid.NewGuid(), DateTime.Now, DateTime.Now.AddHours(1));

            //Act
            await this.BalanceService.UpdateAllGhgBalances(collectionAdmin);

            //Assert
            // - errors were reported
            Assert.IsTrue(collectionAdmin.HasErrors);
            // - the admin record was updated
            this.MockBalanceCollectionAdministrationRepository.Verify(r => r.Update(It.IsAny<CollectionAdministration>()), Times.Once);
            // - the collection was deleted
            tempBalanceRepository.Verify(r => r.Drop(), Times.Once);
            // - update to master was not called
            tempBalanceRepository.Verify(r => r.UpdateToMaster(), Times.Never);

            //Verify an error was logged indicating an error
            this.MockLogger.Verify(l => l.Log(LogLevel.Error,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((a, b) => true),
                It.IsAny<BalanceOrchestrationException>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
        }

        /// <summary>
        /// Tests that UpdateAllGhgBalances method handles a bad authentication result from the identity service
        /// </summary>
        [TestMethod]
        public async Task UpdateAllGhgBalances_Handles_Bad_Authentication_Result()
        {
            //Arrange
            this.SetupMockFuelTypes();

            // - setup invalid authentication result
            var invalidServiceId = "invalidServiceId";
            var appSettings = new AppSettings();
            appSettings.SERVICE_IDENTIFIER = "invalidServiceId";

            this.MockAppSettings.Setup(s => s.Value).Returns(appSettings);
            this.MockIdentityService.Setup(s => s.AuthenticateService(invalidServiceId)).ReturnsAsync(new AuthenticationResult() { Succeeded = false });

            // - setup balance repository
            var tempBalanceRepository = new Mock<IBalanceRepository>();
            this.MockTemporaryBalanceRepositoryFactory.Setup(f => f.GetTemporaryRepository(It.IsAny<Guid>())).Returns(tempBalanceRepository.Object);

            var collectionAdmin = new CollectionAdministration(Guid.NewGuid(), DateTime.Now, DateTime.Now.AddHours(1));

            //Act
            await this.BalanceService.UpdateAllGhgBalances(collectionAdmin);

            //Assert
            // - errors were reported
            Assert.IsTrue(collectionAdmin.HasErrors);
            // - the admin record was updated
            this.MockBalanceCollectionAdministrationRepository.Verify(r => r.Update(It.IsAny<CollectionAdministration>()), Times.Once);
            // - the collection was deleted
            tempBalanceRepository.Verify(r => r.Drop(), Times.Once);
            // - update to master was not called
            tempBalanceRepository.Verify(r => r.UpdateToMaster(), Times.Never);

            //Verify an error was logged indicating an authentication error
            this.MockLogger.Verify(l => l.Log(LogLevel.Error,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((a, b) => true),
                It.IsAny<BalanceOrchestrationAuthenticationException>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
        }

        /// <summary>
        /// Tests that UpdateAllGhgBalances method handles issues connecting to the admin consignment service
        /// </summary>
        [TestMethod]
        public async Task UpdateAllGhgBalances_Handles_Admin_Consignment_Service_Error()
        {
            //Arrange
            this.SetupMockFuelTypes();
            this.SetupMockObligationPeriods();

            var tempBalanceRepository = new Mock<IBalanceRepository>();
            this.MockTemporaryBalanceRepositoryFactory.Setup(f => f.GetTemporaryRepository(It.IsAny<Guid>())).Returns(tempBalanceRepository.Object);

            // - setup an issue with the admin consignment service
            this.MockAdminConsignmentsService.Setup(s => s.Get(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IEnumerable<AdminConsignment>>(HttpStatusCode.NotFound));

            //Act
            var collectionAdmin = new CollectionAdministration(Guid.NewGuid(), DateTime.Now, DateTime.Now.AddHours(1));
            await this.BalanceService.UpdateAllGhgBalances(collectionAdmin);

            //Assert
            // - errors were reported
            Assert.IsTrue(collectionAdmin.HasErrors);
            // - the admin record was updated
            this.MockBalanceCollectionAdministrationRepository.Verify(r => r.Update(It.IsAny<CollectionAdministration>()), Times.Once);
            // - the collection was deleted
            tempBalanceRepository.Verify(r => r.Drop(), Times.Once);
            // - update to master was not called
            tempBalanceRepository.Verify(r => r.UpdateToMaster(), Times.Never);

            //Verify an error was logged indicating an error
            this.MockLogger.Verify(l => l.Log(LogLevel.Error,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((a, b) => true),
                It.IsAny<BalanceOrchestrationException>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
        }

        /// <summary>
        /// Tests that UpdateAllGhgBalances method handles issues connecting to the volumes service
        /// </summary>
        [TestMethod]
        public async Task UpdateAllGhgBalances_Handles_Volumes_Service_Error()
        {
            //Arrange
            this.SetupMockFuelTypes();
            this.SetupMockObligationPeriods();
            this.SetupMockAdminConsignments();

            var tempBalanceRepository = new Mock<IBalanceRepository>();
            this.MockTemporaryBalanceRepositoryFactory.Setup(f => f.GetTemporaryRepository(It.IsAny<Guid>())).Returns(tempBalanceRepository.Object);

            // - setup an issue with the volumes service
            this.MockVolumesService.Setup(s => s.Get(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new HttpObjectResponse<IEnumerable<NonRenewableVolume>>(HttpStatusCode.NotFound));

            //Act
            var collectionAdmin = new CollectionAdministration(Guid.NewGuid(), DateTime.Now, DateTime.Now.AddHours(1));
            await this.BalanceService.UpdateAllGhgBalances(collectionAdmin);

            //Assert
            // - errors were reported
            Assert.IsTrue(collectionAdmin.HasErrors);
            // - the admin record was updated
            this.MockBalanceCollectionAdministrationRepository.Verify(r => r.Update(It.IsAny<CollectionAdministration>()), Times.Once);
            // - the collection was deleted
            tempBalanceRepository.Verify(r => r.Drop(), Times.Once);
            // - update to master was not called
            tempBalanceRepository.Verify(r => r.UpdateToMaster(), Times.Never);

            //Verify an error was logged indicating an error
            this.MockLogger.Verify(l => l.Log(LogLevel.Error,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((a, b) => true),
                It.IsAny<BalanceOrchestrationException>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
        }

        /// <summary>
        /// Tests that the update all method handles any exceptions thrown when updating the collection
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateAllGhgBalances_Handles_Errors_When_Updating_Collection()
        {
            //Arrange
            this.SetupMockFuelTypes();
            this.SetupMockObligationPeriods();
            this.SetupMockAdminConsignments();
            this.SetupMockVolumes();
            this.SetupMockOrganisations();

            var tempBalanceRepository = new Mock<IBalanceRepository>();
            this.MockTemporaryBalanceRepositoryFactory.Setup(f => f.GetTemporaryRepository(It.IsAny<Guid>())).Returns(tempBalanceRepository.Object);

            // - Get the balance repository to throw and exception to test failure
            tempBalanceRepository.Setup(b => b.Save(It.IsAny<OrganisationBalance>())).Throws(new Exception("Some exception thrown"));

            //Act
            var collectionAdmin = new CollectionAdministration(Guid.NewGuid(), DateTime.Now, DateTime.Now.AddHours(1));
            await this.BalanceService.UpdateAllGhgBalances(collectionAdmin);

            //Assert
            // - errors were reported
            Assert.IsTrue(collectionAdmin.HasErrors);
            // - the admin record was updated
            this.MockBalanceCollectionAdministrationRepository.Verify(r => r.Update(It.IsAny<CollectionAdministration>()), Times.Once);
            // - the collection was deleted
            tempBalanceRepository.Verify(r => r.Drop(), Times.Once);
            // - update to master was not called
            tempBalanceRepository.Verify(r => r.UpdateToMaster(), Times.Never);

            //Verify an error was logged indicating an error
            this.MockLogger.Verify(l => l.Log(LogLevel.Error,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((a, b) => true),
                It.IsAny<BalanceOrchestrationException>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
        }

        /// <summary>
        /// Tests that the UpdateAllGhgBalances disregards expired temporary collections, rather than swapping them into master
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateAllGhgBalances_Disregards_Expired_Temporary_Collections()
        {
            //Arrange
            this.SetupMockFuelTypes();
            this.SetupMockObligationPeriods();
            this.SetupMockAdminConsignments();
            this.SetupMockVolumes();
            this.SetupMockOrganisations();

            var tempBalanceRepository = new Mock<IBalanceRepository>();
            this.MockTemporaryBalanceRepositoryFactory.Setup(f => f.GetTemporaryRepository(It.IsAny<Guid>())).Returns(tempBalanceRepository.Object);

            //Act
            var collectionAdmin = new CollectionAdministration(Guid.NewGuid(), DateTime.Now.AddHours(-2), DateTime.Now.AddHours(-1));
            await this.BalanceService.UpdateAllGhgBalances(collectionAdmin);

            //Assert
            // - errors were reported
            Assert.IsTrue(collectionAdmin.HasErrors);
            Assert.IsTrue(collectionAdmin.HasTimedOut);
            Assert.AreEqual(CollectionAdministrationStatus.Failed, collectionAdmin.Status);
            // - the admin record was updated
            this.MockBalanceCollectionAdministrationRepository.Verify(r => r.Update(It.IsAny<CollectionAdministration>()), Times.Once);
            // - the collection was deleted
            tempBalanceRepository.Verify(r => r.Drop(), Times.Once);
            // - update to master was not called
            tempBalanceRepository.Verify(r => r.UpdateToMaster(), Times.Never);

            //Verify an error was logged indicating an error
            this.MockLogger.Verify(l => l.Log(LogLevel.Error,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((a, b) => true),
                It.IsAny<BalanceOrchestrationException>(),
                It.Is<Func<object, Exception, string>>((a, b) => true)), Times.Once);
        }

        /// <summary>
        /// Tests that the UpdateAllGhgBalances happy path
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateAllGhgBalances_Success()
        {
            //Arrange
            this.SetupMockFuelTypes();
            this.SetupMockObligationPeriods();
            this.SetupMockAdminConsignments();
            this.SetupMockVolumes();
            this.SetupMockOrganisations();

            var tempBalanceRepository = new Mock<IBalanceRepository>();
            tempBalanceRepository.Setup(r => r.UpdateToMaster()).ReturnsAsync(true);

            // - Setup the update to master method to return a success status
            this.MockTemporaryBalanceRepositoryFactory.Setup(f => f.GetTemporaryRepository(It.IsAny<Guid>())).Returns(tempBalanceRepository.Object);
            // - setup the balance collection repository update method to return a success result
            this.MockBalanceCollectionAdministrationRepository.Setup(r => r.Update(It.IsAny<CollectionAdministration>())).ReturnsAsync(true);

            //Act
            var collectionAdmin = new CollectionAdministration(Guid.NewGuid(), DateTime.Now, DateTime.Now.AddHours(1));
            await this.BalanceService.UpdateAllGhgBalances(collectionAdmin);

            //Assert
            // - no errors were reported
            Assert.IsFalse(collectionAdmin.HasErrors);
            Assert.AreEqual(CollectionAdministrationStatus.Succeeded, collectionAdmin.Status);
            // - the admin record was updated
            this.MockBalanceCollectionAdministrationRepository.Verify(r => r.Update(It.IsAny<CollectionAdministration>()), Times.Once);
            // - update to master was called
            tempBalanceRepository.Verify(r => r.UpdateToMaster(), Times.Once);
        }

        #endregion UpdateAllGhgBalances Tests

        #endregion Tests
    }
}
