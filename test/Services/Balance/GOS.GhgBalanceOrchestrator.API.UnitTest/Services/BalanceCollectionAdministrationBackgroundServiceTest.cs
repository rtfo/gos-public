﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceOrchestrator.API.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.UnitTest.Services
{
    [TestClass]
    public class BalanceCollectionAdministrationBackgroundServiceTest
    {
        #region Properties

        private BalanceCollectionAdministrationBackgroundService BackgroundService { get; set; }
        private Mock<IBalanceService> BalanceService { get; set; }
        private Mock<ILogger<Startup>> Logger { get; set; }

        #endregion Properties

        #region Initialisation

        [TestInitialize]
        public void Initialize()
        {
            var appSettings = new Mock<IOptions<AppSettings>>();
            appSettings.Setup(mock => mock.Value)
                .Returns(new AppSettings() { BalanceCollectionAdministrationBackgroundServiceDelayMinutes = 0 });
            this.Logger = new Mock<ILogger<Startup>>();
            this.BalanceService = new Mock<IBalanceService>();

            // Setup the service provider to inject a mock BalanceService into the background service
            var serviceProvider = new Mock<IServiceProvider>();
            serviceProvider.Setup(p => p.GetService(typeof(IBalanceService)))
                .Returns(this.BalanceService.Object);
            var serviceScope = new Mock<IServiceScope>();
            serviceScope.Setup(s => s.ServiceProvider).Returns(serviceProvider.Object);
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            serviceScopeFactory.Setup(f => f.CreateScope())
                .Returns(serviceScope.Object);
            serviceProvider.Setup(p => p.GetService(typeof(IServiceScopeFactory)))
                .Returns(serviceScopeFactory.Object);

            this.BackgroundService = new BalanceCollectionAdministrationBackgroundService(this.Logger.Object, appSettings.Object, serviceProvider.Object);
        }

        #endregion Initialisation

        #region Tests

        #region ExecuteAsync Tests

        /// <summary>
        /// Tests that the ExecuteAsync method purges expired collections
        /// </summary>
        [TestMethod]
        public void BalanceCollectionAdministrationBackgroundService_ExecuteAsync_Purges_Expired_Collections()
        {
            //arrange
            this.BalanceService.Setup(mock => mock.PurgeExpiredCollections())
                .ReturnsAsync(1)
                .Verifiable();

            //act
            var backgroundTask = Task.Run(() => this.BackgroundService.StartAsync(new CancellationToken(false)));
            Thread.Sleep(500);
            this.BackgroundService.StopAsync(new CancellationToken(true));

            //assert
            this.BalanceService.Verify();
            Assert.IsNull(backgroundTask.Exception);
        }

        /// <summary>
        /// Tests that the ExecuteAsync method handles exceptions that occur when purging expired collections (we want the process to continue)
        /// </summary>
        [TestMethod]
        public void BalanceCollectionAdministrationBackgroundService_ExecuteAsync_Handles_Exceptions()
        {
            //arrange
            this.BalanceService.Setup(mock => mock.PurgeExpiredCollections())
                .ThrowsAsync(new Exception("test exception"))
                .Verifiable();

            //act
            var backgroundTask = Task.Run(() => this.BackgroundService.StartAsync(new CancellationToken(false)));
            Thread.Sleep(500);
            this.BackgroundService.StopAsync(new CancellationToken(true));

            //assert
            this.BalanceService.Verify();
            Assert.IsNull(backgroundTask.Exception);
        }

        #endregion ExecuteAsync Tests

        #endregion Tests
    }
}
