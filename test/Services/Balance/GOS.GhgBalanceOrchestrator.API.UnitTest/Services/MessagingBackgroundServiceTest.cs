﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceOrchestrator.API.Services;
using DfT.GOS.HealthChecks.Rabbit;
using DfT.GOS.Messaging.Client;
using DfT.GOS.Messaging.Messages;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.UnitTest.Services
{
    [TestClass]
    public class MessagingBackgroundServiceTest
    {
        #region Properties

        private Mock<IMessagingClient> MessagingClient { get; set; }
        private MessagingBackgroundService BackgroundService { get; set; }

        #endregion Properties

        #region Initialisation

        /// <summary>
        /// Performs initialisation for each unit test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.MessagingClient = new Mock<IMessagingClient>();
            var logger = new Mock<ILogger<MessagingBackgroundService>>();
            var healthCheck = new MessagingBackgroundServiceHealthCheck();
            var balanceService = new Mock<IBalanceService>();

            // Setup the service provider to inject a mock BalanceService into the background service
            var serviceProvider = new Mock<IServiceProvider>();
            serviceProvider.Setup(p => p.GetService(typeof(IBalanceService)))
                .Returns(balanceService.Object);
            var serviceScope = new Mock<IServiceScope>();
            serviceScope.Setup(s => s.ServiceProvider).Returns(serviceProvider.Object);
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            serviceScopeFactory.Setup(f => f.CreateScope())
                .Returns(serviceScope.Object);
            serviceProvider.Setup(p => p.GetService(typeof(IServiceScopeFactory)))
                .Returns(serviceScopeFactory.Object);

            this.BackgroundService = new MessagingBackgroundService(this.MessagingClient.Object, healthCheck, logger.Object, serviceProvider.Object);
        }

        #endregion Initialisation

        #region Tests

        /// <summary>
        /// Tests that the Messaging Background Service configures a subscriber for Bulk Update Balances messages
        /// </summary>
        [TestMethod]
        public void MessagingBackgroundService_ExecuteAsync_Configures_Subscriber_For_BulkUpdateBalancesMessage()
        {
            //arrange
            this.MessagingClient.Setup(mock => mock.Subscribe(It.IsAny<Func<RosUpdatedMessage, Task<bool>>>()))
                .Returns(Task.CompletedTask)
                .Verifiable();

            //act
            var backgroundTask = Task.Run(() => this.BackgroundService.StartAsync(new CancellationToken(false)));
            Thread.Sleep(500);
            this.BackgroundService.StopAsync(new CancellationToken(true));

            //assert
            this.MessagingClient.Verify();
            Assert.IsNull(backgroundTask.Exception);
        }

        /// <summary>
        /// Tests that the Messaging Background Service configures a subscriber for Update Balance messages
        /// </summary>
        [TestMethod]
        public void MessagingBackgroundService_ExecuteAsync_Configures_Subscriber_For_UpdateBalanceMessage()
        {
            //arrange
            this.MessagingClient.Setup(mock => mock.Subscribe(It.IsAny<Func<LedgerUpdatedMessage, Task<bool>>>()))
                .Returns(Task.CompletedTask)
                .Verifiable();

            //act
            var backgroundTask = Task.Run(() => this.BackgroundService.StartAsync(new CancellationToken(false)));
            Thread.Sleep(500);
            this.BackgroundService.StopAsync(new CancellationToken(true));

            //assert
            this.MessagingClient.Verify();
            Assert.IsNull(backgroundTask.Exception);
        }

        /// <summary>
        /// Tests that the Messaging Background Service configures a subscriber for Revoke Applications messages
        /// </summary>
        [TestMethod]
        public void MessagingBackgroundService_ExecuteAsync_Configures_Subscriber_For_RevokeApplicationMessage()
        {
            //arrange
            this.MessagingClient.Setup(mock => mock.Subscribe(It.IsAny<Func<ApplicationRevokedMessage, Task<bool>>>()))
                .Returns(Task.CompletedTask)
                .Verifiable();

            //act
            var backgroundTask = Task.Run(() => this.BackgroundService.StartAsync(new CancellationToken(false)));
            Thread.Sleep(500);
            this.BackgroundService.StopAsync(new CancellationToken(true));

            //assert
            this.MessagingClient.Verify();
            Assert.IsNull(backgroundTask.Exception);
        }

        /// <summary>
        /// Tests that the Messaging Background Service tries if there are errors connecting to Rabbit MQ
        /// </summary>
        [TestMethod]
        public async Task MessagingBackgroundService_ExecuteAsync_Retries_On_Error()
        {
            //arrange
            // Number of times subscribe should be called. 
            // (One for each message being subscribed to, multiplied by 3 as we're mocking two failures and a success in the following setup)
            var expectedInvocations = 12;
            this.MessagingClient.SetupSequence(mock => mock.Subscribe(It.IsAny<Func<RosUpdatedMessage, Task<bool>>>()))
                .Returns(Task.FromException(new Exception("subscriber1 exception")))
                .Returns(Task.FromException(new Exception("subscriber1 exception")))
                .Returns(Task.CompletedTask);

            this.MessagingClient.SetupSequence(mock => mock.Subscribe(It.IsAny<Func<LedgerUpdatedMessage, Task<bool>>>()))
                .Returns(Task.FromException(new Exception("subscriber2 exception")))
                .Returns(Task.FromException(new Exception("subscriber2 exception")))
                .Returns(Task.CompletedTask);

            this.MessagingClient.SetupSequence(mock => mock.Subscribe(It.IsAny<Func<CreditsIssuedMessage, Task<bool>>>()))
                .Returns(Task.FromException(new Exception("subscriber3 exception")))
                .Returns(Task.FromException(new Exception("subscriber3 exception")))
                .Returns(Task.CompletedTask);

            this.MessagingClient.SetupSequence(mock => mock.Subscribe(It.IsAny<Func<ApplicationRevokedMessage, Task<bool>>>()))
                .Returns(Task.FromException(new Exception("subscriber4 exception")))
                .Returns(Task.FromException(new Exception("subscriber4 exception")))
                .Returns(Task.CompletedTask);

            var tokenSource = new CancellationTokenSource();

            //act
            var backgroundTask = Task.Run(() => this.BackgroundService.StartAsync(tokenSource.Token));
            Thread.Sleep(500);
            await this.BackgroundService.StopAsync(tokenSource.Token);

            //assert
            Assert.AreEqual(expectedInvocations, this.MessagingClient.Invocations.Count(i => i.Method.Name == "Subscribe"));
            Assert.IsNull(backgroundTask.Exception);
        }

        /// <summary>
        /// Tests that the Messaging Background Service can be shut down gracefully if there are errors connecting to Rabbit MQ
        /// </summary>
        [TestMethod]
        public async Task MessagingBackgroundService_ExecuteAsync_Allows_Graceful_Shutdown()
        {
            //arrange
            this.MessagingClient.Setup(mock => mock.Subscribe(It.IsAny<Func<RosUpdatedMessage, Task<bool>>>()))
                .Returns(Task.FromException(new Exception("subscriber1 exception")));

            this.MessagingClient.Setup(mock => mock.Subscribe(It.IsAny<Func<LedgerUpdatedMessage, Task<bool>>>()))
                .Returns(Task.FromException(new Exception("subscriber2 exception")));

            this.MessagingClient.Setup(mock => mock.Subscribe(It.IsAny<Func<CreditsIssuedMessage, Task<bool>>>()))
                .Returns(Task.FromException(new Exception("subscriber3 exception")));

            var tokenSource = new CancellationTokenSource();

            //act
            var backgroundTask = Task.Run(() => this.BackgroundService.StartAsync(tokenSource.Token));
            Thread.Sleep(500);
            await this.BackgroundService.StopAsync(tokenSource.Token);

            //assert
            Assert.IsNull(backgroundTask.Exception);
        }

        #endregion Tests
    }
}
