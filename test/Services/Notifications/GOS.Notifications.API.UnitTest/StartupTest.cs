// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace DfT.GOS.Notifications.API.UnitTest
{
    /// <summary>
    /// Tests the service's Startup class
    /// </summary>
    [TestClass]
    public class StartupTest
    {
        #region Tests
        /// <summary>
        /// Tests that the Service Identifier variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_validates_Service_Identifier_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_ServiceIdentifier);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_ServiceIdentifierNotSupplied, ex.Message);
        }

        /// <summary>
        /// Tests that the JWTTokenAudience variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_validates_JWTTokenAudience_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_JWTTokenAudience);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_JWTTokenAudience, ex.Message);
        }

        /// <summary>
        /// Tests that the JWTTokenIssuer variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_validates_JWTTokenIssuer_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_JWTTokenIssuer);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_JWTTokenIssuer, ex.Message);
        }

        /// <summary>
        /// Tests that the JWTTokenKey variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_validates_JWTTokenKey_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_JWTTokenKey);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_JWTTokenKey, ex.Message);
        }

        /// <summary>
        /// Tests that the Messaging Client User Name variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_validates_MessagingClient_Username_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_MessagingClientUsername);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_MessagingClientUsername, ex.Message);
        }

        /// <summary>
        /// Tests that the Messaging Client Password variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_validates_MessagingClient_Password_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_MessagingClientPassword);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_MessagingClientPassword, ex.Message);
        }

        /// <summary>
        /// Tests that the GOV Notify URL variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_validates_Gov_Notify_URL_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovNotifyURL);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_GovNotifyURLNotSupplied, ex.Message);
        }

        /// <summary>
        /// Tests that the GOV Notify API key variable validation is called
        /// </summary>
        [TestMethod]
        public void Startup_validates_Gov_Notify_API_key_is_present()
        {
            //arrange
            var configurationItems = this.GetAllConfigurationItems();
            configurationItems.Remove(Startup.EnvironmentVariable_GovNotifyAPIKey);
            var startup = GetStartupInstance(configurationItems);

            //act and assert
            var ex = Assert.ThrowsException<NullReferenceException>(() => startup.ConfigureServices(new ServiceCollection()));
            Assert.AreEqual(Startup.ErrorMessage_GovNotifyAPIKeyNotSupplied, ex.Message);
        }
        #endregion Tests

        #region Private Methods
        /// <summary>
        /// Gets a dictionary of all configuration settings for this service
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetAllConfigurationItems()
        {
            var configurationItems = new Dictionary<string, string>();
            configurationItems.Add(Startup.EnvironmentVariable_ServiceIdentifier, "fake service identifier");
            configurationItems.Add(Startup.EnvironmentVariable_JWTTokenAudience, "fake JWTTokenAudience");
            configurationItems.Add(Startup.EnvironmentVariable_JWTTokenIssuer, "fake JWTTokenIssuer");
            configurationItems.Add(Startup.EnvironmentVariable_JWTTokenKey, "fake JWTTokenKey");
            configurationItems.Add(Startup.EnvironmentVariable_MessagingClientUsername, "fake username");
            configurationItems.Add(Startup.EnvironmentVariable_MessagingClientPassword, "fake password");
            configurationItems.Add(Startup.EnvironmentVariable_GovNotifyURL, "fake gov notify URL");
            configurationItems.Add(Startup.EnvironmentVariable_GovNotifyAPIKey, "fake gov notify API key");
            return configurationItems;
        }

        /// <summary>
        /// Gets an instance of the startup class for testing
        /// </summary>
        /// <returns></returns>
        private Startup GetStartupInstance(IDictionary<string, string> configurationItems)
        {
            var configuration = new ConfigurationBuilder();
            var mockConfiguration = new Mock<IConfiguration>();

            foreach (var configurationItem in configurationItems)
            {
                mockConfiguration.SetupGet(c => c[configurationItem.Key]).Returns(configurationItem.Value);
            }

            var mockLogger = new Mock<ILogger<Startup>>();
            return new Startup(mockConfiguration.Object, mockLogger.Object);
        }

        #endregion Private Methods
    }
}
