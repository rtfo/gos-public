
///////////////////////////////////////////////////////////
//
// Jenkins Job to create a new release
// It does a full build, and tags the images with the latest
// git tag. Tags the configuration repository, and zips up
// the configuration before publishing it to a gcloud bucket
// 
///////////////////////////////////////////////////////////

def VERSION = 'latest'

def getUpdatedVersion(VERSION){
	def split = VERSION.split('\\.')
	split[2]= Integer.parseInt(split[2]) + 1
	return split.join('.')
}

pipeline {
	agent {
		label 'docker_new'
	}
	
	stages {
		stage('Get version') {
			steps{
				dir('gos'){
					script {
						VERSION = sh(returnStdout:  true, script: "git describe --abbrev=0 --tags").trim()
					}
				}
			}
		}
		stage('Package Release'){
			parallel {
				stage('Build and Tag')
				{
					environment {
						MAIN_REPO_PUSH = credentials('ROSGOS-Bitbucket-Push')
					}
					steps {
						dir('gos'){
							// Trigger the Build job and specify to build everything
							build job: 'RTFO.GOS Build', parameters:[[$class: 'BooleanParameterValue', name: 'BUILDALL', value: true]], wait: true
							build job: 'RTFO.GOS Tag Release', parameters:[[$class: 'StringParameterValue', name: 'RELEASE_TAG', value: VERSION]], wait: true
							
							sh(script:"echo ${getUpdatedVersion(VERSION)} > ./APPLICATION_VERSION")
							sh(script:"cat ./APPLICATION_VERSION")
							sh(script:"git add ./APPLICATION_VERSION")
							sh(script:"git commit -m \"update next application version\"")
							
							sh '''
							    git branch version-branch
							    git checkout master
							    git merge version-branch
							'''
							sh(script:"git push ${MAIN_REPO_PUSH}")
						}
					}
				}
				stage('Package and Release')
				{
					environment {
						MAIN_CONFIG_REPO = credentials('RTFO_GOS_CONFIGURATION_REPO_URL')
						MAIN_CONFIG_REPO_PUSH = credentials('ROSGOS-Config-Bitbucket')
					}
					steps {
						dir('../gos-configuration') {
							git url: "${MAIN_CONFIG_REPO}", credentialsId: "346c4743-d78e-484d-8998-f17a2f90f5a9"
							sh(script: "mkdir -p ProductionDeployment/Kubernetes/BaseDeployment/secrets/ && cp -r k8s/secrets/ ProductionDeployment/Kubernetes/BaseDeployment/")
							sh(script: "mkdir -p ProductionDeployment/Kubernetes/BaseDeployment/deployments/ && cp -r k8s/deployments/ ProductionDeployment/Kubernetes/BaseDeployment/")
							sh(script: "zip -r ProductionDeployment-${VERSION}.zip ProductionDeployment")
							sh(script: "gsutil cp ProductionDeployment-${VERSION}.zip gs://rtfo-gos-releases-triad/")
							sh(script: "git tag ${VERSION}")
							sh(script: "git push ${MAIN_CONFIG_REPO_PUSH} ${VERSION}")
						}
					}
				}
			}
		}
		stage('Clean up') {
			steps {
				deleteDir()
			}
		}
	}
}