UPDATE "GhgCreditAdverts" 
SET
	"EmailAddress" = CASE WHEN "EmailAddress" IS NOT NULL THEN 'anonymised@anon.com' END, 
	"FullName" = CASE WHEN "FullName" IS NOT NULL THEN 'anonymised' END, 
	"PhoneNumber" = CASE WHEN "PhoneNumber" IS NOT NULL THEN 'anonymised' END 
where 
   "AdvertiseToBuy" = 'TRUE' 
or "AdvertiseToSell" = 'TRUE';