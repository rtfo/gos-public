DROP ROLE IF EXISTS cloudsqladmin;
DROP ROLE IF EXISTS cloudsqlsuperuser;
DROP ROLE IF EXISTS proxyuser;