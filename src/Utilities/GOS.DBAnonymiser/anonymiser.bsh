#!/bin/bash

LOCAL_POSTGRES_DATABASE=postgres
LOCAL_SERVER=localhost
LOCAL_USER=postgres


function usage()
{
        echo
        echo Usage
        echo
        echo $0 -u username -s server -p password -e environment
        echo
        echo e.g $0 -u proxyuser -s 192.168.0.1 -p something -e rtfocluster-integration
        exit 1
}

if [ $# == 0 ]; then
    usage
fi


POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
       -u)
        USER="$2"
        shift
        shift
        ;;
        -s)
        SERVER="$2"
        shift
        shift
        ;;
        -p)
        export PGPASSWORD="$2"
        shift
        shift
        ;;
        -e)
        ENVIRONMENT="$2"
        shift
        shift
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        echo Unknown option $1
        usage
        shift # past argument
        ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


#######################
##
##  Start the postgres server
##  and wait for it to be ready
##
#######################
echo "*** Starting up local postgres server (silently) ***"
. /docker-entrypoint.sh postgres > /dev/null 2>&1 &


# Wait for postgres
while ( ! pg_isready > /dev/null 2>&1 ); do sleep 3; echo "*** Waiting for Postgres.. ***"; done;
echo "*** Postgres ready! ***"

#######################
##
##   Dump each of the
##   4 CloudSQL databases
##
#######################
LEDGER=ghgledger-api
MARKETPLACE=ghgmarketplace-api
APPLICATIONS=ghgapplications-api
IDENTITY=identity-api

echo "*** Dumping $ENVIRONMENT-$LEDGER ***"
pg_dump --format=c -f $LEDGER  -U $USER -w -h $SERVER $ENVIRONMENT-$LEDGER
echo "*** Dumping $ENVIRONMENT-$MARKETPLACE ***"
pg_dump --format=c -f $MARKETPLACE -U $USER -w -h $SERVER $ENVIRONMENT-$MARKETPLACE
echo "*** Dumping $ENVIRONMENT-$APPLICATIONS ***"
pg_dump --format=c -f $APPLICATIONS -U $USER --exclude-table-data='*.SupportingDocuments*'-w -h $SERVER $ENVIRONMENT-$APPLICATIONS
echo "*** Dumping $ENVIRONMENT-$IDENTITY ***"
pg_dump --format=c -f $IDENTITY -U $USER -w -h $SERVER $ENVIRONMENT-$IDENTITY



#######################
##
##   Clean the local
##   postgres docker
##   server
##
#######################


echo "*** Dropping existing DBs in local postgres (just in case) ***"
psql -U $LOCAL_USER -h $LOCAL_SERVER -d $LOCAL_POSTGRES_DATABASE -c "DROP DATABASE IF EXISTS \"$ENVIRONMENT-$LEDGER\";"
psql -U $LOCAL_USER -h $LOCAL_SERVER -d $LOCAL_POSTGRES_DATABASE -c "DROP DATABASE IF EXISTS \"$ENVIRONMENT-$MARKETPLACE\";"
psql -U $LOCAL_USER -h $LOCAL_SERVER -d $LOCAL_POSTGRES_DATABASE -c "DROP DATABASE IF EXISTS \"$ENVIRONMENT-$APPLICATIONS\";"
psql -U $LOCAL_USER -h $LOCAL_SERVER -d $LOCAL_POSTGRES_DATABASE -c "DROP DATABASE IF EXISTS \"$ENVIRONMENT-$IDENTITY\";"


echo "*** initialising local postgres db with required users... ***"
psql -U $LOCAL_USER -h $LOCAL_SERVER -d $LOCAL_POSTGRES_DATABASE -f initialise_local_db.sql

#######################
##
##  Restore the remote
##  dumps into the
##  local postgres
##  docker server
##
#######################

echo "*** Restoring $ENVIRONMENT-$LEDGER Locally ***"
pg_restore -C -d $LOCAL_POSTGRES_DATABASE -U $LOCAL_USER -h $LOCAL_SERVER $LEDGER
echo "*** Restoring $ENVIRONMENT-$MARKETPLACE Locally ***"
pg_restore -C -d $LOCAL_POSTGRES_DATABASE -U $LOCAL_USER -h $LOCAL_SERVER $MARKETPLACE
echo "*** Restoring $ENVIRONMENT-$APPLICATIONS Locally ***"
pg_restore -C -d $LOCAL_POSTGRES_DATABASE -U $LOCAL_USER -h $LOCAL_SERVER $APPLICATIONS
echo "*** Restoring $ENVIRONMENT-$IDENTITY Locally ***"
pg_restore -C -d $LOCAL_POSTGRES_DATABASE -U $LOCAL_USER -h $LOCAL_SERVER $IDENTITY

#######################
##
##  Anonymise the local
##  copies
##
#######################

echo Running anonymisation locally..
psql -U $LOCAL_USER -h $LOCAL_SERVER -d $ENVIRONMENT-$LEDGER -f anonymise_ledger.sql
psql -U $LOCAL_USER -h $LOCAL_SERVER -d $ENVIRONMENT-$APPLICATIONS -f anonymise_applications.sql
psql -U $LOCAL_USER -h $LOCAL_SERVER -d $ENVIRONMENT-$IDENTITY -f anonymise_identity.sql
psql -U $LOCAL_USER -h $LOCAL_SERVER -d $ENVIRONMENT-$MARKETPLACE -f anonymise_marketplace.sql


#######################
##
##  Dump the local copies
##  to /dumpfiles which
##  should be mounted
##  to the host with
##  docker run -v hostpath:/dumpfiles
##
#######################

echo "*** Outputting Anonymised Dumpfiles... ***"
echo "*** Creating $LEDGER-Anon ***"
pg_dump --format=c -f /dumpfiles/Anonymous-$LEDGER.dmp  -U $USER -w -h $LOCAL_SERVER $ENVIRONMENT-$LEDGER
echo "*** Creating $MARKETPLACE-Anon ***"
pg_dump --format=c -f /dumpfiles/Anonymous-$MARKETPLACE.dmp -U $USER -w -h $LOCAL_SERVER $ENVIRONMENT-$MARKETPLACE
echo "*** Creating $APPLICATIONS-Anon ***"
pg_dump --format=c -f /dumpfiles/Anonymous-$APPLICATIONS.dmp -U $USER -w -h $LOCAL_SERVER $ENVIRONMENT-$APPLICATIONS
echo "*** Creating $IDENTITY-Anon ***"
pg_dump --format=c -f /dumpfiles/Anonymous-$IDENTITY.dmp -U $USER -w -h $LOCAL_SERVER $ENVIRONMENT-$IDENTITY

echo Done.
