WITH v_AspNetUsers AS
(
	SELECT (ROW_NUMBER() OVER())::VARCHAR(200)  AS "rn", "Id" FROM "AspNetUsers"
)
UPDATE "AspNetUsers" 
SET
	"UserName" = v_AspNetUsers."rn"
 , 	"NormalizedUserName" = CONCAT( v_AspNetUsers."rn",'@TRIAD.CO.UK')
 , 	"Email" = CONCAT( v_AspNetUsers."rn",'@TRIAD.CO.UK')
 , 	"NormalizedEmail" = CONCAT( v_AspNetUsers."rn",'@TRIAD.CO.UK')
 ,	"PasswordHash" = 'AQAAAAEAACcQAAAAEB8ewYBzZCp3FHeJAS5VJZI+1I/t5B32VxMz/eIWz/VsVhTsVTEm1bb7WTf5kOWHIw=='
 , 	"FullName" = v_AspNetUsers."rn"
FROM v_AspNetUsers
WHERE
	"AspNetUsers"."Id" = v_AspNetUsers."Id"
AND	"NormalizedEmail" != 'GOSADMIN@TRIAD.CO.UK';

INSERT INTO "AspNetUserRoles"
SELECT
	u."Id",
	r."Id"
FROM
	"AspNetUsers" AS u,
	"AspNetRoles" AS r
WHERE
	UPPER(r."Name")	  = 'ADMINISTRATOR'
AND	"NormalizedEmail" = 'GOSADMIN@TRIAD.CO.UK'
ON CONFLICT ON CONSTRAINT "PK_AspNetUserRoles" DO NOTHING;

CREATE EXTENSION "uuid-ossp";

UPDATE "Invites"
SET
	"OrganisationName" = (CASE WHEN "SenderId" IS NULL
				THEN 'None'
				ELSE CAST("SenderId" AS TEXT)
				END),	
	"Email" = 'anonymised@anon.com',
	"Uri" = uuid_generate_v4();