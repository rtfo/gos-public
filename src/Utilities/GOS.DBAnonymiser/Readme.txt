The DBAnonymiser produces 4 postgres dump files containing anonymised copies of the GOS DBS through
the use of a local postgres docker container as a temporary server within which the anonymisation
scripts are run

The Container performs the following steps...

1. Run a postgres server
2. Create local dumpfiles of the 4 remote databases
3. restore the dumpfiles into the local postgres
4. run the anonymisation scripts
5. create dumpfiles of the now anonymised databases

Dumpfiles are output to the containers '/dumpfiles' directory which should be mapped
to a host directory as part of the docker run command in order to obtain the final dumpfiles


Build the image from the project root folder with

docker build -t anonymiser -f src/Utilities/GOS.DBAnonymiser/Dockerfile .


to run from GitBash and output dump files to the local directory

winpty docker run --rm -v //`pwd`:/dumpfiles -ti anonymiser -u proxyuser -s <server> -p '<password>' -e <environment e.g rtfocluster-integration>


This image is built as part of the normal GOS build and stored in Triads container
registry enabling DfT to pull the image for the purpose of extracting anonymised database copies

To restore see https://confluence.triad.co.uk/pages/viewpage.action?pageId=11731862

Instructions for DFT

You will need access to a gos-admin-service-account.json keyfile. This is output as part of the terraform and stored within Production Deployment/Kubernetes/keyfiles and should still be on the jumpbox under the original installers home folder.
Alternatively, the base64 encoded keyfile is held within the terraform remote state file in a storage bucket of the project. The 'content' element of gos-admin-service-account-json near the bottom of the remote state file.

1. open connection to jumpbox
2. docker login -u _json_key -p "$(base64 -d gos-admin-service-account.json)" https://eu.gcr.io
   OR for a non base64 encoded jsonfile use.
   docker login -u _json_key -p "$(cat gos-admin-service-account.json)" https://eu.gcr.io
3. docker run --rm -v `pwd`:/dumpfiles -ti eu.gcr.io/dft-gos/gos.dbanonymiser:<APPLICATION VERSION> -u proxyuser -s <CloudSQL IP> -p '<password>' -e gos-prod

The application version is the x.x.x displayed on the home page of the application excluding the build number
The current working directory should now contain 4 .dmp files that need to be copied across and shared with Triad.
