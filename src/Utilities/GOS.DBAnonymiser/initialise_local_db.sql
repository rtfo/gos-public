DROP ROLE IF EXISTS cloudsqladmin;
DROP ROLE IF EXISTS cloudsqlsuperuser;
DROP ROLE IF EXISTS proxyuser;

CREATE ROLE cloudsqladmin LOGIN
VALID UNTIL 'infinity';

CREATE ROLE cloudsqlsuperuser LOGIN
VALID UNTIL 'infinity';

CREATE ROLE proxyuser LOGIN
VALID UNTIL 'infinity';