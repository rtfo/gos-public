#!/bin/bash
#Adapted from https://github.com/sjenning/kube-nginx-letsencrypt/blob/master/entrypoint.sh
if [[ -z $EMAIL || -z $DOMAINS || -z $SECRET || -z $INGRESS ]]; then
	echo "EMAIL, DOMAINS, SECRET, and INGRESS env vars required"
	env
	exit 1
fi

NAMESPACE=$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace)

cd $HOME
mkdir -p .well-known/acme-challenge

#Startup HTTP Server to serve the challenge response that certbot writes to ./.well-known/acme-challenge
#LetsEncrypt will call http://<domain>/.well-known/acme-challenge/<key> once
#certbot is run
python -m SimpleHTTPServer 80 &
PID=$!
echo Sleep for 30 seconds to allow HTTPServer to come up..
sleep 30
echo Calling certbot...
#set $DRYRUN to --dry-run when running this container to use the LetsEncrypt staging environment
certbot certonly $DRYRUN --webroot -w $HOME -n --agree-tos --email ${EMAIL} --no-self-upgrade -d ${DOMAINS}

kill $PID

CERTPATH=/etc/letsencrypt/live/$(echo $DOMAINS | cut -f1 -d',')

ls $CERTPATH || exit 1

cat /secret-patch-template.json | \
	sed "s/NAMESPACE/${NAMESPACE}/" | \
	sed "s/NAME/${SECRET}/" | \
	sed "s/TLSCERT/$(cat ${CERTPATH}/fullchain.pem | base64 | tr -d '\n')/" | \
	sed "s/TLSKEY/$(cat ${CERTPATH}/privkey.pem |  base64 | tr -d '\n')/" \
	> /secret-patch.json

ls /secret-patch.json || exit 1

cat /ingress-patch-template.json | \
	sed "s/TLSUPDATED/$(date)/" | \
	sed "s/NAMESPACE/${NAMESPACE}/" | \
	sed "s/NAME/${INGRESS}/" \
	> /ingress-patch.json

ls /ingress-patch.json || exit 1

# update secret
curl -v --cacert /var/run/secrets/kubernetes.io/serviceaccount/ca.crt -H "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" -k -XPATCH  -H "Accept: application/json" -H "Content-Type: application/strategic-merge-patch+json" -d @/secret-patch.json https://$KUBERNETES_PORT_443_TCP_ADDR/api/v1/namespaces/${NAMESPACE}/secrets/${SECRET}

# update pod spec on ingress deployment to trigger redeploy
curl -v --cacert /var/run/secrets/kubernetes.io/serviceaccount/ca.crt -H "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" -k -XPATCH  -H "Accept: application/json" -H "Content-Type: application/strategic-merge-patch+json" -d @/ingress-patch.json https://$KUBERNETES_PORT_443_TCP_ADDR/apis/extensions/v1beta1/namespaces/${NAMESPACE}/ingresses/${INGRESS}
