Only to be used in Non-Production environments
----------------------------------------------

TlsCertificates is a Container that handles the auto-renewal of free TLS certificate using Certbot and the LetsEncrypt service.

Cerbot, when run, creates a challenge response file under the .well-known/acme-challenge folder.
It then contacts LetsEncrypt with the Domain name of the site you want to issue certificates for.
LetsEncrypt first asks the site over http for the challenge reponse at http://<domain>/.well-known-acme-challenge/<challengeresponse>
To make this happen, a Simple HTTP server is run before Certbot is called.

This completes the verification process with Letsencrypt and establishes the neccesary TLS Cert and Key

The TLS Cert and Key is then stored in a kubernetes secret - sslcert (this is actually 'patched' so that any existing cert/key is replaced by the new one)
Ordinarily, the Ingress Network object would not automatically re-read the Secret so to trigger this to happen, the Ingress Object is patched with a dummy change
to just update a 'tlsUpdated' label on the object which causes the re-read of its configuration.


To manually test, the easiest way is to create a Job definition based on the existing cronjob

kubectl create job --from=cronjob/gos-tlscertificate-update-job tlsupdate -o yaml --namespace <environment> --dry-run > job.yml
(The --dry-run flag is a kubectl flag, nothing to do with the LetsEncrypt --dry-run flag)

Given this file, you can now edit it to set a --dry-run flag for certbot if you wish to use the LetsEncrypt staging environment and avoid rate limits (however it 
is worth noting that this causes the job to fail since no certificates are generated - but it is at least useful to test connectivity/compatability with LetsEncrypt)

Edit the job.yml 
 add a namespace within the metadata section
   namespace: <environment>

 add the following in the env section beneath containers if you wish
- name: DRYRUN
  value: --dry-run


You can now apply the yml to test the job (If you used --dry-run, the job will fail but you should have a "The dry run was successful" log entry)