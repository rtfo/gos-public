﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Website.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace DfT.GOS.Website.Middleware
{
    /// <summary>
    /// Middleware component to implement the recommended Security Headers
    /// </summary>
    public class SecurityHeaders
    {
        #region Constants

        //Security Headers
        private const string ContentSecurityPolicyHeader = "Content-Security-Policy";
        private const string CrossSiteScriptingPolicyHeader = "X-XSS-Protection";
        private const string ContentTypeOptionsPolicyHeader = "X-Content-Type-Options";
        private const string ReferrerPolicyHeader = "Referrer-Policy";
        private const string CacheControlHeader = "Cache-control";
        private const string PragmaCacheControlHeader = "Pragma";
        private const string FeaturePolicyHeader = "Feature-Policy";

        //Content Policy Settings
        private const string DefaultToAllowContentFromOwnDomainOnly = "default-src 'self'";
        private const string ScriptSourceSelf = "script-src 'self'";
        private const string ImageSourceSelf = "img-src 'self'";

        //Cross Site Scripting Settings
        private const string BlockCrossSiteScriptingAttacks = "1; mode=block;";

        //Content Type Scripting Settings
        private const string DisableMimeTypeSniffing = "nosniff";

        //Referrer Policy Settings
        private const string NoReferrerAllowed = "no-referrer";

        //Cache Control Settings
        private const string NoCache = "no-cache;";

        //Feature Policy Settings
        private const string RestrictAutoplay = "autoplay 'none'";
        private const string RestrictCamera = "camera 'none'";
        private const string RestrictEncryptedMedia = "encrypted-media 'none'";
        private const string RestrictFullscreen = "fullscreen 'none'";
        private const string RestrictGeoLocation = "geolocation 'none'";
        private const string RestrictMicrophone = "microphone 'none'";
        private const string RestrictMidi = "midi 'none'";
        private const string RestrictPayment = "payment 'none'";
        private const string RestrictVr = "vr 'none'";

        #endregion Constants

        #region Private Variables

        private readonly RequestDelegate _next;

        #endregion Private Variables

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="next">Middleware component to invoke next</param>
        public SecurityHeaders(RequestDelegate next)
        {
            _next = next;           
        }

        /// <summary>
        /// Default  Constructor
        /// </summary>
        protected SecurityHeaders()
        {
        }

        #endregion Constructors
       
        #region Public Methods

        /// <summary>
        /// Performs the required processing for this middleware component, applying all HTTP Header Security Policies
        /// </summary>
        /// <param name="httpContext">Current Http Context</param>
        public Task Invoke(HttpContext httpContext)
        {
            ApplySecurityHeaders(httpContext, applyCachingPolicies: true);
            return _next(httpContext);
        }

        #endregion Public Methods

        #region Public Static Methods

        /// <summary>
        /// Applies HTTP Header Security Policies, optionally applying the Caching Policy (which may or 
        /// may not need to be applied depending on the resource type)
        /// </summary>
        /// <param name="httpContext">Current Http Context</param>
        /// <param name="applyCachingPolicies">Whether to apply to Caching Policy</param>
        public static void ApplySecurityHeaders(HttpContext httpContext, bool applyCachingPolicies)
        {
            //Content Security Policy
            ApplyContentSecurityPolicy(httpContext);

            //Cross Site Scripting
            ApplyCrossSiteScriptingPolicy(httpContext);

            //Content Sniffing
            ApplyContentSniffingPolicy(httpContext);

            //Referrer Policy
            ApplyReferrrerPolicy(httpContext);

            if (applyCachingPolicies)
            {
                //Caching Policies (Cache Control - HTTP/1.1 & Pragma - HTTP/1.0)
                ApplyCachingPolicies(httpContext);
            }

            //Feature Policy
            ApplyFeaturesPolicies(httpContext);
        }

        #endregion Public Static Methods

        #region Private Methods

        /// <summary>
        /// Applies the Content Security Policy to the HTTP Header
        /// </summary>
        /// <param name="httpContex">The HTTP Context to apply the HTTP Header to</param>
        private static void ApplyContentSecurityPolicy(HttpContext httpContext)
        {
            RemoveHeader(httpContext, ContentSecurityPolicyHeader);
            //Add a default of 'self', plus a script source including a nonce, and an image source of Google Analytics to allow tracking
            var nonceService = (CSPNonceService)httpContext.RequestServices.GetService(typeof(CSPNonceService));
            string sources = $"{DefaultToAllowContentFromOwnDomainOnly}; {ScriptSourceSelf} www.googletagmanager.com 'nonce-{nonceService.GetNonce()}'; {ImageSourceSelf} www.google-analytics.com";
            httpContext.Response.Headers.Add(ContentSecurityPolicyHeader, sources);
        }

        /// <summary>
        /// Applies the Cross site scripting Policy to the HTTP Header
        /// </summary>
        /// <param name="httpContex">The HTTP Context to apply the HTTP Header to</param>
        private static void ApplyCrossSiteScriptingPolicy(HttpContext httpContext)
        {
            RemoveHeader(httpContext, CrossSiteScriptingPolicyHeader);
            httpContext.Response.Headers.Add(CrossSiteScriptingPolicyHeader, BlockCrossSiteScriptingAttacks);
        }

        /// <summary>
        /// Applies the Content Sniffing Policy to the HTTP Header
        /// </summary>
        /// <param name="httpContex">The HTTP Context to apply the HTTP Header to</param>
        private static void ApplyContentSniffingPolicy(HttpContext httpContext)
        {
            RemoveHeader(httpContext, ContentTypeOptionsPolicyHeader);
            httpContext.Response.Headers.Add(ContentTypeOptionsPolicyHeader, DisableMimeTypeSniffing);
        }

        /// <summary>
        /// Applies the Referrer Policy to the HTTP Header
        /// </summary>
        /// <param name="httpContex">The HTTP Context to apply the HTTP Header to</param>
        private static void ApplyReferrrerPolicy(HttpContext httpContext)
        {
            RemoveHeader(httpContext, ReferrerPolicyHeader);
            httpContext.Response.Headers.Add(ReferrerPolicyHeader, NoReferrerAllowed);
        }

        /// <summary>
        /// Applies the Caching Policies to the HTTP Header
        /// </summary>
        /// <param name="httpContex">The HTTP Context to apply the HTTP Header to</param>
        private static void ApplyCachingPolicies(HttpContext httpContext)
        {
            RemoveHeader(httpContext, CacheControlHeader);
            httpContext.Response.Headers.Add(CacheControlHeader, NoCache);

            RemoveHeader(httpContext, PragmaCacheControlHeader);
            httpContext.Response.Headers.Add(PragmaCacheControlHeader, NoCache);
        }

        /// <summary>
        /// Applies the Features Policies to the HTTP Header
        /// </summary>
        /// <param name="httpContex">The HTTP Context to apply the HTTP Header to</param>
        private static void ApplyFeaturesPolicies(HttpContext httpContext)
        {
            RemoveHeader(httpContext, FeaturePolicyHeader);
            httpContext.Response.Headers.Add(FeaturePolicyHeader, RestrictAutoplay + "; " +
                RestrictCamera + "; " +
                RestrictEncryptedMedia + "; " +
                RestrictFullscreen + "; " +
                RestrictGeoLocation + "; " +
                RestrictMicrophone + "; " +
                RestrictMidi + "; " +
                RestrictPayment + "; " +
                RestrictVr);
        }

        /// <summary>
        /// Removes a security header from the response if it exists
        /// </summary>
        /// <param name="httpContext">Current Http Context</param>
        /// <param name="key">The key of the header to remove</param>
        private static void RemoveHeader(HttpContext httpContext, string key)
        {
            if (httpContext.Response.Headers.ContainsKey(key))
            {
                httpContext.Response.Headers.Remove(key);
            }
        }

        #endregion Private Methods
    }

    /// <summary>
    /// Extension class for the IApplicationBuilder interface to support installation of the SecurityHeaders middleware
    /// </summary>
    public static class SecurityHeadersExtensions
    {
        /// <summary>
        /// Extension method used to add the middleware to the HTTP request pipeline.
        /// </summary>
        /// <param name="builder">IApplicationBuilder instance to which the middleware should be added</param>
        /// <returns>IApplicationBuilder instance with middleware installed</returns>
        public static IApplicationBuilder UseSecurityHeaders(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SecurityHeaders>();
        }
    }
}
