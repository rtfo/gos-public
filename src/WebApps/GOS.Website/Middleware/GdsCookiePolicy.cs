﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace DfT.GOS.Website.Middleware
{
    /// <summary>
    /// Middleware component to implement the GDS Cookie Policy as described at 
    /// https://www.gov.uk/service-manual/technology/working-with-cookies-and-similar-technologies#displaying-a-cookie-banner
    /// </summary>
    /// <remarks>
    /// You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    /// </remarks>
    public class GdsCookiePolicy
    {
        #region Constants

        /// <summary>
        /// Name of cookie used to indicate that the cookie warning has been shown to the user
        /// </summary>
        public const string CookieName = "seen_cookie_message";

        /// <summary>
        /// Name of HttpContext Item used to store flag indicating whether cookie warning has been shown to the user
        /// </summary>
        public const string ShowCookieWarning = "ShowCookieWarning";

        #endregion Constants

        #region Fields

        private readonly RequestDelegate _next;
        private readonly int _cookieExpirationPeriodInMonths;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Constructor that accepts all depedencies
        /// </summary>
        /// <param name="next">Middleware component to invoke next</param>
        public GdsCookiePolicy(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            if (appSettings == null || appSettings.Value == null)
            {
                throw new ArgumentNullException("appSettings");
            }

            _next = next;
            _cookieExpirationPeriodInMonths = appSettings.Value.CookieWarningCookieExpirationInMonths;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Performs the required processing for this middleware component
        /// </summary>
        /// <param name="httpContext">Current Http Context</param>
        public Task Invoke(HttpContext httpContext)
        {
            // Check for a cookie indicating that we have previously shown this user the cookie warning message
            var cookieWarningShown = httpContext.Request.Cookies
                .Any(c => c.Key == CookieName);

            if (!cookieWarningShown)
            {
                // The user has not been shown the cookie warning before
                // Set a flag to display the cookie warning (in _Layout.cshtml)
                httpContext.Items[ShowCookieWarning] = true;

                // Create a cookie so that we know that the user has been shown the cookie warning
                // The cookie will expire after a given period; the user will then see the warning again
                var cookieBuilder = new CookieBuilder()
                {
                    SameSite = SameSiteMode.Lax,
                    SecurePolicy = CookieSecurePolicy.SameAsRequest,
                    HttpOnly = true,
                    Name = CookieName,
                    Expiration = DateTime.Now.AddMonths(_cookieExpirationPeriodInMonths) - DateTime.Now
                };
                httpContext.Response.Cookies.Append(CookieName, "true", cookieBuilder.Build(httpContext));
            }
            else
            {
                // The user has been shown the cookie warning before so we don't need to show it again
                httpContext.Items[ShowCookieWarning] = false;
            }

            return _next(httpContext);
        }

        #endregion Methods
    }

    /// <summary>
    /// Extension class for the IApplicationBuilder interface to support installation of the GdsCookiePolicy middleware
    /// </summary>
    public static class GdsCookiePolicyExtensions
    {
        /// <summary>
        /// Extension method used to add the middleware to the HTTP request pipeline.
        /// </summary>
        /// <param name="builder">IApplicationBuilder instance to which the middleware should be added</param>
        /// <returns>IApplicationBuilder instance with middleware installed</returns>
        public static IApplicationBuilder UseGdsCookiePolicy(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<GdsCookiePolicy>();
        }
    }
}
