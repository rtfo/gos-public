﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Website.Middleware
{
    /// <summary>
    /// CookieTokenAuthenticationEvents called on each request made by the UI
    /// Allows each request to be authenticated using the values from the cookie context.
    /// </summary>
    public class CookieTokenAuthenticationEvents : CookieAuthenticationEvents
    {
        #region Fields
        private readonly IIdentityService IdentityService;
        private readonly ILogger<CookieTokenAuthenticationEvents> Logger;
        #endregion Fields

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="identityService">Identity service that provides method for validating the principal</param>
        public CookieTokenAuthenticationEvents(IIdentityService identityService, ILogger<CookieTokenAuthenticationEvents> logger)
        {
            this.IdentityService = identityService;
            this.Logger = logger;
        }

        /// <summary>
        /// ValidatePrincipal performs custom validation on the principal in the cookie
        /// </summary>
        /// <param name="context">Context object providing information from the cookie for validation</param>
        /// <returns>Completed task, or rejects the principal</returns>
        public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
        {
            var userPrincipal = context.Principal;
            var securityStamp = userPrincipal.Claims.Where(claim => claim.Type == Claims.SecurityStamp).Select(Claims => Claims.Value).SingleOrDefault();
            var userID = userPrincipal.Claims.Where(claim => claim.Type == Claims.UserId).Select(Claims => Claims.Value).SingleOrDefault();
            var result = await IdentityService.ValidateSecurityStamp(securityStamp, userID);
            if (!result)
            {
                this.Logger.LogWarning("users security stamp did not match the expected security stamp, logging them out");
                context.RejectPrincipal();
            }
        }
    }
}
