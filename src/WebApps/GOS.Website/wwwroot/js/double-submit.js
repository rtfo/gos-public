﻿//Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
$('button[type=submit]').on("click", function () {
    if ($(this.form).valid()) {
        this.disabled = true;
        $(this.form).submit();
        return false;
    }
});