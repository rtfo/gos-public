﻿//Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
var DfT = DfT || {};
DfT.GOS = DfT.GOS || {};

(function (self) {
    //Reload a portion of the screen from a url
    function RefreshDom(selector, url) {
        $.get(url).then(function (content) {
            $(selector).html(content);           
        });
    }

    //Refresh the screen with the current status of the GHG balance update
    self.Refresh = function () {
        var bulkUpdateUniqueId = $("#BulkUpdateUniqueId").val();
        var url = "/SystemAdministration/UpdateGhgBalances/" + bulkUpdateUniqueId + "/Refresh";
        RefreshDom("#updateGhgBalanceResult", url);
        if ($(".govuk-panel--confirmation").length) {
            //The confirmation panel is being displayed so we can stop refreshing the screen
            clearInterval(refreshHandle);
        }
    };

    var refreshInterval = parseInt($("#UpdateGhgBalanceConfirmationRefreshIntervalInSeconds").val());
    var refreshHandle = setInterval(DfT.GOS.Refresh, refreshInterval * 1000);
}(DfT.GOS));