﻿//Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

//Handles showing and hiding the contact details section on the advertise to marketplace page
// based on the combined values of the four radio button options
if ($('#sell-credits-no').is(":checked") && $('#buy-credits-no').is(":checked")) {
    $('#conditional-contact-details').addClass("js-hidden")
}

$("#buy-credits-yes").on('click', function (event) {
    $('#conditional-contact-details').removeClass("js-hidden")
});

$("#sell-credits-yes").on('click', function (event) {
    $('#conditional-contact-details').removeClass("js-hidden")
});

$("#buy-credits-no").on('click', function (event) {
    if (!$('#sell-credits-yes').is(":checked")) {
        $('#conditional-contact-details').addClass("js-hidden")
        clearAll();
    }
});

$("#sell-credits-no").on('click', function (event) {
    if (!$('#buy-credits-yes').is(":checked")) {
        $('#conditional-contact-details').addClass("js-hidden")
        clearAll();
    }
});

function clearAll() {
    $('#contact-details-full-name').val('');
    $('#contact-details-email').val('');
    $('#contact-details-phone-number').val('');
}