﻿//Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

//Clear Electricity
$("#fuel-category-electricity").on('click', function (event) {
    $('#electricity-amount-supplied').val('');
    $('#electricity-ghg-intensity').val('');
});

//Clear Fossil Gas
$("#fuel-category-fossil-gas").on('click', function (event) {
    $('#fossil-gas-volume').val('');
    $('#fossil-gas-type').get(0).selectedIndex = 0;
});

//Clear UER
$("#fuel-category-uer").on('click', function (event) {
    $('#uer-reference').val('');
    $('#uer-amount-supplied').val('');
});
