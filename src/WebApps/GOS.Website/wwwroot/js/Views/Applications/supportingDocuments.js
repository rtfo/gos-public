﻿//Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
$(document).ready(function () {    
    //Auto file-upload
    $("#file-upload-1").change(function () {
        this.form.submit();
    });
});