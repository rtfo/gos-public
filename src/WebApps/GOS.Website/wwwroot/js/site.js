﻿//Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

//Customise JQuery validation
if ($.validator) {
    $.validator.setDefaults({
        onkeyup: false,
        onclick: false,
        onfocusin: false,
        onfocusout: false
    });
}

//Show no-javascript elements
$(".hide-no-javascript").removeClass("hide-no-javascript");

//Show/hide password
$(".password-obfuscator").on('click', function (event) {
    if ($(this).is(':checked')) {
        $('.password').attr('type', 'text');
    }
    else {
        $('.password').attr('type', 'password');
    }
});