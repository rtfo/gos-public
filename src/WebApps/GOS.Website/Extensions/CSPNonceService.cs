﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Security.Cryptography;

namespace DfT.GOS.Website.Extensions
{
    /// <summary>
    /// Service to generate a nonce value for Content Security Policy
    /// </summary>
    public class CSPNonceService
    {
        #region Fields
        private readonly string _nonce;
        #endregion Fields

        #region Constructors

        /// <summary>
        /// Constructor accepting dependencies
        /// </summary>
        /// <param name="nonceByteAmount">The length of the nonce to generate</param>
        public CSPNonceService(int nonceByteAmount = 32)
        {
            byte[] nonceBytes = new byte[nonceByteAmount];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(nonceBytes);
            }

            _nonce = Convert.ToBase64String(nonceBytes);
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Return the generated nonce value
        /// </summary>
        /// <returns>base64 encoded string of the generated nonce</returns>
        public string GetNonce()
        {
            return _nonce;
        }

        #endregion Methods
    }
}
