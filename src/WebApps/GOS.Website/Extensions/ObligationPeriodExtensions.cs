﻿using DfT.GOS.ReportingPeriods.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Website.Extensions
{
    /// <summary>
    /// Extension methods to help when working with Obligation Periods
    /// </summary>
    public static class ObligationPeriodExtensions
    {
        /// <summary>
        /// Gets a description for the Obligation Period
        /// </summary>
        /// <param name="obligationPeriod">Obligation Period for which a description is required</param>
        /// <returns>Obligation Period Description</returns>
        public static string GetDescription(this ObligationPeriod obligationPeriod)
        {
            if (obligationPeriod == null || !obligationPeriod.EndDate.HasValue)
            {
                return "No Obligation Period";
            }
            else
            {
                return obligationPeriod.EndDate.Value.ToString("dd/MM/yyyy");
            }
        }
    }
}
