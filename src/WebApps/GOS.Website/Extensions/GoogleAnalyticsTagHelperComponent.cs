﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Website.Configuration;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.Options;
using System;

namespace DfT.GOS.Website.Extensions
{
    /// <summary>
    /// Tag helper to insert Google Analytics javascript snippet in the head of a page
    /// </summary>
    public class GoogleAnalyticsTagHelperComponent : TagHelperComponent
    {
        #region Fields
        private readonly GoogleAnalyticsConfigurationSettings _googleAnalyticsConfigurationSettings;
        private readonly CSPNonceService _nonceService;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Constructor accepting dependencies
        /// </summary>
        /// <param name="googleAnalyticsConfigurationSettings">Google Analytics settings from configuration</param>
        /// <param name="nonceService">Instance of CSPNonceService for generation of a nonce string</param>
        public GoogleAnalyticsTagHelperComponent(IOptions<GoogleAnalyticsConfigurationSettings> googleAnalyticsConfigurationSettings, CSPNonceService nonceService)
        {
            _googleAnalyticsConfigurationSettings = googleAnalyticsConfigurationSettings.Value;
            _nonceService = nonceService;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Synchronously executes the Microsoft.AspNetCore.Razor.TagHelpers.ITagHelperComponent with the given context and output.        
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="output"> A stateful HTML element used to generate an HTML tag.</param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            // Inject the code only in the head element
            if (string.Equals(output.TagName, "head", StringComparison.OrdinalIgnoreCase))
            {
                // Get the tracking code from the configuration
                var trackingCode = _googleAnalyticsConfigurationSettings.TrackingCode;
                if (!string.IsNullOrEmpty(trackingCode))
                {
                    //Get the nonce value from the nonce service
                    var nonce = _nonceService.GetNonce();
                    string analyticsHtml = $@"
    <script async src='https://www.googletagmanager.com/gtag/js?id={trackingCode}'></script>
    <script nonce='{nonce}'>
        window.dataLayer = window.dataLayer || [];
        function gtag() {{ dataLayer.push(arguments); }}
        gtag('js', new Date());

        gtag('config', '{trackingCode}');
    </script>
";

                    // PreContent corresponds to the text just after the opening tag
                    output.PreContent
                        .AppendHtml(analyticsHtml);
                }
            }
        }
        #endregion Methods
    }
}
