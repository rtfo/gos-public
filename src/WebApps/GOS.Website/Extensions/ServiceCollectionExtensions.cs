﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.DependencyInjection;

namespace DfT.GOS.Website.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="IServiceCollection"/>
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        #region Methods

        /// <summary>
        /// Adds dependencies required for adding
        /// nonces to Content Security Policy headers
        /// and inline scripts and styles.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/></param>
        /// <param name="nonceByteAmount">Length of the nonces to generate in bytes.</param>
        /// <returns>The <see cref="IServiceCollection"/></returns>
        public static IServiceCollection AddCSPNonce(this IServiceCollection services, int nonceByteAmount = 32)
        {
            return services.AddScoped<CSPNonceService>(svcProvider => new CSPNonceService(nonceByteAmount));
        }

        #endregion Methods
    }
}
