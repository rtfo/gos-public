﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Website.Middleware;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Website.Extensions
{
    /// <summary>
    /// Extension methods for the IHtmlHelper interface (used in Views)
    /// </summary>
    public static class IHtmlHelperExtensions
    {
        /// <summary>
        /// Determines whether to display the cookie warning message
        /// </summary>
        /// <param name="htmlHelper">IHtmlHelper instance on which the method is being called</param>
        /// <returns>True if the cookie warning message should be displayed, false otherwise</returns>
        /// <remarks>
        /// The GdsCookiePolicy middleware will determine whether the cookie warning message 
        /// should be displayed and set a flag on the current HttpContext
        /// </remarks>
        public static bool DisplayCookieWarning(this IHtmlHelper<dynamic> htmlHelper)
        {
            // Check for the flag set by the GdsCookiePolicy middleware
            var warningFlagPresent = htmlHelper.ViewContext.HttpContext.Items
                .Any(item => item.Key is string && (string)item.Key == GdsCookiePolicy.ShowCookieWarning);

            if (warningFlagPresent)
            {
                var showCookieWarning = htmlHelper.ViewContext.HttpContext.Items
                    .First(item => item.Key is string && (string)item.Key == GdsCookiePolicy.ShowCookieWarning);

                if (showCookieWarning.Value is bool)
                {
                    return (bool)(showCookieWarning.Value);
                }
                else
                {
                    throw new Exception($"Unexpected value for {GdsCookiePolicy.ShowCookieWarning} flag.");
                }
            }
            else
            {
                return false;
            }
        }
    }
}
