﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Configuration
{
    /// <summary>
    /// Configuration class for Google Analytics settings
    /// </summary>
    public class GoogleAnalyticsConfigurationSettings
    {
        /// <summary>
        /// The Google Analytics tracking code
        /// </summary>
        public string TrackingCode { get; set; }
    }
}
