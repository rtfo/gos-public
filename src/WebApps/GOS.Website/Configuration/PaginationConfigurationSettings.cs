﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Configuration
{
    /// <summary>
    /// Configuration class for UI result pagination
    /// </summary>
    public class PaginationConfigurationSettings
    {
        /// <summary>
        /// The number of results to display on paginated tables throughout the site
        /// </summary>
        public virtual int ResultsPerPage { get; set; }

        /// <summary>
        /// The maximum number of pages that should be displayed
        /// </summary>
        public virtual int MaximumPagesToDisplay { get; set; }
    }
}
