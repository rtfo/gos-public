﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using DfT.GOS.Website.Models;
using Microsoft.AspNetCore.Authorization;
using DfT.GOS.Website.Orchestrators;
using DfT.GOS.Website.Models.UnlinkedUserViewModels;
using System.Threading;
using System.Threading.Tasks;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.Security.Claims;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Models.AdministratorViewModels;
using System;
using Microsoft.Extensions.Logging;
using DfT.GOS.Website.Models.TraderViewModels;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalance.Common.Models;

namespace DfT.GOS.Website.Controllers
{
    public class HomeController : Controller
    {
        #region Properties

        private IUsersService IdentityService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private ISystemParametersService SystemParametersService { get; set; }
        private ITokenService TokenService { get; set; }
        private ISupplierReportingSummaryOrchestrator Orchestrator { get; set; }
        private IBalanceViewService BalanceViewService { get; set; }
        private ILogger<HomeController> Logger { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="identityService">Identity service instance</param>
        /// <param name="reportingPeriodsService">Reporting Periods service instance</param>
        /// <param name="systemParametersService">System Parameters service instance</param>
        /// <param name="tokenService">Token service instance</param>
        /// <param name="orchestrator">Supplier Reporting Summary Orchestrator instance</param>
        /// <param name="logger">Logger instance</param>
        public HomeController(IUsersService identityService
            , IReportingPeriodsService reportingPeriodsService
            , ISystemParametersService systemParametersService
            , ITokenService tokenService
            , ISupplierReportingSummaryOrchestrator orchestrator
            , IBalanceViewService balanceViewService
            , ILogger<HomeController> logger)
        {
            this.IdentityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            this.ReportingPeriodsService = reportingPeriodsService ?? throw new ArgumentNullException(nameof(reportingPeriodsService));
            this.SystemParametersService = systemParametersService ?? throw new ArgumentNullException(nameof(systemParametersService));
            this.TokenService = tokenService ?? throw new ArgumentNullException(nameof(tokenService));
            this.Orchestrator = orchestrator ?? throw new ArgumentNullException(nameof(orchestrator));
            this.BalanceViewService = balanceViewService ?? throw new ArgumentNullException(nameof(balanceViewService));
            this.Logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Handles Errors, including specific status code, such as Not Found.
        /// </summary>
        /// <param name="statusCode">The Http Status Code (if applicable) to handle</param>
        /// <returns>Returns an error view based upon the status code</returns>
        public async Task<IActionResult> Error(int? statusCode = null)
        {
            var systemParameters = await this.SystemParametersService.GetSystemParameters();

            if (statusCode.HasValue 
                && statusCode.Value == 404)
            {
                return View("NotFound");
            }
            else
            {
                return View(new ErrorViewModel {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    RtfoSupportTelephoneNumber = systemParameters.RtfoSupportTelephoneNumber,
                    RtfoSupportEmailAddress = systemParameters.RtfoSupportEmailAddress
                });
            }
        }

        /// <summary>
        /// Provides the contact information for the beta feedback view.
        /// </summary>
        /// <returns>Returns the beta feedback view</returns>
        [HttpGet]
        [Route("Feedback")]
        public async Task<IActionResult> Feedback()
        {
            var systemParameters = await this.SystemParametersService.GetSystemParameters();
            bool isAuthenticated = User.Identity.IsAuthenticated;
            return View(new FeedbackViewModel {
                    RtfoSupportTelephoneNumber = systemParameters.RtfoSupportTelephoneNumber,
                    RtfoSupportEmailAddress = systemParameters.RtfoSupportEmailAddress,
                    IsAuthenticated = isAuthenticated
            });
        }

        /// <summary>
        /// Home page
        /// </summary>
        /// <param name="accountCreated">Flag indicating whether the user has just created a new account</param>
        /// <returns>Relevant home page view for user (based on their type - Supplier, Administrator, Trader, etc)</returns>
        [Authorize]
        [Route("Home")]
        public async Task<IActionResult> Home(bool accountCreated = false, int? obligationPeriodId = null)
        {
            if (obligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = obligationPeriodId;
            }

            if(this.User.IsAdministrator())
            {
                var jwtToken = await this.TokenService.GetJwtToken();
                Task<ObligationPeriod> obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }
                var gosApplicationUserTask = this.IdentityService.FindUserByIdAsync(this.User.GetUserId()
                    , jwtToken
                    , default(CancellationToken));
                var totalBalanceSummaryTask = this.BalanceViewService.GetSummary(obligationPeriod.Result.Id, jwtToken);

                TotalBalanceSummary balanceSummary = null;
                string userFullName = null;

                try
                {
                    await Task.WhenAll(gosApplicationUserTask, totalBalanceSummaryTask);

                    var gosApplicationUser = await gosApplicationUserTask;
                    var totalBalanceSummary = await totalBalanceSummaryTask;

                    if (totalBalanceSummary.HasResult)
                    {
                        balanceSummary = totalBalanceSummary.Result;
                    }
                    else
                    {
                        // If the balance view service is down it's still important that we show the page, just without the detail. Log the excpetion, but handle gracefully.
                        Logger.LogError("A non Success HTTP code was returned by the Balance View Service.");
                    }

                    userFullName = gosApplicationUser.FullName;
                }
                catch (Exception ex)
                {
                    // If the balance view service is down it's still important that we show the page, just without the detail. Log the excpetion, but handle gracefully.
                    Logger.LogError(ex, "Error in GosApplicationUser or TotalBalanceSummary task.");
                }    

                var viewModel = new AdministratorSummaryViewModel(userFullName, obligationPeriod.Result, balanceSummary);
                return View("AdministratorHome", viewModel);
            }
            else if(this.User.IsSupplier())
            {
                // Get Supplier home page details
                var gosApplicationUserTask = this.IdentityService.FindUserByIdAsync(this.User.GetUserId()
                    , await this.TokenService.GetJwtToken()
                    , default(CancellationToken));
                Task<ObligationPeriod> obligationPeriodTask;
                if (obligationPeriodId != null)
                {
                    obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriodTask = this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                await Task.WhenAll(gosApplicationUserTask
                                    , obligationPeriodTask);

                var gosApplicationUser = gosApplicationUserTask.Result;
                var currentObligationPeriod = obligationPeriodTask.Result;

                var viewModel = await this.Orchestrator.GetSupplierReportingSummary(gosApplicationUser.OrganisationId.Value, currentObligationPeriod, this.User);
                
                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    viewModel.ObligationPeriod = currentObligationPeriod;
                    return View("SupplierHome", viewModel);
                }
            }
            else if (this.User.IsTrader())
            {
                var gosApplicationUser = await this.IdentityService.FindUserByIdAsync(this.User.GetUserId()
                    , await this.TokenService.GetJwtToken()
                    , default(CancellationToken));
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }
                var viewModel = await this.Orchestrator.GetTraderReportingSummary(gosApplicationUser.OrganisationId.Value
                    , obligationPeriod, this.User);
                var vModel = new TraderSummaryViewModel(gosApplicationUser.FullName);
                vModel.ObligationPeriod = obligationPeriod;
                vModel.SupplierId = viewModel.SupplierId;
                vModel.SupplierName = viewModel.SupplierName;
                vModel.DisplaySupplierContext = this.User.IsAdministrator();
                vModel.BalanceSummary = viewModel.BalanceSummary;
                return View("TraderHome", vModel);
            }
            else if (this.User.IsVerifier())
            {
                return View("VerifierHome");
            }
            else
            {
                //Default to unlinked user home page
                var systemParameters = await this.SystemParametersService.GetSystemParameters();

                var viewModel = new UnlinkedUserSummaryViewModel() { AccountCreated = accountCreated
                                                                      , RtfoSupportEmailAddress = systemParameters.RtfoSupportEmailAddress
                                                                      , RtfoSupportTelephoneNumber = systemParameters.RtfoSupportTelephoneNumber};
                return View("Home", viewModel);
            }            
        }

        /// <summary>
        /// Cookie information page
        /// </summary>
        /// <returns>Cookie page view</returns>
        public IActionResult Cookies()
        {
            return View();
        }

        /// <summary>
        /// Privacy Policy page
        /// </summary>
        /// <returns>Privacy Policy page view</returns>
        public IActionResult PrivacyPolicy()
        {
            return View();
        }

        /// <summary>
        /// Accessibility Statement page
        /// </summary>
        /// <returns>Accessibility Statement page view</returns>
        public IActionResult Accessibility()
        {
            return View();
        }
        #endregion Methods
    }
}
