﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System;
using DfT.GOS.Http;

namespace DfT.GOS.Website.Controllers
{
    /// <summary>
    /// Provides a common base class for controllers within the GOS Website
    /// </summary>
    public abstract class GosWebsiteControllerBase: Controller
    {
        #region Methods

        /// <summary>
        /// Handles a failed Http Object Response from a service call, returning one of the following action Results:
        /// - Forbid
        /// - Not Found
        /// </summary>
        /// <param name="httpObjectResponse">The failed Http Object Response to handle</param>
        /// <returns>Returns the action result corresponding to the HTTP Status Code emmitted by the service call</returns>
        protected IActionResult HandleFailedHttpObjectResponse(IHttpObjectResponse httpObjectResponse)
        {
            switch (httpObjectResponse.HttpStatusCode)
            {
                case HttpStatusCode.Forbidden:
                    return Forbid();
                case HttpStatusCode.NotFound:
                    return NotFound();
                default:
                    throw new NotSupportedException(httpObjectResponse.HttpStatusCode.ToString() + " not supported.");

            }
        }

        #endregion Methods
    }
}
