﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Website;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Models.OrganisationsViewModels;
using DfT.GOS.Website.Models.SuppliersViewModels;
using DfT.GOS.Website.Orchestrators;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.Website.Controllers;
using DfT.GOS.Security.Services;
using DfT.GOS.Identity.API.Client.Services;
using System.Linq;
using AutoMapper;
using DfT.GOS.Web.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Website.Services;

namespace GOS.Website.Controllers
{
    /// <summary>
    /// Controller for organisations details
    /// </summary>
    [Authorize(Roles = "Administrator, Supplier, Trader")]
    public class OrganisationsController : GosWebsiteControllerBase
    {
        #region Properties

        private IOrganisationsService OrganisationsService { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }
        private IUsersService IdentityService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private ITokenService TokenService { get; set; }
        private ISupplierReportingSummaryOrchestrator Orchestrator { get; set; }
        private IMapper Mapper { get; set; }
        private IOrganisationsSummaryService OrganisationsSummaryService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="appSettings">The app config settings</param>
        /// <param name="organisationsService">The service to get data from</param>
        public OrganisationsController(IOptions<AppSettings> appSettings
            , IOrganisationsService organisationsService
            , IUsersService identityService
            , IReportingPeriodsService reportingPeriodsService
            , ITokenService tokenService
            , ISupplierReportingSummaryOrchestrator orchestrator
            , IMapper mapper
            , IOrganisationsSummaryService organisationsSummaryService)
        {
            this.OrganisationsService = organisationsService;
            this.AppSettings = appSettings;
            this.IdentityService = identityService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.TokenService = tokenService;
            this.Orchestrator = orchestrator;
            this.Mapper = mapper;
            this.OrganisationsSummaryService = organisationsSummaryService;
        }

        #endregion Constructors

        /// <summary>
        /// Gets the view containing a list of paged suppliers
        /// </summary>
        /// <param name="page">The page number to return, defaulting to the first page</param>
        /// <returns>Returns the supplier list page</returns>
        [HttpGet("Organisations")]
        public async Task<IActionResult> Index([FromQuery]int page = 1, [FromQuery]string search = "", int obligationPeriodId = 0, bool approved = true)
        {
            var viewModel = new ViewOrganisationsViewModel();
            var searchText = search ?? "";
            var suppliersResponse = await this.OrganisationsService.GetSuppliersAndTraders(page 
                , this.AppSettings.Value.Pagination.ResultsPerPage
                , await this.TokenService.GetJwtToken()
                , approved
                , searchText); 

            var pagedSupplierBalances = this.Mapper.Map<PagedResult<SupplierBalanceSummary>>(suppliersResponse.Result);

            viewModel.Result = new UIPagedResult<SupplierBalanceSummary>(pagedSupplierBalances
                , AppSettings.Value.Pagination.MaximumPagesToDisplay);
            viewModel.SearchText = searchText;
           
            ObligationPeriod currentObligationPeriod; 
            if(obligationPeriodId > 0)
            {
                currentObligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            }
            else
            {
                currentObligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
            }

            viewModel.ApprovedOrganisationsOnly = approved;
            viewModel.ObligationPeriodId = currentObligationPeriod.Id;
            viewModel.ObligationPeriodEndDate = currentObligationPeriod.EndDate.Value;
            ViewBag.Query = string.Format("&obligationPeriodId={0}&search={1}&approved={2}", currentObligationPeriod.Id, searchText, approved);

            if (viewModel.Result != null && viewModel.Result.Result.Count > 0)
            {
                var balanceTasks = new List<Task<SupplierReportingSummaryViewModel>>();

                foreach (var supplierBalanceSummary in viewModel.Result.Result)
                {
                    var balancesTask = this.Orchestrator.GetSupplierReportingSummary(supplierBalanceSummary.Id, currentObligationPeriod, this.User);
                    balanceTasks.Add(balancesTask);
                }

                await Task.WhenAll(balanceTasks);

                viewModel.Result.Result.ToList().ForEach(supplier =>
                {
                    var supplierBalance = balanceTasks.FirstOrDefault(s => s.Result != null && s.Result.SupplierId == supplier.Id);
                    if(supplierBalance != null
                        && supplierBalance.Result != null
                        && supplierBalance.Result.BalanceSummary != null)
                    {
                        supplier.Obligation = supplierBalance.Result.BalanceSummary.BalanceObligation;
                        supplier.GhgCreditsAvailable = supplierBalance.Result.BalanceSummary.BalanceCredits;
                        supplier.ObligatedStatus = supplier.GhgCreditsAvailable >= supplier.Obligation;
                    }
                });
            }
            return View(viewModel);
        }

        /// <summary>
        /// Gets the view containing a list of paged suppliers
        /// </summary>
        /// <param name="page">The page number to return, defaulting to the first page</param>
        /// <returns>Returns the supplier list page</returns>
        [HttpPost("Organisations")]
        public IActionResult Index(ViewOrganisationsViewModel model, [FromQuery]int page = 1)
        {
            ViewBag.Query = "&search=" + model.SearchText;
            return RedirectToAction("Index", new { page = page, search = model.SearchText, obligationPeriodId = model.ObligationPeriodId, approved = model.ApprovedOrganisationsOnly });
        }

        [HttpGet("Organisations/{obligationPeriodId}/GenerateExport")]
        public async Task<IActionResult> GenerateExport(int obligationPeriodId)
        {
            //Check obligation periodID is OK
            var currentObligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            if (currentObligationPeriod == null)
            {
                return NotFound();
            }
            var summary = await OrganisationsSummaryService.GetSummaryExport(currentObligationPeriod.Id);
            return File(summary, "application/octet-stream", "OrganisationsSummary.csv");           
        }
    }
}