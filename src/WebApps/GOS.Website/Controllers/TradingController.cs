﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Threading.Tasks;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgLedger.API.Client.Services;
using DfT.GOS.GhgLedger.Common.Exceptions;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Claims;
using DfT.GOS.Security.Services;
using DfT.GOS.Website;
using DfT.GOS.Website.Controllers;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Models.SuppliersViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace GOS.Website.Controllers
{
    /// <summary>
    /// Controller for trading related http requests
    /// </summary>
    [Authorize(Roles = Roles.TradingRoles)]
    [Route("transfer")]
    public class TradingController : GosWebsiteControllerBase
    {
        #region Properties
        private IOrganisationsService OrganisationsService { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }
        private ILogger Logger { get; set; }
        private ITokenService TokenService { get; set; }
        private ILedgerService LedgerService { get; set; }
        private IBalanceViewService BalanceViewService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor taking all dependent services
        /// </summary>
        /// <param name="appSettings">AppSettings (from configuration file)</param>
        /// <param name="organisationsService">OrganisationsService instance</param>
        /// <param name="logger">Logger instance</param>
        /// <param name="tokenService">TokenService instance</param>
        /// <param name="ledgerService">LedgerService instance</param>
        /// <param name="balanceViewService">BalanceViewService instance</param>
        /// <param name="reportingPeriods">ReportingPeriodsService instance</param>
        public TradingController(IOptions<AppSettings> appSettings
            , IOrganisationsService organisationsService
            , ILogger<TradingController> logger
            , ITokenService tokenService
            , ILedgerService ledgerService
            , IBalanceViewService balanceViewService
            , IReportingPeriodsService reportingPeriods)
        {
            this.AppSettings = appSettings;
            this.OrganisationsService = organisationsService;
            this.Logger = logger;
            this.TokenService = tokenService;
            this.LedgerService = ledgerService;
            this.BalanceViewService = balanceViewService;
            this.ReportingPeriodsService = reportingPeriods;
        }
        #endregion

        /// <summary>
        /// Gets the view containing a list of suppliers/traders
        /// </summary>
        /// <param name="supplierId">ID of supplier who is transferring the credits</param>
        /// <param name="obligationPeriod">Obligation period id</param>
        /// <param name="page">Page number</param>
        /// <param name="search">Search string to filter the supplier list</param>
        /// <returns>Paged results of supplier list</returns>
        [HttpGet("{supplierId}/obligationPeriod/{obligationPeriodId}")]
        public async Task<IActionResult> Index(int supplierId, int obligationPeriodId, [FromQuery]int page = 1, [FromQuery]string search = "")
        {
            var searchText = search ?? "";
            var token = await this.TokenService.GetJwtToken();
            var organisation = await this.OrganisationsService.GetOrganisation(supplierId, token);
            if (!organisation.HasResult && organisation.Result == null)
            {
                return NotFound("Organisation not found");
            }
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            if (obligationPeriod == null)
            {
                return NotFound("Obligation period not found");
            }
            if (!this.User.CanAccessOrganisationDetails(supplierId))
            {
                return Forbid();
            }

            var suppliersResponse = await this.OrganisationsService.GetSuppliersAndTraders(page
                , this.AppSettings.Value.Pagination.ResultsPerPage
                , token
                , false
                , searchText);

            var viewModel = new TransferRecipientViewModel
            {
                Result = new UIPagedResult<Organisation>(suppliersResponse.Result
                , AppSettings.Value.Pagination.MaximumPagesToDisplay),
                SearchText = searchText,
                ObligationPeriodId = obligationPeriodId,
                ObligationPeriod = obligationPeriod,
                SupplierId = supplierId,
                SupplierName = organisation.Result.Name,
                DisplaySupplierContext = this.User.IsAdministrator(),
                IsSupplier = organisation.Result.IsSupplier,
                IsTrader = organisation.Result.IsTrader
            };
            ViewBag.Query = "&search=" + searchText;
            return View("SelectRecipient", viewModel);
        }

        /// <summary>
        /// Post method to filter the supplier list based on search text
        /// </summary>
        /// <param name="supplierId">ID of supplier who is transferring the credits</param>
        /// <param name="obligationPeriod">Obligation period id</param>
        /// <param name="searchText">Search string to filter the supplier list</param>
        /// <param name="page">Page number</param>
        /// <returns>Redirect action to Index</returns>
        [HttpPost("{supplierId}/obligationPeriod/{obligationPeriod}")]
        [ValidateAntiForgeryToken]
        public IActionResult Index(int supplierId, int obligationPeriod, [FromForm]string searchText, [FromQuery]int page = 1)
        {
            ViewBag.Query = "&search=" + searchText;
            return RedirectToAction("Index", new
            {
                supplierId = supplierId,
                obligationPeriod = obligationPeriod,
                page = page,
                search = searchText
            });
        }

        /// <summary>
        /// Returns the transfer credits view for the selected recipient
        /// </summary>
        /// <param name="supplierId">ID of supplier who is transferring the credits</param>
        /// <param name="tosupplierId">Id of recipient supplier/trader</param>
        /// <param name="obligationPeriodId">Obligation period id</param>
        /// <returns>Returns the Transfer credits view</returns>
        [HttpGet("obligationPeriod/{obligationPeriodId}/fromOrganisation/{supplierId}/toOrganisation/{tosupplierId}/TransferCredits")]
        public async Task<IActionResult> TransferCredits(int supplierId, int tosupplierId, int obligationPeriodId)
        {
            var token = await this.TokenService.GetJwtToken();
            if (!this.User.CanAccessOrganisationDetails(supplierId))
            {
                return Forbid();
            }
            var organisation = await this.OrganisationsService.GetOrganisation(supplierId, token);
            if (!organisation.HasResult && organisation.Result == null)
            {
                return NotFound("Organisation not found");
            }
            var recipient = await this.OrganisationsService.GetOrganisation(tosupplierId,
                 token);
            if (!recipient.HasResult && recipient.Result == null)
            {
                return NotFound("Recipient organisation not found");
            }
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            if (obligationPeriod == null)
            {
                return NotFound("Obligation period not found");
            }

            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = supplierId,
                ObligationPeriodId = obligationPeriodId,
                ToSupplierId = tosupplierId,
                SupplierName = organisation.Result.Name,
                RecipientName = recipient.Result.Name,
                DisplaySupplierContext = this.User.IsAdministrator(),
                IsSupplier = organisation.Result.IsSupplier,
                IsTrader = organisation.Result.IsTrader,
                ObligationPeriod = obligationPeriod
            };
            var balanceSummary = await this.BalanceViewService.GetSummary(organisation.Result.Id, obligationPeriodId, token);

            if (balanceSummary.HasResult && balanceSummary.Result != null)
            {
                viewModel.AvailableCreditBalance = balanceSummary.Result.BalanceCredits;
            }
            else
            {
                ModelState.AddModelError("", "Unable to transfer credits. If this issue persists contact system administrator.");
                viewModel.BalanceServiceFailed = true;
            }

            return View("TransferCredits", viewModel);
        }
        /// <summary>
        /// Posts the viewmodel with number of credits to be transferred
        /// </summary>
        /// <param name="supplierId">ID of supplier who is transferring the credits</param>
        /// <param name="tosupplierId">Id of recipient supplier/trader</param>
        /// <param name="obligationPeriodId">Obligation period id</param>
        /// <param name="model">TransferCreditsViewModel</param>
        /// <returns>Returns the confirmation view if successful</returns>
        [HttpPost("obligationPeriod/{obligationPeriodId}/fromOrganisation/{supplierId}/toOrganisation/{tosupplierId}/TransferCredits")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TransferCredits(int supplierId, int tosupplierId, int obligationPeriodId, [FromForm] TransferCreditsViewModel model)
        {
            var token = await this.TokenService.GetJwtToken();
            if (!this.User.CanAccessOrganisationDetails(supplierId))
            {
                return Forbid();
            }
            var organisation = await this.OrganisationsService.GetOrganisation(supplierId, token);
            if (!organisation.HasResult && organisation.Result == null)
            {
                return NotFound("Organisation not found");
            }
            var recipient = await this.OrganisationsService.GetOrganisation(tosupplierId,
                 token);
            if (!recipient.HasResult && recipient.Result == null)
            {
                return NotFound("Recipient organisation not found");
            }
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            if (obligationPeriod == null)
            {
                return NotFound("Obligation period not found");
            }

            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = supplierId,
                ObligationPeriodId = obligationPeriodId,
                ToSupplierId = tosupplierId,
                SupplierName = organisation.Result.Name,
                RecipientName = recipient.Result.Name,
                DisplaySupplierContext = this.User.IsAdministrator(),
                NumberOfCreditsToTransfer = model.NumberOfCreditsToTransfer,
                BalanceServiceFailed = false,
                IsSupplier = organisation.Result.IsSupplier,
                IsTrader = organisation.Result.IsTrader,
                AdditionalReference = model.AdditionalReference,
                ObligationPeriod = obligationPeriod
            };
            var balanceSummary = await this.BalanceViewService.GetSummary(organisation.Result.Id, obligationPeriodId, token);
            if (balanceSummary.HasResult && balanceSummary.Result != null)
            {
                viewModel.AvailableCreditBalance = balanceSummary.Result.BalanceCredits;
            }
            else
            {
                ModelState.AddModelError("", "Unable to transfer credits. If this issue persists contact system administrator.");
                viewModel.BalanceServiceFailed = true;
                return View("TransferCredits", viewModel);
            }

            if (!ModelState.IsValid)
            {
                return View("TransferCredits", viewModel);
            }
            if (viewModel.AvailableCreditBalance < viewModel.NumberOfCreditsToTransfer)
            {
                ModelState.AddModelError(string.Empty, "Number of credits to transfer exceeds the credits available for transfer.");
                return View("TransferCredits", viewModel);
            }
            if (obligationPeriod.LatestDateForRedemption.Value < DateTime.Now)
            {
                ModelState.AddModelError(string.Empty, "Transfers are no longer allowed for the Obligation Period.");
                return View("TransferCredits", viewModel);
            }
            return View("TransferConfirmation", viewModel);

        }
        /// <summary>
        /// Returns transfer confirmation view
        /// </summary>
        /// <param name="creditsViewModel">TransferCreditsViewModel containing the attributes to transfer credits</param>
        /// <returns>Returns transfer confirmation view</returns>
        [HttpPost("TransferConfirmation")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TransferConfirmation([FromForm] TransferCreditsViewModel creditsViewModel)
        {
            var token = await this.TokenService.GetJwtToken();
            if (!this.User.CanAccessOrganisationDetails(creditsViewModel.FromSupplierId))
            {
                return Forbid();
            }
            var viewModel = await GetTransferCreditsViewModel(creditsViewModel.FromSupplierId, creditsViewModel.ToSupplierId
                     , creditsViewModel.ObligationPeriodId
                     , creditsViewModel.NumberOfCreditsToTransfer
                     , creditsViewModel.AdditionalReference
                     , token);
            if (!ModelState.IsValid)
            {
                return View("TransferCredits", viewModel);
            }
            if (viewModel.AvailableCreditBalance < creditsViewModel.NumberOfCreditsToTransfer)
            {
                ModelState.AddModelError(string.Empty, "Number of credits to transfer exceeds the credits available for transfer.");
                return View("TransferCredits", viewModel);
            }

            return View("TransferConfirmation", viewModel);
        }

        /// <summary>
        /// Returns the summary confirmation page after the transfer is completed
        /// </summary>
        /// <param name="creditsViewModel">TransferCreditsViewModel containing the attributes to transfer credits</param>
        /// <returns>Returns the Transfer summary view</returns>
        [HttpPost("TransferredConfirmation")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TransferredConfirmation([FromForm] TransferCreditsViewModel creditsViewModel)
        {
            var token = await this.TokenService.GetJwtToken();
            if (!this.User.CanAccessOrganisationDetails(creditsViewModel.FromSupplierId))
            {
                return Forbid();
            }
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(creditsViewModel.ObligationPeriodId);
            var viewModel = await GetTransferCreditsViewModel(creditsViewModel.FromSupplierId, creditsViewModel.ToSupplierId
                     , creditsViewModel.ObligationPeriodId
                     , creditsViewModel.NumberOfCreditsToTransfer
                     , creditsViewModel.AdditionalReference
                     , token);
            var transferredViewModel = new TransferredCreditsViewModel(viewModel)
            {
                DisplaySupplierContext = this.User.IsAdministrator(),
                IsSupplier = creditsViewModel.IsSupplier,
                IsTrader = creditsViewModel.IsTrader,
                ObligationPeriod = obligationPeriod
            };
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await this.LedgerService.TransferCredits(creditsViewModel.ObligationPeriodId, creditsViewModel.FromSupplierId
                   , creditsViewModel.ToSupplierId, creditsViewModel.NumberOfCreditsToTransfer, creditsViewModel.AdditionalReference, token);

                    if (result > 0)
                    {
                        transferredViewModel.ObligationPeriod = obligationPeriod;
                        transferredViewModel.TransactionId = result;
                        transferredViewModel.BalanceCreditsAfterTransfer = transferredViewModel.AvailableCreditBalance - transferredViewModel.NumberOfCreditsToTransfer;

                        return View("TransferredConfirmation", transferredViewModel);
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "A problem occurred while transferring credits,  please try again.");
                    }
                }
            }
            catch (TransactionThrottlingException)
            {
                Logger.LogError("Unable to transfer credits to the selected recipient because the ledger service is throttling requests");
                ModelState.AddModelError(string.Empty, "Unable to transfer credits because a transfer has recently occurred. Please try again shortly.");
            }
            catch (Exception e)
            {
                Logger.LogError("Unable to transfer credits to the selected recipient" + e.Message);
                ModelState.AddModelError(string.Empty, "A problem occurred while transferring credits,  please try again.");
            }
            return View("TransferConfirmation", transferredViewModel);
        }

        #region Private Methods
        /// <summary>
        /// Rehydrate view model
        /// </summary>
        /// <param name="supplierId">ID of supplier who is transferring the credits</param>
        /// <param name="toSupplierId">Id of recipient supplier/trader</param>
        /// <param name="obligationId">Obligation period id</param>
        /// <param name="value">Credits that needs to be transferred</param>
        /// <param name="additionalReference">Optional additional reference for the transfer</param>
        /// <param name="token">JWT token</param>
        /// <returns>TransferCreditsViewModel</returns>
        private async Task<TransferCreditsViewModel> GetTransferCreditsViewModel(int supplierId, int toSupplierId
            , int obligationId, decimal value, string additionalReference, string token)
        {
            var viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = supplierId,
                ObligationPeriodId = obligationId,
                ToSupplierId = toSupplierId,
                DisplaySupplierContext = this.User.IsAdministrator(),
                NumberOfCreditsToTransfer = value,
                AdditionalReference = additionalReference
            };

            var suppliersResponse = await this.OrganisationsService.GetOrganisation(supplierId,
                 token);
            if (!suppliersResponse.HasResult || suppliersResponse.Result == null)
            {
                ModelState.AddModelError("", "Organisation not found");
                return viewModel;
            }
            var recipient = await this.OrganisationsService.GetOrganisation(toSupplierId,
                token);
            if (!recipient.HasResult || recipient.Result == null)
            {
                ModelState.AddModelError("", "Recipient organisation not found");
                return viewModel;
            }
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationId);
            if (obligationPeriod == null)
            {
                ModelState.AddModelError("", "Obligation period not found");
                return viewModel;
            }
            var balanceSummary = await this.BalanceViewService.GetSummary(supplierId,
                obligationId, token);
            if (!ModelState.IsValid)
                return viewModel;
            viewModel = new TransferCreditsViewModel
            {
                FromSupplierId = supplierId,
                ObligationPeriodId = obligationId,
                ToSupplierId = toSupplierId,
                SupplierName = suppliersResponse.Result.Name,
                RecipientName = recipient.Result.Name,
                AvailableCreditBalance = balanceSummary.Result.BalanceCredits,
                DisplaySupplierContext = this.User.IsAdministrator(),
                NumberOfCreditsToTransfer = value,
                AdditionalReference = additionalReference
            };
            return viewModel;
        }
        #endregion
    }
}