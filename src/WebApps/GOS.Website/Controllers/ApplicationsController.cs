﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.IO;
using System.Threading.Tasks;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.API.Client.Models;
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using DfT.GOS.Organisations.API.Client.Services;
using Microsoft.Extensions.Options;
using DfT.GOS.Website;
using DfT.GOS.Website.Controllers;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Models.ApplicationsViewModels;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Identity.API.Client.Models;
using System.Threading;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Website.Models;
using DfT.GOS.GhgCalculations.Orchestration;
using DfT.GOS.Web.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace GOS.Website.Controllers
{
    /// <summary>
    /// Controller for GHG applications
    /// </summary>
    [Authorize]
    public class ApplicationsController : GosWebsiteControllerBase
    {
        #region Properties

        private IApplicationsService ApplicationsService { get; set; }
        private IGhgFuelsService GhgFuelsService { get; set; }
        private IOrganisationsService OrganisationsService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private ISystemParametersService SystemParametersService { get; set; }
        private ICalculationsService CalculationsService { get; set; }
        private ILogger<ApplicationsController> Logger { get; set; }
        private ITokenService TokenService { get; set; }
        private IUsersService UsersService { get; set; }
        private IOptions<DfT.GOS.Website.AppSettings> AppSettings { get; set; }
        private IReviewApplicationsService ReviewApplicationsService { get; set; }
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="applicationsService">the application service</param>
        /// <param name="ghgFuelsService">the fuels service</param>
        /// <param name="organisationsService">the organisations service</param>
        /// <param name="reportingPeriodsService">the reporting periods service</param>
        /// <param name="systemParametersService">the systemparameters service</param>
        /// <param name="calculationsService">the calculation service</param>
        /// <param name="usersService">the user service</param>
        /// <param name="logger">logger</param>
        /// <param name="tokenService">The token service for gtting a JWT token</param>
        /// <param name="appSettings">Appsettings</param>
        /// <param name="reviewApplicationsService">the review application service</param>
        public ApplicationsController(
            IApplicationsService applicationsService
            , IGhgFuelsService ghgFuelsService
            , IOrganisationsService organisationsService
            , IReportingPeriodsService reportingPeriodsService
            , ISystemParametersService systemParametersService
            , ICalculationsService calculationsService
            , IUsersService usersService
            , ILogger<ApplicationsController> logger
            , ITokenService tokenService
            , IOptions<AppSettings> appSettings
            , IReviewApplicationsService reviewApplicationsService)
        {
            this.Logger = logger;
            this.ApplicationsService = applicationsService;
            this.GhgFuelsService = ghgFuelsService;
            this.OrganisationsService = organisationsService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.SystemParametersService = systemParametersService;
            this.CalculationsService = calculationsService;
            this.TokenService = tokenService;
            this.AppSettings = appSettings;
            this.UsersService = usersService;
            this.ReviewApplicationsService = reviewApplicationsService;
        }

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Returns the view for creating a new GHG application
        /// </summary>
        /// <param name="supplierId">The organisation of the supplier the application will relate to</param>
        /// <param name="obligationPeriodId">The obligation period the application is being reported under</param>
        /// <returns>Returns the create new GHG application view if the input parameters are resolved</returns>
        [Authorize(Roles = "Administrator, Supplier")]
        [HttpGet("/suppliers/{supplierId}/obligationperiods/{obligationPeriodId}/applications/new")]
        public async Task<IActionResult> New(int supplierId, int obligationPeriodId)
        {
            if (this.User.CanAccessOrganisationDetails(supplierId))
            {
                var viewModel = await this.GetApplicationDetailsViewModel(supplierId, obligationPeriodId);
                if (viewModel.ObligationPeriod == null || viewModel.Supplier == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("Details", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Creates a new GHG application
        /// </summary>
        /// <param name="supplierId">The organisation of the supplier the application will relate to</param>
        /// <param name="obligationPeriodId">The obligation period the application is being reported under</param>
        /// <param name="viewModel">View model containing the application details</param>
        /// <returns>Returns the next view in the process if the application was created, otherwise rehydrates 
        /// the application creation view for validation error checking.</returns>
        [HttpPost("/suppliers/{supplierId}/obligationperiods/{obligationPeriodId}/applications/new")]
        [Authorize(Roles = "Administrator, Supplier")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> New(int supplierId, int obligationPeriodId, ApplicationDetailsViewModel viewModel)
        {
            if (this.User.CanAccessOrganisationDetails(supplierId))
            {
                if (ModelState.IsValid)
                {
                    //Issue the create application command to the service layer
                    decimal amountSupplied;
                    int fuelTypeId;
                    decimal? ghgIntensity = null;
                    string reference = null;

                    switch (viewModel.FuelCategory)
                    {
                        case (ApplicationDetailsViewModel.FuelCategories.Electricity):
                            amountSupplied = viewModel.ElectricityAmountSupplied.Value;
                            fuelTypeId = viewModel.ElectricityFuelTypeId;
                            ghgIntensity = viewModel.ElectricityGhgIntensity;
                            break;
                        case (ApplicationDetailsViewModel.FuelCategories.FossilGas):
                            amountSupplied = viewModel.FossilGasVolume.Value;
                            fuelTypeId = viewModel.FossilGasTypeId.Value;
                            break;
                        case (ApplicationDetailsViewModel.FuelCategories.UER):
                            amountSupplied = viewModel.UerAmount.Value;
                            reference = viewModel.UerReference;
                            fuelTypeId = viewModel.UerFuelTypeId;
                            break;
                        default: throw new ArgumentOutOfRangeException("Unsupported Fuel Category supplied");
                    }

                    var command = new CreateApplicationCommand(obligationPeriodId
                        , supplierId
                        , amountSupplied
                        , fuelTypeId
                        , ghgIntensity
                        , reference);

                    try
                    {
                        var jwtToken = await this.TokenService.GetJwtToken();
                        var applicationId = await this.ApplicationsService.CreateNewApplication(command, jwtToken);
                        if (applicationId == 0)
                        {
                            throw new NullReferenceException("Application was not created.");
                        }

                        //Reditect to the next page
                        return Redirect(string.Format("/applications/{0}/SupportingDocuments", applicationId));
                    }
                    catch (Exception ex)
                    {
                        //An error occurred attempting to create the application record - handle this gracefully!
                        this.Logger.LogError("GHG Application was not created. Inner exception details: " + ex.ToString());
                        ModelState.AddModelError(string.Empty, "The application could not be created at this time, please try again.");
                        var newViewModel = await this.GetApplicationDetailsViewModel(supplierId, obligationPeriodId, viewModel);
                        return View("Details", newViewModel);
                    }
                }
                else
                {
                    //There were validation errors - get the constituent parts (in parallel) to rehydrate the view model
                    var newViewModel = await this.GetApplicationDetailsViewModel(supplierId, obligationPeriodId, viewModel);

                    if (newViewModel == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        return View("Details", newViewModel);
                    }
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Gets the supporting documents view for an application
        /// </summary>
        /// <param name="applicationId">The application to view the supporting documents for</param>
        /// <returns>Returns the supporting documents view</returns>
        [Authorize(Roles = "Administrator, Supplier")]
        [HttpGet("applications/{applicationId}/supportingdocuments")]
        public async Task<IActionResult> SupportingDocuments(int applicationId)
        {
            //Check the user can access the application, based upon their organisation details
            var jwtToken = await this.TokenService.GetJwtToken();
            var applicationResponse = await this.ApplicationsService.GetApplicationSummary(applicationId, jwtToken);
            if (applicationResponse.HasResult)
            {
                if (this.User.CanAccessOrganisationDetails(applicationResponse.Result.SupplierOrganisationId))
                {
                    var viewModel = await this.GetApplicationSupportingDocumentsViewModel(applicationResponse.Result);
                    if (applicationResponse.Result.ApplicationStatusId != (int)ApplicationStatus.Open)
                    {
                        ModelState.AddModelError(string.Empty, "Cannot modify a submitted application");
                        return View("SupportingDocumentError", viewModel);
                    }
                    return View("SupportingDocuments", viewModel);
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationResponse);
            }
        }

        /// <summary>
        /// Upload Supporting document.
        /// </summary>
        /// <param name="fileDetails">The supporting document.</param>
        /// <param name="applicationId">The application identifier.</param>
        /// <returns>Updated View or Forbid</returns>
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Supplier")]
        [HttpPost("/applications/{applicationId}/supportingdocuments")]
        public async Task<IActionResult> SupportingDocuments(List<IFormFile> fileDetails, int applicationId)
        {
            //Check the user can access the application, based upon their organisation details
            var jwtToken = await this.TokenService.GetJwtToken();
            var applicationResponse = await this.ApplicationsService.GetApplicationSummary(applicationId, jwtToken);

            if (applicationResponse.HasResult)
            {
                var supplierTask = await this.OrganisationsService.GetOrganisation(applicationResponse.Result.SupplierOrganisationId
                , jwtToken);
                if (!supplierTask.HasResult)
                {
                    return this.HandleFailedHttpObjectResponse(supplierTask);
                }
                if (this.User.CanAccessOrganisationDetails(applicationResponse.Result.SupplierOrganisationId))
                {
                    if (applicationResponse.Result.ApplicationStatusId != (int)ApplicationStatus.Open)
                    {
                        ModelState.AddModelError(string.Empty, "Cannot modify a submitted application");
                        return View("SupportingDocumentError", new ApplicationSupportingDocumentsViewModel(supplierTask.Result));
                    }
                    if (fileDetails == null)
                    {
                        ModelState.AddModelError("File", "No file selected for upload.");
                    }
                    else
                    {
                        try
                        {
                            var fileErrorCount = 0;
                            foreach (var formFile in fileDetails)
                            {
                                var metaFile = new FileMetadata();
                                metaFile.FileExtension = Path.GetExtension(formFile.FileName);
                                metaFile.FileName = formFile.FileName;
                                metaFile.FileSizeBytes = formFile.Length;

                                var validationResponse = await this.ApplicationsService.ValidateFileMetadata(metaFile, jwtToken);
                                if (validationResponse.Count > 0)
                                {
                                    fileErrorCount++;
                                    foreach (var message in validationResponse)
                                    {
                                        ModelState.AddModelError("File", message);
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        SupportingDocument supportingDocument = new SupportingDocument()
                                        {
                                            ApplicationId = applicationId
                                        };

                                        //var fileName = ContentDispositionHeaderValue.Parse(formFile.ContentDisposition).FileName.Trim();
                                        using (var reader = new StreamReader(formFile.OpenReadStream()))
                                        {
                                            using (MemoryStream ms = new MemoryStream())
                                            {
                                                reader.BaseStream.CopyTo(ms);
                                                supportingDocument.File = ms.ToArray();
                                            }
                                            supportingDocument.FileName = formFile.FileName;
                                            supportingDocument.ContentType = formFile.ContentType;
                                            supportingDocument.FileExtension = Path.GetExtension(formFile.FileName);
                                        }

                                        var response = await this.ApplicationsService.UploadApplicationSupportingDocument(supportingDocument, jwtToken);

                                        var messages = response.Messages;

                                        if (!response.ScanResult.IsVirusFree)
                                        {

                                            messages.Add($"Your file { formFile.FileName } could not be uploaded because the Virus Scanner reported a problem.");
                                            messages.Add(response.ScanResult.Message);
                                        }


                                        if (messages.Count > 0)
                                        {
                                            fileErrorCount++;

                                            foreach (var message in messages)
                                            {
                                                ModelState.AddModelError("File", message);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        fileErrorCount++;
                                        ModelState.AddModelError("File", "Error occured while uploading file");
                                        this.Logger.LogError("Could not upload file. Exception details:" + ex.ToString());
                                    }
                                }
                            }
                            if (fileErrorCount > 0)
                            {
                                ModelState.AddModelError("File", $"{ fileErrorCount } of {fileDetails.Count} failed while uploading.");
                            }


                        }
                        catch (Exception ex)
                        {
                            ModelState.AddModelError("File", "Could not get file details.");
                            this.Logger.LogError("Could not get file details. Exception details: " + ex.ToString());
                        }
                    }
                    if ((fileDetails == null || fileDetails.Count == 0) && ModelState.IsValid)
                    {
                        return RedirectToAction("Submit", new { applicationId = applicationId });
                    }
                    else
                    {
                        var viewModel = await this.GetApplicationSupportingDocumentsViewModel(applicationResponse.Result);
                        return View("SupportingDocuments", viewModel);
                    }


                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationResponse);
            }
        }

        /// <summary>
        /// Removes the supporting document for an application
        /// </summary>
        /// <param name="applicationId">The application to view the supporting documents for</param>
        /// <param name="supportingDocumentId">The ID off the supporting document to delete</param>
        /// <returns>Returns the supporting documents view</returns>
        [Authorize(Roles = "Administrator, Supplier")]
        [HttpGet("applications/{applicationId}/supportingdocuments/{supportingDocumentId}/remove")]
        public async Task<IActionResult> RemoveSupportingDocument(int applicationId, int supportingDocumentId)
        {
            //Check the user can access the application, based upon their organisation details
            var jwtToken = await this.TokenService.GetJwtToken();
            var applicationResponse = await this.ApplicationsService.GetApplicationSummary(applicationId, jwtToken);
            if (applicationResponse.HasResult)
            {
                if (this.User.CanAccessOrganisationDetails(applicationResponse.Result.SupplierOrganisationId))
                {

                    var viewModel = new ApplicationSupportingDocumentRemoveViewModel();
                    var applicationDetailViewTask = this.GetApplicationDetailsViewModel(applicationResponse.Result.SupplierOrganisationId, applicationResponse.Result.ObligationPeriodId);
                    var supportingDocumentTask = this.ApplicationsService.GetSupportingDocument(applicationId, supportingDocumentId, jwtToken);

                    await Task.WhenAll(
                        applicationDetailViewTask,
                        supportingDocumentTask);

                    if (applicationDetailViewTask.Result.ObligationPeriod == null)
                    {
                        ModelState.AddModelError("Remove", "Application obligation period not found");
                        this.Logger.LogError("Error removing document. Application obligation period not found.");
                        return View("SupportingDocumentRemove", viewModel);
                    }

                    var supportingDocumentResponse = supportingDocumentTask.Result;

                    if (!supportingDocumentResponse.HasResult
                        || supportingDocumentResponse.Result.FileName == null)
                    {
                        ModelState.AddModelError("Remove", "File not found");
                        this.Logger.LogError("Error removing document. File not found.");
                        return View("SupportingDocumentRemove", viewModel);
                    }

                    viewModel.ApplicationId = applicationId;
                    viewModel.FileName = supportingDocumentResponse.Result.FileName;
                    viewModel.ObligationPeriod = applicationDetailViewTask.Result.ObligationPeriod;
                    viewModel.ShowOrganisationContext = User.IsAdministrator();
                    viewModel.Supplier = applicationDetailViewTask.Result.Supplier;
                    if (applicationResponse.Result.ApplicationStatusId != (int)ApplicationStatus.Open)
                    {
                        ModelState.AddModelError(string.Empty, "Cannot modify a submitted application");
                    }
                    return View("SupportingDocumentRemove", viewModel);
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationResponse);
            }
        }

        /// <summary>
        /// Removes the supporting document for an application
        /// </summary>
        /// <param name="applicationId">The application to view the supporting documents for</param>
        /// <returns>Returns the supporting documents view</returns>
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Supplier")]
        [HttpPost("applications/{applicationId}/supportingdocuments/{supportingDocumentId}/remove")]
        public async Task<IActionResult> RemoveSupportingDocumentPost(ApplicationSupportingDocumentsViewModel model, int applicationId, int supportingDocumentId)
        {
            //Check the user can access the application, based upon their organisation details
            var jwtToken = await this.TokenService.GetJwtToken();
            var applicationResponse = await this.ApplicationsService.GetApplicationSummary(applicationId, jwtToken);
            if (applicationResponse.HasResult)
            {
                if (this.User.CanAccessOrganisationDetails(applicationResponse.Result.SupplierOrganisationId))
                {

                    if (applicationResponse.Result.ApplicationStatusId != (int)ApplicationStatus.Open)
                    {
                        ModelState.AddModelError(string.Empty, "Cannot modify a submitted application");
                        return Redirect(string.Format("/applications/{0}/SupportingDocuments", applicationId));
                    }
                    var applicationTask = this.GetApplicationDetailsViewModel(applicationResponse.Result.SupplierOrganisationId, applicationResponse.Result.ObligationPeriodId);

                    await Task.WhenAll(applicationTask);

                    var viewModel = new ApplicationSupportingDocumentsViewModel(applicationTask.Result.Supplier);
                    viewModel.ApplicationId = applicationId;
                    viewModel.ShowOrganisationContext = User.IsAdministrator();
                    viewModel.ObligationPeriod = applicationTask.Result.ObligationPeriod;

                    var result = await this.ApplicationsService.RemoveSupportingDocument(applicationId, supportingDocumentId, jwtToken);
                    if (!result)
                    {
                        this.Logger.LogError("Error removing document.");
                    }
                    return Redirect(string.Format("/applications/{0}/SupportingDocuments", applicationId));
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationResponse);
            }
        }

        /// <summary>
        /// Downloads the supporting document for an application
        /// </summary>
        /// <param name="applicationId">The application to view the supporting documents for</param>
        /// <returns>Returns the current view and downloads the file</returns>
        [Authorize(Roles = "Administrator, Supplier")]
        [HttpGet("applications/{applicationId}/supportingdocuments/{supportingDocumentId}")]
        public async Task<IActionResult> DownloadSupportingDocument(int applicationId, int supportingDocumentId)
        {
            //Check the user can access the application, based upon their organisation details
            var jwtToken = await this.TokenService.GetJwtToken();
            var applicationResponse = await this.ApplicationsService.GetApplicationSummary(applicationId, jwtToken);
            if (applicationResponse.HasResult)
            {
                if (this.User.CanAccessOrganisationDetails(applicationResponse.Result.SupplierOrganisationId))
                {
                    var supportingDocumentResponse = await this.ApplicationsService.GetSupportingDocument(applicationId, supportingDocumentId, jwtToken);
                    if (supportingDocumentResponse.HasResult)
                    {
                        return File(supportingDocumentResponse.Result.File, "application/x-msdownload", supportingDocumentResponse.Result.FileName);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationResponse);
            }
        }

        /// <summary>
        /// Returns the view to enable an application to be submitted to RTFO
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <returns>Returns the submit application view</returns>
        [Authorize(Roles = "Administrator, Supplier")]
        [HttpGet("applications/{applicationId}/submit")]
        public async Task<IActionResult> Submit(int applicationId)
        {
            var viewModel = new SubmitApplicationViewModel();
            var applicationResponse = await this.ApplicationsService.GetApplicationSummary(applicationId, await this.TokenService.GetJwtToken());
            if (applicationResponse.HasResult)
            {
                if (this.User.CanAccessOrganisationDetails(applicationResponse.Result.SupplierOrganisationId))
                {
                    viewModel = await GetSubmitApplicationViewModel(viewModel, applicationResponse.Result);
                    if (applicationResponse.Result.ApplicationStatusId != (int)ApplicationStatus.Open)
                    {
                        ModelState.AddModelError(string.Empty, "Cannot modify a submitted application");
                        return View("Submit", viewModel);
                    }
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationResponse);
            }
            return View("Submit", viewModel);
        }


        /// <summary>
        /// Submit an application
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("applications/{applicationId}/submit")]
        [Authorize(Roles = "Administrator, Supplier")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SubmitApplication(SubmitApplicationViewModel viewApplicationModel)
        {
            var command = new SubmitApplicationCommand(viewApplicationModel.ApplicationId);
            var applicationResponse = await this.ApplicationsService.GetApplicationSummary(viewApplicationModel.ApplicationId, await this.TokenService.GetJwtToken());
            if (applicationResponse.HasResult)
            {
                if (this.User.CanAccessOrganisationDetails(applicationResponse.Result.SupplierOrganisationId))
                {
                    if (applicationResponse.Result.ApplicationStatusId != (int)ApplicationStatus.Open)
                    {
                        ModelState.AddModelError(string.Empty, "Cannot modify a submitted application");
                        return View("Submit");
                    }
                    if (!this.User.IsAdministrator())
                    {
                        var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(applicationResponse.Result.ObligationPeriodId);
                        var latestDateForApplication = obligationPeriod.LatestDateForApplicationsFromSupplier;
                        if (latestDateForApplication != null && DateTime.Compare(latestDateForApplication.Value, DateTime.Now) < 0 && !viewApplicationModel.IsLateApplication)
                        {
                            var newViewApplicationModel = await GetSubmitApplicationViewModel(viewApplicationModel, applicationResponse.Result);
                            newViewApplicationModel.IsLateApplication = true;
                            return View("Submit", newViewApplicationModel);
                        }
                    }
                    var result = await this.ApplicationsService.SubmitApplication(command, await this.TokenService.GetJwtToken());
                    if (result.Length > 0)
                    {
                        ModelState.AddModelError("Submit", result);
                        var newViewApplicationModel = await GetSubmitApplicationViewModel(viewApplicationModel, applicationResponse.Result);
                        newViewApplicationModel.IsLateApplication = false;
                        return View("Submit", newViewApplicationModel);
                    }
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationResponse);
            }

            return Redirect(string.Format("/applications/{0}/submitted", viewApplicationModel.ApplicationId));
        }

        /// <summary>
        /// Returns the view to confirm that the application has been submitted
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <returns>Returns the submitted Confirmation application view</returns>
        [Authorize(Roles = "Administrator, Supplier")]
        [HttpGet("applications/{applicationId}/Submitted")]
        public async Task<IActionResult> Submitted(int applicationId)
        {
            var viewModel = new SubmitApplicationViewModel();

            var applicationResponse = await this.ApplicationsService.GetApplicationSummary(applicationId, await this.TokenService.GetJwtToken());
            if (applicationResponse.HasResult)
            {
                if (this.User.CanAccessOrganisationDetails(applicationResponse.Result.SupplierOrganisationId))
                {
                    viewModel = await GetSubmitApplicationViewModel(viewModel, applicationResponse.Result);
                    viewModel = new SubmittedApplicationViewModel(viewModel);
                    var command = new SubmitApplicationCommand(applicationId);
                    viewModel.ApplicationId = applicationId;
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationResponse);
            }
            return View("Submitted", viewModel);
        }

        /// <summary>
        /// Gets the applications specific to the supplier/organisation and obligation period
        /// </summary>
        /// <param name="organisationId">The supplier/organisation id</param>
        /// <param name="obligationId">The obligation period id</param>
        /// <param name="page">page number</param>
        /// <returns></returns>
        [HttpGet("/applications/supplier/{organisationId}")]
        [HttpGet("/applications/supplier/{organisationId}/obligationPeriod/{obligationId}")]
        public async Task<IActionResult> SupplierApplications(int organisationId, int obligationId = 0, [FromQuery]int page = 1)
        {
            var token = await this.TokenService.GetJwtToken();
            PagedSupplierApplicationViewModel pagedResult;
            ObligationPeriod obligationPeriod;
            if (!this.User.CanAccessOrganisationDetails(organisationId))
            {
                return Forbid();
            }
            var organisation = await this.OrganisationsService.GetOrganisation(organisationId, token);
            if (!organisation.HasResult)
            {
                return NotFound("Organisation not found");
            }
            if (obligationId == 0)
            {
                var obligation = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                if (obligation == null)
                {
                    pagedResult = new PagedSupplierApplicationViewModel
                    {
                        Result = new UIPagedResult<SupplierApplicationViewModel>
                        {
                            MaximumPagesToDisplay = AppSettings.Value.Pagination.MaximumPagesToDisplay,
                            PageNumber = page,
                            PageSize = AppSettings.Value.Pagination.ResultsPerPage,
                            TotalResults = 0,
                            Result = new List<SupplierApplicationViewModel>()
                        },
                        ShowOrganisationContext = User.IsAdministrator(),
                        Supplier = organisation.Result
                    };
                    ModelState.AddModelError("ObligationPeriod", "Select a obligation period to continue");
                    return View("SupplierApplications", pagedResult);
                }
                obligationPeriod = obligation;
                obligationId = obligation.Id;
            }
            else
            {
                obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationId);
                if (obligationPeriod == null)
                {
                    return NotFound("Invalid obligation period");
                }
            }
            var result = await this.ApplicationsService.GetSupplierApplications(organisationId
                , obligationId
               , this.AppSettings.Value.Pagination.ResultsPerPage
               , page
               , await this.TokenService.GetJwtToken());

            pagedResult = await GetPagedSupplierApplicationsViewModel(organisation.Result, page, result);
            pagedResult.ObligationPeriod = obligationPeriod;
            return View("SupplierApplications", pagedResult);
        }

        /// <summary>
        /// Application details
        /// </summary>
        /// <param name="supplierId">Supplier/Organisation id</param>
        /// <param name="applicationId">Application identifier</param>
        /// <returns></returns>
        [HttpGet("/applications/{applicationId}/details")]
        public async Task<IActionResult> ApplicationDetails(int applicationId)
        {
            return await GetSupplierApplicationDetails(applicationId);
        }

        /// <summary>
        /// Returns the view to confirm to revoke an application
        /// </summary>
        /// <returns>Returns the application recovation view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("applications/{applicationId}/revoke")]
        public async Task<IActionResult> Revoke(int applicationId)
        {
            var token = await this.TokenService.GetJwtToken();
            var application = await this.ApplicationsService.GetApplicationSummary(applicationId, token);
            if (!application.HasResult || application.Result == null)
            {
                if (application.HttpStatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    return Forbid();
                }
                return NotFound("Application not found");
            }
            if (!this.User.CanAccessOrganisationDetails(application.Result.SupplierOrganisationId))
            {
                return Forbid();
            }

            var organisationTask = this.OrganisationsService.GetOrganisation(application.Result.SupplierOrganisationId
                , token);
            var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(application.Result.ObligationPeriodId);
            var itemsResponseTask = this.ApplicationsService.GetApplicationItems(application.Result.ApplicationId, token);
            var fuelTypesTask = this.GhgFuelsService.GetFuelTypes();

            await Task.WhenAll(organisationTask, obligationPeriodTask, itemsResponseTask, fuelTypesTask);

            var fuelTypes = await fuelTypesTask;
            var itemsResponse = await itemsResponseTask;
            var obligationPeriod = await obligationPeriodTask;

            var calculationOrchestrator = new ApplicationsCalculationOrchestrator(this.ApplicationsService, this.CalculationsService, fuelTypes, token);

            var calculationResult = await calculationOrchestrator.GetCalculationBreakdownForApplicationItems(itemsResponse.Result
                , application.Result.SupplierOrganisationId
                , application.Result.ObligationPeriodId
                , obligationPeriod.GhgThresholdGrammesCO2PerMJ.Value
                , obligationPeriod.DeMinimisLitres
                , obligationPeriod.NoDeMinimisLitres
                , obligationPeriod.GhgLowVolumeSupplierDeductionKGCO2.Value);

            var viewModel = new ApplicationRevocationViewModel()
            {
                ApplicationId = application.Result.ApplicationId,
                Supplier = organisationTask.Result.Result,
                ObligationPeriod = obligationPeriod,
                ShowOrganisationContext = User.IsAdministrator(),
                TotalCredits = calculationResult.NetTotalCredits,
                LatestDateForRevocation = obligationPeriod.LatestDateForRevocation.Value,
                RevocationReason = application.Result.RevocationReason
            };

            return View("ApplicationRevocation", viewModel);
        }

        /// <summary>
        /// Returns the view to the application details page
        /// </summary>
        /// <returns>Returns the application recovation view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpPost("applications/{applicationId}/revoke")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Revoke(int applicationId, string revocationReason)
        {
            var command = new RevokeApplicationCommand(applicationId, revocationReason);
            try
            {
                var jwtToken = await this.TokenService.GetJwtToken();
                await this.ApplicationsService.RevokeApplication(command, jwtToken);

                var application = await this.ApplicationsService.GetApplicationSummary(applicationId, jwtToken);
                if (!application.HasResult || application.Result == null)
                {
                    if (application.HttpStatusCode == System.Net.HttpStatusCode.Forbidden)
                    {
                        return Forbid();
                    }
                    return NotFound("Application not found");
                }
                if (!this.User.CanAccessOrganisationDetails(application.Result.SupplierOrganisationId))
                {
                    return Forbid();
                }

                var organisationTask = this.OrganisationsService.GetOrganisation(application.Result.SupplierOrganisationId, jwtToken);
                var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(application.Result.ObligationPeriodId);

                await Task.WhenAll(organisationTask, obligationPeriodTask);

                var organisation = await organisationTask;
                var obligationPeriod = await obligationPeriodTask;

                var viewModel = new ApplicationRevocationConfirmationViewModel()
                {
                    ApplicationId = application.Result.ApplicationId,
                    Supplier = organisation.Result,
                    ObligationPeriod = obligationPeriod
                };

                return View("ApplicationRevocationConfirmation", viewModel);
            }
            catch (Exception ex)
            {
                this.Logger.LogError("GHG Application was not revoked. Inner exception details: " + ex.ToString());
                ModelState.AddModelError(string.Empty, "The application could not be revoked at this time, please try again.");
                return Redirect(string.Format("/applications/{0}/details", applicationId));
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Gets a an Application Detail View model for the details view based upon an existing application detail view model
        /// </summary>
        /// <param name="supplierId">The ID of the application's supplier</param>
        /// <param name="obligationPeriodId">The ID of the application's obligation period</param>
        /// <returns>Returns the application details view model if all parameters were resolved, otherwise returns NULL</returns>
        private async Task<ApplicationDetailsViewModel> GetApplicationDetailsViewModel(int supplierId, int obligationPeriodId, ApplicationDetailsViewModel viewModel)
        {
            //Get constituent parts in parallel
            var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            var organisationTask = this.OrganisationsService.GetOrganisation(supplierId,
                await this.TokenService.GetJwtToken());
            var fossilGasFuelTypesTask = this.GhgFuelsService.GetFossilGasFuelTypes();
            var electricityFuelTypeTask = this.GhgFuelsService.GetElectricityFuelType();
            var uerFuelTypeTask = this.GhgFuelsService.GetUerFuelType();

            await Task.WhenAll(obligationPeriodTask
                , organisationTask
                , fossilGasFuelTypesTask
                , electricityFuelTypeTask
                , uerFuelTypeTask);

            var obligationPeriod = obligationPeriodTask.Result;
            var organisationResponse = organisationTask.Result;
            var fossilGasFuelTypes = fossilGasFuelTypesTask.Result;
            var electricityFuelType = electricityFuelTypeTask.Result;
            var uerFuelType = uerFuelTypeTask.Result;

            //Check valid obligation period ID and organisation IDs were passed
            if (obligationPeriod == null || organisationResponse == null
                || !organisationResponse.HasResult)
            {
                return viewModel;
            }
            else
            {
                viewModel.ObligationPeriod = obligationPeriod;
                viewModel.FossilGasFuelTypes = fossilGasFuelTypes;

                viewModel.ElectricityFuelTypeId = electricityFuelType.FuelTypeId;
                viewModel.UerFuelTypeId = uerFuelType.FuelTypeId;
                viewModel.Supplier = organisationResponse.Result;
                viewModel.ShowOrganisationContext = User.IsAdministrator();

                return viewModel;
            }
        }

        /// <summary>
        /// Gets a new Application Detail View model for the details view
        /// </summary>
        /// <param name="supplierId">The ID of the application's supplier</param>
        /// <param name="obligationPeriodId">The ID of the application's obligation period</param>
        /// <returns>Returns the application details view model if all parameters were resolved, otherwise returns NULL</returns>
        private async Task<ApplicationDetailsViewModel> GetApplicationDetailsViewModel(int supplierId, int obligationPeriodId)
        {
            return await this.GetApplicationDetailsViewModel(supplierId, obligationPeriodId, new ApplicationDetailsViewModel());
        }



        /// <summary>
        /// Gets the submit application view model
        /// </summary>
        /// <param name="viewModel">SubmitApplicationViewModel</param>
        /// <param name="application">Application</param>
        /// <returns>SubmitApplicationViewModel</returns>
        private async Task<SubmitApplicationViewModel> GetSubmitApplicationViewModel(SubmitApplicationViewModel viewModel, ApplicationSummary application)
        {
            var jwtToken = await this.TokenService.GetJwtToken();

            var organisationTask = this.OrganisationsService.GetOrganisation(application.SupplierOrganisationId, jwtToken);
            var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(application.ObligationPeriodId);
            var itemsResponseTask = this.ApplicationsService.GetApplicationItems(application.ApplicationId, jwtToken);
            var fuelTypesTask = this.GhgFuelsService.GetFuelTypes();
            var systemParametersTask = this.SystemParametersService.GetSystemParameters();

            await Task.WhenAll(organisationTask, obligationPeriodTask, itemsResponseTask, fuelTypesTask, systemParametersTask);

            var applicationItems = await this.GetApplicationItemViewModel(application.ApplicationId, itemsResponseTask.Result.Result);
            var organisationResponse = organisationTask.Result;
            var fuelTypes = await fuelTypesTask;
            var obligationPeriod = await obligationPeriodTask;
            var systemParameters = await systemParametersTask;

            var calculationOrchestrator = new ApplicationsCalculationOrchestrator(this.ApplicationsService, this.CalculationsService, fuelTypes, jwtToken);
            var calculationResult = await calculationOrchestrator.GetCalculationBreakdownForApplicationItems(itemsResponseTask.Result.Result
                , application.SupplierOrganisationId
                , application.ObligationPeriodId
                , obligationPeriod.GhgThresholdGrammesCO2PerMJ.Value
                , obligationPeriod.DeMinimisLitres
                , obligationPeriod.NoDeMinimisLitres
                , obligationPeriod.GhgLowVolumeSupplierDeductionKGCO2.Value);

            var newViewModel = new SubmitApplicationViewModel()
            {
                ApplicationId = application.ApplicationId,
                Supplier = organisationResponse.Result,
                ObligationPeriod = obligationPeriod,
                ApplicationItems = new List<ApplicationItemViewModel>(),
                ShowOrganisationContext = User.IsAdministrator(),
                TotalCredits = calculationResult.NetTotalCredits,
                RtfoSupportTelephoneNumber = systemParameters.RtfoSupportTelephoneNumber,
                RtfoSupportEmailAddress = systemParameters.RtfoSupportEmailAddress,
                IsLateApplication = false
            };

            if (applicationItems != null)
            {
                foreach (var item in applicationItems)
                {
                    var calculationItem = calculationResult.GetAllCalculationItems().SingleOrDefault(ci => ci.Reference == item.ApplicationItemId.ToString());
                    item.Credits = calculationItem.Credits;
                    item.Obligation = calculationItem.Obligation;

                    newViewModel.ApplicationItems.Add(item);
                }
            }

            return newViewModel;
        }

        /// <summary>
        /// Gets the view model for application supporting documents
        /// </summary>
        /// <param name="application">The application to get the view model for</param>
        /// <returns>Returns an instance of application supporting document view model</returns>
        private async Task<ApplicationSupportingDocumentsViewModel> GetApplicationSupportingDocumentsViewModel(ApplicationSummary application)
        {
            var jwtToken = await this.TokenService.GetJwtToken();

            var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(application.ObligationPeriodId);
            var supplierTask = this.OrganisationsService.GetOrganisation(application.SupplierOrganisationId
                , await this.TokenService.GetJwtToken());
            var documentTypesListTask = this.SystemParametersService.GetDocumentTypes();
            var supportingDocumentsTask = this.ApplicationsService.GetSupportingDocuments(application.ApplicationId, jwtToken);

            await Task.WhenAll(obligationPeriodTask
                , supplierTask
                , documentTypesListTask
                , supportingDocumentsTask);

            var supplierResponse = supplierTask.Result;
            var documentTypesResponse = documentTypesListTask.Result;
            var supportingDocumentsResponse = supportingDocumentsTask.Result;

            var supportedExtensions = string.Empty;
            foreach (var type in documentTypesResponse.Result)
            {
                supportedExtensions = supportedExtensions + "." + type.FileExtension + ",";
            }
            supportedExtensions = supportedExtensions.Remove(supportedExtensions.Length - 1);

            var viewModel = new ApplicationSupportingDocumentsViewModel(supplierResponse.Result);
            viewModel.ApplicationId = application.ApplicationId;
            viewModel.ShowOrganisationContext = User.IsAdministrator();
            viewModel.AcceptedFileTypes = supportedExtensions;
            viewModel.ApplicationSupportingDocuments = supportingDocumentsResponse.Result;
            viewModel.ObligationPeriod = obligationPeriodTask.Result;
            return viewModel;
        }

        /// <summary>
        /// Get the application item view model
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <param name="applicationItems">List of application items to display</param>
        /// <returns>List of ApplicationItemViewModel</returns>
        private async Task<IList<ApplicationItemViewModel>> GetApplicationItemViewModel(int applicationId, IList<ApplicationItemSummary> applicationItems)
        {
            var appItemsModel = new List<ApplicationItemViewModel>();

            var fossilFuelTypes = this.GhgFuelsService.GetFossilGasFuelTypes();
            var eFuelType = this.GhgFuelsService.GetElectricityFuelType();
            var uerFuelType = this.GhgFuelsService.GetUerFuelType();

            await Task.WhenAll(fossilFuelTypes, eFuelType, uerFuelType);

            foreach (var item in applicationItems)
            {
                var fuelType = fossilFuelTypes.Result.FirstOrDefault(ftype => ftype.FuelTypeId == item.FuelTypeId);

                //Default the fuel type to the one specified in the item, if there was one. This will be the case for Fossil Gas Items
                var fuelTypeName = fuelType?.Name;
                var fuelTypeId = fuelType?.FuelTypeId;

                if (eFuelType.Result.FuelTypeId == item.FuelTypeId)
                {
                    //This is an Electricity Item
                    fuelTypeName = eFuelType.Result.Name;
                    fuelTypeId = eFuelType.Result.FuelTypeId;
                }
                else if (uerFuelType.Result?.FuelTypeId == item.FuelTypeId)
                {
                    //The is a UER Item
                    fuelTypeName = uerFuelType.Result.Name;
                    fuelTypeId = uerFuelType.Result.FuelTypeId;
                }

                if (!fuelTypeId.HasValue)
                {
                    throw new NullReferenceException("No Fuel Type has been specified for application item with ID " + item.ApplicationItemId);
                }

                var applicationItem = new ApplicationItemViewModel(item.ApplicationId
                    , item.ApplicationItemId
                    , item.Reference
                    , fuelTypeName
                    , fuelTypeId.Value
                    , item.AmountSupplied
                    , item.GhgIntensity
                    , item.EnergyDensity
                    , fuelType != null ? "Kg" : "MJ");

                appItemsModel.Add(applicationItem);
            }

            return appItemsModel;
        }

        /// <summary>
        /// Gets the paged result of supplier application view model specific to organisation/supplier
        /// </summary>
        /// <param name="organisation">The supplier/organisation</param>
        /// <param name="page">page number</param>
        /// <param name="result">PagedResult<ApplicationSummary></param>
        /// <returns></returns>
        private async Task<PagedSupplierApplicationViewModel> GetPagedSupplierApplicationsViewModel(Organisation organisation, int page,
            HttpObjectResponse<PagedResult<ApplicationSummary>> result)
        {
            var pagedResult = new PagedSupplierApplicationViewModel();
            var supplierApplications = new List<SupplierApplicationViewModel>();
            var token = await this.TokenService.GetJwtToken();
            if (result != null && result.HasResult)
            {
                var users = new List<Task<GosApplicationUser>>();
                foreach (var item in result.Result.Result)
                {
                    if (supplierApplications.Count(x => x.SubmittedById == item.SubmittedBy) == 0)
                    {
                        var gosUser = this.UsersService.FindUserByIdAsync(item.SubmittedBy, token, default(CancellationToken));
                        users.Add(gosUser);
                    }
                    var pa = new SupplierApplicationViewModel()
                    {
                        ApplicationId = item.ApplicationId,
                        ApplicationDate = item.SubmittedDate,
                        ApplicationStatus = (ApplicationStatus)Enum.Parse(typeof(ApplicationStatus), item.ApplicationStatusId.ToString()),
                        OrganisationId = item.SupplierOrganisationId,
                        SubmittedById = item.SubmittedBy
                    };

                    supplierApplications.Add(pa);
                }
                await Task.WhenAll(users);
                users.ForEach(x =>
                {
                    if (x.Result != null)
                    {
                        supplierApplications.ForEach(y =>
                        {
                            if (y.SubmittedById == x.Result.Id)
                            {
                                y.SubmittedBy = x.Result.FullName;
                            }
                        });
                    }
                });

                pagedResult.Result = new UIPagedResult<SupplierApplicationViewModel>()
                {
                    Result = supplierApplications,
                    MaximumPagesToDisplay = AppSettings.Value.Pagination.MaximumPagesToDisplay,
                    PageNumber = page,
                    PageSize = AppSettings.Value.Pagination.ResultsPerPage,
                    TotalResults = result.Result.TotalResults
                };

                pagedResult.ShowOrganisationContext = User.IsAdministrator();
                pagedResult.Supplier = organisation;
            }

            return pagedResult;
        }

        /// <summary>
        /// Gets the application details
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <returns></returns>
        private async Task<IActionResult> GetSupplierApplicationDetails(int applicationId)
        {
            var token = await this.TokenService.GetJwtToken();
            var application = await this.ApplicationsService.GetApplicationSummary(applicationId, token);
            if (!application.HasResult || application.Result == null)
            {
                if (application.HttpStatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    return Forbid();
                }
                return NotFound("Application not found");
            }
            if (!this.User.CanAccessOrganisationDetails(application.Result.SupplierOrganisationId))
            {
                return Forbid();
            }
            if (application.Result.ApplicationStatusId == (int)ApplicationStatus.Open)
            {
                return RedirectToRoute(new
                {
                    action = "SupplierApplications",
                    organisationId = application.Result.SupplierOrganisationId
                });
            }
            ApplicationReviewSummary reviewSummary = new ApplicationReviewSummary();
            if (application.Result.ApplicationStatusId == (int)ApplicationStatus.Rejected)
            {
                var reviewResult = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, token);
                if (reviewResult.HasResult && reviewResult.Result != null)
                {
                    reviewSummary = reviewResult.Result;
                }
            }
            var organisationTask = this.OrganisationsService.GetOrganisation(application.Result.SupplierOrganisationId
                , token);
            var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(application.Result.ObligationPeriodId);
            var applicationDocuments = this.ApplicationsService.GetSupportingDocuments(applicationId, token);
            var itemsResponseTask = this.ApplicationsService.GetApplicationItems(application.Result.ApplicationId, token);
            var fuelTypesTask = this.GhgFuelsService.GetFuelTypes();

            await Task.WhenAll(organisationTask, obligationPeriodTask, applicationDocuments, itemsResponseTask, fuelTypesTask);

            var applicationItems = await this.GetApplicationItemViewModel(applicationId, itemsResponseTask.Result.Result);
            var fuelTypes = await fuelTypesTask;
            var obligationPeriod = await obligationPeriodTask;

            var calculationOrchestrator = new ApplicationsCalculationOrchestrator(this.ApplicationsService, this.CalculationsService, fuelTypes, token);

            var calculationResult = await calculationOrchestrator.GetCalculationBreakdownForApplicationItems(itemsResponseTask.Result.Result
                , application.Result.SupplierOrganisationId
                , application.Result.ObligationPeriodId
                , obligationPeriod.GhgThresholdGrammesCO2PerMJ.Value
                , obligationPeriod.DeMinimisLitres
                , obligationPeriod.NoDeMinimisLitres
                , obligationPeriod.GhgLowVolumeSupplierDeductionKGCO2.Value);

            var viewModel = new SupplierApplicationDetailViewModel()
            {
                ApplicationId = application.Result.ApplicationId,
                ApplicationDate = application.Result.SubmittedDate,
                ApplicationStatusId = application.Result.ApplicationStatusId,
                Supplier = organisationTask.Result.Result,
                ObligationPeriod = obligationPeriod,
                ApplicationItems = new List<ApplicationItemViewModel>(),
                ShowOrganisationContext = User.IsAdministrator(),
                TotalCredits = calculationResult.NetTotalCredits,
                ApplicationSupportingDocuments = applicationDocuments.Result.Result,
                VolumesRejectionReason = reviewSummary.ApplicationVolumesRejectionReason,
                CalculationRejectionReason = reviewSummary.CalculationRejectionReason,
                DocumentsRejectionReason = reviewSummary.SupportingDocumentsRejectionReason,
                LatestDateForRevocation = obligationPeriod.LatestDateForRevocation.Value,
                RevocationReason = application.Result.RevocationReason,
                UerFuelTypeId = (int)ApplicationDetailsViewModel.FuelCategories.UER,
                IsAdministrator = User.IsAdministrator(),
                IsStatusCreditsIssued = application.Result.ApplicationStatusId == (int)ApplicationStatus.GHGCreditsIssued,
                IsStatusRevoked = application.Result.ApplicationStatusId == (int)ApplicationStatus.Revoked,
                IsPastRevocationDate = obligationPeriod.LatestDateForRevocation.HasValue && DateTime.Compare(obligationPeriod.LatestDateForRevocation.Value, DateTime.Now) < 0
            };

            if (applicationItems != null)
            {
                foreach (var item in applicationItems)
                {
                    var calculationItem = calculationResult.GetAllCalculationItems().Single(ci => ci.Reference == item.ApplicationItemId.ToString());
                    item.Credits = calculationItem.Credits;
                    item.Obligation = calculationItem.Obligation;
                    viewModel.ApplicationItems.Add(item);
                }
            }
            return View("ApplicationDetails", viewModel);
        }

        #endregion Private Methods
    }
}