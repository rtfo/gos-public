﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgCalculations.Orchestration;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.Security.Services;
using DfT.GOS.Website;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Models.CreditsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GOS.Website.Controllers
{
    /// <summary>
    /// Controller for credits
    /// </summary>
    [Authorize(Roles = Roles.Administrator)]
    public class CreditsController : Controller
    {
        #region Properties

        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private IReviewApplicationsService ReviewApplicationsService { get; set; }
        private IApplicationsService ApplicationsService { get; set; }
        private IOrganisationsService OrganisationsService { get; set; }
        private IGhgFuelsService GhgFuelsService { get; set; }
        private ICalculationsService CalculationsService { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }
        private ITokenService TokenService { get; set; }
        private IBalanceViewService BalanceViewService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking dependencies
        /// </summary>
        /// <param name="appSettings">The application settings configuration</param>
        /// <param name="reviewApplicationsService">The service for application reviews</param>
        /// <param name="applicationsService">The service for application details</param>
        /// <param name="calculationsService">The service for performing calculations</param>
        /// <param name="ghgFuelsService">The service for getting fuels</param>
        /// <param name="organisationsService">The service for getting organisation details</param>
        /// <param name="reportingPeriodsService">The service for getting obligation period details</param>
        /// <param name="tokenService">the token service</param>
        /// <param name="balanceViewService">the service for getting credit transfers</param>
        public CreditsController(IOptions<AppSettings> appSettings
            , IReviewApplicationsService reviewApplicationsService
            , IApplicationsService applicationsService
            , IGhgFuelsService ghgFuelsService
            , ICalculationsService calculationsService
            , IOrganisationsService organisationsService
            , IReportingPeriodsService reportingPeriodsService
            , ITokenService tokenService
            , IBalanceViewService balanceViewService)
        {
            this.AppSettings = appSettings;
            this.ReviewApplicationsService = reviewApplicationsService;
            this.ApplicationsService = applicationsService;
            this.GhgFuelsService = ghgFuelsService;
            this.CalculationsService = calculationsService;
            this.OrganisationsService = organisationsService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.TokenService = tokenService;
            this.BalanceViewService = balanceViewService;
        }

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// View for administrator to issue credits to suppliers
        /// </summary>
        /// <returns>Returns the Issue Credits view</returns>
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> Issue([FromQuery] int? selectedObligationPeriodId, [FromQuery]int page = 1)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            var jwtToken = await this.TokenService.GetJwtToken();
            var pendingCreditIssue = new List<ApplicationReadyForIssueViewModel>();

            var result = await this.ReviewApplicationsService.GetApplicationsReadyForIssuePagedResult(jwtToken
              , this.AppSettings.Value.Pagination.ResultsPerPage
              , page);

            if (result != null
                && result.HasResult
                && result.Result.Result != null)
            {
                var supplierTasks = new List<Task<HttpObjectResponse<Organisation>>>();
                var calculationTasks = new List<Task<ApplicationCalculationSummaryResult>>();

                var fuelTypes = await this.GhgFuelsService.GetFuelTypes();

                var calculationOrchestrator = new ApplicationsCalculationOrchestrator(this.ApplicationsService
                    , this.CalculationsService
                    , fuelTypes
                    , jwtToken);

                var obligationPeriods = new List<ObligationPeriod>();

                foreach (var item in result.Result.Result)
                {
                    ObligationPeriod obligationPeriod;
                    if (obligationPeriods.Exists(op => op.Id == item.ObligationPeriodId))
                    {
                        obligationPeriod = obligationPeriods.Single(op => op.Id == item.ObligationPeriodId);
                    }
                    else
                    {
                        obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(item.ObligationPeriodId);

                        if (obligationPeriod == null)
                        {
                            throw new ArgumentException($"No obligation period could be found with ID {item.ObligationPeriodId}", nameof(page));
                        }

                        obligationPeriods.Add(obligationPeriod);
                    }

                    var calulationResultTask = calculationOrchestrator.GetCalculationSummaryForApplication(item.ApplicationId
                        , item.SupplierOrganisationId
                        , item.ObligationPeriodId
                        , obligationPeriod.GhgThresholdGrammesCO2PerMJ.Value
                        , obligationPeriod.DeMinimisLitres
                        , obligationPeriod.NoDeMinimisLitres
                        , obligationPeriod.GhgLowVolumeSupplierDeductionKGCO2.Value);

                    calculationTasks.Add(calulationResultTask);

                    if (pendingCreditIssue.Count(x => x.SupplierId == item.SupplierOrganisationId) == 0)
                    {
                        var supplier = this.OrganisationsService.GetOrganisation(item.SupplierOrganisationId, jwtToken);
                        supplierTasks.Add(supplier);
                    }

                    var application = new ApplicationReadyForIssueViewModel()
                    {
                        ApplicationId = item.ApplicationId,
                        SupplierId = item.SupplierOrganisationId,
                        SubmittedDate = item.SubmittedDate,
                    };

                    pendingCreditIssue.Add(application);
                }

                //Populate the Supplier Names
                await Task.WhenAll(supplierTasks);

                supplierTasks.ForEach(task =>
                {
                    var updatedViewModel = pendingCreditIssue.Where(x => x.SupplierId == task.Result.Result.Id).Select(x =>
                    {
                        x.Supplier = task.Result.Result.Name;
                        return x;
                    }).ToList();
                });

                //Populate the calculation values
                await Task.WhenAll(calculationTasks);

                foreach (var task in calculationTasks)
                {
                    var pendingApproval = pendingCreditIssue.Single(pa => pa.ApplicationId == task.Result.ApplicationId);
                    pendingApproval.CreditsApplierFor = task.Result.Result.NetTotalCredits;
                }

            }
            var applications = new UIPagedResult<ApplicationReadyForIssueViewModel>()
            {
                Result = pendingCreditIssue,
                MaximumPagesToDisplay = this.AppSettings.Value.Pagination.MaximumPagesToDisplay,
                PageNumber = page,
                PageSize = this.AppSettings.Value.Pagination.ResultsPerPage,
            };

            if (result != null && result.HasResult && result.Result.Result != null)
            {
                applications.TotalResults = result.Result.TotalResults;
            }

            var viewModel = new PagedIssueCreditsViewModel(applications);
            return View("Issue", viewModel);
        }

        /// <summary>
        /// Handles a credit issue request
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = Roles.Administrator)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> Issue([FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            var jwtToken = await this.TokenService.GetJwtToken();
            var result = await this.ReviewApplicationsService.IssueCredits(jwtToken);
            if (result)
            {
                return View("IssueConfirmation");
            }
            else
            {
                ModelState.AddModelError("Error", "Internal error");
                return View("Issue");
            }
        }

        /// <summary>
        /// Gets the a csv file containing all credit transfers for the obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the Obligation Period to filter on</param>
        /// <param name="search">Search term to filter results by organisation name on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <returns>Returns the csv with all credit transfers for the obligaiton period</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("credits/summary/{obligationPeriodId}/csv")]
        public async Task<FileContentResult> SummaryCsv(int obligationPeriodId, [FromQuery]string search)
        {
            var jwtToken = await this.TokenService.GetJwtToken();
            var creditsSummaryFile = await this.BalanceViewService.GetCreditTransfersCsv(obligationPeriodId, search, jwtToken);
            var fileName = "CreditTransfers-ObligationPeriod" + obligationPeriodId + "-" + DateTime.Now.ToString("ddMMyyhhmmss") + ".csv";
            return File(creditsSummaryFile.Result, "application/octet-stream", fileName);
        }

        /// <summary>
        /// Gets the view containing a list of paged credit transfers
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the Obligation Period to filter on</param>
        /// <param name="search">Search term to filter results by organisation name on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <returns>A CreditTransfersSummary ViewModel</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("credits/summary/{obligationPeriodId}")]
        public async Task<IActionResult> Summary(int obligationPeriodId, [FromQuery]string search, [FromQuery]int page = 1)
        {
            var jwtToken = await this.TokenService.GetJwtToken();

            var creditsSummaryTask = this.BalanceViewService.GetCreditTransfers(obligationPeriodId, page, this.AppSettings.Value.Pagination.ResultsPerPage, search, jwtToken);
            var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);

            var creditsSummary = await creditsSummaryTask;
            var obligationPeriod = await obligationPeriodTask;

            if (obligationPeriod == null)
            {
                throw new ArgumentException($"No obligation period could be found with ID {obligationPeriodId}", nameof(obligationPeriodId));
            }

            var pagedresults = new UIPagedResult<CreditTransferSummary>(creditsSummary.Result, this.AppSettings.Value.Pagination.MaximumPagesToDisplay);

            var viewModel = new CreditTransfersViewModel(obligationPeriod.Id, obligationPeriod.EndDate, search, pagedresults);

            if (!string.IsNullOrEmpty(search))
            {
                ViewBag.Query = string.Format("&search={0}", search);
            }

            return View("CreditTransfers", viewModel);
        }

        /// <summary>
        /// Gets the view containing a list of paged credit transfers
        /// </summary>
        /// <param name="model">The view model</param>
        /// <param name="page">The page to go to</param>
        /// <returns>Redirect resolving to CreditTransfersSummary ViewModel</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpPost("/credits/summary/{obligationPeriodId}")]
        [ValidateAntiForgeryToken]
        public IActionResult SummaryPost(CreditTransfersViewModel model, [FromQuery]int page = 1)
        {
            ViewBag.Query = "&search=" + model.SearchText;
            return RedirectToAction("Summary", new { obligationPeriodId = model.ObligationPeriodId, search = model.SearchText, page });
        }

        #endregion Public Methods
    }
}