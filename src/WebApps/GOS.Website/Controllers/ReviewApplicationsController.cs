﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.Common.Exceptions;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgCalculations.Orchestration;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Claims;
using DfT.GOS.Security.Services;
using DfT.GOS.Web.ExtensionMethods;
using DfT.GOS.Web.Models;
using DfT.GOS.Website;
using DfT.GOS.Website.Controllers;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Models.ApplicationsViewModels;
using DfT.GOS.Website.Models.ReviewApplicationsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static DfT.GOS.Website.Models.Review;

namespace GOS.Website.Controllers
{
    /// <summary>
    /// Controller for GHG applications under review
    /// </summary>
    [Authorize(Roles = Roles.Administrator)]
    [Route("Review/Applications")]
    public class ReviewApplicationsController : GosWebsiteControllerBase
    {
        #region Private Members
        private IApplicationsService ApplicationsService { get; set; }
        private IReviewApplicationsService ReviewApplicationsService { get; set; }
        private IGhgFuelsService GhgFuelsService { get; set; }
        private IOrganisationsService OrganisationsService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private ICalculationsService CalculationsService { get; set; }
        private ITokenService TokenService { get; set; }
        private IUsersService UsersService { get; set; }
        private ILogger<ReviewApplicationsController> Logger { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="applicationsService">the applications service</param>
        /// <param name="ghgFuelsService">the review applications service</param>
        /// <param name="organisationsService">the organisation service</param>
        /// <param name="reportingPeriodsService">the reporting periods service</param>
        /// <param name="tokenService">the token service</param>
        /// <param name="usersService">Users service</param>
        public ReviewApplicationsController(IApplicationsService applicationsService
            , IReviewApplicationsService reviewApplicationsService
            , IGhgFuelsService ghgFuelsService
            , IOrganisationsService organisationsService
            , IReportingPeriodsService reportingPeriodsService
            , ICalculationsService calculationsService
            , ITokenService tokenService
            , IUsersService usersService
            , ILogger<ReviewApplicationsController> logger
            , IOptions<AppSettings> appSettings)
        {
            this.ApplicationsService = applicationsService;
            this.ReviewApplicationsService = reviewApplicationsService;
            this.GhgFuelsService = ghgFuelsService;
            this.OrganisationsService = organisationsService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.CalculationsService = calculationsService;
            this.TokenService = tokenService;
            this.UsersService = usersService;
            this.Logger = logger;
            this.AppSettings = appSettings;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the pending applications
        /// </summary>
        /// <param name="applicationParams">Parameters to filter and sort the applications by</param>
        /// <returns>Paged application list</returns>
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> Index(PendingApplicationParameters applicationParams, [FromQuery] int? selectedObligationPeriodId)
        {
            if (applicationParams.PageSize == 0)
            {
                applicationParams.PageSize = this.AppSettings.Value.Pagination.ResultsPerPage;
            }
            var jwtToken = await this.TokenService.GetJwtToken();
            var result = await this.ReviewApplicationsService.GetPendingApplicationPagedResult(jwtToken
                , applicationParams);
            PagedPendingApplicationViewModel pagedResult = await GetPagedPendingApplicationsViewModel(jwtToken, applicationParams.Page, applicationParams.ApplicationStatus, result);

            if (applicationParams.ApplicationStatusIsProvided())
            {
                ViewBag.Query = ViewBag.Query + "&applicationstatus=" + applicationParams.ApplicationStatus.ToString();
            }

            if (applicationParams.Order != null)
            {
                ViewBag.Query = ViewBag.Query + "&order=" + applicationParams.Order.ToString();
            }

            if (selectedObligationPeriodId != null)
            {
                ViewBag.Query = ViewBag.Query + "&selectedObligationPeriodId=" + selectedObligationPeriodId.ToString();
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            return View("Index", pagedResult);
        }

        /// <summary>
        /// Returns the applications approval main view
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <returns>Returns the application approval view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("{applicationId}")]
        public async Task<IActionResult> Approve(int applicationId, string filterQueryString, [FromQuery] int? selectedObligationPeriodId)
        {
            var application = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, await this.TokenService.GetJwtToken());
            if (!application.HasResult || application.Result == null)
            {
                return this.HandleFailedHttpObjectResponse(application);
            }
            if (application.Result.ApplicationStatus != ApplicationStatus.Submitted
                && application.Result.ApplicationStatus != ApplicationStatus.RecommendApproval
                && application.Result.ApplicationStatus != ApplicationStatus.Approved)
            {
                var query = QueryParams.ParseQueryString(filterQueryString ?? Request.QueryString.Value);
                return RedirectToAction("Index", "ReviewApplications", query);
            }
            var viewModel = await GetApplicationApprovalViewModel(application.Result);
            // ViewBag.Query is used by the _PaginationPartial view to maintain the query whilst paging through results
            if (filterQueryString != null)
            {
                ViewBag.Query = new QueryString(filterQueryString);
            }
            else
            {
                ViewBag.Query = Request.QueryString.Value;
            }
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }

            return View("Approve", viewModel);
        }

        /// <summary>
        /// Posts the review status of the application
        /// </summary>
        /// <param name="applicationId">The ID of the application we're recommending for approval</param>
        /// <param name="model">The view model containing approval details</param>
        /// <returns></returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpPost("{applicationId}/RecommendForApproval")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RecommendForApproval(int applicationId, ApplicationApprovalViewModel model)
        {
            ApplicationApprovalViewModel viewModel;
            var jwtToken = await this.TokenService.GetJwtToken();
            var application = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, jwtToken);
            if (!application.HasResult)
            {
                return this.HandleFailedHttpObjectResponse(application);
            }
            var applicationReview = application.Result;

            if (!ModelState.IsValid)
            {
                //Rehydrate the view model to show validation issues
                viewModel = await this.GetApplicationApprovalViewModel(applicationReview);
                return View("Approve", viewModel);
            }

            bool isReviewComplete = IsReviewComplete(applicationReview) || HasRejectedItem(applicationReview);
            if (!isReviewComplete)
            {
                ModelState.AddModelError(string.Empty, "All the application sections are not completed. Please review the sections not marked as Approved or Rejected.");
                viewModel = await this.GetApplicationApprovalViewModel(applicationReview);
                return View("Approve", viewModel);
            }

            var isApproved = !applicationReview.AreAnyReviewStatusesNull()
                && applicationReview.ApplicationVolumesReviewStatus.ReviewStatusId == (int)ReviewStatuses.Approved
                && applicationReview.SupportingDocumentsReviewStatus.ReviewStatusId == (int)ReviewStatuses.Approved
                && applicationReview.CalculationReviewStatus.ReviewStatusId == (int)ReviewStatuses.Approved;

            var applicationStatusId = (int)ApplicationStatus.RecommendApproval;
            if (!isApproved)
            {
                applicationStatusId = (int)ApplicationStatus.Rejected;
            }
            var reviewCommand = new ReviewApplicationCommand(applicationId, applicationStatusId)
            {
                RecommendedBy = this.User.GetUserId(),
                RecommendedDate = DateTime.Now,
            };
            if (applicationStatusId == (int)ApplicationStatus.Rejected)
            {
                reviewCommand.RejectedDate = DateTime.Now;
                reviewCommand.RejectedBy = this.User.GetUserId();
            }
            try
            {
                var result = await this.ReviewApplicationsService.ReviewApplication(reviewCommand, jwtToken);
                if (!result)
                {
                    throw new ServiceException("The API was unable to perform the review action.");
                }
            }
            catch (Exception ex)
            {
                //An error occurred attempting to review the application section - handle this gracefully!
                this.Logger.LogError("Unable to review GHG Application. Inner exception details: " + ex.ToString());
                ModelState.AddModelError(string.Empty, "The application could not be reviewed at this time, please try again.");
                viewModel = await this.GetApplicationApprovalViewModel(applicationReview);
                return View("Approve", viewModel);
            }

            var urlParameters = QueryParams.ParseQueryString(model.FilterQueryString);
            urlParameters.Add("applicationId", applicationId.ToString());
            return RedirectToAction("Approve", urlParameters);

        }

        /// <summary>
        /// Marks the application as approved and ready for credit issue
        /// </summary>
        /// <param name="applicationId">The ID of the application we're approving</param>
        /// <param name="viewModel">View Model containing approval details</param>
        /// <returns></returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpPost("{applicationId}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Approve(int applicationId, ApplicationApprovalViewModel viewModel)
        {
            var jwtToken = await this.TokenService.GetJwtToken();
            var application = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, jwtToken);
            if (!application.HasResult)
            {
                return this.HandleFailedHttpObjectResponse(application);
            }
            var applicationReview = application.Result;

            if (!this.User.CanAccessOrganisationDetails(applicationReview.SupplierOrganisationId))
            {
                return Forbid();
            }

            //If the approve recommendation radio hasn't been selected, then add an error
            if (!viewModel.ApproveApplication.HasValue)
            {
                ModelState.AddModelError("ApproveApplication", "Recommendation for approval or rejection is required.");
            }

            if (!ModelState.IsValid)
            {
                //Rehydrate the view model to show validation issues
                var newViewModel = await this.GetApplicationApprovalViewModel(applicationReview);
                return View("Approve", newViewModel);
            }

            try
            {
                bool result;
                if (viewModel.ApproveApplication.Value)
                {
                    //The application was approved
                    var approveCommand = new ApproveApplicationCommand(applicationId);
                    result = await this.ReviewApplicationsService.ApproveApplication(approveCommand, jwtToken);
                }
                else
                {
                    //The application was rejected
                    var rejectCommand = new RejectApplicationCommand(applicationId);
                    result = await this.ReviewApplicationsService.RejectApplication(rejectCommand, jwtToken);
                }

                if (!result)
                {
                    throw new ServiceException("The API was unable to perform the approve action.");
                }
            }
            catch (Exception ex)
            {
                //An error occurred attempting to approve the application - handle this gracefully!
                this.Logger.LogError("Unable to approve GHG Application. Inner exception details: " + ex);
                ModelState.AddModelError(string.Empty, "The application could not be approved at this time, please try again.");
                var newViewModel = await this.GetApplicationApprovalViewModel(applicationReview);
                return View("Approve", newViewModel);
            }

            return RedirectToAction("Index", "ReviewApplications", QueryParams.ParseQueryString(viewModel.FilterQueryString));
        }


        /// <summary>
        /// Returns the applications volumes approval view
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <returns>Returns the application volumes approval view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("{applicationId}/Volumes")]
        public async Task<IActionResult> ApproveVolumes(int applicationId, [FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            var applicationReviewResponse = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, await this.TokenService.GetJwtToken());

            if (applicationReviewResponse.HasResult)
            {
                var applicationReview = applicationReviewResponse.Result;
                if (applicationReview.ApplicationStatusId == (int)ApplicationStatus.Submitted
                    || applicationReview.ApplicationStatusId == (int)ApplicationStatus.RecommendApproval
                    || applicationReview.ApplicationStatusId == (int)ApplicationStatus.Approved)
                {
                    var viewModel = await this.GetApproveApplicationVolumesViewModel(applicationReview);
                    return View("ApproveVolumes", viewModel);
                }
                else
                {
                    //We're in the incorrect state to view the approval
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationReviewResponse);
            }
        }

        /// <summary>
        /// Returns the view for rejected application
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("{applicationId}/reject")]
        public async Task<IActionResult> ReviewResult(int applicationId)
        {
            var applicationReviewResponse = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, await this.TokenService.GetJwtToken());
            if (applicationReviewResponse.HasResult)
            {
                var reviewedApplication = applicationReviewResponse.Result;
                if (reviewedApplication.ApplicationStatus != ApplicationStatus.Rejected)
                {
                    return RedirectToAction("Index");
                }
                var viewModel = await GetApplicationReviewResultViewModel(applicationReviewResponse.Result);
                return View("ReviewResult", viewModel);
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationReviewResponse);
            }
        }
        /// <summary>
        /// Handles the approve volumes view POST action
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <param name="viewModel">The view model with the new state</param>
        /// <returns>Returns the application volumes approval view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpPost("{applicationId}/Volumes")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApproveVolumes(int applicationId, ApproveApplicationVolumesViewModel viewModel)
        {
            var jwtToken = await this.TokenService.GetJwtToken();
            var applicationReviewResponse = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, jwtToken);
            if (!applicationReviewResponse.HasResult)
            {
                return this.HandleFailedHttpObjectResponse(applicationReviewResponse);
            }

            var applicationReview = applicationReviewResponse.Result;

            if (!ModelState.IsValid)
            {
                //Rehydrate the view model to show validation issues
                var newViewModel = await this.GetApproveApplicationVolumesViewModel(applicationReview);
                return View("ApproveVolumes", newViewModel);
            }

            //Only save if changes were made
            if (applicationReview.ApplicationVolumesReviewStatus.ReviewStatusId != (int)viewModel.Review.ReviewStatus
                || applicationReview.ApplicationVolumesRejectionReason != viewModel.Review.RejectionReason)
            {
                //Rejection reason only needed if the status is rejected
                string rejectionReason = null;
                if (viewModel.Review.ReviewStatus == ReviewStatuses.Rejected)
                {
                    rejectionReason = viewModel.Review.RejectionReason;
                }

                var command = new ReviewApplicationVolumesCommand(viewModel.ApplicationId
                    , (int)viewModel.Review.ReviewStatus
                    , rejectionReason);

                try
                {
                    var success = await this.ReviewApplicationsService.ReviewApplicationVolumes(command, jwtToken);

                    if (!success)
                    {
                        throw new ServiceException("The API was unable to perform the review action.");
                    }
                }
                catch (Exception ex)
                {
                    //An error occurred attempting to review the application section - handle this gracefully!
                    this.Logger.LogError("Unable to review GHG Application volumes. Inner exception details: " + ex);
                    ModelState.AddModelError(string.Empty, "The application's volumes could not be reviewed at this time, please try again.");
                    viewModel = await this.GetApproveApplicationVolumesViewModel(applicationReview);
                    return View("ApproveVolumes", viewModel);
                }
            }
            var urlParameters = QueryParams.ParseQueryString(viewModel.FilterQueryString);
            urlParameters.Add("applicationId", applicationId.ToString());
            return RedirectToAction("Approve", urlParameters);
        }

        /// <summary> 
        /// Returns the applications supporting documents approval view
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <returns>Returns the application supporting documents approval view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("{applicationId}/SupportingDocuments")]
        public async Task<IActionResult> ApproveSupportingDocuments(int applicationId, [FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            var applicationReviewResponse = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, await this.TokenService.GetJwtToken());

            if (applicationReviewResponse.HasResult)
            {
                var applicationReview = applicationReviewResponse.Result;
                if (applicationReview.ApplicationStatusId == (int)ApplicationStatus.Submitted
                    || applicationReview.ApplicationStatusId == (int)ApplicationStatus.RecommendApproval
                    || applicationReview.ApplicationStatusId == (int)ApplicationStatus.Approved)
                {
                    var viewModel = await this.GetApproveApplicationSupportingDocumentsViewModel(applicationReview);
                    return View("ApproveSupportingDocuments", viewModel);
                }
                else
                {
                    //We're in the incorrect state to view the approval
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationReviewResponse);
            }
        }

        /// <summary>
        /// Handles the approve supporting documents view POST action
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <param name="viewModel">The view model with the new state</param>
        /// <returns>Returns the application supporting documents approval view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpPost("{applicationId}/SupportingDocuments")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApproveSupportingDocuments(int applicationId, ApproveApplicationSupportingDocumentsViewModel viewModel)
        {
            var jwtToken = await this.TokenService.GetJwtToken();
            var applicationReviewResponse = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, jwtToken);

            if (!applicationReviewResponse.HasResult)
            {
                return this.HandleFailedHttpObjectResponse(applicationReviewResponse);
            }

            var applicationReview = applicationReviewResponse.Result;

            if (!ModelState.IsValid)
            {
                //Rehydrate the view model to show validation issues
                var newViewModel = await this.GetApproveApplicationSupportingDocumentsViewModel(applicationReview);
                return View("ApproveSupportingDocuments", newViewModel);
            }

            //Only save if changes were made
            if (applicationReview.SupportingDocumentsReviewStatus.ReviewStatusId != (int)viewModel.Review.ReviewStatus
                || applicationReview.SupportingDocumentsRejectionReason != viewModel.Review.RejectionReason)
            {
                //Rejection reason only needed if the status is rejected
                string rejectionReason = null;
                if (viewModel.Review.ReviewStatus == ReviewStatuses.Rejected)
                {
                    rejectionReason = viewModel.Review.RejectionReason;
                }

                var command = new ReviewApplicationSupportingDocumentsCommand(viewModel.ApplicationId
                    , (int)viewModel.Review.ReviewStatus
                    , rejectionReason);

                try
                {
                    var success = await this.ReviewApplicationsService.ReviewApplicationSupportingDocuments(command, jwtToken);

                    if (!success)
                    {
                        throw new ServiceException("The API was unable to perform the review action.");
                    }
                }
                catch (Exception ex)
                {
                    //An error occurred attempting to review the application section - handle this gracefully!
                    this.Logger.LogError("Unable to review GHG Application supporting documents. Inner exception details: " + ex);
                    ModelState.AddModelError(string.Empty, "The application's supporting documents could not be reviewed at this time, please try again.");
                    var newViewModel = await this.GetApproveApplicationSupportingDocumentsViewModel(applicationReview);
                    return View("ApproveSupportingDocuments", newViewModel);
                }
            }

            var urlParameters = QueryParams.ParseQueryString(viewModel.FilterQueryString);
            urlParameters.Add("applicationId", applicationId.ToString());
            return RedirectToAction("Approve", urlParameters);
        }

        /// <summary> 
        /// Returns the applications calculation approval view
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <returns>Returns the application calculation approval view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("{applicationId}/Calculation")]
        public async Task<IActionResult> ApproveCalculation(int applicationId, [FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            var applicationReviewResponse = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, await this.TokenService.GetJwtToken());

            if (applicationReviewResponse.HasResult)
            {
                var applicationReview = applicationReviewResponse.Result;
                if (applicationReview.ApplicationStatusId == (int)ApplicationStatus.Submitted
                    || applicationReview.ApplicationStatusId == (int)ApplicationStatus.RecommendApproval
                    || applicationReview.ApplicationStatusId == (int)ApplicationStatus.Approved)
                {
                    var viewModel = await this.GetApproveApplicationCalculationViewModel(applicationReview);
                    return View("ApproveCalculation", viewModel);
                }
                else
                {
                    //We're in the incorrect state to view the approval
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return this.HandleFailedHttpObjectResponse(applicationReviewResponse);
            }
        }

        /// <summary>
        /// Handles the approve calculation view POST action
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <param name="viewModel">The view model with the new state</param>
        /// <returns>Returns the application calculation approval view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpPost("{applicationId}/Calculation")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApproveCalculation(int applicationId, ApproveApplicationCalculationViewModel viewModel)
        {
            var jwtToken = await this.TokenService.GetJwtToken();
            var applicationReviewResponse = await this.ReviewApplicationsService.GetApplicationReviewSummary(applicationId, jwtToken);

            if (!applicationReviewResponse.HasResult)
            {
                return this.HandleFailedHttpObjectResponse(applicationReviewResponse);
            }

            var applicationReview = applicationReviewResponse.Result;

            if (!ModelState.IsValid)
            {
                //Rehydrate the view model to show validation issues
                var newViewModel = await this.GetApproveApplicationCalculationViewModel(applicationReview);
                return View("ApproveCalculation", newViewModel);
            }

            //Only save if changes were made
            if (applicationReview.CalculationReviewStatus.ReviewStatusId != (int)viewModel.Review.ReviewStatus
                || applicationReview.CalculationRejectionReason != viewModel.Review.RejectionReason)
            {
                //Rejection reason only needed if the status is rejected
                string rejectionReason = null;
                if (viewModel.Review.ReviewStatus == ReviewStatuses.Rejected)
                {
                    rejectionReason = viewModel.Review.RejectionReason;
                }

                var command = new ReviewApplicationCalculationCommand(viewModel.ApplicationId
                    , (int)viewModel.Review.ReviewStatus
                    , rejectionReason);

                try
                {
                    var success = await this.ReviewApplicationsService.ReviewApplicationCalculation(command, jwtToken);

                    if (!success)
                    {
                        throw new ServiceException("The API was unable to perform the review action.");
                    }
                }
                catch (Exception ex)
                {
                    //An error occurred attempting to review the application section - handle this gracefully!
                    this.Logger.LogError("Unable to review GHG Application calculation. Inner exception details: " + ex);
                    ModelState.AddModelError(string.Empty, "The application's calculation could not be reviewed at this time, please try again.");
                    var newViewModel = await this.GetApproveApplicationCalculationViewModel(applicationReview);
                    return View("ApproveCalculation", newViewModel);
                }
            }

            var urlParameters = QueryParams.ParseQueryString(viewModel.FilterQueryString);
            urlParameters.Add("applicationId", applicationId.ToString());
            return RedirectToAction("Approve", urlParameters);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Get the view model for the pending applications
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="result">PagedResult form the api</param>
        /// <returns>PagedPendingApplicationViewModel</returns>
        private async Task<PagedPendingApplicationViewModel> GetPagedPendingApplicationsViewModel(
            string jwtToken,
            int page,
            ApplicationStatus applicationStatus,
            HttpObjectResponse<PagedResult<ApplicationSummary>> result)
        {
            var pagedResult = new PagedPendingApplicationViewModel();
            var pendingApprovals = new List<PendingApplicationViewModel>();

            pagedResult.PageTitle = GetPageTitle(applicationStatus);

            //Return an empty page results when there are no results
            if (result == null || !result.HasResult)
            {
                return pagedResult;
            }

            var supplierTasks = new List<Task<HttpObjectResponse<Organisation>>>();
            var calculationTasks = new List<Task<ApplicationCalculationSummaryResult>>();
            var fuelTypes = await this.GhgFuelsService.GetFuelTypes();

            var calculationOrchestrator = new ApplicationsCalculationOrchestrator(this.ApplicationsService
                , this.CalculationsService
                , fuelTypes
                , jwtToken);

            foreach (var item in result.Result.Result)
            {
                var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(item.ObligationPeriodId);

                if (obligationPeriod == null)
                {
                    this.Logger.LogError("Could not load pending applications for the following Obligation Period ID: " + item.ObligationPeriodId.ToString());
                    continue;
                }

                var calculationResultTask = calculationOrchestrator.GetCalculationSummaryForApplication(item.ApplicationId
                    , item.SupplierOrganisationId
                    , item.ObligationPeriodId
                    , obligationPeriod.GhgThresholdGrammesCO2PerMJ.Value
                    , obligationPeriod.DeMinimisLitres
                    , obligationPeriod.NoDeMinimisLitres
                    , obligationPeriod.GhgLowVolumeSupplierDeductionKGCO2.Value);

                calculationTasks.Add(calculationResultTask);

                if (pendingApprovals.Count(x => x.SupplierId == item.SupplierOrganisationId) == 0)
                {
                    var supplier = this.OrganisationsService.GetOrganisation(item.SupplierOrganisationId, jwtToken);
                    supplierTasks.Add(supplier);
                }

                var pa = new PendingApplicationViewModel
                {
                    ApplicationId = item.ApplicationId,
                    SupplierId = item.SupplierOrganisationId,
                    SubmittedDate = item.SubmittedDate,
                    ObligationPeriodEndDate = obligationPeriod.EndDate.Value,
                };

                if (item.ApplicationStatusId == (int)ApplicationStatus.RecommendApproval)
                {
                    var firstApprover = this.User.Claims.FirstOrDefault(x => x.Type == Claims.UserId);
                    pa.ReviewAction = "Stage 2 Review";
                    pa.ReviewStatus = "Recommended for approval";
                    if (item.RecommendedBy == firstApprover.Value)
                    {
                        pa.ReviewAction = "View";
                    }
                }
                else if (item.ApplicationStatusId != (int)ApplicationStatus.Submitted)
                {
                    pa.ReviewStatus = "Approved";
                    pa.ReviewAction = "View";
                }
                else
                {
                    pa.ReviewStatus = "Submitted";
                    pa.ReviewAction = "Stage 1 Review";
                }

                pendingApprovals.Add(pa);
            }

            //Populate the Supplier Names
            var supplierTaskResults = await Task.WhenAll(supplierTasks);

            foreach (var organisationHttpObject in supplierTaskResults)
            {
                if (organisationHttpObject != null && organisationHttpObject.HasResult)
                {
                    pendingApprovals.Where(x => x.SupplierId == organisationHttpObject.Result.Id).ToList().ForEach(x =>
                    {
                        x.Supplier = organisationHttpObject.Result.Name;
                    });
                }
            }

            // For any approvals where the supplier can't be found, just use their Id
            pendingApprovals.Where(a => a.Supplier == null).ToList().ForEach(a => a.Supplier = "Supplier Id " + a.SupplierId);

            //Populate the calculation values
            var calculationTaskResults = await Task.WhenAll(calculationTasks);

            foreach (var calculationSummaryResult in calculationTaskResults)
            {
                if (calculationSummaryResult != null)
                {
                    var pendingApproval = pendingApprovals.Single(pa => pa.ApplicationId == calculationSummaryResult.ApplicationId);
                    pendingApproval.CreditsApplierFor = calculationSummaryResult.Result.NetTotalCredits;
                }
            }

            pagedResult.Result = new UIPagedResult<PendingApplicationViewModel>
            {
                Result = pendingApprovals,
                MaximumPagesToDisplay = AppSettings.Value.Pagination.MaximumPagesToDisplay,
                PageNumber = page,
                PageSize = AppSettings.Value.Pagination.ResultsPerPage,
                TotalResults = result.Result.TotalResults
            };           

            return pagedResult;
        }

        private string GetPageTitle(ApplicationStatus applicationStatus)
        {
            if (applicationStatus == ApplicationStatus.Submitted)
            {
                return "Applications awaiting stage 1 review";
            }
            else if (applicationStatus == ApplicationStatus.RecommendApproval)
            {
                return "Applications awaiting stage 2 review";
            }
            else
            {
                return "Pending application approvals";
            }
        }

        /// <summary>
        /// Gets the application approval view model
        /// </summary>
        /// <param name="reviewSummary">Object representing the review summary</param>
        /// <returns>ApplicationApprovalViewModel</returns>
        private async Task<ApplicationApprovalViewModel> GetApplicationApprovalViewModel(ApplicationReviewSummary reviewSummary)
        {
            var organisation = this.OrganisationsService.GetOrganisation(reviewSummary.SupplierOrganisationId, await this.TokenService.GetJwtToken());
            var obligationPeriod = this.ReportingPeriodsService.GetObligationPeriod(reviewSummary.ObligationPeriodId);
            var requestor = this.UsersService.FindUserByIdAsync(reviewSummary.SubmittedBy, await this.TokenService.GetJwtToken(), default(CancellationToken));

            await Task.WhenAll(organisation, obligationPeriod, requestor);

            var appApprovalViewModel = new ApplicationApprovalViewModel(reviewSummary.ApplicationId)
            {
                ApplicationDate = reviewSummary.SubmittedDate,
                ApplicationStatus = reviewSummary.ApplicationStatus,
                SubmittedBy = requestor.Result != null ? requestor.Result.FullName : "",
                SupplierName = organisation.Result.Result.Name,
                ObligationPeriod = obligationPeriod.Result.EndDate.Value,
                VolumesReviewStatus = reviewSummary.ApplicationVolumesReviewStatus,
                CalculationReviewStatus = reviewSummary.CalculationReviewStatus,
                DocumentsReviewStatus = reviewSummary.SupportingDocumentsReviewStatus,
                UserCanApproveApplication = false,
                FilterQueryString = Request.QueryString.Value
            };
            if (reviewSummary.RecommendedBy != null)
            {
                var recommendedBy = await this.UsersService.FindUserByIdAsync(reviewSummary.RecommendedBy, await this.TokenService.GetJwtToken(), default(CancellationToken));
                appApprovalViewModel.RecommendedBy = recommendedBy == null ? reviewSummary.RecommendedBy : recommendedBy.FullName;
                appApprovalViewModel.RecommendedDate = reviewSummary.RecommendedDate;
                appApprovalViewModel.UserCanApproveApplication = this.User.GetUserId() != reviewSummary.RecommendedBy;
            }
            return appApprovalViewModel;
        }

        /// <summary>
        /// Gets the view model for approve volumes view
        /// </summary>
        /// <param name="applicationReview">The review model for the application</param>
        /// <returns>Returns a populated view model for the approve volumes view</returns>
        private async Task<ApproveApplicationVolumesViewModel> GetApproveApplicationVolumesViewModel(ApplicationReviewSummary applicationReview)
        {
            var token = await this.TokenService.GetJwtToken();
            var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(applicationReview.ObligationPeriodId);
            var organisationTask = this.OrganisationsService.GetOrganisation(applicationReview.SupplierOrganisationId, token);
            var applicationItemsResponseTask = this.ApplicationsService.GetApplicationItems(applicationReview.ApplicationId, token);

            await Task.WhenAll(obligationPeriodTask
                , organisationTask
                , applicationItemsResponseTask);

            var obligationPeriod = await obligationPeriodTask;
            var organisationResponse = await organisationTask;
            var applicationItemsResponse = await applicationItemsResponseTask;

            var canEditReviewStatus = applicationReview.ApplicationStatusId == (int)ApplicationStatus.Submitted;

            return new ApproveApplicationVolumesViewModel(applicationReview.ApplicationId
                , canEditReviewStatus
                , obligationPeriod
                , organisationResponse.Result
                , await this.GetApplicationItemViewModel(applicationReview.ApplicationId, applicationItemsResponse.Result)
                , (ReviewStatuses)applicationReview.ApplicationVolumesReviewStatus.ReviewStatusId
                , applicationReview.ApplicationVolumesRejectionReason
                , applicationReview.SubmittedDate.Value
                , Request.QueryString.Value);
        }

        /// <summary>
        /// Gets the view model for approve supporting documents view
        /// </summary>
        /// <param name="applicationReview">The review model for the application</param>
        /// <returns>Returns a populated view model for the approve supporting documents view</returns>
        private async Task<ApproveApplicationSupportingDocumentsViewModel> GetApproveApplicationSupportingDocumentsViewModel(ApplicationReviewSummary applicationReview)
        {
            var token = await this.TokenService.GetJwtToken();
            var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(applicationReview.ObligationPeriodId);
            var organisationTask = this.OrganisationsService.GetOrganisation(applicationReview.SupplierOrganisationId, token);
            var supportingDocumentsTask = this.ApplicationsService.GetSupportingDocuments(applicationReview.ApplicationId, token);

            await Task.WhenAll(obligationPeriodTask
                , organisationTask
                , supportingDocumentsTask);

            var obligationPeriod = await obligationPeriodTask;
            var organisationResponse = await organisationTask;
            var supportingDocumentsResponse = await supportingDocumentsTask;

            var canEditReviewStatus = applicationReview.ApplicationStatusId == (int)ApplicationStatus.Submitted;

            return new ApproveApplicationSupportingDocumentsViewModel(applicationReview.ApplicationId
                , canEditReviewStatus
                , obligationPeriod
                , organisationResponse.Result
                , (ReviewStatuses)applicationReview.SupportingDocumentsReviewStatus.ReviewStatusId
                , applicationReview.SupportingDocumentsRejectionReason
                , applicationReview.SubmittedDate.Value
                , supportingDocumentsResponse.Result
                , Request.QueryString.Value);
        }

        /// <summary>
        /// Gets the view model for approve calculation view
        /// </summary>
        /// <param name="applicationReview">The review model for the application</param>
        /// <returns>Returns a populated view model for the approve calculation view</returns>
        private async Task<ApproveApplicationCalculationViewModel> GetApproveApplicationCalculationViewModel(ApplicationReviewSummary applicationReview)
        {
            var token = await this.TokenService.GetJwtToken();
            var obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(applicationReview.ObligationPeriodId);
            var organisationTask = this.OrganisationsService.GetOrganisation(applicationReview.SupplierOrganisationId, token);
            var itemsResponseTask = this.ApplicationsService.GetApplicationItems(applicationReview.ApplicationId, token);
            var fuelTypesTask = this.GhgFuelsService.GetFuelTypes();

            await Task.WhenAll(obligationPeriodTask
                , organisationTask
                , itemsResponseTask
                , fuelTypesTask);

            var applicationItems = await this.GetApplicationItemViewModel(applicationReview.ApplicationId, itemsResponseTask.Result.Result);
            var obligationPeriod = await obligationPeriodTask;
            var organisationResponse = await organisationTask;

            var canEditReviewStatus = applicationReview.ApplicationStatusId == (int)ApplicationStatus.Submitted;

            var fuelTypes = await fuelTypesTask;

            var calculationOrchestrator = new ApplicationsCalculationOrchestrator(this.ApplicationsService, this.CalculationsService, fuelTypes, token);

            var calculationResult = await calculationOrchestrator.GetCalculationBreakdownForApplicationItems(itemsResponseTask.Result.Result
                , applicationReview.SupplierOrganisationId
                , applicationReview.ObligationPeriodId
                , obligationPeriod.GhgThresholdGrammesCO2PerMJ.Value
                , obligationPeriod.DeMinimisLitres
                , obligationPeriod.NoDeMinimisLitres
                , obligationPeriod.GhgLowVolumeSupplierDeductionKGCO2.Value);

            var viewModel = new ApproveApplicationCalculationViewModel(applicationReview.ApplicationId
                , canEditReviewStatus
                , obligationPeriod
                , organisationResponse.Result
                , (ReviewStatuses)applicationReview.CalculationReviewStatus.ReviewStatusId
                , applicationReview.CalculationRejectionReason
                , applicationReview.SubmittedDate.Value
                , new List<ApplicationItemViewModel>()
                , Request.QueryString.Value);

            if (applicationItems != null)
            {
                foreach (var item in applicationItems)
                {
                    var calculationItem = calculationResult.GetAllCalculationItems().Single(ci => ci.Reference == item.ApplicationItemId.ToString());
                    item.Credits = calculationItem.Credits;
                    item.Obligation = calculationItem.Obligation;

                    viewModel.ApplicationItems.Add(item);
                }
            }

            return viewModel;
        }

        /// <summary>
        /// Get the application item view model
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <param name="applicationItems">List of application items to display</param>
        /// <returns>List of ApplicationItemViewModel</returns>
        private async Task<IList<ApplicationItemViewModel>> GetApplicationItemViewModel(int applicationId, IList<ApplicationItemSummary> applicationItems)
        {
            var appItemsModel = new List<ApplicationItemViewModel>();

            var fossilFuelTypesTask = this.GhgFuelsService.GetFossilGasFuelTypes();
            var eFuelTypeTask = this.GhgFuelsService.GetElectricityFuelType();
            var uerFuelTypeTask = this.GhgFuelsService.GetUerFuelType();

            await Task.WhenAll(fossilFuelTypesTask, eFuelTypeTask, uerFuelTypeTask);

            var fossilFuelTypes = await fossilFuelTypesTask;
            var eFuelType = await eFuelTypeTask;
            var uerFuelType = await uerFuelTypeTask;

            foreach (var item in applicationItems)
            {
                var fuelType = fossilFuelTypes.FirstOrDefault(ftype => ftype.FuelTypeId == item.FuelTypeId);

                //Default the fuel type to the one specified in the item, if there was one. This will be the case for Fossil Gas Items
                var fuelTypeName = fuelType?.Name;
                var fuelTypeId = fuelType?.FuelTypeId;

                if (eFuelType.FuelTypeId == item.FuelTypeId)
                {
                    //This is an Electricity Item
                    fuelTypeName = eFuelType.Name;
                    fuelTypeId = eFuelType.FuelTypeId;
                }
                else if (uerFuelType?.FuelTypeId == item.FuelTypeId)
                {
                    //This is a UER Item
                    fuelTypeName = uerFuelType.Name;
                    fuelTypeId = uerFuelType.FuelTypeId;
                }

                if (!fuelTypeId.HasValue)
                {
                    throw new NullReferenceException("No Fuel Type has been specified for application item with ID " + item.ApplicationItemId);
                }

                var applicationItem = new ApplicationItemViewModel(item.ApplicationId
                    , item.ApplicationItemId
                    , item.Reference
                    , fuelTypeName
                    , fuelTypeId.Value
                    , item.AmountSupplied
                    , item.GhgIntensity
                    , item.EnergyDensity
                    , fuelType != null ? "Kg" : "MJ");
                appItemsModel.Add(applicationItem);
            }

            return appItemsModel;
        }

        /// <summary>
        /// Gets the application review result model
        /// </summary>
        /// <param name="reviewSummary">ApplicationReviewSummary</param>
        /// <returns></returns>
        private async Task<ApplicationReviewResultViewModel> GetApplicationReviewResultViewModel(ApplicationReviewSummary reviewSummary)
        {
            var organisation = this.OrganisationsService.GetOrganisation(reviewSummary.SupplierOrganisationId, await this.TokenService.GetJwtToken());
            var obligationPeriod = this.ReportingPeriodsService.GetObligationPeriod(reviewSummary.ObligationPeriodId);
            await Task.WhenAll(organisation, obligationPeriod);
            var reviewResult = new ApplicationReviewResultViewModel(reviewSummary.ApplicationId, reviewSummary.ApplicationStatus)
            {
                ObligationPeriod = obligationPeriod.Result.EndDate.Value,
                Supplier = organisation.Result.Result.Name
            };
            return reviewResult;
        }

        /// <summary>
        /// Check if the application review is complete
        /// </summary>
        /// <param name="applicationReview"></param>
        /// <returns></returns>
        private static bool IsReviewComplete(ApplicationReviewSummary applicationReview)
        {
            return !applicationReview.AreAnyReviewStatusesNull()
                && applicationReview.ApplicationVolumesReviewStatus.ReviewStatusId > (int)ReviewStatuses.NotReviewed
                && applicationReview.SupportingDocumentsReviewStatus.ReviewStatusId > (int)ReviewStatuses.NotReviewed
                && applicationReview.CalculationReviewStatus.ReviewStatusId > (int)ReviewStatuses.NotReviewed;
        }

        /// <summary>
        /// Check if any of the line item is rejected
        /// </summary>
        /// <param name="applicationReview"></param>
        /// <returns></returns>
        private static bool HasRejectedItem(ApplicationReviewSummary applicationReview)
        {
            return ((applicationReview.ApplicationVolumesReviewStatus != null
                && applicationReview.ApplicationVolumesReviewStatus.ReviewStatusId == (int)Review.ReviewStatuses.Rejected)
                || (applicationReview.SupportingDocumentsReviewStatus != null
                && applicationReview.SupportingDocumentsReviewStatus.ReviewStatusId == (int)Review.ReviewStatuses.Rejected)
                || (applicationReview.CalculationReviewStatus != null
                && applicationReview.CalculationReviewStatus.ReviewStatusId == (int)Review.ReviewStatuses.Rejected));
        }
        #endregion
    }
}