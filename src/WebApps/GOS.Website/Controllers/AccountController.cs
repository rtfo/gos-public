﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DfT.GOS.Website.Models.AccountViewModels;
using Microsoft.Extensions.Options;
using DfT.GOS.Identity.API.Client.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Collections.Generic;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Security.Authentication;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.Identity.API.Client.Commands;
using System.Threading;
using DfT.GOS.Security.Claims;
using System.Linq;

namespace DfT.GOS.Website.Controllers
{
    /// <summary>
    /// Controller responsible for user/identity operations
    /// </summary>
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<GosApplicationUser> _userManager;
        private readonly ILogger _logger;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly IUsersService _usersService;
        private readonly IOrganisationsService _organisationService;
        private readonly IIdentityService _identityService;
        private readonly ITokenService _tokenService;
        private readonly ISystemParametersService _systemParametersService;

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="userManager">UserManager instance</param>
        /// <param name="logger">Logger instance</param>
        /// <param name="appSettings">AppSettings (from configuration file)</param>
        /// <param name="usersService">UserService instance</param>
        /// <param name="organisationService">OrganisationsService instance</param>
        /// <param name="identityService">IdentityService instance</param>
        /// <param name="tokenService">TokenService instance</param>
        /// <param name="systemParametersService">SystemParametersService instance</param>
        public AccountController(
            UserManager<Identity.API.Client.Models.GosApplicationUser> userManager,
            ILogger<AccountController> logger,
            IOptions<AppSettings> appSettings,
            IUsersService usersService,
            IOrganisationsService organisationService,
            IIdentityService identityService,
            ITokenService tokenService,
            ISystemParametersService systemParametersService)
        {
            _userManager = userManager;
            _logger = logger;
            _appSettings = appSettings;
            _usersService = usersService;
            _organisationService = organisationService;
            _identityService = identityService;
            _tokenService = tokenService;
            _systemParametersService = systemParametersService;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Displays the "View or amend your details" screen
        /// </summary>
        [HttpGet("/Account")]
        public async Task<IActionResult> Index(int? obligationPeriodId = null)
        {
            var email = this.User.GetUserName();
            var userTask = _userManager.FindByEmailAsync(email);
            var sysParamsTask = _systemParametersService.GetSystemParameters();
            await Task.WhenAll(userTask, sysParamsTask);
            var user = await userTask;
            var sysParams = await sysParamsTask;

            if (user != null)
            {
                var accountDetails = new AccountDetailsViewModel()
                {
                    Email = user.Email,
                    FullName = user.FullName,
                    RtfoSupportEmailAddress = sysParams.RtfoSupportEmailAddress,
                    RtfoSupportTelephoneNumber = sysParams.RtfoSupportTelephoneNumber,
                    ObligationPeriodId = obligationPeriodId
                };
                return View(accountDetails);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Displays the Sign-In page
        /// </summary>
        /// <param name="returnUrl"></param>
        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        [Route("/")]
        public async Task<IActionResult> SignIn(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                //User is already loged in, rediect to home page
                return RedirectToAction("Home", "Home");
            }
            else
            {
                // Clear the existing external cookie to ensure a clean login process
                await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
                returnUrl = !string.IsNullOrEmpty(returnUrl)
                   && returnUrl.IndexOf("SignOut") > -1 ? null : returnUrl;
                ViewData["ReturnUrl"] = returnUrl;
                return View();
            }
        }

        /// <summary>
        /// Signs a user into the system, creating a JWT token
        /// </summary>
        /// <param name="model">Model containing email and password</param>
        /// <param name="returnUrl">Url to which user was navigating when they were redirected to Sign-In page</param>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignIn(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {           
                var signInResult = await this.SignUserIn(model.Email, model.Password);

                if (signInResult.Succeeded)
                {
                    return RedirectToLocal(returnUrl);
                }
                else if(signInResult.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToAction(nameof(Lockout));
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "The supplied credentials were not correct. Please try again.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }        

        /// <summary>
        /// View for the User Account being locked out
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Lockout()
        {
            LockoutViewModel viewModel;
            try
            {
                var authenticationSettings = await _identityService.GetAuthenticationSettings();

                viewModel = new LockoutViewModel()
                {
                    ShowConfiguration = true,
                    DefaultLockoutTimeSpan = authenticationSettings.Result.DefaultLockoutTimeSpan,
                    MaxFailedAccessAttempts = authenticationSettings.Result.MaxFailedAccessAttempts
                };
            }
            catch(Exception ex)
            {
                _logger.LogError("Unable to contact the identity service to get lockout Authentication Settings. Exception: " + ex.ToString());
                viewModel = new LockoutViewModel()
                {
                    ShowConfiguration = false
                };
            }

            return View(viewModel);
        }

        /// <summary>
        /// Displays the "How to register" page
        /// </summary>
        [HttpGet("/Account/HowToRegister")]
        [AllowAnonymous]
        public async Task<IActionResult> HowToRegister()
        {
            var systemParameters = await _systemParametersService.GetSystemParameters();
            var vm = new HowToRegisterViewModel(systemParameters.RtfoSupportEmailAddress, systemParameters.RtfoSupportTelephoneNumber);

            return View("HowToRegister", vm);
        }

        /// <summary>
        /// Displays the "Register GHG User Account" screen for users with an invitation
        /// </summary>
        /// <param name="link">Invitation link used for Registration</param>
        [HttpGet("/Account/Register/{link}")]
        [AllowAnonymous]
        public async Task<IActionResult> Register(string link)
        {            
            GosInvitedUserAssociation gosOrganisationUserAssociation = await _usersService.GetInviteDetailsByLinkAsync(link);
            if (gosOrganisationUserAssociation != null)
            {
                var rvm = new RegisterViewModel
                {
                    Email = gosOrganisationUserAssociation.Email,
                    OrganisationId = gosOrganisationUserAssociation.OrganisationId,
                    Company = gosOrganisationUserAssociation.Company,
                    EmailDisabled = "disabled",
                    Role = gosOrganisationUserAssociation.Role
                };

                return View("Register", rvm);
            }
            else
            {
                return View("InvalidInvitation");
            }
        }
      
        /// <summary>
        /// Registers a new GHG user account
        /// </summary>
        /// <param name="model">Registration details</param>
        /// <param name="link">User's Invitation link</param>
        [HttpPost("/Account/Register/{link}")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string link)
        {
            if (ModelState.IsValid)
            {
                var user = new GosApplicationUserInvite
                {
                    Email = model.Email,
                    FullName = model.FullName,
                    OrganisationId = model.OrganisationId,
                    Company = model.Company,
                    Role = model.Role
                };

                var result = await _usersService.CreateUserAsync(user, model.Password, link, default(CancellationToken));
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");

                    var signInResult = await this.SignUserIn(model.Email, model.Password);

                    if (signInResult.Succeeded)
                    {
                        return RedirectToLocal(null);
                    }
                    else
                    {
                        _logger.LogError("Unable to sign user in after account creation");
                        ModelState.AddModelError(null, "A problem occurred attempting to sign in.");
                    }
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// Signs user out of application
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var userID = this.User.Claims.Where(claim => claim.Type == Claims.UserId).Select(claim => claim.Value).SingleOrDefault();
            await this._identityService.SignOut(userID, await this._tokenService.GetJwtToken());
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(AccountController.SignedOut));
        }

        /// <summary>
        /// Displays the "Signed Out" page
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult SignedOut()
        {
            return View();
        }       

        /// <summary>
        /// Handles the GET method for forgotten Password
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ForgottenPassword()
        {
            var systemParameters = await _systemParametersService.GetSystemParameters();
            var viewModel = new ForgottenPasswordViewModel(systemParameters.RtfoSupportTelephoneNumber, systemParameters.RtfoSupportEmailAddress);
            return View("ForgottenPassword", viewModel);
        }

        /// <summary>
        /// Handles the POST method for sending a forgotten password notification
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgottenPassword(ForgottenPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var command = new IssueForgottenPasswordNotificationCommand(model.Email);
                var result = await _usersService.IssueForgottenPasswordNotification(command);

                if(!result.IsSuccessStatusCode)
                {
                    _logger.LogWarning(string.Format("An attempt to issue a password notification reset was made for {0}, but failed.", command.EmailAddress));
                }

                //Redrect to the confirmation password even if the issue nofification 
                //was successful, for example if the email address doesn't exist - this improves 
                //security, preventing guessing of user accounts
                return RedirectToAction(nameof(ForgottenPasswordConfirmation));              
            }

            // If we got this far, something failed, re-display form
            var systemParameters = await _systemParametersService.GetSystemParameters();
            model.RtfoSupportTelephoneNumber = systemParameters.RtfoSupportTelephoneNumber;
            model.RtfoSupportEmailAddress = systemParameters.RtfoSupportEmailAddress;
            return View("ForgottenPassword", model);
        }

        /// <summary>
        /// Confirmation page to tell the user that a password reset email has been sent
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgottenPasswordConfirmation()
        {
            return View("ForgottenPasswordConfirmation");
        }

        /// <summary>
        /// Displays the password reset view
        /// </summary>
        /// <param name="code">The code, correlating to the password reset request, for the reset</param>
        [HttpGet ("/Account/ResetPassword/{code}")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(string code)
        {
            if (code == null)
            {
                return Forbid("No reset password code was supplied.");
            }

            var model = new ResetPasswordViewModel { Code = code };
            var result = await _usersService.ValidateResetPasswordToken(code);

            if (!result.IsSuccessStatusCode)
            {
                return View("ResetPasswordError", model);
            }

            return View("ResetPassword", model);
        }

        /// <summary>
        /// The Post method for handling password resets
        /// </summary>
        /// <param name="model">Model containing details for the password reset</param>
        /// <returns>Redirects to the Reset Password Confirmation if there were no validation errors</returns>
        [HttpPost("/Account/ResetPassword/{code}")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var command = new ResetPasswordCommand(model.Code, model.Email, model.Password);
                var result = await _usersService.ResetPassword(command);

                if(!result.IsSuccessStatusCode)
                {
                    _logger.LogWarning(string.Format("An attempt was made to reset a password for {0}, however the attempt failed.", command.EmailAddress));
                }

                //Always redirect to the the confirmation, even if the reset password could not 
                //be reset (for example because the email address was not found) - this increases security
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            else
            {
                return View("ResetPassword", model);
            }
        }

        /// <summary>
        /// Confirmation Screen that a password has been reset
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View("ResetPasswordConfirmation");
        }

        /// <summary>
        /// Displays "Access Denied" screen
        /// </summary>
        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        /// <summary>
        /// Displays Invitation Confirmation screen
        /// </summary>
        [HttpGet]
        [AllowAnonymous] // TODO: remove anonymous
        public IActionResult InvitationConfirmation()
        {
            return View();
        }

        #region Private Methods

        /// <summary>
        /// Signs a user into GOS, creating and assigning a JWT token
        /// </summary>
        /// <param name="email">The email address of the user to authenticate</param>
        /// <param name="password">The password of the user to authenticate</param>
        /// <returns>Returns a result indicating whether the user was successfully signed in</returns>
        private async Task<AuthenticationResult> SignUserIn(string email, string password)
        {
            var signInResult = await _identityService.AuthenticateUser(email, password);

            if (signInResult.Succeeded)
            {
                var authProps = new AuthenticationProperties();
                authProps.IsPersistent = true;
                authProps.ExpiresUtc = 
                    new DateTimeOffset(DateTime.Now.AddMinutes(_appSettings.Value.AuthCookieExpiration));
                authProps.StoreTokens(
                    new[]
                    {
                            new AuthenticationToken()
                            {
                                Name = JwtAuthenticationToken.AuthenticationTokenName,
                                Value = signInResult.Token
                            }
                    });

                //Setup the claims principal
                var claims = new List<Claim>();

                foreach (var claimEntry in signInResult.Claims)
                {
                    claims.Add(new Claim(claimEntry.Key, claimEntry.Value));
                }
                claims.Add(new Claim("UserName", signInResult.UserName));
                var claimsPrincipal = new ClaimsPrincipal();
                claimsPrincipal.AddIdentity(new ClaimsIdentity(claims, "Password", ClaimTypes.Name, ClaimTypes.Role));

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal, authProps);

                _logger.LogInformation("User logged in.");                
            }

            return signInResult;
        }

        #endregion Private Methods


        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (error.Code == "DuplicateUserName")
                {
                    ModelState.AddModelError("Email", error.Description);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
        }

        private IActionResult RedirectToLocal(string returnUrl, bool accountCreated = false)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Home), "Home", new { accountCreated });
            }
        }

        #endregion
    }
}
