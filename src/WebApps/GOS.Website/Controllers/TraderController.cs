﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.Security.Services;
using DfT.GOS.Website;
using DfT.GOS.Website.Controllers;
using DfT.GOS.Website.Models.SuppliersViewModels;
using DfT.GOS.Website.Models.TraderViewModels;
using DfT.GOS.Website.Orchestrators;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace GOS.Website.Controllers
{
    [Authorize(Roles = "Administrator, Trader")]
    public class TraderController : GosWebsiteControllerBase
    {
        #region Properties
        private IOrganisationsService OrganisationsService { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }
        private IUsersService IdentityService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private ILogger Logger { get; set; }
        private ITokenService TokenService { get; set; }
        private ISupplierReportingSummaryOrchestrator Orchestrator { get; set; }
        private IMapper Mapper { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="appSettings">The app config settings</param>
        /// <param name="organisationsService">The service to get data from</param>
        public TraderController(IOptions<AppSettings> appSettings
            , IOrganisationsService organisationsService
            , IUsersService identityService
            , IReportingPeriodsService reportingPeriodsService
            , ILogger<TraderController> logger
            , ITokenService tokenService
            , ISupplierReportingSummaryOrchestrator orchestrator
            , IMapper mapper)
        {
            this.OrganisationsService = organisationsService;
            this.AppSettings = appSettings;
            this.IdentityService = identityService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.Logger = logger;
            this.TokenService = tokenService;
            this.Orchestrator = orchestrator;
            this.Mapper = mapper;
        }

        #endregion Constructors

        /// <summary>
        /// Further action page
        /// </summary>
        /// <param name="organisationId">This the organisation Id</param>
        /// <returns></returns>
        [HttpGet("/trader/{organisationId}/furtheractions")]
        [HttpGet("/trader/{organisationId}/ObligationPeriods/{obligationPeriodId}/furtheractions")]
        public async Task<IActionResult> FurtherActions(int organisationId, int obligationPeriodId = 0)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var orgModelResponseTask = this.OrganisationsService.GetOrganisation(organisationId, await this.TokenService.GetJwtToken());
                Task<ObligationPeriod> obligationPeriodTask;

                if (obligationPeriodId == 0)
                {
                    obligationPeriodTask = this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }
                else
                {
                    obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
                }

                await Task.WhenAll(orgModelResponseTask, obligationPeriodTask);

                var orgModelResponse = await orgModelResponseTask;
                var obligationPeriod = await obligationPeriodTask;

                if (orgModelResponse.HasResult)
                {
                    var viewModel = new TraderFurtherActionsViewModel(this.User.IsAdministrator()
                        , obligationPeriod.Id
                        , orgModelResponse.Result);

                    return View("FurtherActions", viewModel);
                }
                else
                {
                    return this.HandleFailedHttpObjectResponse(orgModelResponse);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Returns the Trader landing page for an administrator user
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <returns>Returns the supplier landing page, configured for an administrator</returns>
        [HttpGet("/trader/{organisationId}")]
        [HttpGet("/trader/{organisationId}/obligationPeriod/{obligationPeriodId}")]
        public async Task<IActionResult> TraderHome(int organisationId, int obligationPeriodId = 0)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod currentObligationPeriod;
                if (obligationPeriodId > 0)
                {
                    currentObligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
                }
                else
                {
                    currentObligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetTraderReportingSummary(organisationId, currentObligationPeriod, this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("TraderHome", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Trader home page to manage users
        /// </summary>
        /// <param name="organisationId">Organisation Id</param>
        /// <returns></returns>
        [HttpGet("/trader/{organisationId}/manageusers")]
        public async Task<IActionResult> ManageUsers(int organisationId, int obligationPeriodId = 0)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {       
                var orgModelResponse = await this.OrganisationsService.GetOrganisation(organisationId, await this.TokenService.GetJwtToken());

                if (orgModelResponse.HasResult)
                {
                    var viewModel = new ViewSupplierViewModel()
                    {
                        Id = orgModelResponse.Result.Id,
                        Name = orgModelResponse.Result.Name,
                        Users = await this.IdentityService.GetOrganisationUsers(orgModelResponse.Result.Id, await this.TokenService.GetJwtToken()),
                        ShowOrganisationContext = User.IsAdministrator(),
                        Supplier = orgModelResponse.Result,
                        CurrentUserId = this.User.GetUserId(),
                        ObligationPeriodId = obligationPeriodId
                    };

                    return View("ManageUsers", viewModel);
                }
                else
                {
                    return this.HandleFailedHttpObjectResponse(orgModelResponse);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// A Trader will send an email invitation link to register for an account
        /// </summary>
        /// <param name="model">Supplier model</param>
        /// <returns></returns>
        [HttpPost("/trader/{id}/manageusers")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ManageUsers(ViewSupplierViewModel model)
        {
            if (this.User.CanAccessOrganisationDetails(model.Id))
            {
                var orgModelResponse = await this.OrganisationsService.GetOrganisation(model.Id, await this.TokenService.GetJwtToken());

                if (orgModelResponse.HasResult)
                {
                    var organisation = orgModelResponse.Result;

                    var viewModel = new ViewSupplierViewModel()
                    {
                        Id = organisation.Id,
                        Name = organisation.Name,
                        Email = model.Email,
                        Role = organisation.Type,
                        Users = new List<GosApplicationUser>(),
                        ShowOrganisationContext = User.IsAdministrator(),
                        ObligationPeriodId = model.ObligationPeriodId
                    };
                    try
                    {
                        AddNewGosApplicationUser addUserRequestedBy = new AddNewGosApplicationUser()
                        {
                            RequestedByUserId = this.User.GetUserId(),
                            Email = model.Email,
                            OrganisationId = model.Id,
                            Role = organisation.Type,
                            Company = organisation.Name
                        };
                        var addUserResponse = await this.IdentityService.AddUserAsync(addUserRequestedBy, await this.TokenService.GetJwtToken());
                        viewModel.Users = await this.IdentityService.GetOrganisationUsers(organisation.Id, await this.TokenService.GetJwtToken());

                        if (addUserResponse.Message.Length > 0)
                        {
                            //Unable to add user to the organisation
                            ModelState.AddModelError("Email", addUserResponse.Message);
                        }
                        else if (addUserResponse.UserAssociatedWithOrganisation)
                        {
                            //An association between the user and orgnisation was made
                            return View("ManageUsersLinkedConfirmation", viewModel);
                        }
                        else
                        {
                            //A new user account request has been issued
                            return View("ManageUsersConfirmation", viewModel);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex, "An error occured while adding the user");
                        return View("ManageUsersConfirmationError", viewModel);
                    }

                    return View(viewModel);
                }
                else
                {
                    return this.HandleFailedHttpObjectResponse(orgModelResponse);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Information to remove a user from an organisation
        /// </summary>
        /// <param name="organisationId">Organisation id</param>
        /// <param name="userid">User id</param>
        /// <returns></returns>
        [HttpGet("/trader/{organisationId}/manageusers/{userId}/remove")]
        public async Task<IActionResult> ManageUsersRemove(int organisationId, string userId, int? obligationPeriodId = null)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                if (ModelState.IsValid)
                {
                    var orgModelTask = this.OrganisationsService.GetOrganisation(organisationId, await this.TokenService.GetJwtToken());
                    var userModelTask = this.IdentityService.FindUserByIdAsync(userId, await this.TokenService.GetJwtToken(), default(CancellationToken));

                    await Task.WhenAll(orgModelTask, userModelTask);

                    var orgModelResponse = await orgModelTask;
                    var userModel = await userModelTask;

                    if (userModel == null)
                    {
                        return new NotFoundObjectResult(null);
                    }
                    if (!orgModelResponse.HasResult)
                    {
                        return new NotFoundObjectResult(null);
                    }

                    var viewModel = new ViewSupplierUserViewModel()
                    {
                        OrganisationId = orgModelResponse.Result.Id,
                        OrganisationName = orgModelResponse.Result.Name,
                        UserId = userModel.Id,
                        FullName = userModel.FullName,
                        Email = userModel.Email,
                        Role = orgModelResponse.Result.Type,
                        ObligationPeriodId = obligationPeriodId
                    };

                    return View("ManageUsersRemove", viewModel);
                }
                return View();
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Confirmation to remove a user from an organisation
        /// </summary>
        /// <param name="organisationId">Organisation id</param>
        /// <param name="userid">User id</param>
        /// <returns></returns>
        [HttpPost("/trader/{organisationId}/manageusers/{userId}/remove")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ManageUsersRemove(ViewSupplierViewModel model, int organisationId, string userId)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId)
                && this.User.GetUserId() != userId) //Cannot delete own account
            {
                try
                {
                    await this.IdentityService.RemoveUserAsync(userId, await this.TokenService.GetJwtToken());
                }
                catch (Exception ex)
                {
                    Logger.LogError("ManageUsersRemove:" + ex.ToString());
                }

                string obligationPeriodContext = string.Empty;
                if (model.ObligationPeriodId > 0)
                {
                    obligationPeriodContext = "?obligationPeriodId=" + model.ObligationPeriodId;
                }

                return Redirect("/trader/" + organisationId.ToString() + "/manageusers" + obligationPeriodContext);
            }
            else
            {
                return Forbid();
            }
        }


        /// <summary>
        /// Returns the supplier's trading details
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <returns>Returns the trading summary for a supplier</returns>
        [HttpGet("/trader/{organisationId}/trading")]
        [HttpGet("/trader/{organisationId}/obligationperiod/{obligationPeriodId}/trading")]
        public async Task<IActionResult> Trading(int organisationId, int? obligationPeriodId = null, [FromQuery]int page = 1)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetTradingSummary(organisationId
                    , obligationPeriod
                    , page
                    , this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("Trading", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }
    }
}