﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Website.Models.ObligationPeriodViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.Website.Controllers;
using DfT.GOS.Security.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Routing;

namespace GOS.Website.Controllers
{
    /// <summary>
    /// Controller for obligation period
    /// </summary>
    [Authorize(Roles = Roles.Administrator + ", " + Roles.Supplier + ", " + Roles.Trader)]
    public class ObligationPeriodController : GosWebsiteControllerBase
    {
        #region Private members
        private IOrganisationsService OrganisationsService { get; set; }
        private ITokenService TokenService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        #endregion Private members

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="organisationsService">The service to get data from</param>
        /// <param name="reportingPeriodsService">The reporting period service</param>
        /// <param name="tokenService">The token service</param>
        public ObligationPeriodController(IOrganisationsService organisationsService
            , IReportingPeriodsService reportingPeriodsService
            , ITokenService tokenService)
        {
            this.OrganisationsService = organisationsService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.TokenService = tokenService;
        }

        #endregion Constructors

        /// <summary>
        /// Gets the select obligation period view 
        /// </summary>
        /// <param name="selectedObligationPeriodId">The selected obligation period id</param>
        /// <param name="organisationId">Organisation id</param>
        [HttpGet("/obligationperiods/select/organisations/{organisationId}")]
        [HttpGet("/obligationperiods/{selectedObligationPeriodId}/select/organisations/{organisationId}")]
        public async Task<IActionResult> SelectObligation(int? selectedObligationPeriodId, int organisationId)
        {
            if (User.CanAccessOrganisationDetails(organisationId))
            {
                var suppliersResponse = await this.OrganisationsService.GetOrganisation(organisationId, await this.TokenService.GetJwtToken());
                if (!suppliersResponse.HasResult || suppliersResponse.Result == null)
                {
                    return NotFound("Organisation not found");
                }
                var obligationPeriods = await this.ReportingPeriodsService.GetObligationPeriods();
                var viewModel = new SelectObligationViewModel
                {
                    ObligationPeriods = obligationPeriods,
                    SelectedObligationPeriod = selectedObligationPeriodId,
                    ShowOrganisationContext = User.IsAdministrator(),
                    OrganisationId = organisationId,
                    OrganisationName = suppliersResponse.Result?.Name,
                    OrganisationTypeId = suppliersResponse.Result.TypeId,
                    RedirectTarget = RedirectTarget.None
                };
                ViewData["Referer"] = Request.Headers["Referer"].ToString();
                return View(viewModel);
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Gets the select obligation period admin view
        /// </summary>
        /// <param name="selectedObligationPeriodId">The selected obligation period id</param>
        [HttpGet("/obligationperiods/select")]
        [HttpGet("/obligationperiods/{selectedObligationPeriodId}/select")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> SelectObligationAdmin(int? selectedObligationPeriodId)
        {
            return await GetSelectObligationViewModel(selectedObligationPeriodId, RedirectTarget.None);
        }

        /// <summary>
        /// Gets the select obligation period admin view from admin home
        /// </summary>
        /// <param name="selectedObligationPeriodId">The selected obligation period id</param>
        [HttpGet("/obligationperiods/select/administratorhome")]
        [HttpGet("/obligationperiods/{selectedObligationPeriodId}/select/administratorhome")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> SelectObligationAdminHome(int? selectedObligationPeriodId)
        {
            return await GetSelectObligationViewModel(selectedObligationPeriodId, RedirectTarget.AdminHome);
        }

        /// <summary>
        /// Gets the select obligation period admin view from credit summary
        /// </summary>
        /// <param name="selectedObligationPeriodId">The selected obligation period id</param>
        [HttpGet("/obligationperiods/select/credit")]
        [HttpGet("/obligationperiods/{selectedObligationPeriodId}/select/credit")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> SelectObligationCreditTransfers(int? selectedObligationPeriodId)
        {
            return await GetSelectObligationViewModel(selectedObligationPeriodId, RedirectTarget.CreditSummary);
        }

        /// <summary>
        /// Handles the post from selecting an obligation period
        /// </summary>
        /// <param name="model">The view model</param>
        [HttpPost("/obligationperiods/select")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SelectObligation([FromForm] SelectObligationViewModel model)
        {
            if (this.User.IsAdministrator())
            {
                var suppliersResponseTask = this.OrganisationsService.GetOrganisation(model.OrganisationId
                    , await this.TokenService.GetJwtToken());

                if (model.RedirectTarget == RedirectTarget.AdminHome)
                {
                    return RedirectToAction("Home", "Home", new { obligationPeriodId = model.SelectedObligationPeriod });
                }
                else if (model.RedirectTarget == RedirectTarget.CreditSummary)
                {
                    return RedirectToAction("Summary", "Credits", new RouteValueDictionary(new { obligationPeriodId = model.SelectedObligationPeriod }));
                }
                else if (model.OrganisationId == 0)
                {
                    return RedirectToAction("Index", "Organisations", new { obligationPeriodId = model.SelectedObligationPeriod });
                }

                var supplierResponse = (await suppliersResponseTask).Result;

                if (supplierResponse.IsSupplier)
                {
                    return RedirectToAction("SupplierHome", "Suppliers", new
                    {
                        organisationId = model.OrganisationId,
                        obligationPeriodId = model.SelectedObligationPeriod
                    });
                }
                else if(supplierResponse.IsTrader)
                {
                    return RedirectToAction("TraderHome", "Trader", new
                    {
                        organisationId = model.OrganisationId,
                        obligationPeriodId = model.SelectedObligationPeriod
                    });
                }
            }
            return RedirectToAction("Home", "Home", new { obligationPeriodId = model.SelectedObligationPeriod });
        }

        #region Private Methods

        /// <summary>
        /// Returns the SelectObligationViewModel with Admin Home set to true or false
        /// </summary>
        /// <param name="selectedObligationPeriodId">The selected obligation period id</param>
        /// <param name="target">Redirect to selected target</param>
        /// <returns>Returns the SelectObligationViewModel with Admin Home set</returns>
        private async Task<ViewResult> GetSelectObligationViewModel(int? selectedObligationPeriodId, RedirectTarget target)
        {
            var obligationPeriods = await this.ReportingPeriodsService.GetObligationPeriods();
            var viewModel = new SelectObligationViewModel
            {
                ObligationPeriods = obligationPeriods,
                SelectedObligationPeriod = selectedObligationPeriodId,
                ShowOrganisationContext = User.IsAdministrator(),
                RedirectTarget = target
            };
            ViewData["Referer"] = Request.Headers["Referer"].ToString();
            return View("SelectObligation", viewModel);
        }

        #endregion Private Methods
    }
}
