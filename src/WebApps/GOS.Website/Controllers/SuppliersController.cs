﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Website.Models.SuppliersViewModels;
using DfT.GOS.Website.Orchestrators;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.Website.Controllers;
using DfT.GOS.Security.Services;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace GOS.Website.Controllers
{
    /// <summary>
    /// Controller for supplier details
    /// </summary>
    [Authorize(Roles = "Administrator, Supplier")]
    public class SuppliersController: GosWebsiteControllerBase
    {
        #region Properties

        private IOrganisationsService OrganisationsService { get; set; }
        private IUsersService IdentityService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private ILogger Logger { get; set; }
        private ITokenService TokenService { get; set; }
        private ISupplierReportingSummaryOrchestrator Orchestrator { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="organisationsService">The service to get data from</param>
        public SuppliersController(IOrganisationsService organisationsService
            , IUsersService identityService
            , IReportingPeriodsService reportingPeriodsService
            , ILogger<SuppliersController> logger
            , ITokenService tokenService
            , ISupplierReportingSummaryOrchestrator orchestrator)
        {
            this.OrganisationsService = organisationsService;
            this.IdentityService = identityService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.Logger = logger;
            this.TokenService = tokenService;
            this.Orchestrator = orchestrator;
        }

        #endregion Constructors

        /// <summary>
        /// Returns the supplier landing page for an administrator user
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <returns>Returns the supplier landing page, configured for an administrator</returns>
        [HttpGet("/suppliers/{organisationId}")]
        [HttpGet("/suppliers/{organisationId}/obligationPeriod/{obligationPeriodId}")]
        public async Task<IActionResult> SupplierHome(int organisationId, int obligationPeriodId = 0)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod currentObligationPeriod;
                if (obligationPeriodId > 0)
                {
                    currentObligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
                }
                else
                {
                    currentObligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetSupplierReportingSummary(organisationId, currentObligationPeriod, this.User);
                
                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("SupplierHome", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Navigate to the add a use via an email invite
        /// </summary>
        /// <param name="organisationId">This the organisation Id</param>
        /// <returns></returns>
        [HttpGet("/suppliers/{organisationId}/furtheractions")]
        [HttpGet("/suppliers/{organisationId}/ObligationPeriods/{obligationPeriodId}/furtheractions")]
        public async Task<IActionResult> FurtherActions(int organisationId, int obligationPeriodId = 0)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var orgModelResponseTask = this.OrganisationsService.GetOrganisation(organisationId, await this.TokenService.GetJwtToken());
                Task<ObligationPeriod> obligationPeriodTask;

                if (obligationPeriodId == 0)
                {
                    obligationPeriodTask  = this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }
                else
                {
                    obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
                }

                await Task.WhenAll(orgModelResponseTask, obligationPeriodTask);

                var orgModelResponse = orgModelResponseTask.Result;
                var obligationPeriod = obligationPeriodTask.Result;

                if (orgModelResponse.HasResult)
                {
                    if (obligationPeriod == null)
                    {
                        return new NotFoundResult();
                    }
                    else
                    {
                        var viewModel = new SupplierFurtherActionsViewModel(this.User.IsAdministrator()
                            , obligationPeriod.Id
                            , orgModelResponse.Result);

                        return View("FurtherActions", viewModel);
                    }
                }
                else
                {
                    return this.HandleFailedHttpObjectResponse(orgModelResponse);
                }               
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Supplier home page to manage users
        /// </summary>
        /// <param name="organisationId">Organisation Id</param>
        /// <returns></returns>
        [HttpGet("/suppliers/{organisationId}/manageusers")]
        public async Task<IActionResult> ManageUsers(int organisationId, int obligationPeriodId = 0)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {             
                var orgModelResponse = await this.OrganisationsService.GetOrganisation(organisationId, await this.TokenService.GetJwtToken());

                if (orgModelResponse.HasResult)
                {
                    var viewModel = new ViewSupplierViewModel()
                    {
                        Id = orgModelResponse.Result.Id,
                        Name = orgModelResponse.Result.Name,
                        Users = await this.IdentityService.GetOrganisationUsers(orgModelResponse.Result.Id, await this.TokenService.GetJwtToken()),
                        ShowOrganisationContext = User.IsAdministrator(),
                        Supplier = orgModelResponse.Result,
                        CurrentUserId = this.User.GetUserId(),
                        ObligationPeriodId = obligationPeriodId
                    };

                    return View("ManageUsers", viewModel);
                }
                else
                {
                    return this.HandleFailedHttpObjectResponse(orgModelResponse);
                }               
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// A Supplier will send an email invitation link to register for an account
        /// </summary>
        /// <param name="model">Supplier model</param>
        /// <returns></returns>
        [HttpPost("/suppliers/{id}/manageusers")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ManageUsers(ViewSupplierViewModel model)
        {
            if (this.User.CanAccessOrganisationDetails(model.Id))
            {
                var orgModelResponse = await this.OrganisationsService.GetOrganisation(model.Id, await this.TokenService.GetJwtToken());

                if (orgModelResponse.HasResult)
                {
                    var organisation = orgModelResponse.Result;

                    var viewModel = new ViewSupplierViewModel()
                    {
                        Id = organisation.Id,
                        Name = organisation.Name,
                        Email = model.Email,
                        Role = organisation.Type,
                        Users = new List<GosApplicationUser>(),
                        ShowOrganisationContext = User.IsAdministrator(),
                        ObligationPeriodId = model.ObligationPeriodId,
                        Supplier = organisation
                    };

                    if (!ModelState.IsValid)
                    {
                        return View(viewModel);
                    }

                    try
                    {
                        AddNewGosApplicationUser addUserRequestedBy = new AddNewGosApplicationUser()
                        {
                            RequestedByUserId = this.User.GetUserId(),
                            Email = model.Email,
                            OrganisationId = model.Id,
                            Role = organisation.Type,
                            Company = organisation.Name
                        };
                        var addUserResponse = await this.IdentityService.AddUserAsync(addUserRequestedBy, await this.TokenService.GetJwtToken());
                        viewModel.Users = await this.IdentityService.GetOrganisationUsers(organisation.Id, await this.TokenService.GetJwtToken());

                        if (addUserResponse.Message.Length > 0)
                        {
                            //Unable to add user to the organisation
                            ModelState.AddModelError("Email", addUserResponse.Message);
                        }
                        else if (addUserResponse.UserAssociatedWithOrganisation)
                        {
                            //An association between the user and orgnisation was made
                            return View("ManageUsersLinkedConfirmation", viewModel);
                        }
                        else
                        {
                            //A new user account request has been issued
                            return View("ManageUsersConfirmation", viewModel);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError("ManageUsers:" + ex.ToString());
                        return View("ManageUsersConfirmationError", viewModel);
                    }

                    return View(viewModel);
                }
                else
                {
                    return this.HandleFailedHttpObjectResponse(orgModelResponse);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Information to remove a user from an organisation
        /// </summary>
        /// <param name="organisationId">Organisation id</param>
        /// <param name="userid">User id</param>
        /// <param name="obligationPeriodId">The obligation context to pass back to other pages</param>
        [HttpGet("/suppliers/{organisationId}/manageusers/{userId}/remove")]
        public async Task<IActionResult> ManageUsersRemove(int organisationId, string userId, int? obligationPeriodId = null)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                if (ModelState.IsValid)
                {
                    var orgModelTask = this.OrganisationsService.GetOrganisation(organisationId, await this.TokenService.GetJwtToken());
                    var userModelTask = this.IdentityService.FindUserByIdAsync(userId, await this.TokenService.GetJwtToken(), default(CancellationToken));

                    await Task.WhenAll(orgModelTask, userModelTask);

                    var orgModelResponse = orgModelTask.Result;
                    var userModel = userModelTask.Result;

                    if (userModel == null)
                    {
                        return new NotFoundObjectResult(null);
                    }
                    if (!orgModelResponse.HasResult)
                    {
                        return new NotFoundObjectResult(null);
                    }

                    var viewModel = new ViewSupplierUserViewModel()
                    {
                        OrganisationId = orgModelResponse.Result.Id,
                        OrganisationName = orgModelResponse.Result.Name,
                        UserId = userModel.Id,
                        FullName = userModel.FullName,
                        Email = userModel.Email,
                        Role = orgModelResponse.Result.Type,
                        ObligationPeriodId = obligationPeriodId
                    };

                    return View("ManageUsersRemove", viewModel);
                }
                return View();
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Confirmation to remove a user from an organisation
        /// </summary>
        /// <param name="organisationId">Organisation id</param>
        /// <param name="userid">User id</param>
        /// <param name="obligationPeriodId">The obligation period context, if supplied, to pass to page called by this page</param>
        /// <returns></returns>
        [HttpPost("/suppliers/{organisationId}/manageusers/{userId}/remove")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ManageUsersRemove(ViewSupplierViewModel model, int organisationId, string userId)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId)
                && this.User.GetUserId() != userId) //Cannot delete own account
            {
                try
                {
                    await this.IdentityService.RemoveUserAsync(userId, await this.TokenService.GetJwtToken());
                }
                catch (Exception ex)
                {
                    Logger.LogError("ManageUsersRemove:" + ex.ToString());
                }
                string obligationPeriodContext = string.Empty;
                if(model.ObligationPeriodId > 0)
                {
                    obligationPeriodContext = "?obligationPeriodId=" + model.ObligationPeriodId;
                }
                return Redirect("/suppliers/" + organisationId.ToString() + "/manageusers" + obligationPeriodContext);
            }
            else
            {
                return Forbid();
            }
        }
        
    }
}