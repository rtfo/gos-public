﻿﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgMarketplace.API.Client.Services;
using DfT.GOS.GhgMarketplace.Common.Commands;
using DfT.GOS.GhgMarketplace.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Models.MarketplaceViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Website.Controllers
{
    /// <summary>
    /// Controller for Marketplace
    /// </summary>
    [Authorize(Roles = Roles.Administrator + ", " + Roles.Supplier + ", " + Roles.Trader)]
    public class MarketplaceController : GosWebsiteControllerBase
    {
        #region Properties
        private IMarketplaceService MarketplaceService { get; set; }
        private IOrganisationsService OrganisationsService { get; set; }
        private ILogger<MarketplaceController> Logger { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private ITokenService TokenService { get; set; }
        private IBalanceViewService BalanceViewService { get; set; }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="marketplaceService">Client to call the marketplace service</param>
        /// <param name="logger">Logger instance</param>
        /// <param name="reportingPeriodsService">Client to call the reporting periods service</param>
        /// <param name="tokenService">Service to get the JWT from</param>
        /// <param name="balanceViewService">Service to get balance from</param>
        /// <param name="organisationsService">The service to get organisation details from</param>
        public MarketplaceController(IMarketplaceService marketplaceService
            , ILogger<MarketplaceController> logger
            , IReportingPeriodsService reportingPeriodsService
            , ITokenService tokenService
            , IBalanceViewService balanceViewService
            , IOrganisationsService organisationsService)
        {
            this.MarketplaceService = marketplaceService;
            this.Logger = logger;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.TokenService = tokenService;
            this.BalanceViewService = balanceViewService;
            this.OrganisationsService = organisationsService;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Shows the AdviseMarket view
        /// </summary>
        /// <param name="organisationId">Organisation to show the view for</param>
        /// <param name="obligationPeriodId">Obligation period currently set</param>
        /// <returns>The AdviseMarket view</returns>
        [HttpGet("/suppliers/{organisationId}/obligationperiod/{obligationPeriodId}/Marketplace/Advertise")]
        [HttpGet("/trader/{organisationId}/obligationperiod/{obligationPeriodId}/Marketplace/Advertise")]
        public async Task<IActionResult> AdviseMarket(int organisationId, int? obligationPeriodId)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var jwtToken = await this.TokenService.GetJwtToken();

                Task<ObligationPeriod> obligationPeriodTask;
                var organisationTask = this.OrganisationsService.GetOrganisation(organisationId, jwtToken);

                if (!obligationPeriodId.HasValue)
                {
                    //Default to the current obligation period
                    obligationPeriodTask = this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }
                else
                {
                    obligationPeriodTask = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }

                await Task.WhenAll(obligationPeriodTask, organisationTask);
                var obligationPeriod = await obligationPeriodTask;
                var organisation = await organisationTask;

                if (!organisation.HasResult)
                {
                    return this.HandleFailedHttpObjectResponse(organisation);
                }

                var ghgCreditAdvertTask = this.MarketplaceService.GetGhGCreditAdvert(organisation.Result.Id, obligationPeriod.Id, jwtToken);
                var balanceViewTask = this.BalanceViewService.GetSummary(organisation.Result.Id, obligationPeriod.Id, jwtToken);

                await Task.WhenAll(ghgCreditAdvertTask, balanceViewTask);
                var creditAdvert = await ghgCreditAdvertTask;
                var balanceSummary = await balanceViewTask;

                if (!creditAdvert.HasResult)
                {
                    return this.HandleFailedHttpObjectResponse(creditAdvert);
                }

                var viewModel = new MarketplaceAdvertViewModel(
                    organisation.Result
                    , obligationPeriod
                    , this.User
                    , creditAdvert.Result
                    );

                if (balanceSummary.HasResult && balanceSummary.Result != null)
                {
                    viewModel.Credits = balanceSummary.Result.BalanceCredits;
                }
                return View("AdviseMarket", viewModel);

            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Updates the organisations advertising options
        /// </summary>
        /// <param name="organisationId">Organisation to update</param>
        /// <param name="viewModel">Model containing the details to set</param>
        /// <returns>Redirects to the marketplace on a success</returns>
        [HttpPost("/suppliers/{organisationId}/obligationperiod/{obligationPeriodId}/Marketplace/Advertise")]
        [HttpPost("/trader/{organisationId}/obligationperiod/{obligationPeriodId}/Marketplace/Advertise")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Advertise(int organisationId, int obligationPeriodId, [FromForm] MarketplaceAdvertViewModel viewModel)
        {
            //Call the service to set the Advert settings
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var jwtToken = await this.TokenService.GetJwtToken();
                var advert = new MarketplaceAdvertCommand(
                    viewModel.BuyCredits,
                    viewModel.SellCredits,
                    viewModel.FullName,
                    viewModel.EmailAddress,
                    viewModel.PhoneNumber
                    );

                var success = await this.MarketplaceService.SetGhGCreditAdvert(organisationId, obligationPeriodId, advert, jwtToken);
                if (!success)
                {
                    this.Logger.LogError("Unable to create or update an advert.");
                    if (ModelState.ErrorCount == 0)
                    {
                        ModelState.AddModelError(string.Empty, "Unable to advertise credits to buy or sell at this time, please try again.");
                    }
                    return View("AdviseMarket", viewModel);
                }
                return Redirect("/Marketplace?ObligationPeriodId=" + obligationPeriodId + "&OrganisationId=" + organisationId);
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Get methods for the Marketplace adverts
        /// </summary>
        /// <param name="obligationPeriodId">The obligation period to show the marketplace adverts for</param>
        /// <returns>Returns the marketplace adverts view</returns>
        [HttpGet("/Marketplace")]
        public async Task<IActionResult> Marketplace(int? obligationPeriodId = null, int? organisationId = null)
        {
            ObligationPeriod obligationPeriod = null;
            if (!obligationPeriodId.HasValue)
            {
                //Default to the current obligation period
                obligationPeriod = (await this.ReportingPeriodsService.GetCurrentObligationPeriod());
            }
            else
            {
                obligationPeriod = (await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value));
            }

            if (obligationPeriod == null)
            {
                return NotFound();
            }

            var jwtToken = await this.TokenService.GetJwtToken();

            string organisationName = string.Empty;
            if (organisationId.HasValue)
            {
                var orgModelResponse = await this.OrganisationsService.GetOrganisation(organisationId.Value, jwtToken);
                if (orgModelResponse.HasResult)
                {
                    organisationName = orgModelResponse.Result.Name;
                }
            }

            HttpObjectResponse<MarketplaceAdverts> advertsResponse = null;
            MarketplaceAdvertsViewModel viewModel;

            try
            {
                advertsResponse = await this.MarketplaceService.GetMarketplaceAdverts(obligationPeriod.Id, jwtToken);
            }
            catch (Exception ex)
            {
                //There was an error contacting the marketplace service
                this.Logger.LogError(ex, "An error occurred attempting to contact the marketplace service.");
            }

            if (advertsResponse != null
                && advertsResponse.HasResult)
            {
                var adverts = advertsResponse.Result;

                //Get the Organisation details for the adverts
                var organisationsLookingToBuyTask = AddOrganisationNames(adverts.OrganisationsAdvertisingToBuy, jwtToken);
                var organisationsLookingToSellTask = AddOrganisationNames(adverts.OrganisationsAdvertisingToSell, jwtToken);

                var organisationsLookingToBuy = await organisationsLookingToBuyTask;
                var organisationsLookingToSell = await organisationsLookingToSellTask;

                viewModel = new MarketplaceAdvertsViewModel(obligationPeriod
                    , organisationsLookingToBuy.OrderBy(o => o.OrganisationName).ToList()
                    , organisationsLookingToSell.OrderBy(o => o.OrganisationName).ToList()
                    , organisationId
                    , organisationName
                    , this.User.IsAdministrator());
            }
            else
            {
                //Something went wrong with the service call - simply highlight the issue to the user
                viewModel = new MarketplaceAdvertsViewModel(obligationPeriod
                    , organisationId
                    , organisationName
                    , this.User.IsAdministrator()
                    , marketplaceAvailable: false);
            }

            return View("Marketplace", viewModel);
            
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Gets a list of organisation names based upon an organisation IDs
        /// </summary>
        /// <param name="adverts">The list of adverts</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns a list of organisation with their names</returns>
        private async Task<List<NamedMarketplaceAdvert>> AddOrganisationNames(List<MarketplaceAdvert> adverts, string jwtToken)
        {
            //Get the Organisation names from the organisations service           
            List<Task<HttpObjectResponse<Organisation>>> organisationTasks = new List<Task<HttpObjectResponse<Organisation>>>();
            foreach (var advert in adverts)
            {
                organisationTasks.Add(this.OrganisationsService.GetOrganisation(advert.OrganisationId, jwtToken));
            }

            await Task.WhenAll(organisationTasks.ToArray());

            var advertsWithNames = new List<NamedMarketplaceAdvert>();
            foreach (var organisationTask in organisationTasks)
            {
                var organisationResult = await organisationTask;
                if (organisationResult == null)
                {
                    //We were unable to get the organisation name from the service. Log it, but continue so we still return as much info as possible
                    this.Logger.LogError("Unable to get the organisation details for organisation");
                }
                else
                {
                    // This update the name on the objects in the list
                    advertsWithNames.AddRange(adverts.Where(a => a.OrganisationId == organisationResult.Result.Id).Select(a => new NamedMarketplaceAdvert(organisationResult.Result.Name, a)).ToList());
                }
            }

            return advertsWithNames;
        }

        #endregion Private Methods
    }
}
