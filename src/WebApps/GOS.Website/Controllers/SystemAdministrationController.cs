﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgBalanceOrchestrator.API.Client.Models;
using DfT.GOS.GhgBalanceOrchestrator.API.Client.Services;
using DfT.GOS.GhgLedger.API.Client.Services;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.RtfoApplications.API.Client.Services;
using DfT.GOS.Security.Claims;
using DfT.GOS.Security.Services;
using DfT.GOS.Website;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Models.AdministratorViewModels;
using DfT.GOS.Website.Services;
using GOS.RtfoApplications.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOS.Website.Controllers
{
    /// <summary>
    /// Controller for System Administration specific actions
    /// </summary>
    [Authorize(Roles = Roles.Administrator)]
    public class SystemAdministrationController : Controller
    {
        #region Properties

        private IApplicationsService ApplicationsService { get; set; }
        private IUsersService UsersService { get; set; }
        private ILedgerService LedgerService { get; set; }
        private IBalanceAdministrationService BalanceAdministrationService { get; set; }
        private IRtfoPerformanceDataService RtfoPerformanceDataService { get; set; }
        private IPerformanceDataHelperService PerformanceDataHelperService { get; set; }
        private ITokenService TokenService { get; set; }
        private AppSettings AppSettings { get; set; }
        private ILogger<SystemAdministrationController> Logger { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="applicationsService">the application service</param>
        /// <param name="balanceAdministrationService">The remote service for administering balances</param>
        /// <param name="rtfoPerformanceDataService">The remote service for accessing RTFO based performance data</param>
        public SystemAdministrationController(IApplicationsService applicationsService
            , IUsersService usersService
            , ILedgerService ledgerService
            , IBalanceAdministrationService balanceAdministrationService
            , IRtfoPerformanceDataService rtfoPerformanceDataService
            , IPerformanceDataHelperService performanceDataHelperService
            , ITokenService tokenService
            , IOptions<AppSettings> appSettings
            , ILogger<SystemAdministrationController> logger)
        {
            if (appSettings == null || appSettings.Value == null)
            {
                throw new ArgumentNullException("appSettings");
            }

            this.ApplicationsService = applicationsService ?? throw new ArgumentNullException("applicationsService");
            this.UsersService = usersService ?? throw new ArgumentNullException("usersService");
            this.LedgerService = ledgerService ?? throw new ArgumentNullException("ledgerService");
            this.BalanceAdministrationService = balanceAdministrationService ?? throw new ArgumentNullException("balanceAdministrationService");
            this.RtfoPerformanceDataService = rtfoPerformanceDataService ?? throw new ArgumentNullException("rtfoPerformanceDataService");
            this.PerformanceDataHelperService = performanceDataHelperService ?? throw new ArgumentNullException("performanceDataHelperService");
            this.TokenService = tokenService ?? throw new ArgumentNullException("tokenService");
            this.AppSettings = appSettings.Value;
            this.Logger = logger ?? throw new ArgumentNullException("logger");
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// View for Managing GHG Balance data
        /// </summary>
        /// <returns>Returns the GHG Balances view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet]
        public IActionResult GhgBalances([FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            return View();
        }

        /// <summary>
        /// Handles an update ghg balance request
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = Roles.Administrator)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> UpdateGhgBalances([FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            try
            {
                var result = await this.BalanceAdministrationService.UpdateCollections(await this.TokenService.GetJwtToken());
                if (result.HasResult)
                {
                    return RedirectToAction("UpdateGhgBalances", new { bulkUpdateUniqueId = result.Result, selectedObligationPeriodId });
                }
                else
                {
                    this.ModelState.AddModelError("balance", "Unable to update GHG balances at this time. Please try again later");
                    return View("GhgBalances");
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "An error occurred whilst requesting update of the GHG balances.");
                this.ModelState.AddModelError("balance", "Unable to update GHG balances at this time. Please try again later");
                return View("GhgBalances");
            }
        }

        /// <summary>
        /// Retrieves details of the requested update ghg balance operation
        /// </summary>
        /// <param name="bulkUpdateUniqueId">Id of Collection Administration bulk update operation to check</param>
        /// <param name="getView">Function used to generate output based on the Collection Administration details</param>
        /// <returns>Returns the result of calling the supplied function with the retrieved Collection Administration details</returns>
        private async Task<IActionResult> UpdateGhgBalanceConfirmation(string bulkUpdateUniqueId, Func<CollectionAdministration, IActionResult> getView)
        {
            try
            {
                var jwtToken = await this.TokenService.GetJwtToken();
                var result = await this.BalanceAdministrationService.GetUpdateStatus(bulkUpdateUniqueId, jwtToken);

                if (result.HasResult)
                {
                    return getView(result.Result);
                }
                else
                {
                    this.Logger.LogWarning($"Unable to retrieve current status of Collection Administration with id '{bulkUpdateUniqueId}'.  Returned HTTP status code was '{result.HttpStatusCode}'.");
                    return getView(null);
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "An error occurred whilst checking the status of a GHG bulk update.");
                return getView(null);
            }
        }

        /// <summary>
        /// View confirming the update ghg balance operation was succesfully made
        /// </summary>
        /// <param name="bulkUpdateUniqueId">Id of Collection Administration bulk update operation to check</param>
        /// <returns>Returns the Update GHG Balances confirmation view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("SystemAdministration/UpdateGhgBalances/{bulkUpdateUniqueId}")]
        public async Task<IActionResult> UpdateGhgBalances(string bulkUpdateUniqueId, [FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            ViewBag.UpdateGhgBalanceConfirmationRefreshIntervalInSeconds = this.AppSettings.UpdateGhgBalanceConfirmationRefreshIntervalInSeconds;
            return await UpdateGhgBalanceConfirmation(bulkUpdateUniqueId, ca => View("UpdateGhgBalanceConfirmation", ca));
        }

        /// <summary>
        /// Partial View confirming the update ghg balance operation was succesfully made
        /// </summary>
        /// <param name="bulkUpdateUniqueId">Id of Collection Administration bulk update operation to check</param>
        /// <returns>Returns the Update GHG Balances confirmation partial view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("SystemAdministration/UpdateGhgBalances/{bulkUpdateUniqueId}/Refresh")]
        public async Task<IActionResult> UpdateGhgBalancesPartial(string bulkUpdateUniqueId)
        {
            return await UpdateGhgBalanceConfirmation(bulkUpdateUniqueId, ca => PartialView("_UpdateGhgBalanceConfirmationPartial", ca));
        }

        /// <summary>
        /// View for download all performance data
        /// </summary>
        /// <returns>Returns the GHG Balances view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet]
        public IActionResult DownloadAllPerformanceData([FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            var viewModel = new DownloadAllPerformanceDataViewModel();
            return View("DownloadAllPerformanceData", viewModel);
        }

        /// <summary>
        /// Returns the CSV containing the performance data export
        /// </summary>
        /// <returns>Returns the CSV containing the performance data export</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("SystemAdministration/GeneratePerformanceDataDownload")]
        public async Task<IActionResult> GeneratePerformanceDataDownload()
        {
            var jwtToken = await this.TokenService.GetJwtToken();
            var applicationPerformanceDataTask = this.ApplicationsService.GetApplicationPerformanceData(jwtToken);
            var registrationPerformanceDataTask = this.UsersService.RegistrationsPerformanceData(jwtToken);
            var ledgerPerformanceDataTask = this.LedgerService.GetLedgerPerformanceData(jwtToken);
            var rtfoPerformanceDataTask = this.RtfoPerformanceDataService.GetPerformanceData(jwtToken);

            var applicationPerformanceData = await applicationPerformanceDataTask;
            var registrationPerformanceData = await registrationPerformanceDataTask;
            var ledgerPerformanceData = await ledgerPerformanceDataTask;

            var rtfoPerformanceDataResult = await rtfoPerformanceDataTask;
            var rtfoPerformanceData = new List<RtfoPerformanceData>();

            if (rtfoPerformanceDataResult.HasResult)
            {                
                rtfoPerformanceData = rtfoPerformanceDataResult.Result.ToList();
            }
            else
            {
                this.Logger.LogError("Unable to report on ROS Performance data, the service didn't return results.");
            }

            var appPerformanceData = applicationPerformanceData.ConvertAll(x => new PerformanceData(x));
            var regPerformanceData = registrationPerformanceData.ConvertAll(x => new PerformanceData(x));
            var ledPerformanceData = ledgerPerformanceData.ConvertAll(x => new PerformanceData(x));
            var formattedRtfoPerformanceData = rtfoPerformanceData.ConvertAll(x => new PerformanceData(x));

            var performanceData = this.PerformanceDataHelperService.GetConcatenatedPerformanceData(appPerformanceData
                , regPerformanceData
                , ledPerformanceData
                , formattedRtfoPerformanceData);

            var export = new StringBuilder(PerformanceData.GetHeaderText() + "\r\n");

            foreach (var metric in performanceData)
            {
                export.Append(metric.ToString() + "\r\n");
            }

            var fileName = "PerformanceData-" + DateTime.Now.ToString("ddMMyyhhmmss") + ".csv";

            return File(Encoding.UTF8.GetBytes(export.ToString()), "application/octet-stream", fileName);
        }

        #endregion Methods
    }
}