﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Website.Orchestrators;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using DfT.GOS.Website.Controllers;
using Microsoft.Extensions.Options;
using DfT.GOS.Website;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Website.Models.AdministratorViewModels;

namespace GOS.Website.Controllers
{
    [Route("suppliers")]
    [Authorize(Roles = "Administrator, Supplier")]
    public class SupplierSubmissionsController : GosWebsiteControllerBase
    {
        #region Properties

        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private ISupplierReportingSummaryOrchestrator Orchestrator { get; set; }
        private IBalanceViewService BalanceViewService { get; set; }
        private ITokenService TokenService { get; set; }
        private IOrganisationsService OrganisationsService { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="appSettings">The app config settings</param>
        /// <param name="organisationsService">The service to get data from</param>
        public SupplierSubmissionsController(IReportingPeriodsService reportingPeriodsService
            , ISupplierReportingSummaryOrchestrator orchestrator
            , IBalanceViewService balanceViewService
            , ITokenService tokenService
            , IOrganisationsService organisationsService
            , IOptions<AppSettings> appSettings)
        {

            this.ReportingPeriodsService = reportingPeriodsService ?? throw new ArgumentNullException("reportingPeriodsService");
            this.Orchestrator = orchestrator ?? throw new ArgumentNullException("orchestrator");
            this.BalanceViewService = balanceViewService ?? throw new ArgumentNullException("balanceViewService");
            this.TokenService = tokenService ?? throw new ArgumentNullException("tokenService");
            this.OrganisationsService = organisationsService ?? throw new ArgumentNullException("organisationsService");
            this.AppSettings = appSettings;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Returns the supplier's submissions 1st level details
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <returns>Returns the supplier landing page, configured for an administrator</returns>
        [HttpGet("{organisationId}/submissions")]
        [HttpGet("{organisationId}/obligationperiod/{obligationPeriodId}/submissions")]
        public async Task<IActionResult> Index(int organisationId, int? obligationPeriodId = null)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetSupplierSubmissionsSummary(organisationId, obligationPeriod, this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("Submissions", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Returns the supplier's trading details
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <returns>Returns the trading summary for a supplier</returns>
        [HttpGet("{organisationId}/trading")]
        [HttpGet("{organisationId}/obligationperiod/{obligationPeriodId}/trading")]
        public async Task<IActionResult> Trading(int organisationId, int? obligationPeriodId = null, [FromQuery]int page = 1)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetTradingSummary(organisationId
                    , obligationPeriod
                    , page
                    , this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("Trading", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Returns the supplier's submissions 2nd level liquid and gas summary
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <returns>Returns the liquid and gas summary page</returns>
        [HttpGet("{organisationId}/submissions/liquidgassummary")]
        [HttpGet("{organisationId}/obligationperiod/{obligationPeriodId}/submissions/liquidgassummary")]
        public async Task<IActionResult> LiquidGasSummary(int organisationId, int? obligationPeriodId = null)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetSupplierSubmissionsLiquidGasSummary(organisationId, obligationPeriod, this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("LiquidAndGas", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Returns the supplier's submissions 2nd level liquid and gas summary
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <returns>Returns the liquid and gas summary page</returns>
        [HttpGet("{organisationId}/submissions/rtfoAdminconsignments")]
        [HttpGet("{organisationId}/obligationperiod/{obligationPeriodId}/submissions/rtfoAdminconsignments")]
        public async Task<IActionResult> RtfoAdminConsignments(int organisationId, [FromQuery]int page = 1, int? obligationPeriodId = null)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetSupplierRtfoAdminConsignment(organisationId, obligationPeriod
                    , page, this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("RtfoAdminConsignments", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Returns the supplier's submissions 3rd level other volumes via RTFO view
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="page">Page value</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <returns>Returns the other volumes via RTFO page</returns>
        [HttpGet("{organisationId}/submissions/otherrtfovolumes")]
        [HttpGet("{organisationId}/obligationperiod/{obligationPeriodId}/submissions/otherrtfovolumes")]
        public async Task<IActionResult> OtherRtfoVolumes(int organisationId, [FromQuery]int page = 1, int? obligationPeriodId = null)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetSupplierSubmissionsOtherRtfoVolumes(organisationId, obligationPeriod, page, AppSettings.Value.Pagination.MaximumPagesToDisplay, this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("OtherRtfoVolumes", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Returns the supplier's submissions 3nd level Fossil fuel
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <param name="page">page number</param>
        /// <returns>Returns the liquid and gas summary page</returns>
        [HttpGet("{organisationId}/submissions/fossilgas")]
        [HttpGet("{organisationId}/obligationperiod/{obligationPeriodId}/submissions/fossilgas")]
        public async Task<IActionResult> FossilFuels(int organisationId, [FromQuery]int page = 1, int? obligationPeriodId = null)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetFossilFuelSubmissions(organisationId, obligationPeriod
                    , page, this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("FossilFuels", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Returns the supplier's electricity submissions
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <param name="page">page number</param>
        /// <returns>Returns the electricity submissions details page</returns>
        [HttpGet("{organisationId}/submissions/electricity")]
        [HttpGet("{organisationId}/obligationperiod/{obligationPeriodId}/submissions/electricity")]
        public async Task<IActionResult> Electricity(int organisationId, [FromQuery]int page = 1, int? obligationPeriodId = null)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetElectricitySubmissions(organisationId, obligationPeriod
                    , page, this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("Electricity", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Returns the supplier's Upstream emissions reductions (UER) submissions
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <param name="page">page number</param>
        /// <returns>Returns the Upstream emissions reductions (UER) submissions details page</returns>
        [HttpGet("{organisationId}/submissions/upstreamEmissionReductions")]
        [HttpGet("{organisationId}/obligationperiod/{obligationPeriodId}/submissions/upstreamEmissionReductions")]
        public async Task<IActionResult> UpstreamEmissionsReductions(int organisationId, [FromQuery]int page = 1, int? obligationPeriodId = null)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                ObligationPeriod obligationPeriod;
                if (obligationPeriodId != null)
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                }
                else
                {
                    obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                }

                var viewModel = await this.Orchestrator.GetUpstreamEmissionsReductionsSubmissions(organisationId, obligationPeriod
                    , page, this.User);

                if (viewModel == null)
                {
                    return NotFound();
                }
                else
                {
                    return View("UpstreamEmissionsReductions", viewModel);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Returns the CSV containing the submissions export
        /// </summary>
        /// <param name="organisationId">The unique organisationId of the supplier to view</param>
        /// <param name="obligationPeriodId">Obligation Period Id to view</param>
        /// <returns>Returns the CSV containing the submissions export</returns>
        [HttpGet("{organisationId}/obligationperiod/{obligationPeriodId}/GenerateExportForOrganisation")]
        public async Task<IActionResult> GenerateExportDownload(int organisationId, int obligationPeriodId)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var jwtToken = await this.TokenService.GetJwtToken();
                var exportBytesTask = this.BalanceViewService.GenerateExportForOrganisation(organisationId, obligationPeriodId, jwtToken);
                var orgTask = this.OrganisationsService.GetOrganisation(organisationId, jwtToken);
                var obligationPeriodDateTask = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);

                await Task.WhenAll(exportBytesTask, orgTask, obligationPeriodDateTask);

                var exportBytes = exportBytesTask.Result;
                var org = orgTask.Result;
                var obligationPeriodDate = obligationPeriodDateTask.Result;

                if (org == null || obligationPeriodDate == null)
                {
                    return NotFound();
                }

                var fileName = org.Result.Name.Replace(" ", "") + "-" + obligationPeriodDate.EndDate.Value.Year + "-" + DateTime.Now.ToString("ddMMyyhhmmss") + ".csv";

                return File(exportBytes.Result, "application/octet-stream", fileName);
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// View for Downloading all application data for Obligation Period
        /// </summary>
        /// <returns>Returns the download application data view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("/SupplierSubmissions/DownloadAllApplications")]
        [HttpGet("ObligationPeriod/{obligationPeriodId}/SupplierSubmissions/DownloadAllApplications")]
        public async Task<IActionResult> DownloadAllApplications(int? obligationPeriodId = null)
        {
            ObligationPeriod obligationPeriod;
            if (obligationPeriodId != null)
            {
                obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId.Value);
                if (obligationPeriod == null)
                {
                    return NotFound();
                }
            }
            else
            {
                obligationPeriod = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
            }
            return View(new DownloadAllApplicationsViewModel
            {
                ObligationPeriod = obligationPeriod
            });
        }

        /// <summary>
        /// Method for downloading all application data for Obligation Period
        /// </summary>
        /// <returns>Returns the CSV download</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("obligationperiod/{obligationPeriodId}/GenerateExportForAllOrganisations")]
        public async Task<IActionResult> DownloadAllApplications(int obligationPeriodId)
        {
            var jwtToken = await this.TokenService.GetJwtToken();
            var exportBytesTask = this.BalanceViewService.GenerateExportForAllOrganisations(obligationPeriodId, jwtToken);
            var obligationPeriodDateTask = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);

            await Task.WhenAll(exportBytesTask, obligationPeriodDateTask);

            var exportBytes = await exportBytesTask;
            var obligationPeriodDate = await obligationPeriodDateTask;

            if (obligationPeriodDate == null)
            {
                return NotFound();
            }

            var fileName = "AllOrganisations-" + obligationPeriodDate.EndDate.Value.Year + "-" + DateTime.Now.ToString("ddMMyyhhmmss") + ".csv";

            return File(exportBytes.Result, "application/octet-stream", fileName);
        }
        #endregion Methods
    }
}