﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Models.AdministratorViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GOS.Website.Controllers
{
    /// <summary>
    /// Controller for Administrator specific actions
    /// </summary>
    [Authorize(Roles = Roles.Administrator)]
    public class AdministratorController : Controller
    {
        private IUsersService _usersService;
        private ITokenService _tokenService;
        private IOrganisationsService _organisationsService;
        private readonly ILogger _logger;
        /// <summary>
        /// Constructor taking service parameters
        /// </summary>
        /// <param name="usersService"></param>
        /// <param name="tokenService"></param>
        /// <param name="organisationService"></param>
        /// <param name="logger">Logger service</param>
        public AdministratorController(IUsersService usersService
            , ITokenService tokenService
            , IOrganisationsService organisationService
            , ILogger<AdministratorController> logger)
        {
            this._usersService = usersService;
            this._tokenService = tokenService;
            this._organisationsService = organisationService;
            _logger = logger;
        }
        /// <summary>
        /// View for administrator further actions
        /// </summary>
        /// <returns>Returns the further actions  view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("Administrator/FurtherActions")]
        [HttpGet("ObligationPeriod/{obligationPeriodId}/Administrator/FurtherActions")]
        public IActionResult FurtherActions(int? obligationPeriodId = null)
        {
            var viewModel = new FurtherActionsViewModel();
            if (obligationPeriodId != null)
            {
                viewModel.ObligationPeriodId = obligationPeriodId.Value;
                ViewBag.selectedObligationPeriodId = obligationPeriodId.Value;
                return View("FurtherActions", viewModel);
            }
            else
            {
                return View("FurtherActions", viewModel);
            }
        }

        /// <summary>
        /// Gets the list of admin users
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> ManageUsers([FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            var users = await this._usersService.GetAdminUsers(await this._tokenService.GetJwtToken());
            var viewModel = new ManageAdminUsersViewModel
            {
                CurrentUserId = this.User.GetUserId(),
                Users = users.Result
            };
            return View(viewModel);
        }

        /// <summary>
        /// Posts the request to add a new administrator
        /// </summary>
        /// <param name="viewModel">Manager admin user view model</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> ManageUsers(ManageAdminUsersViewModel viewModel, [FromQuery] int? selectedObligationPeriodId)
        {
            var token = await this._tokenService.GetJwtToken();
            var users = await this._usersService.GetAdminUsers(token);
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var newGosApplicationUser = new AddNewGosApplicationUser
                    {
                        Email = viewModel.Email,
                        Role = Roles.Administrator
                    };

                    var result = await this._usersService.AddAdminUser(newGosApplicationUser, token);
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (result.UserInvited)
                        {
                            // A new user account request has been issued
                            return View("ManageUsersConfirmation", viewModel);
                        }
                        else
                        {
                            // An existing user had been linked
                            return View("ManageUsersLinkedConfirmation", viewModel);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("UnexpectedError"
                            , result.Message);
                        users = await this._usersService.GetAdminUsers(token);
                    }                   
                   
                }
                catch (Exception ex)
                {
                    this._logger.LogError(ex, "An error occurred while updating the user role");
                    ModelState.AddModelError("UnexpectedError", "An error occurred while updating the user role");
                }
            }
            
            var newViewModel = new ManageAdminUsersViewModel
            {
                CurrentUserId = this.User.GetUserId(),
                Users = users.Result
            };
            return View(newViewModel);
        }
        /// <summary>
        /// Remove the administrator role for an user
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <returns></returns>
        [HttpGet("manageUsers/{userId}/remove")]
        public async Task<IActionResult> ManageUsersRemove(string userId, [FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            var token = await this._tokenService.GetJwtToken();
            var user = await this._usersService.FindUserByIdAsync(userId, token, default(CancellationToken));
            if(user  == null)
            {
                return RedirectToAction("ManageUsers", new { selectedObligationPeriodId });
            }
            var users = await this._usersService.GetAdminUsers(token);
            if (users.HasResult)
            {
                if(users.Result.Count(x => x.Id == userId) == 0)
                {
                    return RedirectToAction("ManageUsers", new { selectedObligationPeriodId });
                }
            }
            var viewModel = new AdminUserViewModel()
            {
                Id = user.Id,
                FullName = user.FullName,
                Email = user.Email,
            };
            return View("ManageUsersRemove",viewModel);
        }

        /// <summary>
        /// Posts remove admin user request
        /// </summary>
        /// <param name="adminUser">Admin user view model</param>
        /// <param name="userId">User identifier</param>
        /// <returns></returns>
        [HttpPost("manageUsers/{userId}/remove")]
        public async Task<IActionResult> ManageUsersRemove(AdminUserViewModel adminUser, string userId, [FromQuery] int? selectedObligationPeriodId)
        {
            if (selectedObligationPeriodId != null)
            {
                ViewBag.selectedObligationPeriodId = selectedObligationPeriodId;
            }
            var token = await this._tokenService.GetJwtToken();
            if (this.User.GetUserId() != userId) //Cannot delete own account
            {
                try
                {
                    var result = await this._usersService.RemoveAdministratorRole(userId, token);
                }
                catch(Exception ex)
                {
                    this._logger.LogError(ex, "An error occurred while updating the user role");
                    ModelState.AddModelError("UnexpectedError", "An error occurred while updating the user role. Please try after sometime.");
                }
            }
            else
            {
                return Forbid();
            }
            return RedirectToAction("ManageUsers", new { selectedObligationPeriodId });
        }
    }
}