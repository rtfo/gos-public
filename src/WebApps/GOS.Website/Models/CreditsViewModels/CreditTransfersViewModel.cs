﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Models;
using System;

namespace DfT.GOS.Website.Models.CreditsViewModels
{
    public class CreditTransfersViewModel
    {
        public int ObligationPeriodId { get; set; }
        public DateTime? ObligationPeriodEndDate { get; set; }
        public string SearchText { get; set; }
        public UIPagedResult<CreditTransferSummary> ResultPaged { get; private set; }

        public CreditTransfersViewModel() { }

        public CreditTransfersViewModel(
            int obligationPeriodId,
            DateTime? obligationPeriodEndDate,
            string searchText,
            UIPagedResult<CreditTransferSummary> pagedResult
        )
        {
            this.ObligationPeriodId = obligationPeriodId;
            this.ObligationPeriodEndDate = obligationPeriodEndDate;
            this.SearchText = searchText;
            this.ResultPaged = pagedResult;
        }
    }
}
