﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models.CreditsViewModels
{
    /// <summary>
    /// View model for the Issue Credits screen
    /// </summary>
    public class PagedIssueCreditsViewModel
    {
        #region Properties

        /// <summary>
        /// The application results
        /// </summary>
        public UIPagedResult<ApplicationReadyForIssueViewModel> Applications { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="applications">The applications to display</param>
        public PagedIssueCreditsViewModel(UIPagedResult<ApplicationReadyForIssueViewModel> applications)
        {
            this.Applications = applications;
        }

        #endregion Constructors
    }
}
