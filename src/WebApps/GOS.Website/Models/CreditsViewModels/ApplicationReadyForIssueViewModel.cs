﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.Website.Models.CreditsViewModels
{
    /// <summary>
    /// View model for holding details about an application that is ready for issue
    /// </summary>
    public class ApplicationReadyForIssueViewModel
    {
        public DateTime SubmittedDate { get; set; }
        public string Supplier { get; set; }
        public decimal CreditsApplierFor { get; set; }
        public int SupplierId { get; set; }
        public int ApplicationId { get; set; }
    }
}
