﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Collections.Generic;

namespace DfT.GOS.Website.Models.ObligationPeriodViewModels
{
    /// <summary>
    /// For handling where to navigate the page to after selecting a different obligation period
    /// </summary>
    public enum RedirectTarget
    {
        None = 0,
        AdminHome,
        CreditSummary
    }
    /// <summary>
    /// Select obligation view model
    /// </summary>
    public class SelectObligationViewModel
    {
        public SelectObligationViewModel() { }
        /// <summary>
        /// Gets and sets Obligation periods
        /// </summary>
        public IList<ObligationPeriod> ObligationPeriods { get; set; }
        /// <summary>
        /// Gets and sets selected obligation period
        /// </summary>
        public int? SelectedObligationPeriod { get; set; }
        /// <summary>
        /// Gets and sets the admin falg
        /// </summary>
        public bool ShowOrganisationContext { get; set; }
        /// <summary>
        /// Gets and sets organisationid
        /// </summary>
        public int OrganisationId { get; set; }
        /// <summary>
        /// Gets and sets organisation name
        /// </summary>
        public string OrganisationName { get; internal set; }

        /// <summary>
        /// The ID of the type of the organisation
        /// </summary>
        public int OrganisationTypeId { get; set; }

        /// <summary>
        /// Redirect to admin home flag
        /// </summary>
        public RedirectTarget RedirectTarget { get; set; }
    }
}
