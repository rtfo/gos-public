﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Website.Models
{
    /// <summary>
    /// Class containing review information for the UI
    /// </summary>
    public class Review
    {
        #region Enums

        /// <summary>
        /// Enum of the types of review statuses
        /// </summary>
        public enum ReviewStatuses
        {
            [Display(Name = "Not reviewed")]
            NotReviewed = 1,
            [Display(Name = "Approved")]
            Approved = 2,
            [Display(Name = "Rejected")]
            Rejected = 3
        }

        #endregion Enums
    }
}
