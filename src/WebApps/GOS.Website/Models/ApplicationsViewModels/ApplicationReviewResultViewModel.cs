﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using System;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View model fro application review result
    /// </summary>
    public class ApplicationReviewResultViewModel
    {
        /// <summary>
        /// Application identifier
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Application status
        /// </summary>
        public ApplicationStatus Status { get; set; }

        /// <summary>
        /// Obligation period
        /// </summary>
        public DateTime ObligationPeriod { get; set; }

        /// <summary>
        /// Supplier name
        /// </summary>
        public string Supplier { get; set; }

        /// <summary>
        /// Constructor taking parameters
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <param name="applicationStatus">Application status</param>
        public ApplicationReviewResultViewModel(int applicationId, ApplicationStatus applicationStatus)
        {
            this.ApplicationId = applicationId;
            this.Status = applicationStatus;
        }
    }
}
