﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View model for Application item
    /// </summary>
    public class ApplicationItemViewModel
    {
        #region Properties

        /// <summary>
        /// The ID of the application
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// The ID of the application Item
        /// </summary>
        public int ApplicationItemId { get; set; }

        /// <summary>
        /// The reference for the application item
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// The fuel type of the application item
        /// </summary>
        public string FuelType { get; set; }

        /// <summary>
        /// The ID of the Fuel Type for the application item
        /// </summary>
        public int FuelTypeId { get; set; }

        /// <summary>
        /// The amount of fuel supplied
        /// </summary>
        public decimal? AmountSupplied { get; set; }

        /// <summary>
        /// The GHG Intensity of the fuel
        /// </summary>
        public decimal? GhgIntensity { get; set; }

        /// <summary>
        /// The energy density of the fuel
        /// </summary>
        public decimal? EnergyDensity { get; set; }

        /// <summary>
        /// The type of unit (e.g. units) to display for the application item
        /// </summary>
        public string FuelTypeUnit { get; set; }

        /// <summary>
        /// The credits generated by the application item
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// The obligation generated by the application item
        /// </summary>
        public decimal Obligation { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <param name="applicationItemId">The ID of the application Item</param>
        /// <param name="reference">The reference for the application item</param>
        /// <param name="fuelType">The fuel type of the application item</param>
        /// <param name="fuelTypeId">The ID of the fuel type of the application item</param>
        /// <param name="amountSupplied">The amount of fuel supplied</param>
        /// <param name="ghgIntensity">The GHG Intensity of the fuel</param>
        /// <param name="energyDensity">The energy density of the fuel</param>
        /// <param name="fuelTypeUnit">The type of unit (e.g. units) to display for the application item</param>
        public ApplicationItemViewModel(int applicationId
            , int applicationItemId
            , string reference
            , string fuelType
            , int fuelTypeId
            , decimal? amountSupplied
            , decimal? ghgIntensity
            , decimal? energyDensity
            , string fuelTypeUnit)
        {
            this.ApplicationId = applicationId;
            this.ApplicationItemId = applicationItemId;
            this.Reference = reference;
            this.FuelType = fuelType;
            this.FuelTypeId = fuelTypeId;
            this.AmountSupplied = amountSupplied;
            this.GhgIntensity = ghgIntensity;
            this.EnergyDensity = energyDensity;
            this.FuelTypeUnit = fuelTypeUnit;
        }

        #endregion Constructors
    }
}
