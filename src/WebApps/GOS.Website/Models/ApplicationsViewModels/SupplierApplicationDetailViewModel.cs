﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using System;
using System.Collections.Generic;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// Supplier application detail view model
    /// </summary>
    public class SupplierApplicationDetailViewModel
    {
        /// <summary>
        /// Application identifier
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Application submission date
        /// </summary>
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// Application status Id
        /// </summary>
        public int ApplicationStatusId { get; set; }

        /// <summary>
        /// Supplier
        /// </summary>
        public Organisation Supplier { get; set; }

        /// <summary>
        /// Obligation period
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }

        /// <summary>
        /// Application items
        /// </summary>
        public IList<ApplicationItemViewModel> ApplicationItems { get; internal set; }

        /// <summary>
        /// Electricity amount supplied
        /// </summary>
        public decimal? ElectricityAmountSupplied { get; internal set; }

        /// <summary>
        /// Electricity density
        /// </summary>
        public decimal? ElectricityDensisty { get; internal set; }

        /// <summary>
        /// Flags what checks if the user has admin role
        /// </summary>
        public bool ShowOrganisationContext { get; set; }

        /// <summary>
        /// Total credits
        /// </summary>
        public decimal TotalCredits { get; set; }

        /// <summary>
        /// Application supporting documents list
        /// </summary>
        public List<UploadedSupportingDocument> ApplicationSupportingDocuments { get; set; }

        /// <summary>
        /// Volume rejection reason
        /// </summary>
        public string VolumesRejectionReason { get; internal set; }

        /// <summary>
        /// Calculation rejection reason
        /// </summary>
        public string CalculationRejectionReason { get; internal set; }

        /// <summary>
        /// Document rejection reason
        /// </summary>
        public string DocumentsRejectionReason { get; internal set; }

        /// <summary>
        /// Latest date for revocation 
        /// </summary>
        public DateTime? LatestDateForRevocation { get; set; }

        /// <summary>
        /// Document rejection reason
        /// </summary>
        public string RevocationReason { get; set; }

        /// <summary>
        /// Uer Fuel Type Id
        /// </summary>
        public int UerFuelTypeId { get; set; }

        /// <summary>
        /// If user is an administrator
        /// </summary>
        public bool IsAdministrator { get; set; }

        /// <summary>
        /// If application status is credits issued
        /// </summary>
        public bool IsStatusCreditsIssued { get; set; }

        /// <summary>
        /// If application status is revoked
        /// </summary>
        public bool IsStatusRevoked { get; set; }

        /// <summary>
        /// If application is past the latest date for revocation
        /// </summary>
        public bool IsPastRevocationDate { get; set; }
    }
}
