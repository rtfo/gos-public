﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgFuels.Common;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Collections.Generic;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View model for the submit application view
    /// </summary>
    public class SubmitApplicationViewModel
    {
        public int ApplicationId { get; set; }
        public Organisation Supplier { get; set; }
        public ObligationPeriod ObligationPeriod { get; set; }
        public IList<ApplicationItemViewModel> ApplicationItems { get; internal set; }
        public decimal? ElectricityAmountSupplied { get; internal set; }
        public decimal? ElectricityDensisty { get; internal set; }
        public bool ShowOrganisationContext { get; set; }
        public decimal TotalCredits { get; set; }
        public bool IsLateApplication { get; set; }
        public string RtfoSupportTelephoneNumber { get; set; }
        public string RtfoSupportEmailAddress { get; set; }
        
        /// <summary>
        /// The ID of the UER fuel type
        /// </summary>
        public int UerFuelTypeId { get { return (int)Enums.FuelTypes.UpstreamEmissionsReduction; } }
    }
}
