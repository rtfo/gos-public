﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using System;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View model for supplier application
    /// </summary>
    public class SupplierApplicationViewModel
    {
        /// <summary>
        /// Application id
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Organisation id
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// Status of application
        /// </summary>
        public ApplicationStatus ApplicationStatus { get; set; }

        /// <summary>
        /// user who submits the application
        /// </summary>
        public string SubmittedBy { get; set; }

        /// <summary>
        /// date of application submission
        /// </summary>
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// user id who submits the application
        /// </summary>
        public string SubmittedById { get; internal set; }
    }
}
