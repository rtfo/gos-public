﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View model for the Application Revocation confirmation page
    /// </summary>
    public class ApplicationRevocationConfirmationViewModel
    {
        /// <summary>
        /// Application identifier
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Supplier
        /// </summary>
        public Organisation Supplier { get; set; }

        /// <summary>
        /// Obligation period
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }
    }
}
