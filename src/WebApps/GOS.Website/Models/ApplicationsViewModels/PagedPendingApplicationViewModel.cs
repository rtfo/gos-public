﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    public class PagedPendingApplicationViewModel
    {
        public UIPagedResult<PendingApplicationViewModel> Result { get; set; }

        public string PageTitle { get; set; }
    }
}
