﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View model for the application submitted (confirmation) view
    /// </summary>
    public class SubmittedApplicationViewModel : SubmitApplicationViewModel
    {
        public SubmittedApplicationViewModel(SubmitApplicationViewModel submitViewModel)
        {
            this.ApplicationId = submitViewModel.ApplicationId;
            this.ApplicationItems = submitViewModel.ApplicationItems;
            this.ElectricityAmountSupplied = submitViewModel.ElectricityAmountSupplied;
            this.ElectricityDensisty = submitViewModel.ElectricityDensisty;
            this.ObligationPeriod = submitViewModel.ObligationPeriod;
            this.ShowOrganisationContext = submitViewModel.ShowOrganisationContext;
            this.Supplier = submitViewModel.Supplier;
        }
    }
}
