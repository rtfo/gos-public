﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// Application Supporting Document Remove View Model
    /// </summary>
    public class ApplicationSupportingDocumentRemoveViewModel
    {
        /// <summary>
        /// Show organisation context information on the view
        /// </summary>
        public bool ShowOrganisationContext { get; set; }

        /// <summary>
        /// Show organisation information
        /// </summary>
        public Organisation Supplier { get; set; }

        /// <summary>
        /// Gets or sets the application identifier.
        /// </summary>
        /// <value>
        /// The application identifier.
        /// </value>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the obligation period.
        /// </summary>
        /// <value>
        /// The obligation period.
        /// </value>
        public ObligationPeriod ObligationPeriod { get; set; }
    }
}
