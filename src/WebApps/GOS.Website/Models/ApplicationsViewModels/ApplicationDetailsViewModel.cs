﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Web.Validation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View model for the Application Details view
    /// </summary>
    public class ApplicationDetailsViewModel
    {
        #region Enums

        /// <summary>
        /// Enum of the types of fuel category available
        /// </summary>
        public enum FuelCategories
        {
            [Display(Name = "Electricity")]
            Electricity = 1,
            [Display(Name = "UERs")]
            UER = 2,
            [Display(Name = "Fossil Gas")]
            FossilGas = 3
        }

        #endregion Enums

        /// <summary>
        /// Show organisation context information on the view
        /// </summary>
        public bool ShowOrganisationContext { get; set; }

        /// <summary>
        /// The name of the supplier undertaking the application
        /// </summary>
        public Organisation Supplier { get; set; }

        /// <summary>
        /// The obligation Period for the application
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }

        /// <summary>
        /// The list of possible fuel types for fossil gas
        /// </summary>
        public IList<FuelType> FossilGasFuelTypes { get; set; }

        /// <summary>
        /// The default Carbon / ghg intensity for Electricity
        /// </summary>
        public decimal DefaultElectricityGhgIntensity { get; set; }

        /// <summary>
        /// The Fuel type ID of Electricity
        /// </summary>
        public int ElectricityFuelTypeId { get; set; }

        /// <summary>
        /// The Fuel type ID of UER
        /// </summary>
        public int UerFuelTypeId { get; set; }

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ApplicationDetailsViewModel()
        {
        }

        /// <summary>
        /// Constructor taking all initial values
        /// </summary>
        public ApplicationDetailsViewModel(ObligationPeriod obligationPeriod
            , IList<FuelType> fossilGasFuelTypes
            , Organisation supplier)
        {
            this.ObligationPeriod = obligationPeriod;
            this.FossilGasFuelTypes = fossilGasFuelTypes;
            this.Supplier = supplier;
        }

        #endregion Constructors

        #region Form Entry Values

        [Required]
        [Display(Name = "Fuel Category")]
        [EnumDataType(typeof(FuelCategories), ErrorMessage = "Fuel Category is required.")]
        public FuelCategories FuelCategory { get; set; }

        [Decimal(2)]
        [Display(Name = "Electricity amount supplied")]
        [Range(0.01, 1000000000000.00, ErrorMessage = "The value is not valid for '{0}'")]
        [RequiredIf("FuelCategory", "Electricity")]
        public decimal? ElectricityAmountSupplied { get; set; }

        [Decimal(2)]
        [Display(Name = "Electricity GHG intensity")]
        [Range(-1000.00, 1000.00, ErrorMessage = "The value is not valid for '{0}'")]
        [RequiredIf("FuelCategory", "Electricity")]
        public decimal? ElectricityGhgIntensity { get; set; }

        [Display(Name = "UERs reference")]
        [StringLength(100, ErrorMessage = "UERs reference is required.", MinimumLength = 1)]
        [RequiredIf("FuelCategory", "UER")]
        public string UerReference { get; set; }

        [Decimal(2)]
        [Display(Name = "UERs amount")]
        [Range(0.01, 1000000000000.00, ErrorMessage = "The value is not valid for '{0}'")]
        [RequiredIf("FuelCategory", "UER")]
        public decimal? UerAmount { get; set; }

        [Display(Name = "Fossil gas type")]
        [Range(1, int.MaxValue)]
        [RequiredIf("FuelCategory", "FossilGas" )]
        public int? FossilGasTypeId { get; set; }

        [Decimal(2)]
        [Display(Name = "Fossil gas volume")]
        [Range(0.01, 1000000000000.00, ErrorMessage = "The value is not valid for '{0}'")]
        [RequiredIf("FuelCategory", "FossilGas")]
        public decimal? FossilGasVolume { get; set; }

        #endregion Form Entry Values
    }
}
