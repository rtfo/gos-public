﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Collections.Generic;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View model for GHG application supporting document upload
    /// </summary>
    public class ApplicationSupportingDocumentsViewModel
    {
        /// <summary>
        /// Show organisation context information on the view
        /// </summary>
        public bool ShowOrganisationContext { get; set; }

        /// <summary>
        /// The name of the supplier undertaking the application
        /// </summary>
        public Organisation Supplier { get; set; }

        /// <summary>
        /// Gets or sets the application identifier.
        /// </summary>
        /// <value>
        /// The application identifier.
        /// </value>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the obligation period.
        /// </summary>
        /// <value>
        /// The obligation period.
        /// </value>
        public ObligationPeriod ObligationPeriod { get; set; }

        /// <summary>
        /// Gets or sets the application supporting documents.
        /// </summary>
        /// <value>
        /// The application supporting documents.
        /// </value>
        public List<UploadedSupportingDocument> ApplicationSupportingDocuments { get; set; }

        /// <summary>
        /// Gets or sets the list of accepted file types.
        /// </summary>
        /// <value>
        /// The accepted file types list.
        /// </value>
        public string AcceptedFileTypes { get; set; }

        /// <summary>
        /// Prevents a default instance of the <see cref="ApplicationSupportingDocumentsViewModel"/> class from being created.
        /// </summary>
        public ApplicationSupportingDocumentsViewModel(Organisation supplier) {
            ApplicationSupportingDocuments = new List<UploadedSupportingDocument>();
            this.Supplier = supplier;
        }

        /// <summary>
        /// Parameterless Contructor
        /// </summary>
        public ApplicationSupportingDocumentsViewModel()
        {
            ApplicationSupportingDocuments = new List<UploadedSupportingDocument>();
        }
    }
}
