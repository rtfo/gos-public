﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using System;
using static DfT.GOS.Website.Models.Review;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View Model for the main Application Approval view
    /// </summary>
    public class ApplicationApprovalViewModel
    {
        #region Properties

        /// <summary>
        /// The application we're viewing / approving
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Gets and sets the Supplier name
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets and sets the application date
        /// </summary>
        public DateTime? ApplicationDate { get; set; }

        /// <summary>
        /// User id who submitted the application
        /// </summary>
        public string SubmittedBy { get; set; }

        /// <summary>
        /// Get and sets the obligation period
        /// </summary>
        public DateTime ObligationPeriod { get; set; }

        /// <summary>
        /// Gets and sets the Volume review status
        /// </summary>
        public ReviewStatus VolumesReviewStatus { get; set; }

        /// <summary>
        /// Gets and sets the Calculation review status
        /// </summary>
        public ReviewStatus CalculationReviewStatus { get; set; }

        /// <summary>
        /// Gets and sets Document review status
        /// </summary>
        public ReviewStatus DocumentsReviewStatus { get; set; }

        /// <summary>
        /// Gets and sets the application status
        /// </summary>
        public ApplicationStatus ApplicationStatus { get; set; }
        
        /// <summary>
        /// First level reviewer's ID
        /// </summary>
        public string RecommendedBy { get; set; }

        /// <summary>
        /// Recommended date
        /// </summary>
        public DateTime? RecommendedDate { get; set; }
        /// <summary>
        /// Gets the review status for the application
        /// </summary>
        public bool IsReviewComplete {
            get
            {
                return (VolumesReviewStatus != null && VolumesReviewStatus.ReviewStatusId > (int)ReviewStatuses.NotReviewed
                    && DocumentsReviewStatus != null && DocumentsReviewStatus.ReviewStatusId > (int)ReviewStatuses.NotReviewed
                   && CalculationReviewStatus != null && CalculationReviewStatus.ReviewStatusId > (int)ReviewStatuses.NotReviewed);
            }
        }

        /// <summary>
        /// Gets the status of line items in the application
        /// </summary>
        public bool AllLineItemsApproved
        {
            get
            {
                return (VolumesReviewStatus != null && VolumesReviewStatus.ReviewStatusId == (int)ReviewStatuses.Approved
                   && DocumentsReviewStatus != null && DocumentsReviewStatus.ReviewStatusId == (int)ReviewStatuses.Approved
                   && CalculationReviewStatus != null && CalculationReviewStatus.ReviewStatusId == (int)ReviewStatuses.Approved);
            }
        }

        /// <summary>
        /// Check if any one line item failed review
        /// </summary>
        public bool HasRejectedItem
        {
            get
            {
                return ((VolumesReviewStatus != null && VolumesReviewStatus.ReviewStatusId == (int)ReviewStatuses.Rejected)
                   || (DocumentsReviewStatus != null && DocumentsReviewStatus.ReviewStatusId == (int)ReviewStatuses.Rejected)
                   || (CalculationReviewStatus != null && CalculationReviewStatus.ReviewStatusId == (int)ReviewStatuses.Rejected));
            }
        }

        /// <summary>
        /// Gets the submit button text based on review status of individual line items
        /// </summary>
        public string SubmitButtonText
        {
            get
            {
                return IsReviewComplete && AllLineItemsApproved ? "Recommended for Approval" : "Reject Application";
            }
        }

        /// <summary>
        /// Indicates whether the current user can approve the application
        /// </summary>
        public bool UserCanApproveApplication { get; set; }

        /// <summary>
        /// Indicates whether the user has approved the application
        /// </summary>
        public Nullable<bool> ApproveApplication { get; set; }

        /// <summary>
        /// The query string used to filter the pending applications list
        /// </summary>
        public string FilterQueryString { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public ApplicationApprovalViewModel()
        {
        }
        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        public ApplicationApprovalViewModel(int applicationId)
        {
            this.ApplicationId = applicationId;
        }

        #endregion Constructors
    }
}
