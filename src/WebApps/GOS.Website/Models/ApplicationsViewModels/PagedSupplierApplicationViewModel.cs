﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// Paged Supplier application view model
    /// </summary>
    public class PagedSupplierApplicationViewModel
    {
        /// <summary>
        /// Gets and sets the paged SupplierApplicationViewModel
        /// </summary>
        public UIPagedResult<SupplierApplicationViewModel> Result { get; set; }

        /// <summary>
        /// Flag indicates is the user is administrator
        /// </summary>
        public bool ShowOrganisationContext { get; set; }

        /// <summary>
        /// Supplier/Organisation
        /// </summary>
        public Organisation Supplier { get; internal set; }

        /// <summary>
        /// Obligation period
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }
    }
}
