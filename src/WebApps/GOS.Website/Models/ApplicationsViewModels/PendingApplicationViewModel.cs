﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    public class PendingApplicationViewModel
    {
        public DateTime ObligationPeriodEndDate { get; set; }

        public DateTime SubmittedDate { get; set; }

        public string Supplier { get; set; }

        public decimal CreditsApplierFor { get; set; }

        public string ReviewStatus { get; set; }
        public string ReviewAction { get; set; }
        public int SupplierId { get; set; }

        public int ApplicationId { get; set; }
    }
}
