﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using System;

namespace DfT.GOS.Website.Models.ApplicationsViewModels
{
    /// <summary>
    /// View model for the Application Revocation page
    /// </summary>
    public class ApplicationRevocationViewModel
    {
        /// <summary>
        /// Application identifier
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Supplier
        /// </summary>
        public Organisation Supplier { get; set; }

        /// <summary>
        /// Obligation period
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }

        /// <summary>
        /// Flags what checks if the user has admin role
        /// </summary>
        public bool ShowOrganisationContext { get; set; }

        /// <summary>
        /// Total credits
        /// </summary>
        public decimal TotalCredits { get; set; }

        /// <summary>
        /// Latest date for revocation 
        /// </summary>
        public DateTime? LatestDateForRevocation { get; set; }

        /// <summary>
        /// Document rejection reason
        /// </summary>
        public string RevocationReason { get; set; }
    }
}
