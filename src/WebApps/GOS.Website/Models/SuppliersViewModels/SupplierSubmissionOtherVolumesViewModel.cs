﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// Supplier Submission Other Volumes View Model
    /// </summary>
    public class SupplierSubmissionOtherRtfoVolumesViewModel
    {
        /// <summary>
        /// Supplier Id
        /// </summary>
        public int SupplierId { get; set; }
        /// <summary>
        /// Supplier Name
        /// </summary>
        /// 
        public string SupplierName { get; set; }

        /// <summary>
        /// Obligation period
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }

        /// <summary>
        /// Summary for the Liquid & Gas balance page
        /// </summary>
        public LiquidGasBalanceSummary LiquidGasBalanceSummary { get; set; }

        /// <summary>
        /// Summary for the Other volumes via RTFO details page
        /// </summary>
        public UIPagedResult<OtherVolumesSubmission> OtherVolumesSubmission { get; set; }

        /// <summary>
        /// Supplier Context
        /// </summary>
        public bool DisplaySupplierContext { get; internal set; }
    }
}
