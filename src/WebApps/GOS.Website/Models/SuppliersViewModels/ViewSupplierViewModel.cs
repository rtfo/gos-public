﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Web.Validation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// View Supplier View Model
    /// </summary>
    public class ViewSupplierViewModel
    {
        #region Properties
        /// <summary>
        /// Get and sets the Supplier/Organisation Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets and sets the Supplier/Organisation Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Get and sets the email address
        /// </summary>
        [Required]
        [GosEmailAddress]
        [Display(Name = "email address")]
        [EmailAddress(ErrorMessage = "Enter an email address in the correct format, like name@example.com")]
        public string Email { get; set; }

        /// <summary>
        /// Gets and sets the Role
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// Gets and sets the users associated with the supplier/organisation
        /// </summary>
        public List<GosApplicationUser> Users { get; set; }

        /// <summary>
        /// Gets and private sets the obligation id
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Flags what checks if the user has admin role
        /// </summary>
        public bool ShowOrganisationContext { get; set; }

        /// <summary>
        /// Supplier
        /// </summary>
        public Organisation Supplier { get; set; }

        /// <summary>
        /// Current User Id
        /// </summary>
        public string CurrentUserId { get; set; }
        #endregion Properties


    }
}
