﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// Represents the tranfer credit view model
    /// </summary>
    public class TransferCreditsViewModel
    {
        /// <summary>
        /// Tranaction id
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// Obligation period id
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// ID of the Supplier/Trader who is transferring the credits
        /// </summary>
        public int FromSupplierId { get; set; }

        /// <summary>
        /// ID of the recipient supplier/trader
        /// </summary>
        public int ToSupplierId { get; set; }

        /// <summary>
        /// Available credit balance
        /// </summary>
        public decimal AvailableCreditBalance { get; set; }

        /// <summary>
        /// Credits to be transferred
        /// </summary>
        [Display(Name = "Credits to be transferred")]
        [Range(0.01, (double)decimal.MaxValue, ErrorMessage = "The value is not valid for '{0}'")]
        [Decimal(2)]
        [Required]
        public decimal NumberOfCreditsToTransfer { get; set; }

        /// <summary>
        /// Falg to display admin contents
        /// </summary>
        public bool DisplaySupplierContext { get; internal set; }

        /// <summary>
        /// Supplier/trader name
        /// </summary>
        public string SupplierName { get; internal set; }

        /// <summary>
        /// Recipient supplier.trader name
        /// </summary>
        public string RecipientName { get; set; }

        /// <summary>
        /// Flag to indicate the balance summary service failure
        /// </summary>
        public bool BalanceServiceFailed { get; internal set; }

        /// <summary>
        /// Flag for if organisation is a Supplier
        /// </summary>
        public bool IsSupplier { get; set; }

        /// <summary>
        /// Flag for if organisation is a Trader
        /// </summary>
        public bool IsTrader { get; set; }

        /// <summary>
        /// Additional reference
        /// </summary>
        [Display(Name = "Additional reference")]
        [StringLength(40, ErrorMessage = "Additional reference cannot be more than 40 characters.")]
        public string AdditionalReference { get; set; }

        /// <summary>
        /// Obligation Period details
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }
    }
}
