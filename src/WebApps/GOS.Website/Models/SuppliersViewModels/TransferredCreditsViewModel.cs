﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// Represents the transferred credit view model
    /// </summary>
    public class TransferredCreditsViewModel : TransferCreditsViewModel
    {
        /// <summary>
        /// Reference number for the transaction
        /// </summary>
        public int ReferenceNumber { get; set; }

        /// <summary>
        /// Credits balance after the transfer
        /// </summary>
        public decimal BalanceCreditsAfterTransfer { get; set; }

        public TransferredCreditsViewModel(TransferCreditsViewModel transferCredits)
        {
            this.TransactionId = transferCredits.TransactionId;
            this.ObligationPeriodId = transferCredits.ObligationPeriodId;
            this.RecipientName = transferCredits.RecipientName;
            this.SupplierName = transferCredits.SupplierName;
            this.ToSupplierId = transferCredits.ToSupplierId;
            this.FromSupplierId = transferCredits.FromSupplierId;
            this.AvailableCreditBalance = transferCredits.AvailableCreditBalance;
            this.NumberOfCreditsToTransfer = transferCredits.NumberOfCreditsToTransfer;
            this.AdditionalReference = transferCredits.AdditionalReference;
        }
    }
}
