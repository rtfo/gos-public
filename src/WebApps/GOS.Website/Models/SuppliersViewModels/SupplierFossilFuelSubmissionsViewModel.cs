﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// Supplier Fossil fule submissions view model
    /// </summary>
    public class SupplierFossilFuelSubmissionsViewModel
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public ObligationPeriod ObligationPeriod { get; set; }
        public LiquidGasBalanceSummary LiquidGasBalanceSummary { get; set; }
        public UIPagedResult<FossilGasSubmission> FossilFuelSubmissions { get; set; }
        public bool DisplaySupplierContext { get; internal set; }
    }
}
