﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// Supplier Balance Summary model
    /// </summary>
    public class SupplierBalanceSummaryExport : SupplierBalanceSummary
    {
        /// <summary>
        /// HasAccount indicates if the organisation has any accounts associated to it
        /// </summary>
        public bool HasAccount { get; set; }

        /// <summary>
        /// GetTitleRow creates the CSV header row
        /// </summary>
        /// <returns></returns>
        public static string GetTitleRow()
        {
            return "Organisation,Status,Type,GHG Credits Available,Obligation,Obligated Status,Has Account\r\n";
        }

        /// <summary>
        /// ToString produces a CSV formatted string of the Supplier balance summary
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Join(","
                , Name
                , Status
                , IsSupplier ? "Supplier": "Trader"
                , GhgCreditsAvailable
                , Obligation
                , ObligatedStatus ? "Obligated" : "In Credit"
                , HasAccount.ToString()
                );
        }
     }
}
