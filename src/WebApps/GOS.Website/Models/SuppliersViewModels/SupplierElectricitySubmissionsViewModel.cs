﻿using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// Supplier Electricity submissions view model
    /// </summary>
    public class SupplierElectricitySubmissionsViewModel
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public ObligationPeriod ObligationPeriod { get; set; }
        public SubmissionBalanceSummary SubmissionBalanceSummary { get; set; }
        public UIPagedResult<ElectricitySubmission> ElectricitySubmissions { get; set; }
        public bool DisplaySupplierContext { get; internal set; }
    }
}
