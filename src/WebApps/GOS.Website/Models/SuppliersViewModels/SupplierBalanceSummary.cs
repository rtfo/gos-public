﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// Organistion Summary details, including obligation and credits
    /// </summary>
    public class SupplierBalanceSummary
    {
        /// <summary>
        /// The ID of the supplier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the supplier
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The status of the supplier
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The current obligation balance for the supplier
        /// </summary>
        public decimal? Obligation { get; set; }

        /// <summary>
        /// The current credits available for the supplier
        /// </summary>
        public decimal? GhgCreditsAvailable { get; set; }

        /// <summary>
        /// Gets and sets IsSupplier
        /// </summary>
        public bool IsSupplier { get; set; }

        /// <summary>
        /// Gets and sets IsTrader 
        /// </summary>
        public bool IsTrader { get; set; }

        /// <summary>
        /// Gets and sets ObligatedStatus
        /// </summary>
        public bool ObligatedStatus { get; set; }
    }
}
