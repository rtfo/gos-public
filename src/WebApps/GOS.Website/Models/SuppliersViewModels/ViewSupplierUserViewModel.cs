﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// View Supplier User View Model
    /// </summary>
    public class ViewSupplierUserViewModel
    {

        public int OrganisationId { get; set; }
        public string OrganisationName { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }

        /// <summary>
        /// The obligation period, if supplied, to maintain on the page
        /// </summary>
        public int? ObligationPeriodId { get; set; }
    }
}
