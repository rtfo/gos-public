﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// View model for the Trading Summary view
    /// </summary>
    public class TradingSummaryViewModel
    {
        #region Properties

        /// <summary>
        /// The ID of the organisation
        /// </summary>
        public int OrganisationId { get; private set; }

        /// <summary>
        /// The name of the organisation
        /// </summary>
        public string OrganisationName { get; private set; }

        /// <summary>
        /// The Obligation Period
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; private set; }

        /// <summary>
        /// The paged result of trading summaries
        /// </summary>
        public UIPagedResult<CreditEvent> PagedTradingResult { get; set; }

        /// <summary>
        /// Whether to show an context on the view for the organisation
        /// </summary>
        public bool DisplayOrganisationContext { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="displayOrganisationContext">Whether to show an context on the view for the organisation</param>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="organisationName">The name of the organisation</param>
        /// <param name="obligationPeriod">The Obligation Period</param>
        /// <param name="pagedTradingResult">The paged result of trading summaries</param>
        public TradingSummaryViewModel(bool displayOrganisationContext
            , int organisationId
            , string organisationName
            , ObligationPeriod obligationPeriod
            , UIPagedResult<CreditEvent> pagedTradingResult)
        {
            this.DisplayOrganisationContext = displayOrganisationContext;
            this.OrganisationId = organisationId;
            this.OrganisationName = organisationName;
            this.ObligationPeriod = obligationPeriod;
            this.PagedTradingResult = pagedTradingResult;
        }

        #endregion Constructors
    }
}
