﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Client.Models;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// View model for the supplier further actions page
    /// </summary>
    public class SupplierFurtherActionsViewModel
    {
        #region Properties

        /// <summary>
        /// Whether to show organisation context information in the view
        /// </summary>
        public bool ShowOrganisationContext { get; set; }

        /// <summary>
        /// The ID of the obligation period
        /// </summary>
        public int ObligationPeriodId { get;set; }

        /// <summary>
        /// The Supplier details
        /// </summary>
        public Organisation Supplier { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="showOrganisationContext">Whether to show organisation context information in the view</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="supplier">The Supplier details</param>
        public SupplierFurtherActionsViewModel(bool showOrganisationContext
            , int obligationPeriodId
            , Organisation supplier)
        {
            this.ShowOrganisationContext = showOrganisationContext;
            this.ObligationPeriodId = obligationPeriodId;
            this.Supplier = supplier;
        }

        #endregion Constructors
    }
}
