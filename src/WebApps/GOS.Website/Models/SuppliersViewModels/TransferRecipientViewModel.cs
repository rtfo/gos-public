﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// Transfer recipient view model
    /// </summary>
    public class TransferRecipientViewModel
    {
        /// <summary>
        /// Search text
        /// </summary>
        public string SearchText { get; set; }

        /// <summary>
        /// Supplier id
        /// </summary>
        public int SupplierId { get; set; }

        /// <summary>
        /// Name of the supplier
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        /// Paged result for supplier/traders
        /// </summary>
        public UIPagedResult<Organisation> Result { get; set; }

        /// <summary>
        /// Flag to show adimin content
        /// </summary>
        public bool DisplaySupplierContext { get; set; }

        /// <summary>
        /// Obligation period id
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Obligation period
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }

        /// <summary>
        /// Flag for if organisation is a Supplier
        /// </summary>
        public bool IsSupplier { get; set; }

        /// <summary>
        /// Flag for if organisation is a Trader
        /// </summary>
        public bool IsTrader { get; set; }
    }
}
