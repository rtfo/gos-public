﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.SuppliersViewModels
{
    /// <summary>
    /// View model for the Upstream Emissions Reductions Summary view
    /// </summary>
    public class SupplierUpstreamEmissionsReductionsSubmissionsViewModel
    {
        #region Properties

        /// <summary>
        /// The ID of the organisation
        /// </summary>
        public int OrganisationId { get; private set; }

        /// <summary>
        /// The name of the organisation
        /// </summary>
        public string OrganisationName { get; private set; }

        /// <summary>
        /// The Obligation Period
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; private set; }

        /// <summary>
        /// The paged result of UER summaries
        /// </summary>
        public UIPagedResult<UpstreamEmissionsReductionSubmission> PagedUpstreamEmissionsReductionsResult { get; set; }

        /// <summary>
        /// Whether to show an context on the view for the organisation
        /// </summary>
        public bool DisplayOrganisationContext { get; private set; }

        /// <summary>
        /// The aggregated total of the UER amounts
        /// </summary>
        public decimal TotalAmount { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="displayOrganisationContext">Whether to show an context on the view for the organisation</param>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="organisationName">The name of the organisation</param>
        /// <param name="obligationPeriod">The Obligation Period</param>
        /// <param name="pagedUpstreamEmissionsReductionsResult">The paged result of UER summaries</param>
        /// <param name="totalAmount">The aggregated total of the UER amounts</param>
        public SupplierUpstreamEmissionsReductionsSubmissionsViewModel(bool displayOrganisationContext
            , int organisationId
            , string organisationName
            , ObligationPeriod obligationPeriod
            , UIPagedResult<UpstreamEmissionsReductionSubmission> pagedUpstreamEmissionsReductionsResult
            , decimal totalAmount)
        {
            this.DisplayOrganisationContext = displayOrganisationContext;
            this.OrganisationId = organisationId;
            this.OrganisationName = organisationName;
            this.ObligationPeriod = obligationPeriod;
            this.PagedUpstreamEmissionsReductionsResult = pagedUpstreamEmissionsReductionsResult;
            this.TotalAmount = totalAmount;
        }

        #endregion Constructors
    }
}
