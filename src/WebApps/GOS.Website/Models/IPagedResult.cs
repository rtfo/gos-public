﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Website.Models
{
    public interface IPagedResult
    {
        int TotalResults { get; }
        int PageNumber { get; }
        int NextPageNumber { get; }
        int PreviousPageNumber { get; }
        int PageSize { get; }
        int PageCount { get; }
        int FirstPage { get; }
        int NumberOfPagesToDisplay { get; }
        int FirstPageToDisplay { get; }
        int LastPageToDisplay { get; }
        int FirstResult { get; }
        int LastResult { get; }
    }
}
