﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using System;
using System.Collections.Generic;
using static DfT.GOS.Website.Models.Review;

namespace DfT.GOS.Website.Models.ReviewApplicationsViewModels
{ 
    /// <summary>
    /// View model for the approve application supporting documents page
    /// </summary>
    public class ApproveApplicationSupportingDocumentsViewModel
    {
        /// <summary>
        /// The ID of the application we're reviewing
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// The name of the supplier undertaking the application
        /// </summary>
        public Organisation Supplier { get; set; }

        /// <summary>
        /// The obligation Period for the application
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }

        /// <summary>
        /// The submitted date of the application
        /// </summary>
        public DateTime SubmittedDate { get; set; }

        /// <summary>
        /// The review details
        /// </summary>
        public ApplicationSectionReview Review { get; set; }

        /// <summary>
        /// Gets or sets the application supporting documents.
        /// </summary>
        /// <value>
        /// The application supporting documents.
        /// </value>
        public List<UploadedSupportingDocument> ApplicationSupportingDocuments { get; set; }

        /// <summary>
        /// The query string used to filter the pending applications list
        /// </summary>
        public string FilterQueryString { get; set; }

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ApproveApplicationSupportingDocumentsViewModel()
        {
            this.Review = new ApplicationSectionReview();
        }

        /// <summary>
        /// Constructor taking all initial values
        /// </summary>
        public ApproveApplicationSupportingDocumentsViewModel(int applicationId
            , bool canEditReviewStatus
            , ObligationPeriod obligationPeriod
            , Organisation supplier
            , ReviewStatuses reviewStatus
            , string rejectionReason
            , DateTime submittedDate
            , List<UploadedSupportingDocument> applicationSupportingDocuments
            , string filterQueryString)
        {
            this.ApplicationId = applicationId;
            this.ObligationPeriod = obligationPeriod;
            this.Supplier = supplier;
            this.SubmittedDate = submittedDate;
            this.Review = new ApplicationSectionReview(applicationId, canEditReviewStatus, reviewStatus, rejectionReason, filterQueryString);
            this.ApplicationSupportingDocuments = applicationSupportingDocuments;
            this.FilterQueryString = filterQueryString;
        }

        #endregion Constructors
    }
}
