﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgFuels.Common;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Website.Models.ApplicationsViewModels;
using System;
using System.Collections.Generic;
using static DfT.GOS.Website.Models.Review;

namespace DfT.GOS.Website.Models.ReviewApplicationsViewModels
{
    /// <summary>
    /// View model for the approve application volumes page
    /// </summary>
    public class ApproveApplicationVolumesViewModel
    {
        /// <summary>
        /// The ID of the application we're reviewing
        /// </summary>
        public int ApplicationId { get; set; }        

        /// <summary>
        /// The name of the supplier undertaking the application
        /// </summary>
        public Organisation Supplier { get; set; }

        /// <summary>
        /// The obligation Period for the application
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }

        /// <summary>
        /// The application Items being reviewed
        /// </summary>
        public IList<ApplicationItemViewModel> ApplicationItems { get; set; }

        /// <summary>
        /// The submitted date of the application
        /// </summary>
        public DateTime SubmittedDate { get; set; }  
        
        /// <summary>
        /// The review details
        /// </summary>
        public ApplicationSectionReview Review { get; set; }

        /// <summary>
        /// The query string used to filter the pending applications list
        /// </summary>
        public string FilterQueryString { get; set; }

        /// <summary>
        /// The ID of the UER fuel type
        /// </summary>
        public int UerFuelTypeId { get { return (int)Enums.FuelTypes.UpstreamEmissionsReduction; } }

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ApproveApplicationVolumesViewModel()
        {
            this.Review = new ApplicationSectionReview();
        }

        /// <summary>
        /// Constructor taking all initial values
        /// </summary>
        public ApproveApplicationVolumesViewModel(int applicationId
            , bool canEditReviewStatus
            , ObligationPeriod obligationPeriod
            , Organisation supplier
            , IList<ApplicationItemViewModel> applicationItems
            , ReviewStatuses reviewStatus
            , string rejectionReason
            , DateTime submittedDate
            , string filterQueryString)
        {
            this.ApplicationId = applicationId;
            this.ObligationPeriod = obligationPeriod;
            this.Supplier = supplier;
            this.ApplicationItems = applicationItems;
            this.SubmittedDate = submittedDate;
            this.Review = new ApplicationSectionReview(applicationId, canEditReviewStatus, reviewStatus, rejectionReason, filterQueryString);
            this.FilterQueryString = filterQueryString;
        }

        #endregion Constructors
    }
}
