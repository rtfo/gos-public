﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;
using static DfT.GOS.Website.Models.Review;

namespace DfT.GOS.Website.Models.ReviewApplicationsViewModels
{
    /// <summary>
    /// View model for a review of an application section 
    /// </summary>
    public class ApplicationSectionReview
    {
        #region Properties

        /// <summary>
        /// The ID of the application the review relates to
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Indicates whether the user can edit the review status
        /// </summary>
        public bool CanEditReviewStatus { get; set; }

        /// <summary>
        /// The review status of the section
        /// </summary>
        [Required]
        [Display(Name = "Review status")]
        [EnumDataType(typeof(ReviewStatuses), ErrorMessage = "Review status is required.")]
        public ReviewStatuses ReviewStatus { get; set; }

        /// <summary>
        /// The rejection reason if the section has been rejected
        /// </summary>
        [Display(Name = "Rejection reason")]
        [RequiredIf("ReviewStatus", "Rejected")]
        public string RejectionReason { get; set; }

        /// <summary>
        /// The query string used to filter the pending applications list
        /// allows returning to the filtered list
        /// </summary>
        public string FilterQueryString { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ApplicationSectionReview()
        {
        }

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public ApplicationSectionReview(int applicationId
            , bool canEditReviewStatus
            , ReviewStatuses reviewStatus
            , string rejectionReason
            , string filterQueryString)
        {
            this.ApplicationId = applicationId;
            this.CanEditReviewStatus = canEditReviewStatus;
            this.ReviewStatus = reviewStatus;
            this.RejectionReason = rejectionReason;
            this.FilterQueryString = filterQueryString;
        }

        #endregion Constructors
    }
}
