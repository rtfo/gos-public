﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Website.Models.SuppliersViewModels;
using System;

namespace DfT.GOS.Website.Models.OrganisationsViewModels
{
    /// <summary>
    /// View Suppliers View Model
    /// </summary>
    public class ViewOrganisationsViewModel
    {
        /// <summary>
        /// The text to search for Organisations
        /// </summary>
        public string SearchText { get; set; }

        /// <summary>
        /// The paged result of supplier balances
        /// </summary>
        public UIPagedResult<SupplierBalanceSummary> Result { get; set; }

        /// <summary>
        /// The ID of the obligation period we're displaying details for
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// The end date of the obligation period we're displaying details for
        /// </summary>
        public DateTime ObligationPeriodEndDate { get; set; }

        /// <summary>
        /// Whether to only display approved organisations
        /// </summary>
        public bool ApprovedOrganisationsOnly { get; set; }
    }
}
