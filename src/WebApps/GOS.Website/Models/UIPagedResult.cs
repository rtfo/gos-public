﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using DfT.GOS.Web.Models;

namespace DfT.GOS.Website.Models
{
    /// <summary>
    /// Holds UI inforation required for a page result of type T.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UIPagedResult<T> : IPagedResult
    {
        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public UIPagedResult()
        {
            //Set default values
            this.Result = new List<T>();
            this.MaximumPagesToDisplay = 5;
        }

        /// <summary>
        /// Creates a UI Paged result from a Paged Result
        /// </summary>
        /// <param name="pagedResult">The paged result</param>
        /// <param name="maximumPagesToDisplay">The maximum number of pages to display on the page control</param>
        public UIPagedResult(PagedResult<T> pagedResult
            , int maximumPagesToDisplay)
        {
            this.Result = pagedResult.Result;
            this.PageNumber = pagedResult.PageNumber;
            this.PageSize = pagedResult.PageSize;
            this.TotalResults = pagedResult.TotalResults;
            this.MaximumPagesToDisplay = maximumPagesToDisplay;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// The page of results
        /// </summary>
        public IList<T> Result { get; set; }

        /// <summary>
        /// The total number of results in the entire results set
        /// </summary>
        public int TotalResults { get; set; }

        /// <summary>
        /// The current page of results
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// The page number of the next page to display
        /// </summary>
        public int NextPageNumber { get { return PageNumber + 1; } }

        /// <summary>
        /// The page number of the previous page to display
        /// </summary>
        public int PreviousPageNumber { get { return PageNumber - 1; } }

        /// <summary>
        /// The number of results to return in a page
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Gets or sets the maximimum number of pages to display
        /// </summary>
        public int MaximumPagesToDisplay { get; set; }

        /// <summary>
        /// Gets the number of pages to display
        /// </summary>
        public int NumberOfPagesToDisplay
        {
            get
            {
                if(this.PageCount > this.MaximumPagesToDisplay)
                {
                    return this.MaximumPagesToDisplay;
                }
                else
                {
                    return this.PageCount;
                }
            }
        }

        /// <summary>
        /// Get the First page number to display
        /// </summary>
        public int FirstPageToDisplay
        {
            get
            {
                int firstPageToDisplay = 1;
                int minimumLeftCount = (int)Math.Floor((decimal)this.MaximumPagesToDisplay / 2);

                if(this.PageNumber > minimumLeftCount)
                {
                    firstPageToDisplay = this.PageNumber - minimumLeftCount;
                }

                if(firstPageToDisplay > this.PageCount - this.NumberOfPagesToDisplay)
                {
                    firstPageToDisplay = this.PageCount - this.NumberOfPagesToDisplay + 1;
                }

                return firstPageToDisplay;
            }
        }

        /// <summary>
        /// Get the last page number to display
        /// </summary>
        public int LastPageToDisplay
        {
            get
            {
                int lastPage = this.FirstPageToDisplay + NumberOfPagesToDisplay -1;
                if(lastPage > this.PageCount)
                {
                    lastPage = this.PageCount;
                }
                return lastPage;
            }
        }

        /// <summary>
        /// Gets the number of pages required to display the entire result set, given the current configuration
        /// </summary>
        public int PageCount { get { return (int)Math.Ceiling(((decimal)this.TotalResults / (decimal)this.PageSize)); } }

        /// <summary>
        /// Returns the first page which is always 1
        /// </summary>
        public int FirstPage { get { return 1; } }

        /// <summary>
        /// The first result index within the current page
        /// </summary>
        public int FirstResult { get { return ((this.PageNumber - 1) * this.PageSize ) + 1;  } }

        /// <summary>
        /// The last result index within the current page
        /// </summary>
        public int LastResult
        {
            get
            {
                var lastResult = this.FirstResult + this.PageSize -1;
                if (lastResult > TotalResults)
                {
                    return TotalResults;
                }
                else
                {
                    return lastResult;
                }
            }
        }

        #endregion Properties
    }
}
