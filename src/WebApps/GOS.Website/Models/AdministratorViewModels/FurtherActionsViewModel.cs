﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

namespace DfT.GOS.Website.Models.AdministratorViewModels
{
    /// <summary>
    /// Further Actions View Model
    /// </summary>
    public class FurtherActionsViewModel
    {
        public int? ObligationPeriodId { get; set; }
    }
}
