﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models.AdministratorViewModels
{
    /// <summary>
    /// Admin user view model
    /// </summary>
    public class AdminUserViewModel
    {
        /// <summary>
        /// User identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Full name of the user
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
    }

}
