﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.AdministratorViewModels
{ 
    /// <summary>
    /// View model for the administrator home page
    /// </summary>
    public class AdministratorSummaryViewModel
    {
        #region Properties

        /// <summary>
        /// The display name of the user
        /// </summary>
        public string UserDisplayName { get; set; }

        /// <summary>
        /// The obligation period context, if supplied
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; set; }

        /// <summary>
        /// The balance summary to be displayed on page
        /// </summary>
        public TotalBalanceSummary TotalBalanceSummary { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="userDisplayName">The display name of the user</param>
        /// <param name="obligationPeriod">The obligation period</param>
        /// <param name="balanceSummary">The balance summary to be displayed on page</param>
        public AdministratorSummaryViewModel(string userDisplayName, ObligationPeriod obligationPeriod, TotalBalanceSummary totalBalanceSummary)
        {
            this.UserDisplayName = userDisplayName;
            this.ObligationPeriod = obligationPeriod;
            this.TotalBalanceSummary = totalBalanceSummary;
        }

        #endregion Constructors
    }
}
