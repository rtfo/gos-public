﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.AdministratorViewModels
{
    /// <summary>
    /// Download All Applications View Model
    /// </summary>
    public class DownloadAllPerformanceDataViewModel
    {
        public ObligationPeriod ObligationPeriod { get; set; }
    }
}
