﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Web.Validation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Website.Models.AdministratorViewModels
{
    /// <summary>
    /// Admin users view model
    /// </summary>
    public class ManageAdminUsersViewModel
    {
        /// <summary>
        /// Email
        /// </summary>
        [Required]
        [GosEmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Logged on user identifier
        /// </summary>
        public string CurrentUserId { get; set; }

        /// <summary>
        /// List of admin users
        /// </summary>
        public IList<GosApplicationUser> Users { get; set; }
    }

}
