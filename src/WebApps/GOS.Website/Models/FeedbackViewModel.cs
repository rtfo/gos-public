// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models
{
    public class FeedbackViewModel
    {
        public string RtfoSupportTelephoneNumber { get; set; }
        public string RtfoSupportEmailAddress { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}