﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models.UnlinkedUserViewModels
{
    /// <summary>
    /// View model for the unlinked user landing page
    /// </summary>
    public class UnlinkedUserSummaryViewModel
    {
        /// <summary>
        /// Indicates whether a user account was created as part of the service
        /// </summary>
        public bool AccountCreated { get; set; }

        /// <summary>
        /// The RTFO Support Telephone Number
        /// </summary>
        public string RtfoSupportTelephoneNumber { get; set; }

        /// <summary>
        /// The RTFO Support Email Address
        /// </summary>
        public string RtfoSupportEmailAddress { get; set; }
    }
}
