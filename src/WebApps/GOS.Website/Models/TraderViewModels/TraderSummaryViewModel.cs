﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.Website.Models.TraderViewModels
{
    public class TraderSummaryViewModel
    {
        
        #region Properties

        /// <summary>
        /// The display name of the user
        /// </summary>
        public string UserDisplayName { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public ObligationPeriod ObligationPeriod { get; set; }
        public bool DisplaySupplierContext { get; set; }
        public BalanceSummary BalanceSummary { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="userDisplayName">The display name of the user</param>
        public TraderSummaryViewModel(string userDisplayName)
        {
            this.UserDisplayName = userDisplayName;
        }

        #endregion Constructors
    }
}
