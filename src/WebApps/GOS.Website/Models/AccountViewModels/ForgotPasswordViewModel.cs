﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Website.Models.AccountViewModels
{
    public class ForgottenPasswordViewModel
    {
        #region Form Properties

        /// <summary>
        /// The user's email address to send the forgotten password notification to
        /// </summary>
        [Required]
        [GosEmailAddress]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        #endregion Form Properties

        #region Display Properties

        /// <summary>
        /// The RTFO Support Telephone Number
        /// </summary>
        public string RtfoSupportTelephoneNumber { get; set; }

        /// <summary>
        /// The RTFO Support Email Address
        /// </summary>
        public string RtfoSupportEmailAddress { get; set; }

        #endregion Display Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public ForgottenPasswordViewModel()
        {

        }

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="RtfoSupportTelephoneNumber">The telephone number for RTFO support</param>
        /// <param name="RtfoSupportEmailAddress">The email address for RTFO support</param>
        public ForgottenPasswordViewModel(string rtfoSupportTelephoneNumber
            , string rtfoSupportEmailAddress)
        {
            this.RtfoSupportTelephoneNumber = rtfoSupportTelephoneNumber;
            this.RtfoSupportEmailAddress = rtfoSupportEmailAddress;
        }

        #endregion Constructors
    }
}
