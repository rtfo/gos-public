﻿namespace DfT.GOS.Website.Models.AccountViewModels
{
    /// <summary>
    /// View model for the "How to register" page
    /// </summary>
    public class HowToRegisterViewModel
    {
        #region Properties

        /// <summary>
        /// The Email addresss for RTFO contact
        /// </summary>
        public string RtfoSupportEmailAddress { get; private set; }

        /// <summary>
        /// The Telephone Number for RTFO contact
        /// </summary>
        public string RtfoSupportTelephoneNumber { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="rtfoSupportEmailAddress">The Email addresss for RTFO contact</param>
        /// <param name="rtfoSupportTelephoneNumber">The Telephone Number for RTFO contact</param>
        public HowToRegisterViewModel(string rtfoSupportEmailAddress
            , string rtfoSupportTelephoneNumber)
        {
            this.RtfoSupportEmailAddress = rtfoSupportEmailAddress;
            this.RtfoSupportTelephoneNumber = rtfoSupportTelephoneNumber;
        }

        #endregion Construtors
    }
}
