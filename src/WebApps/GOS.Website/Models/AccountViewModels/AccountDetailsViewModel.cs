﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

namespace DfT.GOS.Website.Models.AccountViewModels
{
    /// <summary>
    /// View model for the account details page
    /// </summary>
    public class AccountDetailsViewModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string RtfoSupportTelephoneNumber { get; set; }
        public string RtfoSupportEmailAddress { get; set; }

        /// <summary>
        /// The ID of the Obligation Period 
        /// </summary>
        public int? ObligationPeriodId { get; set; }
    }
}
