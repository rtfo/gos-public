﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Website.Models.AccountViewModels
{
    public class InvitationViewModel
    {
        [Required]
        [GosEmailAddress]
        [Display(Name = "Email address")]
        public string Email { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
