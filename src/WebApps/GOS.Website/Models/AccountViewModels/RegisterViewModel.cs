﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Website.Models.AccountViewModels
{
    /// <summary>
    /// Model for fields from the registration view
    /// </summary>
    public class RegisterViewModel
    {
        /// <summary>
        /// Full name for user to be created
        /// </summary>
        [Required]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        /// <summary>
        /// Email address for user to be created
        /// </summary>
        [Required]
        [GosEmailAddress]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        /// <summary>
        /// Password for user to be created
        /// </summary>
        [Required]
        [GosPassword]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Confirmation password for user to be created
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string EmailDisabled{ get; set; }

        /// <summary>
        /// Confirmation password for user to be created
        /// </summary>
        public int? OrganisationId { get; set; }

        /// <summary>
        /// Organisation name to be attached to user
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Role for user to be created
        /// </summary>
        public string Role { get; set; }
    }
}
