﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;
using DfT.GOS.Web.Validation;

namespace DfT.GOS.Website.Models.AccountViewModels
{
    public class ResetPasswordViewModel
    {
        [Required]
        [GosEmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [GosPassword]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}
