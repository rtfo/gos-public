﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Website.Models
{
    public class GosOrganisationUserAssociation
    {
        public int OrganisationId { get; set; }
        public string OrganisationName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
