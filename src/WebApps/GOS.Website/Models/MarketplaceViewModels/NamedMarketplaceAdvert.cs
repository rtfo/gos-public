﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.Common.Models;

namespace DfT.GOS.Website.Models.MarketplaceViewModels
{
    /// <summary>
    /// Contains the flags for whether an organisation wants to advertise to buy and/or sell
    /// </summary>
    public class NamedMarketplaceAdvert : MarketplaceAdvert
    {
        #region Properties
        /// <summary>
        /// Name of the organisation advertising
        /// </summary>
        public string OrganisationName { get; set; }

        #endregion Properties

        #region Constructors
        /// <summary>
        /// Constructor taking advert and contact settings
        /// </summary>
        /// <param name="organisationName">Name of the organisation advertising</param>
        /// <param name="marketplaceAdvert">Advert containing all details for the marketplace advert other than name</param>
        public NamedMarketplaceAdvert(string organisationName, MarketplaceAdvert marketplaceAdvert) 
            : base(marketplaceAdvert.OrganisationId, marketplaceAdvert.AdvertiseToBuy, 
                  marketplaceAdvert.AdvertiseToSell, marketplaceAdvert.FullName,
                  marketplaceAdvert.EmailAddress, marketplaceAdvert.PhoneNumber)
        {
            this.OrganisationName = organisationName;
        }
        #endregion Constructors
    }
}
