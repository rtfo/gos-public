﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.Common.Models;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.Web.Validation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

namespace DfT.GOS.Website.Models.MarketplaceViewModels
{
    public class MarketplaceAdvertViewModel
    {

        private const  int MaxInputLength = 50;
        private const int MaxPhoneNumberLength = 16;
        /// <summary>
        /// ID of the organisation
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// ID of the obligation period
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Name of the organisation
        /// </summary>
        public string OrganisationName { get; set; }

        /// <summary>
        /// End date of the currently selected Obligation Period
        /// </summary>
        public DateTime? ObligationPeriodEndDate { get; set; }

        /// <summary>
        /// Whether the organisation is interested in buying credits
        /// </summary>
        public bool BuyCredits { get; set; }

        /// <summary>
        /// Whether the organisation is interested in selling credits
        /// </summary>
        public bool SellCredits { get; set; }

        /// <summary>
        /// Number of credits the organisation has available to transfer
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// Indicates if the user is an administrator
        /// </summary>
        public bool IsAdministrator { get; set; }

        /// <summary>
        /// Indicates if the user is a Trader
        /// </summary>
        public bool IsTrader { get; set; }

        /// <summary>
        /// Indicates if the user is a Supplier
        /// </summary>
        public bool IsSupplier { get; set; }

        /// <summary>
        /// Optional name of contact
        /// </summary>
        [MaxLength(MaxInputLength)]
        public string FullName { get; set; }

        /// <summary>
        /// Optional email contact
        /// </summary>
        [GosEmailAddress]
        [MaxLength(MaxInputLength)]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Optional phone number contact
        /// </summary>
        [Phone(ErrorMessage = "Enter a telephone number, like 01632 960 001, 07700 900 982 or +44 0808 157 0192")]
        [MaxLength(MaxPhoneNumberLength)]
        public string PhoneNumber { get; set; }

        #region Constructors

        /// <summary>
        /// Constructor taking all parameters
        /// </summary>
        /// <param name="organisation">Organisation that is being viewed</param>
        /// <param name="obligationPeriod">Obligation period that is being viewed</param>
        /// <param name="user">User doing the viewing</param>
        /// <param name="advert">Marketplace advert containing users preferences</param>
        public MarketplaceAdvertViewModel(Organisation organisation, ObligationPeriod obligationPeriod, ClaimsPrincipal user, MarketplaceAdvert advert)
        {
            this.ObligationPeriodEndDate = obligationPeriod.EndDate;
            this.BuyCredits = advert.AdvertiseToBuy;
            this.SellCredits = advert.AdvertiseToSell;
            this.FullName = advert.FullName;
            this.EmailAddress = advert.EmailAddress;
            this.PhoneNumber = advert.PhoneNumber;
            this.OrganisationId = organisation.Id;
            this.ObligationPeriodId = obligationPeriod.Id;
            this.OrganisationName = organisation.Name;
            this.IsAdministrator = user.IsAdministrator();
            this.IsSupplier = organisation.IsSupplier;
            this.IsTrader = organisation.IsTrader;
        }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public MarketplaceAdvertViewModel() { }
        #endregion Constructors
    }
}
