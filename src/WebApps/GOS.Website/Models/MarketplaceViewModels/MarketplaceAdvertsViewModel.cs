﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using System;
using System.Collections.Generic;

namespace DfT.GOS.Website.Models.MarketplaceViewModels
{

    /// <summary>
    /// View model for Marketplace adverts view
    /// </summary>
    public class MarketplaceAdvertsViewModel
    {
        #region Properties

        /// <summary>
        /// The obligation period the adverts relate to
        /// </summary>
        public ObligationPeriod ObligationPeriod { get; private set; }

        /// <summary>
        /// A list of organisations looking to buy
        /// </summary>
        public List<NamedMarketplaceAdvert> OrganisationsLookingToBuy { get; private set; }

        /// <summary>
        /// A list of organisations looking to sell
        /// </summary>
        public List<NamedMarketplaceAdvert> OrganisationsLookingToSell { get; private set; }

        /// <summary>
        /// The organisation ID context, if applicable.
        /// </summary>
        public int? OrganisationId { get; private set; }

        /// <summary>
        /// The organisation name, if applicable.
        /// </summary>
        public string OrganisationName { get; private set; }

        /// <summary>
        /// Flag to display admin contents
        /// </summary>
        public bool DisplaySupplierContext { get; private set; }

        /// <summary>
        /// Indidates whether the marketplace functionality is available - can be used as a service bulkhead
        /// </summary>
        public bool MarketplaceAvailable { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="obligationPeriod">The obligation period the adverts relate to</param>
        /// <param name="organisationsLookingToBuy">A list of organisations looking to buy</param>
        /// <param name="organisationsLookingToSell">A list of organisations looking to sell</param>
        /// <param name="organisationId">The organisation ID context, if applicable</param>
        public MarketplaceAdvertsViewModel(ObligationPeriod obligationPeriod
            , List<NamedMarketplaceAdvert> organisationsLookingToBuy
            , List<NamedMarketplaceAdvert> organisationsLookingToSell
            , int? organisationId
            , string organisationName
            , bool displaySupplierContext)
        {
            this.ObligationPeriod = obligationPeriod;
            this.OrganisationsLookingToBuy = organisationsLookingToBuy;
            this.OrganisationsLookingToSell = organisationsLookingToSell;
            this.OrganisationId = organisationId;
            this.OrganisationName = organisationName;
            this.DisplaySupplierContext = displaySupplierContext;
            this.MarketplaceAvailable = true;
        }

        /// <summary>
        /// Constructor for use when the marketplace is unavailable, perhaps because of an issue with the underlying service
        /// </summary>
        /// <param name="marketplaceAvailable">Indicates whether the marketplace functionality is available - can be used as a service bulkhead. 
        /// Note this constructor only accepts a false value</param>
        public MarketplaceAdvertsViewModel(ObligationPeriod obligationPeriod
            , int? organisationId
            , string organisationName
            , bool displaySupplierContext
            , bool marketplaceAvailable)
        {
            if(marketplaceAvailable)
            {
                throw new ArgumentException("Only use this constructor with marketplaceAvailable = false");
            }

            this.ObligationPeriod = obligationPeriod;
            this.OrganisationId = organisationId;
            this.OrganisationName = organisationName;
            this.DisplaySupplierContext = displaySupplierContext;
            this.MarketplaceAvailable = false;
        }

        #endregion Constructors
    }
}
