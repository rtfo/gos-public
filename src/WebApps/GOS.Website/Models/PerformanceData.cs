﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgLedger.Common.Models;
using DfT.GOS.Identity.Common.Models;
using GOS.RtfoApplications.Common.Models;
using System;

namespace DfT.GOS.Website.Models
{
    /// <summary>
    /// Performance Data
    /// </summary>
    public class PerformanceData
    {
        #region Properties

        /// <summary>
        /// Gets or sets the date for the submissions.
        /// </summary>
        /// <value>
        /// The date for the submissions.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the number of GOS applications with credits issued for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of GOS applications with credits issued for the supplied date range.
        /// </value>
        public int NoOfGosApplicationsWithCreditsIssued { get; set; }

        /// <summary>
        /// Gets or sets the number of incomplete GOS applications for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of incomplete GOS applications for the supplied date range.
        /// </value>
        public int NoOfIncompleteGosApplications { get; set; }

        /// <summary>
        /// Gets or sets the number of GOS applications rejected for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of GOS applications rejected for the supplied date range.
        /// </value>
        public int NoOfRejectedGosApplications { get; set; }

        /// <summary>
        /// Gets or sets the number of GOS applications revoked for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of GOS applications revoked for the supplied date range.
        /// </value>
        public int NoOfRevokedGosApplications { get; set; }

        /// <summary>
        /// Gets or sets the number of GOS applications submitted for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of GOS applications submitted for the supplied date range.
        /// </value>
        public int NoOfSubmittedGosApplications { get; set; }

        /// <summary>
        /// Gets or sets the number of GOS applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently).
        /// </summary>
        /// <value>
        /// No. of GOS applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently).
        /// </value>
        public int NoOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue { get; set; }

        /// <summary>
        /// Gets or sets the total number of GOS applications for the supplied date range.
        /// </summary>
        /// <value>
        /// Total number of GOS applications for the supplied date range.
        /// </value>
        public int TotalNoOfGosApplications { get; set; }

        /// <summary>
        /// Number of invitations sent in the given month that aren't linked to an account for the supplied date range.
        /// </summary>
        public int InvitationsWithoutAccount { get; private set; }

        /// <summary>
        /// Number of invitations sent for the supplied date range.
        /// </summary>
        public int Invitations { get; private set; }

        /// <summary>
        /// Total number of new users for the supplied date range.
        /// </summary>
        public int TotalUsers { get; private set; }

        /// <summary>
        /// Gets or sets the number of transfer transactions for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of transfer transactions for the supplied date range.
        /// </value>
        public int NoOfTransferTransactions { get; set; }

        /// <summary>
        /// Number of admin consignments reported for the supplied date range
        /// </summary>
        public int AdminConsignmentCount { get; private set; }

        /// <summary>
        /// Gets the total combined number of GOS and ROS applications
        /// </summary>
        public int TotalNoOfGosAndRosApplications 
        {  
            get 
            {
                return this.TotalNoOfGosApplications + this.AdminConsignmentCount;

            } 
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="date">Date</param>
        /// <param name="noOfGosApplicationsWithCreditsIssued">No. Of Applications With Credits Issued</param>
        /// <param name="noOfIncompleteGosApplications">No. Of Incomplete Applications</param>
        /// <param name="noOfRejectedGosApplications">No. Of Rejected Applications</param>
        /// <param name="noOfRevokedGosApplications">No. Of Revoked Applicationsd</param>
        /// <param name="noOfSubmittedGosApplications">No. Of Submitted Applications</param>
        /// <param name="noOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue">No. Of Applications Over Threshold Between Submission And Credit Issue</param>
        /// <param name="totalNoOfGosApplications">Total No. Of Applications</param>
        /// <param name="invitationsWithoutAccount">Number of invitations sent that don't have an account</param>
        /// <param name="invitations">Number of invitations sent</param>
        /// <param name="totalUsers">Number of user accounts</param>
        /// <param name="noOfTransferTransactions">Number of transfer transactions</param>
        /// <param name="adminConsignmentCount">Number of ROS Admin Consignemnts</param>
        public PerformanceData(DateTime date
            , int noOfGosApplicationsWithCreditsIssued
            , int noOfIncompleteGosApplications
            , int noOfRejectedGosApplications
            , int noOfRevokedGosApplications
            , int noOfSubmittedGosApplications
            , int noOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue
            , int totalNoOfGosApplications
            , int invitationsWithoutAccount
            , int invitations
            , int totalUsers
            , int noOfTransferTransactions
            , int adminConsignmentCount)
        {
            this.Date = date;
            this.NoOfGosApplicationsWithCreditsIssued = noOfGosApplicationsWithCreditsIssued;
            this.NoOfIncompleteGosApplications = noOfIncompleteGosApplications;
            this.NoOfRejectedGosApplications = noOfRejectedGosApplications;
            this.NoOfRevokedGosApplications = noOfRevokedGosApplications;
            this.NoOfSubmittedGosApplications = noOfSubmittedGosApplications;
            this.NoOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue = noOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue;
            this.TotalNoOfGosApplications = totalNoOfGosApplications;
            this.InvitationsWithoutAccount = invitationsWithoutAccount;
            this.Invitations = invitations;
            this.TotalUsers = totalUsers;
            this.NoOfTransferTransactions = noOfTransferTransactions;
            this.AdminConsignmentCount = adminConsignmentCount;
        }

        public PerformanceData(ApplicationPerformanceData applicationPerformanceData)
        {
            this.Date = applicationPerformanceData.Date;
            this.NoOfGosApplicationsWithCreditsIssued = applicationPerformanceData.NoOfApplicationsWithCreditsIssued;
            this.NoOfIncompleteGosApplications = applicationPerformanceData.NoOfIncompleteApplications;
            this.NoOfRejectedGosApplications = applicationPerformanceData.NoOfRejectedApplications;
            this.NoOfRevokedGosApplications = applicationPerformanceData.NoOfRevokedApplications;
            this.NoOfSubmittedGosApplications = applicationPerformanceData.NoOfSubmittedApplications;
            this.NoOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue = applicationPerformanceData.NoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue;
            this.TotalNoOfGosApplications = applicationPerformanceData.TotalNoOfApplications;
        }

        public PerformanceData(RegistrationsPerformanceData registrationsPerformanceData)
        {
            this.Date = registrationsPerformanceData.Date;
            this.InvitationsWithoutAccount = registrationsPerformanceData.InvitationsWithoutAccount;
            this.Invitations = registrationsPerformanceData.Invitations;
            this.TotalUsers = registrationsPerformanceData.TotalUsers;
        }


        public PerformanceData(LedgerPerformanceData ledgerPerformanceData)
        {
            this.Date = ledgerPerformanceData.Date;
            this.NoOfTransferTransactions = ledgerPerformanceData.NoOfTransferTransactions;
        }

        /// <summary>
        /// Constructor taking RTFO based performance data
        /// </summary>
        /// <param name="rtfoPerformanceData"></param>
        public PerformanceData(RtfoPerformanceData rtfoPerformanceData)
        {
            this.Date = rtfoPerformanceData.Date;
            this.AdminConsignmentCount = rtfoPerformanceData.AdminConsignmentCount;
        }

        /// <summary>
        /// Column headers for the application performance data export
        /// </summary>
        public static string GetHeaderText()
        {
            return string.Join(","
                , "Date"
                , "No. Of GOS Applications With Credits Issued"
                , "No. Of Incomplete GOS Applications"
                , "No. Of Rejected GOS Applications"
                , "No. Of Revoked GOS Applications"
                , "No. Of Submitted GOS Applications"
                , "No. Of GOS Applications Over Threshold Between Submission And Credit Issue"
                , "Total No. Of GOS Applications"
                , "No. Of Invitations Without An Account"
                , "No. Of Sent Invitations"
                , "Total No. Of Users"
                , "No. Of Transfer Transactions"
                , "Total No. Of ROS Applications (Admin Consignments)"
                , "Total GOS and ROS Applications");
        }

        /// <summary>
        /// Converting the values within the application performance data into a complete string to be exported into a CSV
        /// </summary>
        public override string ToString()
        {
            return string.Join(","
                , Date.ToString("MM/yyyy")
                , NoOfGosApplicationsWithCreditsIssued.ToString()
                , NoOfIncompleteGosApplications.ToString()
                , NoOfRejectedGosApplications.ToString()
                , NoOfRevokedGosApplications.ToString()
                , NoOfSubmittedGosApplications.ToString()
                , NoOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue.ToString()
                , TotalNoOfGosApplications.ToString()
                , InvitationsWithoutAccount.ToString()
                , Invitations.ToString()
                , TotalUsers.ToString()
                , NoOfTransferTransactions.ToString()
                , AdminConsignmentCount.ToString()
                , TotalNoOfGosAndRosApplications.ToString());
        }

        #endregion Constructors
    }
}
