﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using DfT.GOS.Website.Models;

namespace DfT.GOS.Website.Services
{
    /// <summary>
    /// Interface for Performance Data Helper Service
    /// </summary>
    public interface IPerformanceDataHelperService
    {
        /// <summary>
        /// Helper method of concatinating the various performance data objects
        /// </summary>
        /// <param name="appPerformanceData">Application performance data</param>
        /// <param name="regPerformanceData">Registrations performance data</param>
        /// <param name="ledPerformanceData">Ledger performance data</param>
        /// <param name="rtfoPerformanceData">RTFO application performance data</param>
        /// <returns>Returns concatenated performance data/returns>
        /// <remarks>
        /// Helper to get concatenated performance data
        /// </remarks>
        IEnumerable<PerformanceData> GetConcatenatedPerformanceData(List<PerformanceData> appPerformanceData
            , List<PerformanceData> regPerformanceData
            , List<PerformanceData> ledPerformanceData
            , List<PerformanceData> rtfoPerformanceData);
    }
}