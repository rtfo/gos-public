﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Models.SuppliersViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.Website.Services
{
    /// <summary>
    /// OrganisationSummaryService provides methods for retrieving a summary of all Organisations data
    /// </summary>
    public class OrganisationsSummaryService : IOrganisationsSummaryService
    {
        #region fields
        private readonly IOrganisationsService OrganisationsService;
        private readonly ITokenService TokenService;
        private readonly IBalanceViewService BalanceViewService;
        private readonly IUsersService UserService;
        #endregion fields

        #region constructor
        /// <summary>
        /// OrganisationsSummaryService constructor to provide all dependencies
        /// </summary>
        /// <param name="organisationsService">Service providing organisation data</param>
        /// <param name="tokenService">Service providing the JWT authentication token</param>
        /// <param name="balanceViewService">Service providing the balance for an organisation</param>
        /// <param name="userService">Service providing user information</param>
        public OrganisationsSummaryService(
            IOrganisationsService organisationsService
            , ITokenService tokenService
            , IBalanceViewService balanceViewService
            , IUsersService userService)
        {
            this.OrganisationsService = organisationsService;
            this.TokenService = tokenService;
            this.BalanceViewService = balanceViewService;
            this.UserService = userService;
        }
        #endregion constructor

        #region implementation methods
        /// <summary>
        /// GetSummaryExport produces a summary of all organisations as per the Administrators'
        /// Organisations page for download in CSV format, for a given obligation period
        /// </summary>
        /// <param name="obligationPeriod">Id of the obligation period to get the summary for</param>
        /// <returns>byte array of the summary to be download as a file</returns>
        public async Task<byte[]> GetSummaryExport(int obligationPeriodId)
        {
            var jwtToken = await TokenService.GetJwtToken();
            var organisations = await OrganisationsService.GetSuppliersAndTraders(jwtToken);
            if (!organisations.HasResult)
            {
                throw new OrganisationsSummaryException("unable to get organisations for obligation period: " + obligationPeriodId);
            }

            var summaryTasks = new List<Task<SupplierBalanceSummaryExport>>();

            foreach (var organisation in organisations.Result.OrderBy(o => o.Name))
            {
                var summaryTask = GetSummaryForOrganisation(organisation, obligationPeriodId, jwtToken);
                summaryTasks.Add(summaryTask);
            }

            await Task.WhenAll(summaryTasks);

            var summaryCSV = new StringBuilder();
            summaryCSV.Append(SupplierBalanceSummaryExport.GetTitleRow());

            summaryTasks.ForEach(async summary =>
            {
                var completedSummary = await summary;
                summaryCSV.Append(completedSummary);
                summaryCSV.Append("\r\n");
            });

            return Encoding.Unicode.GetBytes(summaryCSV.ToString());
        }
        #endregion implementation methods

        #region private methods
        private async Task<SupplierBalanceSummaryExport> GetSummaryForOrganisation(Organisation organisation, int obligationPeriodId, string jwtToken)
        {
            var balanceTask = BalanceViewService.GetSummary(organisation.Id, obligationPeriodId, jwtToken);
            var userTask = UserService.GetOrganisationUsers(organisation.Id, jwtToken);

            await Task.WhenAll(balanceTask, userTask);
            var balance = await balanceTask;
            var users = await userTask;

            if (!balance.HasResult || users == null)
            {
                throw new OrganisationsSummaryException("unable to get balance view or users for organisation: " + organisation.Name);
            }
            var balanceSummary = balance.Result;
            var obligated = balanceSummary.BalanceCredits < balanceSummary.BalanceObligation;

            return new SupplierBalanceSummaryExport
            {
                Id = organisation.Id,
                Name = organisation.Name,
                Status = organisation.Status,
                IsSupplier = organisation.IsSupplier,
                Obligation = balanceSummary.BalanceObligation,
                GhgCreditsAvailable = balanceSummary.BalanceCredits,
                ObligatedStatus = obligated,
                HasAccount = users.Count > 0
            };
        }
        #endregion private methods
    }
}
