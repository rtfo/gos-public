﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Website.Models;
using System.Collections.Generic;
using System.Linq;

namespace DfT.GOS.Website.Services
{
    /// <summary>
    /// Performance Data Helper Service
    /// </summary>
    public class PerformanceDataHelperService : IPerformanceDataHelperService
    {
        /// <summary>
        /// Helper method of concatenating the various performance data objects
        /// </summary>
        /// <param name="appPerformanceData">Application performance data</param>
        /// <param name="regPerformanceData">Registrations performance data</param>
        /// <param name="ledPerformanceData">Ledger performance data</param>
        /// <param name="rtfoPerformanceData">RTFO application performance data</param>
        /// <returns>Returns concatenated performance data/returns>
        /// <remarks>
        /// Helper to get concatenated performance data
        /// </remarks>
        public IEnumerable<PerformanceData> GetConcatenatedPerformanceData(List<PerformanceData> appPerformanceData
            , List<PerformanceData> regPerformanceData
            , List<PerformanceData> ledPerformanceData
            , List<PerformanceData> rtfoPerformanceData)
        {
            var performanceData = appPerformanceData
                .Concat(regPerformanceData)
                .Concat(ledPerformanceData)
                .Concat(rtfoPerformanceData)
            .ToLookup(p => p.Date)
            .Select(g => g.Aggregate((x, y) => new PerformanceData(x.Date
            , x.NoOfGosApplicationsWithCreditsIssued + y.NoOfGosApplicationsWithCreditsIssued
            , x.NoOfIncompleteGosApplications + y.NoOfIncompleteGosApplications
            , x.NoOfRejectedGosApplications + y.NoOfRejectedGosApplications
            , x.NoOfRevokedGosApplications + y.NoOfRevokedGosApplications
            , x.NoOfSubmittedGosApplications + y.NoOfSubmittedGosApplications
            , x.NoOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue + y.NoOfGosApplicationsOverThresholdBetweenSubmissionAndCreditIssue
            , x.TotalNoOfGosApplications + y.TotalNoOfGosApplications
            , x.InvitationsWithoutAccount + y.InvitationsWithoutAccount
            , x.Invitations + y.Invitations
            , x.TotalUsers + y.TotalUsers
            , x.NoOfTransferTransactions + y.NoOfTransferTransactions
            , x.AdminConsignmentCount + y.AdminConsignmentCount)));

            performanceData = performanceData.OrderBy(o => o.Date).ToList();

            return performanceData;
        }
    }
}
