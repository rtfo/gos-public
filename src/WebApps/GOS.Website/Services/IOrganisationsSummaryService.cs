﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Threading.Tasks;

namespace DfT.GOS.Website.Services
{
    /// <summary>
    /// Interface for retrieving a summary of all Organisations data
    /// </summary>
    public interface IOrganisationsSummaryService
    {
        /// <summary>
        /// GetSummaryExport produces a summary of all organisations as per the Administrators
        /// Organisations page for download in CSV format, for a given obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of the obligation period to get the summary for</param>
        /// <returns>byte array of the summary for download as a file</returns>
        Task<byte[]> GetSummaryExport(int obligationPeriodId);
    }
}
