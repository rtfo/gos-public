﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Runtime.Serialization;

namespace DfT.GOS.Website.Services
{
    /// <summary>
    /// OrganisationsSummaryException thrown when it has not been possible to get
    /// an organisations summary
    /// </summary>
    [Serializable]
    public class OrganisationsSummaryException : Exception
    {
        /// <summary>
        /// Default Constructor for the OrganisationsSummaryException
        /// </summary>
        public OrganisationsSummaryException()
        {
        }

        /// <summary>
        /// Creates an OrganisationsSummaryException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public OrganisationsSummaryException(string message) : base(message)
        {
        }

        /// <summary>
        /// Creates an OrganisationsSummaryException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="inner">The inner exception</param>
        public OrganisationsSummaryException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// Creates an OrganisationsSummaryException with serialized data
        /// </summary>
        /// <param name="info">The object that holds the serialized object data</param>
        /// <param name="context">The contextual information about the source or destination</param>
        protected OrganisationsSummaryException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
