﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Security.Claims;
using System.Threading.Tasks;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Website.Models.SuppliersViewModels;
using DfT.GOS.Website.Models.TraderViewModels;

namespace DfT.GOS.Website.Orchestrators
{
    /// <summary>
    /// Interface for orchestrator retrieving GHG Balance information for Suppliers
    /// </summary>
    public interface ISupplierReportingSummaryOrchestrator
    {
        /// <summary>
        /// Gets reporting summary details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier reporting summary for a particular obligation period</returns>
        Task<SupplierReportingSummaryViewModel> GetSupplierReportingSummary(int supplierId, ObligationPeriod obligationPeriod, ClaimsPrincipal user);

        /// <summary>
        /// Get the Submission liquid and gas summary view model
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier submission Liquid and Gas summary view model for a particular obligation period</returns>
        Task<SupplierSubmissionLiquidGasSummaryViewModel> GetSupplierSubmissionsLiquidGasSummary(int organisationId, ObligationPeriod obligationPeriod, ClaimsPrincipal user);

        /// <summary>
        /// Get the Submission other volumes via RTFO view model
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier submission other volumes view model for a particular obligation period</returns>
        Task<SupplierSubmissionOtherRtfoVolumesViewModel> GetSupplierSubmissionsOtherRtfoVolumes(int organisationId, ObligationPeriod obligationPeriod, int page, int pageSize, ClaimsPrincipal user);

        /// <summary>
        /// Get the Submission summary view model
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier submission summary for a particular obligation period</returns>
        Task<SupplierSubmissionSummaryViewModel> GetSupplierSubmissionsSummary(int organisationId, ObligationPeriod obligationPeriod, ClaimsPrincipal user);

        /// <summary>
        /// Get the Trading summary view model for an organisation
        /// </summary>
        /// <param name="organisationId">The organisation's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The page number to display</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a trading summary for a particular obligation period</returns>
        Task<TradingSummaryViewModel> GetTradingSummary(int organisationId
            , ObligationPeriod obligationPeriod
            , int page
            , ClaimsPrincipal user);

        /// <summary>
        /// Gets Rtfo admin consignments details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier submission Liquid and Gas summary view model for a particular obligation period</returns>
        Task<SupplierRtfoAdminConsignmentViewModel> GetSupplierRtfoAdminConsignment(int organisationId
            , ObligationPeriod obligationPeriod, int page, ClaimsPrincipal user);

        /// <summary>
        /// Gets fossil fuel details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier submission Liquid and Gas summary view model for a particular obligation period</returns>
        Task<SupplierFossilFuelSubmissionsViewModel> GetFossilFuelSubmissions(int organisationId
            , ObligationPeriod obligationPeriod, int page, ClaimsPrincipal user);

        /// <summary>
        /// Gets Electricity submissions details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier electricity submissions view model for a particular obligation period</returns>
        Task<SupplierElectricitySubmissionsViewModel> GetElectricitySubmissions(int organisationId, ObligationPeriod obligationPeriod, int page, ClaimsPrincipal user);

        /// <summary>
        /// Gets Upstream Emissions Reductions (UERs) submissions details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier Upstream Emissions Reductions (UERs) submissions view model for a particular obligation period</returns>
        Task<SupplierUpstreamEmissionsReductionsSubmissionsViewModel> GetUpstreamEmissionsReductionsSubmissions(int organisationId, ObligationPeriod obligationPeriod, int page, ClaimsPrincipal user);

        /// <summary>
        /// Gets reporting summary details for a trader for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The trader's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a trader reporting summary for a particular obligation period</returns>
        Task<TraderSummaryViewModel> GetTraderReportingSummary(int supplierId, ObligationPeriod obligationPeriod, ClaimsPrincipal user);
    }
}