﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.Security.Services;
using DfT.GOS.Website.Models;
using DfT.GOS.Website.Models.SuppliersViewModels;
using DfT.GOS.Website.Models.TraderViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using ElectricitySubmission = DfT.GOS.GhgBalanceView.Common.Models.ElectricitySubmission;

namespace DfT.GOS.Website.Orchestrators
{
    /// <summary>
    /// Class to orchestrate reporting summary details for a supplier
    /// </summary>
    public class SupplierReportingSummaryOrchestrator : ISupplierReportingSummaryOrchestrator
    {
        #region Properties

        private IOrganisationsService OrganisationsService { get; set; }
        private IBalanceViewService BalanceViewService { get; set; }
        private ITokenService TokenService { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        public SupplierReportingSummaryOrchestrator( 
            IOrganisationsService organisationsService,
            IBalanceViewService balanceViewService,
            ITokenService tokenService,
            IOptions<AppSettings> appSettings)
        {
            this.OrganisationsService = organisationsService ?? throw new ArgumentNullException("organisationsService");
            this.BalanceViewService = balanceViewService ?? throw new ArgumentNullException("balanceViewService");
            this.TokenService = tokenService ?? throw new ArgumentNullException("tokenService");
            this.AppSettings = appSettings;
        }

        #endregion Constructors

        #region ISupplierReportingSummaryOrchestrator implementation

        /// <summary>
        /// Gets reporting summary details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier reporting summary for a particular obligation period</returns>
        public async Task<SupplierReportingSummaryViewModel> GetSupplierReportingSummary(int supplierId, ObligationPeriod obligationPeriod, ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();

            var supplierResponseTask = this.OrganisationsService.GetOrganisation(supplierId, jwtToken);
            var balanceSummaryTask = this.BalanceViewService.GetSummary(supplierId, obligationPeriod.Id, jwtToken);

            await Task.WhenAll(supplierResponseTask, balanceSummaryTask);

            var supplierResponse = await supplierResponseTask;
            var balanceSummaryResponse = await balanceSummaryTask;

            if (supplierResponse.HasResult
                && (supplierResponse.Result.IsSupplier || supplierResponse.Result.IsTrader))
            {
                BalanceSummary balanceSummary = null;
                if (balanceSummaryResponse.HasResult)
                {
                    balanceSummary = balanceSummaryResponse.Result;
                }

                var summary = new SupplierReportingSummaryViewModel
                {
                    DisplaySupplierContext = this.DisplaySupplierContext(user),
                    SupplierId = supplierResponse.Result.Id,
                    SupplierName = supplierResponse.Result.Name,
                    ObligationPeriod = obligationPeriod,
                    BalanceSummary = balanceSummary
                };
                return summary;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets reporting summary details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="organisationId">The organisation's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier reporting summary for a particular obligation period</returns>
        public async Task<TraderSummaryViewModel> GetTraderReportingSummary(int organisationId, ObligationPeriod obligationPeriod, ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();

            var supplierResponseTask = this.OrganisationsService.GetOrganisation(organisationId, jwtToken);
            var balanceSummaryTask = this.BalanceViewService.GetSummary(organisationId, obligationPeriod.Id, jwtToken);

            await Task.WhenAll(supplierResponseTask, balanceSummaryTask);

            var supplierResponse = await supplierResponseTask;
            var balanceSummaryResponse = await balanceSummaryTask;

            if (supplierResponse.HasResult
                && supplierResponse.Result.IsTrader)
            {
                BalanceSummary balanceSummary = null;
                if (balanceSummaryResponse.HasResult)
                {
                    balanceSummary = balanceSummaryResponse.Result;
                }

                var summary = new TraderSummaryViewModel(user.GetUserName())
                {
                    DisplaySupplierContext = this.DisplaySupplierContext(user),
                    SupplierId = supplierResponse.Result.Id,
                    SupplierName = supplierResponse.Result.Name,
                    ObligationPeriod = obligationPeriod,
                    BalanceSummary = balanceSummary
                };
                return summary;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the Submission summary view model
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier submission summary for a particular obligation period</returns>
        public async Task<SupplierSubmissionSummaryViewModel> GetSupplierSubmissionsSummary(int organisationId, ObligationPeriod obligationPeriod, ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();
            var supplierResponseTask = this.OrganisationsService.GetOrganisation(organisationId
                , jwtToken);
            var submissionSummaryTask = this.BalanceViewService.GetSubmissions(organisationId
                , obligationPeriod.Id, jwtToken);

            await Task.WhenAll(supplierResponseTask, submissionSummaryTask);

            var supplierResponse = await supplierResponseTask;
            var submissionSummary = await submissionSummaryTask;

            if (supplierResponse.HasResult
               && supplierResponse.Result.IsSupplier)
            {
                SubmissionBalanceSummary balanceSummary = null;
                if (submissionSummary.HasResult)
                {
                    balanceSummary = submissionSummary.Result;
                }

                var summary = new SupplierSubmissionSummaryViewModel
                {
                    DisplaySupplierContext = this.DisplaySupplierContext(user),
                    SupplierId = supplierResponse.Result.Id,
                    SupplierName = supplierResponse.Result.Name,
                    ObligationPeriod = obligationPeriod,
                    SubmissionBalanceSummary = balanceSummary
                };
                return summary;
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// Get the Submission liquid and gas summary view model
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier submission Liquid and Gas summary view model for a particular obligation period</returns>
        public async Task<SupplierSubmissionLiquidGasSummaryViewModel> GetSupplierSubmissionsLiquidGasSummary(int organisationId, ObligationPeriod obligationPeriod, ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();
            var supplierResponseTask = this.OrganisationsService.GetOrganisation(organisationId
                , jwtToken);
            var liquidGasSummaryTask = this.BalanceViewService.GetLiquidGasSummary(organisationId
                , obligationPeriod.Id, jwtToken);

            await Task.WhenAll(supplierResponseTask, liquidGasSummaryTask);

            var supplierResponse = await supplierResponseTask;
            var liquidGasSummary = await liquidGasSummaryTask;

            if (supplierResponse.HasResult
               && supplierResponse.Result.IsSupplier)
            {
                LiquidGasBalanceSummary balanceSummary = null;
                if (liquidGasSummary.HasResult)
                {
                    balanceSummary = liquidGasSummary.Result;
                }

                var summary = new SupplierSubmissionLiquidGasSummaryViewModel
                {
                    DisplaySupplierContext = this.DisplaySupplierContext(user),
                    SupplierId = supplierResponse.Result.Id,
                    SupplierName = supplierResponse.Result.Name,
                    ObligationPeriod = obligationPeriod,
                    LiquidGasBalanceSummary = balanceSummary
                };
                return summary;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the other volumes via RTFO details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns the other volumes via RTFO view model for a particular obligation period</returns>
        public async Task<SupplierSubmissionOtherRtfoVolumesViewModel> GetSupplierSubmissionsOtherRtfoVolumes(int organisationId, ObligationPeriod obligationPeriod, int page, int pageSize, ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();
            var supplierResponseTask = this.OrganisationsService.GetOrganisation(organisationId
                , jwtToken);
            var liquidGasSummaryTask = this.BalanceViewService.GetLiquidGasSummary(organisationId
                , obligationPeriod.Id, jwtToken);
            var otherRtfoVolumesTask = this.BalanceViewService.GetOtherRtfoVolumes(organisationId
                , obligationPeriod.Id, page, this.AppSettings.Value.Pagination.ResultsPerPage, jwtToken);

            await Task.WhenAll(supplierResponseTask, liquidGasSummaryTask, otherRtfoVolumesTask);

            var supplierResponse = await supplierResponseTask;
            var liquidGasSummary = await liquidGasSummaryTask;
            var otherRtfoVolumes = await otherRtfoVolumesTask;

            if (supplierResponse.HasResult
               && supplierResponse.Result.IsSupplier)
            {
                LiquidGasBalanceSummary balanceSummary = null;
                if (liquidGasSummary.HasResult)
                {
                    balanceSummary = liquidGasSummary.Result;
                }

                var summary = new SupplierSubmissionOtherRtfoVolumesViewModel
                {
                    DisplaySupplierContext = this.DisplaySupplierContext(user),
                    SupplierId = supplierResponse.Result.Id,
                    SupplierName = supplierResponse.Result.Name,
                    ObligationPeriod = obligationPeriod,
                    LiquidGasBalanceSummary = balanceSummary
                };
                if(otherRtfoVolumes.HasResult && otherRtfoVolumes.Result != null)
                {
                    summary.OtherVolumesSubmission = new UIPagedResult<DfT.GOS.GhgBalanceView.Common.Models.OtherVolumesSubmission>(otherRtfoVolumes.Result, this.AppSettings.Value.Pagination.MaximumPagesToDisplay);
                }
                return summary;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the Trading summary view model for an organisation
        /// </summary>
        /// <param name="organisationId">The organisation's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The page number to display</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a trading summary for a particular obligation period</returns>
        public async Task<TradingSummaryViewModel> GetTradingSummary(int organisationId
            , ObligationPeriod obligationPeriod
            , int page
            , ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();

            var supplierResponseTask = this.OrganisationsService.GetOrganisation(organisationId, jwtToken);

            var tradingSummaryTask = this.BalanceViewService.GetTrading(organisationId
                , obligationPeriod.Id
                , page
                , this.AppSettings.Value.Pagination.ResultsPerPage
                , jwtToken);

            await Task.WhenAll(supplierResponseTask, tradingSummaryTask);

            var supplierResponse = await supplierResponseTask;
            var tradingSummaryResponse = await tradingSummaryTask;

            if (supplierResponse.HasResult
                && tradingSummaryResponse.HasResult)
            {                
                var viewModel = new TradingSummaryViewModel(this.DisplaySupplierContext(user) 
                    , organisationId
                    , supplierResponse.Result.Name
                    , obligationPeriod
                    , new UIPagedResult<CreditEvent>(tradingSummaryResponse.Result.CreditEvents
                    , this.AppSettings.Value.Pagination.MaximumPagesToDisplay));

                return viewModel;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets Rtfo admin consignments details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier submission Liquid and Gas summary view model for a particular obligation period</returns>
        public async Task<SupplierRtfoAdminConsignmentViewModel> GetSupplierRtfoAdminConsignment(int organisationId
            , ObligationPeriod obligationPeriod,int page, ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();
            var supplierResponseTask = this.OrganisationsService.GetOrganisation(organisationId
                , jwtToken);
            var liquidGasSummaryTask = this.BalanceViewService.GetLiquidGasSummary(organisationId
                , obligationPeriod.Id, jwtToken);
            var rtfoAdminConsignmentTask = this.BalanceViewService.GetRtfoAdminConsignment(organisationId
                , obligationPeriod.Id, page, this.AppSettings.Value.Pagination.ResultsPerPage, jwtToken);
            await Task.WhenAll(supplierResponseTask, liquidGasSummaryTask, rtfoAdminConsignmentTask);

            var supplierResponse = await supplierResponseTask;
            var liquidGasSummary = await liquidGasSummaryTask;
            var rtfoAdminConsignment = await rtfoAdminConsignmentTask;
            if (supplierResponse.HasResult
               && supplierResponse.Result.IsSupplier)
            {
                LiquidGasBalanceSummary balanceSummary = null;
                if (liquidGasSummary.HasResult)
                {
                    balanceSummary = liquidGasSummary.Result;
                }

                var summary = new SupplierRtfoAdminConsignmentViewModel
                {
                    DisplaySupplierContext = this.DisplaySupplierContext(user),
                    SupplierId = supplierResponse.Result.Id,
                    SupplierName = supplierResponse.Result.Name,
                    ObligationPeriod = obligationPeriod,
                    LiquidGasBalanceSummary = balanceSummary
                };
                if (rtfoAdminConsignment.HasResult && rtfoAdminConsignment.Result != null)
                {
                    summary.RtfoAdminConsignmentSubmissions = new UIPagedResult<DfT.GOS.GhgBalanceView.Common.Models.RtfoAdminConsignmentSubmission>
                        (rtfoAdminConsignment.Result, this.AppSettings.Value.Pagination.MaximumPagesToDisplay);
                }
                return summary;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets fossil fuel details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier submission Liquid and Gas summary view model for a particular obligation period</returns>
        public async Task<SupplierFossilFuelSubmissionsViewModel> GetFossilFuelSubmissions(int organisationId
            , ObligationPeriod obligationPeriod, int page, ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();
            var supplierResponseTask = this.OrganisationsService.GetOrganisation(organisationId
                , jwtToken);
            var liquidGasSummaryTask = this.BalanceViewService.GetLiquidGasSummary(organisationId
                , obligationPeriod.Id, jwtToken);
            var fossilFuelSubmissionTask = this.BalanceViewService.GetFossilFuelSubmissions(organisationId
                , obligationPeriod.Id, page, this.AppSettings.Value.Pagination.ResultsPerPage, jwtToken);
            await Task.WhenAll(supplierResponseTask, liquidGasSummaryTask, fossilFuelSubmissionTask);

            var supplierResponse = await supplierResponseTask;
            var liquidGasSummary = await liquidGasSummaryTask;
            var fossilFuelSubmissions = await fossilFuelSubmissionTask;
            if (supplierResponse.HasResult
               && supplierResponse.Result.IsSupplier)
            {
                LiquidGasBalanceSummary balanceSummary = null;
                if (liquidGasSummary.HasResult)
                {
                    balanceSummary = liquidGasSummary.Result;
                }

                var summary = new SupplierFossilFuelSubmissionsViewModel
                {
                    DisplaySupplierContext = this.DisplaySupplierContext(user),
                    SupplierId = supplierResponse.Result.Id,
                    SupplierName = supplierResponse.Result.Name,
                    ObligationPeriod = obligationPeriod,
                    LiquidGasBalanceSummary = balanceSummary
                };
                if(fossilFuelSubmissions.HasResult && fossilFuelSubmissions.Result != null)
                {
                    summary.FossilFuelSubmissions = new UIPagedResult<DfT.GOS.GhgBalanceView.Common.Models.FossilGasSubmission>
                        (fossilFuelSubmissions.Result
                    , this.AppSettings.Value.Pagination.MaximumPagesToDisplay);
                }
                if(fossilFuelSubmissions.HasResult && fossilFuelSubmissions.Result != null)
                {
                    summary.FossilFuelSubmissions = 
                        new UIPagedResult<DfT.GOS.GhgBalanceView.Common.Models.FossilGasSubmission>(fossilFuelSubmissions.Result
                    , this.AppSettings.Value.Pagination.MaximumPagesToDisplay);
                }
                return summary;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets Electricity details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier Electricity submissions details view model for a particular obligation period</returns>
        public async Task<SupplierElectricitySubmissionsViewModel> GetElectricitySubmissions(int organisationId
            , ObligationPeriod obligationPeriod, int page, ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();
            var supplierResponseTask = this.OrganisationsService.GetOrganisation(organisationId
                , jwtToken);
            var submissionSummaryTask = this.BalanceViewService.GetSubmissions(organisationId
                , obligationPeriod.Id, jwtToken);
            var electricitySubmissionTask = this.BalanceViewService.GetElectricitySubmissions(organisationId
                , obligationPeriod.Id, page, this.AppSettings.Value.Pagination.ResultsPerPage, jwtToken);
            await Task.WhenAll(supplierResponseTask, submissionSummaryTask, electricitySubmissionTask);

            var supplierResponse = await supplierResponseTask;
            var submissionSummary = await submissionSummaryTask;
            var electricitySubmissions = await electricitySubmissionTask;
            if (supplierResponse.HasResult
               && supplierResponse.Result.IsSupplier)
            {
                SubmissionBalanceSummary balanceSummary = null;
                if (submissionSummary.HasResult)
                {
                    balanceSummary = submissionSummary.Result;
                }

                var summary = new SupplierElectricitySubmissionsViewModel
                {
                    DisplaySupplierContext = this.DisplaySupplierContext(user),
                    SupplierId = supplierResponse.Result.Id,
                    SupplierName = supplierResponse.Result.Name,
                    ObligationPeriod = obligationPeriod,
                    SubmissionBalanceSummary = balanceSummary
                };
                if (electricitySubmissions.HasResult && electricitySubmissions.Result != null)
                {
                    summary.ElectricitySubmissions = new UIPagedResult<ElectricitySubmission>
                        (electricitySubmissions.Result
                    , this.AppSettings.Value.Pagination.MaximumPagesToDisplay);
                }
                return summary;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets Upstream Emissions Reductions (UERs) submissions details for a supplier for a particular obligation period
        /// </summary>
        /// <param name="supplierId">The supplier's unique ID</param>
        /// <param name="obligationPeriod">The obligation period we are reporting on</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <param name="user">Current user</param>
        /// <returns>Returns a supplier Upstream Emissions Reductions (UERs) submissions view model for a particular obligation period</returns>
        public async Task<SupplierUpstreamEmissionsReductionsSubmissionsViewModel> GetUpstreamEmissionsReductionsSubmissions(int organisationId, ObligationPeriod obligationPeriod, int page, ClaimsPrincipal user)
        {
            if (obligationPeriod == null)
            {
                // We cannot proceed with no Obligation Period
                return null;
            }

            var jwtToken = await this.TokenService.GetJwtToken();

            var supplierResponseTask = this.OrganisationsService.GetOrganisation(organisationId, jwtToken);

            var uerSummaryTask = this.BalanceViewService.GetUpstreamEmissionReductionSubmissions(organisationId
                , obligationPeriod.Id
                , page
                , this.AppSettings.Value.Pagination.ResultsPerPage
                , jwtToken);

            await Task.WhenAll(supplierResponseTask, uerSummaryTask);

            var supplierResponse = await supplierResponseTask;
            var uerSummaryResponse = await uerSummaryTask;

            if (supplierResponse.HasResult
                && uerSummaryResponse.HasResult)
            {
                var viewModel = new SupplierUpstreamEmissionsReductionsSubmissionsViewModel(this.DisplaySupplierContext(user)
                    , organisationId
                    , supplierResponse.Result.Name
                    , obligationPeriod
                    , new UIPagedResult<UpstreamEmissionsReductionSubmission>(uerSummaryResponse.Result.UpstreamEmissionsReductions
                    , this.AppSettings.Value.Pagination.MaximumPagesToDisplay)
                    , uerSummaryResponse.Result.TotalAmount);

                return viewModel;
            }
            else
            {
                return null;
            }
        }

        #endregion ISupplierReportingSummaryOrchestrator implementation

        #region Methods

        /// <summary>
        /// Whether the supplier context (i.e. their name) should be displayed to the user
        /// </summary>
        /// <returns>True if the Supplier Context should be displayed, false otherwise</returns>
        private bool DisplaySupplierContext(ClaimsPrincipal user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return user.IsAdministrator();
        }       

        #endregion Methods
    }
}
