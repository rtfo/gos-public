﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Website.Configuration;

namespace DfT.GOS.Website
{
    public class AppSettings
    {
        public string ApplicationVersion { get; set; }
        public string BuildNumber { get; set; }
        public string ApplicationsServiceApiUrl { get; set; }
        public string RtfoApplicationsServiceApiUrl { get; set; }
        public string GhgFuelsServiceApiUrl { get; set; }
        public string IdentityApiUrl { get; set; }
        public string OrganisationsApiUrl { get; set; }
        public string ReportingPeriodsApiUrl {get; set; }
        public string SystemParametersServiceApiUrl { get; set; }
        public string CalculationsApiUrl { get; set; }
        public string BalanceViewServiceApiUrl { get; set; }
        public string GhgBalanceOrchestratorApiUrl { get; set; }
        public string GhgLedgerApiUrl { get; set; }
        public string GhgMarketplaceApiUrl { get; set; }
        public HttpClientAppSettings HttpClient { get; set; }
        public virtual PaginationConfigurationSettings Pagination { get; set; }
        public int AuthCookieExpiration { get; set; }
        public int CookieWarningCookieExpirationInMonths { get; set; }
        public int UpdateGhgBalanceConfirmationRefreshIntervalInSeconds { get; set; }
    }
}
