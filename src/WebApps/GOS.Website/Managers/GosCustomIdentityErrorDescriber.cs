﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Identity;

namespace DfT.GOS.Website.Managers
{
    public class GosCustomIdentityErrorDescriber : IdentityErrorDescriber
    {
        /// <summary>
        /// Error message to display for duplicate username / email address
        /// </summary>
        public override IdentityError DuplicateUserName(string userName) { return new IdentityError { Code = nameof(DuplicateUserName), Description = $"A user already exists with the email address '{userName}'" }; }
    }
}
