﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Security.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Website.Managers
{
    public class UserManager : IUserStore<GosApplicationUser>
        , IUserPasswordStore<GosApplicationUser>
        , IUserEmailStore<GosApplicationUser>
        , IRoleStore<GosRole> 
        , IUserRoleStore<GosApplicationUser>
        , IUserLockoutStore<GosApplicationUser>
    {
        #region Private Properties
        
        private IUsersService IdentityService {get;set;}
        private ITokenService TokenService { get; set; }
        #endregion Private Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        public UserManager(IUsersService identityService, ITokenService tokenService)
        {            
            this.IdentityService = identityService;
            this.TokenService = tokenService;
        }

        #endregion Constructors

        #region IUserStore Implementation

        /// <summary>
        /// Handles creation of a new user
        /// </summary>
        /// <param name="user">The user to create</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<IdentityResult> IUserStore<GosApplicationUser>.CreateAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<IdentityResult> IUserStore<GosApplicationUser>.DeleteAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }        

        /// <summary>
        /// Finds a user by Id
        /// </summary>
        /// <param name="userId">The ID of the user to find</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<GosApplicationUser> IUserStore<GosApplicationUser>.FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return this.IdentityService.FindUserByIdAsync(userId, this.TokenService.GetJwtToken().Result, cancellationToken);            
        }

        Task<GosApplicationUser> IUserStore<GosApplicationUser>.FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return this.IdentityService.FindUserByNameAsync(normalizedUserName, cancellationToken, this.TokenService.GetJwtToken().Result);
        }

        Task<string> IUserStore<GosApplicationUser>.GetNormalizedUserNameAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormalizedUserName);
        }

        Task<string> IUserStore<GosApplicationUser>.GetUserIdAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id);
        }

        Task<string> IUserStore<GosApplicationUser>.GetUserNameAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }       

        Task IUserStore<GosApplicationUser>.SetNormalizedUserNameAsync(GosApplicationUser user, string normalizedName, CancellationToken cancellationToken)
        {
            user.NormalizedUserName = normalizedName;
            return Task.FromResult(user);
        }        

        Task IUserStore<GosApplicationUser>.SetUserNameAsync(GosApplicationUser user, string userName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        async Task<IdentityResult> IUserStore<GosApplicationUser>.UpdateAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            var result = await this.IdentityService.UpdateUserAsync(user, cancellationToken, await this.TokenService.GetJwtToken());
            return IdentityResult.Success;
        }

        #endregion IUserStore Implementation

        #region IUserPasswordStore Implementation

        Task IUserPasswordStore<GosApplicationUser>.SetPasswordHashAsync(GosApplicationUser user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        Task<bool> IUserPasswordStore<GosApplicationUser>.HasPasswordAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IUserPasswordStore<GosApplicationUser>.GetPasswordHashAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        #endregion IUserPasswordStore Implementation

        #region IUserEmailStore implementation

        Task IUserEmailStore<GosApplicationUser>.SetEmailAsync(GosApplicationUser user, string email, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IUserEmailStore<GosApplicationUser>.GetEmailAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        Task<bool> IUserEmailStore<GosApplicationUser>.GetEmailConfirmedAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.EmailConfirmed);
        }

        Task IUserEmailStore<GosApplicationUser>.SetEmailConfirmedAsync(GosApplicationUser user, bool confirmed, CancellationToken cancellationToken)
        {
            user.EmailConfirmed = confirmed;
            return Task.FromResult(user);
        }

        /// <summary>
        /// Finds a user by normalised email
        /// </summary>
        /// <param name="normalizedEmail">The normalised email address to find the user by</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<GosApplicationUser> IUserEmailStore<GosApplicationUser>.FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            return this.IdentityService.FindUserByEmailAsync(normalizedEmail, cancellationToken, this.TokenService.GetJwtToken().Result);           
        }

        Task<string> IUserEmailStore<GosApplicationUser>.GetNormalizedEmailAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task IUserEmailStore<GosApplicationUser>.SetNormalizedEmailAsync(GosApplicationUser user, string normalizedEmail, CancellationToken cancellationToken)
        {
            user.NormalizedEmail = normalizedEmail;
            return Task.FromResult(user);
        }

        #endregion IUserEmailStore implementation

        #region IUserRoleStore implementation

        Task IUserRoleStore<GosApplicationUser>.AddToRoleAsync(GosApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task IUserRoleStore<GosApplicationUser>.RemoveFromRoleAsync(GosApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<IList<string>> IUserRoleStore<GosApplicationUser>.GetRolesAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return this.IdentityService.GetRolesByUserIdAsync(user.Id, cancellationToken, this.TokenService.GetJwtToken().Result);
        }

        Task<bool> IUserRoleStore<GosApplicationUser>.IsInRoleAsync(GosApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<IList<GosApplicationUser>> IUserRoleStore<GosApplicationUser>.GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion IUserRoleStore implementation

        #region IRoleStore implementation

        Task<IdentityResult> IRoleStore<GosRole>.CreateAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<IdentityResult> IRoleStore<GosRole>.UpdateAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<IdentityResult> IRoleStore<GosRole>.DeleteAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IRoleStore<GosRole>.GetRoleIdAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IRoleStore<GosRole>.GetRoleNameAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task IRoleStore<GosRole>.SetRoleNameAsync(GosRole role, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IRoleStore<GosRole>.GetNormalizedRoleNameAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task IRoleStore<GosRole>.SetNormalizedRoleNameAsync(GosRole role, string normalizedName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<GosRole> IRoleStore<GosRole>.FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<GosRole> IRoleStore<GosRole>.FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion IRoleStore implementation        

        #region IUserLockoutStore Implementation

        Task<DateTimeOffset?> IUserLockoutStore<GosApplicationUser>.GetLockoutEndDateAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.LockoutEnd);
        }

        Task IUserLockoutStore<GosApplicationUser>.SetLockoutEndDateAsync(GosApplicationUser user, DateTimeOffset? lockoutEnd, CancellationToken cancellationToken)
        {
            user.LockoutEnd = lockoutEnd;
            return Task.FromResult(0);
        }

        Task<int> IUserLockoutStore<GosApplicationUser>.IncrementAccessFailedCountAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            user.AccessFailedCount++;            
            return Task.FromResult(user.AccessFailedCount);          
        }

        Task IUserLockoutStore<GosApplicationUser>.ResetAccessFailedCountAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            user.AccessFailedCount = 0;
            return Task.FromResult(0);
        }

        Task<int> IUserLockoutStore<GosApplicationUser>.GetAccessFailedCountAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.AccessFailedCount);
        }

        Task<bool> IUserLockoutStore<GosApplicationUser>.GetLockoutEnabledAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.LockoutEnabled);
        }

        Task IUserLockoutStore<GosApplicationUser>.SetLockoutEnabledAsync(GosApplicationUser user, bool enabled, CancellationToken cancellationToken)
        {
            user.LockoutEnabled = enabled;
            return Task.FromResult(0);
        }

        #endregion IUserLockoutStore Implementation

        void IDisposable.Dispose()
        {
            //TODO: Dispose of any child objects
        }
    }
}
