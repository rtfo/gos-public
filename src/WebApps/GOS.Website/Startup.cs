﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DfT.GOS.Website;
using DfT.GOS.Website.Managers;
using DfT.GOS.Http;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.Identity.API.Client.Services;
using Microsoft.AspNetCore.Http;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.Cookies;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Security.Services;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgBalanceOrchestrator.API.Client.Services;
using DfT.GOS.Website.Middleware;
using Microsoft.Extensions.Caching.Memory;
using DfT.GOS.Website.Orchestrators;
using DfT.GOS.GhgLedger.API.Client.Services;
using AutoMapper;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Website.Models.SuppliersViewModels;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using DfT.GOS.GhgMarketplace.API.Client.Services;
using DfT.GOS.Website.Services;
using Microsoft.Extensions.Hosting;
using DfT.GOS.RtfoApplications.API.Client.Services;
using DfT.GOS.Website.Configuration;
using Microsoft.AspNetCore.Razor.TagHelpers;
using DfT.GOS.Website.Extensions;

namespace GOS.Website
{
    /// <summary>
    /// Startup class for the GHG Website
    /// </summary>
    public class Startup
    {
        #region Constants

        public const string EnvironmentVariable_ApplicationVersion = "APPLICATION_VERSION";
        public const string EnvironmentVariable_BuildNumber = "BUILD_NUMBER";
        public const string EnvironmentVariable_JwtTokenAudience = "JWT_TOKEN_AUDIENCE";
        public const string EnvironmentVariable_JwtTokenIssuer = "JWT_TOKEN_ISSUER";
        public const string EnvironmentVariable_JwtTokenKey = "JWT_TOKEN_KEY";

        public const string ErrorMessage_ApplicationVersionNotSupplied = "Unable to get the application version from the enironmental veriables.  Missing variable: " + EnvironmentVariable_ApplicationVersion;
        public const string ErrorMessage_BuildNumberNotSupplied = "Unable to get the build number from the enironmental veriables.  Missing variable: " + EnvironmentVariable_BuildNumber;
        public const string ErrorMessage_JwtTokenAudience = "Unable to get the JWT Token Audience from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenAudience;
        public const string ErrorMessage_JwtTokenIssuer = "Unable to get the JWT Token Issuer from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenIssuer;
        public const string ErrorMessage_JwtTokenKey = "Unable to get the JWT Token Key from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenKey;

        #endregion Constants

        #region Properties

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; private set; }
        private ILogger<Startup> Logger { get; set; }

        #endregion Properties

        #region Constructors

        public Startup(IConfiguration configuration
            , IWebHostEnvironment environment
            , ILogger<Startup> logger)
        {
            this.Configuration = configuration;
            this.Environment = environment;
            this.Logger = logger;
        }

        #endregion Constructors

        #region Public Methods

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            this.ValidateEnvironmentVariables();

            var appSettings = Configuration.Get<AppSettings>();

            services.AddIdentity<GosApplicationUser, GosRole>(o =>
            {
                o.Password.RequireUppercase = true;
                o.Password.RequireLowercase = true;
                o.Password.RequireNonAlphanumeric = true;
                o.Password.RequiredLength = 8;
            })

            .AddDefaultTokenProviders()
            .AddErrorDescriber<GosCustomIdentityErrorDescriber>();

            services.AddLogging();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options =>
            {
                options.SlidingExpiration = true;
                options.AccessDeniedPath = new PathString("/Account/AccessDenied");
                options.LoginPath = new PathString("/Account/SignIn");
                options.EventsType = typeof(CookieTokenAuthenticationEvents);

                options.Cookie = new CookieBuilder
                {
                    HttpOnly = true,
                    Name = "GOS",
                    Path = "/",
                    SameSite = SameSiteMode.Lax,
                    SecurePolicy = CookieSecurePolicy.SameAsRequest
                };
            })
            .AddJwtBearer(cfg =>
            {
                //Setup JWT Auth Token
                var tokenAudience = Configuration[EnvironmentVariable_JwtTokenAudience];
                var tokenIssuer = Configuration[EnvironmentVariable_JwtTokenIssuer];
                var tokenKey = Encoding.UTF8.GetBytes(Configuration[EnvironmentVariable_JwtTokenKey]);

                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,

                    ValidIssuer = tokenIssuer,
                    ValidAudience = tokenAudience,
                    IssuerSigningKey = new SymmetricSecurityKey(tokenKey)
                };
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;
            });

            //Authentication event called on each request made from the UI
            services.AddScoped<CookieTokenAuthenticationEvents>();

            services.ConfigureApplicationCookie(config =>
            {
                config.LoginPath = "/Account/SignIn";
            });

            services.Configure<AppSettings>(Configuration);
            //Initialize Cache Entry options from appsettings
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetAbsoluteExpiration(TimeSpan.FromSeconds(appSettings.HttpClient.HttpClientCacheTimeout));
            // Add application services.
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IUserStore<GosApplicationUser>, UserManager>();
            services.AddTransient<IRoleStore<GosRole>, UserManager>();
            services.AddSingleton<ITokenService, TokenService>();
            services.AddSingleton(typeof(MemoryCacheEntryOptions), cacheEntryOptions);
            // Configure Services / Resiliant HttpClient for service calls
            var httpConfigurer = new HttpClientConfigurer(appSettings.HttpClient, this.Logger);

            // - Services
            httpConfigurer.Configure(services.AddHttpClient<IApplicationsService, ApplicationsService>(config => { config.BaseAddress = new Uri(appSettings.ApplicationsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IBalanceViewService, BalanceViewService>(config => { config.BaseAddress = new Uri(appSettings.BalanceViewServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IReviewApplicationsService, ReviewApplicationsService>(config => { config.BaseAddress = new Uri(appSettings.ApplicationsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IGhgFuelsService, GhgFuelsService>(config => { config.BaseAddress = new Uri(appSettings.GhgFuelsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IUsersService, UsersService>(config => { config.BaseAddress = new Uri(appSettings.IdentityApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IOrganisationsService, OrganisationsService>(config => { config.BaseAddress = new Uri(appSettings.OrganisationsApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IReportingPeriodsService, ReportingPeriodsService>(config => { config.BaseAddress = new Uri(appSettings.ReportingPeriodsApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<ISystemParametersService, SystemParametersService>(config => { config.BaseAddress = new Uri(appSettings.SystemParametersServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IIdentityService, IdentityService>(config => { config.BaseAddress = new Uri(appSettings.IdentityApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<ICalculationsService, CalculationsService>(config => { config.BaseAddress = new Uri(appSettings.CalculationsApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IBalanceAdministrationService, BalanceAdministrationService>(config => { config.BaseAddress = new Uri(appSettings.GhgBalanceOrchestratorApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<ILedgerService, LedgerService>(config => { config.BaseAddress = new Uri(appSettings.GhgLedgerApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IMarketplaceService, MarketplaceService>(config => { config.BaseAddress = new Uri(appSettings.GhgMarketplaceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IRtfoPerformanceDataService, RtfoPerformanceDataService>(config => { config.BaseAddress = new Uri(appSettings.RtfoApplicationsServiceApiUrl); }));

            services.AddTransient<ISupplierReportingSummaryOrchestrator, SupplierReportingSummaryOrchestrator>();
            services.AddTransient<IPerformanceDataHelperService, PerformanceDataHelperService>();
            services.AddTransient<IOrganisationsSummaryService, OrganisationsSummaryService>();

            services.AddAntiforgery(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.Name = "XSRF-TOKEN";
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
            });

            var mvcBuilder = services.AddMvc();
            mvcBuilder.AddNewtonsoftJson();

            if (this.Environment.IsDevelopment())
            {
                mvcBuilder.AddRazorRuntimeCompilation();
            }

            services.AddAutoMapper(mapper => mapper.CreateMap<Organisation, SupplierBalanceSummary>());
            
            // Add service to generate nonce for CSP
            services.AddCSPNonce(nonceByteAmount: 32);

            // Register the Google Analytics configuration
            services.Configure<GoogleAnalyticsConfigurationSettings>(options => Configuration.GetSection("GoogleAnalytics").Bind(options));

            // Register the Google Analytics TagHelperComponent
            services.AddTransient<ITagHelperComponent, GoogleAnalyticsTagHelperComponent>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");

                //Force requests to use HTTPS (when not in dev)
                // - The load balance terminiates HTTPS, forwarding to HTTP in the website. When we do automatic redirectes (because we are 
                //   not logged in to see a resource) we use the requested scheme to redirect, i.e. HTTP. We want to force HTTP. See
                //   JIRA ROSGOS-1442 for further details.
                app.Use(async (context, next) =>
                {
                    context.Request.Scheme = "https";
                    await next.Invoke();
                });
            }

            app.UseStaticFiles(new StaticFileOptions()
            {
                //Apply appropriate HTTP Headers to static files
                OnPrepareResponse = context =>
                {
                    SecurityHeaders.ApplySecurityHeaders(context.Context, applyCachingPolicies: false);
                }
            });

            app.UseStatusCodePagesWithReExecute("/Home/Error", "?statusCode={0}");
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseGdsCookiePolicy();
            app.UseSecurityHeaders();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default",
                     "{controller=Home}/{action=Index}/{id?}");
            });
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Validates that all expected environment variables have been supplied
        /// </summary>
        private void ValidateEnvironmentVariables()
        {
            //Add version info
            if (Configuration[EnvironmentVariable_ApplicationVersion] == null)
            {
                throw new NullReferenceException(ErrorMessage_ApplicationVersionNotSupplied);
            }
            Configuration["ApplicationVersion"] = Configuration[EnvironmentVariable_ApplicationVersion];

            //Build Number
            if (Configuration[EnvironmentVariable_BuildNumber] == null)
            {
                throw new NullReferenceException(ErrorMessage_BuildNumberNotSupplied);
            }
            Configuration["BuildNumber"] = Configuration[EnvironmentVariable_BuildNumber];

            //JWT Token Audience
            if (Configuration[EnvironmentVariable_JwtTokenAudience] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenAudience);
            }

            //JWT Token Issuer
            if (Configuration[EnvironmentVariable_JwtTokenIssuer] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenIssuer);
            }

            //JWT Token Key
            if (Configuration[EnvironmentVariable_JwtTokenKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenKey);
            }
        }

        #endregion Private Methods
    }
}
