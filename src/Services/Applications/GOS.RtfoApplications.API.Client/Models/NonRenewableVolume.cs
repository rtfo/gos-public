﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.RtfoApplications.API.Client.Models
{
    /// <summary>
    /// Represents a non-renewable Volume from ROS
    /// </summary>
    public class NonRenewableVolume
    {
        #region Properties

        /// <summary>
        /// Obligation Period Id
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Organisation Id
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// End Date of Reported Month
        /// </summary>
        public DateTime ReportedMonthEndDate { get; set; }

        /// <summary>
        /// The ID of the Fuel Type
        /// </summary>
        public int FuelTypeId { get; set; }

        /// <summary>
        /// Fuel Type
        /// </summary>
        public string FuelTypeName { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        public long Volume { get; set; }

        /// <summary>
        /// Energy Density
        /// </summary>
        public decimal? EnergyDensity { get; set; }

        /// <summary>
        /// Carbon Intensity
        /// </summary>
        public decimal? CarbonIntensity { get; set; }

        #endregion Properties
    }
}
