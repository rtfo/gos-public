﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using GOS.RtfoApplications.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Client.Services
{
    /// <summary>
    /// Defines the interface for retrieving performance data from ROS
    /// </summary>
    public interface IRtfoPerformanceDataService
    {
        /// <summary>
        /// Returns the performance data relating to RTFO based Applications
        /// </summary>
        /// <remarks>
        ///  - Total No. of Admin Consignments for each calendar month
        /// </remarks>
        /// <returns>JSON array of objects for the applications performance data</returns>
        Task<HttpObjectResponse<IEnumerable<RtfoPerformanceData>>> GetPerformanceData(string jwtToken);
    }
}
