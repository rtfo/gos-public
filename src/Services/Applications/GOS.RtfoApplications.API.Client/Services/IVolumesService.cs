﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.RtfoApplications.API.Client.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Client.Services
{
    /// <summary>
    /// Defines the interface for retrieving Volume data from ROS
    /// </summary>
    public interface IVolumesService
    {
        /// <summary>
        /// Retrieves Volumes by Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Volumes for the specified Obligation Period and Supplier</returns>
        Task<HttpObjectResponse<IEnumerable<NonRenewableVolume>>> Get(int obligationPeriodId, string jwtToken);

        /// <summary>
        /// Retrieves Volumes by Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Volumes for the specified Obligation Period and Supplier</returns>
        Task<HttpObjectResponse<IEnumerable<NonRenewableVolume>>> Get(int obligationPeriodId, int organisationId, string jwtToken);
    }
}
