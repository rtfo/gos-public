﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using GOS.RtfoApplications.Common.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Client.Services
{
    /// <summary>
    /// Client wrapper for the RTFO Applications service
    /// </summary>
    public class RtfoPerformanceDataService : HttpServiceClient, IRtfoPerformanceDataService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">HttpClient instance to use when making requests</param>
        public RtfoPerformanceDataService(HttpClient httpClient) : base(httpClient)
        {
        }

        #endregion Constructors

        #region IPerformanceDataService Implementation

        /// <summary>
        /// Returns the performance data relating to RTFO based Applications
        /// </summary>
        /// <remarks>
        ///  - Total No. of Admin Consignments for each calendar month
        /// </remarks>
        /// <returns>JSON array of objects for the applications performance data</returns>
        async Task<HttpObjectResponse<IEnumerable<RtfoPerformanceData>>> IRtfoPerformanceDataService.GetPerformanceData(string jwtToken)
        {
            var url = $"/api/v1/PerformanceData";
            return await this.HttpGetAsync<IEnumerable<RtfoPerformanceData>>(url, jwtToken);
        }

        #endregion IPerformanceDataService Implementation
    }
}
