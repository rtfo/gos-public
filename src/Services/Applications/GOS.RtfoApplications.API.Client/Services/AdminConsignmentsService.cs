﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using DfT.GOS.Http;
using DfT.GOS.RtfoApplications.API.Client.Models;

namespace DfT.GOS.RtfoApplications.API.Client.Services
{
    /// <summary>
    /// Client implementation of the Admin Consignments service
    /// </summary>
    public class AdminConsignmentsService : HttpServiceClient, IAdminConsignmentsService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all depedencies
        /// </summary>
        /// <param name="httpClient">HttpClient instance to use when making requests</param>
        public AdminConsignmentsService(HttpClient httpClient) : base(httpClient)
        {
        }

        #endregion Constructors

        #region IAdminConsignmentsService implementation

        /// <summary>
        /// Retrieves Admin Consignments by Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>All Admin Consignments for the specified Obligation Period</returns>
        public async Task<HttpObjectResponse<IEnumerable<AdminConsignment>>> Get(int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/ObligationPeriod/{obligationPeriodId}/AdminConsignments";
            return await this.HttpGetAsync<IEnumerable<AdminConsignment>>(url, jwtToken);
        }

        /// <summary>
        /// Retrieves Admin Consignments by Organisation (Supplier) and Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Admin Consignments for the specified Supplier and Obligation Period</returns>
        public async Task<HttpObjectResponse<IEnumerable<AdminConsignment>>> Get(int obligationPeriodId, int organisationId, string jwtToken)
        {
            var url = $"/api/v1/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/AdminConsignments";
            return await this.HttpGetAsync<IEnumerable<AdminConsignment>>(url, jwtToken);
        }

        #endregion IAdminConsignmentsService implementation
    }
}
