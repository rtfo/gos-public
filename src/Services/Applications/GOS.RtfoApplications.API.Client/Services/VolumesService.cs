﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using DfT.GOS.Http;
using DfT.GOS.RtfoApplications.API.Client.Models;

namespace DfT.GOS.RtfoApplications.API.Client.Services
{
    /// <summary>
    /// Client implementation of the Volumes Service
    /// </summary>
    public class VolumesService : HttpServiceClient, IVolumesService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">HttpClient to use when making requests</param>
        public VolumesService(HttpClient httpClient) : base(httpClient)
        {
        }

        #endregion Constructors

        #region IVolumesService implementation

        /// <summary>
        /// Retrieves Volumes for a given Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Volumes for specified Obligation Period</returns>
        public async Task<HttpObjectResponse<IEnumerable<NonRenewableVolume>>> Get(int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/ObligationPeriod/{obligationPeriodId}/Volumes";
            return await this.HttpGetAsync<IEnumerable<NonRenewableVolume>>(url, jwtToken);
        }

        /// <summary>
        /// Retrieves Volumes for a given Supplier and Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Volumes for specified Obligation Period and Supplier</returns>
        public async Task<HttpObjectResponse<IEnumerable<NonRenewableVolume>>> Get(int obligationPeriodId, int organisationId, string jwtToken)
        {
            var url = $"/api/v1/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Volumes";
            return await this.HttpGetAsync<IEnumerable<NonRenewableVolume>>(url, jwtToken);
        }

        #endregion IVolumesService implementation
    }
}
