﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.RtfoApplications.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Repositories
{
    /// <summary>
    /// Interface for repository to retrieve non-renewable volumes
    /// </summary>
    public interface IVolumesRepository
    {
        /// <summary>
        /// Retrieves non-renewable volume data for the given Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>Non-renewable volume data for the given Obligation Period</returns>
        Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId);

        /// <summary>
        /// Retrieves non-renewable volume data for the given Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <returns>Non-renewable volume data for the given Obligation Period and Supplier</returns>
        Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId, int organisationId);
    }
}
