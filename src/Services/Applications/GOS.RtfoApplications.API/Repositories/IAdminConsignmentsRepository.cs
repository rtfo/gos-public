﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.RtfoApplications.API.Models;
using GOS.RtfoApplications.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Repositories
{
    /// <summary>
    /// Interface for repository to retrieve Admin Consignment details
    /// </summary>
    public interface IAdminConsignmentsRepository
    {
        /// <summary>
        /// Retrieves all Admin Consignment records for the given Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>Admin Consignments for the specified Obligation Period</returns>
        Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId);

        /// <summary>
        /// Retrieves all Admin Consignment records for the given Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <returns>Admin Consignments for the specified Obligation Period and Supplier</returns>
        Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId, int organisationId);

        /// <summary>
        /// Gets The count of admin consignments reported per month
        /// </summary>
        /// <returns>Returns an enumeration of admin consignment counts per month</returns>
        Task<IEnumerable<RtfoPerformanceData>> GetAdminConsignmentCounts();
    }
}
