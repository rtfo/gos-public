﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DfT.GOS.RtfoApplications.API.Models;
using GOS.RtfoApplications.Common.Models;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.RtfoApplications.API.Repositories.ROS
{
    /// <summary>
    /// Implementation of the IAdminConsignmentRepository interface that retrieves data from the ROS database
    /// </summary>
    public class RosAdminConsignmentsRepository : RosBaseRepository, IAdminConsignmentsRepository
    {
        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="dataContext">Database context for ROS database</param>
        /// <param name="mapper">AutoMapper instance</param>
        public RosAdminConsignmentsRepository(RosDataContext dataContext, IMapper mapper) : base(dataContext, mapper)
        {
        }

        #endregion Constructors

        #region IAdminConsignmentsRepository instance

        /// <summary>
        /// Retrieves all Admin Consignments for the specified Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of the Obligation Period</param>
        /// <returns>All Admin Consignments for the specified Obligation Period</returns>
        public async Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId)
        {
            var gosAdminConsignments = await this.DataContext.GosAdminConsignments
                .Where(ac => ac.ObligationPeriodId == obligationPeriodId)
                .ToListAsync();
            return Map<GosAdminConsignments, AdminConsignment>(gosAdminConsignments);
        }

        /// <summary>
        /// Retrieves all Admin Consignments for the specified Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <returns>All Admin Consignments for the specified Obligation Period and Supplier</returns>
        public async Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId, int organisationId)
        {
            var gosAdminConsignments = await this.DataContext.GosAdminConsignments
                .Where(ac => ac.ObligationPeriodId == obligationPeriodId && ac.OrganisationId == organisationId)
                .ToListAsync();
            return Map<GosAdminConsignments, AdminConsignment>(gosAdminConsignments);
        }

        /// <summary>
        /// Gets The count of admin consignments reported per month
        /// </summary>
        /// <returns>Returns an enumeration of admin consignment counts per month</returns>
        async Task<IEnumerable<RtfoPerformanceData>> IAdminConsignmentsRepository.GetAdminConsignmentCounts()
        {
            var performanceData = this.DataContext.GosAdminConsignments
                            .GroupBy(ac => new { ac.ReportedMonthEndDate.Month,
                                ac.ReportedMonthEndDate.Year})         
                            .Select(g => new RtfoPerformanceData(new DateTime(g.Key.Year, g.Key.Month, 1)
                                                        , g.Count()))
                            .ToListAsync();

            return await performanceData;
        }

        #endregion IAdminConsignmentsRepository instance
    }
}
