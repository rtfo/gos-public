﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DfT.GOS.RtfoApplications.API.Repositories.ROS
{
    /// <summary>
    /// DTO corresponding to the Gos_AdminConsignmentsView view in the ROS database
    /// </summary>
    [Table("Gos_AdminConsignmentsView")]
    public class GosAdminConsignments
    {
        /// <summary>
        /// Obligation Period Id
        /// </summary>
        public int ObligationPeriodId { get; private set; }

        /// <summary>
        /// Organisation (Supplier) Id
        /// </summary>
        public int OrganisationId { get; private set; }

        /// <summary>
        /// End Date of Reported Month
        /// </summary>
        public DateTime ReportedMonthEndDate { get; set; }

        /// <summary>
        /// Admin Consignment Id
        /// </summary>
        [Key]
        public int AdminConsignmentId { get; private set; }

        /// <summary>
        /// Admin Consignment Supplier Reference
        /// </summary>
        public string Reference { get; private set; }

        /// <summary>
        /// The ID of the Fuel Type
        /// </summary>
        public int FuelTypeId { get; set; }

        /// <summary>
        /// Fuel Type
        /// </summary>
        public string FuelTypeName { get; private set; }

        /// <summary>
        /// The ID of the feed stock
        /// </summary>
        public int FeedStockId { get; set; }

        /// <summary>
        /// Feedstock
        /// </summary>
        public string FeedStockName { get; private set; }

        /// <summary>
        /// Volume
        /// </summary>
        public long Volume { get; private set; }

        /// <summary>
        /// Energy Density
        /// </summary>
        public decimal? EnergyDensity { get; private set; }

        /// <summary>
        /// Carbon Intensity
        /// </summary>
        public int? CarbonIntensity { get; private set; }
    }
}
