﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using AutoMapper;
using System.Collections.Generic;

namespace DfT.GOS.RtfoApplications.API.Repositories.ROS
{
    /// <summary>
    /// Base class for repositories that retrieve data from the ROS database
    /// </summary>
    public class RosBaseRepository
    {
        #region Properties

        /// <summary>
        /// Context for ROS database
        /// </summary>
        protected RosDataContext DataContext { get; set; }

        /// <summary>
        /// AutoMapper instance
        /// </summary>
        protected IMapper Mapper { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataContext">The data context for ROS</param>
        /// <param name="mapper">The automapper mapper instance</param>
        public RosBaseRepository(RosDataContext dataContext, IMapper mapper)
        {
            this.DataContext = dataContext;
            this.Mapper = mapper;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Maps values retrieved from the database to the DTO used for transporting information
        /// </summary>
        /// <param name="gosEntities">Admin Consignments retrieved from the database</param>
        /// <returns>Mapped Admin Consignments</returns>
        protected IEnumerable<TTarget> Map<TSource, TTarget>(IEnumerable<TSource> gosEntities)
        {
            var mappedEntities = this.Mapper.Map<IEnumerable<TSource>, IEnumerable<TTarget>>(gosEntities);
            return mappedEntities;
        }

        #endregion Methods
    }
}
