﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DfT.GOS.RtfoApplications.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.RtfoApplications.API.Repositories.ROS
{
    /// <summary>
    /// Implementation of the IVolumesRepository interface that retrieves data from the ROS database
    /// </summary>
    public class RosVolumesRepository : RosBaseRepository, IVolumesRepository
    {
        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="dataContext">Database context for ROS database</param>
        /// <param name="mapper">AutoMapper instance</param>
        public RosVolumesRepository(RosDataContext dataContext, IMapper mapper) : base(dataContext, mapper)
        {
        }

        #endregion Constructors

        #region IVolumesRepository instance

        /// <summary>
        /// Retrieves all Volumes for the specified Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of the Obligation Period</param>
        /// <returns>All Volumes for the specified Obligation Period</returns>
        public async Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId)
        {
            var gosVolumes = await this.DataContext.GosVolumes
                .Where(ac => ac.ObligationPeriodId == obligationPeriodId)
                .ToListAsync();
            return Map<GosVolumes, NonRenewableVolume>(gosVolumes);
        }

        /// <summary>
        /// Retrieves all Volumes for the specified Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <returns>All Volumes for the specified Obligation Period and Supplier</returns>
        public async Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId, int organisationId)
        {
            var gosVolumes = await this.DataContext.GosVolumes
                .Where(ac => ac.ObligationPeriodId == obligationPeriodId && ac.OrganisationId == organisationId)
                .ToListAsync();
            return Map<GosVolumes, NonRenewableVolume>(gosVolumes);
        }

        #endregion IVolumesRepository instance
    }
}
