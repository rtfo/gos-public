﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.RtfoApplications.API.Repositories.ROS
{
    /// <summary>
    /// Database context for data retrieved from ROS database
    /// </summary>
    public class RosDataContext : DbContext
    {
        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="options">Options for this instance of the database context</param>
        public RosDataContext(DbContextOptions<RosDataContext> options) : base(options)
        {
        }

        /// <summary>
        /// Retrieves data from the Gos_AdminConsignmentsView view in ROS
        /// </summary>
        public DbSet<GosAdminConsignments> GosAdminConsignments { get; set; }

        /// <summary>
        /// Retrieves data from the Gos_VolumesView view in ROS
        /// </summary>
        public DbSet<GosVolumes> GosVolumes { get; set; }
    }
}
