﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.RtfoApplications.API.Models;
using DfT.GOS.RtfoApplications.API.Repositories.Stub;
using GOS.RtfoApplications.Common.Models;
using Newtonsoft.Json;

namespace DfT.GOS.RtfoApplications.API.Repositories.ROS
{
    /// <summary>
    /// Implementation of the IAdminConsignmentRepository interface that retrieves data from the ROS database
    /// </summary>
    public class StubAdminConsignmentsRepository : StubRepositoryBase, IAdminConsignmentsRepository
    {
        #region Properties

        private IEnumerable<AdminConsignment> AdminConsignments { get; set; }
        private IReportingPeriodsService ReportingPeriodsService {get;set;}

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="jsonFilePath">Path of JSON file containing data</param>
        /// <param name="reportingPeriodsService">Instance of Reporting Periods service</param>
        public StubAdminConsignmentsRepository(string jsonFilePath, IReportingPeriodsService reportingPeriodsService)
        {
            if (jsonFilePath == null)
            {
                throw new ArgumentNullException("jsonFilePath");
            }

            this.ReportingPeriodsService = reportingPeriodsService ?? throw new ArgumentNullException("reportingPeriodsService");

            //Read in organisations from json file
            var json = File.ReadAllText(Path.Combine(jsonFilePath, "StubAdminConsignments.json"));
            this.AdminConsignments = JsonConvert.DeserializeObject<List<AdminConsignment>>(json);
        }

        #endregion Constructors

        #region IAdminConsignmentsRepository implementation

        /// <summary>
        /// Creates stub Admin Consignment data using the given sample data
        /// </summary>
        /// <param name="adminConsignments">Sample data to use to create stub data</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>Given Admin Consignments, with adjusted Obligation Period Id and Reported Month End Date</returns>
        private async Task<IEnumerable<AdminConsignment>> SelectAdminConsignments(IEnumerable<AdminConsignment> adminConsignments, int obligationPeriodId)
        {
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);

            if (obligationPeriod == null)
            {
                throw new Exception(string.Format("Unable to find Obligation Period with Id {0}.", obligationPeriodId));
            }

            return adminConsignments
                .Select(v => new AdminConsignment()
                {
                    AdminConsignmentId = v.AdminConsignmentId,
                    CarbonIntensity = v.CarbonIntensity,
                    EnergyDensity = v.EnergyDensity,
                    FeedStockId = v.FeedStockId,
                    FeedStockName = v.FeedStockName,
                    FuelTypeId = v.FuelTypeId,
                    FuelTypeName = v.FuelTypeName,
                    ObligationPeriodId = obligationPeriodId,
                    OrganisationId = v.OrganisationId,
                    Reference = v.Reference,
                    ReportedMonthEndDate = GetReportedMonthEndDate(v.ReportedMonthEndDate, obligationPeriod.StartDate, obligationPeriod.EndDate),
                    Volume = v.Volume + obligationPeriodId
                });
        }

        /// <summary>
        /// Retrieves all Admin Consignments for the specified Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of the Obligation Period</param>
        /// <returns>All Admin Consignments for the specified Obligation Period</returns>
        public async Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId)
        {
            return await SelectAdminConsignments(this.AdminConsignments, obligationPeriodId);
        }

        /// <summary>
        /// Retrieves all Admin Consignments for the specified Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <returns>All Admin Consignments for the specified Obligation Period and Supplier</returns>
        public async Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId, int organisationId)
        {
            var adminConsignments = this.AdminConsignments
                .Where(ac => ac.OrganisationId == organisationId);
            return await SelectAdminConsignments(adminConsignments, obligationPeriodId);
        }


        /// <summary>
        /// Returns the performance data relating to RTFO applications
        ///  - Total number of Admin Consignments for a calendar month
        /// </summary>
        /// <returns>List of the above metrics for each month</returns>
        async Task<IEnumerable<RtfoPerformanceData>> IAdminConsignmentsRepository.GetAdminConsignmentCounts()
        {
            var performanceData = this.AdminConsignments
                           .GroupBy(ac => new {
                               ac.ReportedMonthEndDate.Month,
                               ac.ReportedMonthEndDate.Year
                           })
                           .Select(g => new RtfoPerformanceData(new DateTime(g.Key.Year, g.Key.Month, 1)
                                                       , g.Count()))                          
                           .ToList();

            return await Task.FromResult(performanceData);
        }

        #endregion IAdminConsignmentsRepository implementation
    }
}
