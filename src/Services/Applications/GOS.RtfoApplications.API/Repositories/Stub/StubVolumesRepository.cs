﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.RtfoApplications.API.Models;
using DfT.GOS.RtfoApplications.API.Repositories.Stub;
using Newtonsoft.Json;

namespace DfT.GOS.RtfoApplications.API.Repositories.ROS
{
    /// <summary>
    /// Implementation of the IVolumesRepository interface that retrieves data from the ROS database
    /// </summary>
    public class StubVolumesRepository : StubRepositoryBase, IVolumesRepository
    {
        #region Properties

        private IEnumerable<NonRenewableVolume> Volumes { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="jsonFilePath">Path of JSON file containing data</param>
        /// <param name="reportingPeriodsService">Reporting Periods API Client instance</param>
        public StubVolumesRepository(string jsonFilePath, IReportingPeriodsService reportingPeriodsService)
        {
            if (jsonFilePath == null)
            {
                throw new ArgumentNullException("jsonFilePath");
            }

            this.ReportingPeriodsService = reportingPeriodsService ?? throw new ArgumentOutOfRangeException("reportingPeriodsService");

            //Read in organisations from json file
            var json = File.ReadAllText(Path.Combine(jsonFilePath, "StubVolumes.json"));
            this.Volumes = JsonConvert.DeserializeObject<List<NonRenewableVolume>>(json);
        }

        #endregion Constructors

        #region IVolumesRepository implementation

        /// <summary>
        /// Creates stub Volume data using the given sample data
        /// </summary>
        /// <param name="volumes">Sample data to use to create stub data</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>Given Volumes, with adjusted Obligation Period Id and Reported Month End Date</returns>
        private async Task<IEnumerable<NonRenewableVolume>> SelectVolumes(IEnumerable<NonRenewableVolume> volumes, int obligationPeriodId)
        {
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);

            if (obligationPeriod == null)
            {
                throw new Exception(string.Format("Unable to find Obligation Period with Id {0}.", obligationPeriodId));
            }

            return volumes
                .Select(v => new NonRenewableVolume()
                {
                    CarbonIntensity = v.CarbonIntensity,
                    EnergyDensity = v.EnergyDensity,
                    FuelTypeId = v.FuelTypeId,
                    FuelTypeName = v.FuelTypeName,
                    ObligationPeriodId = obligationPeriodId,
                    OrganisationId = v.OrganisationId,
                    ReportedMonthEndDate = GetReportedMonthEndDate(v.ReportedMonthEndDate, obligationPeriod.StartDate, obligationPeriod.EndDate),
                    Volume = v.Volume == 0 ? 0: v.Volume + obligationPeriodId
                });
        }

        /// <summary>
        /// Retrieves all Volumes for the specified Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of the Obligation Period</param>
        /// <returns>All Volumes for the specified Obligation Period</returns>
        public async Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId)
        {
            return await SelectVolumes(this.Volumes, obligationPeriodId);
        }

        /// <summary>
        /// Retrieves all Volumes for the specified Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <returns>All Volumes for the specified Obligation Period and Supplier</returns>
        public async Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId, int organisationId)
        {
            var volumes = this.Volumes
                .Where(ac => ac.OrganisationId == organisationId);
            return await SelectVolumes(volumes, obligationPeriodId);
        }

        #endregion IVolumesRepository implementation
    }
}
