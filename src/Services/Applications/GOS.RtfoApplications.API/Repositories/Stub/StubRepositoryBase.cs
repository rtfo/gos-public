﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Repositories.Stub
{
    /// <summary>
    /// Base class for Stub Repositories
    /// </summary>
    public class StubRepositoryBase
    {
        #region Methods
        /// <summary>
        /// Retrieves a valid End Date within the specified Obligation Period
        /// </summary>
        /// <param name="originalEndDate">Obligation End Date from stub data</param>
        /// <param name="obligationPeriodStartDate">Obligation Period Start Date</param>
        /// <param name="obligationPeriodEndDate">Obligation Period End Date</param>
        /// <returns>Date within the specified Obligation Period that is closest to the given date</returns>
        protected DateTime GetReportedMonthEndDate(DateTime originalEndDate, DateTime? obligationPeriodStartDate, DateTime? obligationPeriodEndDate)
        {
            if (!obligationPeriodStartDate.HasValue)
            {
                throw new Exception("Requested Obligation Period has no Start Date (this is required to generate stub test data).");
            }

            if (!obligationPeriodEndDate.HasValue)
            {
                throw new Exception("Requested Obligation Period ({0}) has no End Date (this is required to generate stub test data).");
            }

            if (originalEndDate >= obligationPeriodStartDate && originalEndDate <= obligationPeriodEndDate)
            {
                return originalEndDate;
            }
            else
            {
                var newEndDate = new DateTime(obligationPeriodStartDate.Value.Year, originalEndDate.Month, originalEndDate.Day);

                if (newEndDate < obligationPeriodStartDate)
                {
                    return newEndDate.AddYears(1);
                }
                else if (newEndDate > obligationPeriodEndDate)
                {
                    return newEndDate.AddYears(-1);
                }
                else
                {
                    return newEndDate;
                }
            }
        }

        #endregion Methods
    }
}
