﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.RtfoApplications.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Services
{
    /// <summary>
    /// Interface definition for service that retrieves non-renewable Volume data
    /// </summary>
    public interface IVolumesService
    {
        /// <summary>
        /// Retrieves Volumes by Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>Volumes for the specified Obligation Period and Supplier</returns>
        Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId);

        /// <summary>
        /// Retrieves Volumes by Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <returns>Volumes for the specified Obligation Period and Supplier</returns>
        Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId, int organisationId);
    }
}
