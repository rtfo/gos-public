﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.RtfoApplications.API.Models;
using GOS.RtfoApplications.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Services
{
    /// <summary>
    /// Interface definition for service that retrieves Admin Consignment data
    /// </summary>
    public interface IAdminConsignmentsService
    {
        /// <summary>
        /// Retrieves Admin Consignments by Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>Admin Consignments for the specified Obligation Period</returns>
        Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId);

        /// <summary>
        /// Retrieves Admin Consignments by Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <returns>Admin Consignments for the specified Obligation Period</returns>
        Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId, int organisationId);

        /// <summary>
        /// Returns the performance data relating to RTFO applications
        ///  - Total number of Admin Consignments for a calendar month
        /// </summary>
        /// <returns>List of the above metrics for each month</returns>
        Task<IEnumerable<RtfoPerformanceData>> GetPerformanceData();
    }
}
