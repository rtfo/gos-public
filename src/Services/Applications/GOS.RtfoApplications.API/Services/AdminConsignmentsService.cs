﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.RtfoApplications.API.Models;
using DfT.GOS.RtfoApplications.API.Repositories;
using GOS.RtfoApplications.Common.Models;

namespace DfT.GOS.RtfoApplications.API.Services
{
    /// <summary>
    /// Provides access to Admin Consignment data from ROS
    /// </summary>
    public class AdminConsignmentsService : IAdminConsignmentsService
    {
        #region Properties

        /// <summary>
        /// Instance of Admin Consignment Repository used to retrieve data
        /// </summary>
        private IAdminConsignmentsRepository AdminConsignmentsRepository { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="adminConsignmentsRepository">Instance of Admin Consignment Repository</param>
        public AdminConsignmentsService(IAdminConsignmentsRepository adminConsignmentsRepository)
        {
            this.AdminConsignmentsRepository = adminConsignmentsRepository;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Retrieves all Admin Consignments for an Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>All Admin Consignments for the specified Obligation Period</returns>
        public async Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId)
        {
            return await this.AdminConsignmentsRepository.Get(obligationPeriodId);
        }

        /// <summary>
        /// Retrieves all Admin Consignments for an Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Supplier</param>
        /// <returns>All Admin Consignments for the specified Obligation Period and Supplier</returns>
        public async Task<IEnumerable<AdminConsignment>> Get(int obligationPeriodId, int organisationId)
        {
            return await this.AdminConsignmentsRepository.Get(obligationPeriodId, organisationId);
        }

        /// <summary>
        /// Returns the performance data relating to RTFO applications
        ///  - Total number of Admin Consignments for a calendar month
        /// </summary>
        /// <returns>List of the above metrics for each month</returns>
        async Task<IEnumerable<RtfoPerformanceData>> IAdminConsignmentsService.GetPerformanceData()
        {
            return await this.AdminConsignmentsRepository.GetAdminConsignmentCounts();
        }

        #endregion Methods
    }
}
