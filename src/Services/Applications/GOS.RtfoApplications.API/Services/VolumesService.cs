﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.RtfoApplications.API.Models;
using DfT.GOS.RtfoApplications.API.Repositories;

namespace DfT.GOS.RtfoApplications.API.Services
{
    /// <summary>
    /// Provides access to non-renewable Volume data from ROS
    /// </summary>
    public class VolumesService : IVolumesService
    {
        #region Properties

        private IVolumesRepository VolumesRepository { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="volumesRepository">The repository holding Volume details</param>
        public VolumesService(IVolumesRepository volumesRepository)
        {
            this.VolumesRepository = volumesRepository;
        }

        #endregion Constructors

        #region IVolumesService methods

        /// <summary>
        /// Retrieves non-renewable Volumes by Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>All Volumes for the given Obligation Period</returns>
        public async Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId)
        {
            return await this.VolumesRepository.Get(obligationPeriodId);
        }

        /// <summary>
        /// Retrieves non-renewable Volumes by Obligation Period and Organisation
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="organisationId">Id of Organisation</param>
        /// <returns>All Volumes for the given Obligation Period and Organisation</returns>
        public async Task<IEnumerable<NonRenewableVolume>> Get(int obligationPeriodId, int organisationId)
        {
            return await this.VolumesRepository.Get(obligationPeriodId, organisationId);
        }

        #endregion IVolumesService methods
    }
}
