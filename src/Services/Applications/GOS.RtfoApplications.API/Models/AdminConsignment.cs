﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.RtfoApplications.API.Models
{
    /// <summary>
    /// Represents an RTFO Administrative Consignment 
    /// </summary>
    public class AdminConsignment
    {
        #region Properties

        /// <summary>
        /// Obligation Period Id
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Organisation Id
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// End Date of Reported Month
        /// </summary>
        public DateTime ReportedMonthEndDate { get; set; }

        /// <summary>
        /// Admin Consignment Id
        /// </summary>
        public int AdminConsignmentId { get; set; }

        /// <summary>
        /// Supplier Admin Consignment Reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// The ID of the Fuel Type
        /// </summary>
        public int FuelTypeId { get; set; }

        /// <summary>
        /// Fuel Type
        /// </summary>
        public string FuelTypeName { get; set; }

        /// <summary>
        /// The ID of the feed stock
        /// </summary>
        public int FeedStockId { get; set; }

        /// <summary>
        /// Feedstock
        /// </summary>
        public string FeedStockName { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        public long Volume { get; set; }

        /// <summary>
        /// Energy Density
        /// </summary>
        public decimal? EnergyDensity { get; set; }

        /// <summary>
        /// Carbon Intensity
        /// </summary>
        public int? CarbonIntensity { get; set; }

        #endregion Properties
    }
}
