﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;

namespace DfT.GOS.RtfoApplications.API.Models
{
    /// <summary>
    /// Represents the application settings stored in the appsettings.json file
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// The URL for the remote Reporting Periods Service
        /// </summary>
        public string ReportingPeriodsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the organisations service API URL
        /// </summary>
        public string OrganisationsServiceApiUrl { get; set; }

        /// <summary>
        /// HTTP Client setting for resilliant HTTP communication
        /// </summary>
        public HttpClientAppSettings HttpClient { get; set; }
    }
}
