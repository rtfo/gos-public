﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.RtfoApplications.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Controllers
{
    /// <summary>
    /// Retrieves ROS Volume data
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class VolumesController : RtfoApplicationsControllerBase
    {
        #region Properties

        private IVolumesService VolumesService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="volumesService">Instance of Volumes service</param>
        /// <param name="reportingPeriodsService">Instance of Reporting Periods service</param>
        /// <param name="organisationsService">Instance of Organisation service</param>
        public VolumesController(IVolumesService volumesService, IReportingPeriodsService reportingPeriodsService, IOrganisationsService organisationsService) 
            : base(reportingPeriodsService, organisationsService)
        {
            this.VolumesService = volumesService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Retrieves Volume data for an Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period for which Volume data is required</param>
        /// <returns>Volume data for the specified Obligation Period</returns>
        [HttpGet("ObligationPeriod/{obligationPeriodId}/Volumes")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> GetVolumes(int obligationPeriodId)
        {
            return await CheckObligationPeriod(obligationPeriodId, async () => {
                var volumes = await this.VolumesService.Get(obligationPeriodId);
                return Ok(volumes);
            });
        }

        /// <summary>
        /// Retrieves Volume data for an Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period for which Volume data is required</param>
        /// <param name="organisationId">Id of Supplier for which Volume data is requested</param>
        /// <returns>Volume data for the specified Obligation Period and Supplier</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Volumes")]
        public async Task<IActionResult> GetVolumes(int obligationPeriodId, int organisationId)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                return await CheckObligationPeriod(obligationPeriodId, async () => await CheckOrganisation(organisationId, async () => {
                    var volumes = await this.VolumesService.Get(obligationPeriodId, organisationId);
                    return Ok(volumes);
                }));
            }
            else
            {
                return Forbid();
            }
        }

        #endregion Methods
    }
}
