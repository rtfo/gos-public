﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.RtfoApplications.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Controllers
{
    /// <summary>
    /// Retrieves ROS Admin Consignment data
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminConsignmentsController : RtfoApplicationsControllerBase
    {
        #region Properties

        private IAdminConsignmentsService AdminConsignmentsService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="adminConsignmentsService">Instance of Admin Consignments service</param>
        /// <param name="reportingPeriodsService">Instance of Reporting Periods client service</param>
        /// <param name="organisationsService">Instance of organisations service</param>
        public AdminConsignmentsController(IAdminConsignmentsService adminConsignmentsService, IReportingPeriodsService reportingPeriodsService, IOrganisationsService organisationsService) 
            : base(reportingPeriodsService, organisationsService)
        {
            this.AdminConsignmentsService = adminConsignmentsService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Retrieves Admin Consignment data for an Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period for which Admin Consignment data is required</param>
        /// <returns>Admin Consignment data for the specified Obligation Period</returns>
        [HttpGet("ObligationPeriod/{obligationPeriodId}/AdminConsignments")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> GetAdminConsignments(int obligationPeriodId)
        {
            return await CheckObligationPeriod(obligationPeriodId, async () => {
                var adminConsignments = await this.AdminConsignmentsService.Get(obligationPeriodId);
                return Ok(adminConsignments);
            });
        }

        /// <summary>
        /// Retrieves Admin Consignment data for an Obligation Period and Supplier
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period for which Admin Consignment data is required</param>
        /// <param name="organisationId">Id of Supplier for which Admin Consignment data is requested</param>
        /// <returns>Admin Consignment data for the specified Obligation Period and Supplier</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/AdminConsignments")]
        public async Task<IActionResult> GetAdminConsignments(int obligationPeriodId, int organisationId)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                return await CheckObligationPeriod(obligationPeriodId, async () => await CheckOrganisation(organisationId, async () => {
                    var adminConsignments = await this.AdminConsignmentsService.Get(obligationPeriodId, organisationId);
                    return Ok(adminConsignments);
                }));
            }
            else
            {
                return Forbid();
            }
        }

        #endregion Methods
    }
}
