﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.RtfoApplications.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Controllers
{
    /// <summary>
    /// Gets GHG Application Details
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PerformanceDataController : Controller
    {
        #region Properties

        private IAdminConsignmentsService AdminConsignmentsService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependencies
        /// </summary>
        /// <param name="adminConsignmentsService">The admin consignment service</param>
        public PerformanceDataController(IAdminConsignmentsService adminConsignmentsService)
        {
            this.AdminConsignmentsService = adminConsignmentsService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Returns the performance data relating to RTFO based Applications
        /// </summary>
        /// <remarks>
        ///  - Total No. of Admin Consignments for each calendar month
        /// </remarks>
        /// <returns>JSON array of objects for the applications performance data</returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> RtfoPerformanceData()
        {
            var adminConsignmentPerformanceData = await this.AdminConsignmentsService.GetPerformanceData();
            return new OkObjectResult(adminConsignmentPerformanceData);
        }

        #endregion Methods
    }
}
