﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.API.Controllers
{
    /// <summary>
    /// Base class for Controllers in the RTFO Application service
    /// </summary>
    public class RtfoApplicationsControllerBase : ControllerBase
    {
        #region Properties

        private IReportingPeriodsService ReportingPeriodsService { get; set; }

        private IOrganisationsService OrganisationsService { get; set; }

        /// <summary>
        /// Retrieves the JWT authentication Token for the current request
        /// </summary>
        protected string AuthToken
        {
            get
            {
                var auth = HttpContext.Request.Headers["Authorization"];

                if (auth.ToString() != null)
                {
                    return auth.ToString().Replace("Bearer ", "");
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        public RtfoApplicationsControllerBase(IReportingPeriodsService reportingPeriodsService, IOrganisationsService organisationsService)
        {
            this.ReportingPeriodsService = reportingPeriodsService;
            this.OrganisationsService = organisationsService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Checks that the given Organisation exists
        /// </summary>
        /// <param name="organisationId">Id of Organisation to check</param>
        /// <param name="action">Action to perform if check succeeds</param>
        /// <returns>BadRequest if organisation does not exist, otherwise the value returned by given action</returns>
        protected async Task<IActionResult> CheckOrganisation(int organisationId, Func<Task<IActionResult>> action)
        {
            var organisation = await this.OrganisationsService.GetOrganisation(organisationId, this.AuthToken);

            if (organisation.HasResult)
            {
                return await action();
            }
            else
            {
                return BadRequest("Invalid Organisation specified.");
            }
        }

        /// <summary>
        /// Checks that the given Obligation exists
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation to check</param>
        /// <param name="action">Action to perform if check succeeds</param>
        /// <returns>BadRequest if obligation does not exist, otherwise the value returned by given action</returns>
        protected async Task<IActionResult> CheckObligationPeriod(int obligationPeriodId, Func<Task<IActionResult>> action)
        {
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);

            if (obligationPeriod == null)
            {
                return BadRequest("Invalid Obligation Period specified.");
            }
            else
            {
                return await action();
            }
        }

        #endregion Methods
    }
}
