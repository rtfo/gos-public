﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AutoMapper;
using DfT.GOS.HealthChecks.Common;
using DfT.GOS.HealthChecks.Common.Extensions;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.RtfoApplications.API.Models;
using DfT.GOS.RtfoApplications.API.Repositories;
using DfT.GOS.RtfoApplications.API.Repositories.ROS;
using DfT.GOS.RtfoApplications.API.Services;
using DfT.GOS.Security.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace DfT.GOS.RtfoApplications.API
{
    /// <summary>
    /// Configures web application
    /// </summary>
    public class Startup
    {
        #region Constants

        /// <summary>
        /// The environment variable to parse for the connection string
        /// </summary>
        public const string EnvironmentVariable_ConnectionString = "CONNECTION_STRING";

        /// <summary>
        /// The environment variable for the security JWT Token Audience used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenAudience = "JWT_TOKEN_AUDIENCE";

        /// <summary>
        /// The environment variable for the security JWT Token Issuer used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenIssuer = "JWT_TOKEN_ISSUER";

        /// <summary>
        /// The environment variable for the security JWT Token Key used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenKey = "JWT_TOKEN_KEY";

        /// <summary>
        /// The error message to throw when no connection string is supplied
        /// </summary>
        public const string ErrorMessage_ConnectionStringNotSupplied = "Unable to connect to the applications datasource, the connection string was not specified. Missing variable: " + EnvironmentVariable_ConnectionString;

        /// <summary>
        /// The error message to throw when no JWT Token Audience environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenAudience = "Unable to get the JWT Token Audience from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenAudience;

        /// <summary>
        /// The error message to throw when no JWT Token Issuer environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenIssuer = "Unable to get the JWT Token Issuer from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenIssuer;

        /// <summary>
        /// The error message to throw when no JWT Token Key environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenKey = "Unable to get the JWT Token Key from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenKey;

        #endregion Constants

        #region Properties

        /// <summary>
        /// Application configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Logger instance for current class
        /// </summary>
        public ILogger<Startup> Logger { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="configuration">Configuration settings</param>
        /// <param name="logger">Logger instance</param>
        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            this.Configuration = configuration;
            this.Logger = logger;
            this.Logger.LogInformation("Startup called for new instance of DfT.GOS.RtfoApplications.API");
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Adds services to the container
        /// </summary>
        /// <param name="services">Container to which required services will be added</param>
        public void ConfigureServices(IServiceCollection services)
        {
            this.ValidateEnvironmentVariables();

            // Resolve repositories
            var connectionString = Configuration[EnvironmentVariable_ConnectionString];

            if (connectionString == null)
            {
                throw new NullReferenceException(ErrorMessage_ConnectionStringNotSupplied);
            }
            else if (connectionString.ToLower().StartsWith("file="))
            {
                // Use the stub repository
                var jsonRepositoryPath = connectionString.Split("=")[1];
                services.AddTransient<IAdminConsignmentsRepository>(r => new StubAdminConsignmentsRepository(jsonRepositoryPath, r.GetService<IReportingPeriodsService>()));
                services.AddTransient<IVolumesRepository>(r => new StubVolumesRepository(jsonRepositoryPath, r.GetService<IReportingPeriodsService>()));

                //Add standard health check (we don't have a DB to probe)
                services.AddHealthChecks();
            }
            else
            {
                // Use ROS as the repository

                //Add health check to probe the DB
                services.AddHealthChecks()
                    .AddSqlServer(connectionString, name: "sql-server", tags: new[] { HealthCheckTypes.Readiness });

                // Configure the db context, using retries for resilience
                services.AddDbContext<RosDataContext>(options =>
                {
                    options.UseSqlServer(connectionString, sqlServerOptionsAction: sqlOptions =>
                        {
                            sqlOptions.EnableRetryOnFailure();
                        });
                });

                services.AddTransient<IAdminConsignmentsRepository, RosAdminConsignmentsRepository>();
                services.AddTransient<IVolumesRepository, RosVolumesRepository>();
            }

            // Configure Resiliant HttpClient for service calls
            var appSettings = Configuration.Get<AppSettings>();
            var httpConfigurer = new HttpClientConfigurer(appSettings.HttpClient, this.Logger);

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetAbsoluteExpiration(TimeSpan.FromSeconds(appSettings.HttpClient.HttpClientCacheTimeout));

            // Resolve services
            services.AddTransient<IAdminConsignmentsService, AdminConsignmentsService>();
            services.AddTransient<IVolumesService, VolumesService>();
            services.AddSingleton(typeof(MemoryCacheEntryOptions), cacheEntryOptions);

            httpConfigurer.Configure(services.AddHttpClient<IReportingPeriodsService, ReportingPeriodsService>(config => config.BaseAddress = new Uri(appSettings.ReportingPeriodsServiceApiUrl)));
            httpConfigurer.Configure(services.AddHttpClient<IOrganisationsService, OrganisationsService>(config => config.BaseAddress = new Uri(appSettings.OrganisationsServiceApiUrl)));

            //JWT Security Authentication
            var jwtSettings = new JwtAuthenticationSettings(Configuration[EnvironmentVariable_JwtTokenIssuer]
                , Configuration[EnvironmentVariable_JwtTokenAudience]
                , Configuration[EnvironmentVariable_JwtTokenKey]);

            var jwtConfigurer = new JwtAuthenticationConfigurer(jwtSettings);
            jwtConfigurer.Configure(services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            );

            // Register the Swagger generator
            try
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "GHG RTFO Applications API", Version = "v1", Description = "GHG RTFO Applications API is used to retrieve data from ROS for use in Obligation/Certificate calculations." });
                    c.IncludeXmlComments(xmlPath);
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { In = ParameterLocation.Header, Description = "Please enter JWT with Bearer into field in the format 'Bearer [JWT token]'", Name = "Authorization", Type = SecuritySchemeType.ApiKey });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement() {
                        {
                            new OpenApiSecurityScheme {
                                Reference = new OpenApiReference {
                                    Id = "Bearer"
                                    , Type = ReferenceType.SecurityScheme
                                },
                                Scheme = "bearer",
                                Name = "security",
                                In = ParameterLocation.Header
                            }, new List<string>()
                        }
                    });
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Register Swagger generator because of the following exception: ", ex.ToString());
            }

            services.AddControllers().AddNewtonsoftJson();
            services.AddMemoryCache();

            services.AddAutoMapper();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Application pipeline to configure</param>
        /// <param name="env">Hosting environment</param>
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            try
            {
                //Enable Health Checks
                app.UseGosHealthChecks();

                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "GHG ROS Applications V1");
                    c.RoutePrefix = string.Empty;
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Configure Swagger because of the following exception: " + ex.ToString());
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Validates that all expected environment variables have been supplied
        /// </summary>
        private void ValidateEnvironmentVariables()
        {
            //Connection String
            if (Configuration[EnvironmentVariable_ConnectionString] == null)
            {
                throw new NullReferenceException(ErrorMessage_ConnectionStringNotSupplied);
            }

            //JWT Token Audience
            if (Configuration[EnvironmentVariable_JwtTokenAudience] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenAudience);
            }

            //JWT Token Issuer
            if (Configuration[EnvironmentVariable_JwtTokenIssuer] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenIssuer);
            }

            //JWT Token Key
            if (Configuration[EnvironmentVariable_JwtTokenKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenKey);
            }
        }

        #endregion Methods
    }
}
