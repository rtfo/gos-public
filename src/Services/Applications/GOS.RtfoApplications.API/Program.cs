﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace DfT.GOS.RtfoApplications.API
{
    /// <summary>
    /// Class containing entry point for GOS RTFO Applications service
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entry point for application
        /// </summary>
        /// <param name="args">Command line arguments</param>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Create a builder to construct the website
        /// </summary>
        /// <param name="args">Command line arguments</param>
        /// <returns>WebHostBuilder instance to build the website</returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://*:80")            
                .UseStartup<Startup>();
    }
}
