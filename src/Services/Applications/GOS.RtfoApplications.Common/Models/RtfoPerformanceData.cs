﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

using System;

namespace GOS.RtfoApplications.Common.Models
{
    /// <summary>
    /// Performance data for RTFO Applications
    /// </summary>
    public class RtfoPerformanceData
    {
        /// <summary>
        /// Month the data relates to
        /// </summary>
        public DateTime Date { get; private set; }

        /// <summary>
        /// The count of admin consignments reported in GOS
        /// </summary>
        public int AdminConsignmentCount { get; private set; }

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        /// <param name="date">Month the data relates to</param>
        /// <param name="adminConsignmentCount">The count of admin consignments reported in GOS</param>
        public RtfoPerformanceData(DateTime date
            , int adminConsignmentCount)
        {
            this.Date = date;
            this.AdminConsignmentCount = adminConsignmentCount;
        }
    }
}
