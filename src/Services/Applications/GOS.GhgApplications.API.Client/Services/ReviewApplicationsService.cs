﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Web.ExtensionMethods;
using DfT.GOS.Web.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Client.Services
{
    /// <summary>
    /// Class to connect to remote review application service
    /// </summary>
    public class ReviewApplicationsService: HttpServiceClient, IReviewApplicationsService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        public ReviewApplicationsService(HttpClient httpClient)
            : base(httpClient)
        {
        }

        #endregion Constructors

        #region IReviewApplicationsService Implementation

        /// <summary>
        /// Gets all the applications that are ready for review  
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <param name="applicationParams">Parameters to filter and sort the applications by</param>
        /// <returns>List of application summary</returns>
        async Task<HttpObjectResponse<PagedResult<ApplicationSummary>>> IReviewApplicationsService.GetPendingApplicationPagedResult(string jwtToken, PendingApplicationParameters applicationParams)
        {
            return await this.HttpGetAsync<PagedResult<ApplicationSummary>>(
                string.Format("api/v1/Applications/PendingApproval?{0}", QueryParams.ToString(applicationParams)), jwtToken
                );
        }

        /// <summary>
        /// Gets all the applications that are ready for issue  
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <param name="pageSize">No. of rows per page</param>
        /// <param name="pageNumber">Current page number</param>
        /// <returns>List of application summary</returns>
        async Task<HttpObjectResponse<PagedResult<ApplicationSummary>>> IReviewApplicationsService.GetApplicationsReadyForIssuePagedResult(string jwtToken, int pageSize, int pageNumber)
        {
            return await this.HttpGetAsync<PagedResult<ApplicationSummary>>(string.Format("api/v1/Applications/ReadyForIssue?pageNumber={0}&pageSize={1}", pageNumber, pageSize), jwtToken);
        }

        /// <summary>
        /// Gets the application details for review
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        async Task<HttpObjectResponse<ApplicationReviewSummary>> IReviewApplicationsService.GetApplicationReviewSummary(int applicationId, string jwtToken)
        {
            return await this.HttpGetAsync<ApplicationReviewSummary>(string.Format("api/v1/Review/Applications/{0}", applicationId), jwtToken);
        }

        /// <summary>
        /// Issues the review application volumes command to the remote service
        /// </summary>
        /// <param name="command">The command containing review details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns a boolean indicating whether the operation was successful</returns>
        async Task<bool> IReviewApplicationsService.ReviewApplicationVolumes(ReviewApplicationVolumesCommand command, string jwtToken)
        {
            var response = await this.HttpPostAsync(string.Format("api/v1/Review/Applications/{0}/Volumes", command.ApplicationId), command, jwtToken);
            return bool.Parse(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Issues the review application supporting documents command to the remote service
        /// </summary>
        /// <param name="command">The command containing review details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns a boolean indicating whether the operation was successful</returns>
        async Task<bool> IReviewApplicationsService.ReviewApplicationSupportingDocuments(ReviewApplicationSupportingDocumentsCommand command, string jwtToken)
        {
            var response = await this.HttpPostAsync(string.Format("api/v1/Review/Applications/{0}/SupportingDocuments", command.ApplicationId), command, jwtToken);
            return bool.Parse(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Issues the review application command to the remote service
        /// </summary>
        /// <param name="command">The command containing review details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        async Task<bool> IReviewApplicationsService.ReviewApplication(ReviewApplicationCommand command, string jwtToken)
        {
            var response = await this.HttpPostAsync(string.Format("api/v1/Review/Applications/{0}", command.ApplicationId), command, jwtToken);
            return bool.Parse(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Issues the review application calculation command to the remote service
        /// </summary>
        /// <param name="command">The command containing review details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns a boolean indicating whether the operation was successful</returns>
        async Task<bool> IReviewApplicationsService.ReviewApplicationCalculation(ReviewApplicationCalculationCommand command, string jwtToken)
        {
            var response = await this.HttpPostAsync(string.Format("api/v1/Review/Applications/{0}/Calculation", command.ApplicationId), command, jwtToken);
            return bool.Parse(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Issues the approve application command to the remote service
        /// </summary>
        /// <param name="command">The command containing approval details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns a boolean indicating whether the operation was successful</returns>
        async Task<bool> IReviewApplicationsService.ApproveApplication(ApproveApplicationCommand command, string jwtToken)
        {
            var response = await this.HttpPostAsync(string.Format("api/v1/Review/Applications/{0}/Approve", command.ApplicationId), command, jwtToken);
            return bool.Parse(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Issues the reject application command to the remote service
        /// </summary>
        /// <param name="command">The command containing rejection details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns a boolean indicating whether the operation was successful</returns>
        async Task<bool> IReviewApplicationsService.RejectApplication(RejectApplicationCommand command, string jwtToken)
        {
            var response = await this.HttpPostAsync(string.Format("api/v1/Review/Applications/{0}/Reject", command.ApplicationId), command, jwtToken);
            return bool.Parse(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Issue credits to the approved applications
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns a boolean value indicating success</returns>
        async Task<bool> IReviewApplicationsService.IssueCredits(string jwtToken)
        {
            var response = await this.HttpPostAsync("api/v1/Review/Applications/issuecredits",null, jwtToken);
            return bool.Parse(await response.Content.ReadAsStringAsync());
        }
        #endregion IReviewApplicationsService Implementation
    }
}
