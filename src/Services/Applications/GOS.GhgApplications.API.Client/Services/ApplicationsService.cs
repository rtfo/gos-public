﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.API.Client.Models;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Web.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DfT.GOS.GhgApplications.API.Client.Services
{
    /// <summary>
    /// Class to connect to remote application service
    /// </summary>
    public class ApplicationsService : HttpServiceClient, IApplicationsService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        public ApplicationsService(HttpClient httpClient)
            : base(httpClient)
        {
        }

        #endregion Constructors

        #region IApplicationService Implementation

        /// <summary>
        /// Issues a command to create a new GHG application
        /// </summary>
        /// <param name="command">The details of the application to create</param>
        /// <param name="jwtToken">The JWT Token for performing Auth on the remote service</param>
        /// <returns>Returns the ID of the application, if creation is successful</returns>
        async Task<int> IApplicationsService.CreateNewApplication(CreateApplicationCommand command, string jwtToken)
        {
            var response = await this.HttpPostAsync("api/v1/Applications/", command, jwtToken);
            return int.Parse(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Issues a submit application command to the service layer
        /// </summary>
        /// <param name="command">The command containing details for submitting the application</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns bool indicating whether the application was submitted successfully</returns>
        async Task<string> IApplicationsService.SubmitApplication(SubmitApplicationCommand command, string jwtToken)
        {
            var response = await this.HttpPostAsync("api/v1/Applications/Submit", command, jwtToken);
            return JsonConvert.DeserializeObject<string>((await response.Content.ReadAsStringAsync()).ToString());
        }

        /// <summary>
        /// Call the application API service to upload a supporting document to an application
        /// </summary>
        /// <param name="supportingDocument">The file, including metadata, to upload</param>
        /// <param name="jwtToken">The JWT Token for performing Auth on the remote service</param>
        /// <returns>Returns a result containing the status of the file upload</returns>
        async Task<SupportingDocumentUpload> IApplicationsService.UploadApplicationSupportingDocument(SupportingDocument supportingDocument, string jwtToken)
        {
            var response = await this.HttpPostAsync("api/v1/Applications/" + supportingDocument.ApplicationId.ToString() + "/supportingdocuments", supportingDocument, jwtToken);

            var respContent = response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<SupportingDocumentUpload>(respContent.Result.ToString());
            return result;
        }

        /// <summary>
        /// Validate Supporting Document Meta data
        /// </summary>
        /// <param name="fileMetadata">Metadata for the file we're validating</param>
        /// <param name="jwtToken">JwtToken for authenticating against the remote service</param>
        /// <returns>List of Validation Messages/errors</returns>
        async Task<List<string>> IApplicationsService.ValidateFileMetadata(FileMetadata fileMetadata, string jwtToken)
        {
            List<string> messages = new List<string>();

            var response = await this.HttpPostAsync("api/v1/Applications/ValidateFileMetadata", fileMetadata, jwtToken);

            if (response.IsSuccessStatusCode)
            {
                var respContent = response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<SupportingDocumentUpload>(respContent.Result.ToString());
                if (!result.UploadStatus.Equals("Validated"))
                    messages.AddRange(result.Messages);
            }
            else
            {
                messages.Add("File Processing error");
            }
            return messages;
        }

        /// <summary>
        /// Gets a list of supporting documents for an application from the remote applications API service
        /// </summary>
        /// <param name="applicationId">The ID of the application to get the supporting document details for</param>
        /// <param name="jwtToken">The JWT Token for performing Auth on the remote service</param>
        /// <returns>Returns a list containing supporting documents for an application</returns>
        public async Task<HttpObjectResponse<List<UploadedSupportingDocument>>> GetSupportingDocuments(int applicationId, string jwtToken)
        {
            return await this.HttpGetAsync<List<UploadedSupportingDocument>>("api/v1/Applications/" + applicationId.ToString() + "/SupportingDocuments", jwtToken);
        }

        /// <summary>
        /// Gets an application supporting document for download.
        /// </summary>
        /// <param name="applicationId">The ID of the application to get the supporting document for</param>
        /// <param name="supportingDocumentId">The supporting Document ID</param>
        /// <param name="jwtToken">The JWT Token for performing Auth on the remote service</param>
        /// <returns>Returns the supporting document, including the file, for download</returns>
        public async Task<HttpObjectResponse<DownloadSupportingDocument>> GetSupportingDocument(int applicationId, int supportingDocumentId, string jwtToken)
        {
            return await this.HttpGetAsync<DownloadSupportingDocument>("api/v1/Applications/" + applicationId.ToString()
                + "/SupportingDocuments/" + supportingDocumentId.ToString(), jwtToken);
        }

        /// <summary>
        /// Deletes a supporting document from an application
        /// </summary>
        /// <param name="applicationId">The ID of the application to remove the document from</param>
        /// <param name="supportingDocumentId">The ID of the supporting document to remove</param>
        /// <param name="jwtToken">The JWT Token for performing Auth on the remote service</param>
        /// <returns>Returns a boolean value indicating whether the operation completed successfully</returns>
        public async Task<bool> RemoveSupportingDocument(int applicationId, int supportingDocumentId, string jwtToken)
        {
            var response = await this.HttpDeleteAsync("api/v1/Applications/" + applicationId.ToString() + "/SupportingDocuments/" + supportingDocumentId.ToString() + "/remove", jwtToken);

            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                if (json.Equals("true"))
                    return true;
                else
                    return false;
            }
            return false;
        }

        /// <summary>
        /// Gets summary details for a GHG application
        /// </summary>
        /// <param name="applicationId">The application ID to retrieve details about</param>
        /// <param name="jwtToken">The JWT token for authorisation on the remote serice</param>
        /// <returns>Returns summary details for a GHG Application</returns>
        async Task<HttpObjectResponse<ApplicationSummary>> IApplicationsService.GetApplicationSummary(int applicationId, string jwtToken)
        {
            return await this.HttpGetAsync<ApplicationSummary>(string.Format("api/v1/Applications/" + applicationId), jwtToken);
        }


        /// <summary>
        /// Gets the application items for an application
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <param name="jwtToken">JWT token</param>
        /// <returns>List of Appplication items</returns>
        async Task<HttpObjectResponse<IList<ApplicationItemSummary>>> IApplicationsService.GetApplicationItems(int applicationId, string jwtToken)
        {
            return await this.HttpGetAsync<IList<ApplicationItemSummary>>(string.Format("api/v1/Applications/" + applicationId + "/items"), jwtToken);
        }

        /// <summary>
        /// Gets all applications items for an organisation/obligation period associated with Applications that have had GHG credits issued against them
        /// </summary>
        /// <param name="organisationId">The ID of the organisation to filter on</param>
        /// <param name="obligationPeriodId">The ID of the obligation period to filter on</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>List of ApplicationItemSummary</returns>
        async Task<HttpObjectResponse<IList<ApplicationItemSummary>>> IApplicationsService.GetApplicationItemsWithIssuedCredits(int organisationId, int obligationPeriodId, string jwtToken)
        {
            return await this.HttpGetAsync<IList<ApplicationItemSummary>>(string.Format("api/v1/Applications/Organisation/" + organisationId.ToString() + "/ObligationPeriod/" + obligationPeriodId.ToString() + "/IssuedCreditsApplicationItems"), jwtToken);
        }

        /// <summary>
        /// Gets all the applications that are ready for review  
        /// </summary>
        /// <param name="organisationId">Organisation Id</param>
        /// <param name="obligationPeriodId">Obligation period id</param>
        /// <param name="pageSize">No. of records per page</param>
        /// <param name="pageNumber">current page</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>List of application summary</returns>
        async Task<HttpObjectResponse<PagedResult<ApplicationSummary>>> IApplicationsService.GetSupplierApplications(int organisationId, int obligationPeriodId, int pageSize, int pageNumber, string jwtToken)
        {
            return await this.HttpGetAsync<PagedResult<ApplicationSummary>>(string.Format("api/v1/Applications/Supplier/{0}/ObligationPeriod/{1}?pageNumber={2}&pageSize={3}", organisationId, obligationPeriodId, pageNumber, pageSize), jwtToken);
        }

        /// <summary>
        /// Revokes the specified application
        /// </summary>
        /// <param name="command">RevokeApplicationCommand containing applicationId and reason</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Response on success of revoke action</returns>
        async Task<bool> IApplicationsService.RevokeApplication(RevokeApplicationCommand command, string jwtToken)
        {
            var response = await this.HttpPostAsync("api/v1/Applications/revoke", command, jwtToken);

            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Returns the performance data relating to applications
        ///  - No. of applications with credits issued for each calendar month
        ///  - No. of incomplete applications for each calendar month
        ///  - No. of applications rejected each calendar month
        ///  - No. of applications revoked each calendar month
        ///  - No. of applications submitted for each calendar month
        ///  - No. of applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently)
        ///  - Total No. of applications for the calendar month
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>List of the above metrics for each month</returns>
        async Task<List<ApplicationPerformanceData>> IApplicationsService.GetApplicationPerformanceData(string jwtToken)
        {
            var response = await this.HttpGetAsync<List<ApplicationPerformanceData>>("api/v1/PerformanceData/Applications", jwtToken);
            return response.Result;
        }

        #endregion IApplicationService Implementation
    }
}
