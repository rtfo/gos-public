﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Web.Models;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Client.Services
{
    /// <summary>
    /// Definition of service methods for reviewing applications
    /// </summary>
    public interface IReviewApplicationsService
    {
        /// <summary>
        /// Gets all the applications that are ready for review  
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <param name="applicationParams">Parameters to filter and sort the applications by</param>
        /// <returns>Paged pending application list</returns>
        Task<HttpObjectResponse<PagedResult<ApplicationSummary>>> GetPendingApplicationPagedResult(string jwtToken, PendingApplicationParameters applicationParams);

        /// <summary>
        /// Gets all the applications that are ready for issue  
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <param name="pageSize">No. of records per page</param>
        /// <param name="pageNumber">current page</param>
        /// <returns>Paged pending application list</returns>
        Task<HttpObjectResponse<PagedResult<ApplicationSummary>>> GetApplicationsReadyForIssuePagedResult(string jwtToken, int pageSize, int pageNumber);

        /// <summary>
        /// Gets the application details for review
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        Task<HttpObjectResponse<ApplicationReviewSummary>> GetApplicationReviewSummary(int applicationId, string jwtToken);

        /// <summary>
        /// Issues the review application volumes command to the remote service
        /// </summary>
        /// <param name="command">The command containing review details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Boolean value indicating success</returns>
        Task<bool> ReviewApplicationVolumes(ReviewApplicationVolumesCommand command, string jwtToken);

        /// <summary>
        /// Issues the review application supporting documents command to the remote service
        /// </summary>
        /// <param name="command">The command containing review details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Boolean value indicating success</returns>
        Task<bool> ReviewApplicationSupportingDocuments(ReviewApplicationSupportingDocumentsCommand command, string jwtToken);

        /// <summary>
        /// Issues the review application command to the remote service
        /// </summary>
        /// <param name="command">The command containing review details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        Task<bool> ReviewApplication(ReviewApplicationCommand command, string jwtToken);

        /// <summary>
        /// Issues the review application calculation command to the remote service
        /// </summary>
        /// <param name="command">The command containing review details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Boolean value indicating success</returns>
        Task<bool> ReviewApplicationCalculation(ReviewApplicationCalculationCommand command, string jwtToken);

        /// <summary>
        /// Issues the approve application command to the remote service
        /// </summary>
        /// <param name="command">The command containing approve details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        Task<bool> ApproveApplication(ApproveApplicationCommand command, string jwtToken);

        /// <summary>
        /// Issues the reject application command to the remote service
        /// </summary>
        /// <param name="command">The command containing rejection details</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        Task<bool> RejectApplication(RejectApplicationCommand command, string jwtToken);

        /// <summary>
        /// Issue credits to the approved applications
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> IssueCredits(string jwtToken);
    }
}
