﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.API.Client.Models;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Client.Services
{
    /// <summary>
    /// Interface to ApplicationsService
    /// </summary>
    public interface IApplicationsService
    {
        /// <summary>
        /// Issues a create new application command to the service layer
        /// </summary>
        /// <param name="command">The command containing details for creating the application</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns the ID of the application if it is created</returns>
        Task<int> CreateNewApplication(CreateApplicationCommand command, string jwtToken);

        /// <summary>
        /// Gets GHG Application summary details by application ID
        /// </summary>
        /// <param name="applicationId">The ID of the application to get</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns summary details for an application</returns>
        Task<HttpObjectResponse<ApplicationSummary>> GetApplicationSummary(int applicationId, string jwtToken);

        /// <summary>
        /// Uploads a supporting document to an application
        /// </summary>
        /// <param name="supportingDocument">The file, including metadata, to upload</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns a result indicating the status of the arrempted document upload</returns>
        Task<SupportingDocumentUpload> UploadApplicationSupportingDocument(SupportingDocument supportingDocument, string jwtToken);

        /// <summary>
        /// Validate the metadata for a file we wish to upload
        /// </summary>
        /// <param name="fileMetadata">Metadata for the file we want to be validated</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>A list of Errors</returns>
        Task<List<string>> ValidateFileMetadata(FileMetadata fileMetadata, string jwtToken);

        /// <summary>
        /// Gets the uploaded supporting documents.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns the uploaded supporting document</returns>
        Task<HttpObjectResponse<List<UploadedSupportingDocument>>> GetSupportingDocuments(int applicationId, string jwtToken);

        /// <summary>
        /// Downloads the supporting document.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumentId">The supporting document identifier.</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>DownloadSupportingDocument(File and Filename)</returns>
        Task<HttpObjectResponse<DownloadSupportingDocument>> GetSupportingDocument(int applicationId, int supportingDocumentId, string jwtToken);

        /// <summary>
        /// Removes the supporting document.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumentId">The supporting document identifier.</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>True/False</returns>
        Task<bool> RemoveSupportingDocument(int applicationId, int supportingDocumentId, string jwtToken);

        /// <summary>
        /// Issues a submit application command to the service layer
        /// </summary>
        /// <param name="command">The command containing details for submitting the application</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns a message indicating whether the application was submitted successfully. Empty message is success.</returns>
        Task<string> SubmitApplication(SubmitApplicationCommand command, string jwtToken);

        /// <summary>
        /// Gets the application items for the related to the application
        /// </summary>
        /// <param name="applicationId">The application identifier</param>
        /// <returns>List of ApplicationItemSummary</returns>
        Task<HttpObjectResponse<IList<ApplicationItemSummary>>> GetApplicationItems(int applicationId, string jwtToken);

        /// <summary>
        /// Gets all applications items for an organisation/obligation period associated with Applications that have had GHG credits issued against them
        /// </summary>
        /// <param name="organisationId">The ID of the organisation to filter on</param>
        /// <param name="obligationPeriodId">The ID of the obligation period to filter on</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>List of ApplicationItemSummary</returns>
        Task<HttpObjectResponse<IList<ApplicationItemSummary>>> GetApplicationItemsWithIssuedCredits(int organisationId, int obligationPeriodId, string jwtToken);

        /// <summary>
        /// Gets the applications for the supplier/oraganisation
        /// </summary>
        /// <param name="organisationId">Organisation Id</param>
        /// <param name="obligationPeriodId">Obligation period id</param>
        /// <param name="pageSize">No. of records per page</param>
        /// <param name="pageNumber">current page</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        Task<HttpObjectResponse<PagedResult<ApplicationSummary>>> GetSupplierApplications(int organisationId, int obligationPeriodId, int pageSize, int pageNumber, string jwtToken);

        /// <summary>
        /// Revokes the specified application
        /// </summary>
        /// <param name="command">RevokeApplicationCommand containing applicationId and reason</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Response on success of revoke action</returns>
        Task<bool> RevokeApplication(RevokeApplicationCommand command, string jwtToken);

        /// <summary>
        /// Returns the performance data relating to applications
        ///  - No. of applications with credits issued for each calendar month
        ///  - No. of incomplete applications for each calendar month
        ///  - No. of applications rejected each calendar month
        ///  - No. of applications revoked each calendar month
        ///  - No. of applications submitted for each calendar month
        ///  - No. of applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently)
        ///  - Total No. of applications for the calendar month
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>List of the above metrics for each month</returns>
        Task<List<ApplicationPerformanceData>> GetApplicationPerformanceData(string jwtToken);
    }
}
