﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using DfT.GOS.GhgApplications.Common.Models;

namespace DfT.GOS.GhgApplications.API.Client.Models
{
    /// <summary>
    /// Supporting Document Upload
    /// </summary>
    public class SupportingDocumentUpload
    {
        /// <summary>
        /// Gets or sets the upload status.
        /// </summary>
        /// <value>
        /// The upload status.
        /// </value>
        public string UploadStatus { get; set; }

        /// <summary>
        /// Gets or sets the scan result.
        /// </summary>
        /// <value>
        /// The scan result.
        /// </value>
        public ScanResult ScanResult { get; set; }

        /// <summary>
        /// Gets or sets the messages.
        /// </summary>
        /// <value>
        /// The messages.
        /// </value>
        public List<string> Messages { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public SupportingDocumentUpload()
        {
            this.Messages = new List<string>();
        }

    }
}
