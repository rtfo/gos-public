﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.EntityFrameworkCore.Migrations;

namespace DfT.GOS.GhgApplications.API.Migrations
{
    /// <summary>
    /// Database migration for UER changes
    /// </summary>
    public partial class UER_Reference : Migration
    {
        /// <summary>
        /// Apply database changes for UERs
        /// </summary>
        /// <param name="migrationBuilder">MigrationBuilder instance</param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Reference",
                table: "ApplicationItem",
                nullable: true);
        }

        /// <summary>
        /// Rollback database changes for UERs
        /// </summary>
        /// <param name="migrationBuilder">MigrationBuilder instance</param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Reference",
                table: "ApplicationItem");
        }
    }
}
