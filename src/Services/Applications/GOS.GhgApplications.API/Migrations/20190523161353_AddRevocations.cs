﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DfT.GOS.GhgApplications.API.Migrations
{
    /// <summary>
    /// Migration class for Applications database.
    /// Adds fields related to revocations
    /// </summary>
    public partial class AddRevocations : Migration
    {
        /// <summary>
        /// Upgrades the Applications database
        /// </summary>
        /// <param name="migrationBuilder">MigrationBuilder instance</param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RevokedBy",
                table: "Applications",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RevokedDate",
                table: "Applications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RevocationReason",
                table: "Applications",
                nullable: true);
        }

        /// <summary>
        /// Downgrades an Applications database
        /// </summary>
        /// <param name="migrationBuilder">MigrationBuilder instance</param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RevokedBy",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "RevokedDate",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "RevocationReason",
                table: "Applications");
        }
    }
}
