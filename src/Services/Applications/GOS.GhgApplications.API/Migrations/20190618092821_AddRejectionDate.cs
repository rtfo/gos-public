﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DfT.GOS.GhgApplications.API.Migrations
{
    public partial class AddRejectionDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RejectedBy",
                table: "Applications",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RejectedDate",
                table: "Applications",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RejectedBy",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "RejectedDate",
                table: "Applications");
        }
    }
}
