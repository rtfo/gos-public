﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DfT.GOS.GhgApplications.API.Migrations
{
    /// <summary>
    /// EF Migration for Applications database
    /// </summary>
    public partial class ApplicationStatus : Migration
    {
        /// <summary>
        /// Upgrade an existing Applications database
        /// </summary>
        /// <param name="migrationBuilder">MigrationBuilder instance</param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ApplicationStatus",
                columns: table => new
                {
                    ApplicationStatusId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationStatus", x => x.ApplicationStatusId);
                });

            migrationBuilder.CreateTable(
                name: "ReviewStatus",
                columns: table => new
                {
                    ReviewStatusId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReviewStatus", x => x.ReviewStatusId);
                });

            migrationBuilder.CreateTable(
                name: "SupportingDocuments",
                columns: table => new
                {
                    SupportingDocumentId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ApplicationId = table.Column<int>(nullable: false),
                    File = table.Column<byte[]>(nullable: false),
                    FileName = table.Column<string>(nullable: false),
                    DocumentTypeId = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportingDocuments", x => x.SupportingDocumentId);
                });

            migrationBuilder.CreateTable(
                name: "Applications",
                columns: table => new
                {
                    ApplicationId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ApplicationStatusId = table.Column<int>(nullable: false),
                    ObligationPeriodId = table.Column<int>(nullable: false),
                    SupplierOrganisationId = table.Column<int>(nullable: false),
                    ApplicationVolumesReviewStatusReviewStatusId = table.Column<int>(nullable: false),
                    ApplicationVolumesRejectionReason = table.Column<string>(nullable: true),
                    SupportingDocumentsReviewStatusReviewStatusId = table.Column<int>(nullable: false),
                    SupportingDocumentsRejectionReason = table.Column<string>(nullable: true),
                    CalculationReviewStatusReviewStatusId = table.Column<int>(nullable: false),
                    CalculationRejectionReason = table.Column<string>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: true),
                    ApprovedBy = table.Column<string>(nullable: true),
                    IssuedDate = table.Column<DateTime>(nullable: true),
                    IssuedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: false),
                    SubmittedDate = table.Column<DateTime>(nullable: true),
                    SubmittedBy = table.Column<string>(nullable: true),
                    RecommendedDate = table.Column<DateTime>(nullable: true),
                    RecommendedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applications", x => x.ApplicationId);
                    table.ForeignKey(
                        name: "FK_Applications_ApplicationStatus_ApplicationStatusId",
                        column: x => x.ApplicationStatusId,
                        principalTable: "ApplicationStatus",
                        principalColumn: "ApplicationStatusId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Applications_ReviewStatus_ApplicationVolumesReviewStatusRev~",
                        column: x => x.ApplicationVolumesReviewStatusReviewStatusId,
                        principalTable: "ReviewStatus",
                        principalColumn: "ReviewStatusId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Applications_ReviewStatus_CalculationReviewStatusReviewStat~",
                        column: x => x.CalculationReviewStatusReviewStatusId,
                        principalTable: "ReviewStatus",
                        principalColumn: "ReviewStatusId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Applications_ReviewStatus_SupportingDocumentsReviewStatusRe~",
                        column: x => x.SupportingDocumentsReviewStatusReviewStatusId,
                        principalTable: "ReviewStatus",
                        principalColumn: "ReviewStatusId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationItem",
                columns: table => new
                {
                    ApplicationItemId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ApplicationId = table.Column<int>(nullable: false),
                    FuelTypeId = table.Column<int>(nullable: false),
                    AmountSupplied = table.Column<decimal>(nullable: false),
                    GhgIntensity = table.Column<decimal>(nullable: true),
                    EnergyDensity = table.Column<decimal>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationItem", x => x.ApplicationItemId);
                    table.ForeignKey(
                        name: "FK_ApplicationItem_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "ApplicationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationItem_ApplicationId",
                table: "ApplicationItem",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_ApplicationStatusId",
                table: "Applications",
                column: "ApplicationStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_ApplicationVolumesReviewStatusReviewStatusId",
                table: "Applications",
                column: "ApplicationVolumesReviewStatusReviewStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_CalculationReviewStatusReviewStatusId",
                table: "Applications",
                column: "CalculationReviewStatusReviewStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_SupportingDocumentsReviewStatusReviewStatusId",
                table: "Applications",
                column: "SupportingDocumentsReviewStatusReviewStatusId");
        }

        /// <summary>
        /// Downgrades an existing Applications database
        /// </summary>
        /// <param name="migrationBuilder">MigrationBuilder instance</param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationItem");

            migrationBuilder.DropTable(
                name: "SupportingDocuments");

            migrationBuilder.DropTable(
                name: "Applications");

            migrationBuilder.DropTable(
                name: "ApplicationStatus");

            migrationBuilder.DropTable(
                name: "ReviewStatus");
        }
    }
}
