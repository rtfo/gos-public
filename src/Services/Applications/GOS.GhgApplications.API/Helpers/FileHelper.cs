﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgApplications.API.Helpers
{
    /// <summary>
    /// Provides helper methods for dealing with files
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// Gets a user friendly representation of a file size
        /// </summary>
        /// <param name="maxFileSizeBytes">The file size in bytes</param>
        /// <returns>Returns a user-friendly display file size</returns>
        public static string GetMaxFileSizeDisplay(long maxFileSizeBytes)
        {
            string displayValue;

            if (maxFileSizeBytes >= 1048576)
            {
                //Mega bytes
                displayValue = string.Format("{0:0.0}", (maxFileSizeBytes / (double)1048576)) + " MB";
            }
            else if (maxFileSizeBytes >= 1024)
            {
                //kilobyte
                displayValue = string.Format("{0:0.0}", (maxFileSizeBytes / (double)1024)) + " KB";
            }
            else
            {
                //bytes
                displayValue = maxFileSizeBytes.ToString() + " bytes";
            }

            return displayValue;
        }
    }
}
