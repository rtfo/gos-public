﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using DfT.GOS.GhgApplications.Common.Models;

namespace DfT.GOS.GhgApplications.API.Models
{
    /// <summary>
    /// Contains result of the validation process for a file that has been uploaded
    /// </summary>
    public class FileValidationResult
    {
        /// <summary>
        /// Gets or sets the upload status.
        /// </summary>
        /// <value>
        /// The upload status.
        /// </value>
        public string UploadStatus { get; set; }

        /// <summary>
        /// Gets or sets the filen name of the scanned file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the scan result.
        /// </summary>
        /// <value>
        /// The scan result.
        /// </value>
        public ScanResult ScanResult { get; set; }

        /// <summary>
        /// Gets or sets the messages.
        /// </summary>
        /// <value>
        /// The messages.
        /// </value>
        public List<string> Messages { get; set; }

        /// <summary>
        /// Extension TypeId
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public FileValidationResult()
        {
            this.Messages = new List<string>();
        }
    }
}
