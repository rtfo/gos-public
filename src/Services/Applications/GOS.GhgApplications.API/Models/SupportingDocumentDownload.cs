﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgApplications.API.Models
{
    /// <summary>
    /// Supporting Document Upload
    /// </summary>
    public class SupportingDocumentDownload
    {
        /// <summary>
        /// Gets or sets the supporting document identifier.
        /// </summary>
        /// <value>
        /// The supporting document identifier.
        /// </value>
        public int SupportingDocumentId { get; set; }

        /// <summary>
        /// Gets or sets the filen name of the scanned file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName { get; set; }

    }
}
