﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Messaging.Models;

namespace DfT.GOS.GhgApplications.API
{
    /// <summary>
    /// Application settings for the API
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// The URL for the remote GHG Fuels Service
        /// </summary>
        public string GhgFuelsServiceApiUrl { get; set; }

        /// <summary>
        /// The URL for the remote Reporting Periods Service
        /// </summary>
        public string ReportingPeriodsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the system parameters service API URL.
        /// </summary>
        public string SystemParametersServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the organisations service API URL
        /// </summary>
        public string OrganisationsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the number of months from an application submission that credits should be issued within 
        /// </summary>
        public int MonthThresholdForIssuingCredits { get; set; }

        /// <summary>
        /// HTTP Client setting for resilliant HTTP communication
        /// </summary>
        public HttpClientAppSettings HttpClient { get; set; }

        /// <summary>
        /// Gets or sets the file upload maximum size for the applications service .
        /// </summary>
        /// <value>
        /// The file upload maximum size for the applications service.
        /// </value>
        public long ApplicationsServiceFileUploadMaxSizeBytes { get; set; }

        /// <summary>
        /// RabbitMQ settings for communicating with message queues
        /// </summary>
        public RabbitMessagingClientSettings MessagingClient { get; set; }
    }
}
