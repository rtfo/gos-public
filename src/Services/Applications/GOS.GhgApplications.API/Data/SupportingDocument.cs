﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.API.Data
{
    /// <summary>
    /// Database table definition for a supporting document for an application
    /// </summary>
    public class SupportingDocument
    {
        /// <summary>
        /// The primary Key of the supporting document table
        /// </summary>
        [Key]
        [Required]
        public int SupportingDocumentId { get; set; }

        /// <summary>
        /// The application this supporting document relates to
        /// </summary>
        [Required]
        public int ApplicationId { get; set; }

        /// <summary>
        /// The file binary blob
        /// </summary>
        [Required]
        public byte[] File { get; set; }

        /// <summary>
        /// The name of the file
        /// </summary>
        [Required]
        public string FileName { get; set; }

        /// <summary>
        /// The ID of the document type for the docuement's file
        /// </summary>
        [Required]
        public int DocumentTypeId { get; set; }

        /// <summary>
        /// The date the document record was uploaded
        /// </summary>
        [Required]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// The ID of the user that the document was uploaded by
        /// </summary>
        [Required]
        public string CreatedBy { get; set; }
    }
}
