﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.API.Data
{
    /// <summary>
    /// Database table definition for an Application Review Status
    /// </summary>
    public class ReviewStatus
    {
        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ReviewStatus()
        {
        }

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        public ReviewStatus(int reviewStatusId
            , string name)
        {
            this.ReviewStatusId = reviewStatusId;
            this.Name = name;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// The Primary Key of the review status
        /// </summary>
        [Key]
        [Required]
        public int ReviewStatusId { get; set; }

        /// <summary>
        /// The name of the status
        /// </summary>
        [Required]
        public string Name { get; set; }

        #endregion Properties
    }
}
