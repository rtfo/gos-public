﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.API.Data
{
    /// <summary>
    /// Database table definition for an Application Item
    /// </summary>
    public class ApplicationItem : IEntity<int>
    {
        /// <summary>
        /// The primary key of Application Item
        /// </summary>
        [Key]
        [Required]
        public int ApplicationItemId { get; set; }

        /// <summary>
        /// The application this application item is related to
        /// </summary>
        [Required]
        public Application Application { get; set; }

        /// <summary>
        /// The ID of the application this item corresponds to
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// The ID of the fuel type this application item is reporting
        /// </summary>
        [Required]
        public int FuelTypeId { get; set; }

        /// <summary>
        /// The amount of fuel supplied
        /// </summary>
        [Required]
        public decimal AmountSupplied { get; set; }

        /// <summary>
        /// The GHG Intensity of the application item
        /// </summary>
        public Nullable<decimal> GhgIntensity { get; set; }

        /// <summary>
        /// The energy density of the application item
        /// </summary>
        public Nullable<decimal> EnergyDensity { get; set; }

        /// <summary>
        /// The reference of the application item
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// The date the application item record was created
        /// </summary>
        [Required]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// The ID of the user that the application item was created by
        /// </summary>
        [Required]
        public string CreatedBy { get; set; }

        #region IEntity Implementation
        int IEntity<int>.Id
        {
            get
            {
                return this.ApplicationItemId;
            }
            set
            {
                this.ApplicationItemId = value;
            }
        }
        #endregion IEntity Implementation
    }
}
