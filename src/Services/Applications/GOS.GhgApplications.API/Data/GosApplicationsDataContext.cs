﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.GhgApplications.API.Data
{
    /// <summary>
    /// Database context for GHG Apllications within ROS
    /// </summary>
    public class GosApplicationsDataContext : DbContext, IDbContext
    {
        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="options">context options</param>
        public GosApplicationsDataContext(DbContextOptions<GosApplicationsDataContext> options)
           : base(options)
        {
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public GosApplicationsDataContext() { }

        /// <summary>
        /// GHG Applications
        /// </summary>
        public virtual DbSet<Application> Applications { get; set; }

        /// <summary>
        /// GHG Application Statuses
        /// </summary>
        public virtual DbSet<ApplicationStatus> ApplicationStatus {get;set;}

        /// <summary>
        /// GHG Application Items
        /// </summary>
        public virtual DbSet<ApplicationItem> ApplicationItem { get; set; }

        /// <summary>
        /// GHG Application Supporting Documents
        /// </summary>
        public virtual DbSet<SupportingDocument> SupportingDocuments { get; set; }

        /// <summary>
        /// GHG Application Review Statuses
        /// </summary>
        public virtual DbSet<ReviewStatus> ReviewStatus { get; set; }
    }
}
