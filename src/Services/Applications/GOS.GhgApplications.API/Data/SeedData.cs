﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Data;
using DfT.GOS.Web.ExtensionMethods.Enum;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace DfT.GOS.Applications.API.Data
{
    /// <summary>
    /// Class used to migrate and seed the Applications Database
    /// </summary>
    public static class SeedData
    {
        #region Public Methods

        /// <summary>
        /// Initialises the database, migrating the schema and seeding with data
        /// </summary>
        /// <param name="serviceProvider">The service provider used to resolve services</param>
        public static void Initialise(IServiceProvider serviceProvider)
        {
            using (var context = serviceProvider.GetRequiredService<GosApplicationsDataContext>())
            {
                //Migrate the database (creating schema if necessary)
                context.Database.Migrate();

                CreateApplicationStatuses(context);
                CreateReviewStatuses(context);
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Creates Application Statuses
        /// <param name="context">The current db context</param>
        /// </summary>
        private static void CreateApplicationStatuses(GosApplicationsDataContext context)
        {

            CheckAndAddStatuses(context, new ApplicationStatus((int)ApplicationStatus.ApplicationStatuses.Open, ApplicationStatus.ApplicationStatuses.Open.GetDisplayName()));
            CheckAndAddStatuses(context, new ApplicationStatus((int)ApplicationStatus.ApplicationStatuses.PendingVerification, ApplicationStatus.ApplicationStatuses.PendingVerification.GetDisplayName()));
            CheckAndAddStatuses(context, new ApplicationStatus((int)ApplicationStatus.ApplicationStatuses.Submitted, ApplicationStatus.ApplicationStatuses.Submitted.GetDisplayName()));
            CheckAndAddStatuses(context, new ApplicationStatus((int)ApplicationStatus.ApplicationStatuses.Approved, ApplicationStatus.ApplicationStatuses.Approved.GetDisplayName()));
            CheckAndAddStatuses(context, new ApplicationStatus((int)ApplicationStatus.ApplicationStatuses.GHGCreditsIssued, ApplicationStatus.ApplicationStatuses.GHGCreditsIssued.GetDisplayName()));
            CheckAndAddStatuses(context, new ApplicationStatus((int)ApplicationStatus.ApplicationStatuses.Rejected, ApplicationStatus.ApplicationStatuses.Rejected.GetDisplayName()));
            CheckAndAddStatuses(context, new ApplicationStatus((int)ApplicationStatus.ApplicationStatuses.RecommendApproval, ApplicationStatus.ApplicationStatuses.RecommendApproval.GetDisplayName()));
            CheckAndAddStatuses(context, new ApplicationStatus((int)ApplicationStatus.ApplicationStatuses.Revoked, ApplicationStatus.ApplicationStatuses.Revoked.GetDisplayName()));

            context.SaveChanges();

        }

        private static void CheckAndAddStatuses(GosApplicationsDataContext context, ApplicationStatus status)
        {
            if (!context.ApplicationStatus.Contains(status))
            {
                context.ApplicationStatus.Add(status);
            }
        }

        /// <summary>
        /// Creates Application Review Statuses
        /// <param name="context">The current db context</param>
        /// </summary>
        private static void CreateReviewStatuses(GosApplicationsDataContext context)
        {
            if (!context.ReviewStatus.Any())
            {
                context.ReviewStatus.Add(new ReviewStatus(1, "Not Reviewed"));
                context.ReviewStatus.Add(new ReviewStatus(2, "Approved"));
                context.ReviewStatus.Add(new ReviewStatus(3, "Rejected"));

                context.SaveChanges();
            }
        }

        #endregion Private Methods
    }
}
