﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DfT.GOS.GhgApplications.API.Data
{
    /// <summary>
    /// Database table definition for an Application
    /// </summary>
    public class Application : IEntity<int>
    {
        /// <summary>
        /// Primary key for applications
        /// </summary>
        [Key]
        [Required]
        public int ApplicationId { get; set; }

        /// <summary>
        /// Foreign key to Application Status
        /// </summary>
        [ForeignKey("ApplicationStatus")]
        public int ApplicationStatusId { get; set; }

        /// <summary>
        /// The current status of the application
        /// </summary>
        [Required]
        public ApplicationStatus ApplicationStatus { get; set; }

        /// <summary>
        /// The Application's application items
        /// </summary>
        public List<ApplicationItem> ApplicationItems { get; set; }

        /// <summary>
        /// The ID for the obligation period this application relates to
        /// </summary>
        [Required]
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// The organisation ID of the supplier that this application is for
        /// </summary>
        [Required]
        public int SupplierOrganisationId { get; set; }

        /// <summary>
        /// The review status of the application volumes section
        /// </summary>
        [Required]
        public ReviewStatus ApplicationVolumesReviewStatus { get; set; }

        /// <summary>
        /// The rejection reason, if application, for the volumes section of the application
        /// </summary>
        public string ApplicationVolumesRejectionReason { get; set; }

        /// <summary>
        /// The review status of the supporting documents section
        /// </summary>
        [Required]
        public ReviewStatus SupportingDocumentsReviewStatus { get; set; }

        /// <summary>
        /// The rejection reason, if application, for the supporting documents section of the application
        /// </summary>
        public string SupportingDocumentsRejectionReason { get; set; }

        /// <summary>
        /// The review status of the calculation section
        /// </summary>
        [Required]
        public ReviewStatus CalculationReviewStatus { get; set; }

        /// <summary>
        /// The rejection reason, if application, for the calculations section of the application
        /// </summary>
        public string CalculationRejectionReason { get; set; }

        /// <summary>
        /// The date the application is approved
        /// </summary>
        public Nullable<DateTime> ApprovedDate { get; set; }

        /// <summary>
        /// The ID of the user who approved the application
        /// </summary>
        public string ApprovedBy { get; set; }

        /// <summary>
        /// The date credits were issued against the application
        /// </summary>
        public Nullable<DateTime> IssuedDate { get; set; }

        /// <summary>
        /// The ID of the user that credits were issued by
        /// </summary>
        public string IssuedBy { get; set; }

        /// <summary>
        /// The date the application record was created
        /// </summary>
        [Required]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// The ID of the user that the application was created by
        /// </summary>
        [Required]
        public string CreatedBy { get; set; }

        /// <summary>
        /// The date the application record was submitted
        /// </summary>
        public Nullable<DateTime> SubmittedDate { get; set; }

        /// <summary>
        /// The ID of the user that the application was submitted by
        /// </summary>
        public string SubmittedBy { get; set; }

        /// <summary>
        /// The date the application was recommended for approval
        /// </summary>
        public Nullable<DateTime> RecommendedDate { get; set; }

        /// <summary>
        /// The ID of the user that the application was recommended by
        /// </summary>
        public string RecommendedBy { get; set; }

        /// <summary>
        /// Date that the application was revoked, if it has been
        /// </summary>
        public DateTime? RevokedDate { get; set; }

        /// <summary>
        /// The ID of the user who revoked the application
        /// </summary>
        public string RevokedBy { get; set; }

        /// <summary>
        /// The reason given by the user who revoked the application
        /// </summary>
        public string RevocationReason { get; set; }

        /// <summary>
        /// Date that the application was rejected, if it has been
        /// </summary>
        public Nullable<DateTime> RejectedDate { get; set; }

        /// <summary>
        /// The ID of the user who rejected the application
        /// </summary>
        public string RejectedBy { get; set; }

        #region IEntity Implementation

        int IEntity<int>.Id
        {
            get
            {
                return this.ApplicationId;
            }
            set
            {
                this.ApplicationId = value;
            }
        }

        #endregion IEntity Implementation
    }
}
