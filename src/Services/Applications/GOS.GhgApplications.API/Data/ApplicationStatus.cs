﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.API.Data
{
    /// <summary>
    /// Database table definition for an Application Status
    /// </summary>
    public class ApplicationStatus
    {
        /// <summary>
        /// Application Statuses
        /// </summary>
        public enum ApplicationStatuses
        {
            /// <summary>
            /// Application is Open
            /// </summary>
            [Display(Name = "Open")]
            Open = 1,

            /// <summary>
            /// Application Pending Verification
            /// </summary>
            [Display(Name = "Pending Verification")]
            PendingVerification = 2,

            /// <summary>
            /// Application is Submitted
            /// </summary>
            [Display(Name = "Submitted")]
            Submitted = 3,

            /// <summary>
            /// Application is Approved
            /// </summary>
            [Display(Name = "Approved")]
            Approved = 4,

            /// <summary>
            /// Application has GHG Credits Issued
            /// </summary>
            [Display(Name = "Issued")]
            GHGCreditsIssued = 6,

            /// <summary>
            /// Application is Rejected
            /// </summary>
            [Display(Name = "Rejected")]
            Rejected = 7,

            /// <summary>
            /// Application is Recommended for approval
            /// </summary>
            [Display(Name = "Recommended for approval")]
            RecommendApproval = 8,

            /// <summary>
            /// Application has been Revoked
            /// </summary>
            [Display(Name = "Revoked")]
            Revoked = 9
        }

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ApplicationStatus()
        {
        }

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        /// <param name="applicationStatusId">The ID of the application status</param>
        /// <param name="name">The name of the application status</param>
        public ApplicationStatus(int applicationStatusId, string name)
        {
            this.ApplicationStatusId = applicationStatusId;
            this.Name = name;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// The primary key of application status
        /// </summary>
        [Key]
        [Required]
        public int ApplicationStatusId { get; set; }

        /// <summary>
        /// The name of the status
        /// </summary>
        [Required]
        public string Name { get; set; }

        #endregion Properties
    }
}
