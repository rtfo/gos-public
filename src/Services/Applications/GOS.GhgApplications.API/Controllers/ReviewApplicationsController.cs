﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Threading.Tasks;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.API.Commands;
using DfT.GOS.GhgApplications.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static DfT.GOS.GhgApplications.API.Data.ApplicationStatus;

namespace GOS.GhgApplications.API.Controllers
{
    /// <summary>
    /// GHG Application review process related APIs
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/Review/Applications")]
    [Authorize(Roles = Roles.Administrator, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ReviewApplicationsController : ControllerBase
    {
        #region Private Members
        private IApplicationsService ApplicationsService { get; set; }
        #endregion Private Members

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationsService"></param>
        public ReviewApplicationsController(IApplicationsService applicationsService)
        {
            this.ApplicationsService = applicationsService;
        }
        #endregion Constructors

        /// <summary>
        /// Gets the application details for review
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <returns></returns>
        [HttpGet("{applicationId}")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> GetApplicationReviewSummary(int applicationId)
        {
            var reviewSummary = await this.ApplicationsService.GetApplicationReviewSummary(applicationId);
            if (reviewSummary == null)
            {
                return NotFound("Application not found for ID :" + applicationId);
            }
            return Ok(reviewSummary);
        }

        /// <summary>
        /// Review application volumes
        /// </summary>
        /// <param name="applicationId">The ID of the Application</param>
        /// <param name="command">The command for reviewing an application's volumes</param>
        [HttpPost("{applicationId}")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> ReviewApplication(int applicationId, [FromBody]ReviewApplicationCommand command)
        {
            if (ModelState.IsValid)
            {
                if (applicationId != command.ApplicationId)
                {
                    return BadRequest("Application ID parameters in command and URL do not match");
                }
                else
                {
                    var applicationSummary = await this.ApplicationsService.GetApplicationReviewSummary(command.ApplicationId);

                    if (applicationSummary == null)
                    {
                        return NotFound("Application not found");
                    }
                    else if (applicationSummary.ApplicationStatusId != (int)ApplicationStatuses.Submitted)
                    {
                        // The application isn't in the right state, a change to review can only happen if the application is in submitted state
                        return BadRequest();
                    }
                    else if (this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                    {
                        var success = await this.ApplicationsService.ReviewApplication(command);
                        return Ok(success);
                    }
                    else
                    {
                        return Forbid();
                    }
                }
            }
            else
            {
                return BadRequest();
            }
        }
        /// <summary>
        /// Review application volumes
        /// </summary>
        /// <param name="applicationId">The ID of the Application</param>
        /// <param name="command">The command for reviewing an application's volumes</param>
        [HttpPost("{applicationId}/Volumes")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> ReviewApplicationVolumes(int applicationId, [FromBody]ReviewApplicationVolumesCommand command)
        {
            if (ModelState.IsValid)
            {
                if (applicationId != command.ApplicationId)
                {
                    return BadRequest("Application ID parameters in command and URL do not match");
                }
                else
                {
                    var applicationSummary = await this.ApplicationsService.GetApplicationSummary(command.ApplicationId);

                    if (applicationSummary == null)
                    {
                        return NotFound("Application not found");
                    }
                    else if (applicationSummary.ApplicationStatusId != (int)ApplicationStatuses.Submitted)
                    {
                        // The application isn't in the right state, a change to review can only happen if the application is in submitted state
                        return BadRequest();
                    }
                    else if (this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                    {
                        var success = await this.ApplicationsService.ReviewApplicationVolumes(command);
                        return Ok(success);
                    }
                    else
                    {
                        return Forbid();
                    }
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Review application Supporting Documents
        /// </summary>
        /// <param name="applicationId">The ID of the Application</param>
        /// <param name="command">The command for reviewing an application's supporting documents</param>
        [HttpPost("{applicationId}/SupportingDocuments")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> ReviewApplicationSupportingDocuments(int applicationId, [FromBody]ReviewApplicationSupportingDocumentsCommand command)
        {
            if (ModelState.IsValid)
            {
                if (applicationId != command.ApplicationId)
                {
                    return BadRequest("Application ID parameters in command and URL do not match");
                }
                else
                {
                    var applicationSummary = await this.ApplicationsService.GetApplicationSummary(command.ApplicationId);

                    if (applicationSummary == null)
                    {
                        return NotFound("Application not found");
                    }
                    else if (applicationSummary.ApplicationStatusId != (int)ApplicationStatuses.Submitted)
                    {
                        // The application isn't in the right state, a change to review can only happen if the application is in submitted state
                        return BadRequest();
                    }
                    else if (this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                    {
                        var success = await this.ApplicationsService.ReviewApplicationSupportingDocuments(command);
                        return Ok(success);
                    }
                    else
                    {
                        return Forbid();
                    }
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Review application Calculation
        /// </summary>
        /// <param name="applicationId">The ID of the Application</param>
        /// <param name="command">The command for reviewing an application's calculation</param>
        [HttpPost("{applicationId}/Calculation")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> ReviewApplicationCalculation(int applicationId, [FromBody]ReviewApplicationCalculationCommand command)
        {
            if (ModelState.IsValid)
            {
                if (applicationId != command.ApplicationId)
                {
                    return BadRequest("Application ID parameters in command and URL do not match");
                }
                else
                {
                    var applicationSummary = await this.ApplicationsService.GetApplicationSummary(command.ApplicationId);

                    if (applicationSummary == null)
                    {
                        return NotFound("Application not found");
                    }
                    else if (applicationSummary.ApplicationStatusId != (int)ApplicationStatuses.Submitted)
                    {
                        // The application isn't in the right state, a change to review can only happen if the application is in submitted state
                        return BadRequest();
                    }
                    else if (this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                    {
                        var success = await this.ApplicationsService.ReviewApplicationCalculation(command);
                        return Ok(success);
                    }
                    else
                    {
                        return Forbid();
                    }
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Approve application
        /// </summary>
        /// <param name="applicationId">The ID of the Application</param>
        /// <param name="command">The command for approving an application</param>
        [HttpPost("{applicationId}/Approve")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> ApproveApplication(int applicationId, [FromBody]ApproveApplicationCommand command)
        {
            if (ModelState.IsValid)
            {
                if (applicationId != command.ApplicationId)
                {
                    return BadRequest("Application ID parameters in command and URL do not match");
                }
                else
                {
                    var applicationSummary = await this.ApplicationsService.GetApplicationReviewSummary(command.ApplicationId);

                    if (applicationSummary == null)
                    {
                        return NotFound("Application not found");
                    }
                    else if (applicationSummary.ApplicationStatusId != (int)ApplicationStatuses.RecommendApproval)
                    {
                        // The application isn't in the right state, a change to review can only happen if the application is in Recommended for Approval state
                        return BadRequest("The application is in the incorrect state for approval");
                    }
                    else if (applicationSummary.RecommendedBy == this.User.GetUserId())
                    {
                        //The application can't be approved by the same administrator who recommended it for approval
                        return BadRequest("The application can't be approved by the same administrator who recommended it for approval");
                    }
                    else if (this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                    {
                        var success = await this.ApplicationsService.ApproveApplication(command, this.User.GetUserId());
                        return Ok(success);
                    }
                    else
                    {
                        return Forbid();
                    }
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Reject application
        /// </summary>
        /// <param name="applicationId">The ID of the Application</param>
        /// <param name="command">The command for rejecting an application</param>
        [HttpPost("{applicationId}/Reject")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> RejectApplication(int applicationId, [FromBody]RejectApplicationCommand command)
        {
            if (ModelState.IsValid)
            {
                if (applicationId != command.ApplicationId)
                {
                    return BadRequest("Application ID parameters in command and URL do not match");
                }
                else
                {
                    var applicationSummary = await this.ApplicationsService.GetApplicationReviewSummary(command.ApplicationId);

                    if (applicationSummary == null)
                    {
                        return NotFound("Application not found");
                    }
                    else if (applicationSummary.ApplicationStatusId != (int)ApplicationStatuses.RecommendApproval)
                    {
                        // The application isn't in the right state, a change to review can only happen if the application is in Recommended for Approval state
                        return BadRequest("The application is in the incorrect state for rejection");
                    }
                    else if (applicationSummary.RecommendedBy == this.User.GetUserId())
                    {
                        //The application can't be rejected by the same administrator who recommended it for approval
                        return BadRequest("The application can't be rejected by the same administrator who recommended it for approval");
                    }
                    else if (this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                    {
                        var success = await this.ApplicationsService.RejectApplication(command, this.User.GetUserId());
                        return Ok(success);
                    }
                    else
                    {
                        return Forbid();
                    }
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Issues credits for the aproved applications
        /// </summary>
        /// <returns></returns>
        [HttpPost("issuecredits")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> IssueCredits()
        {
            var success = await this.ApplicationsService.IssueCredits(
                new IssueCreditCommand() { IssuedBy = this.User.GetUserId(), IssuedDate = DateTime.Now });
            return Ok(success);
        }
    }
}