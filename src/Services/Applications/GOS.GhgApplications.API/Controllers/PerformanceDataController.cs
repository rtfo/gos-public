﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace GOS.GhgApplications.API.Controllers
{
    /// <summary>
    /// Gets GHG Application Details
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PerformanceDataController : Controller
    {
        #region Properties
        private IApplicationsService ApplicationsService { get; set; }
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependencies
        /// </summary>
        /// <param name="applicationsService">The applications service</param>
        public PerformanceDataController(IApplicationsService applicationsService)
        {
            this.ApplicationsService = applicationsService;
        }
        #endregion Constructors

        #region Methods

        /// <summary>
        /// Returns the performance data relating to applications
        /// </summary>
        /// <remarks>
        ///  - No. of applications with credits issued for each calendar month
        ///  - No. of incomplete applications for each calendar month
        ///  - No. of applications rejected each calendar month
        ///  - No. of applications revoked each calendar month
        ///  - No. of applications submitted for each calendar month
        ///  - No. of applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently)
        ///  - Total No. of applications for the calendar month
        /// </remarks>
        /// <returns>JSON array of objects for the applications performance data</returns>
        [Route("Applications")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> ApplicationPerformanceData()
        {
            var applicationPerformanceData = await ApplicationsService.GetApplicationPerformanceData();
            return new OkObjectResult(applicationPerformanceData);
        }
        #endregion Methods
    }
}
