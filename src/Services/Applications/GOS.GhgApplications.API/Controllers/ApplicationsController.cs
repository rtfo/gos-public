﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Commands;
using DfT.GOS.GhgApplications.API.Services;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.Common.Exceptions;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Claims;
using DfT.GOS.SystemParameters.API.Client.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DfT.GOS.GhgApplications.API.Data.ApplicationStatus;

namespace GOS.GhgApplications.API.Controllers
{
    /// <summary>
    /// Gets GHG Application Details
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/Applications")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ApplicationsController : ControllerBase
    {
        #region Properties
        private IApplicationsService ApplicationsService { get; set; }
        private IScanVirusService ScanViruses { get; set; }
        private ISystemParametersService SystemParametersService { get; set; }
        private ISupportingDocumentParametersService SupportingDocumentParametersService { get; set; }
        private IOrganisationsService OrganisationsService { get; set; }
        private IGhgFuelsService GhgFuelsService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependancies
        /// </summary>
        /// <param name="applicationsService">The service</param>
        /// <param name="scanViruses">The virus scanner service to use</param>
        /// <param name="systemParametersService">The system parameter service</param>
        /// <param name="supportingDocumentParametersService">The service for getting document parameter info</param>
        /// <param name="organisationsService">The service for getting information about organisations</param>
        /// <param name="ghgFuelsService">The service for getting the fuel types</param>
        /// <param name="reportingPeriodsService">The service for getting obligation period</param>
        public ApplicationsController(IApplicationsService applicationsService
            , IScanVirusService scanViruses
            , ISystemParametersService systemParametersService
            , ISupportingDocumentParametersService supportingDocumentParametersService
            , IOrganisationsService organisationsService
            , IGhgFuelsService ghgFuelsService
            , IReportingPeriodsService reportingPeriodsService)
        {
            this.ApplicationsService = applicationsService;
            this.ScanViruses = scanViruses;
            this.SystemParametersService = systemParametersService;
            this.SupportingDocumentParametersService = supportingDocumentParametersService;
            this.OrganisationsService = organisationsService;
            this.GhgFuelsService = ghgFuelsService;
            this.ReportingPeriodsService = reportingPeriodsService;
        }

        #endregion Constructors

        #region Methods       

        /// <summary>
        /// Creates a new GHG application
        /// </summary>
        /// <param name="command">The command for creating an application</param>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]CreateApplicationCommand command)
        {
            if (ModelState.IsValid)
            {
                var auth = HttpContext.Request.Headers["Authorization"];
                IActionResult result;
                result = await ValidateCreateCommand(command, auth.ToString());
                if (result != null)
                {
                    return result;
                }

                if (this.User.CanAccessOrganisationDetails(command.SupplierOrganisationId))
                {
                    var organisationResult = await this.OrganisationsService.GetOrganisation(command.SupplierOrganisationId, auth.ToString().Replace("Bearer ", ""));

                    if (organisationResult.HasResult
                        && organisationResult.Result.IsSupplier)
                    {
                        var applicationId = await this.ApplicationsService.CreateApplication(command, this.User.GetUserId());
                        result = Created(string.Format("applications/{0}", applicationId.ToString()), applicationId);
                    }
                    else
                    {
                        result = BadRequest("Invalid organisation id");
                    }
                }
                else
                {
                    result = Forbid();
                }

                return result;
            }
            else
            {
                var error = ModelState.Keys
                    .Aggregate(new StringBuilder(), (sb, k) => sb.AppendFormat("The value supplied for '{0}' is invalid. ", k), sb => sb.ToString());
                return BadRequest(error);
            }
        }

        /// <summary>
        /// Gets an application summary by application ID
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <returns>Returns summary details about an application</returns>
        [HttpGet("{applicationId}")]
        public async Task<IActionResult> GetApplicationSummary(int applicationId)
        {
            var applicationSummary = await this.ApplicationsService.GetApplicationSummary(applicationId);

            if (applicationSummary == null)
            {
                return NotFound("Application not found");
            }
            else if (this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
            {
                return Ok(applicationSummary);
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Gets application items for an application
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <returns>Returns summary details about an application</returns>
        [HttpGet("{applicationId}/items")]
        public async Task<IActionResult> GetApplicationItems(int applicationId)
        {
            var application = await this.ApplicationsService.GetApplicationSummary(applicationId);
            if (application != null)
            {
                if (this.User.CanAccessOrganisationDetails(application.SupplierOrganisationId))
                {
                    var applicationSummary = await this.ApplicationsService.GetApplicationItems(applicationId);
                    return Ok(applicationSummary);
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Gets all application items that have had GHG credits issued for a company within an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>Returns the application items</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/IssuedCreditsApplicationItems")]
        public async Task<IActionResult> GetApplicationItemsWithIssuedCredits(int organisationId, int obligationPeriodId)
        {
            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var applicationSummary = await this.ApplicationsService.GetApplicationItemsWithIssuedCredits(organisationId, obligationPeriodId);
                return Ok(applicationSummary);
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Get the supporting document for the application.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <returns>Returns the supporting documents for an application</returns>
        [HttpGet("{applicationId}/supportingdocuments")]
        public async Task<IActionResult> SupportingDocuments(int applicationId)
        {
            var applicationSummary = await this.ApplicationsService.GetApplicationSummary(applicationId);
            if (applicationSummary == null)
            {
                return NotFound("Application not found");
            }
            if (!this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
            {
                return Forbid();
            }
            var result = await this.ApplicationsService.GetUploadedSupportingDocuments(applicationId);
            return Ok(result);
        }

        /// <summary>
        /// Upload Supporting document.
        /// </summary>
        /// <param name="supportingDocument">The supporting document.</param>
        /// <param name="applicationId">The application identifier.</param>
        /// <returns>OK with the result or Bad Request</returns>
        [HttpPost("{applicationId}/supportingdocuments")]
        public async Task<IActionResult> SupportingDocuments([FromBody]AddSupportingDocumentCommand supportingDocument,
            int applicationId)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                var applicationSummary = await this.ApplicationsService.GetApplicationSummary(applicationId);
                if (applicationSummary == null)
                {
                    return NotFound("Application not found");
                }
                if (!this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                {
                    return Forbid();
                }
                if (applicationSummary.ApplicationStatusId != (int)ApplicationStatus.Open)
                {
                    message = "Cannot modify submitted application";
                }
                var fileMetadata = new FileMetadata
                {
                    FileExtension = supportingDocument.FileExtension,
                    FileName = supportingDocument.FileName,
                    FileSizeBytes = supportingDocument.File.Length
                };

                var validationResult = await this.ApplicationsService.ValidateFileMetadata(fileMetadata);

                validationResult = validationResult.Messages.Count == 0 ?
                    await this.ApplicationsService.UploadSupportingDocument(supportingDocument,
                    validationResult.TypeId,
                    applicationId,
                    this.User.GetUserId()) : validationResult;
                if (validationResult != null && validationResult.Messages.Count > 0)
                {
                    message = message.Length == 0 ? string.Join(',', validationResult.Messages) : message;
                }
                if (message.Length == 0)
                {
                    return Ok(validationResult);
                }
            }

            return BadRequest(message);
        }

        /// <summary>
        /// Validate Supporting Document
        /// </summary>
        /// <param name="fileMetadata">Meta data for the file we're uploading</param>
        /// <returns>List of Validation Messages</returns>
        [HttpPost("ValidateFileMetadata")]
        public async Task<IActionResult> ValidateFileMetadata([FromBody]FileMetadata fileMetadata)
        {
            if (ModelState.IsValid)
            {
                var result = await this.ApplicationsService.ValidateFileMetadata(fileMetadata);
                if (result != null)
                {
                    return Ok(result);
                }
            }
            return BadRequest();
        }

        /// <summary>
        /// Get the application supporting the documents.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumentId">The supporting document.</param>
        /// <returns>
        /// OK with SupportingDocumentId, File
        /// or BadRequest
        /// </returns>
        [HttpGet("{applicationId}/supportingdocuments/{supportingDocumentId}")]
        public async Task<IActionResult> SupportingDocument(int applicationId, int supportingDocumentId)
        {
            if (ModelState.IsValid)
            {
                var applicationSummary = await this.ApplicationsService.GetApplicationSummary(applicationId);
                if (applicationSummary == null)
                {
                    return NotFound("Application not found");
                }
                if (!this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                {
                    return Forbid();
                }

                var result = await this.ApplicationsService.GetUploadedSupportingDocument(applicationId, supportingDocumentId);
                return Ok(result);
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Remove Supporting document.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumentId">The supporting document.</param>
        /// <returns>OK true/false of Bad request</returns>
        [HttpDelete("{applicationId}/supportingdocuments/{supportingDocumentId}/remove")]
        public async Task<IActionResult> RemoveSupportingDocument(int applicationId, int supportingDocumentId)
        {
            if (ModelState.IsValid)
            {
                var applicationSummary = await this.ApplicationsService.GetApplicationSummary(applicationId);
                if (applicationSummary == null)
                {
                    return NotFound("Application not found");
                }
                if (!this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                {
                    return Forbid();
                }
                if (applicationSummary.ApplicationStatusId != (int)ApplicationStatus.Open)
                {
                    return BadRequest("Cannot modify submitted application");
                }
                var document = await this.ApplicationsService.GetUploadedSupportingDocument(applicationId, supportingDocumentId);
                if (document == null)
                {
                    return NotFound("Document not found");
                }

                var result = await this.ApplicationsService.RemoveSupportingDocument(applicationId, supportingDocumentId);
                return Ok(result);
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Submits a GHG application
        /// </summary>
        /// <param name="command">The command for submitting an application</param>
        [HttpPost("Submit")]
        public async Task<IActionResult> SubmitApplication([FromBody]SubmitApplicationCommand command)
        {
            if (ModelState.IsValid)
            {
                var application = await this.ApplicationsService.GetApplicationSummary(command.ApplicationId);
                if (application != null)
                {
                    if (this.User.CanAccessOrganisationDetails(application.SupplierOrganisationId))
                    {
                        string result = null;
                        if (application.ApplicationStatusId != (int)ApplicationStatus.Open)
                        {
                            result = "Application has already been submitted.";
                        }
                        result = result ?? await this.ApplicationsService.SubmitApplication(command.ApplicationId, this.User.GetUserId());
                        if (!string.IsNullOrEmpty(result))
                        {
                            return BadRequest(result);
                        }
                        return Ok(result);
                    }
                    else
                    {
                        return Forbid();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Gets the applications that are ready for review 
        /// </summary>
        /// <param name="applicationParameters">Parameters to filter and sort the applications by</param>
        /// <returns>Paged list of application summaries</returns>
        [HttpGet("PendingApproval")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> GetPendingApplicationPagedResult(PendingApplicationParameters applicationParameters)
        {
            var applications = await this.ApplicationsService.GetPendingApplicationPagedResult(applicationParameters);
            return Ok(applications);
        }

        /// <summary>
        /// Gets the applications specific to the supplier/organisation and obligation period
        /// </summary>
        /// <param name="organisationId">The supplier/organisation id</param>
        /// <param name="obligationId">The obligation period id</param>
        /// <param name="pageSize">No. of records per page</param>
        /// <param name="pageNumber">current page</param>
        /// <returns></returns>
        [HttpGet("supplier/{organisationId}/obligationPeriod/{obligationId}")]
        public async Task<IActionResult> GetSupplierApplicationPagedResult(int organisationId, int obligationId
            , int pageSize, int pageNumber)
        {
            var auth = HttpContext.Request.Headers["Authorization"];
            if (!this.User.CanAccessOrganisationDetails(organisationId))
            {
                return Forbid();
            }
            var organisation = await this.OrganisationsService.GetOrganisation(organisationId, auth.ToString().Replace("Bearer ", ""));
            if (!organisation.HasResult)
            {
                return NotFound("Organisation not found");
            }
            if (obligationId == 0)
            {
                var obligation = await this.ReportingPeriodsService.GetCurrentObligationPeriod();
                if (obligation == null)
                {
                    return NotFound("Obligation period not found");
                }
                var applications = await this.ApplicationsService.GetSupplierApplications(organisationId, obligation.Id, pageSize, pageNumber);
                return Ok(applications);
            }
            else
            {
                var obligation = await this.ReportingPeriodsService.GetObligationPeriod(obligationId);
                if (obligation == null)
                {
                    return NotFound("Obligation period not found");
                }
                var applications = await this.ApplicationsService.GetSupplierApplications(organisationId, obligationId, pageSize, pageNumber);
                return Ok(applications);
            }
        }

        /// <summary>
        /// Gets all the applications that are ready for issue 
        /// </summary>
        /// <param name="pageSize">No. of records per page</param>
        /// <param name="pageNumber">current page</param>
        /// <returns>List of application summary</returns>
        [HttpGet("ReadyForIssue")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> GetApplicationsReadyForIssuePagedResult(int pageSize, int pageNumber)
        {
            var applications = await this.ApplicationsService.GetApplicationsReadyForIssuePagedResult(pageSize, pageNumber);
            return Ok(applications);
        }

        /// <summary>
        /// Review application volumes
        /// </summary>
        /// <param name="command">The command for reviewing an application's volumes</param>
        [HttpPost("Review/ApplicationVolumes")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> ReviewApplicationVolumes([FromBody]ReviewApplicationVolumesCommand command)
        {
            if (ModelState.IsValid)
            {
                var applicationSummary = await this.ApplicationsService.GetApplicationSummary(command.ApplicationId);

                if (applicationSummary == null)
                {
                    return NotFound("Application not found");
                }
                else if (applicationSummary.ApplicationStatusId != (int)ApplicationStatuses.Submitted)
                {
                    // The application isn't in the right state, a change to review can only happen if the application is in submitted state
                    return BadRequest("Can only review submitted Applications");
                }
                else if (this.User.CanAccessOrganisationDetails(applicationSummary.SupplierOrganisationId))
                {
                    var success = await this.ApplicationsService.ReviewApplicationVolumes(command);
                    return Ok(success);
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Revokes the specified application
        /// </summary>
        /// <param name="command">JSON body containing applicationId and reason for the revoke(optional)</param>
        /// <returns>Response on success of revoke action</returns>
        [HttpPost("revoke")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> RevokeApplication([FromBody]RevokeApplicationCommand command)
        {
            if (command == null || !ModelState.IsValid)
            {
                return BadRequest("Invalid body provided");
            }

            var userId = this.User.GetUserId();

            try
            {
                var result = await this.ApplicationsService.RevokeApplication(command, userId);
                if (result)
                {
                    return Ok("Application Revoked");
                }
            }
            catch (ApplicationNotFoundException)
            {
                return NotFound("Application not found");
            }
            catch (InvalidApplicationRevokeException badRequest)
            {
                return BadRequest(badRequest.Message);
            }
            catch (ServiceException)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            return new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }


        /// <summary>
        /// Validated the CreateApplication command object
        /// </summary>
        /// <param name="command">CreateApplicationCommand</param>
        /// <param name="authToken"></param>
        /// <returns>ActionResult</returns>
        private async Task<IActionResult> ValidateCreateCommand(CreateApplicationCommand command, string authToken)
        {
            var fuelType = await this.GhgFuelsService.GetFuelType(command.FuelTypeId);
            if (fuelType == null)
            {
                return BadRequest("Invalid fuel type");
            }
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(command.ObligationPeriodId);
            if (obligationPeriod == null)
            {
                return BadRequest("Invalid obligation period");
            }
            var supplier = await this.OrganisationsService.GetOrganisation(command.SupplierOrganisationId, authToken.Replace("Bearer ", ""));
            if (!supplier.HasResult)
            {
                return BadRequest("Invalid organisation id");
            }
            return null;
        }
        #endregion Methods
    }
}
