﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgApplications.API
{
    /// <summary>
    /// Class Library Constants
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The virus scan URL
        /// </summary>
        public const string VirusScanURL = "gos-clamav";

        /// <summary>
        /// The virus scan port
        /// </summary>
        public const int VirusScanPort = 3310;
    }
}
