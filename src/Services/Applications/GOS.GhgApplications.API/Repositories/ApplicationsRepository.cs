﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data.Repositories;
using DfT.GOS.GhgApplications.API.Data;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.Web.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Repository for GHG application details CRUD
    /// </summary>
    public class ApplicationsRepository : AsyncEntityFrameworkRepository<Application, int>, IApplicationsRepository
    {
        /// <summary>
        /// DataContext which represents the GosApplicationsDataContext
        /// </summary>
        private new GosApplicationsDataContext DataContext { get; set; }

        #region Constructors

        /// <summary>
        /// Constructor taking all arguments
        /// </summary>
        /// <param name="dataContext">The db context to use</param>
        public ApplicationsRepository(GosApplicationsDataContext dataContext)
            : base(dataContext)
        {
            this.DataContext = dataContext;

        }

        #endregion Constructors

        /// <summary>
        /// Gets a single Application, loading child properties as relevant
        /// </summary>
        /// <param name="id">The ID of the application to get</param>
        /// <returns>Returns a single application, if found</returns>
        public override async Task<Application> GetAsync(int id)
        {
            return await this.DataContext.Applications
                .Include(a => a.ApplicationStatus)
                .Include(a => a.ApplicationVolumesReviewStatus)
                .Include(a => a.SupportingDocumentsReviewStatus)
                .Include(a => a.CalculationReviewStatus)
                .SingleOrDefaultAsync(a => a.ApplicationId == id);
        }

        /// <summary>
        /// Updates the application given
        /// </summary>
        /// <param name="entity">An application to be updated</param>
        /// <returns>Returns nothing</returns>
        public override async Task UpdateAsync(Application entity)
        {
            var existing = await this.DataContext.Applications.AsNoTracking().SingleOrDefaultAsync(e => e.ApplicationId == entity.ApplicationId);

            if (existing != null)
            {
                this.DataContext.Update<Application>(entity);
            }
        }

        /// <summary>
        /// Finds the application items based on the expression
        /// </summary>
        /// <param name="match">Function to test each element for a condition</param>
        /// <returns>List of Application item</returns>
        public new Task<IList<Application>> FindAll(Expression<Func<Application, bool>> match)
        {
            return Task.Run<IList<Application>>(() =>
            {
                var response = this.DataContext.Applications.Where(match).ToList();
                return response;
            });
        }

        /// <summary>
        /// Gets the paged applications
        /// </summary>
        /// <param name="match">Filter to get the applications</param>
        /// <param name="pageSize">No. of rows per page</param>
        /// <param name="pageNumber">Current page number</param>
        /// <param name="order">Order of records, ascending or descending</param>
        /// <returns>Paged list of applications ordered by Submitted Date (descending)</returns>
        Task<PagedResult<Application>> IApplicationsRepository.GetApplicationsPagedResult(
            Expression<Func<Application, bool>> match, 
            int pageSize, int pageNumber, 
            SortOrder order = SortOrder.Decending)
        {
            if (order == SortOrder.Ascending)
            {
                return Task.Run(() =>
                {
                    var response = this.DataContext.Applications
                        .Where(match)
                        .OrderBy(app => app.SubmittedDate);
                    var pagedResult = PagedResult.GetPagedResult(response, pageSize, pageNumber);
                    return pagedResult;
                });
            }
            else
            {
                return Task.Run(() =>
                {
                    var response = this.DataContext.Applications
                        .Where(match)
                        .OrderByDescending(app => app.SubmittedDate);
                    var pagedResult = PagedResult.GetPagedResult(response, pageSize, pageNumber);
                    return pagedResult;
                });
            }
        }

        /// <summary>
        /// Date of first application
        /// </summary>
        /// <returns>Returns date of first application</returns>
        async Task<DateTime?> IApplicationsRepository.GetFirstApplicationDate()
        {
            var firstApp = await this.DataContext.Applications.OrderBy(u => u.CreatedDate).FirstOrDefaultAsync();

            if (firstApp == null)
            {
                return null;
            }

            return firstApp.CreatedDate;
        }

        /// <summary>
        /// Number of applications with credits issued for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of applications with credits issued</returns>
        async Task<int> IApplicationsRepository.GetNoOfApplicationsWithCreditsIssued(DateTime startDate, DateTime endDate)
        {
            var count = await this.DataContext.Applications
                .Where(u => u.IssuedDate >= startDate && u.IssuedDate <= endDate)
                .CountAsync();
            return count;
        }

        /// <summary>
        /// Number of incomplete applications for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of incomplete applications</returns>
        async Task<int> IApplicationsRepository.GetNoOfIncompleteApplications(DateTime startDate, DateTime endDate)
        {
            var count = await this.DataContext.Applications
                .Where(a => (a.ApplicationStatusId == (int)ApplicationStatus.ApplicationStatuses.Open ||
                                a.ApplicationStatusId == (int)ApplicationStatus.ApplicationStatuses.PendingVerification)
                            && (a.CreatedDate >= startDate && a.CreatedDate <= endDate))
                .CountAsync();
            return count;
        }

        /// <summary>
        /// Number of applications rejected for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of rejected applications</returns>
        async Task<int> IApplicationsRepository.GetNoOfRejectedApplications(DateTime startDate, DateTime endDate)
        {
            var count = await this.DataContext.Applications
                .Where(a => a.ApplicationStatusId == (int)ApplicationStatus.ApplicationStatuses.Rejected
                    && a.RejectedDate >= startDate && a.RejectedDate <= endDate)
                .CountAsync();
            return count;
        }

        /// <summary>
        /// Number of applications revoked for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of revoked applications</returns>
        async Task<int> IApplicationsRepository.GetNoOfRevokedApplications(DateTime startDate, DateTime endDate)
        {
            var count = await this.DataContext.Applications
                .Where(a => a.ApplicationStatusId == (int)ApplicationStatus.ApplicationStatuses.Revoked
                    && a.RevokedDate >= startDate && a.RevokedDate <= endDate)
                .CountAsync();
            return count;
        }

        /// <summary>
        /// Number of applications submitted for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of submitted applications</returns>
        async Task<int> IApplicationsRepository.GetNoOfSubmittedApplications(DateTime startDate, DateTime endDate)
        {
            var count = await this.DataContext.Applications
                .Where(u => u.SubmittedDate >= startDate && u.SubmittedDate <= endDate)
                .CountAsync();
            return count;
        }

        /// <summary>
        /// Number of applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently).
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="threshold">Number of months that defines the threshold</param>
        /// <returns>Returns number of applications over the threshold time between application submission and credit issue</returns>
        async Task<int> IApplicationsRepository.GetNoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue(DateTime startDate, DateTime endDate, int threshold)
        {
            // Get applications that weren't rejected, that were submitted more than the threshold months ago and haven't been issued
            // or have been issued in the given month (or after the given month) that became late in this given month.
            return await (from a in this.DataContext.Applications
                          where (
                          a.RejectedDate == null && (
                               a.SubmittedDate != null
                               && a.SubmittedDate <= endDate.AddMonths(-threshold)
                               && a.IssuedDate == null)
                          ||
                          (a.IssuedDate != null
                              && (
                                       (a.IssuedDate >= startDate && a.IssuedDate <= endDate)
                                       || a.SubmittedDate != null && a.SubmittedDate <= endDate.AddMonths(-threshold) && a.IssuedDate >= startDate
                                     )
                              && ((DateTime)a.IssuedDate).AddMonths(-threshold) > a.SubmittedDate)
                          )
                          orderby a.SubmittedDate
                          select a.ApplicationId
                               ).CountAsync();
        }

        /// <summary>
        /// Total number of applications for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns total number of applications</returns>
        async Task<int> IApplicationsRepository.GetTotalNoOfApplications(DateTime startDate, DateTime endDate)
        {
            var count = await this.DataContext.Applications
                   .Where(a => a.CreatedDate >= startDate && a.CreatedDate <= endDate)
                   .CountAsync();
            return count;
        }
    }
}
