﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data.Repositories;
using DfT.GOS.GhgApplications.API.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Definition for GHG application supporting CRUD repository
    /// </summary>
    public interface ISupportingDocumentRepository : IAsyncRepository<SupportingDocument, int>
    {
        /// <summary>
        /// Finds all supporting documents.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <returns></returns>
        Task<List<SupportingDocument>> FindAll(int applicationId);

        /// <summary>
        /// Gets a supporting document.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumentId">The supporting document identifier.</param>
        /// <returns></returns>
        Task<SupportingDocument> GetFile(int applicationId, int supportingDocumentId);

        /// <summary>
        /// Deletes the asynchronously a supporting document.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumnetId">The supporting documnet identifier.</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(int applicationId, int supportingDocumnetId);
    }
}
