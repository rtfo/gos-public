﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data.Repositories;
using DfT.GOS.GhgApplications.API.Data;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.Web.Models;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Definition for GHG application details CRUD repository
    /// </summary>
    public interface IApplicationsRepository : IAsyncRepository<Application, int>
    {
        /// <summary>
        /// Gets the paged applications
        /// </summary>
        /// <param name="match">Filter to get the applications</param>
        /// <param name="pageSize">No. of rows per page</param>
        /// <param name="pageNumber">Current page number</param>
        /// <param name="order">Order of records, ascending or descending</param>
        /// <returns>Paged list of applications ordered by Submitted Date (descending)</returns>
        Task<PagedResult<Application>> GetApplicationsPagedResult(
            Expression<Func<Application, bool>> match, int pageSize, int pageNumber, SortOrder order = SortOrder.Decending
            );

        /// <summary>
        /// Date of first application
        /// </summary>
        /// <returns>Returns date of first application</returns>
        Task<DateTime?> GetFirstApplicationDate();
        /// <summary>
        /// Number of applications with credits issued for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of applications with credits issued</returns>
        Task<int> GetNoOfApplicationsWithCreditsIssued(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Number of incomplete applications for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of incomplete applications</returns>
        Task<int> GetNoOfIncompleteApplications(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Number of applications rejected for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of rejected applications</returns>
        Task<int> GetNoOfRejectedApplications(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Number of applications revoked for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of revoked applications</returns>
        Task<int> GetNoOfRevokedApplications(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Number of applications submitted for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of submitted applications</returns>
        Task<int> GetNoOfSubmittedApplications(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Number of applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently).
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="threshold">Number of months that defines the threshold</param>
        /// <returns>Returns number of applications over the threshold time between application submission and credit issue</returns>
        Task<int> GetNoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue(DateTime startDate, DateTime endDate, int threshold);

        /// <summary>
        /// Total number of applications for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns total number of applications</returns>
        Task<int> GetTotalNoOfApplications(DateTime startDate, DateTime endDate);
    }
}
