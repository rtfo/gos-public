﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Repository for GHG application status
    /// </summary>
    public class ApplicationStatusRepository : IApplicationStatusRepository
    {
        #region Properties

        private GosApplicationsDataContext DataContext { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataContext">The database context to db operations against</param>
        public ApplicationStatusRepository(GosApplicationsDataContext dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion Constructors

        /// <summary>
        /// Gets the initial application status
        /// </summary>
        /// <returns></returns>
        Task<ApplicationStatus> IApplicationStatusRepository.GetOpenApplicationStatus()
        {
            return GetStatus((int)ApplicationStatus.ApplicationStatuses.Open);        
        }

        /// <summary>
        /// Gets the application submitted status
        /// </summary>
        /// <returns></returns>
        Task<ApplicationStatus> IApplicationStatusRepository.GetSubmittedApplicationStatus()
        {
            return GetStatus((int)ApplicationStatus.ApplicationStatuses.Submitted);
        }

        Task<ApplicationStatus> IApplicationStatusRepository.Get(int statusId)
        {
            return GetStatus(statusId);
        }

        private Task<ApplicationStatus> GetStatus(int statusId)
        {
            return Task.Run<ApplicationStatus>(() =>
            {
                return this.DataContext.ApplicationStatus
                    .Single(a => a.ApplicationStatusId == statusId);
            });
        }
    }
}
