﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Repository for GHG application section review status
    /// </summary>
    public class ReviewStatusRepository : IReviewStatusRepository
    {
        #region Constants

        private const int InitialReviewStatusId = 1;

        #endregion Constants

        #region Properties

        private GosApplicationsDataContext DataContext { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataContext">The database context to db operations against</param>
        public ReviewStatusRepository(GosApplicationsDataContext dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion Constructors

        #region IReviewStatusRepository implementation

        /// <summary>
        /// Gets the initial application section review status
        /// </summary>
        Task<ReviewStatus> IReviewStatusRepository.GetInitialReviewStatus()
        {
            return Task.Run<ReviewStatus>(() =>
            {
                return this.DataContext.ReviewStatus
                         .Single(s => s.ReviewStatusId == InitialReviewStatusId);
            });
        }

        /// <summary>
        /// Gets the review status by ID
        /// </summary>
        /// <param name="reviewStatusId">The ID of the review status</param>
        /// <returns>Returns the status that correlates to the ID</returns>
        Task<ReviewStatus> IReviewStatusRepository.Get(int reviewStatusId)
        {
            return Task.Run<ReviewStatus>(() =>
            {
                return this.DataContext.ReviewStatus
                         .Single(s => s.ReviewStatusId == reviewStatusId);
            });
        }

        #endregion IReviewStatusRepository implementation
    }
}
