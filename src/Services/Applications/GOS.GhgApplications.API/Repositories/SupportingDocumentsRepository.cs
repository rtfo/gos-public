﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Repository for GHG supporting documents
    /// </summary>
    public class SupportingDocumentsRepository : ISupportingDocumentRepository
    {
        #region Constants

        private const int InitialApplicationStatusId = 1;

        #endregion Constants

        #region Properties

        private GosApplicationsDataContext DataContext { get; set; }
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SupportingDocumentsRepository"/> class.
        /// </summary>
        /// <param name="dataContext">The data context.</param>
        public SupportingDocumentsRepository(GosApplicationsDataContext dataContext)
        {
            this.DataContext = dataContext;
        }

        /// <summary>
        /// Retrieves a Supporting Document by Id
        /// </summary>
        /// <param name="id">Id of required Supporting Document</param>
        /// <returns>Requested Supported Document, if it exists, null otherwise</returns>
        public Task<SupportingDocument> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Finds the specified match.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns></returns>
        public Task<SupportingDocument> Find(Expression<Func<SupportingDocument, bool>> match)
        {
            return Task.Run<SupportingDocument>(() =>
            {
                SupportingDocument supportingDocument = new SupportingDocument();
                return supportingDocument;
            });
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Finds all.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <returns></returns>
        public Task<List<SupportingDocument>> FindAll(int applicationId)
        {
            return Task.Run<List<SupportingDocument>>(() =>
            {
                var response = this.DataContext.SupportingDocuments.Where(d => d.ApplicationId == applicationId).ToList();
                return response;
            });
        }

        /// <summary>
        /// Gets the file.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumentId">The supporting document identifier.</param>
        /// <returns></returns>
        public Task<SupportingDocument> GetFile(int applicationId, int supportingDocumentId)
        {
            return Task.Run<SupportingDocument>(() =>
            {
                var response = this.DataContext.SupportingDocuments.Where(d => d.ApplicationId == applicationId && d.SupportingDocumentId == supportingDocumentId).FirstOrDefault();
                return response;
            });
        }

        /// <summary>
        /// Creates a supporting document asynchronously.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns></returns>
        public async Task CreateAsync(SupportingDocument document)
        {
            await this.DataContext.SupportingDocuments.AddAsync(document);
            await this.DataContext.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the given Supporting Document
        /// </summary>
        /// <param name="entity">Supporting Document to update</param>
        /// <returns></returns>
        public Task UpdateAsync(SupportingDocument entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the given Supporting Document
        /// </summary>
        /// <param name="entity">Supporting Document to delete</param>
        /// <returns></returns>
        public Task DeleteAsync(SupportingDocument entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the asynchronously a supporting document.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumnetId">The supporting documnet identifier.</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int applicationId, int supportingDocumnetId)
        {
            try
            {
                var supportingDocument = this.DataContext.SupportingDocuments.Where(d =>
                d.ApplicationId == applicationId &&
                d.SupportingDocumentId == supportingDocumnetId).FirstOrDefault();

                this.DataContext.SupportingDocuments.Remove(supportingDocument);

                await this.DataContext.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                //TODO: Log error
                return false;
            }
            
        }

        /// <summary>
        /// Finds Supporting Documents that match the given expression
        /// </summary>
        /// <param name="match">Criteria used to find the required Supporting Documents</param>
        /// <returns>Supporting Documents that meet the criteria provided</returns>
        public Task<IList<SupportingDocument>> FindAll(Expression<Func<SupportingDocument, bool>> match)
        {
            throw new NotImplementedException();
        }
        #endregion Constructors
    }
}
