﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Data;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Repository definition for application statuses
    /// </summary>
    public interface IApplicationStatusRepository
    {
        /// <summary>
        /// Gets the initial application status for an application section
        /// </summary>
        /// <returns></returns>
        Task<ApplicationStatus> GetOpenApplicationStatus();

        /// <summary>
        /// Get the Pending Approval status for an application section
        /// </summary>
        /// <returns>
        /// Returns the ApplicationStatus
        ///     Name
        ///     Id
        /// </returns>
        Task<ApplicationStatus> GetSubmittedApplicationStatus();

        /// <summary>
        /// Get the application status for id provided
        /// </summary>
        /// <param name="statusId"></param>
        /// <returns></returns>
        Task<ApplicationStatus> Get(int statusId);
    }
}
