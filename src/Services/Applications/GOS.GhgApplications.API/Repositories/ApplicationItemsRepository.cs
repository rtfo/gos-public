﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data.Repositories;
using DfT.GOS.GhgApplications.API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Repository for GHG application items details CRUD
    /// </summary>
    public class ApplicationItemsRepository : AsyncEntityFrameworkRepository<ApplicationItem, int>, IApplicationItemsRepository
    {
        /// <summary>
        /// DataContext which represents the GosApplicationsDataContext
        /// </summary>
        private new GosApplicationsDataContext DataContext { get; set; }

        #region Constructors
        /// <summary>
        /// Contructor taking all arguments
        /// </summary>
        /// <param name="dataContext">The db context to use</param>
        public ApplicationItemsRepository(GosApplicationsDataContext dataContext)
            : base(dataContext)
        {
            this.DataContext = dataContext;
        }
        #endregion Construtors

        /// <summary>
        /// Finds the application items based on the expression
        /// </summary>
        /// <param name="match">Function to test each element for a condition</param>
        /// <returns>List of Application item</returns>
        public new Task<IList<ApplicationItem>> FindAll(Expression<Func<ApplicationItem, bool>> match)
        {
            return Task.Run<IList<ApplicationItem>>(() =>
            {
                var response = this.DataContext.ApplicationItem.Where(match).ToList();
                return response;
            });
        }
    }
}
