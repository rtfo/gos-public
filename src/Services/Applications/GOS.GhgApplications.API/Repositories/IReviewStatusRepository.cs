﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Data;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Repository definition for review statuses
    /// </summary>
    public interface IReviewStatusRepository
    {
        /// <summary>
        /// Gets the initial review status for an application section
        /// </summary>
        /// <returns></returns>
        Task<ReviewStatus> GetInitialReviewStatus();

        /// <summary>
        /// Gets the review status by ID
        /// </summary>
        /// <param name="reviewStatusId">The ID of the review status</param>
        /// <returns>Returns the status that correlates to the ID</returns>
        Task<ReviewStatus> Get(int reviewStatusId);
    }
}
