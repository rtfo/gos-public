﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data.Repositories;
using DfT.GOS.GhgApplications.API.Data;

namespace DfT.GOS.GhgApplications.API.Repositories
{
    /// <summary>
    /// Definition for GHG application item details CRUD repository
    /// </summary>
    public interface IApplicationItemsRepository: IAsyncRepository<ApplicationItem, int>
    {
    }
}
