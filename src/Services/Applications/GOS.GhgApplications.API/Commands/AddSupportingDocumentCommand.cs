﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.API.Commands
{
    /// <summary>
    /// Command to instruct the application API to add a new supporting document to an application
    /// </summary>
    public class AddSupportingDocumentCommand
    {
        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>
        /// The file.
        /// </value>
        [Required(AllowEmptyStrings = false)]
        public byte[] File { get; set; }
       
        /// <summary>
        /// Gets or sets the file extension.
        /// </summary>
        /// <value>
        /// The file extension.
        /// </value>
        [Required(AllowEmptyStrings = false)]
        public string FileExtension { get; set; }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        [Required(AllowEmptyStrings = false)]
        public string FileName { get; set; }
    }
}
