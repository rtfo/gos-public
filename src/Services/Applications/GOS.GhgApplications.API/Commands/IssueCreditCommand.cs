﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Commands
{
    /// <summary>
    /// Represents the IssueCredit command object
    /// </summary>
    public class IssueCreditCommand
    {
        /// <summary>
        /// User Identifier who issues the credit
        /// </summary>
        public string IssuedBy { get; set; }

        /// <summary>
        /// Date and time of credit issuance
        /// </summary>
        public DateTime IssuedDate { get; set; }
    }
}
