﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Services
{
    /// <summary>
    /// Supporting Document Parameters Service
    /// </summary>
    /// <seealso cref="DfT.GOS.GhgApplications.API.Services.ISupportingDocumentParametersService" />
    public class SupportingDocumentParametersService : ISupportingDocumentParametersService
    {
        /// <summary>
        /// Gets the application settings.
        /// </summary>
        /// <value>
        /// The application settings.
        /// </value>
        public AppSettings AppSettings { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SupportingDocumentParametersService"/> class.
        /// </summary>
        /// <param name="appSettings">The application settings.</param>
        public SupportingDocumentParametersService(AppSettings appSettings)
        {
            this.AppSettings = appSettings;
        }

        /// <summary>
        /// Gets the Uploaded Supporting Document the maximum file size.
        /// </summary>
        /// <returns>maximum file size</returns>
        long ISupportingDocumentParametersService.GetUploadedSupportingDocumentMaxSizeBytes()
        {
            return AppSettings.ApplicationsServiceFileUploadMaxSizeBytes;
        }
    }
}
