﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using AutoMapper;
using DfT.GOS.Data;
using DfT.GOS.GhgApplications.API.Commands;
using DfT.GOS.GhgApplications.API.Data;
using DfT.GOS.GhgApplications.API.Helpers;
using DfT.GOS.GhgApplications.API.Models;
using DfT.GOS.GhgApplications.API.Repositories;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.Common.Exceptions;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.Models;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.Messaging.Client;
using DfT.GOS.Messaging.Messages;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.SystemParameters.API.Client.Models;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.Web.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static DfT.GOS.GhgApplications.API.Data.ApplicationStatus;
using ModelApplicationStatus = DfT.GOS.GhgApplications.Common.Models.ApplicationStatus;

namespace DfT.GOS.GhgApplications.API.Services
{
    /// <summary>
    /// Service for GHG Applications CRUD
    /// </summary>
    public class ApplicationsService : DataServiceBase, IApplicationsService
    {
        #region Properties
        private IMapper Mapper { get; set; }
        private IApplicationsRepository ApplicationsRepository { get; set; }
        private IApplicationItemsRepository ApplicationItemsRepository { get; set; }
        private IApplicationStatusRepository ApplicationStatusRepository { get; set; }
        private IReviewStatusRepository ReviewStatusRepository { get; set; }
        private ISupportingDocumentRepository SupportingDocumentRepository { get; set; }
        private IScanVirusService ScanViruses { get; set; }
        private IGhgFuelsService GhgFuelsService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private ISystemParametersService SystemParametersService { get; set; }
        private ILogger<ApplicationsService> Logger { get; set; }
        private long ApplicationsServiceFileUploadMaxSize { get; set; }
        private ISupportingDocumentParametersService SupportingDocumentParametersService { get; set; }
        private int MonthThresholdForIssuingCredits { get; set; }

        private readonly IMessagingClient MessagingClient;
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationsService"/> class.
        /// </summary>
        /// <param name="mapper">The object mapper to use</param>
        /// <param name="dataContext">The data context.</param>
        /// <param name="scanViruses">The scan viruses.</param>
        /// <param name="ghgFuelsService">The GHG fuels service.</param>
        /// <param name="reportingPeriodsService">The reporting periods service.</param>
        /// <param name="systemParameterSerice">The system parameter service.</param>
        /// <param name="supportingDocumentParametersService">Supporting document repository</param>
        /// <param name="logger">Logging interface</param>
        /// <param name="messagingClient">Messaging Client used to notify the system when the Ledger is updated</param>
        /// <param name="monthThresholdForIssuingCredits">Number of months within which a submission should have credits issued</param>
        public ApplicationsService(IMapper mapper
            , GosApplicationsDataContext dataContext
            , IScanVirusService scanViruses
            , IGhgFuelsService ghgFuelsService
            , IReportingPeriodsService reportingPeriodsService
            , ISystemParametersService systemParameterSerice
            , ISupportingDocumentParametersService supportingDocumentParametersService
            , ILogger<ApplicationsService> logger
            , IMessagingClient messagingClient
            , int monthThresholdForIssuingCredits)
            : base(dataContext)
        {
            this.Mapper = mapper;
            this.ApplicationsRepository = new ApplicationsRepository(dataContext);
            this.ApplicationItemsRepository = new ApplicationItemsRepository(dataContext);
            this.ApplicationStatusRepository = new ApplicationStatusRepository(dataContext);
            this.ReviewStatusRepository = new ReviewStatusRepository(dataContext);
            this.SupportingDocumentRepository = new SupportingDocumentsRepository(dataContext);
            this.ScanViruses = scanViruses;
            this.GhgFuelsService = ghgFuelsService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.SystemParametersService = systemParameterSerice;
            this.SupportingDocumentParametersService = supportingDocumentParametersService;
            this.Logger = logger;
            this.MessagingClient = messagingClient;
            this.MonthThresholdForIssuingCredits = monthThresholdForIssuingCredits;
        }

        /// <summary>
        /// Constructor taking all dependencies, used by unit tests
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="dataContext"></param>
        /// <param name="scanViruses">The File Scanning virus checker</param>
        /// <param name="ghgFuelsService">The service to get GHG Fuel information from</param>
        /// <param name="reportingPeriodsService">The service to get reporting period information, such as obligation periods</param>
        /// <param name="applicationItemsRepository">The application items repository</param>
        /// <param name="applicationRepository">The applications repository</param>
        /// <param name="applicationStatusRepository">Application status repository</param>
        /// <param name="reviewStatusRepository">The review status repository</param>
        /// <param name="supportingDocumentRepository">Supporting document repository</param>
        /// <param name="systemParametersService">The service for getting system parameters</param>
        /// <param name="supportingDocumentParametersService">The service for getting details about supporting documents</param>
        /// <param name="logger">Logging interface</param>
        /// <param name="messagingClient">Messaging Client used to notify the system when the Ledger is updated</param>
        /// <param name="monthThresholdForIssuingCredits">Number of months within which a submission should have credits issued</param>
        public ApplicationsService(IMapper mapper
            , GosApplicationsDataContext dataContext
            , IScanVirusService scanViruses
            , IGhgFuelsService ghgFuelsService
            , IReportingPeriodsService reportingPeriodsService
            , IApplicationsRepository applicationRepository
            , IApplicationItemsRepository applicationItemsRepository
            , IApplicationStatusRepository applicationStatusRepository
            , IReviewStatusRepository reviewStatusRepository
            , ISupportingDocumentRepository supportingDocumentRepository
            , ISystemParametersService systemParametersService
            , ISupportingDocumentParametersService supportingDocumentParametersService
            , ILogger<ApplicationsService> logger
            , IMessagingClient messagingClient
            , int monthThresholdForIssuingCredits)
            : base(dataContext)
        {
            this.Mapper = mapper;
            this.ApplicationsRepository = applicationRepository;
            this.ApplicationItemsRepository = applicationItemsRepository;
            this.ApplicationStatusRepository = applicationStatusRepository;
            this.ReviewStatusRepository = reviewStatusRepository;
            this.SupportingDocumentRepository = supportingDocumentRepository;
            this.ScanViruses = scanViruses;
            this.GhgFuelsService = ghgFuelsService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.SystemParametersService = systemParametersService;
            this.SupportingDocumentParametersService = supportingDocumentParametersService;
            this.Logger = logger;
            this.MessagingClient = messagingClient;
            this.MonthThresholdForIssuingCredits = monthThresholdForIssuingCredits;
        }

        #endregion Constructors

        #region IApplicationsService Implementation

        /// <summary>
        /// Creates a GHG application
        /// </summary>
        /// <param name="command">The command containing the details of the application to create</param>
        /// <param name="createdBy">The ID of the user creating the application</param>
        /// <returns>Returns the ID of the application that was created</returns>
        async Task<int> IApplicationsService.CreateApplication(CreateApplicationCommand command, string createdBy)
        {
            var selectedFuelType = await this.GhgFuelsService.GetFuelType(command.FuelTypeId);
            var electricityFuelType = await this.GhgFuelsService.GetElectricityFuelType();
            var selectedObligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(command.ObligationPeriodId);

            if (selectedFuelType == null)
            {
                throw new ArgumentException("No Fuel Type could be found with ID = " + command.FuelTypeId);
            }

            if (selectedObligationPeriod == null)
            {
                throw new ArgumentException("No Obligation Period could be found with ID = " + command.ObligationPeriodId);
            }

            var initialReviewStatus = await ReviewStatusRepository.GetInitialReviewStatus();
            var initialApplicationStatus = await ApplicationStatusRepository.GetOpenApplicationStatus();
            var createdDate = DateTime.Now; // TODO: Possibly don't hard code this - see JIRA ROSGOS-819

            //Persist the application
            var application = new Application()
            {
                ObligationPeriodId = command.ObligationPeriodId,
                SupplierOrganisationId = command.SupplierOrganisationId,
                ApplicationStatus = initialApplicationStatus,
                ApplicationVolumesReviewStatus = initialReviewStatus,
                SupportingDocumentsReviewStatus = initialReviewStatus,
                CalculationReviewStatus = initialReviewStatus,
                CreatedBy = createdBy,
                CreatedDate = createdDate,
            };

            await this.ApplicationsRepository.CreateAsync(application);

            //Persist the application item
            var applicationItem = new ApplicationItem()
            {
                Application = application,
                FuelTypeId = command.FuelTypeId,
                AmountSupplied = command.AmountSupplied,
                EnergyDensity = this.GetApplicationItemEnergyDensity(selectedFuelType),
                GhgIntensity = this.GetApplicationItemGhgIntensity(selectedFuelType, command.GhgIntensity, electricityFuelType.FuelCategoryId),
                CreatedBy = createdBy,
                CreatedDate = createdDate,
                Reference = command.Reference
            };

            await this.ApplicationItemsRepository.CreateAsync(applicationItem);
            try
            {
                var result = await this.SaveAsync();
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Error creating application:" + ex.ToString());
                throw new ApplicationException("Error creating application:" + " " + ex.ToString());
            }

            return application.ApplicationId;
        }

        /// <summary>
        /// Gets summary details about an application
        /// </summary>
        /// <param name="applicationId">The ID of the application to get</param>
        /// <returns>Returns an application summary</returns>
        async Task<ApplicationSummary> IApplicationsService.GetApplicationSummary(int applicationId)
        {
            var application = await this.ApplicationsRepository.GetAsync(applicationId);
            return this.Mapper.Map<Application, ApplicationSummary>(application);
        }

        /// <summary>
        /// Gets all applications that are ready for review 
        /// </summary>
        /// <param name="applicationParameters">Parameters to filter and sort the applications by</param>
        /// <returns>Returns list of application summary</returns>
        async Task<PagedResult<ApplicationSummary>> IApplicationsService.GetPendingApplicationPagedResult(PendingApplicationParameters applicationParameters)
        {
            Expression<Func<Application, bool>> match;

            if (applicationParameters.ApplicationStatus == ModelApplicationStatus.Submitted)
            {
                match = x => x.ApplicationStatusId == (int)ModelApplicationStatus.Submitted;
            }
            else if (applicationParameters.ApplicationStatus == ModelApplicationStatus.RecommendApproval)
            {
                match = x => x.ApplicationStatusId == (int)ModelApplicationStatus.RecommendApproval;
            }
            else if (applicationParameters.ApplicationStatus == ModelApplicationStatus.Approved)
            {
                match = x => x.ApplicationStatusId == (int)ModelApplicationStatus.Approved;
            }
            else
            {
                match = x =>
                (x.ApplicationStatusId == (int)ModelApplicationStatus.Submitted) ||
                (x.ApplicationStatusId == (int)ModelApplicationStatus.RecommendApproval) ||
                (x.ApplicationStatusId == (int)ModelApplicationStatus.Approved);
            }

            var applications = await this.ApplicationsRepository.GetApplicationsPagedResult(match
                , applicationParameters.PageSize
                , applicationParameters.Page
                , applicationParameters.Order.HasValue ? applicationParameters.Order.Value : SortOrder.Decending
            );
            return this.Mapper.Map<PagedResult<Application>, PagedResult<ApplicationSummary>>(applications);
        }

        /// <summary>
        /// Gets all the applications that are ready for issue 
        /// </summary>
        /// <param name="pageSize">No. of records per page</param>
        /// <param name="pageNumber">current page</param>
        /// <returns>Returns list of application summary</returns>
        async Task<PagedResult<ApplicationSummary>> IApplicationsService.GetApplicationsReadyForIssuePagedResult(int pageSize, int pageNumber)
        {
            var applications = await this.ApplicationsRepository.GetApplicationsPagedResult(x =>
                (x.ApplicationStatusId == (int)ModelApplicationStatus.Approved)
                , pageSize
                , pageNumber
            );
            return this.Mapper.Map<PagedResult<Application>, PagedResult<ApplicationSummary>>(applications);
        }

        /// <summary>
        /// Gets the application items for an application
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <returns>List of application items</returns>
        async Task<IList<ApplicationItemSummary>> IApplicationsService.GetApplicationItems(int applicationId)
        {
            // Check if the application exists (Do we need this check here?)
            var application = await this.ApplicationsRepository.GetAsync(applicationId);
            if (application != null)
            {
                var appitem = await this.ApplicationItemsRepository.FindAll(x => x.Application.ApplicationId == applicationId);
                // In order to map the List objects we need to explicitly create the map between the underlying objects refer services.AddAutoMapper() in Startup.cs
                return this.Mapper.Map<IList<ApplicationItem>, IList<ApplicationItemSummary>>(appitem);
            }
            return null;
        }

        /// <summary>
        /// Gets all application items that have had GHG credits issued for a company within an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>Returns the application items</returns>
        async Task<IList<ApplicationItemSummary>> IApplicationsService.GetApplicationItemsWithIssuedCredits(int organisationId, int obligationPeriodId)
        {
            var applicationHasBeenIssuedStatuses = this.GetApplicationHasBeenIssuedStatusIds();

            var issuedApplicationitems = await this.ApplicationItemsRepository.FindAll(ai => ai.Application.SupplierOrganisationId == organisationId
                                                                                            && ai.Application.ObligationPeriodId == obligationPeriodId
                                                                                            && applicationHasBeenIssuedStatuses.Contains(ai.Application.ApplicationStatusId));

            return this.Mapper.Map<IList<ApplicationItem>, IList<ApplicationItemSummary>>(issuedApplicationitems);
        }

        /// <summary>
        /// Supporting document upload.
        /// </summary>
        /// <param name="command">File command</param>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="typeId">ROS file extension id.</param>
        /// <param name="createdBy">The ID of the user uploading the document</param>
        /// <returns>
        ///     UploadStatus
        ///     FileName 
        ///     ScanResult         
        /// </returns>
        async Task<FileValidationResult> IApplicationsService.UploadSupportingDocument(AddSupportingDocumentCommand command, int typeId, int applicationId, string createdBy)
        {
            FileValidationResult supportingDocumentUpload = new FileValidationResult();

            supportingDocumentUpload.FileName = command.FileName;
            supportingDocumentUpload.UploadStatus = "Scanning";
            supportingDocumentUpload.ScanResult = await ScanViruses.ScanBytes(command.File);
            supportingDocumentUpload.UploadStatus = "Scanned";

            if (supportingDocumentUpload.ScanResult.IsVirusFree)
            {
                SupportingDocument supportingDocument = new SupportingDocument()
                {
                    ApplicationId = applicationId,
                    FileName = command.FileName,
                    File = command.File,
                    CreatedBy = createdBy,
                    DocumentTypeId = typeId,
                    CreatedDate = DateTime.Now,
                    SupportingDocumentId = 0
                };
                await this.SupportingDocumentRepository.CreateAsync(supportingDocument);
                var result = await this.SaveAsync();
            }
            return supportingDocumentUpload;

        }

        /// <summary>
        /// Validates the metadata of a file against file size, extension, etc
        /// </summary>
        /// <param name="fileMetadata">The metadata for the file we're validating</param>
        /// <returns>Returns the result of the validation</returns>
        async Task<FileValidationResult> IApplicationsService.ValidateFileMetadata(FileMetadata fileMetadata)
        {
            var fileValidationResult = new FileValidationResult();
            fileValidationResult.UploadStatus = "Validating";

            if (fileMetadata == null)
            {
                //No File metadata was supplied
                fileValidationResult.UploadStatus = "No File is attached";
                fileValidationResult.Messages.Add("No File is attached");
            }
            else if (fileMetadata.FileName == null)
            {
                //The file doesn't have a filename
                fileValidationResult.UploadStatus = "No File is attached";
                fileValidationResult.Messages.Add("No File is attached");
            }
            else
            {
                fileValidationResult.FileName = fileMetadata.FileName;

                if (fileMetadata.FileExtension == null)
                {
                    //No file extension has been supplied
                    fileValidationResult.UploadStatus = "File extension is missing";
                    fileValidationResult.Messages.Add("File extension is missing");
                }
                else if (fileMetadata.FileSizeBytes <= 0)
                {
                    //An empty file was supplied
                    fileValidationResult.UploadStatus = "File is Empty";
                    fileValidationResult.Messages.Add("File " + fileValidationResult.FileName + " is empty");
                }
                else if (!fileMetadata.FileExtension.Contains("."))
                {
                    //No file extension supplied
                    fileValidationResult.UploadStatus = "File extension is missing";
                    fileValidationResult.Messages.Add("File extension is missing");
                }
                else
                {
                    var fileExtension = fileMetadata.FileExtension.Split(".", StringSplitOptions.None)[1];
                    var supportedDocumentTypes = (await this.SystemParametersService.GetDocumentTypes()).Result;
                    var documentType = supportedDocumentTypes.SingleOrDefault(dt => dt.FileExtension.ToLower() == fileExtension.ToLower());

                    if (documentType == null)
                    {
                        //The document file extension is not supported
                        var supportedDocumentExtensionsMessage = this.GetSupportedDocumentExtensionsMessage(supportedDocumentTypes);

                        fileValidationResult.UploadStatus = "File Type Not Supported";
                        fileValidationResult.Messages.Add("The " + fileMetadata.FileExtension.Trim()
                            + " file type is not supported. Please use one of " + supportedDocumentExtensionsMessage);
                    }
                    else
                    {
                        var maxFileSizeBytes = this.SupportingDocumentParametersService.GetUploadedSupportingDocumentMaxSizeBytes();
                        if (fileMetadata.FileSizeBytes > maxFileSizeBytes)
                        {
                            //The file is too large
                            fileValidationResult.UploadStatus = "MaxFilesize exceeded";
                            fileValidationResult.Messages.Add("File " + fileMetadata.FileName + " exceeds the maximum file size of " + FileHelper.GetMaxFileSizeDisplay(maxFileSizeBytes));
                        }
                        else
                        {
                            //The document is valid!
                            fileValidationResult.UploadStatus = "Validated";
                        }
                    }
                }
            }

            return fileValidationResult;
        }

        /// <summary>
        /// Gets the uploaded supporting documents.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <returns>
        ///  A list of
        ///     SupportingDocumentId 
        ///     FileName
        /// </returns>
        async Task<List<UploadedSupportingDocument>> IApplicationsService.GetUploadedSupportingDocuments(int applicationId)
        {
            List<UploadedSupportingDocument> supportingDocumentsUpload = new List<UploadedSupportingDocument>();
            try
            {
                var response = await this.SupportingDocumentRepository.FindAll(applicationId);

                foreach (var sd in response)
                {
                    UploadedSupportingDocument supportingDocumentUpload = new UploadedSupportingDocument();

                    supportingDocumentUpload.SupportingDocumentId = sd.SupportingDocumentId;
                    supportingDocumentUpload.FileName = sd.FileName;

                    supportingDocumentsUpload.Add(supportingDocumentUpload);
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Could not retrieve Uploaded documents " + ex.ToString());
            }
            return supportingDocumentsUpload;
        }

        /// <summary>
        /// Gets the uploaded supporting document for download.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumentId">The supporting document identifier.</param>
        /// <returns>
        ///     FileName
        ///     File(Contents)
        /// </returns>
        async Task<DownloadSupportingDocument> IApplicationsService.GetUploadedSupportingDocument(int applicationId, int supportingDocumentId)
        {
            DownloadSupportingDocument supportingDocumentsUpload = new DownloadSupportingDocument();
            try
            {
                var response = await this.SupportingDocumentRepository.GetFile(applicationId, supportingDocumentId);

                if (response != null)
                {
                    supportingDocumentsUpload.File = response.File;
                    supportingDocumentsUpload.FileName = response.FileName;
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Could not retrieve Uploaded document " + ex.ToString());
            }

            return supportingDocumentsUpload;
        }

        /// <summary>
        /// Remove Supporting Document from Application
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="supportingDocumentId"></param>
        /// <returns>True/False</returns>
        async Task<bool> IApplicationsService.RemoveSupportingDocument(int applicationId, int supportingDocumentId)
        {
            try
            {
                var response = await this.SupportingDocumentRepository.DeleteAsync(applicationId, supportingDocumentId);
                return response;
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Could not retrieve Remove document " + ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Submit an application and relate the user
        /// </summary>
        /// <param name="applicationId">Application id</param>
        /// <param name="userId">User that made the submission</param>
        /// <returns>A message, empty message if it is success</returns>
        async Task<string> IApplicationsService.SubmitApplication(int applicationId, string userId)
        {
            string message = string.Empty;
            try
            {
                var application = await this.ApplicationsRepository.GetAsync(applicationId);
                if (application != null)
                {
                    if (application.ApplicationStatusId == (int)ModelApplicationStatus.Open)
                    {
                        application.ApplicationStatusId = (int)ModelApplicationStatus.Submitted;
                        application.SubmittedBy = userId;
                        application.SubmittedDate = DateTime.Now;
                        try
                        {
                            await this.ApplicationsRepository.UpdateAsync(application);
                            await this.SaveAsync();
                        }
                        catch (Exception ex)
                        {
                            this.Logger.LogError(ex.ToString());
                            message = "Saving Application Submission Error. Application was not submitted.";
                        }
                    }
                    else
                    {
                        if (application.ApplicationStatusId == (int)ModelApplicationStatus.Submitted)
                        {
                            message = "Application has already been submitted.";
                        }
                        else
                        {
                            message = "Invalid application status. Application was not submitted.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Error in IApplicationsService.SubmitApplication:" + ex.ToString());
                message = "Application Submission Error. Application was not submitted.";
            }

            return message;
        }

        /// <summary>
        /// Gets the application details for review
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <returns>ApplicationReviewSummary</returns>
        async Task<ApplicationReviewSummary> IApplicationsService.GetApplicationReviewSummary(int applicationId)
        {
            var application = await this.ApplicationsRepository.GetAsync(applicationId);

            if (application != null)
            {
                return this.Mapper.Map<Application, ApplicationReviewSummary>(application, x => x.ConfigureMap()
                    .ForMember(app => app.ApplicationStatus, opt => opt.Ignore())
                    .AfterMap((s, d) =>
                    {
                        d.ApplicationStatus = (ModelApplicationStatus)Enum.Parse(typeof(ModelApplicationStatus), s.ApplicationStatusId.ToString());
                    }));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Updates the review section for volumes of an application
        /// </summary>
        /// <param name="command">The command containing details of the review</param>
        /// <returns>Returns a boolean indicating success</returns>
        async Task<bool> IApplicationsService.ReviewApplicationVolumes(ReviewApplicationVolumesCommand command)
        {
            var application = await this.ApplicationsRepository.GetAsync(command.ApplicationId);
            var reviewStatus = await this.ReviewStatusRepository.Get(command.VolumesReviewStatusId);

            application.ApplicationVolumesReviewStatus = reviewStatus;
            application.ApplicationVolumesRejectionReason = command.RejectionReason;

            var result = await this.SaveAsync();

            return result > 0;
        }

        /// <summary>
        /// Performs a updates the status of an application
        /// </summary>
        /// <param name="command">The command containing details of the review</param>
        /// <returns></returns>
        async Task<bool> IApplicationsService.ReviewApplication(ReviewApplicationCommand command)
        {
            var application = await this.ApplicationsRepository.GetAsync(command.ApplicationId);
            var reviewStatus = await this.ApplicationStatusRepository.Get(command.ApplicationStatusId);

            if (application.ApplicationStatus.ApplicationStatusId == (int)ApplicationStatuses.Submitted)
            {
                //First level approver
                application.RecommendedBy = command.RecommendedBy;
                application.RecommendedDate = command.RecommendedDate;
            }
            else if (application.ApplicationStatus.ApplicationStatusId == (int)ApplicationStatuses.RecommendApproval)
            {
                //Second level approver
                application.ApprovedBy = command.ApprovedBy;
                application.ApprovedDate = command.ApprovedDate;
            }

            if (command.ApplicationStatusId == (int)ApplicationStatuses.Rejected)
            {
                application.RejectedDate = command.RejectedDate;
                application.RejectedBy = command.RejectedBy;
            }
            application.ApplicationStatus = reviewStatus;
            application.ApplicationStatusId = command.ApplicationStatusId;

            var result = await this.SaveAsync();

            return result > 0;
        }

        /// <summary>
        /// Updates the review section for supporting documents of an application
        /// </summary>
        /// <param name="command">The command containing details of the review</param>
        /// <returns>Returns a boolean indicating success</returns>
        async Task<bool> IApplicationsService.ReviewApplicationSupportingDocuments(ReviewApplicationSupportingDocumentsCommand command)
        {
            var application = await this.ApplicationsRepository.GetAsync(command.ApplicationId);
            var reviewStatus = await this.ReviewStatusRepository.Get(command.SupportingDocumentsReviewStatusId);

            application.SupportingDocumentsReviewStatus = reviewStatus;
            application.SupportingDocumentsRejectionReason = command.RejectionReason;

            var result = await this.SaveAsync();

            return result > 0;
        }

        /// <summary>
        /// Updates the review section for the calculation of an application
        /// </summary>
        /// <param name="command">The command containing details of the review</param>
        /// <returns>Returns a boolean indicating success</returns>
        async Task<bool> IApplicationsService.ReviewApplicationCalculation(ReviewApplicationCalculationCommand command)
        {
            var application = await this.ApplicationsRepository.GetAsync(command.ApplicationId);
            var reviewStatus = await this.ReviewStatusRepository.Get(command.CalculationReviewStatusId);

            application.CalculationReviewStatus = reviewStatus;
            application.CalculationRejectionReason = command.RejectionReason;

            var result = await this.SaveAsync();

            return result > 0;
        }

        /// <summary>
        /// Approves an application
        /// </summary>
        /// <param name="command">The command containing details of the approval</param>
        /// <param name="userId">The ID of the user performing the approval</param>
        /// <returns>Returns a boolean value indicating success</returns>
        async Task<bool> IApplicationsService.ApproveApplication(ApproveApplicationCommand command, string userId)
        {
            var application = await this.ApplicationsRepository.GetAsync(command.ApplicationId);
            var reviewStatus = await this.ApplicationStatusRepository.Get((int)ApplicationStatuses.Approved);

            application.ApprovedBy = userId;
            application.ApprovedDate = DateTime.Now;

            application.ApplicationStatus = reviewStatus;
            application.ApplicationStatusId = (int)ApplicationStatuses.Approved;

            var result = await this.SaveAsync();

            return result > 0;
        }

        /// <summary>
        /// Rejects an application
        /// </summary>
        /// <param name="command">The command containing details of the rejection</param>
        /// <param name="userId">The ID of the user performing the rejection</param>
        /// <returns>Returns a boolean value indicating success</returns>
        async Task<bool> IApplicationsService.RejectApplication(RejectApplicationCommand command, string userId)
        {
            var application = await this.ApplicationsRepository.GetAsync(command.ApplicationId);
            var reviewStatus = await this.ApplicationStatusRepository.Get((int)ApplicationStatuses.Submitted);

            application.ApplicationStatus = reviewStatus;
            application.ApplicationStatusId = (int)ApplicationStatuses.Submitted;

            var result = await this.SaveAsync();

            return result > 0;
        }

        /// <summary>
        /// Gets all the applications for the supplier
        /// </summary>
        /// <param name="organisationId"></param>
        /// <param name="obligationId"></param>
        /// <param name="pageSize">No. of records per page</param>
        /// <param name="pageNumber">current page</param>
        /// <returns>Returns list of application summary</returns>
        async Task<PagedResult<ApplicationSummary>> IApplicationsService.GetSupplierApplications(int organisationId, int obligationId, int pageSize, int pageNumber)
        {
            var applications = await this.ApplicationsRepository.GetApplicationsPagedResult(x =>
                x.SupplierOrganisationId == organisationId && x.ObligationPeriodId == obligationId
                && x.ApplicationStatusId != (int)ApplicationStatuses.Open
                , pageSize
                , pageNumber
            );
            return this.Mapper.Map<PagedResult<Application>, PagedResult<ApplicationSummary>>(applications);
        }

        /// <summary>
        /// Issue credits to the approved applications
        /// </summary>
        /// <param name="issueCreditCommand">The command containing details of the issuer</param>
        /// <returns>Returns a boolean value indicating success</returns>
        async Task<bool> IApplicationsService.IssueCredits(IssueCreditCommand issueCreditCommand)
        {
            var issueStatus = await this.ApplicationStatusRepository.Get((int)ApplicationStatuses.GHGCreditsIssued);
            var approvedApplications = await this.ApplicationsRepository.FindAll(x => x.ApplicationStatusId == (int)ApplicationStatuses.Approved);
            foreach (var application in approvedApplications)
            {
                application.ApplicationStatus = issueStatus;
                application.ApplicationStatusId = (int)ApplicationStatuses.GHGCreditsIssued;
                application.IssuedBy = issueCreditCommand.IssuedBy;
                application.IssuedDate = issueCreditCommand.IssuedDate;
            }
            var result = await this.SaveAsync();
            if (result > 0)
            {
                try
                {
                    var tasks = new List<Task>();
                    approvedApplications.ToList().ForEach(x =>
                    {
                        var t = this.MessagingClient.Publish(new CreditsIssuedMessage()
                        {
                            ObligationPeriodId = x.ObligationPeriodId,
                            OrganisationId = x.SupplierOrganisationId,
                            ApplicationId = x.ApplicationId
                        });
                        tasks.Add(t);
                    });
                    await Task.WhenAll(tasks);
                }
                catch (Exception ex)
                {
                    this.Logger.LogError(ex, "An error occurred whilst publishing messages for the IssueCredits updates.");
                }
            }

            return result > 0;
        }

        /// <summary>
        /// Revokes the specified application
        /// </summary>
        /// <param name="command">Command containing the Id of the application to revoke and the revocation reason</param>
        /// <param name="userId">Id of the user preforming the revoke</param>
        /// <returns>Returns a boolean value indicating success</returns>
        async Task<bool> IApplicationsService.RevokeApplication(RevokeApplicationCommand command, string userId)
        {
            var application = await this.ApplicationsRepository.GetAsync(command.ApplicationId);
            if (application == null)
            {
                this.Logger.LogWarning("User: " + userId + " Attempted to revoke application that doesn't exist");
                throw new ApplicationNotFoundException();
            }

            if (application.ApplicationStatusId != (int)ModelApplicationStatus.GHGCreditsIssued)
            {
                this.Logger.LogInformation("User: " + userId + " Attempted to revoke application not in Credits Issued state");
                throw new InvalidApplicationRevokeException("Can only revoke applications where credits have been issued");
            }

            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(application.ObligationPeriodId);
            if (obligationPeriod == null)
            {
                this.Logger.LogError("Failed to get obligation period for application");
                throw new ServiceException("Failed to get obligation period for application");
            }
            var lastRevocationDate = obligationPeriod.LatestDateForRevocation;

            if (lastRevocationDate < DateTime.Now)
            {
                this.Logger.LogInformation("User: " + userId + " Attempted to revoke application outside period for this obligation period");
                throw new InvalidApplicationRevokeException("Revocation period for this obligation period has ended");
            }

            if (!string.IsNullOrWhiteSpace(command.RevocationReason))
            {
                application.RevocationReason = command.RevocationReason;
            }
            application.RevokedDate = DateTime.Now;
            application.RevokedBy = userId;
            application.ApplicationStatusId = (int)ApplicationStatuses.Revoked;


            await this.ApplicationsRepository.UpdateAsync(application);
            await this.SaveAsync();

            _ = this.MessagingClient.Publish(new ApplicationRevokedMessage
            {
                ApplicationId = application.ApplicationId,
                ObligationPeriodId = application.ObligationPeriodId,
                OrganisationId = application.SupplierOrganisationId
            });

            this.Logger.LogInformation("Application revoked: " + application.ApplicationId);

            return true;
        }

        #endregion IApplicationsService Implementation

        #region Methods        

        /// <summary>
        /// Gets a string representation of the document extensions that are supported
        /// </summary>
        /// <param name="supportedDocumentTypes">The list of supported document types</param>
        /// <returns>Returns a string representation of the supported document extensions</returns>
        private string GetSupportedDocumentExtensionsMessage(IList<DocumentType> supportedDocumentTypes)
        {
            var supportedExtensions = string.Empty;

            if (supportedDocumentTypes != null)
            {
                foreach (var type in supportedDocumentTypes)
                {
                    supportedExtensions = supportedExtensions + "." + type.FileExtension + " , ";
                }
                supportedExtensions = supportedExtensions.Remove(supportedExtensions.Length - 3);
            }

            return supportedExtensions;
        }

        /// <summary>
        /// Gets the Energy Density for an application item
        /// </summary>
        /// <param name="selectedFuelType">The Fuel Type that has been selected</param>
        /// <returns>Returns the application item Energy Density</returns>
        private decimal? GetApplicationItemEnergyDensity(FuelType selectedFuelType)
        {
            return selectedFuelType.EnergyDensity;
        }

        /// <summary>
        /// Gets the GHG Intensity for an application item
        /// </summary>
        /// <param name="selectedFuelType">The Fuel Type that has been selected</param>
        /// <param name="overriddenGhgIntensity">A user overriden GHG Intensity, if applicable</param>
        /// <param name="electricityFuelCategoryId">The ID of electricity fuel category</param>
        /// <returns>Returns the application item GHG Intensity</returns>
        private decimal? GetApplicationItemGhgIntensity(FuelType selectedFuelType, decimal? overriddenGhgIntensity, int electricityFuelCategoryId)
        {
            //Get the default value from the selected fuel type
            var applicationItemGhgIntensity = selectedFuelType.CarbonIntensity;

            if (selectedFuelType.FuelCategoryId == electricityFuelCategoryId)
            {
                if (!overriddenGhgIntensity.HasValue)
                {
                    throw new ArgumentNullException("Electricity overriden GHG Intensity cannot be null");
                }

                applicationItemGhgIntensity = overriddenGhgIntensity;
            }

            return applicationItemGhgIntensity;
        }

        /// <summary>
        /// Gets a list of status IDs for all application status that have credits issued against them
        /// </summary>
        /// <returns></returns>
        private List<int> GetApplicationHasBeenIssuedStatusIds()
        {
            var applicationHasBeenIssuedStatusIds = new List<int>();
            applicationHasBeenIssuedStatusIds.Add((int)ApplicationStatuses.GHGCreditsIssued);
            return applicationHasBeenIssuedStatusIds;
        }

        /// <summary>
        /// Returns the performance data relating to applications
        ///  - No. of applications with credits issued for each calendar month
        ///  - No. of incomplete applications for each calendar month
        ///  - No. of applications rejected each calendar month
        ///  - No. of applications revoked each calendar month
        ///  - No. of applications submitted for each calendar month
        ///  - No. of applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently)
        ///  - Total No. of applications for the calendar month
        /// </summary>
        /// <returns>List of the above metrics for each month</returns>
        public async Task<List<ApplicationPerformanceData>> GetApplicationPerformanceData()
        {
            var earliestDate = await this.ApplicationsRepository.GetFirstApplicationDate();
            if (earliestDate.HasValue)
            {
                var startMonth = new DateTime(earliestDate.Value.Year, earliestDate.Value.Month, 1);

                var counts = new List<ApplicationPerformanceData>();
                counts = await this.GetApplicationPerformanceDataSince(startMonth, counts);

                return counts;
            }
            return new List<ApplicationPerformanceData>();
        }

        /// <summary>
        /// Go through every month between the month of the date provided and todays month
        /// and get the applications performance data
        /// </summary>
        /// <param name="startMonth">Month to start from</param>
        /// <param name="metrics">List of the counts so far</param>
        /// <returns>Returns list of metrics</returns>
        private async Task<List<ApplicationPerformanceData>> GetApplicationPerformanceDataSince(DateTime startMonth, List<ApplicationPerformanceData> metrics)
        {
            if (DateTime.Compare(startMonth, DateTime.Now) > 0)
            {
                throw new ArgumentException("Start month can't be over today's date");
            }

            //Set the end date to the end of the month, unless we're looking at the current month, then use todays date
            DateTime endDate = startMonth.Month.Equals(DateTime.Now.Month) && startMonth.Year.Equals(DateTime.Now.Year) ? DateTime.Now : startMonth.AddMonths(1).AddMilliseconds(-1);

            var NoOfSubmittedApplicationsTask = await this.ApplicationsRepository.GetNoOfSubmittedApplications(startMonth, endDate);
            var NoOfRevokedApplicationsTask = await this.ApplicationsRepository.GetNoOfRevokedApplications(startMonth, endDate);
            var NoOfRejectedApplicationsTask = await this.ApplicationsRepository.GetNoOfRejectedApplications(startMonth, endDate);
            var NoOfIncompleteApplicationsTask = await this.ApplicationsRepository.GetNoOfIncompleteApplications(startMonth, endDate);
            var NoOfApplicationsWithCreditsIssuedTask = await this.ApplicationsRepository.GetNoOfApplicationsWithCreditsIssued(startMonth, endDate);
            var NoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssueTask =
                await this.ApplicationsRepository.GetNoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue(startMonth, endDate, this.MonthThresholdForIssuingCredits);
            var TotalNoOfApplicationsTask = await this.ApplicationsRepository.GetTotalNoOfApplications(startMonth, endDate);

            // NB: We are awaiting the repository calls individually here to avoid the following error noted during Integration testing
            // System.InvalidOperationException: A second operation started on this context before a previous operation completed. This is usually caused by different threads using the same instance of DbContext.

            var applicationPerformanceData = new ApplicationPerformanceData(startMonth
                , NoOfApplicationsWithCreditsIssuedTask
                , NoOfIncompleteApplicationsTask
                , NoOfRejectedApplicationsTask
                , NoOfRevokedApplicationsTask
                , NoOfSubmittedApplicationsTask
                , NoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssueTask
                , TotalNoOfApplicationsTask);
            metrics.Add(applicationPerformanceData);

            // Check if the month that we just got data for is this month
            if (DateTime.Now.Year.Equals(startMonth.Year) && DateTime.Now.Month.Equals(startMonth.Month))
            {
                return metrics;
            }
            else
            {
                // As it's not move on to the next month
                return await this.GetApplicationPerformanceDataSince(startMonth.AddMonths(1), metrics);
            }
        }

        #endregion Methods
    }
}
