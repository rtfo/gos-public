﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using nClam;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Services
{
    /// <summary>
    /// Scan Virus Service
    /// </summary>
    public interface IScanVirusService
    {

        /// <summary>
        /// Scans a file for virus.
        /// </summary>
        /// <param name="fullPath">The full path.</param>
        /// <returns>
        ///     Scan Result Message
        ///     Virus Existence True/False
        /// </returns>
        Task<ScanResult> ScanFile(string fullPath);

        /// <summary>
        /// Scans bytes for virus.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>
        ///     Scan Result Message
        ///     Virus Existence True/False
        /// </returns>
        Task<ScanResult> ScanBytes(byte[] bytes);

        /// <summary>
        /// Scans a stream for virus.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>
        ///     Scan Result Message
        ///     Virus Existence True/False
        /// </returns>
        Task<ScanResult> ScanStream(Stream stream);
    }
}
