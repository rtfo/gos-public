﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Services
{
    /// <summary>
    /// Supporting Document Parameters Service Service
    /// </summary>
    public interface ISupportingDocumentParametersService
    {
        /// <summary>
        /// Gets the Uploaded Supporting Document the maximum file size.
        /// </summary>
        /// <returns>maximum file size</returns>
        long GetUploadedSupportingDocumentMaxSizeBytes();
    }
}
