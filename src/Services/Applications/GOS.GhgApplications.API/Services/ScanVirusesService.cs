﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using System.IO;
using System.Linq;
using nClam;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.API.Services
{
    /// <summary>
    /// Scan Viruses Service
    /// </summary>
    /// <seealso cref="DfT.GOS.GhgApplications.API.Services.IScanVirusService" />
    public class ScanVirusesService : IScanVirusService
    {
        /// <summary>
        /// Scans bytes for virus.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>
        ///     Scan Result Message
        ///     Virus Existence True/False
        /// </returns>
        async Task<ScanResult> IScanVirusService.ScanBytes(byte[] bytes)
        {
            var clam = new ClamClient(Constants.VirusScanURL, Constants.VirusScanPort);
            var temp = await clam.SendAndScanFileAsync(bytes);
            return MapScanResult(temp);
        }

        /// <summary>
        /// Scans a file for virus.
        /// </summary>
        /// <param name="fullPath">The full path.</param>
        /// <returns>
        ///     Scan Result Message
        ///     Virus Existence True/False
        /// </returns>
        async Task<ScanResult> IScanVirusService.ScanFile(string fullPath)
        {
            var clam = new ClamClient(Constants.VirusScanURL, Constants.VirusScanPort);
            return MapScanResult(await clam.ScanFileOnServerAsync(fullPath));
        }

        /// <summary>
        /// Scans a stream for virus.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>
        ///     Scan Result Message
        ///     Virus Existence True/False
        /// </returns>
        async Task<ScanResult> IScanVirusService.ScanStream(Stream stream)
        {
            var clam = new ClamClient(Constants.VirusScanURL, Constants.VirusScanPort);
            return MapScanResult(await clam.SendAndScanFileAsync(stream));
        }

        /// <summary>
        /// Maps the virus scan result.
        /// </summary>
        /// <param name="scanresult">The scanresult.</param>
        /// <returns>
        ///     Scan Result Message
        ///     Virus Existence True/False 
        /// </returns>
        private ScanResult MapScanResult(ClamScanResult scanresult)
        {
            var result = new ScanResult();
            switch (scanresult.Result)
            {
                case ClamScanResults.Unknown:
                    result.Message = "Could not scan file";
                    result.IsVirusFree = false;
                    break;
                case ClamScanResults.Clean:
                    result.Message = "No Virus found";
                    result.IsVirusFree = true;
                    break;
                case ClamScanResults.VirusDetected:
                    result.Message = "Virus found: " + scanresult.InfectedFiles.First().VirusName;
                    result.IsVirusFree = false;
                    break;
                case ClamScanResults.Error:
                    result.Message = string.Format("VIRUS SCAN ERROR! {0}", scanresult.RawResult);
                    result.IsVirusFree = false;
                    break;
            }
            return result;
        }
    }
}
