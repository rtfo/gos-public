﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Models;
using DfT.GOS.GhgApplications.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.GhgApplications.Common.Commands;
using DfT.GOS.GhgApplications.API.Commands;
using DfT.GOS.GhgApplications.API.Models;
using DfT.GOS.Web.Models;

namespace DfT.GOS.GhgApplications.API.Services
{
    /// <summary>
    /// Definition for GHG Applications CRUD service
    /// </summary>
    public interface IApplicationsService
    {
        /// <summary>
        /// Creates a GHG application
        /// </summary>
        /// <param name="command">The command containing the details of the application to create</param>
        /// <param name="createdBy">The ID of the user creating the application</param>
        /// <returns>Returns the ID of the application that was created</returns>
        Task<int> CreateApplication(CreateApplicationCommand command, string createdBy);

        /// <summary>
        /// Gets summary details about an application 
        /// </summary>
        /// <param name="applicationId">The ID of the application to get</param>
        /// <returns>Returns the application summary</returns>
        Task<ApplicationSummary> GetApplicationSummary(int applicationId);

        /// <summary>
        /// Supporting document upload.
        /// </summary>
        /// <param name="command">File command</param>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="typeId">The extension typeId.</param>
        /// <param name="createdBy">The ID of the user that has uploaded the document</param>
        /// <returns>
        ///     Upload Status
        ///     Filename
        ///     ScanResult(Message, VirusFree true/False)
        /// </returns>
        Task<FileValidationResult> UploadSupportingDocument(AddSupportingDocumentCommand command, int typeId, int applicationId, string createdBy);

        /// <summary>
        /// Gets the uploaded supporting documents.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <returns>
        ///     DocumentId
        ///     Filename
        /// </returns>
        Task<List<UploadedSupportingDocument>> GetUploadedSupportingDocuments( int applicationId);

        /// <summary>
        /// Validate Supporting Document Metadata
        /// </summary>
        /// <param name="fileMetadata">the metadata of the file we're attempting to validate</param>
        /// <returns>Validation Results</returns>
        Task<FileValidationResult> ValidateFileMetadata(FileMetadata fileMetadata);

        /// <summary>
        /// Gets the uploaded supporting document for download.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumentId">The supporting document identifier.</param>
        /// <returns>
        ///     Filename 
        ///     File Contents
        /// </returns>
        Task<DownloadSupportingDocument> GetUploadedSupportingDocument(int applicationId, int supportingDocumentId);

        /// <summary>
        /// Removes the supporting document.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="supportingDocumentId">The supporting document identifier.</param>
        /// <returns>True/False</returns>
        Task<bool> RemoveSupportingDocument(int applicationId, int supportingDocumentId);

        /// <summary>
        /// Gets the application items for the related to the application
        /// </summary>
        /// <param name="applicationId">The application identifier</param>
        /// <returns>List of ApplicationItemSummary</returns>
        Task<IList<ApplicationItemSummary>> GetApplicationItems(int applicationId);

        /// <summary>
        /// Gets all application items that have had GHG credits issued for a company within an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>Returns the application items</returns>
        Task<IList<ApplicationItemSummary>> GetApplicationItemsWithIssuedCredits(int organisationId, int obligationPeriodId);

        /// <summary>
        /// Submit an application
        /// </summary>
        /// <param name="applicationId">AplicationId of the submitting Application</param>
        /// <param name="userId">userId of the user submitting Application</param>
        /// <returns>a message of error/warning or "Success"</returns>
        Task<string> SubmitApplication(int applicationId, string userId);

        /// <summary>
        /// Gets all the applications that are ready for review  
        /// </summary>
        /// <param name="applicationParameters">Parameters to filter and sort the applications by</param>
        /// <returns>Paged pending application list</returns>
        Task<PagedResult<ApplicationSummary>> GetPendingApplicationPagedResult(PendingApplicationParameters applicationParameters);

        /// <summary>
        /// Gets all the applications that are ready for issue  
        /// </summary>
        /// <param name="pageSize">No. of records per page</param>
        /// <param name="pageNumber">current page</param>
        /// <returns>Paged pending application list</returns>
        Task<PagedResult<ApplicationSummary>> GetApplicationsReadyForIssuePagedResult(int pageSize, int pageNumber);        

        /// <summary>
        /// Gets the application details for review
        /// </summary>
        /// <param name="applicationId">Application identifier</param>
        /// <returns>ApplicationReviewSummary</returns>
        Task<ApplicationReviewSummary> GetApplicationReviewSummary(int applicationId);

        /// <summary>
        /// Performs a updates the review section for volumes of an application
        /// </summary>
        /// <param name="command">The command containing details of the review</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> ReviewApplicationVolumes(ReviewApplicationVolumesCommand command);

        /// <summary>
        /// Performs a updates the review status of an application
        /// </summary>
        /// <param name="command">The command containing details of the review</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> ReviewApplication(ReviewApplicationCommand command);


        /// <summary>
        /// Performs a updates the review section for supporting documents of an application
        /// </summary>
        /// <param name="command">The command containing details of the review</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> ReviewApplicationSupportingDocuments(ReviewApplicationSupportingDocumentsCommand command);

        /// <summary>
        /// Performs a updates the review section for the calculation of an application
        /// </summary>
        /// <param name="command">The command containing details of the review</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> ReviewApplicationCalculation(ReviewApplicationCalculationCommand command);

        /// <summary>
        /// Approves an application
        /// </summary>
        /// <param name="command">The command containing details of the approval</param>
        /// <param name="userId">The ID of the user performing the approval</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> ApproveApplication(ApproveApplicationCommand command, string userId);

        /// <summary>
        /// Rejects an application
        /// </summary>
        /// <param name="command">The command containing details of the rejection</param>
        /// <param name="userId">The ID of the user performing the rejection</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> RejectApplication(RejectApplicationCommand command, string userId);

        /// <summary>
        /// Gets the applications for the supplier/oraganisation
        /// </summary>
        /// <param name="organisationId">Organisation Id</param>
        /// <param name="obligationId">Obligation period id</param>
        /// <param name="pageSize">No. of records per page</param>
        /// <param name="pageNumber">current page</param>
        /// <returns></returns>
        Task<PagedResult<ApplicationSummary>> GetSupplierApplications(int organisationId, int obligationId, int pageSize, int pageNumber);

        /// <summary>
        /// Issue credits to the approved applications
        /// </summary>
        /// <param name="issueCreditCommand">The command containing details of the issuer</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> IssueCredits(IssueCreditCommand issueCreditCommand);

        /// <summary>
        /// Revokes the specified application
        /// </summary>
        /// <param name="command">Command containing the Id of the application to revoke and the revocation reason</param>
        /// <param name="userId">Id of the user preforming the revoke</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> RevokeApplication(RevokeApplicationCommand command, string userId);

        /// <summary>
        /// Returns the performance data relating to applications
        ///  - No. of applications with credits issued for each calendar month
        ///  - No. of incomplete applications for each calendar month
        ///  - No. of applications rejected each calendar month
        ///  - No. of applications revoked each calendar month
        ///  - No. of applications submitted for each calendar month
        ///  - No. of applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently)
        ///  - Total No. of applications for the calendar month
        /// </summary>
        /// <returns>List of the above metrics for each month</returns>
        Task<List<ApplicationPerformanceData>> GetApplicationPerformanceData();
    }
}
