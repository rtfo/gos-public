﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Applications.API.Data;
using DfT.GOS.GhgApplications.API.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Npgsql.Logging;
using System;

namespace DfT.GOS.GhgApplications.API
{
    /// <summary>
    /// Entry point for the GOS Applications service
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entry point for the GOS GHG Applications Service
        /// </summary>
        /// <param name="args">any arguments</param>
        public static void Main(string[] args)
        {
            //Enable verbose logging for the npSQL (PostgreSQL) database driver
            NpgsqlLogManager.Provider = new ConsoleLoggingProvider(NpgsqlLogLevel.Warn, true, true);

            var host = BuildWebHost(args);

            //Migrate and Seed the database
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<GosApplicationsDataContext>();
                var configuration = services.GetRequiredService<IConfiguration>();

                try
                {
                    SeedData.Initialise(services);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred seeding the Applications DB.");
                }
            }

            host.Run();
        }

        /// <summary>
        /// Configures the service
        /// </summary>
        /// <param name="args">any arguaments</param>
        /// <returns>Returns the configured service</returns>
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://*:80")
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    config.AddEnvironmentVariables();
                })
                .Build();
    }
}
