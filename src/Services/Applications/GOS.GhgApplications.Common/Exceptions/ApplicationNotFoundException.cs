﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Runtime.Serialization;

namespace DfT.GOS.GhgApplications.Common.Exceptions
{
    /// <summary>
    /// An exception thrown when an application can not be found
    /// </summary>
    [Serializable]
    public class ApplicationNotFoundException : ApplicationException
    {
        /// <summary>
        /// Default Constructor for the ApplicationNotFoundException
        /// </summary>
        public ApplicationNotFoundException()
        {
        }

        /// <summary>
        /// Creates an ApplicationNotFoundException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public ApplicationNotFoundException(string message) : base(message)
        {
        }

        /// <summary>
        /// Creates an ApplicationNotFoundException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="inner">The inner exception</param>
        public ApplicationNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// Creates an ApplicationNotFoundException with serialized data
        /// </summary>
        /// <param name="info">The object that holds the serialized object data</param>
        /// <param name="context">The contextual information about the source or destination</param>
        protected ApplicationNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}