﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Runtime.Serialization;

namespace DfT.GOS.GhgApplications.Common.Exceptions
{
    /// <summary>
    /// An exception thrown when the client provides invalid data for a revoke request
    /// </summary>
    [Serializable]
    public class InvalidApplicationRevokeException : ApplicationException
    {
        /// <summary>
        /// Default Constructor for the InvalidApplicationRevokeException
        /// </summary>
        public InvalidApplicationRevokeException()
        {
        }

        /// <summary>
        /// Creates an InvalidApplicationRevokeException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public InvalidApplicationRevokeException(string message) : base(message)
        {
        }

        /// <summary>
        /// Creates an InvalidApplicationRevokeException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="inner">The inner exception</param>
        public InvalidApplicationRevokeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// Creates an InvalidApplicationRevokeException with serialized data
        /// </summary>
        /// <param name="info">The object that holds the serialized object data</param>
        /// <param name="context">The contextual information about the source or destination</param>
        protected InvalidApplicationRevokeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}