﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Runtime.Serialization;

namespace DfT.GOS.GhgApplications.Common.Exceptions
{
    /// <summary>
    /// An exception thrown when the service unexpectedly fails to perform an action
    /// </summary>
    [Serializable]
    public class ServiceException : ApplicationException
    {
        /// <summary>
        /// Default Constructor for the ServiceException
        /// </summary>
        public ServiceException()
        {
        }

        /// <summary>
        /// Creates a ServiceException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public ServiceException(string message) : base(message)
        {
        }

        /// <summary>
        /// Creates a ServiceException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="inner">The inner exception</param>
        public ServiceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// Creates a ServiceException with serialized data
        /// </summary>
        /// <param name="info">The object that holds the serialized object data</param>
        /// <param name="context">The contextual information about the source or destination</param>
        protected ServiceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}