﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.Common.Commands
{
    /// <summary>
    /// Command to update review status of the application
    /// </summary>
    public class ReviewApplicationCommand
    {
        /// <summary>
        /// The ID of the application
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Application status identifier
        /// </summary>
        [Required]
        public int ApplicationStatusId { get; set; }

        /// <summary>
        /// First level reviewer's id
        /// </summary>
        public string ApprovedBy { get; set; }

        /// <summary>
        /// Approval/Rejection date
        /// </summary>
        public DateTime ApprovedDate { get; set; }

        /// <summary>
        /// First level reviewer's ID
        /// </summary>
        public string RecommendedBy { get; set; }

        /// <summary>
        /// Recommended date
        /// </summary>
        public DateTime RecommendedDate { get; set; }

        /// <summary>
        /// First level reviewer's ID
        /// </summary>
        public string RejectedBy { get; set; }

        /// <summary>
        /// Rejected date
        /// </summary>
        public DateTime RejectedDate { get; set; }

        /// <summary>
        /// Constructor taking all the variables
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="applicationStatusId"></param>
        public ReviewApplicationCommand(int applicationId, int applicationStatusId)
        {
            this.ApplicationId = applicationId;
            this.ApplicationStatusId = applicationStatusId;
        }
    }
}
