﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.Common.Commands
{
    /// <summary>
    /// Command to mark an application as approved 
    /// </summary>
    public class ApproveApplicationCommand
    {
        #region Properties

        /// <summary>
        /// The ID of the application to approve
        /// </summary>
        [Required]
        public int ApplicationId { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="applicationId">The ID of the application to approve</param>
        public ApproveApplicationCommand(int applicationId)
        {
            this.ApplicationId = applicationId;
        }

        #endregion Constructors
    }
}
