﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using Microsoft.AspNetCore.Mvc;
using System;

namespace DfT.GOS.GhgApplications.Common.Commands
{
    /// <summary>
    /// PendingApplicationParameters defines the query parameters that can be used
    /// to filter the pending applications results
    /// </summary>
    public class PendingApplicationParameters
    {
        /// <summary>
        /// Page Number of results to display
        /// </summary>
        [FromQuery]
        public int Page { get; set; }

        /// <summary>
        /// Order to sort by, ascending or descending
        /// </summary>
        [FromQuery]
        public SortOrder? Order { get; set; }

        /// <summary>
        /// Number of results to return per page
        /// </summary>
        [FromQuery]
        public int PageSize { get; set; }

        /// <summary>
        /// Status of applications that should be shown
        /// </summary>
        [FromQuery]
        public ApplicationStatus ApplicationStatus { get; set; }

        /// <summary>
        /// Checks whether the application status provided is valid
        /// </summary>
        /// <returns>True if status has been provided, and is a valid ApplicationStatus</returns>
        public bool ApplicationStatusIsProvided()
        {
            return Enum.IsDefined(ApplicationStatus.GetType(), this.ApplicationStatus);
        }
    }

    /// <summary>
    /// SortOrder provides the possible sort orders (ascending and descending)
    /// </summary>
    public enum SortOrder
    {
        Ascending,
        Decending 
    }
}
