﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.Common.Commands
{
    /// <summary>
    /// Command to instruct the Application API to create a new Application with a single Application Item
    /// </summary>
    public class CreateApplicationCommand
    {
        #region Properties

        /// <summary>
        /// The Obligation ID of the Application
        /// </summary>
        [Required, Range(1, int.MaxValue)]
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// The ID of the supplier this application relates to
        /// </summary>
        [Required, Range(1, int.MaxValue)]
        public int SupplierOrganisationId { get; set; }

        /// <summary>
        /// The Amount of Fuel supplier
        /// </summary>
        [Required, Range(0.01, 1000000000000.00)]
        public decimal AmountSupplied { get; set; }

        /// <summary>
        /// The ID of the fuel type
        /// </summary>
        [Required, Range(1, int.MaxValue)]
        public int FuelTypeId { get; set; }

        /// <summary>
        /// The GHG density of the fuel
        /// </summary>
        [Range(-1000.00, 1000.00)]
        public decimal? GhgIntensity { get; set; }

        /// <summary>
        /// The Reference for UERs
        /// </summary>
        [StringLength(100, MinimumLength = 1)]
        public string Reference { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking values
        /// </summary>
        public CreateApplicationCommand(int obligationPeriodId
            , int supplierOrganisationId
            , decimal amountSupplied
            , int fuelTypeId
            , decimal? ghgIntensity
            , string reference)
        {
            this.ObligationPeriodId = obligationPeriodId;
            this.SupplierOrganisationId = supplierOrganisationId;
            this.AmountSupplied = amountSupplied;
            this.FuelTypeId = fuelTypeId;
            this.GhgIntensity = ghgIntensity;
            this.Reference = reference;
        }

        #endregion Constructors
    }
}
