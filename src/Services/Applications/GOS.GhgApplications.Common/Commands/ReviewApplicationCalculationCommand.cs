﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgApplications.Common.Commands
{
    /// <summary>
    /// Command for actioning a review of the application calculation
    /// </summary>
    public class ReviewApplicationCalculationCommand
    {
        #region Properties

        /// <summary>
        /// The ID of the application
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// The ID of the review status for the application volumes
        /// </summary>
        public int CalculationReviewStatusId { get; set; }

        /// <summary>
        /// The rejection reason, if applicable
        /// </summary>
        public string RejectionReason { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public ReviewApplicationCalculationCommand(int applicationId
            , int calculationReviewStatusId
            , string rejectionReason)
        {
            this.ApplicationId = applicationId;
            this.CalculationReviewStatusId = calculationReviewStatusId;
            this.RejectionReason = rejectionReason;
        }

        #endregion Constructors
    }
}
