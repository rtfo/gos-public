﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.Common.Commands
{
    /// <summary>
    /// Command to reject an application
    /// </summary>
    public class RejectApplicationCommand
    {
        #region Properties

        /// <summary>
        /// The ID of the application to reject
        /// </summary>
        [Required]
        public int ApplicationId { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="applicationId">The ID of the application to reject</param>
        public RejectApplicationCommand(int applicationId)
        {
            this.ApplicationId = applicationId;
        }

        #endregion Constructors
    }
}
