﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgApplications.Common.Commands
{
    /// <summary>
    /// Command to instruct the application API to submit an application
    /// </summary>
    public class SubmitApplicationCommand
    {
        #region Properties

        /// <summary>
        /// The ID of the application to submit
        /// </summary>
        public int ApplicationId { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all arguments
        /// </summary>
        /// <param name="applicationId">The ID of the application to submit</param>
        public SubmitApplicationCommand(int applicationId)
        {
            this.ApplicationId = applicationId;
        }

        #endregion Constructors
    }
}
