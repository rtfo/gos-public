﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.Common.Commands
{
    /// <summary>
    /// Command for revoking an application
    /// </summary>
    public class RevokeApplicationCommand
    {
        #region Properties

        /// <summary>
        /// The ID of the application
        /// </summary>
        [Required]
        public int ApplicationId { get; set; }

        /// <summary>
        /// The revocation reason, if applicable
        /// </summary>
        public string RevocationReason { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all arguments
        /// </summary>
        /// <param name="applicationId">The ID of the application to revoke</param>
        /// <param name="revocationReason">Reason for revocation</param>
        public RevokeApplicationCommand(int applicationId, string revocationReason)
        {
            this.ApplicationId = applicationId;
            this.RevocationReason = revocationReason;
        }

        #endregion Constructors
    }
}
