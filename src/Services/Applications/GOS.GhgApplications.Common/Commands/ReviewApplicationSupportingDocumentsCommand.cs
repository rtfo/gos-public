﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

/// <summary>
/// Command for actioning a review of the application supporting documents
/// </summary>
namespace DfT.GOS.GhgApplications.Common.Commands
{
    public class ReviewApplicationSupportingDocumentsCommand
    {
        #region Properties

        /// <summary>
        /// The ID of the application
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// The ID of the review status for the application supporting documents
        /// </summary>
        public int SupportingDocumentsReviewStatusId { get; set; }

        /// <summary>
        /// The rejection reason, if applicable
        /// </summary>
        public string RejectionReason { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public ReviewApplicationSupportingDocumentsCommand(int applicationId
            , int supportingDocumentsReviewStatusId
            , string rejectionReason)
        {
            this.ApplicationId = applicationId;
            this.SupportingDocumentsReviewStatusId = supportingDocumentsReviewStatusId;
            this.RejectionReason = rejectionReason;
        }

        #endregion Constructors
    }
}
