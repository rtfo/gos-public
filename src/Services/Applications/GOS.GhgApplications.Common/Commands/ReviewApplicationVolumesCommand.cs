﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgApplications.Common.Commands
{
    /// <summary>
    /// Command for actioning a review of the application volumes
    /// </summary>
    public class ReviewApplicationVolumesCommand
    {
        #region Properties

        /// <summary>
        /// The ID of the application
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// The ID of the review status for the application volumes
        /// </summary>
        public int VolumesReviewStatusId { get; set; }

        /// <summary>
        /// The rejection reason, if applicable
        /// </summary>
        public string RejectionReason { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public ReviewApplicationVolumesCommand(int applicationId
            , int volumeReviewStatusId
            , string rejectionReason)
        {
            this.ApplicationId = applicationId;
            this.VolumesReviewStatusId = volumeReviewStatusId;
            this.RejectionReason = rejectionReason;
        }

        #endregion Constructors
    }
}
