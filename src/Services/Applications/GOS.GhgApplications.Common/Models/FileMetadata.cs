﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgApplications.Common.Models
{
    /// <summary>
    /// metadata for an uploaded file
    /// </summary>
    public class FileMetadata
    {
        /// <summary>
        /// Gets or sets the file extension.
        /// </summary>
        /// <value>
        /// The file extension.
        /// </value>
        [Required]
        public string FileExtension { get; set; }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        [Required(AllowEmptyStrings = false)]
        public string FileName { get; set; }

        /// <summary>
        /// File Size in bytes
        /// </summary>
        [Required]
        public long FileSizeBytes { get; set; }
    }
}
