﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.Common.Models
{
    /// <summary>
    /// DTO for Application Review Status
    /// </summary>
    public class ReviewStatus
    {
        /// <summary>
        /// The Primary Key of the review status
        /// </summary>
        public int ReviewStatusId { get; set; }

        /// <summary>
        /// The name of the status
        /// </summary>
        public string Name { get; set; }
    }
}
