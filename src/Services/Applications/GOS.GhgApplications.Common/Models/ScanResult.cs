﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgApplications.Common.Models
{
    /// <summary>
    /// Virus Scan Result
    /// </summary>
    public class ScanResult
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the is virus free.
        /// </summary>
        /// <value>
        /// The is virus free.
        /// </value>
        public bool IsVirusFree { get; set; }
    }

}
