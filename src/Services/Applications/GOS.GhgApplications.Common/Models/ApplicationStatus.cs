﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgApplications.Common.Models
{
    /// <summary>
    /// Application status enum
    /// </summary>
    public enum ApplicationStatus
    {
        /// <summary>
        /// Application is Open
        /// </summary>
        [Display(Name = "Open")]
        Open = 1,

        /// <summary>
        /// Application Pending Verification
        /// </summary>
        [Display(Name = "Pending Verification")]
        PendingVerification = 2,

        /// <summary>
        /// Application is Submitted
        /// </summary>
        [Display(Name = "Submitted")]
        Submitted = 3,

        /// <summary>
        /// Application is Approved
        /// </summary>
        [Display(Name = "Approved")]
        Approved = 4,

        /// <summary>
        /// Application has GHG Credits Issued
        /// </summary>
        [Display(Name = "Issued")]
        GHGCreditsIssued = 6,

        /// <summary>
        /// Application is Rejected
        /// </summary>
        [Display(Name = "Rejected")]
        Rejected = 7,

        /// <summary>
        /// Application is Recommended for approval
        /// </summary>
        [Display(Name = "Recommended for approval")]
        RecommendApproval = 8,

        /// <summary>
        /// Application has been Revoked
        /// </summary>
        [Display(Name = "Revoked")]
        Revoked = 9
    }
}
