﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgApplications.Common.Models
{
    /// <summary>
    /// Application Performance Data
    /// </summary>
    public class ApplicationPerformanceData
    {
        #region Properties

        /// <summary>
        /// Gets or sets the date for the metrics.
        /// </summary>
        /// <value>
        /// The date for the metrics.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the number of applications with credits issued for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of applications with credits issued for the supplied date range.
        /// </value>
        public int NoOfApplicationsWithCreditsIssued { get; set; }

        /// <summary>
        /// Gets or sets the number of incomplete applications for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of incomplete applications for the supplied date range.
        /// </value>
        public int NoOfIncompleteApplications { get; set; }

        /// <summary>
        /// Gets or sets the number of applications rejected for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of applications rejected for the supplied date range.
        /// </value>
        public int NoOfRejectedApplications { get; set; }

        /// <summary>
        /// Gets or sets the number of applications revoked for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of applications revoked for the supplied date range.
        /// </value>
        public int NoOfRevokedApplications { get; set; }

        /// <summary>
        /// Gets or sets the number of applications submitted for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of applications submitted for the supplied date range.
        /// </value>
        public int NoOfSubmittedApplications { get; set; }

        /// <summary>
        /// Gets or sets the number of applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently).
        /// </summary>
        /// <value>
        /// No. of applications over the threshold time between application submission and credit issue, where the threshold is passed as a variable. (2 calendar months currently).
        /// </value>
        public int NoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue { get; set; }

        /// <summary>
        /// Gets or sets the total number of applications for the supplied date range.
        /// </summary>
        /// <value>
        /// Total number of applications for the supplied date range.
        /// </value>
        public int TotalNoOfApplications { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="date">Date</param>
        /// <param name="noOfApplicationsWithCreditsIssued">No. Of Applications With Credits Issued</param>
        /// <param name="noOfIncompleteApplications">No. Of Incomplete Applications</param>
        /// <param name="noOfRejectedApplications">No. Of Rejected Applications</param>
        /// <param name="noOfRevokedApplications">No. Of Revoked Applicationsd</param>
        /// <param name="noOfSubmittedApplications">No. Of Submitted Applications</param>
        /// <param name="noOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue">No. Of Applications Over Threshold Between Submission And Credit Issue</param>
        /// <param name="totalNoOfApplications">Total No. Of Applications</param>
        public ApplicationPerformanceData(DateTime date
            , int noOfApplicationsWithCreditsIssued
            , int noOfIncompleteApplications
            , int noOfRejectedApplications
            , int noOfRevokedApplications
            , int noOfSubmittedApplications
            , int noOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue
            , int totalNoOfApplications)
        {
            this.Date = date;
            this.NoOfApplicationsWithCreditsIssued = noOfApplicationsWithCreditsIssued;
            this.NoOfIncompleteApplications = noOfIncompleteApplications;
            this.NoOfRejectedApplications = noOfRejectedApplications;
            this.NoOfRevokedApplications = noOfRevokedApplications;
            this.NoOfSubmittedApplications = noOfSubmittedApplications;
            this.NoOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue = noOfApplicationsOverThresholdBetweenSubmissionAndCreditIssue;
            this.TotalNoOfApplications = totalNoOfApplications;
        }

        #endregion Constructors
    }
}
