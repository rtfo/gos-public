﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgApplications.Common.Models
{
    /// <summary>
    /// DTO for a GHG ApplicationItem
    /// </summary>
    public class ApplicationItemSummary
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="applicationId">The application identifier</param>
        /// <param name="applicationItemId">Application item identifier</param>
        /// <param name="fuelTypeId">The ID of the fuel type</param>
        /// <param name="reference">The reference of a UER application</param>
        /// <param name="amountSupplied">The amount of fuel supplied</param>
        /// <param name="ghgIntensity">The GHG Intensity of the application item</param>
        /// <param name="energyDensity">The energy density of the application item</param>
        public ApplicationItemSummary(int applicationId
            , int applicationItemId
            , int fuelTypeId
            , string reference
            , decimal? amountSupplied
            , decimal? ghgIntensity
            , decimal? energyDensity)
        {
            this.ApplicationId = applicationId;
            this.ApplicationItemId = applicationItemId;
            this.FuelTypeId = fuelTypeId;
            this.Reference = reference;
            this.AmountSupplied = amountSupplied;
            this.GhgIntensity = ghgIntensity;
            this.EnergyDensity = energyDensity;
        }

        /// <summary>
        /// The application identifier this application item is related to
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Application Item identifier
        /// </summary>
        public int ApplicationItemId { get; set; }

        /// <summary>
        /// The ID of the fuel type this application item is reporting
        /// </summary>
        public int FuelTypeId { get; set; }

        /// <summary>
        /// The UER Reference 
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// The amount of fuel supplied
        /// </summary>
        public decimal? AmountSupplied { get; set; }

        /// <summary>
        /// The GHG Intensity of the application item
        /// </summary>
        public decimal? GhgIntensity { get; set; }

        /// <summary>
        /// The energy density of the application item
        /// </summary>
        public decimal? EnergyDensity { get; set; }
    }
}
