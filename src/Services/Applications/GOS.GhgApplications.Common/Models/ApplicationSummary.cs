﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgApplications.Common.Models
{
    /// <summary>
    /// DTO for a GHG Application
    /// </summary>
    public class ApplicationSummary
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <param name="supplierOrganisationId">The organisation ID of the supplier for the application</param>
        /// <param name="obligationPeriodId">The application's Obligation Period ID</param>
        public ApplicationSummary(int applicationId
            , int supplierOrganisationId
            , int obligationPeriodId)
        {
            this.ApplicationId = applicationId;
            this.SupplierOrganisationId = supplierOrganisationId;
            this.ObligationPeriodId = obligationPeriodId;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// The ID of the application
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// The Organisation of the supplier for this application
        /// </summary>
        public int SupplierOrganisationId { get; set; }

        /// <summary>
        /// The Obligation Period ID for this application
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// The status id for this application
        /// </summary>
        public int ApplicationStatusId { get; set; }

        /// <summary>
        /// The date the application record was created
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// The date the application record was submitted
        /// </summary>
        public DateTime SubmittedDate { get; set; }


        /// <summary>
        /// The ID of the user who approved the application
        /// </summary>
        public string ApprovedBy { get; set; }

        /// <summary>
        /// The ID of the user who recommended the application for approval
        /// </summary>
        public string RecommendedBy { get; set; }

        /// <summary>
        /// The ID of the user who submits the application
        /// </summary>
        public string SubmittedBy { get; set; }

        /// <summary>
        /// Reason that the application was revoked, if is has been and if supplied
        /// </summary>
        public string RevocationReason { get; set; }

        #endregion Properties
    }
}
