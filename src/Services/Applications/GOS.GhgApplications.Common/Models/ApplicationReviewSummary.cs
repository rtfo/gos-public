﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Text;

namespace DfT.GOS.GhgApplications.Common.Models
{
    /// <summary>
    /// Represents the application review state
    /// </summary>
    public class ApplicationReviewSummary
    {
        /// <summary>
        /// The ID of the application
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public int ApplicationStatusId { get; set; }

        /// <summary>
        /// The current status of the application
        /// </summary>
        public ApplicationStatus ApplicationStatus { get; set; }

        /// <summary>
        /// The ID for the obligation period this application relates to
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// The organisation ID of the supplier that this application is for
        /// </summary>
        public int SupplierOrganisationId { get; set; }

        /// <summary>
        /// The review status of the application volumes section
        /// </summary>
        public ReviewStatus ApplicationVolumesReviewStatus { get; set; }

        /// <summary>
        /// The rejection reason, if application, for the volumes section of the application
        /// </summary>
        public string ApplicationVolumesRejectionReason { get; set; }

        /// <summary>
        /// The review status of the supporting documents section
        /// </summary>
        public ReviewStatus SupportingDocumentsReviewStatus { get; set; }

        /// <summary>
        /// The rejection reason, if application, for the supporting documents section of the application
        /// </summary>
        public string SupportingDocumentsRejectionReason { get; set; }

        /// <summary>
        /// The review status of the calculation section
        /// </summary>
        public ReviewStatus CalculationReviewStatus { get; set; }

        /// <summary>
        /// The rejection reason, if application, for the calculations section of the application
        /// </summary>
        public string CalculationRejectionReason { get; set; }

        /// <summary>
        /// The date the application record was submitted
        /// </summary>
        public Nullable<DateTime> SubmittedDate { get; set; }

        /// <summary>
        /// The ID of the user that the application was submitted by
        /// </summary>
        public string SubmittedBy { get; set; }

        /// <summary>
        /// The date the application is approved
        /// </summary>
        public Nullable<DateTime> ApprovedDate { get; set; }

        /// <summary>
        /// The ID of the user who approved the application
        /// </summary>
        public string ApprovedBy { get; set; }

        /// <summary>
        /// First level reviewer's ID
        /// </summary>
        public string RecommendedBy { get; set; }

        /// <summary>
        /// Recommended date
        /// </summary>
        public DateTime RecommendedDate { get; set; }

        /// <summary>
        /// Checks if any of the review statuses  (for volumes, documents or calculations)
        /// are null
        /// </summary>
        /// <returns>True if any are null</returns>
        public bool AreAnyReviewStatusesNull()
        {
            return ApplicationVolumesReviewStatus == null
                || SupportingDocumentsReviewStatus == null
                || CalculationReviewStatus == null;
        }
    }
}
