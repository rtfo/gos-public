﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Messaging.Models;

namespace DfT.GOS.RtfoApplications.Scheduler
{
    /// <summary>
    /// Application settings for the RtfoApplications Scheduler
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// Time interval in minutes
        /// </summary>
        public double TimeIntervalInMinutes { get; set; }

        /// <summary>
        /// Settings for the RabbitMQ messaging queue
        /// </summary>
        public RabbitMessagingClientSettings MessagingClient { get; set; }
    }
}
