﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using DfT.GOS.HealthChecks.Common;
using DfT.GOS.HealthChecks.Common.Extensions;
using DfT.GOS.HealthChecks.Rabbit;
using DfT.GOS.Messaging.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace DfT.GOS.RtfoApplications.Scheduler
{
    /// <summary>
    /// Configures the GOS RTFO Applications Scheduler service
    /// </summary>
    public class Startup
    {
        #region Constants
        /// <summary>
        /// The environment variable for the messaging client username
        /// </summary>
        public const string EnvironmentVariable_MessagingClientUsername = "MESSAGINGCLIENT:USERNAME";

        /// <summary>
        /// The environment variable for the messaging client password
        /// </summary>
        public const string EnvironmentVariable_MessagingClientPassword = "MESSAGINGCLIENT:PASSWORD";

        /// <summary>
        /// The error message to throw when messaging client user name environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_MessagingClientUsername = "Unable to get the Messaging Client User Name from the enironmental veriables.  Missing variable: " + EnvironmentVariable_MessagingClientUsername;

        /// <summary>
        /// The error message to throw when messaging client password environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_MessagingClientPassword = "Unable to get the Messaging Client Password from the enironmental veriables.  Missing variable: " + EnvironmentVariable_MessagingClientPassword;
        #endregion

        #region Properties
        private ILogger<Startup> Logger { get; set; }

        /// <summary>
        /// Gets the configuration settings
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="configuration">Configuration settings</param>
        /// <param name="logger">Logger instance</param>
        public Startup(IConfiguration configuration
            , ILogger<Startup> logger)
        {
            this.Logger = logger;
            this.Logger.LogInformation("Startup called for new instance of DfT.GOS.RtfoApplications.Scheduler");
            this.Configuration = configuration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Adds services to the container
        /// </summary>
        /// <param name="services">Container to which required services will be added</param>
        public void ConfigureServices(IServiceCollection services)
        {
            this.ValidateEnvironmentVariables();

            var appSettings = Configuration.Get<AppSettings>();
            services.Configure<AppSettings>(Configuration);

            services.AddHealthChecks()
                .AddCheck<RabbitHealthCheck>("rabbit", tags: new[] { HealthCheckTypes.Readiness });

            services.AddHostedService<BalanceCalculationHostedService>();

            // - Resolve Messaging Client
            // This needs to be a singleton so that the Rabbit connection is reused
            services.AddSingleton<IMessagingClient>(s => new RabbitMessagingClient(appSettings.MessagingClient, s.GetService<ILogger<RabbitMessagingClient>>()));

            // - Resolve Messaging Health Check
            // This needs to be a singleton so that the Rabbit connection is reused
            services.AddSingleton(s => new RabbitHealthCheck(RabbitMessagingClient.BuildConnectionFactory(appSettings.MessagingClient), s.GetService<ILogger<RabbitHealthCheck>>()));

            services.AddControllers().AddNewtonsoftJson();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Application pipeline to configure</param>
        /// <param name="env">Hosting environment</param>
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //Enable Health Checks
            app.UseGosHealthChecks();

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Validates that all expected environment variables have been supplied
        /// </summary>
        private void ValidateEnvironmentVariables()
        {
            //Messaging Client User name
            if (Configuration[EnvironmentVariable_MessagingClientUsername] == null)
            {
                throw new NullReferenceException(ErrorMessage_MessagingClientUsername);
            }

            //Messaging Client Password
            if (Configuration[EnvironmentVariable_MessagingClientPassword] == null)
            {
                throw new NullReferenceException(ErrorMessage_MessagingClientPassword);
            }
        }

        #endregion Methods
    }
}
