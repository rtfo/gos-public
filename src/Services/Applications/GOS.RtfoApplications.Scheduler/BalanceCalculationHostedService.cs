﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Messaging.Client;
using DfT.GOS.Messaging.Messages;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.RtfoApplications.Scheduler
{
    /// <summary>
    /// Balance calculation hosted background service
    /// </summary>
    public class BalanceCalculationHostedService : IHostedService, IDisposable
    {
        #region Properties

        private ILogger<BalanceCalculationHostedService> Logger { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }
        private IMessagingClient MessagingClient;
        private Timer Timer;

        #endregion Properties

        #region Constructor
        /// <summary>
        /// Constructor taking all the dependencies
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="messagingClient">Message client</param>
        /// <param name="appSettings">AppSettings</param>
        public BalanceCalculationHostedService(ILogger<BalanceCalculationHostedService> logger
            , IMessagingClient messagingClient
            , IOptions<AppSettings> appSettings)
        {
            this.Logger = logger;
            this.AppSettings = appSettings;
            this.MessagingClient = messagingClient;
        }
        #endregion


        #region StartAsync
        /// <summary>
        /// Contains the logic to start the background task. 
        /// StartAsync is called after the server has started and IApplicationLifetime.ApplicationStarted is triggered
        /// </summary>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            this.Logger.LogInformation("Balance calculation timed service is starting");
            this.Timer = new Timer(PublishRosUpdatedMessage
                , null, TimeSpan.Zero
                , TimeSpan.FromMinutes(AppSettings.Value.TimeIntervalInMinutes));

            return Task.CompletedTask;
        }

        #endregion

        /// <summary>
        /// Triggered when the host is performing a graceful shutdown. StopAsync contains the logic to end the background task.
        /// </summary>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <returns></returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.Logger.LogInformation("Timed Background Service is stopping.");

            this.Timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        #region IDisposable implementation
        /// <summary>
        /// Clean up resources
        /// </summary>
        public void Dispose()
        {
            this.Timer?.Dispose();
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Perform the task on based on configured the time interval
        /// </summary>
        /// <param name="state"></param>
        private void PublishRosUpdatedMessage(object state)
        {
            this.Logger.LogInformation("Timed Background Service is working.");
            try
            {
                Task.Run(async () =>
                {
                    this.Logger.LogInformation("Timed Background Service is updating the collection.");
                    
                    await this.MessagingClient.Publish(new RosUpdatedMessage());
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "An error occurred whilst subscribing to the GHG Balance message queues.");
            }

        }
        #endregion
    }
}
