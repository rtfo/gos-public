﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Organisations.API.Client.Models
{
    /// <summary>
    /// Represents an Organisation (Supplier, Trader, Verifier) in GOS
    /// </summary>
    public class Organisation
    {
        #region Enums

        /// <summary>
        /// Enum of organisation types
        /// </summary>
        public enum OrganisationTypes
        {
            Supplier = 1,
            Trader = 2,
            Verifier = 3
        }

        /// <summary>
        /// Enum of organisation statuses
        /// </summary>
        public enum OrganistionStatuses
        {
            Submitted = 1,
            NotSubmitted = 2,
            Approved = 3,
            Retired = 4,
            Suspecnded = 5
        }

        #endregion Enums

        #region Properties

        public int Id { get; private set; }
        public string Name { get; private set; }
        public int StatusId { get; private set; }
        public string Status { get; private set; }
        public int TypeId { get; private set; }
        public string Type { get; private set; }
        public bool IsSupplier { get { return this.TypeId == (int)OrganisationTypes.Supplier; } }
        public bool IsTrader { get { return this.TypeId == (int)OrganisationTypes.Trader; } }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Contructor for an organisation
        /// </summary>
        public Organisation(int id
            , string name
            , int statusId
            , string status
            , int typeId
            , string type)
        {
            this.Id = id;
            this.Name = name;
            this.StatusId = statusId;
            this.Status = status;
            this.TypeId = typeId;
            this.Type = type;
        }

        #endregion Constructors
    }
}
