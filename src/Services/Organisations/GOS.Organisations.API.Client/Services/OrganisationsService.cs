﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;
using System;
using DfT.GOS.Web.Models;

namespace DfT.GOS.Organisations.API.Client.Services
{
    /// <summary>
    /// Services for getting organisation details from a remote service
    /// </summary>
    public class OrganisationsService : HttpServiceClient, IOrganisationsService
    {
        private readonly IMemoryCache MemoryCache;
        private readonly MemoryCacheEntryOptions MemoryCacheEntryOptions;

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        /// <param name="memoryCache"></param>
        /// <param name="cacheEntryOptions"></param>
        public OrganisationsService(HttpClient httpClient, IMemoryCache memoryCache, MemoryCacheEntryOptions cacheEntryOptions)
            :base(httpClient)
        {
            this.MemoryCache = memoryCache;
            this.MemoryCacheEntryOptions = cacheEntryOptions;
        }

        #endregion Constructors

        #region IOrganisationsService Implementation

        /// <summary>
        /// Gets a list of all suppliers from the organisations service
        /// </summary>
        /// <param name="page">current page</param>
        /// <param name="resultsPerPage">No. of records per page</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <param name="searchText">Search filter text</param>
        public async Task<HttpObjectResponse<PagedResult<Organisation>>> GetSuppliers(int page, int resultsPerPage, string jwtToken, string searchText = "")
        {
            return new HttpObjectResponse<PagedResult<Organisation>>(
                await GetSupplier(page, resultsPerPage, jwtToken, searchText));
        }

        /// <summary>
        /// Gets a list of all suppliers
        /// </summary>
        /// <returns>Gets a list of all suppliers</returns>
        public async Task<HttpObjectResponse<IList<Organisation>>> GetSuppliers(string jwtToken)
        {
            return new HttpObjectResponse<IList<Organisation>>(await GetOrCreateSupplierCache(jwtToken));
        }

        /// <summary>
        /// Gets a single organisation from the organisations service
        /// </summary>
        /// <param name="id">The ID of the organisation to get</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Returns an organisation, if found</returns>
        public async Task<HttpObjectResponse<Organisation>> GetOrganisation(int id, string jwtToken)
        {
            return new HttpObjectResponse<Organisation>(await GetOrganisationCache(id, jwtToken));
        }

        /// <summary>
        /// Gets a list of all suppliers and traders from the organisations service
        /// </summary>
        /// <param name="page">current page</param>
        /// <param name="resultsPerPage">No. of records per page</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <param name="searchText">Search filter text</param>
        /// <param name="approvedOrganisationsOnly">Whether to only return organisations with "Active" status. Defaults to false, so all organisations are returned</param>
        public async Task<HttpObjectResponse<PagedResult<Organisation>>> GetSuppliersAndTraders(int page, int resultsPerPage, string jwtToken, bool approvedOrganisationsOnly, string searchText = "")
        {
            var suppliers = await GetOrganisation(page, resultsPerPage, jwtToken, approvedOrganisationsOnly, searchText);
            if(suppliers != null && suppliers.Result != null)
            {
                suppliers.Result = suppliers.Result.Where(x => x.IsSupplier || x.IsTrader).ToList();
            }
            return new HttpObjectResponse<PagedResult<Organisation>>(suppliers);
        }

        /// <summary>
        /// Gets a list of all suppliers and traders
        /// </summary>
        /// <returns>Gets a list of all suppliers and traders</returns>
        public async Task<HttpObjectResponse<IList<Organisation>>> GetSuppliersAndTraders(string jwtToken)
        {
            var list = await GetOrCreateOrganisationCache(jwtToken);
            list = list.Where(x => x.IsSupplier || x.IsTrader).ToList();
            return new HttpObjectResponse<IList<Organisation>>(list);
        }

        #endregion IOrganisationsService Implementation

        #region Private Methods

        /// <summary>
        /// Get the Organisation from cache if it exists
        /// </summary>
        /// <param name="id">The ID of the organisation to get</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        private async Task<Organisation> GetOrganisationCache(int id, string jwtToken)
        {
            var pagedResult = await GetOrCreateOrganisationCache(jwtToken);
            return pagedResult.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Get the Paged Supplier list from the cache if exists
        /// </summary>
        /// <param name="page">current page</param>
        /// <param name="resultsPerPage">No. of records per page</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <param name="searchText">Search filter text</param>
        /// <returns></returns>
        private async Task<PagedResult<Organisation>> GetSupplier(int page, int resultsPerPage, string jwtToken, string searchText = "")
        {
            var list = await GetOrCreateSupplierCache(jwtToken);

            var skipRecords = (page - 1) * resultsPerPage;
            var filteredResult = list.Where(x =>
            x.Name.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0);
            return new PagedResult<Organisation>()
            {
                TotalResults = filteredResult.Count(),
                PageNumber = page,
                PageSize = resultsPerPage,
                Result = filteredResult.Skip(skipRecords).Take(resultsPerPage).ToList()
            };
        }

        /// <summary>
        /// Get the Paged Organisation list from the cache if exists
        /// </summary>
        /// <param name="page">current page</param>
        /// <param name="resultsPerPage">No. of records per page</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <param name="searchText">Search filter text</param>
        /// <param name="approvedOrganisationsOnly">Whether to only return organisations with "Approved" status</param>
        /// <returns></returns>
        private async Task<PagedResult<Organisation>> GetOrganisation(int page, int resultsPerPage, string jwtToken, bool approvedOrganisationsOnly, string searchText = "")
        {
            var list = await GetOrCreateOrganisationCache(jwtToken);
            list = list.Where(x => x.IsSupplier || x.IsTrader).ToList().OrderBy(o=>o.Name).ToList();
            var skipRecords = (page - 1) * resultsPerPage;
            var filteredResult = list.Where(x =>
                x.Name.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0
                && (x.StatusId == (int)Organisation.OrganistionStatuses.Approved || !approvedOrganisationsOnly)
            );
            return new PagedResult<Organisation>()
            {
                TotalResults = filteredResult.Count(),
                PageNumber = page,
                PageSize = resultsPerPage,
                Result = filteredResult.Skip(skipRecords).Take(resultsPerPage).ToList()
            };
        }

        /// <summary>
        /// Get or create a cache entry for the Supplier list
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        private async Task<IList<Organisation>> GetOrCreateSupplierCache(string jwtToken)
        {
            if (!MemoryCache.TryGetValue("AllSuppliers", out IList<Organisation> suppliers))
            {
                var httpResult = await GetAllSuppliers(jwtToken);
                suppliers = httpResult.Result;
                MemoryCache.Set("AllSuppliers", suppliers, MemoryCacheEntryOptions);
            }
            return suppliers;
        }
        
        /// <summary>
        /// Gets all the suppliers
        /// </summary>
        /// <param name="jwtToken"></param>
        /// <returns></returns>
        private async Task<HttpObjectResponse<IList<Organisation>>> GetAllSuppliers(string jwtToken)
        {
            return await this.HttpGetAsync<IList<Organisation>>("api/v1/suppliers", jwtToken);
        }

        /// <summary>
        /// Get or create a cache entry for the Organisation list
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns></returns>
        private async Task<IList<Organisation>> GetOrCreateOrganisationCache(string jwtToken)
        {
            if (!MemoryCache.TryGetValue("AllOrganisations", out IList<Organisation> organisations))
            {
                var httpResult = await GetAllOrganisations(jwtToken);
                organisations = httpResult.Result;
                MemoryCache.Set("AllOrganisations", organisations, MemoryCacheEntryOptions);
            }
            return organisations;
        }

        /// <summary>
        /// Gets all the suppliers
        /// </summary>
        /// <param name="jwtToken"></param>
        /// <returns></returns>
        private async Task<HttpObjectResponse<IList<Organisation>>> GetAllOrganisations(string jwtToken)
        {
            return await this.HttpGetAsync<IList<Organisation>>("api/v1/organisations", jwtToken);
        }

        #endregion
    }
}
