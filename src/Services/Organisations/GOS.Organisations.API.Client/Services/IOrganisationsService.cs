﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.Http;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Web.Models;

namespace DfT.GOS.Organisations.API.Client.Services
{
    /// <summary>
    /// Definition for getting organisation details from a remote service
    /// </summary>
    public interface IOrganisationsService
    {
        /// <summary>
        /// Gets a list of all suppliers from the organisations service
        /// </summary>
        /// <param name="page">current page</param>
        /// <param name="resultsPerPage">No. of records per page</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Paged oraganisation list</returns>
        Task<HttpObjectResponse<PagedResult<Organisation>>> GetSuppliers(int page, int resultsPerPage, string jwtToken, string searchText);

        /// <summary>
        /// Gets a list of all suppliers
        /// </summary>
        /// <returns>Returns a list of all suppliers</returns>
        Task<HttpObjectResponse<IList<Organisation>>> GetSuppliers(string jwtToken);

        /// <summary>
        /// Gets the oraganisation details
        /// </summary>
        /// <param name="id">The identifier for the organisation</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>Oraganisation</returns>
        Task<HttpObjectResponse<Organisation>> GetOrganisation(int id, string jwtToken);

        /// <summary>
        /// Gets a list of all suppliers from the organisations service
        /// </summary>
        /// <param name="page">current page</param>
        /// <param name="resultsPerPage">No. of records per page</param>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <param name="searchText">Search text for the name of the organisation</param>
        /// <param name="approvedOrganisationsOnly">Whether to only return organisations with "Approved" status</param>
        /// <returns>Paged organisations list</returns>
        Task<HttpObjectResponse<PagedResult<Organisation>>> GetSuppliersAndTraders(int page, int resultsPerPage, string jwtToken, bool approvedOrganisationsOnly, string searchText);

        /// <summary>
        /// Gets a list of all suppliers and traders
        /// </summary>
        /// <returns>Returns a list of all suppliers and traders</returns>
        Task<HttpObjectResponse<IList<Organisation>>> GetSuppliersAndTraders(string jwtToken);
    }
}
