﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.IO;
using System.Reflection;
using DfT.GOS.Organisations.API.Repositories;
using DfT.GOS.Organisations.API.Repositories.ROS;
using DfT.GOS.Organisations.API.Repositories.Stub;
using DfT.GOS.Organisations.API.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AutoMapper;
using DfT.GOS.Security.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Collections.Generic;
using DfT.GOS.HealthChecks.Common.Extensions;
using DfT.GOS.HealthChecks.Common;
using Microsoft.OpenApi.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace GOS.Organisations.API
{
    /// <summary>
    /// The startup file for Organisations API
    /// </summary>
    public class Startup
    {
        #region Constants

        /// <summary>
        /// The environment variable to parse for the connection string
        /// </summary>
        public const string EnvironmentVariable_ConnectionString = "CONNECTION_STRING";

        /// <summary>
        /// The error message to throw when no connection string is supplied
        /// </summary>
        public const string ErrorMessage_ConnectionStringNotSupplied = "Unable to connect to the organisations datasource, the connection string was not specified. Missing variable: " + EnvironmentVariable_ConnectionString;

        /// <summary>
        /// The environment variable for the security JWT Token Audience used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenAudience = "JWT_TOKEN_AUDIENCE";

        /// <summary>
        /// The environment variable for the security JWT Token Issuer used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenIssuer = "JWT_TOKEN_ISSUER";

        /// <summary>
        /// The environment variable for the security JWT Token Key used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenKey = "JWT_TOKEN_KEY";

        /// <summary>
        /// The error message to throw when no JWT Token Audience environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenAudience = "Unable to get the JWT Token Audience from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenAudience;

        /// <summary>
        /// The error message to throw when no JWT Token Issuer environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenIssuer = "Unable to get the JWT Token Issuer from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenIssuer;

        /// <summary>
        /// The error message to throw when no JWT Token Key environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenKey = "Unable to get the JWT Token Key from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenKey;

        #endregion Constants

        #region Properties
        private ILogger<Startup> Logger { get; set; }

        /// <summary>
        /// Gets the configuration settings
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="configuration">The configuration settings</param>
        /// <param name="logger">The logger</param>
        public Startup(IConfiguration configuration
            , ILogger<Startup> logger)
        {
            this.Logger = logger;
            this.Logger.LogInformation("Startup called for new instance of DfT.GOS.Organisations.API");
            Configuration = configuration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            this.ValidateEnvironmentVariables();
            // - Resolve Service References
            services.AddTransient<IOrganisationsService, OrganisationsService>();

            // - Resolve Repository References
            var connectionString = Configuration[EnvironmentVariable_ConnectionString];

            if (connectionString == null)
            {
                throw new NullReferenceException(ErrorMessage_ConnectionStringNotSupplied);
            }
            else if (connectionString.ToLower().StartsWith("file="))
            {
                //Use the stub repository
                var jsonRepositoryPath = connectionString.Split("=")[1];
                services.AddTransient<IOrganisationsRepository>(r => new StubOrganisationRepository(jsonRepositoryPath));

                //Add standard health check (we don't have a DB to probe)
                services.AddHealthChecks();
            }
            else
            {
                //Use ROS as the repository

                //Add health check to probe the DB
                services.AddHealthChecks()
                    .AddSqlServer(connectionString, name: "sql-server", tags: new[] { HealthCheckTypes.Readiness });

                //Configure the db context, using retries for resilience
                services.AddDbContext<RosOrganisationsDataContext>(options =>
                {
                    options.UseSqlServer(connectionString
                        , sqlServerOptionsAction: sqlOptions =>
                            {
                                sqlOptions.EnableRetryOnFailure();
                            });
                });

                services.AddTransient<IOrganisationsRepository, RosOrganisationsRepository>();
            }

            //JWT Security Authentication
            var jwtSettings = new JwtAuthenticationSettings(Configuration[EnvironmentVariable_JwtTokenIssuer]
                , Configuration[EnvironmentVariable_JwtTokenAudience]
                , Configuration[EnvironmentVariable_JwtTokenKey]);

            var jwtConfigurer = new JwtAuthenticationConfigurer(jwtSettings);
            jwtConfigurer.Configure(services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            );

            // Register the Swagger generator
            try
            {
                //Swagger required services
                services.AddSingleton<IApiDescriptionGroupCollectionProvider, ApiDescriptionGroupCollectionProvider>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Organistations API", Version = "v1", Description = "Organisations are Companies that Trade GHG Credits (Traders), Supply Fuel or Energy (Suppliers), or are responsible for Verifying applications (Verifiers.)" });
                    c.IncludeXmlComments(xmlPath);
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { In = ParameterLocation.Header, Description = "Please enter JWT with Bearer into field in the format 'Bearer [JWT token]'", Name = "Authorization", Type = SecuritySchemeType.ApiKey });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement() {
                        {
                            new OpenApiSecurityScheme {
                                Reference = new OpenApiReference {
                                    Id = "Bearer"
                                    , Type = ReferenceType.SecurityScheme
                                },
                                Scheme = "bearer",
                                Name = "security",
                                In = ParameterLocation.Header
                            }, new List<string>()
                        }
                    });
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Register Swagger generator because of the following exception: " + ex.ToString());
            }

            services.AddControllers().AddNewtonsoftJson();

            services.AddAutoMapper();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            try
            {
                //Enable Health Checks
                app.UseGosHealthChecks();

                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Organisations V1");
                    c.RoutePrefix = string.Empty;
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Configure Swagger because of the following exception: " + ex.ToString());
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Validates that all expected environment variables have been supplied
        /// </summary>
        private void ValidateEnvironmentVariables()
        {
            //Connection String
            if (Configuration[EnvironmentVariable_ConnectionString] == null)
            {
                throw new NullReferenceException(ErrorMessage_ConnectionStringNotSupplied);
            }

            //JWT Token Audience
            if (Configuration[EnvironmentVariable_JwtTokenAudience] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenAudience);
            }

            //JWT Token Issuer
            if (Configuration[EnvironmentVariable_JwtTokenIssuer] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenIssuer);
            }

            //JWT Token Key
            if (Configuration[EnvironmentVariable_JwtTokenKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenKey);
            }
        }

        #endregion Methods
    }
}
