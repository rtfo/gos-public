﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DfT.GOS.Organisations.API.Models;
using DfT.GOS.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.Organisations.API.Repositories.ROS
{
    /// <summary>
    /// Repository for connecting into Orgnisations held within ROS
    /// </summary>
    public class RosOrganisationsRepository : IOrganisationsRepository
    {
        #region Properties
        private RosOrganisationsDataContext DataContext { get; set; }
        private IMapper Mapper { get; set; }

        #endregion Properties

        #region Constructors       

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataContext">The data context for ROS</param>
        /// <param name="mapper">The automapper mapper instane</param>
        public RosOrganisationsRepository(RosOrganisationsDataContext dataContext, IMapper mapper)
        {
            this.DataContext = dataContext;
            this.Mapper = mapper;
        }

        #endregion Constructors

        #region IOrganisationsRepository Implementation

        /// <summary>
        /// Gets a list of all organisations
        /// </summary>
        /// <returns>Returns a list of all organisations</returns>
        async Task<IList<Organisation>> IOrganisationsRepository.Get()
        {
            var organisationsView = await this.DataContext.GosOrganisationsView.ToListAsync();
            var organisations = this.Mapper.Map<IList<GosOrganisationsView>, IList<Organisation>>(organisationsView);
            return organisations;
        }

        /// <summary>
        /// Gets a list of all Suppliers
        /// </summary>
        /// <returns>Returns a list of all suppliers</returns>
        async Task<IList<Organisation>> IOrganisationsRepository.GetSuppliers()
        {
            var organisationsView = await this.DataContext.GosOrganisationsView
                                            .Where(c => c.TypeId == (int)Organisation.OrganisationTypes.Supplier)
                                            .OrderBy(o => o.Name)
                                            .ToListAsync();

            var organisations = this.Mapper.Map<IList<GosOrganisationsView>, IList<Organisation>>(organisationsView);

            return organisations;
        }

        /// <summary>
        /// Gets a paged list of organistations of a particular type, for example Supplier, Trader, Verifier
        /// </summary>
        /// <param name="searchText">Search text</param>
        /// <param name="organisationType">The type of organisation to filter on</param>
        /// <param name="pageSize">The number of records to return</param>
        /// <param name="pageNumber">The page number of results to return</param>
        /// <returns>Returns a paged list of organisations</returns>
        async Task<PagedResult<Organisation>> IOrganisationsRepository.Get(string searchText, string organisationType, int pageSize, int pageNumber)
        {
            var query = await this.DataContext.GosOrganisationsView
                                            .Where(o => o.Type.ToLower() == organisationType.ToLower()
                                             && o.Name.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0)
                                            .OrderBy(o => o.Name)
                                            .ToListAsync();

            var pagedResult = PagedResult.GetPagedResult(query, pageSize, pageNumber);
            return this.Mapper.Map<PagedResult<GosOrganisationsView>, PagedResult<Organisation>>(pagedResult);
        }

        /// <summary>
        /// Gets an organisation by it's unique ID
        /// </summary>
        /// <param name="id">The ID of the organisation to get</param>
        /// <returns>Returns an organisation, if it exists</returns>
        async Task<Organisation> IOrganisationsRepository.GetById(int id)
        {
            var organisationView = await this.DataContext.GosOrganisationsView.SingleOrDefaultAsync(o => o.Id == id);
            Organisation organisation = null;

            if (organisationView != null)
            {
                organisation = this.Mapper.Map<GosOrganisationsView, Organisation>(organisationView);
            }

            return organisation;
        }

        #endregion IOrganisationsRepository Implementation
    }
}
