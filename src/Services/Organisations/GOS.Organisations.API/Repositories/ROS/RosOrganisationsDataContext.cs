﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.Organisations.API.Repositories.ROS
{
    /// <summary>
    /// Database context for Organisations within ROS
    /// </summary>
    public class RosOrganisationsDataContext: DbContext
    {
        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="options">context options</param>
        public RosOrganisationsDataContext(DbContextOptions<RosOrganisationsDataContext> options)
           : base(options)
        {
        }

        /// <summary>
        /// Gets a reference to the Gos_OrganisationsView in ROS
        /// </summary>
        public DbSet<GosOrganisationsView> GosOrganisationsView { get; set; }
    }
}
