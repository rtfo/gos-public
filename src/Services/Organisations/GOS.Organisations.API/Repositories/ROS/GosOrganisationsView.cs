﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations.Schema;

namespace DfT.GOS.Organisations.API.Repositories.ROS
{
    /// <summary>
    /// DTO corresponding to the Gos_OrganisationsView in ROS database
    /// </summary>
    [Table("Gos_OrganisationsView")]
    public class GosOrganisationsView
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int StatusId { get; private set; }
        public string Status { get; private set; }
        public int TypeId { get; private set; }
        public string Type { get; private set; }
    }
}