﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Models;
using DfT.GOS.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Organisations.API.Repositories
{
    /// <summary>
    /// Interface contract for a repository to get organisation details.
    /// </summary>
    public interface IOrganisationsRepository
    {
        /// <summary>
        /// Gets an organisation by it's unique ID
        /// </summary>
        /// <param name="id">The ID of the organisation to get</param>
        /// <returns>Returns an organisation, if it exists</returns>
        Task<Organisation> GetById(int id);

        /// <summary>
        /// Gets a list of all organisations
        /// </summary>
        /// <returns>Returns a list of all organisations</returns>
        Task<IList<Organisation>> Get();

        /// <summary>
        /// Gets a list of all suppliers
        /// </summary>
        /// <returns></returns>
        Task<IList<Organisation>> GetSuppliers();

        /// <summary>
        /// Gets a paged list of organistations of a particular type, for example Supplier, Trader, Verifier
        /// </summary>
        /// <param name="searchText">Search text</param>
        /// <param name="organisationType">The type of organisation to filter on</param>
        /// <param name="pageSize">The number of records to return</param>
        /// <param name="pageNumber">The page number of results to return</param>
        /// <returns>Returns a paged list of organisations</returns>
        Task<PagedResult<Organisation>> Get(string searchText, string organisationType, int pageSize, int pageNumber);
    }
}
