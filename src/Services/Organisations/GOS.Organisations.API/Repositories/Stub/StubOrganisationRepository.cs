﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DfT.GOS.Organisations.API.Models;
using DfT.GOS.Web.Models;
using Newtonsoft.Json;

namespace DfT.GOS.Organisations.API.Repositories.Stub
{
    /// <summary>
    /// Provides a stub organisation repository, based upon a Json file, to enable testing the orgnisations without being connected to ROS.
    /// </summary>
    public class StubOrganisationRepository : IOrganisationsRepository
    {
        #region Properties

        private List<Organisation> Organisations { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Construct a stub organisation repository based upon a Json file
        /// </summary>
        public StubOrganisationRepository(string jsonFilePath)
        {
            //Read in organisations from json file
            var json = File.ReadAllText(jsonFilePath);
            this.Organisations = JsonConvert.DeserializeObject<List<Organisation>>(json);
        }

        #endregion Constructors

        #region IOrganisationsRepository Implementation

        /// <summary>
        /// Gets all organisations
        /// </summary>
        /// <returns>Returns a list of all organisations</returns>
        Task<IList<Organisation>> IOrganisationsRepository.Get()
        {
            return Task.Run<IList<Organisation>>(() =>
            {
                return this.Organisations;
            });
        }

        /// <summary>
        /// Gets all suppliers
        /// </summary>
        /// <returns>Returns a list of all suppliers</returns>
        Task<IList<Organisation>> IOrganisationsRepository.GetSuppliers()
        {
            return Task.Run<IList<Organisation>>(() =>
            {
                return this.Organisations.Where(o => o.TypeId == (int)Organisation.OrganisationTypes.Supplier)
                        .OrderBy(o => o.Name)
                        .ToList();
            });
        }

        /// <summary>
        /// Gets a paged list of organistations of a particular type, for example Supplier, Trader, Verifier
        /// </summary>
        /// <param name="searchText">Search text</param>
        /// <param name="organisationType">The type of organisation to filter on</param>
        /// <param name="pageSize">The number of records to return</param>
        /// <param name="pageNumber">The page number of results to return</param>
        /// <returns>Returns a paged list of organisations</returns>
        Task<PagedResult<Organisation>> IOrganisationsRepository.Get(string searchText, string organisationType, int pageSize, int pageNumber)
        {
            return Task.Run<PagedResult<Organisation>>(() =>
            {             
                var query = this.Organisations.Where(o => o.Type.ToLower() == organisationType.ToLower()
                                                && o.Name.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0)
                                              .OrderBy(o => o.Name);
           
                return PagedResult.GetPagedResult(query, pageSize, pageNumber);
            });
        }        

        /// <summary>
        /// Gets an organisation by it's unique ID
        /// </summary>
        /// <param name="id">The unique ID of the orgnisation</param>
        /// <returns>Returns an organisation by ID</returns>
        Task<Organisation> IOrganisationsRepository.GetById(int id)
        {
            return Task.Run<Organisation>(() =>
            {
                return this.Organisations.SingleOrDefault(o => o.Id == id);
            });
        }       

        #endregion IOrganisationsRepository Implementation
    }
}
