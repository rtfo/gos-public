﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.Organisations.API.Models;
using DfT.GOS.Organisations.API.Repositories;
using DfT.GOS.Web.Models;

namespace DfT.GOS.Organisations.API.Services
{
    /// <summary>
    /// Organisation Service
    /// </summary>
    public class OrganisationsService : IOrganisationsService
    {
        #region Properties

        private IOrganisationsRepository OrganisationsRepository {get;set;}

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="organisationsRepository">The repository holding organisation details</param>
        public OrganisationsService(IOrganisationsRepository organisationsRepository)
        {
            this.OrganisationsRepository = organisationsRepository;
        }

        #endregion Constructors

        #region IOrganisationsService implementation

        /// <summary>
        /// Get all organisations
        /// </summary>
        /// <returns>Returns all organisations</returns>
        public async Task<IList<Organisation>> Get()
        {
            return await this.OrganisationsRepository.Get();
        }

        /// <summary>
        /// Get all suppliers
        /// </summary>
        /// <returns>Returns all suppliers</returns>
        public async Task<IList<Organisation>> GetSuppliers()
        {
            return await this.OrganisationsRepository.GetSuppliers();
        }

        /// <summary>
        /// Get an organisation by Id
        /// </summary>
        /// <param name="id">The unique ID of the organisation</param>
        /// <returns>Returns an organisation</returns>
        public async Task<Organisation> GetById(int id)
        {
            return await this.OrganisationsRepository.GetById(id);
        }

        /// <summary>
        /// Gets all organisations of a particular type, for example Supplier, Trader, Verifier.
        /// Results are paged.
        /// </summary>
        /// <param name="searchText">Search text</param>
        /// <param name="type">The type of organisations to return</param>
        /// <param name="pageNumber">The page number of results to return</param>
        /// <param name="pageSize">The size number of results per page to return</param>
        /// <returns>Returns a list of matching organisations</returns>
        public async Task<PagedResult<Organisation>> Get(string searchText, string type, int pageSize, int pageNumber)
        {
            return await this.OrganisationsRepository.Get(searchText, type, pageSize, pageNumber);
        }

        #endregion IOrganisationsService implementation
    }
}
