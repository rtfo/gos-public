﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Models;
using DfT.GOS.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Organisations.API.Services
{
    /// <summary>
    /// Organisation Service interface definition
    /// </summary>
    public interface IOrganisationsService
    {
        /// <summary>
        /// Get an organisation by Id
        /// </summary>
        /// <param name="id">The unique ID of the organisation</param>
        /// <returns>Returns an organisation</returns>
        Task<Organisation> GetById(int id);

        /// <summary>
        /// Get all organisations
        /// </summary>
        /// <returns>Returns all organisations</returns>
        Task<IList<Organisation>> Get();

        /// <summary>
        /// Get all suppliers
        /// </summary>
        /// <returns>Returns all suppliers</returns>
        Task<IList<Organisation>> GetSuppliers();

        /// <summary>
        /// Gets all organisations of a particular type, for example Supplier, Trader, Verifier.
        /// Results are paged.
        /// </summary>
        /// <param name="searchText">Search text</param>
        /// <param name="type">The type of organisations to return</param>
        /// <param name="pageNumber">The page number of results to return</param>
        /// <param name="pageSize">The size number of results per page to return</param>
        /// <returns>Returns a list of matching organisations</returns>
        Task<PagedResult<Organisation>> Get(string searchText,string type, int pageSize, int pageNumber);
    }
}
