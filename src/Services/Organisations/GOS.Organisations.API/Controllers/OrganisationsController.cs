﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Organisations.API.Models;
using DfT.GOS.Organisations.API.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Organisations.API.Controllers
{

    /// <summary>
    /// Gets organisation details
    /// </summary>
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrganisationsController : ControllerBase
    {
        #region Properties

        private IOrganisationsService OrganisationsService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependancies
        /// </summary>
        /// <param name="organisationsService">The service</param>
        public OrganisationsController(IOrganisationsService organisationsService)
        {
            this.OrganisationsService = organisationsService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Returns a list of all organisations.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return all organisations:     /api/Organisations 
        /// </remarks>
        /// <returns>Returns a list of all organisations</returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            IList<Organisation> organisations = await this.OrganisationsService.Get();
            return Ok(organisations);
        }


        /// <summary>
        /// Returns a list of all organisations.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return all organisations:     /api/Organisations 
        /// </remarks>
        /// <returns>Returns a list of all organisations</returns>
        [HttpGet("/api/v1/suppliers")]
        public async Task<IActionResult> GetSuppliers()
        {
            IList<Organisation> organisations = await this.OrganisationsService.GetSuppliers();
            return Ok(organisations);
        }

        /// <summary>
        /// Returns a list of organisations filtered by type, for example Supplier, Verifier or Trader.
        /// </summary>
        /// <remarks>
        /// Sample requests:
        ///     - return all suppliers:         /api/v1/Organisations/type/supplier
        ///     - return all traders:           /api/v1/Organisations/type/trader
        ///     - return all verifiers:         /api/v1/Organisations/type/verifier
        /// </remarks>
        /// <param name="type">Parameter to filter organisations by type</param>
        /// <param name="search">Search text</param>
        /// <param name="pageSize">The number of results to return</param>
        /// <param name="pageNumber">The page number of results to return</param>
        /// <returns>Returns a list of organisations</returns>
        [HttpGet("type/{type}")]
        public async Task<IActionResult> GetByType( string type, [FromQuery]string search, [FromQuery] int pageSize = 20, [FromQuery] int pageNumber = 1)
        {
            if (type == null
                || type == string.Empty)
            {
                return BadRequest("Invalid organisation type supplied.");
            }
            else
            {
                var filterType = type.ToLower();
                if (filterType == "supplier"
                    || filterType == "trader"
                    || filterType == "verifier")
                {
                    var organisations = await this.OrganisationsService.Get(search ?? "", filterType, pageSize, pageNumber);
                    return Ok(organisations);
                }
                else
                {
                    return NotFound(type + " organisation type not recognised.");
                }
            }
        }

        /// <summary>
        /// Gets an organisation by unique ID.
        /// </summary>
        /// <param name="id">The ID of the organisation to get</param>
        /// <returns>Returns the organisation matching the supplier ID.</returns>
        /// <response code="200">Returns the organisation if found</response>
        ///  <response code="404">Returns if the organisation cannot be found</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var organisation = await this.OrganisationsService.GetById(id);

            if (organisation == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(organisation);
            }
        }

        #endregion Methods
    }
}
