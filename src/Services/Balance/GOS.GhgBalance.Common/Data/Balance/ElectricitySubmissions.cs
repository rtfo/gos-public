﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Electricity Submissions for a company in an obligation period
    /// </summary>
    public class ElectricitySubmissions
    {
        #region Properties

        /// <summary>
        /// The Total number of credits for the Electricity Submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The Total obligation for the Electricity Submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The Total amount Supplied for the Electricity Submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalAmountSupplied { get; private set; }

        /// <summary>
        /// The Electricity Submissions
        /// </summary>
        public IList<ElectricitySubmission> Submissions { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="submissions">A list of individual electricity submissions</param>
        /// <param name="totalCredits">The Total number of credits for the Electricity Submissions</param>
        /// <param name="totalObligation">The Total obligation for the Electricity Submissions</param>
        /// <param name="totalAmountSupplied">The Total amount Supplied for the Electricity Submissions</param>
        public ElectricitySubmissions(IList<ElectricitySubmission> submissions
            , decimal totalCredits
            , decimal totalObligation
            , decimal totalAmountSupplied)
        {
            this.Submissions = submissions;
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.TotalAmountSupplied = totalAmountSupplied;
        }

        #endregion Constructors
    }
}
