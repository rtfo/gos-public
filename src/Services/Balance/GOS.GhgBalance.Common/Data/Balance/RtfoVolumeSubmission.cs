﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// An individual Volume submission
    /// </summary>
    public class RtfoVolumeSubmission
    {
        #region Properties

        /// <summary>
        /// The reporting month the volume was submitted for
        /// </summary>
        public DateTime ReportingMonth { get; private set; }

        /// <summary>
        /// The ID of the Fuel Type
        /// </summary>
        public int FuelTypeId { get; private set; }

        /// <summary>
        /// The Fuel Type description
        /// </summary>
        public string FuelType { get; private set; }

        /// <summary>
        /// The volume of fuel (units)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Volume { get; set; }

        /// <summary>
        /// The Energy Density of fuel (MJ / unit)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public Nullable<decimal> EnergyDensity { get; set; }

        /// <summary>
        /// The GHG of the energy (gCO2 / MJ)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public Nullable<decimal> GhgIntensity { get; private set; }

        /// <summary>
        /// The difference to target CI (gCO2e / MJ)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal DifferenceToTarget { get; set; }

        /// <summary>
        /// The Energy Supplied in (MJ)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal EnergySupplied { get; set; }

        /// <summary>
        /// The number of credits produced (KgCO2e)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Credits { get; set; }

        /// <summary>
        /// The obligation produced (KgCO2e)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Obligation { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="reportingMonth">The reporting month the volume was submitted for</param>
        /// <param name="fuelTypeId">The ID of the Fuel Type</param>
        /// <param name="fuelType">The Fuel Type description</param>
        /// <param name="volume">The volume of fuel (units)</param>
        /// <param name="energyDensity">The Energy Density of fuel (MJ / unit)</param>
        /// <param name="ghgIntensity">The GHG of the energy (gCO2 / MJ)</param>
        /// <param name="differenceToTarget">The difference to target CI (gCO2e / MJ)</param>
        /// <param name="energySupplied">The Energy Supplied in (MJ)</param>
        /// <param name="credits">The number of credits produced (KgCO2e)</param>
        /// <param name="obligation">The obligation produced (KgCO2e)</param>
        public RtfoVolumeSubmission(DateTime reportingMonth
            , int fuelTypeId
            , string fuelType
            , decimal volume
            , Nullable<decimal> energyDensity
            , Nullable<decimal> ghgIntensity
            , decimal differenceToTarget
            , decimal energySupplied
            , decimal credits
            , decimal obligation)
        {
            this.ReportingMonth = reportingMonth;
            this.FuelTypeId = fuelTypeId;
            this.FuelType = fuelType;
            this.Volume = volume;
            this.EnergyDensity = energyDensity;
            this.GhgIntensity = ghgIntensity;
            this.DifferenceToTarget = differenceToTarget;
            this.EnergySupplied = energySupplied;
            this.Credits = credits;
            this.Obligation = obligation;
        }

        #endregion Constructors

    }
}
