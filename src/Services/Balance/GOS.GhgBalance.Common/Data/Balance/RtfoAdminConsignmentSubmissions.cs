﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Represents the total Admin Consignment submissions for a company in an obligation period
    /// </summary>
    public class RtfoAdminConsignmentSubmissions
    {
        #region Properties

        /// <summary>
        /// The Total number of credits for the Admin Consignment Submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The Total obligation for the Admin Consignment Submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The Total volume Supplied for the Admin Consignment Submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalVolumeSupplied { get; private set; }

        /// <summary>
        /// The Admin Consignment Submissions
        /// </summary>
        public IList<RtfoAdminConsignmentSubmission> Submissions { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="submissions">A list of individual Admin Consignment submissions</param>
        /// <param name="totalCredits">The Total number of credits for the Admin Consignment Submissions</param>
        /// <param name="totalObligation">The Total obligation for the Admin Consignment Submissions</param>
        /// <param name="totalVolumeSupplied">The Total volume Supplied for the Admin Consignment Submissions</param>
        public RtfoAdminConsignmentSubmissions(IList<RtfoAdminConsignmentSubmission> submissions
            , decimal totalCredits
            , decimal totalObligation
            , decimal totalVolumeSupplied)
        {
            this.Submissions = submissions;
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.TotalVolumeSupplied = totalVolumeSupplied;
        }

        #endregion Constructors
    }
}
