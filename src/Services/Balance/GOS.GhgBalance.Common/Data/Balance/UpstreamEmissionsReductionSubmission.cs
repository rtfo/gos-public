﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Represents a single Upstream Emissions Reduction (UER) submission
    /// </summary>
    public class UpstreamEmissionsReductionSubmission
    {
        #region Properties

        /// <summary>
        /// The ID of the GHG Application this was submitted for
        /// </summary>
        public int ApplicationId { get; private set; }

        /// <summary>
        /// The date of the submission
        /// </summary>
        public DateTime Date { get; private set; }

        /// <summary>
        /// The Unique Reference for the submission
        /// </summary>
        public string Reference { get; private set; }

        /// <summary>
        /// The Amount in KgCO2e of the submission
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Amount { get; private set; }

        /// <summary>
        /// The internal reference
        /// </summary>
        public string InternalReference { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="applicationId">The ID of the GHG Application this was submitted for</param>
        /// <param name="date">The date of the submission</param>
        /// <param name="reference">The Unique Reference for the submission</param>
        /// <param name="amount">The Amount in KgCO2e of the submission</param>
        /// <param name="internalReference">The internal reference</param>
        public UpstreamEmissionsReductionSubmission(int applicationId
            , DateTime date
            , string reference
            , decimal amount
            , string internalReference)
        {
            this.ApplicationId = applicationId;
            this.Date = date;
            this.Reference = reference;
            this.Amount = amount;
            this.InternalReference = internalReference;
        }

        #endregion Constructors
    }
}
