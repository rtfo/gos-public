﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Represents submissions for a company within an obligation period
    /// </summary>
    public class Submissions
    {
        #region Properties

        /// <summary>
        /// The total number of credits for this obligation
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The total obligation for this obligation period
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// Liquid and Gas Submissions
        /// </summary>
        public LiquidGasSubmissions LiquidGasSubmissions { get; private set; }

        /// <summary>
        /// Electricity Submissions
        /// </summary>
        public ElectricitySubmissions ElectricitySubmissions { get; private set; }

        /// <summary>
        /// Upstream Emissions Reduction (UER) submissions
        /// </summary>
        public UpstreamEmissionsReductionSubmissions UpstreamEmissionsReductionSubmissions { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor for Unit Testing
        /// </summary>
        public Submissions()
        {
        }

        /// <summary>
        /// Constructor taking all parameters
        /// </summary>
        /// <param name="liquidGasSubmissions">Liquid and Gas Submissions</param>
        /// <param name="electricitySubmissions">Electricity Submissions</param>
        /// <param name="upstreamEmissionsReductionSubmissions">Upstream Emissions Reduction (UER) submissions</param>
        /// <param name="totalCredits">The total number of credits from submissions</param>
        /// <param name="totalObligation">The total obligation from submissions</param>
        public Submissions(LiquidGasSubmissions liquidGasSubmissions
            , ElectricitySubmissions electricitySubmissions
            , UpstreamEmissionsReductionSubmissions upstreamEmissionsReductionSubmissions
            , decimal totalCredits
            , decimal totalObligation)
        {
            this.LiquidGasSubmissions = liquidGasSubmissions;
            this.ElectricitySubmissions = electricitySubmissions;
            this.UpstreamEmissionsReductionSubmissions = upstreamEmissionsReductionSubmissions;
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
        }

        #endregion Constructors
    }
}
