﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Trading Ledger information
    /// </summary>
    public class Trading
    {
        #region Properties

        /// <summary>
        /// Gets the total balance of trades
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Balance { get; private set; }

        /// <summary>
        /// Gets the Credit Events for Trading
        /// </summary>
        public virtual IList<CreditEvent> CreditEvents { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default constructor for Unit Testing
        /// </summary>
        public Trading()
        {
        }

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="creditEvents">The credit events</param>
        /// <param name="balance">Gets the total balance of trades</param>
        public Trading(IList<CreditEvent> creditEvents
            , decimal balance)
        {
            this.CreditEvents = creditEvents;
            this.Balance = balance;
        }

        #endregion Constructors
    }
}
