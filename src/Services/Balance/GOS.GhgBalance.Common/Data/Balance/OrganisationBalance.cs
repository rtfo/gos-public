﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// The Data Structure for the Organisation Balance View
    /// </summary>
    public class OrganisationBalance
    {
        #region Properties

        /// <summary>
        /// The Unique ID of the Organisation Balance
        /// </summary>
        [BsonId]
        public OrganisationBalanceUniqueKey Id { get; set; }

        /// <summary>
        /// The date the Organisation Balance record was generated
        /// </summary>
        public DateTime _generationDate { get; internal set; }

        /// <summary>
        /// The ID of the Obligation Period this balance corresponds to
        /// </summary>
        public int ObligationPeriodId { get; internal set; }

        /// <summary>
        /// The End Date of the Obligation Period this balance corresponds to
        /// </summary>
        public DateTime? ObligationPeriodEndDate { get; internal set; }

        /// <summary>
        /// The ID of the Organisation this balance corresponds to
        /// </summary>
        public int OrganisationId { get; internal set; }

        /// <summary>
        /// The name of the organisation this balance corresponds to
        /// </summary>
        public string OrganisationName { get; internal set; }

        /// <summary>
        /// The Submissions information (GOS Applications and ROS Applications) for the balance
        /// </summary>
        public virtual Submissions Submissions { get; internal set; }

        /// <summary>
        /// The Trading transactions for the balance
        /// </summary>
        public virtual Trading Trading { get; internal set; }

        /// <summary>
        /// The total credits for the company in the obligation period 
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalCredits { get; internal set; }

        /// <summary>
        /// The total obligation for the company in the obligation period
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalObligation { get; internal set; }

        /// <summary>
        /// The Net balance (Total Credits - Total Obligation)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal NetBalance { get; internal set; }

        /// <summary>
        /// The buyout cost per tonne of CO2 equivalent
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal BuyOutPer_tCO2e {get; internal set;}

        /// <summary>
        /// The buyout cost for the company in the obligation period
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal BuyOutCost { get; internal set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default constructor for unit testing
        /// </summary>
        public OrganisationBalance()
        {
            this._generationDate = DateTime.Now;
        }

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the Obligation Period this balance corresponds to</param>
        /// <param name="obligationPeriodEndDate">The End Date of the Obligation Period this balance corresponds to</param>
        /// <param name="organisationId">The ID of the Organisation this balance corresponds to</param>
        /// <param name="organisationName">The name of the organisation this balance corresponds to</param>
        /// <param name="submissions">The Submissions information (GOS Applications and ROS Applications) for the balance</param>
        /// <param name="trading">The Trading transactions for the balance</param>
        /// <param name="totalCredits">The total credits for the company in the obligation period</param>
        /// <param name="totalObligation">The total obligation for the company in the obligation period</param>
        /// <param name="netBalance">The Net balance (Total Credits - Total Obligation)</param>
        /// <param name="buyOutPer_tCO2e">The buyout cost per tonne of CO2 equivalent</param>
        /// <param name="buyOutCost">The buyout cost for the company in the obligation period</param>
        public OrganisationBalance(int obligationPeriodId
            , DateTime? obligationPeriodEndDate
            , int organisationId
            , string organisationName
            , Submissions submissions
            , Trading trading
            , decimal totalCredits
            , decimal totalObligation
            , decimal netBalance
            , decimal buyOutPer_tCO2e
            , decimal buyOutCost)
        {
            this.Id = new OrganisationBalanceUniqueKey(organisationId, obligationPeriodId);
            this._generationDate = DateTime.Now;
            this.ObligationPeriodId = obligationPeriodId;
            this.ObligationPeriodEndDate = obligationPeriodEndDate;
            this.OrganisationId = organisationId;
            this.OrganisationName = organisationName;
            this.Submissions = submissions;
            this.Trading = trading;
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.NetBalance = netBalance;
            this.BuyOutPer_tCO2e = buyOutPer_tCO2e;
            this.BuyOutCost = buyOutCost;
        }

        #endregion Constructors
    }
}
