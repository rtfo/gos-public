﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Represents all Upstream Emissions Reduction (UER) submissions for a company within an obligation period
    /// </summary>
    public class UpstreamEmissionsReductionSubmissions
    {
        #region Properties

        /// <summary>
        /// The total number of UER credits
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The list of UER submissions
        /// </summary>
        public IList<UpstreamEmissionsReductionSubmission> Submissions { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="submissions">The list of UER submissions</param>
        /// <param name="totalCredits">The total number of UER credits</param>
        public UpstreamEmissionsReductionSubmissions(IList<UpstreamEmissionsReductionSubmission> submissions
            , decimal totalCredits)
        {
            this.Submissions = submissions;
            this.TotalCredits = totalCredits;
        }

        #endregion Constructors
    }
}
