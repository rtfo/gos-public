﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// An individual Electricity Submission
    /// </summary>
    public class ElectricitySubmission
    {
        #region Properties

        /// <summary>
        /// The ID of the application the submission relates to
        /// </summary>
        public int ApplicationId { get; private set; }

        /// <summary>
        /// The date of the submissions
        /// </summary>
        public DateTime Date { get; private set; }

        /// <summary>
        /// The unique reference for the submission
        /// </summary>
        public int Reference { get; private set; }

        /// <summary>
        /// The amount supplied
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal AmountSupplied { get; private set; }

        /// <summary>
        /// The GHG of the energy (gCO2 / MJ)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal GhgIntensity { get; private set; }

        /// <summary>
        /// The difference to target CI (gCO2e / MJ)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal DifferenceToTarget { get; set; }

        /// <summary>
        /// The Energy Supplied in (MJ)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal EnergySupplied { get; set; }

        /// <summary>
        /// The number of credits produced (KgCO2e)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Credits { get; set; }

        /// <summary>
        /// The obligation produced (KgCO2e)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Obligation { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="applicationId">The ID of the application the submission relates to</param>
        /// <param name="date">The date of the submissions</param>
        /// <param name="reference">The unique reference for the submission</param>
        /// <param name="amountSupplied">The amount supplied</param>
        /// <param name="ghgIntensity">The GHG of the energy (gCO2 / MJ)</param>
        /// <param name="differenceToTarget">The difference to target CI (gCO2e / MJ)</param>
        /// <param name="energySupplied">The Energy Supplied in (MJ)</param>
        /// <param name="credits">The number of credits produced (KgCO2e)</param>
        /// <param name="obligation">The obligation produced (KgCO2e)</param>
        public ElectricitySubmission(int applicationId
            , DateTime date
            , int reference
            , decimal amountSupplied
            , decimal ghgIntensity
            , decimal differenceToTarget
            , decimal energySupplied
            , decimal credits
            , decimal obligation)
        {
            this.ApplicationId = applicationId;
            this.Date = date;
            this.Reference = reference;
            this.AmountSupplied = amountSupplied;
            this.GhgIntensity = ghgIntensity;
            this.DifferenceToTarget = differenceToTarget;
            this.EnergySupplied = energySupplied;
            this.Credits = credits;
            this.Obligation = obligation;
        }

        #endregion Constructors
    }
}
