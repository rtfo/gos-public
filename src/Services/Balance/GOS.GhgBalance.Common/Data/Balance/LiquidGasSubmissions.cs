﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Represents the total Liquid and Gas submissions for a company in an obligation period
    /// </summary>
    public class LiquidGasSubmissions
    {
        #region Properties

        /// <summary>
        /// The Admin Consignment Submissions
        /// </summary>
        public RtfoAdminConsignmentSubmissions RtfoAdminConsignmentSubmissions { get; private set; }

        /// <summary>
        /// The Volume Submissions
        /// </summary>
        public RtfoVolumeSubmissions RtfoVolumeSubmissions { get; private set; }

        /// <summary>
        /// The Fossil Gas Submissions
        /// </summary>
        public FossilGasSubmissions FossilGasSubmissions { get; private set; }

        /// <summary>
        /// The total credits for liquid and gas submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The total obligation for liquid and gas submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The aggregated volume supplied across all items (Units)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalVolumeOfFuelSupplied { get; private set; }

        /// <summary>
        /// The volume of fuel above the GHG target
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal VolumeOfFuelAboveTheGhgTarget { get; private set; }

        /// <summary>
        /// The low volume supplier deduction to be taken off the supplier's obligation for an obligation period (KgCO2e)
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal LowVolumeSupplierDeduction { get; private set; }

        /// <summary>
        /// The Deminimis in litres, used to work out low volume supplier deduction
        /// </summary>
        public decimal DeMinimisLitres { get; private set; }

        /// <summary>
        /// The No-Deminimis in litres, used to work out low volume supplier deduction
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal NoDeMinimisLitres { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="rtfoAdminConsignmentSubmissions">The Admin Consignment Submissions</param>
        /// <param name="rtfoVolumeSubmissions">The Volume Submissions</param>
        /// <param name="fossilGasSubmissions">The Fossil Gas Submissions</param>
        /// <param name="lowVolumeSupplierDeduction">The low volume supplier deduction to be taken off the supplier's obligation for an obligation period (KgCO2e)</param>
        /// <param name="totalVolumeOfFuelSupplied">The total volume of fuel supplied</param>
        /// <param name="volumeOfFuelAboveTheGhgTarget">The total volume of fuel above the GHG Target</param>
        /// <param name="totalCredits">The total number of credits</param>
        /// <param name="totalObligation">The total obligation</param>
        /// <param name="deMinimisLitres">The Deminimis in litres, used to work out low volume supplier deduction</param>
        /// <param name="noDeMinimisLitres">The No-Deminimis in litres, used to work out low volume supplier deduction</param>
        public LiquidGasSubmissions(RtfoAdminConsignmentSubmissions rtfoAdminConsignmentSubmissions
            , RtfoVolumeSubmissions rtfoVolumeSubmissions
            , FossilGasSubmissions fossilGasSubmissions
            , decimal lowVolumeSupplierDeduction
            , decimal totalVolumeOfFuelSupplied
            , decimal volumeOfFuelAboveTheGhgTarget
            , decimal totalCredits
            , decimal totalObligation
            , decimal deMinimisLitres
            , decimal noDeMinimisLitres)
        {
            this.RtfoAdminConsignmentSubmissions = rtfoAdminConsignmentSubmissions;
            this.RtfoVolumeSubmissions = rtfoVolumeSubmissions;
            this.FossilGasSubmissions = fossilGasSubmissions;
            this.LowVolumeSupplierDeduction = lowVolumeSupplierDeduction;
            this.TotalVolumeOfFuelSupplied = totalVolumeOfFuelSupplied;
            this.VolumeOfFuelAboveTheGhgTarget = volumeOfFuelAboveTheGhgTarget;
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.DeMinimisLitres = deMinimisLitres;
            this.NoDeMinimisLitres = noDeMinimisLitres;
        }

        #endregion Constructors
    }
}
