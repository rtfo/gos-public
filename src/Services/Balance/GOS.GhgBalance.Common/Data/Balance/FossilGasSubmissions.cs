﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Fossil Gas submissions for a company in an obligation period
    /// </summary>
    public class FossilGasSubmissions
    {
        #region Properties

        /// <summary>
        /// The Total number of credits for the Fossil Gas Submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The Total obligation for the Fossil Gas Submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The Total volume Supplied for the Fossil Gas Submissions
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal TotalVolumeSupplied { get; private set; }

        /// <summary>
        /// The Fossil Gas Submissions
        /// </summary>
        public IList<FossilGasSubmission> Submissions { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="submissions">A list of individual fossil gas submissions</param>
        /// <param name="totalCredits">The Total number of credits for the Fossil Gas Submissions</param>
        /// <param name="totalObligation">The Total obligation for the Fossil Gas Submissions</param>
        /// <param name="totalVolumeSupplied">The Total volume Supplied for the Fossil Gas Submissions</param>
        public FossilGasSubmissions(IList<FossilGasSubmission> submissions
            , decimal totalCredits
            , decimal totalObligation
            , decimal totalVolumeSupplied)
        {
            this.Submissions = submissions;
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.TotalVolumeSupplied = totalVolumeSupplied;
        }

        #endregion Constructors
    }
}
