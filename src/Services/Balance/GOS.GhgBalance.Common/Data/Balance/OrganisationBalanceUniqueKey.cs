﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Represents a unique key for a Organisation Balance record
    /// </summary>
    public class OrganisationBalanceUniqueKey
    {
        #region Properties

        /// <summary>
        /// Gets the Organisation ID
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// Gets the Obligation Period ID
        /// </summary>
        public int ObligationPeriodId { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all unique components of the key
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        public OrganisationBalanceUniqueKey(int organisationId, int obligationPeriodId)
        {
            this.OrganisationId = organisationId;
            this.ObligationPeriodId = obligationPeriodId;
        }

        #endregion Constructors
    }
}
