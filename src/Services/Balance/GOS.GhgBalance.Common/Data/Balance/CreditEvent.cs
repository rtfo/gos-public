﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DfT.GOS.GhgBalance.Common.Data.Balance
{
    /// <summary>
    /// Represents a single credit event from the GHG Ledger
    /// </summary>
    public class CreditEvent
    {
        #region Properties

        /// <summary>
        /// The unique reference for the credit event
        /// </summary>
        public int Reference { get; private set; }

        /// <summary>
        /// The date / time of the credit event
        /// </summary>
        public DateTime Date { get; private set; }

        /// <summary>
        /// The ID of the credit event type
        /// </summary>
        public int EventTypeId { get; private set; }

        /// <summary>
        /// The credit event type
        /// </summary>
        public string EventType { get; private set; }

        /// <summary>
        /// The ID of the other organisation in the trade event
        /// </summary>
        public int? TradingOrganisationId { get; private set; }

        /// <summary>
        /// The name of the other organisation in the trade event
        /// </summary>
        public string TradingOrganisation { get; private set; }


        /// <summary>
        /// The number of credits that were traded
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Credits { get; private set; }

        /// <summary>
        /// The additional reference for the credit event
        /// </summary>
        public string AdditionalReference { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="reference">The unique reference for the credit event</param>
        /// <param name="date">The date / time of the credit event</param>
        /// <param name="eventTypeId">The ID of the credit event type</param>
        /// <param name="eventType">The credit event type</param>
        /// <param name="tradingOrganisationId">The ID of the other organisation in the trade event</param>
        /// <param name="tradingOrganisation">The name of the other organisation in the trade event</param>
        /// <param name="credits">The number of credits that were traded</param>
        /// <param name="additionalReference">Optional additional reference for the credit event</param>
        public CreditEvent(int reference
            , DateTime date
            , int eventTypeId
            , string eventType
            , int? tradingOrganisationId
            , string tradingOrganisation
            , decimal credits
            , string additionalReference)
        {
            this.Reference = reference;
            this.Date = date;
            this.EventTypeId = eventTypeId;
            this.EventType = eventType;
            this.TradingOrganisationId = tradingOrganisationId;
            this.TradingOrganisation = tradingOrganisation;
            this.Credits = credits;
            this.AdditionalReference = additionalReference;
        }

        #endregion Constructors
    }
}
