﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Text;

namespace DfT.GOS.GhgBalance.Common.Data.Administration
{
    /// <summary>
    /// Indicates the status of a Collection Administration bulk update job
    /// </summary>
    public enum CollectionAdministrationStatus
    {
        InProgress,
        Succeeded,
        Failed
    }
}
