﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace DfT.GOS.GhgBalance.Common.Data.Administration
{
    /// <summary>
    /// Provides metadata about a mongo collection
    /// </summary>
    public class CollectionAdministration
    {
        #region Private Variables

        /// <summary>
        /// Gets/sets the time the update should timeout/be disregarded
        /// </summary>
        public DateTime TimeoutCancellation { get; set; }

        #endregion Private Variables

        #region Public Properties

        /// <summary>
        /// The Unique ID of the Organisation Balance Collection
        /// </summary>
        [BsonId]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the date/time the bulk load was started
        /// </summary>
        public DateTime LoadStarted { get; set; }        

        /// <summary>
        /// Gets the date/time the bulk load was created
        /// </summary>
        public Nullable<DateTime> LoadSuccessfullyCompleted { get; set; }

        /// <summary>
        /// A list of errors that occured during the bulk load
        /// </summary>
        public List<string> Errors { get; set; } = new List<string>();

        /// <summary>
        /// Indicates whether errors occured when performing the bulk load
        /// </summary>
        public bool HasErrors { get { return this.Errors.Count > 0; } }

        /// <summary>
        /// Indicates whether the operation has timed out
        /// </summary>
        public bool HasTimedOut { get { return DateTime.Now > this.TimeoutCancellation; } }

        /// <summary>
        /// Status indicator for operation
        /// </summary>
        public CollectionAdministrationStatus Status { get; set; }

        /// <summary>
        /// Status description for operation
        /// </summary>
        public string StatusDescription { get { return this.Status.ToString(); } set { /* intentionally left blank - required so that property is persisted */ } }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="id">The Unique ID of the Organisation Balance Collection</param>
        /// <param name="loadStarted">Gets the date/time the bulk load was started</param>
        /// <param name="timeoutCancellation">The date/time the bulk updload should timeout / be disregarded</param>
        public CollectionAdministration(Guid id
            , DateTime loadStarted
            , DateTime timeoutCancellation)
        {
            this.Id = id;
            this.LoadStarted = loadStarted;
            this.TimeoutCancellation = timeoutCancellation;
            this.Status = CollectionAdministrationStatus.InProgress;
        }

        #endregion Constructors
    }
}
