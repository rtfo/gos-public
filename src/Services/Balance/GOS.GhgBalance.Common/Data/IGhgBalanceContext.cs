﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Driver;
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalance.Common.Data.Administration;
using System;

namespace DfT.GOS.GhgBalance.Common.Data
{
    /// <summary>
    /// Definition for the Mongo DB Data Context
    /// </summary>
    public interface IGhgBalanceContext
    {
        /// <summary>
        /// The collection used to administer Organisation Balances for Green/Blue collection loading
        /// </summary>
        IMongoCollection<CollectionAdministration> OrganisationBalancesCollectionAdministration { get; }

        /// <summary>
        /// The collection of Organisation Balances (Organisation / Obligation Period Specific)
        /// </summary>
        IMongoCollection<OrganisationBalance> OrganisationBalances { get; }

        /// <summary>
        /// Gets a temporary Organisation Balances collection by some unique key.
        /// 
        /// This method is used to access temporary collections when Green/Blue collection loading
        /// </summary>
        /// <param name="collectionKey">The unique ID of the temporary collection</param>
        /// <returns>Returns the collection associated with the key</returns>
        IMongoCollection<OrganisationBalance> GetTempOrganisationBalances(Guid collectionKey);
    }
}
