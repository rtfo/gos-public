﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Administration;
using DfT.GOS.GhgBalance.Common.Data.Balance;
using MongoDB.Driver;
using System;

namespace DfT.GOS.GhgBalance.Common.Data
{
    /// <summary>
    /// Mongo DB Data Context for GHG Balance data
    /// </summary>
    public class GhgBalanceContext : IGhgBalanceContext
    {
        #region Constants

        public const string MasterCollectionName = "OrganisationBalances";
        public const string TemporaryCollectionPrefix = MasterCollectionName + "_";

        #endregion Constants

        #region Properties

        private IMongoDatabase Database { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="mongoClient">The mongo client used to connect to mongo</param>
        /// <param name="databaseName">The database name to connect to</param>
        public GhgBalanceContext(IMongoClient mongoClient, string databaseName)
        {
            this.Database = mongoClient.GetDatabase(databaseName);
        }

        #endregion Constructors

        #region IGhgBalanceContext Implementation

        /// <summary>
        /// The collection used to administer Organisation Balances for Green/Blue collection loading
        /// </summary>
        IMongoCollection<CollectionAdministration> IGhgBalanceContext.OrganisationBalancesCollectionAdministration => this.Database.GetCollection<CollectionAdministration>("OrganisationBalancesCollectionAdministration");

        /// <summary>
        /// Gets the Organisation Balance information
        /// </summary>
        IMongoCollection<OrganisationBalance> IGhgBalanceContext.OrganisationBalances => this.Database.GetCollection<OrganisationBalance>(MasterCollectionName);

        /// <summary>
        /// Gets a temporary Organisation Balances collection by some unique key.
        /// 
        /// This method is used to access temporary collections when Green/Blue collection loading
        /// </summary>
        /// <param name="collectionKey">The unique ID of the temporary collection</param>
        /// <returns>Returns the collection associated with the key</returns>
        IMongoCollection<OrganisationBalance> IGhgBalanceContext.GetTempOrganisationBalances(Guid collectionKey)
        {
            return this.Database.GetCollection<OrganisationBalance>(TemporaryCollectionPrefix + collectionKey.ToString());
        }

        #endregion IGhgBalanceContext Implementation
    }
}
