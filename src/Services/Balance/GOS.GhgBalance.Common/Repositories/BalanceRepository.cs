﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data;
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.GhgBalance.Common.Services;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalance.Common.Repositories
{
    /// <summary>
    /// Provides a MongoDb repository for GHG Balances
    /// </summary>
    public class BalanceRepository : IBalanceRepository
    {
        #region Properties

        /// <summary>
        /// The data context
        /// </summary>
        protected IGhgBalanceContext Context { get; set; }

        private const int dontInclude = 0;
        private const int include = 1;

        private const int orderDescending = -1;
        private const int orderAscending = 1;

        private const int defaultPageSize = 10;
        private const int firstPage = 1;

        /// <summary>
        /// Gets the mongo collection for the repository
        /// </summary>
        protected virtual IMongoCollection<OrganisationBalance> Collection { get { return this.Context.OrganisationBalances; } }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="context">The data context used by the repository to get data</param>
        public BalanceRepository(IGhgBalanceContext context)
        {
            this.Context = context;
        }

        #endregion Constructors

        #region IBalanceRepository Implementation

        /// <summary>
        /// Saves an organisation balance to the repository, either inserting it or updating it
        /// </summary>
        /// <param name="organisationBalance">The organisation balance to save</param>
        /// <returns>Returns true if the object was save successfully, otherwise false</returns>
        async Task<bool> IBalanceRepository.Save(OrganisationBalance organisationBalance)
        {
            //Check the timestamp - only update if we've got a newer timestamp (or there is a new record)
            var updated = false;
            var existingRecord = await this.Get(organisationBalance.OrganisationId, organisationBalance.ObligationPeriodId);

            if (existingRecord == null
                || organisationBalance._generationDate > existingRecord._generationDate)
            {
                var result = await this.Collection.ReplaceOneAsync(b => b.Id == organisationBalance.Id
                                                                    , organisationBalance
                                                                    , new UpdateOptions() { IsUpsert = true });
                updated = result.IsAcknowledged;
            }

            return updated;
        }

        /// <summary>
        /// Gets a single Organisation Balance for an organisation / Obligation Period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation to get the balance for</param>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <returns>Returns a single organisation Balance, if found, otherwise Null</returns>
        Task<OrganisationBalance> IBalanceRepository.Get(int organisationId, int obligationPeriodId)
        {
            return this.Get(organisationId, obligationPeriodId);
        }

        /// <summary>
        /// Gets Organisation Balance for all organisations for a Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <returns>Returns organisation Balance, if found for all organisations</returns>
        Task<List<OrganisationBalance>> IBalanceRepository.GetAll(int obligationPeriodId)
        {
            return this.GetAll(obligationPeriodId);
        }

        /// <summary>
        /// Gets total credit and obligation balance for all organisations for a Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <returns>Returns the total balance across the system, if found for all organisations</returns>
        Task<TotalBalanceSummary> IBalanceRepository.GetTotal(int obligationPeriodId)
        {
            return this.GetTotal(obligationPeriodId);
        }

        /// <summary>
        ///  Gets a list of credit transfers for all organisations for a specific Obligation Period with filtering on organisation name
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <param name="searchTerm">Search term to filter organisation name on</param>
        /// <param name="pageNumber">The number of the page currently on</param>
        /// <param name="pageSize">Size of page to return results for</param>
        /// <returns>Returns the result wrapped with info for paging</returns>
        async Task<ResultSet<CreditTransferSummary>> IBalanceRepository.GetCreditTransfers(int obligationPeriodId, string searchTerm, int pageNumber, int pageSize)
        {
            var documentsQuery = await this.GetCreditTransfersQuery(obligationPeriodId, searchTerm);
            var transfersPageSize = pageSize;
            var transfersPageNumber = pageNumber;

            if (transfersPageSize < 1)
            {
                transfersPageSize = defaultPageSize;
            }

            if (transfersPageNumber < 1)
            {
                transfersPageNumber = firstPage;
            }

            var skipRecords = (transfersPageNumber - 1) * transfersPageSize;
            var countPagingStage = MongoHelper.PageCountStages(skipRecords, transfersPageSize);
            PipelineDefinition<OrganisationBalance, ResultSet<CreditTransferSummary>> pipeline = documentsQuery.Concat(countPagingStage).ToArray();

            return await Collection.Aggregate(pipeline, new AggregateOptions() { AllowDiskUse = true }).SingleAsync();
        }

        /// <summary>
        ///  Gets a list of credit transfers for all organisations for a specific Obligation Period with filtering on organisation name
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <param name="searchTerm">Search term to filter organisation name on</param>
        /// <returns>Returns the full result</returns>
        async Task<IList<CreditTransferSummary>> IBalanceRepository.GetCreditTransfers(int obligationPeriodId, string searchTerm)
        {
            PipelineDefinition<OrganisationBalance, CreditTransferSummary> pipeline = await this.GetCreditTransfersQuery(obligationPeriodId, searchTerm);

            return await Collection.Aggregate(pipeline, new AggregateOptions() { AllowDiskUse = true }).ToListAsync();
        }

        /// <summary>
        /// Deletes a single organisation balance
        /// </summary>
        /// <param name="organisationId">The ID of the organisation to delete the organisation balance for</param>
        /// <param name="obligationPeriodId">The ID of the obligation period to delete the organisation balance for</param>
        /// <returns>Returns true if the delete was successful, otherwise false</returns>
        async Task<bool> IBalanceRepository.Delete(int organisationId, int obligationPeriodId)
        {
            var result = await this.Collection.DeleteOneAsync(b => b.Id == new OrganisationBalanceUniqueKey(organisationId, obligationPeriodId));

            return result.IsAcknowledged
                && result.DeletedCount > 0;
        }

        /// <summary>
        /// Drops the repository collection
        /// </summary>
        /// <returns>Returns true if the operation was successful, otherwise false</returns>
        async Task<bool> IBalanceRepository.Drop()
        {
            await this.Collection.Database.DropCollectionAsync(this.Collection.CollectionNamespace.CollectionName);
            return true;
        }

        /// <summary>
        /// Renames the collection to be the master collection
        /// </summary>
        /// <returns>Returns true if the operation was successful, otherwise false</returns>
        Task<bool> IBalanceRepository.UpdateToMaster()
        {
            return this.UpdateToMaster();
        }

        #endregion IBalanceRepository Implementation

        #region Protected Methods

        /// <summary>
        /// Renames the collection to be the master collection
        /// </summary>
        /// <returns>Returns true if the operation was successful, otherwise false</returns>
        protected virtual Task<bool> UpdateToMaster()
        {
            throw new NotSupportedException("Cannot update the Master balance repository to itself!");
        }

        /// <summary>
        /// Gets a single Organisation Balance for an organisation / Obligation Period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation to get the balance for</param>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <returns>Returns a single organisation Balance, if found, otherwise Null</returns>
        protected Task<OrganisationBalance> Get(int organisationId, int obligationPeriodId)
        {
            return this.Collection.Find(b => b.Id == new OrganisationBalanceUniqueKey(organisationId, obligationPeriodId)).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Gets Organisation Balance for all organisations for a Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <returns>Returns organisation Balance, if found for all organisations, otherwise Null</returns>
        protected Task<List<OrganisationBalance>> GetAll(int obligationPeriodId)
        {
            return this.Collection.Find(b => b.ObligationPeriodId == obligationPeriodId).ToListAsync();
        }

        /// <summary>
        /// Gets total credit and obligation balance for all organisations for a Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <returns>Returns the total balance across the system, if found for all organisations</returns>
        protected Task<TotalBalanceSummary> GetTotal(int obligationPeriodId)
        {
            return this.Collection.Aggregate()
                .Match(doc => doc.ObligationPeriodId == obligationPeriodId)
                .Group(doc => doc.ObligationPeriodId,
                    g => new TotalBalanceSummary(
                        g.Sum(orgs => orgs.TotalCredits)
                        , g.Sum(orgs => orgs.TotalObligation)
                        , g.Sum(orgs => orgs.NetBalance)
                    )
                ).SingleOrDefaultAsync();
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Gets the query to be executed for Credit Transfers
        /// </summary>
        /// <param name="obligationPeriodId">The obligation period we're reporting on</param>
        /// <param name="searchTerm">Any search term for filtering on</param>
        /// <returns></returns>
        private async Task<BsonDocument[]> GetCreditTransfersQuery(int obligationPeriodId, string searchTerm)
        {
            var filterStage = new BsonDocument("$match", new BsonDocument()); /// Empty documents passed into match are equivalent to match everything

            if (!string.IsNullOrEmpty(searchTerm))
            {
                var expression = new BsonRegularExpression(Regex.Escape(searchTerm), "i");

                filterStage = new BsonDocument("$match", new BsonDocument()
                    .Add("$or", new BsonArray()
                        .Add(new BsonDocument()
                            .Add("OrganisationName", new BsonDocument()
                                .Add("$regex", expression)
                            )
                        )
                        .Add(new BsonDocument()
                            .Add("Trading.CreditEvents.TradingOrganisation", new BsonDocument()
                                .Add("$regex", expression)
                            )
                        )
                ));
            }

            BsonDocument[] documentsQuery = new BsonDocument[]
            {
                MongoHelper.Match("ObligationPeriodId",obligationPeriodId),/// Filter for ObligationPeriodId
                MongoHelper.Project(new Dictionary<string, BsonValue>
                {
                    ["_id"] = include,
                    ["OrganisationName"] = include,
                    ["OrganisationId"] = include,
                    ["ObligationPeriodId"] = include,
                    ["Trading"] = include,
                }),
                MongoHelper.Unwind("$Trading.CreditEvents"),
                filterStage,
                MongoHelper.Sort(new KeyValuePair<string, int>("Trading.CreditEvents.Credits", orderAscending)), // Order ascending to get the trade out (-ve) record
                MongoHelper.FirstInEachGroup("$Trading.CreditEvents.Reference","Reference"),
                MongoHelper.Project(new Dictionary<string, BsonValue>(){
                    ["_id"] = dontInclude,
                    ["OrganisationName"] = "$doc.OrganisationName",
                    ["OrganisationId"] = "$doc._id.OrganisationId",
                    ["ObligationPeriodId"] = "$doc._id.ObligationPeriodId",
                    ["Reference"] = "$doc.Trading.CreditEvents.Reference",
                    ["Date"] = "$doc.Trading.CreditEvents.Date",
                    ["EventTypeId"] = "$doc.Trading.CreditEvents.EventTypeId",
                    ["EventType"] = "$doc.Trading.CreditEvents.EventType",
                    ["TradingOrganisationId"] = "$doc.Trading.CreditEvents.TradingOrganisationId",
                    ["TradingOrganisationName"] = "$doc.Trading.CreditEvents.TradingOrganisation",
                    ["Credits"] = new BsonDocument("$abs", "$doc.Trading.CreditEvents.Credits"), // Show the trade out (-ve) as positive
                    ["AdditionalReference"] = "$doc.Trading.CreditEvents.AdditionalReference"
                }),
                MongoHelper.Sort(new KeyValuePair<string, int>("Date", orderDescending), new KeyValuePair<string, int>("Reference", orderDescending))
            };

            return documentsQuery;
        }

        #endregion Private Methods
    }
}
