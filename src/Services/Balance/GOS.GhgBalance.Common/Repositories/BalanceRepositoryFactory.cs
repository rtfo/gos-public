﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data;
using System;

namespace DfT.GOS.GhgBalance.Common.Repositories
{
    /// <summary>
    /// Factory class used to manage instances of balance collections used in Green/Blue loading
    /// </summary>
    public class BalanceRepositoryFactory : IBalanceRepositoryFactory
    {
        #region Properties

        private IGhgBalanceContext Context { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="context">The database connection</param>
        public BalanceRepositoryFactory(IGhgBalanceContext context)
        {
            this.Context = context;
        }

        #endregion Constructors

        #region IBalanceRepositoryFactory Implementation

        /// <summary>
        /// Gets an instance of a repository pointing to the master balance collection
        /// </summary>
        /// <returns>Returns the master collection repository</returns>
        IBalanceRepository IBalanceRepositoryFactory.GetMasterRepository()
        {
            return new BalanceRepository(this.Context);
        }

        /// <summary>
        /// Gets an instance of a repository pointing at the temporary collection with the supplied temporyCollectionId
        /// </summary>
        /// <param name="temporyCollectionId">The ID of the balance view collection to get</param>
        /// <returns>Returns a repository pointing at the correct collection</returns>
        IBalanceRepository IBalanceRepositoryFactory.GetTemporaryRepository(Guid temporyCollectionId)
        {
            return new TemporaryBalanceRepository(temporyCollectionId, this.Context);
        }

        #endregion IBalanceRepositoryFactory Implementation
    }
}
