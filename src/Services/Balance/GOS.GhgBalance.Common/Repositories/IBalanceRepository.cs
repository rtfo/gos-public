﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalance.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalance.Common.Repositories
{
    /// <summary>
    /// Definition for a GHG Balance Repository
    /// </summary>
    public interface IBalanceRepository
    {
        /// <summary>
        /// Saves an organisation balance to the repository, either inserting it or updating it
        /// </summary>
        /// <param name="organisationBalance">The organisatin balance to save</param>
        /// <returns>Returns true if the object was save successfully, otherwise false</returns>
        Task<bool> Save(OrganisationBalance organisationBalance);

        /// <summary>
        /// Gets a single Organisation Balance for an organisation / Obligation Period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation to get the balance for</param>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <returns>Returns a single organisation Balance, if found, otherwise Null</returns>
        Task<OrganisationBalance> Get(int organisationId, int obligationPeriodId);

        /// <summary>
        /// Gets Organisation Balance for all organisations for a Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <returns>Returns organisation Balance, if found for all organisations</returns>
        Task<List<OrganisationBalance>> GetAll(int obligationPeriodId);

        /// <summary>
        /// Gets total credit and obligation balance for all organisations for a Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the balance for</param>
        /// <returns>Returns the total balance across the system, if found for all organisations</returns>
        Task<TotalBalanceSummary> GetTotal(int obligationPeriodId);

        /// <summary>
        /// Deletes a single organisation balance
        /// </summary>
        /// <param name="organisationId">The ID of the organisation to delete the organisation balance for</param>
        /// <param name="obligationPeriodId">The ID of the obligation period to delete the organistion balance for</param>
        /// <returns>Returns true if the delete was successful, otherwise false</returns>
        Task<bool> Delete(int organisationId, int obligationPeriodId);

        /// <summary>
        /// Drops the repository collection
        /// </summary>
        /// <returns>Returns true if the operation was successful, otherwise false</returns>
        Task<bool> Drop();

        /// <summary>
        /// Renames the collection to be the master collection
        /// </summary>
        /// <returns>Returns true if the operation was successful, otherwise false</returns>
        Task<bool> UpdateToMaster();

        /// <summary>
        /// Gets Credit Transfer Summary result for all organisations for an Obligation Period and the count
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the credit transfers for</param>
        /// <param name="searchTerm">The search term to filter organisations names on</param>
        /// <param name="pageSize">The size of the page to return</param>
        /// <param name="pageNumber">The page of results to return</param>
        /// <returns>Returns list of Credit Transfer Summarys and count</returns>
        Task<ResultSet<CreditTransferSummary>> GetCreditTransfers(int obligationPeriodId, string searchTerm, int pageNumber, int pageSize);

        /// <summary>
        /// Gets Credit Transfer Summary result for all organisations for an Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the credit transfers for</param>
        /// <param name="searchTerm">The search term to filter organisations names on</param>
        /// <returns>Returns list of Credit Transfer Summarys</returns>
        Task<IList<CreditTransferSummary>> GetCreditTransfers(int obligationPeriodId, string searchTerm);
    }
}