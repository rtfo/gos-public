﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.GhgBalance.Common.Data;
using DfT.GOS.GhgBalance.Common.Data.Administration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DfT.GOS.GhgBalance.Common.Repositories
{
    /// <summary>
    /// Repository for administering balance collections for Green/blue loading
    /// </summary>
    public class BalanceCollectionAdministrationRepository : IBalanceCollectionAdministrationRepository
    {
        #region Properties

        private IGhgBalanceContext Context { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="context">The data context used by the repository to get data</param>
        public BalanceCollectionAdministrationRepository(IGhgBalanceContext context)
        {
            this.Context = context;
        }

        #endregion Constructors

        #region IBalanceCollectionAdministrationRepository Implementation

        /// <summary>
        /// Creates an record used to administer temporary balance collections used for Green/Blue loading
        /// </summary>
        /// <param name="collectionAdministration">The class describing the temporary collection to administer</param>
        /// <returns>Returns a value indeicating whether the creation was successful</returns>
        async Task<bool> IBalanceCollectionAdministrationRepository.Create(CollectionAdministration collectionAdministration)
        {
            await this.Context.OrganisationBalancesCollectionAdministration.InsertOneAsync(collectionAdministration);
            return true;
        }

        /// <summary>
        ///updates a record used to administer temporary balance collections used for Green/Blue loading
        /// </summary>
        /// <param name="collectionAdministration">The class describing the temporary collection to administer</param>
        /// <returns>Returns a value indeicating whether the update was successful</returns>
        async Task<bool> IBalanceCollectionAdministrationRepository.Update(CollectionAdministration collectionAdministration)
        {
            var result = await this.Context.OrganisationBalancesCollectionAdministration
                .ReplaceOneAsync(ca => ca.Id == collectionAdministration.Id, collectionAdministration);

            return result.IsAcknowledged;
        }

        /// <summary>
        /// Gets a list of all temporary balance collection admin records
        /// </summary>
        /// <returns>Returns a list of for all temporary balance collections admin records</returns>
        async Task<IList<CollectionAdministration>> IBalanceCollectionAdministrationRepository.GetTemporaryCollectionAdministrationRecords()
        {
            var collectionAdministrationRecords = new List<CollectionAdministration>();

            var collectionNamesCursor = await this.Context.OrganisationBalancesCollectionAdministration.Database.ListCollectionNamesAsync();
            var collectionsNames = await collectionNamesCursor.ToListAsync<string>();

            foreach (var collectionName in collectionsNames)
            {
                if (Guid.TryParse(collectionName.Replace(GhgBalanceContext.TemporaryCollectionPrefix, string.Empty), out Guid collectionKey))
                {
                    var managedCollection = await this.Context.OrganisationBalancesCollectionAdministration
                        .Find(ca => ca.Id == collectionKey)
                        .SingleOrDefaultAsync();

                    if (managedCollection != null)
                    {
                        collectionAdministrationRecords.Add(managedCollection);
                    }
                }
            }

            return collectionAdministrationRecords;
        }

        /// <summary>
        /// Gets a collection administration record by ID
        /// </summary>
        /// <param name="id">The ID of the collection administration record to get</param>
        /// <returns>Returns a single colelction administration record if it exists, else null</returns>
        Task<CollectionAdministration> IBalanceCollectionAdministrationRepository.Get(Guid id)
        {
            return this.Context.OrganisationBalancesCollectionAdministration.Find(ca => ca.Id == id).SingleOrDefaultAsync();
        }

        #endregion IBalanceCollectionAdministrationRepository Implementation
    }
}
