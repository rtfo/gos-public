﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Administration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalance.Common.Repositories
{
    /// <summary>
    /// Defines a repository for administering balance collections for Green/blue loading
    /// </summary>
    public interface IBalanceCollectionAdministrationRepository
    {
        /// <summary>
        /// Gets a collection administration record by ID
        /// </summary>
        /// <param name="id">The ID of the collection administration record to get</param>
        /// <returns>Returns a single colelction administration record if it exists, else null</returns>
        Task<CollectionAdministration> Get(Guid id);

        /// <summary>
        /// Creates an record used to administer temporary balance collections used for Green/Blue loading
        /// </summary>
        /// <param name="collectionAdministration">The class describing the temporary collection to administer</param>
        /// <returns>Returns a value indeicating whether the creation was successful</returns>
        Task<bool> Create(CollectionAdministration collectionAdministration);

        /// <summary>
        ///updates a record used to administer temporary balance collections used for Green/Blue loading
        /// </summary>
        /// <param name="collectionAdministration">The class describing the temporary collection to administer</param>
        /// <returns>Returns a value indeicating whether the update was successful</returns>
        Task<bool> Update(CollectionAdministration collectionAdministration);

        /// <summary>
        /// Gets a list of all temporary balance collection admin records
        /// </summary>
        /// <returns>Returns a list of for all temporary balance collections admin records</returns>
        Task<IList<CollectionAdministration>> GetTemporaryCollectionAdministrationRecords();
    }
}
