﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Threading.Tasks;
using DfT.GOS.GhgBalance.Common.Data;
using DfT.GOS.GhgBalance.Common.Data.Balance;
using MongoDB.Driver;

namespace DfT.GOS.GhgBalance.Common.Repositories
{
    /// <summary>
    /// Provides a MongoDb repository for temporary GHG Balances used for Green/Blue collection loading
    /// </summary>
    public class TemporaryBalanceRepository : BalanceRepository
    {
        #region Properties

        private Guid TemporaryCollectionId { get; set; }

        /// <summary>
        /// Gets the Mongo collection for the repository
        /// </summary>
        protected override IMongoCollection<OrganisationBalance> Collection { get { return this.Context.GetTempOrganisationBalances(this.TemporaryCollectionId); } }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="context">The data context used by the repository to get data</param>
        public TemporaryBalanceRepository(Guid temporaryCollectionId, IGhgBalanceContext context)
            :base(context)
        {
            this.TemporaryCollectionId = temporaryCollectionId;
        }

        #endregion Constructors 

        #region Overriden Methods

        /// <summary>
        /// Renames the collection to be the master collection
        /// </summary>
        /// <returns>Returns true if the operation was successful, otherwise false</returns>
        protected async override Task<bool> UpdateToMaster()
        {
            var tempCollectionName = this.Collection.CollectionNamespace.CollectionName;
            var masterCollectionName = base.Collection.CollectionNamespace.CollectionName;

            await this.Collection.Database.RenameCollectionAsync(tempCollectionName, masterCollectionName, new RenameCollectionOptions() { DropTarget = true });
            return true;
        }

        #endregion Overriden Methods
    }
}
