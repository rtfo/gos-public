﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgBalance.Common.Repositories
{
    /// <summary>
    /// Factory class definition used to manage instances of balance collections used in Green/Blue loading
    /// </summary>
    public interface IBalanceRepositoryFactory
    {
        /// <summary>
        /// Gets an instance of a repository pointing to the master balance collection
        /// </summary>
        /// <returns>Returns the master collection repository</returns>
        IBalanceRepository GetMasterRepository();

        /// <summary>
        /// Gets an instance of a repository pointing at the temporary collection with the supplied temporyCollectionId
        /// </summary>
        /// <param name="temporyCollectionId">The ID of the balance view collection to get</param>
        /// <returns>Returns a repository pointing at the correct collection</returns>
        IBalanceRepository GetTemporaryRepository(Guid temporyCollectionId);
    }
}
