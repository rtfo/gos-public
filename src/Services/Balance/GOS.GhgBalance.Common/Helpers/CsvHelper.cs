﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Linq;

namespace DfT.GOS.GhgBalance.Common.Helpers
{
    /// <summary>
    /// Provides a helper class for CSV files
    /// </summary>
    public static class CsvHelper
    {
        #region Methods

        /// <summary>
        /// Method to handle escape characters that may cause issues within the CSV export
        /// </summary>
        public static string ConvertToCsvCell(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                var mustQuote = value.Any(x => x == ',' || x == '\"' || x == '\r' || x == '\n');
                if (!mustQuote)
                {
                    return value;
                }
                string valueWithEscapeChar = value.Replace("\"", "\"\"");
                return string.Format("\"{0}\"", valueWithEscapeChar);
            }
            else
            {
                return null;
            }
        }

        #endregion Methods
    }
}
