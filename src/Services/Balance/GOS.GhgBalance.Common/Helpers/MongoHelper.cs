﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using System;
using System.Collections.Generic;

namespace DfT.GOS.GhgBalance.Common.Services
{
    /// <summary>
    /// Helper class providing methods to return BsonDocument for use in aggreegation pipeline on MongoDbServer
    /// </summary>
    public static class MongoHelper
    {
        /// <summary>
        /// Matches documents based off given path and value
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="value"></param>
        /// <returns>BsonDocument for filtering</returns>
        public static BsonDocument Match(string expression, int value)
        {
            return new BsonDocument("$match", new BsonDocument()
                    .Add(expression, value));
        }

        /// <summary>
        /// Creates a document clone for each element of array with element substituted in.
        /// </summary>
        /// <param name="pathToTarget"></param>
        /// <param name="preserveNullAndEmptyArrays"></param>
        /// <returns>BsonDocument for unwinding</returns>
        public static BsonDocument Unwind(string pathToTarget, bool preserveNullAndEmptyArrays = false)
        {
            return new BsonDocument("$unwind", new BsonDocument() /// Create document for each Credit event, ideally we could filter Trading.CreditEvents collection first for only relevant entries but to RegExp match in aggregation MongoDb server needs to be 4.1.11
                    .Add("path", pathToTarget)
                    .Add("preserveNullAndEmptyArrays", new BsonBoolean(preserveNullAndEmptyArrays)));
        }

        /// <summary>
        /// Sorts documents in ascending or descending order
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>BsonDocument for sorting</returns>
        public static BsonDocument Sort(params KeyValuePair<string, int>[] parameters)
        {
            var projectForm = new BsonDocument();

            foreach (var x in parameters)
            {
                projectForm.Add(new BsonElement(x.Key, x.Value));
            }
            return new BsonDocument("$sort", projectForm); /// Sort by value of credits exchanged
        }

        /// <summary>
        /// Group documents by uniqueness and returns first of each in grouping
        /// </summary>
        /// <param name="expression">Path to property to group on</param>
        /// <param name="groupingID">Name of property that will be new Id based off groupuing</param>
        /// <returns>BsonDocument for gropuing</returns>
        public static BsonDocument FirstInEachGroup(string expression, string groupingID)
        {
            return new BsonDocument("$group", new BsonDocument() /// Group by reference then take first element of grouping. Should be higher value of credits exchanged for one transaction
                .Add("_id", new BsonDocument()
                    .Add(groupingID, expression)
                )
                .Add("doc", new BsonDocument()
                    .Add("$first", "$$ROOT")
                ));
        }

        /// <summary>
        /// Project documents to a new form 
        /// </summary>
        /// <param name="projections">Collection of key as path to property value as argument for docuemnt</param>
        /// <returns>BsonDocument to project documents from one form to another</returns>
        public static BsonDocument Project(Dictionary<string, BsonValue> projections)
        {
            if (projections == null || projections.Count == 0)
            {
                throw new ArgumentNullException("Mongo projections must have a value");
            }

            var projectForm = new BsonDocument();

            foreach (var x in projections)
            {
                projectForm.Add(new BsonElement(x.Key, x.Value));
            }
            return new BsonDocument("$project", projectForm);
        }

        /// <summary>
        /// For getting count and paged result set of documents. If form of document is same as <T> will turn into ResultSet<T>
        /// </summary>
        /// <param name="skip"></param>
        /// <param name="pageSize"></param>
        /// <returns>Array of BsonDocuments for paging and counting</returns>
        public static BsonDocument[] PageCountStages(int skip, int pageSize)
        {
            return new BsonDocument[]{
                new BsonDocument("$facet", new BsonDocument() /// Split to two pipelines, one for desired results, one for counting all results
                    .Add("Results", new BsonArray()
                        .Add(new BsonDocument()
                            .Add("$skip", skip)
                        )
                        .Add(new BsonDocument()
                            .Add("$limit", pageSize)
                        )
                    )
                    .Add("Count", new BsonArray()
                        .Add(new BsonDocument()
                            .Add("$count", "count")
                        )
                    )),
                new BsonDocument("$replaceRoot", new BsonDocument() /// Tidy up document, handle no results found
                    .Add("newRoot", new BsonDocument()
                        .Add("Count", new BsonDocument()
                            .Add("$cond", new BsonDocument()
                                .Add("if", new BsonDocument()
                                    .Add("$eq", new BsonArray()
                                        .Add(new BsonDocument()
                                            .Add("$size", "$Count")
                                        )
                                        .Add(0)
                                    )
                                )
                                .Add("then", new BsonDocument()
                                    .Add("$literal", 0)  /// Return 0 for count if no results, mongo will return empty elseways
                                )
                                .Add("else", new BsonDocument()
                                    .Add("$map", new BsonDocument()
                                        .Add("input", "$Count")
                                        .Add("as", "num")
                                        .Add("in", "$$num.count")
                                    )
                                )
                            )
                        )
                        .Add("Results", "$Results")
                )),
                new BsonDocument("$unwind", new BsonDocument()
                        .Add("path", "$Count"))
            };
        }


        //public static implicit operator byte(Digit d) => d.digit;
    }
}
