﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace DfT.GOS.GhgBalance.Common.Models
{
    /// <summary>
    /// Stores front-end bound data about CreditEvents
    /// </summary>
    public class ResultSet<T>
    {
        /// <summary>
        /// The total count of results found
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Collection of T results
        /// </summary>
        public IList<T> Results { get; set; }
    }
}