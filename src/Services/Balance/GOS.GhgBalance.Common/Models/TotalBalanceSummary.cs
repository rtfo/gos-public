﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgBalance.Common.Models
{
    /// <summary>
    /// Represents a high-level summary of the GHG Balance
    /// </summary>
    public class TotalBalanceSummary
    {
        #region Properties

        /// <summary>
        /// The balance of credits from submissions and trading
        /// </summary>
        public decimal BalanceCredits { get; private set; }

        /// <summary>
        /// The balance of obligation from submissions and trading
        /// </summary>
        public decimal BalanceObligation { get; private set; }

        /// <summary>
        /// The net position of Balance Credits and Balance Obligation
        /// </summary>
        public decimal NetPosition { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="balanceCredits">The balance of credits from submissions and trading</param>
        /// <param name="balanceObligation">The balance of obligation from submissions and trading</param>
        /// <param name="netPosition">The net position of Balance Credits and Balance Obligation</param>
        public TotalBalanceSummary(decimal balanceCredits
            , decimal balanceObligation
            , decimal netPosition)
        {
            this.BalanceCredits = balanceCredits;
            this.BalanceObligation = balanceObligation;
            this.NetPosition = netPosition;
        }
        #endregion Constructors
    }
}
