﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Helpers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DfT.GOS.GhgBalance.Common.Models
{
    /// <summary>
    /// Represents a high-level summary of a Credit Event
    /// </summary>
    public class CreditTransferSummary
    {
        #region Properties

        /// <summary>
        /// The name of the primary organisation in the trade event
        /// </summary>
        public string OrganisationName { get; private set; }

        /// <summary>
        /// The id of the primary organisation in the trade event
        /// </summary>
        public int OrganisationId { get; private set; }

        /// <summary>
        /// The id of the primary obligation period in the trade event
        /// </summary>
        public int ObligationPeriodId { get; private set; }

        /// <summary>
        /// The unique reference for the credit event
        /// </summary>
        public int Reference { get; private set; }

        /// <summary>
        /// The date / time of the credit event
        /// </summary>
        public DateTime Date { get; private set; }

        /// <summary>
        /// The ID of the credit event type
        /// </summary>
        public int EventTypeId { get; private set; }

        /// <summary>
        /// The credit event type
        /// </summary>
        public string EventType { get; private set; }

        /// <summary>
        /// The ID of the other organisation in the trade event
        /// </summary>
        public int? TradingOrganisationId { get; private set; }

        /// <summary>
        /// The name of the other organisation in the trade event
        /// </summary>
        public string TradingOrganisationName { get; private set; }

        /// <summary>
        /// The number of credits that were traded
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Credits { get; private set; }

        /// <summary>
        /// The additional reference for the credit event
        /// </summary>
        public string AdditionalReference { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that requires all properties
        /// </summary>
        /// <param name="organisationName">The name of the primary organisation in the trade event</param>
        /// <param name="organisationId">The id of the primary organisation in the trade event</param>
        /// <param name="obligationPeriodId">The id of the primary obligation period in the trade event</param>
        /// <param name="reference">The unique reference for the credit event</param>
        /// <param name="date">The date / time of the credit event</param>
        /// <param name="eventTypeId">The ID of the credit event type</param>
        /// <param name="eventType">The credit event type</param>
        /// <param name="tradingOrganisationId">The ID of the other organisation in the trade event</param>
        /// <param name="tradingOrganisationName">The name of the other organisation in the trade event</param>
        /// <param name="credits">The number of credits that were traded</param>
        /// <param name="additionalReference">The additional reference for the credit event</param>
        public CreditTransferSummary(
            string organisationName,
            int organisationId,
            int obligationPeriodId,
            int reference,
            DateTime date,
            int eventTypeId,
            string eventType,
            int? tradingOrganisationId,
            string tradingOrganisationName,
            decimal credits,
            string additionalReference
            )
        {
            this.OrganisationName = organisationName;
            this.OrganisationId = organisationId;
            this.ObligationPeriodId = obligationPeriodId;
            this.Reference = reference;
            this.Date = date;
            this.EventTypeId = eventTypeId;
            this.EventType = eventType;
            this.TradingOrganisationId = tradingOrganisationId;
            this.TradingOrganisationName = tradingOrganisationName;
            this.Credits = credits;
            this.AdditionalReference = additionalReference;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets a header row for CSV files
        /// </summary>
        /// <returns></returns>
        public static string GetCsvHeader()
        {
            return string.Join(","
                , "Reference"
                , "Date"
                , "Sender"
                , "Receiver"
                , "Additional Reference"
                , "Credits"
                );
        }

        /// <summary>
        /// Gets a string representation of the Credit Transfer Summary for CSV download
        /// </summary>
        public override string ToString()
        {
            return string.Join(","
                , this.Reference.ToString()
                , this.Date.ToString("dd/MM/yyyy")
                , CsvHelper.ConvertToCsvCell( this.OrganisationName)
                , CsvHelper.ConvertToCsvCell(this.TradingOrganisationName)
                , CsvHelper.ConvertToCsvCell(this.AdditionalReference)
                , this.Credits
                );
        }

        #endregion Methods
    }
}
