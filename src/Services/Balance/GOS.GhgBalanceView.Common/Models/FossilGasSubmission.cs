﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// An individual Fossil Gas Submission
    /// </summary>
    public class FossilGasSubmission
    {
        #region Properties

        /// <summary>
        /// The ID of the application this submission was submitted under
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// The date of the submissions
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The unique reference for the submission
        /// </summary>
        public int Reference { get; set; }

        /// <summary>
        /// The ID of the Fuel Type
        /// </summary>
        public int FuelTypeId { get; set; }

        /// <summary>
        /// The Fuel Type description
        /// </summary>
        public string FuelType { get; set; }

        /// <summary>
        /// The volume of fuel (units)
        /// </summary>
        public decimal Volume { get; set; }

        /// <summary>
        /// The Energy Density of fuel (MJ / unit)
        /// </summary>
        public Nullable<decimal> EnergyDensity { get; set; }

        /// <summary>
        /// The GHG of the energy (gCO2 / MJ)
        /// </summary>
        public Nullable<decimal> GhgIntensity { get; set; }

        /// <summary>
        /// The difference to target CI (gCO2e / MJ)
        /// </summary>
        public decimal DifferenceToTarget { get; set; }

        /// <summary>
        /// The Energy Supplied in (MJ)
        /// </summary>
        public decimal EnergySupplied { get; set; }

        /// <summary>
        /// The number of credits produced (KgCO2e)
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// The obligation produced (KgCO2e)
        /// </summary>
        public decimal Obligation { get; set; }

        #endregion Properties

        #region Constructors
        public FossilGasSubmission() { }
        #endregion
    }
}
