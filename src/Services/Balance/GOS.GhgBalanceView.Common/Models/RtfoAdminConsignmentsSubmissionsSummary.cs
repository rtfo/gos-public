﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents summary of RTFO admin consignments submissions for a company within an obligation period
    /// </summary>
    public class RtfoAdminConsignmentsSubmissionsSummary
    {
        #region Properties

        /// <summary>
        /// The total credits for RTFO admin consignments submissions
        /// </summary>
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The total obligation RTFO admin consignments submissions
        /// </summary>
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The aggregated volume supplied across all items (Units)
        /// </summary>
        public decimal TotalVolumeOfFuelSupplied { get; private set; }

        #endregion Properties

        /// <summary>
        /// Constructor that accepts all properties
        /// </summary>
        /// <param name="totalCredits">Total credits</param>
        /// <param name="totalObligation">Total obligation</param>
        /// <param name="totalVolumeOfFuelSupplied">Total volume of fuel supplied</param>
        public RtfoAdminConsignmentsSubmissionsSummary(decimal totalCredits
            , decimal totalObligation
            , decimal totalVolumeOfFuelSupplied)
        {
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.TotalVolumeOfFuelSupplied = totalVolumeOfFuelSupplied;
        }
    }
}
