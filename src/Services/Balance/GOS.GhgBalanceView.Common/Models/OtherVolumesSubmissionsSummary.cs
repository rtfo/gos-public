﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents summary of other volumes submitted via RTFO submissions for a company within an obligation period
    /// </summary>
    public class OtherVolumesSubmissionsSummary
    {
        /// <summary>
        /// The total credits forother volumes submitted via RTFO
        /// </summary>
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The total obligation for other volumes submitted via RTFO
        /// </summary>
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The aggregated volume supplied across all items (Units)
        /// </summary>
        public decimal TotalVolumeOfFuelSupplied { get; private set; }

        public OtherVolumesSubmissionsSummary(decimal totalCredits
            , decimal totalObligation
            , decimal totalVolumeOfFuelSupplied)
        {
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.TotalVolumeOfFuelSupplied = totalVolumeOfFuelSupplied;
        }
    }


}
