﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents summary of liquid and gas for a company within an obligation period
    /// </summary>
    public class LiquidGasBalanceSummary
    {
        /// <summary>
        /// The total number of credits for this obligation
        /// </summary>
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The total obligation for this obligation period
        /// </summary>
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The total volume of fuel supplied for this obligation period
        /// </summary>
        public decimal TotalVolumeOfFuelSupplied { get; private set; }

        /// <summary>
        /// The total volume above GHG targer for this obligation period
        /// </summary>
        public decimal TotalVolumeAboveGHGTarget { get; private set; }

        /// <summary>
        /// The total low volume supplier deduction for this obligation period
        /// </summary>
        public decimal LowVolumeSupplierDeduction { get; private set; }

        /// <summary>
        /// RTFO Admin Consignments Submissions
        /// </summary>
        public RtfoAdminConsignmentsSubmissionsSummary RTFOAdminConsignmentsSubmissions { get; private set; }

        /// <summary>
        /// Other Volumes Submissions
        /// </summary>
        public OtherVolumesSubmissionsSummary OtherVolumesSubmissions { get; private set; }

        /// <summary>
        /// Fossil Gas submissions
        /// </summary>
        public FossilGasSubmissionsSummary FossilGasSubmissions { get; private set; }

        public LiquidGasBalanceSummary(decimal totalCredits
            , decimal totalObligation
            , decimal totalVolumeOfFuelSupplied
            , decimal totalVolumeAboveGHGTarget
            , decimal lowVolumeSupplierDeduction
            , RtfoAdminConsignmentsSubmissionsSummary rtfoAdminConsignmentsSubmissions
            , OtherVolumesSubmissionsSummary otherVolumesSubmissions
            , FossilGasSubmissionsSummary fossilGasSubmissions)
        {
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.TotalVolumeOfFuelSupplied = totalVolumeOfFuelSupplied;
            this.TotalVolumeAboveGHGTarget = totalVolumeAboveGHGTarget;
            this.LowVolumeSupplierDeduction = lowVolumeSupplierDeduction;
            this.RTFOAdminConsignmentsSubmissions = rtfoAdminConsignmentsSubmissions;
            this.OtherVolumesSubmissions = otherVolumesSubmissions;
            this.FossilGasSubmissions = fossilGasSubmissions;
        }
    }


}
