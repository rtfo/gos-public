﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents summary of RTFO admin consignments submission for a company within an obligation period
    /// </summary>
    public class RtfoAdminConsignmentSubmission
    {
        #region Properties
        /// <summary>
        /// The reporting month the admin consignment was submitted for
        /// </summary>
        public DateTime ReportingMonth { get; set; }

        /// <summary>
        /// The ID of the admin consignment the submission was reported in
        /// </summary>
        public int AdminConsignmentId { get; set; }

        /// <summary>
        /// The ID of the Fuel Type
        /// </summary>
        public int FuelTypeId { get; private set; }

        /// <summary>
        /// The Fuel Type description
        /// </summary>
        public string FuelType { get; set; }

        /// <summary>
        /// The ID of the Feedstock for the fuel
        /// </summary>
        public int FeedstockId { get; set; }

        /// <summary>
        /// The Feedstock for the fuel
        /// </summary>
        public string Feedstock { get; set; }

        /// <summary>
        /// The volume of fuel (units)
        /// </summary>
        public decimal Volume { get; set; }

        /// <summary>
        /// The Energy Density of fuel (MJ / unit)
        /// </summary>
        public Nullable<decimal> EnergyDensity { get; set; }

        /// <summary>
        /// The GHG of the energy (gCO2 / MJ)
        /// </summary>
        public Nullable<decimal> GhgIntensity { get; set; }

        /// <summary>
        /// The difference to target CI (gCO2e / MJ)
        /// </summary>
        public decimal DifferenceToTarget { get; set; }

        /// <summary>
        /// The Energy Supplied in (MJ)
        /// </summary>
        public decimal EnergySupplied { get; set; }

        /// <summary>
        /// The number of credits produced (KgCO2e)
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// The obligation produced (KgCO2e)
        /// </summary>
        public decimal Obligation { get; set; }

        /// <summary>
        /// The ID of the admin consignment, as supplied by the supplier
        /// </summary>
        public string InternalReference { get; set; }
        #endregion

        public RtfoAdminConsignmentSubmission() { }
    }
}
