﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents summary of UER submissions for a company within an obligation period
    /// </summary>
    public class UpstreamEmissionsReductionSubmissionsSummary
    {
        #region Properties

        /// <summary>
        /// The total number of UER credits
        /// </summary>
        public decimal TotalCredits { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="totalCredits">Total credits</param>
        public UpstreamEmissionsReductionSubmissionsSummary(decimal totalCredits)
        {
            this.TotalCredits = totalCredits;
        }

        #endregion Constructors
    }
}
