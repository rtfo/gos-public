﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents summary of electricity submissions for a company within an obligation period
    /// </summary>
    public class ElectricitySubmissionsSummary
    {
        #region Properties

        /// <summary>
        /// The Total number of credits for the Electricity Submissions
        /// </summary>
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The Total obligation for the Electricity Submissions
        /// </summary>
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The Total amount Supplied for the Electricity Submissions
        /// </summary>
        public decimal TotalAmountSupplied { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="totalCredits">Total credits</param>
        /// <param name="totalObligation">Total obligation</param>
        /// <param name="totalAmountSupplied">Total volume of fuel supplied</param>
        public ElectricitySubmissionsSummary(decimal totalCredits
            , decimal totalObligation
            , decimal totalAmountSupplied)
        {
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.TotalAmountSupplied = totalAmountSupplied;
        }

        #endregion Constructors
    }
}
