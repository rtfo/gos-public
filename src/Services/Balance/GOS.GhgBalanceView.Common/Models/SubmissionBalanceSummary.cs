﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents summary of submissions for a company within an obligation period
    /// </summary>
    public class SubmissionBalanceSummary
    {
        #region Properties

        /// <summary>
        /// The total number of credits for this obligation
        /// </summary>
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The total obligation for this obligation period
        /// </summary>
        public decimal TotalObligation { get; private set; }
        
        /// <summary>
        /// Liquid and Gas Submissions
        /// </summary>
        public LiquidGasSubmissionsSummary LiquidGasSubmissions { get; private set; }

        /// <summary>
        /// Electricity Submissions
        /// </summary>
        public ElectricitySubmissionsSummary ElectricitySubmissions { get; private set; }

        /// <summary>
        /// Upstream Emissions Reduction (UER) submissions
        /// </summary>
        public UpstreamEmissionsReductionSubmissionsSummary UpstreamEmissionsReductionSubmissions { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Consstructor taking all variables
        /// </summary>
        /// <param name="totalCredits">Total credits</param>
        /// <param name="totalObligation">Total obligation</param>
        /// <param name="liquidGasSubmissions">Liquid Gas submissions</param>
        /// <param name="electricitySubmissions">Electricity submissions</param>
        /// <param name="upstreamEmissionsReductionSubmissions">UER submissions</param>
        public SubmissionBalanceSummary(decimal totalCredits
            , decimal totalObligation
            , LiquidGasSubmissionsSummary liquidGasSubmissions
            , ElectricitySubmissionsSummary electricitySubmissions
            , UpstreamEmissionsReductionSubmissionsSummary upstreamEmissionsReductionSubmissions)
        {
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.LiquidGasSubmissions = liquidGasSubmissions;
            this.ElectricitySubmissions = electricitySubmissions;
            this.UpstreamEmissionsReductionSubmissions = upstreamEmissionsReductionSubmissions;
        }

        #endregion Constructors
    }
}
