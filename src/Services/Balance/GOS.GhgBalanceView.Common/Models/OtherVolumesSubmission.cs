﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents details of other volumes submitted via RTFO submissions for a company within an obligation period
    /// </summary>
    public class OtherVolumesSubmission
    {
        /// <summary>
        /// The reporting month the other volumes via RTFO were submitted
        /// </summary>
        public DateTime ReportingMonth { get; set; }

        /// <summary>
        /// The ID of the Fuel Type
        /// </summary>
        public int FuelTypeId { get; set; }

        /// <summary>
        /// The Fuel Type description
        /// </summary>
        public string FuelType { get; set; }

        /// <summary>
        /// The Energy Density of fuel (MJ / unit)
        /// </summary>
        public Nullable<decimal> EnergyDensity { get; set; }

        /// <summary>
        /// The GHG of the energy (gCO2 / MJ)
        /// </summary>
        public Nullable<decimal> GhgIntensity { get; set; }

        /// <summary>
        /// The difference to target CI (gCO2e / MJ)
        /// </summary>
        public decimal DifferenceToTarget { get; set; }

        /// <summary>
        /// The Energy Supplied in (MJ)
        /// </summary>
        public decimal EnergySupplied { get; set; }

        /// <summary>
        /// The total credits forother volumes submitted via RTFO
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// The total obligation for other volumes submitted via RTFO
        /// </summary>
        public decimal Obligation { get; set; }

        /// <summary>
        /// The aggregated volume supplied across all items (Units)
        /// </summary>
        public decimal Volume { get; set; }

        public OtherVolumesSubmission()
        {
        }
    }


}
