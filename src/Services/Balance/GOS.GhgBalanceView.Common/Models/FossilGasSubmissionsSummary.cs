﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents summary of fossil gas submissions for a company within an obligation period
    /// </summary>
    public class FossilGasSubmissionsSummary
    {
        /// <summary>
        /// The total credits for fossil gas submissions
        /// </summary>
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The total obligation for fossil gas submissions
        /// </summary>
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The aggregated volume supplied across all items (Units)
        /// </summary>
        public decimal TotalVolumeOfFuelSupplied { get; private set; }

        public FossilGasSubmissionsSummary(decimal totalCredits
            , decimal totalObligation
            , decimal totalVolumeOfFuelSupplied)
        {
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.TotalVolumeOfFuelSupplied = totalVolumeOfFuelSupplied;
        }

    }
}
