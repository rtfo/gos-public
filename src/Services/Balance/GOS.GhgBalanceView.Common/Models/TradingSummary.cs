﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.Web.Models;

namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents a summary of trading for an organisation
    /// </summary>
    public class TradingSummary
    {
        #region Properties

        /// <summary>
        /// The balance of credits from submissions and trading
        /// </summary>
        public decimal BalanceCredits { get; private set; }

        /// <summary>
        /// A page of Credit Events
        /// </summary>
        public PagedResult<CreditEvent> CreditEvents { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="balanceCredits">The balance of credits from submissions and trading</param>
        /// <param name="creditEvents">A page of Credit Events</param>
        public TradingSummary(decimal balanceCredits
            , PagedResult<CreditEvent> creditEvents)
        {
            this.BalanceCredits = balanceCredits;
            this.CreditEvents = creditEvents;
        }

        #endregion Constructors
    }
}
