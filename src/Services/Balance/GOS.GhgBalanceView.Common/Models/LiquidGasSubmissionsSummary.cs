﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents summary of Liquid and Gas submissions for a company within an obligation period
    /// </summary>
    public class LiquidGasSubmissionsSummary
    {
        #region Properties

        /// <summary>
        /// The total credits for liquid and gas submissions
        /// </summary>
        public decimal TotalCredits { get; private set; }

        /// <summary>
        /// The total obligation for liquid and gas submissions
        /// </summary>
        public decimal TotalObligation { get; private set; }

        /// <summary>
        /// The aggregated volume supplied across all items (Units)
        /// </summary>
        public decimal TotalVolumeOfFuelSupplied { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that takes all variables
        /// </summary>
        /// <param name="totalCredits">Total credits</param>
        /// <param name="totalObligation">Total obligation</param>
        /// <param name="totalVolumeOfFuelSupplied">Total volume of fuel supplied</param>
        public LiquidGasSubmissionsSummary(decimal totalCredits
            , decimal totalObligation
            , decimal totalVolumeOfFuelSupplied)
        {
            this.TotalCredits = totalCredits;
            this.TotalObligation = totalObligation;
            this.TotalVolumeOfFuelSupplied = totalVolumeOfFuelSupplied;
        }

        #endregion Constructors
    }
}
