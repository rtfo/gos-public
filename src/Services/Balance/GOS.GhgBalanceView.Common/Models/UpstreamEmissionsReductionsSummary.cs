﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.Web.Models;

namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents a summary of Upstream Emissions Reductions (UERs) submissions for a company within an obligation period
    /// </summary>
    public class UpstreamEmissionsReductionsSummary
    {
        #region Properties

        /// <summary>
        /// The total amount of Upstream Emissions Reduction
        /// </summary>
        public decimal TotalAmount { get; private set; }

        /// <summary>
        /// A page of Upstream Emissions Reductions (UERs)
        /// </summary>
        public PagedResult<UpstreamEmissionsReductionSubmission> UpstreamEmissionsReductions { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="totalAmount">The total amount of Upstream Emissions Reduction</param>
        /// <param name="upstreamEmissionsReductions">A page of Upstream Emissions Reductions (UERs)</param>
        public UpstreamEmissionsReductionsSummary(decimal totalAmount
            , PagedResult<UpstreamEmissionsReductionSubmission> upstreamEmissionsReductions)
        {
            this.TotalAmount = totalAmount;
            this.UpstreamEmissionsReductions = upstreamEmissionsReductions;
        }

        #endregion Constructors
    }
}
