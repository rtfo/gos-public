﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgBalanceView.Common.Models
{
    /// <summary>
    /// Represents a high-level summary of the GHG Balance
    /// </summary>
    public class BalanceSummary
    {
        #region Properties

        /// <summary>
        /// Total credits from all submissions
        /// </summary>
        public decimal SubmissionsCredits { get; private set; }

        /// <summary>
        /// Total obligation from all submissions
        /// </summary>
        public decimal SubmissionsObligation { get; private set; }

        /// <summary>
        /// Total credits from all trading
        /// </summary>
        public decimal TradingCredits { get; private set; }

        /// <summary>
        /// The balance of credits from submissions and trading
        /// </summary>
        public decimal BalanceCredits { get; private set; }

        /// <summary>
        /// The balance of obligation from submissions and trading
        /// </summary>
        public decimal BalanceObligation { get; private set; }

        /// <summary>
        /// The net position of Balance Credits and Balance Obligation
        /// </summary>
        public decimal NetPosition { get; private set; }

        /// <summary>
        /// The buyout cost (of there is an obligation)
        /// </summary>
        public decimal BuyoutCost { get; private set; }

        /// <summary>
        /// The cost of buyout per tCO2e
        /// </summary>
        public decimal BuyOutPer_tCO2e { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="submissionsCredits">Total credits from all submissions</param>
        /// <param name="submissionsObligation">Total obligation from all submissions</param>
        /// <param name="tradingCredits">Total credits from all trading</param>
        /// <param name="balanceCredits">The balance of credits from submissions and trading</param>
        /// <param name="balanceObligation">The balance of obligation from submissions and trading</param>
        /// <param name="netPosition">The net position of Balance Credits and Balance Obligation</param>
        /// <param name="buyoutCost">The buyout cost (of there is an obligation)</param>
        /// <param name="buyOutPer_tCO2e">The cost of buyout per tCO2e</param>
        public BalanceSummary(decimal submissionsCredits
            , decimal submissionsObligation
            , decimal tradingCredits
            , decimal balanceCredits
            , decimal balanceObligation
            , decimal netPosition
            , decimal buyoutCost
            , decimal buyOutPer_tCO2e)
        {
            this.SubmissionsCredits = submissionsCredits;
            this.SubmissionsObligation = submissionsObligation;
            this.TradingCredits = tradingCredits;
            this.BalanceCredits = balanceCredits;
            this.BalanceObligation = balanceObligation;
            this.NetPosition = netPosition;
            this.BuyoutCost = buyoutCost;
            this.BuyOutPer_tCO2e = buyOutPer_tCO2e;
        }

        #endregion Constructors
    }
}
