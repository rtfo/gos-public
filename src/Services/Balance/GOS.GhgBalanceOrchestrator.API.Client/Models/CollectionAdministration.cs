﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Text;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Client.Models
{
    /// <summary>
    /// Represents a Collection Administration bulk update job
    /// </summary>
    public class CollectionAdministration
    {
        #region Public Properties

        /// <summary>
        /// The Unique ID of the Organisation Balance Collection
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the date/time the bulk load was started
        /// </summary>
        public DateTime LoadStarted { get; set; }

        /// <summary>
        /// Gets the date/time the bulk load was created
        /// </summary>
        public Nullable<DateTime> LoadSuccessfullyCompleted { get; set; }

        /// <summary>
        /// A list of errors that occured during the bulk load
        /// </summary>
        public List<string> Errors { get; set; }

        /// <summary>
        /// Indicates whether errors occured when performing the bulk load
        /// </summary>
        public bool HasErrors { get; set; }

        /// <summary>
        /// Indicates whether the operation has timed out
        /// </summary>
        public bool HasTimedOut { get; set; }

        /// <summary>
        /// Status indicator for operation
        /// </summary>
        public CollectionAdministrationStatus Status { get; set; }

        /// <summary>
        /// Status description
        /// </summary>
        public string StatusDescription { get; set; }

        #endregion Properties
    }
}
