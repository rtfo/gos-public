﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceOrchestrator.API.Client.Models;
using DfT.GOS.Http;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Client.Services
{
    /// <summary>
    /// Client interface to the Balance administration service
    /// </summary>
    public class BalanceAdministrationService : HttpServiceClient, IBalanceAdministrationService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        public BalanceAdministrationService(HttpClient httpClient)
            : base(httpClient)
        {
        }

        #endregion Constructors

        #region IBalanceAdministrationService Implementation

        /// <summary>
        /// Issues a request to update the GHG Balance view
        /// </summary>
        /// <param name="jwtToken">The token used for auth</param>
        /// <returns>Returns information about the request to indicate whether it was scheduled successfully</returns>
        async Task<HttpObjectResponse<string>> IBalanceAdministrationService.UpdateCollections(string jwtToken)
        {
            var response = await this.HttpPostAsync("api/v1/BalanceAdministration/UpdateCollections", null, jwtToken);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var bulkUpdateUniqueId = JsonConvert.DeserializeObject<string>(content);
                return new HttpObjectResponse<string>(bulkUpdateUniqueId);
            }
            else
            {
                return new HttpObjectResponse<string>(response.StatusCode);
            }
        }

        /// <summary>
        /// Retrieves information about the progress of a Collection Administration bulk update job
        /// </summary>
        /// <param name="bulkUpdateUniqueId">Id of Collection Administration bulk update job</param>
        /// <param name="jwtToken">The token used for authentication</param>
        /// <returns>Details of the bulk update job, including current status</returns>
        async Task<HttpObjectResponse<CollectionAdministration>> IBalanceAdministrationService.GetUpdateStatus(string bulkUpdateUniqueId, string jwtToken)
        {
            return await this.HttpGetAsync<CollectionAdministration>($"api/v1/BalanceAdministration/UpdateCollections/Status/{bulkUpdateUniqueId}", jwtToken);
        }

        #endregion IBalanceAdministrationService Implementation
    }
}
