﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceOrchestrator.API.Client.Models;
using DfT.GOS.Http;
using System;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Client.Services
{
    /// <summary>
    /// Client interface to the Balance administration service
    /// </summary>
    public interface IBalanceAdministrationService
    {
        /// <summary>
        /// Issues a request to update the GHG Balance view
        /// </summary>
        /// <param name="jwtToken">The token used for auth</param>
        /// <returns>Returns information about the request to indicate whether it was scheduled successfully</returns>
        Task<HttpObjectResponse<string>> UpdateCollections(string jwtToken);

        /// <summary>
        /// Retrieves information about the progress of a Collection Administration bulk update job
        /// </summary>
        /// <param name="bulkUpdateUniqueId">Id of Collection Administration bulk update job</param>
        /// <param name="jwtToken">The token used for authentication</param>
        /// <returns>Details of the bulk update job, including current status</returns>
        Task<HttpObjectResponse<CollectionAdministration>> GetUpdateStatus(string bulkUpdateUniqueId, string jwtToken);
    }
}
