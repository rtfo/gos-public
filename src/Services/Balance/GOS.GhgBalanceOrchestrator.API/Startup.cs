﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AutoMapper;
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgBalance.Common.Data;
using DfT.GOS.GhgBalance.Common.Repositories;
using DfT.GOS.GhgBalanceOrchestrator.API.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.GhgLedger.API.Client.Services;
using DfT.GOS.HealthChecks.Common;
using DfT.GOS.HealthChecks.Common.Extensions;
using DfT.GOS.HealthChecks.Mongo;
using DfT.GOS.HealthChecks.Rabbit;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Messaging.Client;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.RtfoApplications.API.Client.Services;
using DfT.GOS.Security.Authentication;
using DfT.GOS.Web.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;

namespace DfT.GOS.GhgBalanceOrchestrator.API
{
    /// <summary>
    /// The startup file for the GHG Balance Orchestrator API
    /// </summary>
    public class Startup
    {
        #region Constants

        /// <summary>
        /// The environment variable to parse for the service identifier used to gain a JWT token
        /// for authentication against remote services
        /// </summary>
        public const string EnvironmentVariable_ServiceIdentifier = "SERVICE_IDENTIFIER";

        /// <summary>
        /// The environment variable to parse for the connection string
        /// </summary>
        public const string EnvironmentVariable_ConnectionString = "CONNECTION_STRING";

        /// <summary>
        /// The environment variable to parse for the database name
        /// </summary>
        public const string EnvironmentVariable_DatabaseName = "DATABASE_NAME";

        /// <summary>
        /// The environment variable for the security JWT Token Audience used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenAudience = "JWT_TOKEN_AUDIENCE";

        /// <summary>
        /// The environment variable for the security JWT Token Issuer used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenIssuer = "JWT_TOKEN_ISSUER";

        /// <summary>
        /// The environment variable for the security JWT Token Key used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenKey = "JWT_TOKEN_KEY";

        /// <summary>
        /// The environment variable for the messaging client username
        /// </summary>
        public const string EnvironmentVariable_MessagingClientUsername = "MESSAGINGCLIENT:USERNAME";

        /// <summary>
        /// The environment variable for the messaging client password
        /// </summary>
        public const string EnvironmentVariable_MessagingClientPassword = "MESSAGINGCLIENT:PASSWORD";

        /// <summary>
        /// The error message to throw when no service identifier is supplied
        /// </summary>
        public const string ErrorMessage_ServiceIdentifierNotSupplied = "Unable to get the Service Idetifier from the environmental variables. Missing variable: " + EnvironmentVariable_ServiceIdentifier;

        /// <summary>
        /// The error message to throw when no connection string is supplied
        /// </summary>
        public const string ErrorMessage_ConnectionStringNotSupplied = "Unable to connect to the GHG Balance Orchestrator datasource, the connection string was not specified. Missing variable: " + EnvironmentVariable_ConnectionString;

        /// <summary>
        /// The error message to throw when no connection string is supplied
        /// </summary>
        public const string ErrorMessage_DatabaseNameNotSupplied = "Unable to connect to the GHG Balance Orchestrator datasource, the database name was not specified. Missing variable: " + EnvironmentVariable_DatabaseName;

        /// <summary>
        /// The error message to throw when no JWT Token Audience environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenAudience = "Unable to get the JWT Token Audience from the enironmental variables.  Missing variable: " + EnvironmentVariable_JwtTokenAudience;

        /// <summary>
        /// The error message to throw when no JWT Token Issuer environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenIssuer = "Unable to get the JWT Token Issuer from the enironmental variables.  Missing variable: " + EnvironmentVariable_JwtTokenIssuer;

        /// <summary>
        /// The error message to throw when no JWT Token Key environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenKey = "Unable to get the JWT Token Key from the enironmental variables.  Missing variable: " + EnvironmentVariable_JwtTokenKey;

        /// <summary>
        /// The error message to throw when messaging client user name environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_MessagingClientUsername = "Unable to get the Messaging Client User Name from the enironmental veriables.  Missing variable: " + EnvironmentVariable_MessagingClientUsername;

        /// <summary>
        /// The error message to throw when messaging client password environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_MessagingClientPassword = "Unable to get the Messaging Client Password from the enironmental veriables.  Missing variable: " + EnvironmentVariable_MessagingClientPassword;

        #endregion Constants

        #region Properties
        private ILogger<Startup> Logger { get; set; }

        /// <summary>
        /// Gets the configuration settings
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="configuration">The configuration settings</param>
        /// <param name="logger">The logger</param>
        public Startup(IConfiguration configuration
            , ILogger<Startup> logger)
        {
            this.Logger = logger;
            this.Logger.LogInformation("Startup called for new instance of DfT.GOS.GhgBalanceOrchestrator.API");
            this.Configuration = configuration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            this.ValidateEnvironmentVariables();

            var appSettings = Configuration.Get<AppSettings>();
            services.AddLogging();

            var cacheEntryOptions = new MemoryCacheEntryOptions()
               .SetAbsoluteExpiration(TimeSpan.FromSeconds(appSettings.HttpClient.HttpClientCacheTimeout));

            var connectionString = Configuration[EnvironmentVariable_ConnectionString];
            var databaseName = Configuration[EnvironmentVariable_DatabaseName];

            // - Resolve the mongo connection
            services.AddSingleton<IMongoClient>(c => new MongoClient(connectionString));
            services.AddTransient<IGhgBalanceContext>(s => new GhgBalanceContext(s.GetService<IMongoClient>(), databaseName));

            // - Resolve the background task execution services
            services.AddHostedService<QueuedHostedService>();
            services.AddHostedService<BalanceCollectionAdministrationBackgroundService>();
            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
            services.AddSingleton<MessagingBackgroundServiceHealthCheck>();
            services.AddHostedService<MessagingBackgroundService>();

            // - Resolve Service References
            services.Configure<AppSettings>(Configuration);
            services.AddTransient<IBalanceService, BalanceService>();
            services.AddTransient<IBalanceCalculationService, BalanceCalculationService>();

            // - Resolve Repositories
            services.AddSingleton(typeof(MemoryCacheEntryOptions), cacheEntryOptions);
            services.AddScoped<IBalanceRepositoryFactory, BalanceRepositoryFactory>();
            services.AddScoped<IBalanceCollectionAdministrationRepository, BalanceCollectionAdministrationRepository>();

            // Configure Services / Resiliant HttpClient for service calls
            var httpConfigurer = new HttpClientConfigurer(appSettings.HttpClient, this.Logger);

            httpConfigurer.Configure(services.AddHttpClient<IGhgFuelsService, GhgFuelsService>(config => { config.BaseAddress = new Uri(appSettings.GhgFuelsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IReportingPeriodsService, ReportingPeriodsService>(config => { config.BaseAddress = new Uri(appSettings.ReportingPeriodsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IOrganisationsService, OrganisationsService>(config => { config.BaseAddress = new Uri(appSettings.OrganisationsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IIdentityService, IdentityService>(config => { config.BaseAddress = new Uri(appSettings.IdentityServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IApplicationsService, ApplicationsService>(config => { config.BaseAddress = new Uri(appSettings.ApplicationsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<ICalculationsService, CalculationsService>(config => { config.BaseAddress = new Uri(appSettings.CalculationsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IAdminConsignmentsService, AdminConsignmentsService>(config => { config.BaseAddress = new Uri(appSettings.AdminConsignmentsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IVolumesService, VolumesService>(config => { config.BaseAddress = new Uri(appSettings.VolumesServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<ILedgerService, LedgerService>(config => { config.BaseAddress = new Uri(appSettings.GhgLedgerServiceApiUrl); }));

            // Configure Messaging Client
            // This needs to be a singleton so that the Rabbit connection is reused
            services.AddSingleton<IMessagingClient>(s => new RabbitMessagingClient(appSettings.MessagingClient, s.GetService<ILogger<RabbitMessagingClient>>()));

            // - Resolve Messaging Health Check
            // This needs to be a singleton so that the Rabbit connection is reused
            services.AddSingleton(s => new RabbitHealthCheck(RabbitMessagingClient.BuildConnectionFactory(appSettings.MessagingClient), s.GetService<ILogger<RabbitHealthCheck>>()));

            //JWT Security Authentication
            var jwtSettings = new JwtAuthenticationSettings(Configuration[EnvironmentVariable_JwtTokenIssuer]
                , Configuration[EnvironmentVariable_JwtTokenAudience]
                , Configuration[EnvironmentVariable_JwtTokenKey]);

            var jwtConfigurer = new JwtAuthenticationConfigurer(jwtSettings);
            jwtConfigurer.Configure(services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }));

            //Add Health Checks
            //Add the Mongo health check as a liveness check because, if it fails, we need to restart the container 
            //(this is due to a limitation in the Mongo API, which does not recover from a restart of the Mongo containers)
            //The RabbitMQ API does recover from restarts of the Rabbit containers hence we add it to the readiness check
            services.AddSingleton(o => new MongoHealthCheckOptions(databaseName, appSettings.MongoHealthCheck.TimoutMilliseconds));
            services.AddHealthChecks()
                .AddCheck<MongoHealthCheck>("mongo", tags: new[] { HealthCheckTypes.Liveness })
                .AddCheck<RabbitHealthCheck>("rabbit", tags: new[] { HealthCheckTypes.Readiness })
                .AddCheck<MessagingBackgroundServiceHealthCheck>("messaging-background-service", tags: new[] { HealthCheckTypes.Readiness });

            //Register the Swagger generator
            try
            {
                //Swagger required services
                services.AddSingleton<IApiDescriptionGroupCollectionProvider, ApiDescriptionGroupCollectionProvider>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "GHG Balance Orchestrator API", Version = "v1", Description = "GHG Balance Orchestrator API is used to aggregate GHG credit/obligation details." });
                    c.IncludeXmlComments(xmlPath);
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { In = ParameterLocation.Header, Description = "Please enter JWT with Bearer into field in the format 'Bearer [JWT token]'", Name = "Authorization", Type = SecuritySchemeType.ApiKey });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement() {
                        {
                            new OpenApiSecurityScheme {
                                Reference = new OpenApiReference {
                                    Id = "Bearer"
                                    , Type = ReferenceType.SecurityScheme
                                },
                                Scheme = "bearer",
                                Name = "security",
                                In = ParameterLocation.Header
                            }, new List<string>()
                        }
                    });
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Register Swagger generator because of the following exception: " + ex.ToString());
            }

            services.AddControllers();
            services.AddMemoryCache();

            services.AddAutoMapper();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            try
            {
                //Healthcheck
                app.UseGosHealthChecks();

                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "GHG Balance Orchestrator V1");
                    c.RoutePrefix = string.Empty;
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Configure Swagger because of the following exception: " + ex.ToString());
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Validates that all expected environment variables have been supplied
        /// </summary>
        private void ValidateEnvironmentVariables()
        {
            //Service Identifier
            if (Configuration[EnvironmentVariable_ServiceIdentifier] == null)
            {
                throw new NullReferenceException(ErrorMessage_ServiceIdentifierNotSupplied);
            }

            //Connection String
            if (Configuration[EnvironmentVariable_ConnectionString] == null)
            {
                throw new NullReferenceException(ErrorMessage_ConnectionStringNotSupplied);
            }

            //Database Name
            if (Configuration[EnvironmentVariable_DatabaseName] == null)
            {
                throw new NullReferenceException(ErrorMessage_DatabaseNameNotSupplied);
            }

            //JWT Token Audience
            if (Configuration[EnvironmentVariable_JwtTokenAudience] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenAudience);
            }

            //JWT Token Issuer
            if (Configuration[EnvironmentVariable_JwtTokenIssuer] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenIssuer);
            }

            //JWT Token Key
            if (Configuration[EnvironmentVariable_JwtTokenKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenKey);
            }

            //Messaging Client User name
            if (Configuration[EnvironmentVariable_MessagingClientUsername] == null)
            {
                throw new NullReferenceException(ErrorMessage_MessagingClientUsername);
            }

            //Messaging Client Password
            if (Configuration[EnvironmentVariable_MessagingClientPassword] == null)
            {
                throw new NullReferenceException(ErrorMessage_MessagingClientPassword);
            }
        }

        #endregion Methods
    }
}
