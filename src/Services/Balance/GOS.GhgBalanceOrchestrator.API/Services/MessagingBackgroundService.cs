﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.HealthChecks.Rabbit;
using DfT.GOS.Messaging.Client;
using DfT.GOS.Messaging.Messages;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Services
{
    /// <summary>
    /// Background service for processing messages from the GHG Balance message queue
    /// </summary>
    public class MessagingBackgroundService : BackgroundService
    {
        #region Properties

        private ILogger<MessagingBackgroundService> Logger { get; set; }
        private IMessagingClient MessagingClient { get; set; }
        private MessagingBackgroundServiceHealthCheck HealthCheck { get; set; }
        private IServiceProvider Services { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all parameters
        /// </summary>
        /// <param name="messagingClient">Messaging Client instance</param>
        /// <param name="healthCheck">Health Check instance</param>
        /// <param name="logger">Logger instance</param>
        /// <param name="services">Provides access to objects in the service</param>
        public MessagingBackgroundService(IMessagingClient messagingClient, MessagingBackgroundServiceHealthCheck healthCheck, ILogger<MessagingBackgroundService> logger
            , IServiceProvider services)
        {
            this.MessagingClient = messagingClient ?? throw new ArgumentNullException(nameof(messagingClient));
            this.Services = services ?? throw new ArgumentNullException(nameof(services));
            this.HealthCheck = healthCheck ?? throw new ArgumentNullException(nameof(healthCheck));
            this.Logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion Constructors

        #region Overridden Methods

        /// <summary>
        /// Process messages from the GHG Balance message queue
        /// </summary>
        /// <param name="stoppingToken">Cancellation token</param>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            this.Logger.LogInformation("MessagingBackgroundService Started");

            stoppingToken.Register(() => this.Logger.LogInformation("MessagingBackgroundService Stopping"));

            // Specify a scope in which a BalanceService should be created, this is because this class ends up behaving like a singleton and normally a BalanceService
            // is transient but makes use of scoped repositories. You can't consume a scoped service (the repository) from a singleton.
            // This "using" limits the lifetime of this BalanceService to just this scope within this method.
            using (var scope = this.Services.CreateScope())
            {
                // Get a BalanceService (that has scoped dependencies of its own)
                var balanceService = scope.ServiceProvider.GetService<IBalanceService>();

                while (!(this.HealthCheck.StartupTaskCompleted || stoppingToken.IsCancellationRequested))
                {
                    try
                    {
                        List<Task> subscribeTasks = new List<Task>();

                        // Configure a subscription to "ROS Updated" messages (these trigger an update of balances for all Organisations and Obligation Periods)
                        subscribeTasks.Add(this.MessagingClient.Subscribe<RosUpdatedMessage>(async message =>
                        {
                            var collectionAdministration = await balanceService.CreateCollectionAdministration();
                            await balanceService.UpdateAllGhgBalances(collectionAdministration);
                            return true;
                        }));

                        // Configure a subscription to "Ledger Updated" messages (these trigger an update of the GHG balance for a specified Organisation and Obligation Period)
                        subscribeTasks.Add(this.MessagingClient.Subscribe<LedgerUpdatedMessage>(async message =>
                        {
                            await balanceService.UpdateGhgBalance(message.OrganisationId, message.ObligationPeriodId);
                            return true;
                        }));

                        // Configure a subscription to "Credits Issued" messages (these trigger an update of the GHG balance for a specified Organisation and Obligation Period)
                        subscribeTasks.Add(this.MessagingClient.Subscribe<CreditsIssuedMessage>(async message =>
                        {
                            await balanceService.UpdateGhgBalance(message.OrganisationId, message.ObligationPeriodId);
                            return true;
                        }));

                        //Configure a subscription to "Application Revoked" messages (these trigger an update of the GHG balance for a specified Organisation and Obligation Period)
                        subscribeTasks.Add(this.MessagingClient.Subscribe<ApplicationRevokedMessage>(async message =>
                        {
                            await balanceService.UpdateGhgBalance(message.OrganisationId, message.ObligationPeriodId);
                            return true;
                        }));

                        await Task.WhenAll(subscribeTasks);

                        // Check for any errors in the Tasks
                        foreach (Task subscription in subscribeTasks)
                        {
                            await subscription;
                        }

                        this.HealthCheck.StartupTaskCompleted = true;
                    }
                    catch (Exception ex)
                    {
                        this.Logger.LogError(ex, "An error occurred whilst subscribing to the GHG Balance message queues.");
                    }
                }
            }

            this.Logger.LogInformation("MessagingBackgroundService Stopped");
        }

        #endregion Overridden Methods
    }
}
