﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Services
{
    /// <summary>
    /// A background service for administering the balance collection service, responsible for running scheduled tasks
    /// </summary>
    public class BalanceCollectionAdministrationBackgroundService : BackgroundService
    {
        #region Properties

        private ILogger<Startup> Logger { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }
        private IServiceProvider Services { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="logger">The logger</param>
        /// <param name="appSettings">Application settings</param>
        /// <param name="services">Provides access to objects in the service, allows us to get an instance of the BalanceService in a specified scope</param>
        public BalanceCollectionAdministrationBackgroundService(
             ILogger<Startup> logger
            , IOptions<AppSettings> appSettings
            , IServiceProvider services)
        {
            this.Logger = logger;
            this.AppSettings = appSettings;
            this.Services = services;
        }

        #endregion Constructors

        /// <summary>
        /// Schedules admin functions, such as purging expired temporary collections.
        /// </summary>
        /// <param name="stoppingToken">The cancellation token</param>
        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            this.Logger.LogInformation("BalanceCollectionAdministrationBackgroundService Started");

            stoppingToken.Register(() => this.Logger.LogInformation("BalanceCollectionAdministrationBackgroundService Stopping"));

            while (!stoppingToken.IsCancellationRequested)
            {
                int purgedCount = 0;
                try
                {
                    // Specify a scope in which a BalanceService should be created, this is because this class ends up behaving like a singleton and normally a BalanceService
                    // is transient but makes use of scoped repositories. You can't consume a scoped service (the repository) from a singleton.
                    // This "using" limits the lifetime of this BalanceService to just this scope within this method.
                    using (var scope = this.Services.CreateScope())
                    {
                        // Get a BalanceService (that has scoped dependencies of its own)
                        var balanceService = scope.ServiceProvider.GetService<IBalanceService>();
                        //Clear out any orphaned mongo collections
                        purgedCount = await balanceService.PurgeExpiredCollections();
                    }
                }
                catch (Exception ex)
                {
                    this.Logger.LogError("An error occurred in the BalanceCollectionAdministrationBackgroundService. Details: " + ex.ToString());
                }

                if (purgedCount > 0)
                {
                    this.Logger.LogInformation("BalanceCollectionAdministrationBackgroundService - "
                        + purgedCount.ToString() + " expired temporary collections deleted");
                }

                var delayMinutes = this.AppSettings.Value.BalanceCollectionAdministrationBackgroundServiceDelayMinutes;
                await Task.Delay(delayMinutes * 60000, stoppingToken);
            }

            this.Logger.LogInformation("BalanceCollectionAdministrationBackgroundService Stopping");
        }
    }
}
