﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgCalculations.Orchestration;
using DfT.GOS.GhgFuels.API.Client.Services;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.RtfoApplications.API.Client.Models;
using DfT.GOS.RtfoApplications.API.Client.Services;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Microsoft.Extensions.Logging;
using DfT.GOS.GhgBalance.Common.Repositories;
using DfT.GOS.GhgBalanceOrchestrator.API.Exceptions;
using DfT.GOS.GhgBalance.Common.Data.Administration;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Services
{
    /// <summary>
    /// Service to aggregate Balance details and persist them
    /// </summary>
    public class BalanceService : IBalanceService
    {
        #region Properties

        private ILogger Logger { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }
        private IBalanceCollectionAdministrationRepository BalanceCollectionAdministrationRepository { get; set; }
        private IBalanceRepositoryFactory BalanceRepositoryFactory { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private IOrganisationsService OrganisationsService { get; set; }
        private IGhgFuelsService GhgFuelsService { get; set; }       
        private IIdentityService IdentityService { get; set; }
        private IApplicationsService GhgApplicationsService { get; set; }
        private ICalculationsService CalculationsService { get; set; }
        private IAdminConsignmentsService AdminConsignmentsService { get; set; }
        private IVolumesService VolumesService { get; set; }
        private IBalanceCalculationService BalanceCalculationService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor using taking all dependancies
        /// </summary>
        /// <param name="appSettings">Application Settings</param>
        /// <param name="logger">The Logger</param>
        /// <param name="balanceCollectionAdministrationRepository">The repository used to administer temporary collections (Green / blue loading)</param>
        /// <param name="temporaryBalanceRepositoryFactory">The factory used to reference temporary balance repository collections</param>
        /// <param name="reportingPeriodsService">The service for getting information about reporting periods</param>
        /// <param name="organisationsService">The service for getting information about organisations</param>
        /// <param name="ghgFuelsService">The service for getting information about Fuel Types</param>
        /// <param name="identityService">The service for performing authentication</param>
        /// <param name="ghgApplicationsService">The service for getting information about GHG applications</param>
        /// <param name="calculationsService">The service for performing calculations</param>
        /// <param name="adminConsignmentsService">The service for getting admin consignment submissions information</param>
        /// <param name="volumesService">The service for getting volume submissions information</param>
        /// <param name="balanceCalculationService">The service used to calculate the balance for an obligation period / organisation</param>
        public BalanceService(IOptions<AppSettings> appSettings
            , ILogger<Startup> logger
            , IBalanceCollectionAdministrationRepository balanceCollectionAdministrationRepository
            , IBalanceRepositoryFactory temporaryBalanceRepositoryFactory
            , IReportingPeriodsService reportingPeriodsService
            , IOrganisationsService organisationsService
            , IGhgFuelsService ghgFuelsService
            , IIdentityService identityService
            , IApplicationsService ghgApplicationsService
            , ICalculationsService calculationsService
            , IAdminConsignmentsService adminConsignmentsService
            , IVolumesService volumesService
            , IBalanceCalculationService balanceCalculationService)
        {
            //Setup Dependancies
            this.AppSettings = appSettings;
            this.Logger = logger;
            this.BalanceCollectionAdministrationRepository = balanceCollectionAdministrationRepository;
            this.BalanceRepositoryFactory = temporaryBalanceRepositoryFactory;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.OrganisationsService = organisationsService;
            this.GhgFuelsService = ghgFuelsService;
            this.IdentityService = identityService;
            this.GhgApplicationsService = ghgApplicationsService;
            this.CalculationsService = calculationsService;
            this.AdminConsignmentsService = adminConsignmentsService;
            this.VolumesService = volumesService;
            this.BalanceCalculationService = balanceCalculationService;
        }

        #endregion Constructors

        #region IBalanceService Implementation 

        /// <summary>
        /// Creates a new Collection Administration record, used to track the progress of a bulk update operation
        /// </summary>
        /// <returns>A new CollectionAdministration object</returns>
        async Task<CollectionAdministration> IBalanceService.CreateCollectionAdministration()
        {
            //Create a unique ID to enable us to monitor the background work
            var bulkUpdateUniqueId = Guid.NewGuid();

            //Check that the Bulk update unique ID hasn't already been used
            var existingCollectionAdmin = await this.BalanceCollectionAdministrationRepository.Get(bulkUpdateUniqueId);
            if (existingCollectionAdmin != null)
            {
                throw new BalanceOrchestrationException(string.Format("The generated bulk update unique id, {0}, has already been used."
                    , bulkUpdateUniqueId.ToString()));
            }

            var timeoutMinutes = this.AppSettings.Value.BulkUpdateTimeoutMinutes;
            var collectionAdministration = new CollectionAdministration(bulkUpdateUniqueId, DateTime.Now, DateTime.Now.AddMinutes(timeoutMinutes));

            if (!await this.BalanceCollectionAdministrationRepository.Create(collectionAdministration))
            {
                throw new BalanceOrchestrationException("Unable to create a new balance administration record.");
            }

            return collectionAdministration;
        }

        /// <summary>
        /// Gets status details for a bulk update by the unique bulk update ID the was returned by the UpdateCollections method
        /// </summary>
        /// <param name="bulkUpdateUniqueId">The unique ID of the bulk update operation to enable tracking of progress</param>
        /// <returns>Returns the collection update status information, if it exists</returns>
        async Task<CollectionAdministration> IBalanceService.GetBulkUpdateStatus(Guid bulkUpdateUniqueId)
        {
            return await this.BalanceCollectionAdministrationRepository.Get(bulkUpdateUniqueId);
        }

        /// <summary>
        /// Deletes any collections that have been created as part of a bulk update, but have expired
        /// </summary>
        /// <returns>Returns the number of collections that were purged</returns>
        async Task<int> IBalanceService.PurgeExpiredCollections()
        {
            int purgedCollectionCount = 0;
            var temporaryCollections = await this.BalanceCollectionAdministrationRepository.GetTemporaryCollectionAdministrationRecords();

            if (temporaryCollections != null)
            {
                foreach (var temporaryCollection in temporaryCollections)
                {
                    if (temporaryCollection.HasTimedOut)
                    {
                        temporaryCollection.Status = CollectionAdministrationStatus.Failed;
                        temporaryCollection.Errors.Add("Bulk Load expired, temporary collection deleted.");
                        await this.BalanceCollectionAdministrationRepository.Update(temporaryCollection);

                        var temporaryBalanceRepository = this.BalanceRepositoryFactory.GetTemporaryRepository(temporaryCollection.Id);
                        await temporaryBalanceRepository.Drop();

                        this.Logger.LogWarning("Bulk Load expired for " + temporaryCollection.Id.ToString() + ", temporary collection deleted.");

                        purgedCollectionCount++;
                    }
                }
            }

            return purgedCollectionCount;
        }

        /// <summary>
        /// Updates all GHG Balances for all companies and all GHG Obligation Periods
        /// </summary>
        /// <param name="collectionAdministration">A CollectionAdministration record used to identify the bulk update process to enable reporting on progress</param>
        async Task IBalanceService.UpdateAllGhgBalances(CollectionAdministration collectionAdministration)
        {
            //Get a unique ID for the temporary collection used to do Green/Blue loading          
            this.Logger.LogInformation("GhgBalanceOrchestrator.UpdateAllGhgBalances - Started loading collection " + collectionAdministration.Id);

            //Get a reference to the temporary repository we'll populate first, before switching to the master repository
            var temporaryBalanceRepository = this.BalanceRepositoryFactory.GetTemporaryRepository(collectionAdministration.Id);

            try
            {
                //Get the details for repopulating the repository
                var authResult = await this.IdentityService.AuthenticateService(this.AppSettings.Value.SERVICE_IDENTIFIER);
                var obligationPeriodsTask = this.ReportingPeriodsService.GetObligationPeriods();
                var fuelTypesTask = this.GhgFuelsService.GetFuelTypes();

                await Task.WhenAll(obligationPeriodsTask
                    , fuelTypesTask);

                var fuelTypes = await fuelTypesTask;

                //TODO: Remove this once ROSGOS-1342 is implemented
                if (fuelTypes == null)
                {
                    throw new BalanceOrchestrationException("No fuel types found");
                }

                if (!authResult.Succeeded)
                {
                    throw new BalanceOrchestrationException("Unable to authenticate using the Identity Service");
                }
                else
                {
                    var organisations = await this.OrganisationsService.GetSuppliersAndTraders(authResult.Token);
                    var obligationPeriods = await obligationPeriodsTask;

                    var calculationOrchestrator = new ApplicationsCalculationOrchestrator(this.GhgApplicationsService
                        , this.CalculationsService
                        , fuelTypes
                        , authResult.Token);

                    foreach (var obligationPeriod in obligationPeriods)
                    {
                        //The Calculation is dependent on the obligation period having the GHG Threshold set. If it doesn't, don't run the calculation.
                        if (obligationPeriod.GhgThresholdGrammesCO2PerMJ.HasValue)
                        {
                            this.Logger.LogInformation("GhgBalanceOrchestrator.UpdateAllGhgBalances - Processing Obligation Period " + obligationPeriod.Id);

                            //Get admin consignments for the obligation period
                            var adminConsignmentsTask = this.AdminConsignmentsService.Get(obligationPeriod.Id, authResult.Token);

                            //Get volumes for the obligation period
                            var volumesTask = this.VolumesService.Get(obligationPeriod.Id, authResult.Token);

                            await Task.WhenAll(adminConsignmentsTask, volumesTask);

                            var allObligationPeriodAdminConsignmentResponse = await adminConsignmentsTask;
                            var allObligationPeriodVolumesResponse = await volumesTask;

                            if(!allObligationPeriodAdminConsignmentResponse.HasResult)
                            {
                                throw new BalanceOrchestrationException("Unable to contact the remote Admin Consignment service.");
                            }

                            if(!allObligationPeriodVolumesResponse.HasResult)
                            {
                                throw new BalanceOrchestrationException("Unable to contact the remote Volumes service.");
                            }

                            var updateGhgBalanceTasks = new List<Task<bool>>();

                            foreach (var supplier in organisations.Result)
                            {
                                var adminConsignments = allObligationPeriodAdminConsignmentResponse.Result.Where(ac => ac.OrganisationId == supplier.Id);
                                var volumes = allObligationPeriodVolumesResponse.Result.Where(ac => ac.OrganisationId == supplier.Id);

                                updateGhgBalanceTasks.Add(this.UpdateGhgBalance(obligationPeriod
                                    , supplier
                                    , fuelTypes
                                    , adminConsignments
                                    , volumes
                                    , calculationOrchestrator
                                    , authResult.Token
                                    , temporaryBalanceRepository));
                            }
                            
                            try
                            {
                                await Task.WhenAll(updateGhgBalanceTasks);
                            }
                            catch
                            {
                                //Report all exceptions that might have occurred, that are now reported by the WhenAll
                                var exceptions = updateGhgBalanceTasks.Where(t => t.Exception != null).Select(t => t.Exception);

                                foreach (var exception in exceptions)
                                {
                                    collectionAdministration.Errors.Add(string.Format("An error occurred attempting to update the balance: " + exception.ToString()));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError("GhgBalanceOrchestrator: One or more errors occurred attempting to update the balance. Exception:" + ex.ToString());
                collectionAdministration.Errors.Add(string.Format("One or more errors occurred attempting to update the balance. Please check the logs for details."));
            }

            if (collectionAdministration.HasTimedOut)
            {
                //The update took much longer than expected, so we'll abandon it because it might have been replaced by another bulk update
                collectionAdministration.Errors.Add("The administration collection update timed out at " 
                    + DateTime.Now + ". The timeout period is " + this.AppSettings.Value.BulkUpdateTimeoutMinutes + " minutes.");

                this.Logger.LogError("GhgBalanceOrchestrator: Bulk update timeout occurred for process "+ collectionAdministration.Id.ToString());
            }

            if (collectionAdministration.HasErrors)
            {
                //Errors occured - report and cleanup the temporary collection
                collectionAdministration.Status = CollectionAdministrationStatus.Failed;
                await this.BalanceCollectionAdministrationRepository.Update(collectionAdministration);
                await temporaryBalanceRepository.Drop();
            }
            else
            {
                //Now the temporary collection is populated, rename it to be the master collection (re-pointing the master data)
                var updatedToMaster = await temporaryBalanceRepository.UpdateToMaster();

                //Update the admin record to state it has been completed
                collectionAdministration.LoadSuccessfullyCompleted = DateTime.Now;
                collectionAdministration.Status = CollectionAdministrationStatus.Succeeded;
                if (updatedToMaster
                        && !await this.BalanceCollectionAdministrationRepository.Update(collectionAdministration))
                {
                    throw new BalanceOrchestrationException("Unable to update the administration collection for temporary collection "
                        + collectionAdministration.Id);
                }
            }                 

            this.Logger.LogInformation("GhgBalanceOrchestrator.UpdateAllGhgBalances - Ended for collection " + collectionAdministration.Id);
        }

        /// <summary>
        ///  Updates the GHG Balance for an organisation within an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation to populate the balance for</param>
        /// <param name="obligationPeriodId">The ID of the obligation period to populate the balance for</param>
        /// <returns>Returns a boolean value indicating success</returns>
        async Task<bool> IBalanceService.UpdateGhgBalance(int organisationId, int obligationPeriodId)
        {
            this.Logger.LogInformation("GhgBalanceOrchestrator.UpdateGhgBalance - Started");

            if(organisationId <= 0)
            {
                throw new ArgumentOutOfRangeException("organisationId");
            }

            if (obligationPeriodId <= 0)
            {
                throw new ArgumentOutOfRangeException("obligationPeriodId");
            }

            //Get the details for repopulating the repository
            var authResult = await this.IdentityService.AuthenticateService(this.AppSettings.Value.SERVICE_IDENTIFIER);

            if(!authResult.Succeeded 
                || authResult.Token == null)
            {
                throw new BalanceOrchestrationAuthenticationException("Unable to authenticate the balance orchestrator against the identity service.");
            }

            var obligationPeriodsTask = this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            var fuelTypesTask = this.GhgFuelsService.GetFuelTypes();

            //Get admin consignments
            var adminConsignmentsTask = this.AdminConsignmentsService.Get(obligationPeriodId, organisationId, authResult.Token);

            //Get volumes
            var volumesTask = this.VolumesService.Get(obligationPeriodId, organisationId, authResult.Token);

            //Get the organisation
            var organisationResponseTask = this.OrganisationsService.GetOrganisation(organisationId, authResult.Token);

            await Task.WhenAll(obligationPeriodsTask
                , fuelTypesTask
                , adminConsignmentsTask
                , volumesTask
                , organisationResponseTask);
            
            var fuelTypes = await fuelTypesTask;
            var obligationPeriod = await obligationPeriodsTask;
            var organisationResponse = await organisationResponseTask;
            var volumesResponse = await volumesTask;
            var adminConsignmentsResponse = await adminConsignmentsTask;

            //Check the results from the remote services

            //TODO: Remove this once ROSGOS-1342 is implemented
            if (fuelTypes == null)
            {
                throw new BalanceOrchestrationException("No fuel types found");
            }

            if (obligationPeriod == null)
            {
                throw new ArgumentException("No obligation period found with ID " + obligationPeriodId, "obligationPeriodId");
            }

            if (!organisationResponse.HasResult)
            {
                throw new ArgumentException("No organisation found with ID " + organisationId, "organisationId");
            }

            if (!adminConsignmentsResponse.HasResult)
            {
                throw new BalanceOrchestrationException("No admin consignment data could be retrieved.");
            }

            if (!volumesResponse.HasResult)
            {
                throw new BalanceOrchestrationException("No volume data could be retrieved.");
            }

            var calculationOrchestrator = new ApplicationsCalculationOrchestrator(this.GhgApplicationsService
                    , this.CalculationsService
                    , fuelTypes
                    , authResult.Token);

            //Update the master repository
            var organisationBalance = await this.BalanceCalculationService.CalculateGhgBalance(obligationPeriod
                , organisationResponse.Result
                , fuelTypes
                , adminConsignmentsResponse.Result
                , volumesResponse.Result
                , calculationOrchestrator
                , authResult.Token);

            var result = await this.BalanceRepositoryFactory.GetMasterRepository().Save(organisationBalance);

            //Update any temporary repositories that are being processed as part of bulk update
            var tempCollectionAdminRecords = await this.BalanceCollectionAdministrationRepository.GetTemporaryCollectionAdministrationRecords();

            if (tempCollectionAdminRecords != null)
            {
                foreach (var tempCollectionAdminRecord in tempCollectionAdminRecords)
                {
                    var tempCollectionRepository = this.BalanceRepositoryFactory.GetTemporaryRepository(tempCollectionAdminRecord.Id);
                    await tempCollectionRepository.Save(organisationBalance);
                }
            }

            this.Logger.LogInformation("GhgBalanceOrchestrator.UpdateGhgBalance - Ended");

            return result;
        }

        #endregion IBalanceService Implementation 

        #region Private Methods

        /// <summary>
        /// Updates a GHG Balance for a organisation within an obligation period
        /// </summary>
        /// <param name="obligationPeriod">The obligation period</param>
        /// <param name="supplier">The supplier details</param>
        /// <param name="fuelTypes">A list of all fuel types</param>
        /// <param name="adminConsignments">A list of Admin Consignments for the company / obligation period</param>
        /// <param name="volumes">A list of Volumes for the company / obligation period</param>
        /// <param name="applicationsCalculationOrchestrator">The Orchestrator to use to orchestrate application calculations</param>
        /// <param name="jwtToken">JWT Token for calling remote services</param>
        /// <param name="balanceRepository">The repository to update</param>
        /// <returns>Returns a bool indicating success</returns>
        private async Task<bool> UpdateGhgBalance(ObligationPeriod obligationPeriod
            , Organisation supplier
            , IList<FuelType> fuelTypes
            , IEnumerable<AdminConsignment> adminConsignments
            , IEnumerable<NonRenewableVolume> volumes
            , ApplicationsCalculationOrchestrator applicationsCalculationOrchestrator
            , string jwtToken
            , IBalanceRepository balanceRepository)
        {
            try
            {
                var organisationBalance = await this.BalanceCalculationService.CalculateGhgBalance(obligationPeriod
                    , supplier
                    , fuelTypes
                    , adminConsignments
                    , volumes
                    , applicationsCalculationOrchestrator
                    , jwtToken);            

                return await balanceRepository.Save(organisationBalance);
            }
            catch (Exception ex)
            {
                this.Logger.LogError(string.Format("Unable to update GHG Balance for company {0}, obligation period {1}."
                    + " Details: {2}", supplier.Id, obligationPeriod.Id, ex.ToString()));

                throw new BalanceOrchestrationException(string.Format("Unable to update GHG Balance for company {0}, obligation period {1}."
                    , supplier.Id, obligationPeriod.Id));
            }
        }

        #endregion Private Methods
    }
}
