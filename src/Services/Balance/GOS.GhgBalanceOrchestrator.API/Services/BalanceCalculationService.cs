﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgBalanceOrchestrator.API.Models;
using DfT.GOS.GhgCalculations.Common.Models;
using DfT.GOS.GhgCalculations.Orchestration;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.GhgLedger.API.Client.Models;
using DfT.GOS.GhgLedger.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.RtfoApplications.API.Client.Models;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Services
{
    /// <summary>
    /// Service for calculating the an organisation's balance for an obligation period
    /// </summary>
    public class BalanceCalculationService : IBalanceCalculationService
    {
        #region Constants

        private const int numberOfKilogramsInTonne = 1000;
        private const int numberOfPenceInAPound = 100;

        #endregion Constants

        #region Properties

        private ILedgerService LedgerService { get; set; }
        private IApplicationsService GhgApplicationsService { get; set; }
        private IOrganisationsService OrganisationsService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="ledgerService">The ledger service</param>
        /// <param name="ghgApplicationsService">The GHG Applications Service</param>
        /// <param name="organisationsService">The GHG Applications Service</param>
        public BalanceCalculationService(ILedgerService ledgerService
            , IApplicationsService ghgApplicationsService, IOrganisationsService organisationsService)
        {
            this.LedgerService = ledgerService;
            this.GhgApplicationsService = ghgApplicationsService;
            this.OrganisationsService = organisationsService;
        }

        #endregion Constructors

        #region IBalanceCalculationService Implementation

        /// <summary>
        /// Calculates the GHG Balance for a organisation within an obligation period
        /// </summary>
        /// <param name="obligationPeriod">The obligation period</param>
        /// <param name="organisation">The organisation details</param>
        /// <param name="adminConsignments">A list of Admin Consignments for the company / obligation period</param>
        /// <param name="volumes">A list of Volumes for the company / obligation period</param>
        /// <param name="fuelTypes">A list of fuel types</param>
        /// <param name="applicationsCalculationOrchestrator">The Orchestrator to use to orchestrate application calculations</param>
        /// <param name="jwtToken">JWT Token for calling remote services</param>
        /// <returns>Returns the constructed GHG Balance</returns>
        async Task<OrganisationBalance> IBalanceCalculationService.CalculateGhgBalance(ObligationPeriod obligationPeriod
            , Organisation organisation
            , IList<FuelType> fuelTypes
            , IEnumerable<AdminConsignment> adminConsignments
            , IEnumerable<NonRenewableVolume> volumes
            , ApplicationsCalculationOrchestrator applicationsCalculationOrchestrator
            , string jwtToken)
        {
            //Get GHG Application details
            var issuedApplicationDetailsTask = this.GetIssuedCreditsApplicationDetails(organisation.Id, obligationPeriod.Id, jwtToken);

            //Get GHG Ledger items
            var transactionsTask = this.LedgerService.Get(obligationPeriod.Id, organisation.Id, jwtToken);

            await Task.WhenAll(issuedApplicationDetailsTask, transactionsTask);
            var issuedApplicationDetails = await issuedApplicationDetailsTask;
            var transactions = await transactionsTask;

            //Filter out any Non-Renewable-volumes with zero Volume,
            // these have almost certainly been offset entirely by Admin Consignments.
            // We want to filer this noise out because it is confusing to users (see ROSGOS-1523 for further details)
            var filteredVolumes = volumes.Where(v => v.Volume > 0);

            //Perform the calculation
            var calculationResult = await applicationsCalculationOrchestrator.GetCalculationBreakdownForApplicationItems(issuedApplicationDetails.ApplicationItems
                , adminConsignments
                , filteredVolumes
                , organisation.Id
                , obligationPeriod.Id
                , obligationPeriod.GhgThresholdGrammesCO2PerMJ.Value
                , obligationPeriod.DeMinimisLitres
                , obligationPeriod.NoDeMinimisLitres
                , obligationPeriod.GhgLowVolumeSupplierDeductionKGCO2.Value);

            var submissions = this.GetSubmissions(calculationResult
                , issuedApplicationDetails.ApplicationItems
                , issuedApplicationDetails.Applications
                , adminConsignments
                , filteredVolumes
                , fuelTypes);

            var trading = await this.GetTrading(transactions.Result, jwtToken);
            var totalCredits = submissions.TotalCredits + trading.Balance;
            var totalObligation = submissions.TotalObligation;
            var netBalance = totalCredits - totalObligation;
            var buyOutPoundsPer_tCO2e = obligationPeriod.GhgBuyoutPencePerKGCO2.Value * numberOfKilogramsInTonne / numberOfPenceInAPound;

            //Buy out cost
            decimal buyOutCost = 0;
            if (netBalance < 0)
            {
                buyOutCost = (-1 * (netBalance / numberOfKilogramsInTonne) * buyOutPoundsPer_tCO2e);
            }

            var organisationBalance = new OrganisationBalance(obligationPeriod.Id
                , obligationPeriod.EndDate
                , organisation.Id
                , organisation.Name
                , submissions
                , trading
                , totalCredits
                , totalObligation
                , netBalance
                , buyOutPoundsPer_tCO2e
                , buyOutCost);

            return organisationBalance;
        }

        #endregion IBalanceCalculationService Implementation

        #region Private Methods

        /// <summary>
        /// Get Application / Application Item details for applications that have had credits issued against them for an organisation in an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="jwtToken">The JWT Token for connecting to remote services</param>
        /// <returns></returns>
        private async Task<ApplicationItemsApplications> GetIssuedCreditsApplicationDetails(int organisationId, int obligationPeriodId, string jwtToken)
        {
            //Get GHG Application details
            var applicationItemsResponse = await this.GhgApplicationsService.GetApplicationItemsWithIssuedCredits(organisationId, obligationPeriodId, jwtToken);

            //Get all Applications for the application items
            var applications = new List<ApplicationSummary>();
            var applicationIds = applicationItemsResponse.Result.Select(i => i.ApplicationId).Distinct();

            foreach (var applicationId in applicationIds)
            {
                var applicationResponse = await this.GhgApplicationsService.GetApplicationSummary(applicationId, jwtToken);
                applications.Add(applicationResponse.Result);
            }

            return new ApplicationItemsApplications(applications, applicationItemsResponse.Result);
        }

        /// <summary>
        /// Translates a calculation result into a submission
        /// </summary>
        /// <param name="calculationResult">The calculation result to translate</param>
        /// <param name="applicationItems">The GHG Application Items to enrich the calculation result with</param>
        /// <param name="applications">The GHG Applications (corresponding to the application items) to enrich the calculate the calculation result with</param>
        /// <param name="adminConsignments">The RTFO Admin consignments to enrich the calculation result with</param>
        /// <param name="volumes">The RTFO volumes to enrich the calculation result with</param>
        /// <param name="fuelTypes">A list of all fuel types</param>
        /// <returns>Returns a submission</returns>
        private Submissions GetSubmissions(CalculationResult calculationResult
            , IList<ApplicationItemSummary> applicationItems
            , IList<ApplicationSummary> applications
            , IEnumerable<AdminConsignment> adminConsignments
            , IEnumerable<NonRenewableVolume> volumes
            , IList<FuelType> fuelTypes)
        {
            //Liquid and gas
            var adminConsignmentItems = new List<RtfoAdminConsignmentSubmission>();
            var volumeItems = new List<RtfoVolumeSubmission>();
            var fossilGasItems = new List<FossilGasSubmission>();

            foreach (var calculationItem in calculationResult.VolumeCalculation.Items)
            {
                switch (calculationItem.FuelCategoryReference)
                {
                    case ApplicationsCalculationOrchestrator.VolumeFuelCategory_AdminConsignment:
                        //Admin Consignment
                        var adminConsignment = adminConsignments.Single(ac => ac.AdminConsignmentId.ToString() == calculationItem.Reference);

                        adminConsignmentItems.Add(new RtfoAdminConsignmentSubmission(adminConsignment.ReportedMonthEndDate
                            , adminConsignment.AdminConsignmentId
                            , adminConsignment.Reference
                            , adminConsignment.FuelTypeId
                            , adminConsignment.FuelTypeName
                            , adminConsignment.FeedStockId
                            , adminConsignment.FeedStockName
                            , adminConsignment.Volume
                            , calculationItem.EnergyDensity
                            , calculationItem.GhgIntensity
                            , calculationItem.DifferenceToTarget
                            , calculationItem.EnergySupplied
                            , calculationItem.Credits
                            , calculationItem.Obligation));
                        break;

                    case ApplicationsCalculationOrchestrator.VolumeFuelCategory_Volume:
                        //Volume
                        var volumeCompositeKey = new VolumeCompositeKey(calculationItem.Reference);

                        var volume = volumes.Single(v => v.ReportedMonthEndDate == volumeCompositeKey.ReportedMonthEndDate
                                                            && v.FuelTypeId == volumeCompositeKey.FuelTypeId
                                                            && v.OrganisationId == volumeCompositeKey.OrganisationId
                                                            && v.ObligationPeriodId == volumeCompositeKey.ObligationPeriodId);

                        volumeItems.Add(new RtfoVolumeSubmission(volume.ReportedMonthEndDate
                            , volume.FuelTypeId
                            , volume.FuelTypeName
                            , volume.Volume
                            , calculationItem.EnergyDensity
                            , calculationItem.GhgIntensity
                            , calculationItem.DifferenceToTarget
                            , calculationItem.EnergySupplied
                            , calculationItem.Credits
                            , calculationItem.Obligation));

                        break;
                    case ApplicationsCalculationOrchestrator.VolumeFuelCategory_FossilGas:
                        //Fossil Gas
                        var fossilGas = applicationItems.Single(i => i.ApplicationItemId == int.Parse(calculationItem.Reference));

                        var application = applications.Single(a => a.ApplicationId == fossilGas.ApplicationId);
                        var fuelType = fuelTypes.Single(ft => ft.FuelTypeId == fossilGas.FuelTypeId);

                        fossilGasItems.Add(new FossilGasSubmission(application.ApplicationId
                            , application.SubmittedDate
                            , fossilGas.ApplicationItemId
                            , fossilGas.FuelTypeId
                            , fuelType.Name
                            , calculationItem.Volume
                            , calculationItem.EnergyDensity
                            , calculationItem.GhgIntensity
                            , calculationItem.DifferenceToTarget
                            , calculationItem.EnergySupplied
                            , calculationItem.Credits
                            , calculationItem.Obligation));

                        break;
                    default:
                        throw new NotSupportedException("Fuel type not supported");
                }
            }

            //Admin Consignments
            var adminConsignmentTotalCredits = adminConsignmentItems.Sum(s => s.Credits);
            var adminConsignmentTotalObligation = adminConsignmentItems.Sum(s => s.Obligation);
            var adminConsignmentTotalVolumeSupplied = adminConsignmentItems.Sum(s => s.Volume);

            var adminConsignmentSubmissions = new RtfoAdminConsignmentSubmissions(adminConsignmentItems
                , adminConsignmentTotalCredits
                , adminConsignmentTotalObligation
                , adminConsignmentTotalVolumeSupplied);

            //Volumes
            var volumeTotalCredits = volumeItems.Sum(s => s.Credits);
            var volumeTotalObligation = volumeItems.Sum(s => s.Obligation);
            var volumeTotalVolumeSupplied = volumeItems.Sum(s => s.Volume);

            var volumeSubmissions = new RtfoVolumeSubmissions(volumeItems
                , volumeTotalCredits
                , volumeTotalObligation
                , volumeTotalVolumeSupplied);

            //Fossil Gas
            var fossilGasTotalCredits = fossilGasItems.Sum(s => s.Credits);
            var fossilGasTotalObligation = fossilGasItems.Sum(s => s.Obligation);
            var fossilGasTotalVolumeSupplied = fossilGasItems.Sum(s => s.Volume);

            var fossilGasSubmissions = new FossilGasSubmissions(fossilGasItems, fossilGasTotalCredits, fossilGasTotalObligation, fossilGasTotalVolumeSupplied);

            //Liquid and Gas
            var liquidGasSubmissions = new LiquidGasSubmissions(adminConsignmentSubmissions
                , volumeSubmissions
                , fossilGasSubmissions
                , calculationResult.VolumeCalculation.ObligationPeriodLowVolumeSupplierDeduction
                , calculationResult.VolumeCalculation.TotalVolumeSupplied
                , calculationResult.VolumeCalculation.TotalVolumeAboveGhgTarget
                , calculationResult.VolumeCalculation.TotalVolumeCredits
                , calculationResult.VolumeCalculation.TotalVolumeObligationWithObligationPeriodAdjustments
                , calculationResult.VolumeCalculation.DeMinimisLitres
                , calculationResult.VolumeCalculation.NoDeMinimisLitres);

            //Electricity
            var electricityItems = new List<ElectricitySubmission>();
            foreach (var calculationItem in calculationResult.EnergyCalculation.Items)
            {
                var applicationItem = applicationItems.Single(i => i.ApplicationItemId == int.Parse(calculationItem.Reference));

                var application = applications.Single(a => a.ApplicationId == applicationItem.ApplicationId);

                electricityItems.Add(new ElectricitySubmission(application.ApplicationId
                    , application.SubmittedDate
                    , applicationItem.ApplicationItemId
                    , applicationItem.AmountSupplied.Value
                    , calculationItem.GhgIntensity
                    , calculationItem.DifferenceToTarget
                    , calculationItem.EnergySupplied
                    , calculationItem.Credits
                    , calculationItem.Obligation));
            }

            var electricityTotalCredits = electricityItems.Sum(s => s.Credits);
            var electricityTotalObligation = electricityItems.Sum(s => s.Obligation);
            var electricityTotalAmountSupplied = electricityItems.Sum(s => s.AmountSupplied);

            var electricitySubmissions = new ElectricitySubmissions(electricityItems
                , electricityTotalCredits
                , electricityTotalObligation
                , electricityTotalAmountSupplied);

            //UER
            var uerItems = new List<UpstreamEmissionsReductionSubmission>();
            foreach (var calculationItem in calculationResult.EmissionsReductionCalculation.Items)
            {
                var applicationItem = applicationItems.Single(i => i.ApplicationItemId == int.Parse(calculationItem.Reference));

                var application = applications.Single(a => a.ApplicationId == applicationItem.ApplicationId);

                uerItems.Add(new UpstreamEmissionsReductionSubmission(application.ApplicationId
                    , application.SubmittedDate
                    , calculationItem.Reference
                    , calculationItem.Credits
                    , calculationItem.InternalReference));
            }

            var totalUerCredits = uerItems.Sum(s => s.Amount);
            var uerSubmissions = new UpstreamEmissionsReductionSubmissions(uerItems, totalUerCredits);

            var submissions = new Submissions(liquidGasSubmissions
                , electricitySubmissions
                , uerSubmissions
                , calculationResult.TotalCreditsWithObligationPeriodAdjustments
                , calculationResult.TotalObligationWithObligationPeriodAdjustments);

            return submissions;
        }

        /// <summary>
        /// Translates the ledger result into a trading result
        /// </summary>
        /// <returns>Returns trading data</returns>
        private async Task<Trading> GetTrading(IEnumerable<TransactionSummary> transactions, string jwtToken)
        {
            var creditEvents = new List<CreditEvent>();
            foreach (var transaction in transactions)
            {
                string supplierName = null;
                if(transaction.TradingOrganisationId.HasValue)
                {
                    var getSupplierName = await this.OrganisationsService.GetOrganisation(transaction.TradingOrganisationId.Value, jwtToken);
                    supplierName = getSupplierName.Result.Name;
                }

                creditEvents.Add(new CreditEvent(transaction.TransactionId
                    , transaction.CreatedDate
                    , transaction.TransactionItemTypeId
                    , transaction.Description
                    , transaction.TradingOrganisationId
                    , supplierName
                    , transaction.Value
                    , transaction.AdditionalReference
                    ));
            }

            var balance = creditEvents.Sum(e => e.Credits);
            var trading = new Trading(creditEvents, balance);

            return trading;
        }

        #endregion Private Methods
    }
}
