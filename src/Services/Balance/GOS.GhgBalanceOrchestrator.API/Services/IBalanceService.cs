﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Administration;
using System;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Services
{
    /// <summary>
    /// Service definition to aggregate Balance details and persist them
    /// </summary>
    public interface IBalanceService
    {
        /// <summary>
        /// Creates a new Collection Administration record, used to track the progress of a bulk update operation
        /// </summary>
        /// <returns>A new CollectionAdministration object</returns>
        Task<CollectionAdministration> CreateCollectionAdministration();

        /// <summary>
        /// Updates all GHG Balances for all companies and all GHG Obligation Periods
        /// </summary>
        /// <param name="collectionAdministration">A Collection Administration record used to identify the bulk update process to enable reporting on progress</param>
        Task UpdateAllGhgBalances(CollectionAdministration collectionAdministration);

        /// <summary>
        ///  Updates the GHG Balance for an organisation within an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation to populate the balance for</param>
        /// <param name="obligationPeriodId">The ID of the obligation period to populate the balance for</param>
        /// <returns>Returns a boolean value indicating success</returns>
        Task<bool> UpdateGhgBalance(int organisationId, int obligationPeriodId);

        /// <summary>
        /// Gets status details for a bulk update by the unique bulk update ID the was returned by the UpdateCollections method
        /// </summary>
        /// <param name="bulkUpdateUniqueId">The unique ID of the bulk update operation to enable tracking of progress</param>
        /// <returns>Returns the collection update status information, if it exists</returns>
        Task<CollectionAdministration> GetBulkUpdateStatus(Guid bulkUpdateUniqueId);

        /// <summary>
        /// Deletes any collections that have been created as part of a bulk update, but have expired
        /// </summary>
        /// <returns>Returns the number of collections that were purged</returns>
        Task<int> PurgeExpiredCollections();
    }
}
