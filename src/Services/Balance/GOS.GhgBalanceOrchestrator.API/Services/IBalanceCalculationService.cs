﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Data.Balance;
using DfT.GOS.GhgCalculations.Orchestration;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.Organisations.API.Client.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using DfT.GOS.RtfoApplications.API.Client.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Services
{
    /// <summary>
    /// Service definition for calculating the an organisation's balance for an obligation period
    /// </summary>
    public interface IBalanceCalculationService
    {
        /// <summary>
        /// Calculates the GHG Balance for a organisation within an obligation period
        /// </summary>
        /// <param name="obligationPeriod">The obligation period</param>
        /// <param name="organisation">The organisation details</param>
        /// <param name="adminConsignments">A list of Admin Consignments for the company / obligation period</param>
        /// <param name="volumes">A list of Volumes for the company / obligation period</param>
        /// <param name="fuelTypes">A list of fuel types</param>
        /// <param name="applicationsCalculationOrchestrator">The Orchestrator to use to orchestrate application calculations</param>
        /// <param name="jwtToken">JWT Token for calling remote services</param>
        /// <returns>Returns the constructed GHG Balance</returns>
        Task<OrganisationBalance> CalculateGhgBalance(ObligationPeriod obligationPeriod
            , Organisation organisation
            , IList<FuelType> fuelTypes
            , IEnumerable<AdminConsignment> adminConsignments
            , IEnumerable<NonRenewableVolume> volumes
            , ApplicationsCalculationOrchestrator applicationsCalculationOrchestrator
            , string jwtToken);
    }
}
