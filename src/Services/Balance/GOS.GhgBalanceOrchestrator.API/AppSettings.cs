﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.HealthChecks.Mongo;
using DfT.GOS.Messaging.Models;

namespace DfT.GOS.GhgBalanceOrchestrator.API
{
    /// <summary>
    /// Application settings for the API
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// The Identifier for the service, used to connect to remote revices via JWT Token
        /// </summary>
        public string SERVICE_IDENTIFIER { get; set; }

        /// <summary>
        /// The URL for the remote GHG Fuels Service
        /// </summary>
        public string GhgFuelsServiceApiUrl { get; set; }

        /// <summary>
        /// The URL for the remote Reporting Periods Service
        /// </summary>
        public string ReportingPeriodsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the organisations service API URL
        /// </summary>
        public string OrganisationsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the applications service API URL
        /// </summary>
        public string ApplicationsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Calculations service API URL
        /// </summary>
        public string CalculationsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Identity service API URL
        /// </summary>
        public string IdentityServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Admin Consignments service API URL
        /// </summary>
        public string AdminConsignmentsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Volumes service API URL
        /// </summary>
        public string VolumesServiceApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Ledger service API URL
        /// </summary>
        public string GhgLedgerServiceApiUrl { get; set; }

        /// <summary>
        /// HTTP Client setting for resilliant HTTP communication
        /// </summary>
        public HttpClientAppSettings HttpClient { get; set; }

        /// <summary>
        /// Settings for the RabbitMQ messaging queue
        /// </summary>
        public RabbitMessagingClientSettings MessagingClient {get;set;}

        /// <summary>
        /// The number of minutes a bulk update can take before timing out
        /// </summary>
        public int BulkUpdateTimeoutMinutes { get; set; }

        /// <summary>
        /// The number of minutes between calls to purge orphaned temporary collection in mongo
        /// </summary>
        public int BalanceCollectionAdministrationBackgroundServiceDelayMinutes { get; set; }

        /// <summary>
        /// Configuration for Mongo Health Checks
        /// </summary>
        public MongoHealthCheckConfiguration MongoHealthCheck { get; set; }
    }
}
