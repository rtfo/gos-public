﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalanceOrchestrator.API.Exceptions;
using DfT.GOS.GhgBalanceOrchestrator.API.Services;
using DfT.GOS.Security.Claims;
using DfT.GOS.Web.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GOS.GhgBalanceOrchestrator.API.Controllers
{
    /// <summary>
    /// Controller for Administration Tasks for the GHG Balance Orchestration Service
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BalanceAdministrationController : ControllerBase
    {
        #region Properties

        private IBackgroundTaskQueue Queue { get; set; }
        private IBalanceService BalanceService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        public BalanceAdministrationController(IBackgroundTaskQueue queue
            , IBalanceService balanceService)
        {
            this.Queue = queue;
            this.BalanceService = balanceService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Updates all collections in the GHG Balance View (Organisation Balances).
        /// 
        /// Returns the unique ID of the bulk update operation to enable tracking of progress.
        /// </summary>
        /// <returns>Returns the unique ID of the bulk update operation to enable tracking of progress</returns>
        [HttpPost("UpdateCollections")]
        [Authorize(Roles = Roles.Administrator)]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status202Accepted)]
        public async Task<IActionResult> UpdateCollections()
        {
            //Create a record to track the progress of the bulk update operation
            var collectionAdmin = await this.BalanceService.CreateCollectionAdministration();

            //Updating the GHG balance is a long running operation, queue it to be run as a background task.
            this.Queue.QueueBackgroundWorkItem(async token =>
            {
                await this.BalanceService.UpdateAllGhgBalances(collectionAdmin);
            });

            return Accepted(collectionAdmin.Id);
        }

        /// <summary>
        /// Updates all collections in the GHG Balance View (Organisation Balances)
        /// </summary>
        /// <returns>Indicates whether the update was successful</returns>
        [HttpPost("UpdateCollections/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> UpdateCollection(int organisationId, int obligationPeriodId)
        {
            try
            {
                var result = await this.BalanceService.UpdateGhgBalance(organisationId, obligationPeriodId);
                if (!result)
                {
                    throw new BalanceOrchestrationException(string.Format("An error occurred whilst attempting to update the Balance for Organisation {0}, Obligation period {1}"
                        , organisationId, obligationPeriodId));
                }
                return Ok();
            }
            catch(ArgumentException ex)
            {
                return BadRequest("An invalid " + ex.ParamName + " parameter was passed.");
            }
        }

        /// <summary>
        /// Gets status details for a bulk update by the unique bulk update ID the was returned by the UpdateCollections method
        /// </summary>
        /// <param name="bulkUpdateUniqueId">The unique ID of the bulk update operation to enable tracking of progress</param>
        /// <returns>Returns the collection update status information, if it exists</returns>
        [HttpGet("UpdateCollections/Status/{bulkUpdateUniqueId}")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> GetBulkUpdateStatus(Guid bulkUpdateUniqueId)
        {
            var result = await this.BalanceService.GetBulkUpdateStatus(bulkUpdateUniqueId);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        #endregion Methods
    }
}