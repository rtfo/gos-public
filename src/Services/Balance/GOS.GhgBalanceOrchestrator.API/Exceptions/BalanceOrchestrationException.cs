﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Exceptions
{
    /// <summary>
    /// An exception indicating an issue with Balance Orchestration
    /// </summary>
    public class BalanceOrchestrationException : Exception
    {
        #region Constructors

        /// <summary>
        /// Default constructor for BalanceOrchestrationException
        /// </summary>
        public BalanceOrchestrationException()
            :base()
        {
        }

        /// <summary>
        /// Creates a BalanceOrchestrationException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public BalanceOrchestrationException(string message)
            :base(message)
        {
        }

        /// <summary>
        /// Creates a BalanceOrchestrationException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="inner">The inner exception</param>
        public BalanceOrchestrationException(string message, Exception inner)
            : base(message, inner)
        {
        }

        #endregion Constructors
    }
}
