﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Exceptions
{
    /// <summary>
    /// An exception indicating an authentication issue with Balance Orchestration
    /// </summary>
    public class BalanceOrchestrationAuthenticationException : Exception
    {
        #region Constructors

        /// <summary>
        /// Default constructor for BalanceOrchestrationAuthenticationException
        /// </summary>
        public BalanceOrchestrationAuthenticationException()
            :base()
        {
        }

        /// <summary>
        /// Creates a BalanceOrchestrationAuthenticationException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public BalanceOrchestrationAuthenticationException(string message)
            :base(message)
        {
        }

        /// <summary>
        /// Creates a BalanceOrchestrationAuthenticationException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="inner">The inner exception</param>
        public BalanceOrchestrationAuthenticationException(string message, Exception inner)
            : base(message, inner)
        {
        }

        #endregion Constructors
    }
}
