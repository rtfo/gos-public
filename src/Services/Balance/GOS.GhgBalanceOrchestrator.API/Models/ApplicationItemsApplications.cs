﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using System.Collections.Generic;

namespace DfT.GOS.GhgBalanceOrchestrator.API.Models
{
    /// <summary>
    /// Wrapper class for getting application item details and application details
    /// </summary>
    public class ApplicationItemsApplications
    {
        #region Properties

        /// <summary>
        /// Returns a list of Applications
        /// </summary>
        public IList<ApplicationSummary> Applications { get; private set; }

        /// <summary>
        /// Returns a list Application Items
        /// </summary>
        public IList<ApplicationItemSummary> ApplicationItems { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Construtor taking all variables
        /// </summary>
        /// <param name="applications">The Applications</param>
        /// <param name="applicationItems">The Application Items</param>
        public ApplicationItemsApplications(IList<ApplicationSummary> applications
            , IList<ApplicationItemSummary> applicationItems)
        {
            this.Applications = applications;
            this.ApplicationItems = applicationItems;
        }

        #endregion Constructors
    }
}
