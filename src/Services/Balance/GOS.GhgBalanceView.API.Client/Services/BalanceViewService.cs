﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Net.Http;
using System.Threading.Tasks;
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Web.Models;

namespace DfT.GOS.GhgBalanceView.API.Client.Services
{
    /// <summary>
    ///Reads GHG Balance information from the remote service
    /// </summary>
    public class BalanceViewService : HttpServiceClient, IBalanceViewService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">HttpClient to use when making requests</param>
        public BalanceViewService(HttpClient httpClient)
            : base(httpClient)
        {
        }

        #endregion Constructors

        #region IBalanceViewService Implementation

        /// <summary>
        /// Retrieves a high level balance summary for an organisation within an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the balance summary for the company/obligation period</returns>
        async Task<HttpObjectResponse<BalanceSummary>> IBalanceViewService.GetSummary(int organisationId, int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Summary";
            return await this.HttpGetAsync<BalanceSummary>(url, jwtToken);
        }

        /// <summary>
        /// Retrieves a high level balance summary for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the overall balance summary for the obligation period</returns>
        async Task<HttpObjectResponse<TotalBalanceSummary>> IBalanceViewService.GetSummary(int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/Balance/ObligationPeriod/{obligationPeriodId}/Summary";
            return await this.HttpGetAsync<TotalBalanceSummary>(url, jwtToken);
        }

        /// <summary>
        /// Retrieves a high level balance summary for an organisation within an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the submission balance summary for the company/obligation period</returns>
        async Task<HttpObjectResponse<SubmissionBalanceSummary>> IBalanceViewService.GetSubmissions(int organisationId, int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Submissions";
            return await this.HttpGetAsync<SubmissionBalanceSummary>(url, jwtToken);
        }

        /// <summary>
        /// Retrieves the liquid and gas summary for an organistion within an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the liquid and gas balance summary for the company/obligation period</returns>
        async Task<HttpObjectResponse<LiquidGasBalanceSummary>> IBalanceViewService.GetLiquidGasSummary(int organisationId, int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Submissions/LiquidGasSummary";
            return await this.HttpGetAsync<LiquidGasBalanceSummary>(url, jwtToken);
        }

        /// <summary>
        /// Retrieves the other volumes via rtfo summary for an organistion within an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the other volumes via rtfo summary for the company/obligation period</returns>
        async Task<HttpObjectResponse<PagedResult<OtherVolumesSubmission>>> IBalanceViewService.GetOtherRtfoVolumes(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Submissions/OtherRtfoVolumes?page={page}&pageSize={resultsPerPage}";
            return await this.HttpGetAsync<PagedResult<OtherVolumesSubmission>>(url, jwtToken);
        }

        /// <summary>
        /// Gets the Trading details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the trading summary for the company/obligation period</returns>
        async Task<HttpObjectResponse<TradingSummary>> IBalanceViewService.GetTrading(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Trading?pageNumber={page}&pageSize={resultsPerPage}";
            return await this.HttpGetAsync<TradingSummary>(url, jwtToken);
        }

        /// <summary>
        /// Gets the Rtfo admin consignment details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Rtfo admin consignment details for the company/obligation period</returns>
        async Task<HttpObjectResponse<PagedResult<RtfoAdminConsignmentSubmission>>> IBalanceViewService.GetRtfoAdminConsignment(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/RftoAdminConsignment?page={page}&pageSize={resultsPerPage}";
            return await this.HttpGetAsync<PagedResult<RtfoAdminConsignmentSubmission>>(url, jwtToken);
        }

        /// <summary>
        /// Gets the Fossil fuel submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Fossil Fuel details for the company/obligation period</returns>
        async Task<HttpObjectResponse<PagedResult<FossilGasSubmission>>> IBalanceViewService.GetFossilFuelSubmissions(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/FossilFuelSubmissions?page={page}&pageSize={resultsPerPage}";
            return await this.HttpGetAsync<PagedResult<FossilGasSubmission>>(url, jwtToken);
        }

        /// <summary>
        /// Gets the Electricity submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Electricity for the company/obligation period</returns>
        async Task<HttpObjectResponse<PagedResult<ElectricitySubmission>>> IBalanceViewService.GetElectricitySubmissions(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/ElectricitySubmissions?page={page}&pageSize={resultsPerPage}";
            return await this.HttpGetAsync<PagedResult<ElectricitySubmission>>(url, jwtToken);
        }

        /// <summary>
        /// Gets the Upstream Emission Reduction details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Upstream Emission Reduction summary for the company/obligation period</returns>
        async Task<HttpObjectResponse<UpstreamEmissionsReductionsSummary>> IBalanceViewService.GetUpstreamEmissionReductionSubmissions(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/UpstreamEmissionReductionSubmissions?page={page}&pageSize={resultsPerPage}";
            return await this.HttpGetAsync<UpstreamEmissionsReductionsSummary>(url, jwtToken);
        }

        /// <summary>
        /// Gets the submissions to export as a CSV using the organisationId and obligationPeriodId
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns a String/CSV containing the submissions to export a the organisation/obligation period</returns>
        async Task<HttpObjectResponse<byte[]>> IBalanceViewService.GenerateExportForOrganisation(int organisationId, int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/Balance/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/GenerateExportForOrganisation";
            return await this.HttpGetAsync<byte[]>(url, jwtToken);
        }

        /// <summary>
        /// Gets the submissions to export as a CSV using the obligationPeriodId
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns a String/CSV containing the submissions to export for the obligation period</returns>
        async Task<HttpObjectResponse<byte[]>> IBalanceViewService.GenerateExportForAllOrganisations(int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/Balance/ObligationPeriod/{obligationPeriodId}/GenerateExportForAllOrganisations";
            return await this.HttpGetAsync<byte[]>(url, jwtToken);
        }

        /// <summary>
        /// Gets the Credit Transfer summaries for an organisation for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="pageNumber">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="searchTerm">The search term to filter results by</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Electricity for the company/obligation period</returns>
        async Task<HttpObjectResponse<PagedResult<CreditTransferSummary>>> IBalanceViewService.GetCreditTransfers(int obligationPeriodId, int pageNumber, int resultsPerPage, string searchTerm, string jwtToken)
        {
            var url = $"/api/v1/Balance/ObligationPeriod/{obligationPeriodId}/CreditTransfers?pageNumber={pageNumber}&pageSize={resultsPerPage}&searchTerm={searchTerm}";
            return await this.HttpGetAsync<PagedResult<CreditTransferSummary>>(url, jwtToken);
        }

        /// <summary>
        /// Gets a CSV of Credit Transfer submissions details for an for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="searchTerm">The search term to filter results by</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Electricity for the company/obligation period</returns>
        async Task<HttpObjectResponse<byte[]>> IBalanceViewService.GetCreditTransfersCsv(int obligationPeriodId, string searchTerm, string jwtToken)
        {
            var url = $"/api/v1/Balance/ObligationPeriod/{obligationPeriodId}/CreditTransfers/Csv?searchTerm={searchTerm}";
            return await this.HttpGetAsync<byte[]>(url, jwtToken);
        }

        #endregion IBalanceViewService Implementation
    }
}