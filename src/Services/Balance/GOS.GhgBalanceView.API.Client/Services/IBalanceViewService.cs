﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Http;
using DfT.GOS.Web.Models;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceView.API.Client.Services
{
    /// <summary>
    /// Definition for reading GHG Balance information from the remote service
    /// </summary>
    public interface IBalanceViewService
    {
        /// <summary>
        /// Retrieves a high level balance summary for an organisation within an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the balance summary for the company/obligation period</returns>
        Task<HttpObjectResponse<BalanceSummary>> GetSummary(int organisationId, int obligationPeriodId, string jwtToken);

        /// <summary>
        /// Retrieves a high level balance summary for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the overall balance summary for the obligation period</returns>
        Task<HttpObjectResponse<TotalBalanceSummary>> GetSummary(int obligationPeriodId, string jwtToken);

        /// <summary>
        /// Gets the Submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the submission balance summary for the company/obligation period</returns>
        Task<HttpObjectResponse<SubmissionBalanceSummary>> GetSubmissions(int organisationId, int obligationPeriodId, string jwtToken);

        /// <summary>
        /// Gets the Trading details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the trading summary for the company/obligation period</returns>
        Task<HttpObjectResponse<TradingSummary>> GetTrading(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken);

        /// <summary>
        /// Gets the liquid and gas details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the liquid and gas balance summary for the company/obligation period</returns>
        Task<HttpObjectResponse<LiquidGasBalanceSummary>> GetLiquidGasSummary(int organisationId, int obligationPeriodId, string jwtToken);

        /// <summary>
        /// Gets the Rtfo admin consignment details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Rtfo admin consignment details for the company/obligation period</returns>
        Task<HttpObjectResponse<PagedResult<RtfoAdminConsignmentSubmission>>> GetRtfoAdminConsignment(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken);

        /// <summary>
        /// Gets the other volumes via RTFO details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the other volumes via RTFO details for the company/obligation period</returns>
        Task<HttpObjectResponse<PagedResult<OtherVolumesSubmission>>> GetOtherRtfoVolumes(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken);

        /// <summary>
        /// Gets the Fossil fuel submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Fossil fuel details for the organisation in the obligation period</returns>
        Task<HttpObjectResponse<PagedResult<FossilGasSubmission>>> GetFossilFuelSubmissions(int organisationId, int obligationPeriodId
            , int pageNumber, int pageSize, string jwtToken);

        /// <summary>
        /// Gets the Electricity submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Electricity submission details for the organisation in the obligation period</returns>
        Task<HttpObjectResponse<PagedResult<ElectricitySubmission>>> GetElectricitySubmissions(int organisationId, int obligationPeriodId
            , int pageNumber, int pageSize, string jwtToken);

        /// <summary>
        /// Gets the Upstream Emission Reduction details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Upstream Emission Reduction summary for the company/obligation period</returns>
        Task<HttpObjectResponse<UpstreamEmissionsReductionsSummary>> GetUpstreamEmissionReductionSubmissions(int organisationId, int obligationPeriodId, int page, int resultsPerPage, string jwtToken);

        /// <summary>
        /// Gets the submissions to export as a CSV using the organisationId and obligationPeriodId
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns a String/CSV containing the submissions to export a the organisation/obligation period</returns>
        Task<HttpObjectResponse<byte[]>> GenerateExportForOrganisation(int organisationId, int obligationPeriodId, string jwtToken);

        /// <summary>
        /// Gets the submissions to export as a CSV using the the obligationPeriodId
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns a String/CSV containing the submissions to export for the obligation period</returns>
        Task<HttpObjectResponse<byte[]>> GenerateExportForAllOrganisations(int obligationPeriodId, string jwtToken);

        /// <summary>
        /// Gets the Credit Transfer submissions details for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="resultsPerPage">The results per page to display</param>
        /// <param name="searchTerm">The search term to filter results by</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Electricity for the company/obligation period</returns>
        Task<HttpObjectResponse<PagedResult<CreditTransferSummary>>> GetCreditTransfers(int obligationPeriodId, int page, int resultsPerPage, string searchTerm, string jwtToken);

        /// <summary>
        /// Gets a CSV of Credit Transfer submissions details for an for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="searchTerm">The search term to filter results by</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the Electricity for the company/obligation period</returns>
        Task<HttpObjectResponse<byte[]>> GetCreditTransfersCsv(int obligationPeriodId, string searchTerm, string jwtToken);
    }
}
