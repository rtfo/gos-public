﻿﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.GhgBalance.Common.Repositories;
using DfT.GOS.GhgBalanceView.API.Models;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Web.Models;
using dataElectricitySubmission = DfT.GOS.GhgBalance.Common.Data.Balance.ElectricitySubmission;
using OrganisationBalance = DfT.GOS.GhgBalance.Common.Data.Balance.OrganisationBalance;

namespace DfT.GOS.GhgBalanceView.API.Services
{
    /// <summary>
    /// Service responsible for reading from balance details from the balance repository
    /// </summary>
    public class BalanceViewService : IBalanceViewService
    {
        #region Properties

        private IBalanceRepository BalanceRepository { get; set; }

        private IMapper Mapper { get; set; }

        private const int defaultPageSize = 10;
        private const int firstPage = 1;

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="balanceRepository">The GHG Balance repository</param>
        /// <param name="mapper">Maper to map the Model from DTO</param>
        public BalanceViewService(IBalanceRepository balanceRepository, IMapper mapper)
        {
            this.BalanceRepository = balanceRepository;
            this.Mapper = mapper;
        }

        #endregion Constructors

        #region IBalanceService Implementation

        /// <summary>
        /// Gets a balance summary for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Organisation ID</param>
        /// <param name="obligationPeriodId">The Obligation Period ID</param>
        /// <returns>Returns the BalanceSummary for the organisation and obligation period</returns>
        async Task<BalanceSummary> IBalanceViewService.GetSummary(int organisationId, int obligationPeriodId)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);
            if (balance == null)
            {
                return null;
            }
            else
            {
                return new BalanceSummary(balance.Submissions.TotalCredits
                    , balance.Submissions.TotalObligation
                    , balance.Trading.Balance
                    , balance.TotalCredits
                    , balance.TotalObligation
                    , balance.NetBalance
                    , balance.BuyOutCost
                    , balance.BuyOutPer_tCO2e);
            }
        }

        /// <summary>
        /// Gets the total balance summary for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>returns a single balance summary for all organisations</returns>
        async Task<TotalBalanceSummary> IBalanceViewService.GetSummary(int obligationPeriodId)
        {
            return await this.BalanceRepository.GetTotal(obligationPeriodId);
        }

        /// <summary>
        /// Gets the Trading details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Trading details for the organisation in the obligation period</returns>
        async Task<TradingSummary> IBalanceViewService.GetTrading(int organisationId, int obligationPeriodId, int pageNumber, int pageSize)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);
            if (balance == null)
            {
                return null;
            }
            else
            {
                var pagedResult = PagedResult.GetPagedResult(balance.Trading.CreditEvents.OrderByDescending(ce => ce.Date)
                                                            , pageSize
                                                            , pageNumber);

                return new TradingSummary(balance.Trading.Balance, pagedResult);
            }
        }

        /// <summary>
        /// Gets the Submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId"></param>
        /// <param name="obligationPeriodId"></param>
        /// <returns>Returns the SubmissionBalanceSummary for the organisation and obligation period</returns>
        async Task<SubmissionBalanceSummary> IBalanceViewService.GetSubmissions(int organisationId, int obligationPeriodId)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);

            if (balance == null || balance.Submissions == null)
            {
                return null;
            }
            var submissions = balance.Submissions;
            var liquidGasSubmission = default(LiquidGasSubmissionsSummary);
            if (submissions.LiquidGasSubmissions != null)
            {
                liquidGasSubmission = new LiquidGasSubmissionsSummary(
                    submissions.LiquidGasSubmissions.TotalCredits
                    , submissions.LiquidGasSubmissions.TotalObligation
                    , submissions.LiquidGasSubmissions.TotalVolumeOfFuelSupplied);
            }
            var elecSubmission = default(ElectricitySubmissionsSummary);
            if (submissions.ElectricitySubmissions != null)
            {
                elecSubmission = new ElectricitySubmissionsSummary(
                    submissions.ElectricitySubmissions.TotalCredits
                    , submissions.ElectricitySubmissions.TotalObligation
                    , submissions.ElectricitySubmissions.TotalAmountSupplied);
            }
            var upstreamEmissions = default(UpstreamEmissionsReductionSubmissionsSummary);
            if (submissions.UpstreamEmissionsReductionSubmissions != null)
            {
                upstreamEmissions = new UpstreamEmissionsReductionSubmissionsSummary(
                submissions.UpstreamEmissionsReductionSubmissions.TotalCredits);
            }
            return new SubmissionBalanceSummary(submissions.TotalCredits, submissions.TotalObligation
                , liquidGasSubmission, elecSubmission, upstreamEmissions);
        }

        /// <summary>
        /// Gets the liquid and gass summary details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId"></param>
        /// <param name="obligationPeriodId"></param>
        /// <returns>Returns the liquid and gass summary for the organisation and obligation period</returns>
        async Task<LiquidGasBalanceSummary> IBalanceViewService.GetLiquidGasSummary(int organisationId, int obligationPeriodId)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);

            if (balance == null || balance.Submissions == null)
            {
                return null;
            }
            var submissions = balance.Submissions.LiquidGasSubmissions;
            var rtfoAdminConsignmentsSubmission = default(RtfoAdminConsignmentsSubmissionsSummary);
            if (submissions.RtfoAdminConsignmentSubmissions != null)
            {
                rtfoAdminConsignmentsSubmission = new RtfoAdminConsignmentsSubmissionsSummary(
                    submissions.RtfoAdminConsignmentSubmissions.TotalCredits
                    , submissions.RtfoAdminConsignmentSubmissions.TotalObligation
                    , submissions.RtfoAdminConsignmentSubmissions.TotalVolumeSupplied);
            }
            var otherVolumesSubmissionsSummary = default(OtherVolumesSubmissionsSummary);
            if (submissions.RtfoVolumeSubmissions != null)
            {
                otherVolumesSubmissionsSummary = new OtherVolumesSubmissionsSummary(
                    submissions.RtfoVolumeSubmissions.TotalCredits
                    , submissions.RtfoVolumeSubmissions.TotalObligation
                    , submissions.RtfoVolumeSubmissions.TotalVolumeSupplied);
            }
            var fossilGasSubmissionsSummary = default(FossilGasSubmissionsSummary);
            if (submissions.FossilGasSubmissions != null)
            {
                fossilGasSubmissionsSummary = new FossilGasSubmissionsSummary(
                    submissions.FossilGasSubmissions.TotalCredits
                    , submissions.FossilGasSubmissions.TotalObligation
                    , submissions.FossilGasSubmissions.TotalVolumeSupplied);
            }
            return new LiquidGasBalanceSummary(submissions.TotalCredits
                , submissions.TotalObligation
                , submissions.TotalVolumeOfFuelSupplied
                , submissions.VolumeOfFuelAboveTheGhgTarget
                , submissions.LowVolumeSupplierDeduction
                , rtfoAdminConsignmentsSubmission
                , otherVolumesSubmissionsSummary
                , fossilGasSubmissionsSummary);
        }

        /// <summary>
        /// Gets the Rfto Admin Consignment details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Trading details for the organisation in the obligation period</returns>
        async Task<PagedResult<RtfoAdminConsignmentSubmission>> IBalanceViewService.GetRftoAdminConsignment(int organisationId, int obligationPeriodId, int pageNumber, int pageSize)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);
            if (balance == null)
            {
                return null;
            }
            else
            {
                var adminConsignments = this.Mapper.Map<IList<GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission>
                    , IList<RtfoAdminConsignmentSubmission>>(balance.Submissions.LiquidGasSubmissions.RtfoAdminConsignmentSubmissions.Submissions);

                var pagedResult = PagedResult.GetPagedResult(adminConsignments
                                                            , pageSize
                                                            , pageNumber);
                return pagedResult;
            }
        }

        /// <summary>
        /// Gets the other volumes via RTFO details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the other volumes via RTFO details for the organisation and obligation period</returns>
        async Task<PagedResult<OtherVolumesSubmission>> IBalanceViewService.GetOtherRtfoVolumes(int organisationId, int obligationPeriodId, int pageNumber, int pageSize)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);

            if (balance == null)
            {
                return null;
            }
            else
            {
                var otherRftoVolumes = this.Mapper.Map<IList<GhgBalance.Common.Data.Balance.RtfoVolumeSubmission>
                    , IList<OtherVolumesSubmission>>(balance.Submissions.LiquidGasSubmissions.RtfoVolumeSubmissions.Submissions);
                var pagedResult = PagedResult.GetPagedResult(otherRftoVolumes
                    , pageSize
                    , pageNumber);
                return pagedResult;
            }
        }

        /// <summary>
        /// Gets the Fossil fuel submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Trading details for the organisation in the obligation period</returns>
        async Task<PagedResult<FossilGasSubmission>> IBalanceViewService.GetFossilFuelSubmissions(int organisationId, int obligationPeriodId, int pageNumber, int pageSize)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);
            if (balance == null)
            {
                return null;
            }
            else
            {
                var adminConsignments = this.Mapper.Map<IList<GhgBalance.Common.Data.Balance.FossilGasSubmission>
                    , IList<FossilGasSubmission>>(balance.Submissions.LiquidGasSubmissions.FossilGasSubmissions.Submissions);

                var pagedResult = PagedResult.GetPagedResult(adminConsignments
                                                            , pageSize
                                                            , pageNumber);
                return pagedResult;
            }
        }

        /// <summary>
        /// Gets the Electricity submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Electricity submissions details for the organisation in the obligation period</returns>
        async Task<PagedResult<ElectricitySubmission>> IBalanceViewService.GetElectricitySubmissions(int organisationId, int obligationPeriodId, int pageNumber, int pageSize)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);
            if (balance == null)
            {
                return null;
            }
            else
            {
                var electricity = this.Mapper.Map<IList<dataElectricitySubmission>
                    , IList<ElectricitySubmission>>(balance.Submissions.ElectricitySubmissions.Submissions);

                var pagedResult = PagedResult.GetPagedResult(electricity
                                                            , pageSize
                                                            , pageNumber);
                return pagedResult;
            }
        }

        /// <summary>
        /// Gets the  Upstream emission reduction (UER) submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the  Upstream emission reduction (UER) submissions details for the organisation in the obligation period</returns>
        async Task<UpstreamEmissionsReductionsSummary> IBalanceViewService.GetUpstreamEmissionReductionSubmissions(int organisationId, int obligationPeriodId, int pageNumber, int pageSize)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);
            if (balance == null)
            {
                return null;
            }
            else
            {
                var pagedResult = PagedResult.GetPagedResult(balance.Submissions.UpstreamEmissionsReductionSubmissions.Submissions.OrderByDescending(ce => ce.Date)
                                                          , pageSize
                                                          , pageNumber);

                return new UpstreamEmissionsReductionsSummary(balance.Submissions.UpstreamEmissionsReductionSubmissions.TotalCredits, pagedResult);
            }
        }

        /// <summary>
        /// Gets the submissions to export as a CSV using the organisationId and obligationPeriodId
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>Returns a String/CSV containing the submissions mmary to export a the organisation/obligation period</returns>
        async Task<string> IBalanceViewService.GenerateExportForOrganisation(int organisationId, int obligationPeriodId)
        {
            var balance = await this.BalanceRepository.Get(organisationId, obligationPeriodId);
            if (balance == null)
            {
                return null;
            }
            else
            {
                var export = new StringBuilder(new SubmissionsExport().GetHeaderText() + "\r\n");
                export.Append(GenerateCSV(balance));
                return export.ToString();
            }
        }

        /// <summary>
        /// Gets the submissions to export as a CSV using obligationPeriodId
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>Returns a String/CSV containing the submissions summary to export for obligation period</returns>
        async Task<string> IBalanceViewService.GenerateExportForAllOrganisations(int obligationPeriodId)
        {
            var balance = await this.BalanceRepository.GetAll(obligationPeriodId);
            if (balance == null || !balance.Any())
            {
                return null;
            }
            else
            {
                var export = new StringBuilder(new SubmissionsExport().GetHeaderText() + "\r\n");
                foreach (var organisationBalance in balance)
                {
                    var result = GenerateCSV(organisationBalance);
                    export.Append(result);
                }
                return export.ToString();
            }
        }

        /// <summary>
        /// Gets the credit transfers by obligationPeriodId filtered by organisation name
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="searchTerm">The search term to filter by organisation name on</param>
        /// <param name="pageNumber">The  page number</param>
        /// <param name="pageSize">Length of page size</param>
        /// <returns>Returns the credit transfer details for all organisations for a given obligation period</returns>
        async Task<PagedResult<CreditTransferSummary>> IBalanceViewService.GetCreditTransfers(int obligationPeriodId, string searchTerm, int pageSize, int pageNumber)
        {
            var transfersPageNumber = pageNumber;
            var transfersPageSize = pageSize;
            if (transfersPageSize < 1)
            {
                transfersPageSize = defaultPageSize;
            }

            if (transfersPageNumber < 1)
            {
                transfersPageNumber = firstPage;
            }

            var creditTransferSummary = await this.BalanceRepository.GetCreditTransfers(obligationPeriodId, searchTerm, transfersPageNumber, transfersPageSize);

            if (creditTransferSummary == null)
            {
                return null;
            }
            else
            {
                return new PagedResult<CreditTransferSummary>
                {
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    Result = creditTransferSummary.Results,
                    TotalResults = creditTransferSummary.Count
                };
            }
        }

        /// <summary>
        /// Gets the credit transfers by obligationPeriodId filtered by organisation name
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="searchTerm">The search term to filter by organisation name on</param>
        /// <returns>Returns the credit transfer details for all organisations for a given obligation period</returns>
        async Task<IList<CreditTransferSummary>> IBalanceViewService.GetCreditTransfers(int obligationPeriodId, string searchTerm)
        {
            return await this.BalanceRepository.GetCreditTransfers(obligationPeriodId, searchTerm);
        }

        #region Private Methods

        /// <summary>
        /// Generates a CSV from the balance provided
        /// </summary>
        /// <param name="balance">Balance</param>
        /// <returns>Returns a String/CSV containing the submissions to export</returns>
        private static string GenerateCSV(OrganisationBalance balance)
        {
            var obligationPeriodYear = balance.ObligationPeriodEndDate.Value.Year;
            var organisationId = balance.OrganisationId;
            var organisationName = balance.OrganisationName;
            var export = new StringBuilder();
            foreach (var submission in balance.Submissions.LiquidGasSubmissions.RtfoAdminConsignmentSubmissions.Submissions)
            {
                var rtfoAdminConsignmentsSummary = new SubmissionsExport(submission.ReportingMonth
                    , "RTFO Admin Consignments"
                    , organisationName
                    , organisationId
                    , obligationPeriodYear
                    , null
                    , submission.AdminConsignmentId
                    , submission.InternalReference
                    , submission.FuelType
                    , submission.Feedstock
                    , submission.Volume
                    , submission.EnergyDensity
                    , submission.GhgIntensity
                    , submission.DifferenceToTarget
                    , submission.EnergySupplied
                    , submission.Credits
                    , submission.Obligation);
                export.Append(rtfoAdminConsignmentsSummary.ToString() + "\r\n");
            }
            foreach (var submission in balance.Submissions.LiquidGasSubmissions.RtfoVolumeSubmissions.Submissions)
            {
                var fossilFuelsSummary = new SubmissionsExport(submission.ReportingMonth
                    , "RTFO Fossil Fuels"
                    , organisationName
                    , organisationId
                    , obligationPeriodYear
                    , null
                    , null
                    , null
                    , submission.FuelType
                    , null
                    , submission.Volume
                    , submission.EnergyDensity
                    , submission.GhgIntensity
                    , submission.DifferenceToTarget
                    , submission.EnergySupplied
                    , submission.Credits
                    , submission.Obligation);
                export.Append(fossilFuelsSummary.ToString() + "\r\n");
            }
            foreach (var submission in balance.Submissions.LiquidGasSubmissions.FossilGasSubmissions.Submissions)
            {
                var fossilGasSummary = new SubmissionsExport(submission.Date
                    , "Fossil Gas"
                    , organisationName
                    , organisationId
                    , obligationPeriodYear
                    , submission.ApplicationId
                    , submission.Reference
                    , null
                    , submission.FuelType
                    , null
                    , submission.Volume
                    , submission.EnergyDensity
                    , submission.GhgIntensity
                    , submission.DifferenceToTarget
                    , submission.EnergySupplied
                    , submission.Credits
                    , submission.Obligation);
                export.Append(fossilGasSummary.ToString() + "\r\n");
            }
            foreach (var submission in balance.Submissions.ElectricitySubmissions.Submissions)
            {
                var electricitySummary = new SubmissionsExport(submission.Date
                    , "Electricity"
                    , organisationName
                    , organisationId
                    , obligationPeriodYear
                    , submission.ApplicationId
                    , submission.Reference
                    , null
                    , "Electricity"
                    , null
                    , submission.AmountSupplied
                    , null
                    , submission.GhgIntensity
                    , submission.DifferenceToTarget
                    , submission.EnergySupplied
                    , submission.Credits
                    , submission.Obligation);
                export.Append(electricitySummary.ToString() + "\r\n");
            }
            foreach (var submission in balance.Submissions.UpstreamEmissionsReductionSubmissions.Submissions)
            {
                int? reference = null;
                bool validReference = int.TryParse(submission.Reference, out int output);
                if (validReference)
                {
                    reference = output;
                }
                var uerSummary = new SubmissionsExport(submission.Date
                    , "UER"
                    , organisationName
                    , organisationId
                    , obligationPeriodYear
                    , submission.ApplicationId
                    , reference
                    , submission.InternalReference
                    , "UER"
                    , null
                    , submission.Amount
                    , null
                    , null
                    , null
                    , null
                    , submission.Amount
                    , 0);
                export.Append(uerSummary.ToString() + "\r\n");
            }
            return export.ToString();
        }

        #endregion Private Methods

        #endregion IBalanceService Implementation
    }
}