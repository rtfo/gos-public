﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.GhgBalanceView.Common.Models;
using DfT.GOS.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceView.API.Services
{
    /// <summary>
    /// Service definition for reading from balance details from the balance repository
    /// </summary>
    public interface IBalanceViewService
    {
        /// <summary>
        /// Gets a balance summary for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>returns a single balance summary, if it exists</returns>
        Task<BalanceSummary> GetSummary(int organisationId, int obligationPeriodId);

        /// <summary>
        /// Gets the total balance summary for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>returns a single balance summary for all organisations</returns>
        Task<TotalBalanceSummary> GetSummary(int obligationPeriodId);

        /// <summary>
        /// Gets the Submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>Returns the submission balance summary</returns>
        Task<SubmissionBalanceSummary> GetSubmissions(int organisationId, int obligationPeriodId);

        /// <summary>
        /// Gets the Trading details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Trading details for the organisation in the obligation period</returns>
        Task<TradingSummary> GetTrading(int organisationId, int obligationPeriodId, int pageNumber, int pageSize);

        /// <summary>
        /// Gets the liquid and gas details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>Returns the liquid and gas balance summary for the company/obligation period</returns>
        Task<LiquidGasBalanceSummary> GetLiquidGasSummary(int organisationId, int obligationPeriodId);

        /// <summary>
        /// Gets the Rfto Admin Consignment details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Trading details for the organisation in the obligation period</returns>
        Task<PagedResult<RtfoAdminConsignmentSubmission>> GetRftoAdminConsignment(int organisationId, int obligationPeriodId, int pageNumber, int pageSize);

        /// <summary>
        /// Gets the other volumes via RTFO details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The Id of the organisation</param>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the other volumes via RTFO details for the company/obligation period</returns>
        Task<PagedResult<OtherVolumesSubmission>> GetOtherRtfoVolumes(int organisationId, int obligationPeriodId, int pageNumber, int pageSize);

        /// <summary>
        /// Gets the Fossil fuel submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Fossil fuel details for the organisation in the obligation period</returns>
        Task<PagedResult<FossilGasSubmission>> GetFossilFuelSubmissions(int organisationId, int obligationPeriodId, int pageNumber, int pageSize);

        /// <summary>
        /// Gets the Electricity submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Electricity submissions details for the organisation in the obligation period</returns>
        Task<PagedResult<ElectricitySubmission>> GetElectricitySubmissions(int organisationId, int obligationPeriodId, int pageNumber, int pageSize);

        /// <summary>
        /// Gets the  Upstream emission reduction (UER) submissions details for an organisation for an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The number of the page of results to return</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <returns>Returns the Upstream emission reduction (UER) submissions details for the organisation in the obligation period</returns>
        Task<UpstreamEmissionsReductionsSummary> GetUpstreamEmissionReductionSubmissions(int organisationId, int obligationPeriodId, int pageNumber, int pageSize);

        /// <summary>
        /// Gets the submissions to export as a CSV using the organisationId and obligationPeriodId
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>Returns a String/CSV containing the submissions to export a the organisation/obligation period</returns>
        Task<string> GenerateExportForOrganisation(int organisationId, int obligationPeriodId);

        /// <summary>
        /// Gets the submissions to export as a CSV using the obligationPeriodId
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>Returns a String/CSV containing the submissions to export fro the obligation period</returns>
        Task<string> GenerateExportForAllOrganisations(int obligationPeriodId);

        /// <summary>
        /// Gets the summary of credit transfers for an obligationPeriodId with organisation name and paging
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="searchTerm">Search term to filter results by organisation name on</param>
        /// <param name="pageSize">The size of the page of results to return</param>
        /// <param name="page">The number of the page of results to return</param>
        /// <returns>Returns the Credits Transferred for a given obligation period</returns>
        Task<PagedResult<CreditTransferSummary>> GetCreditTransfers(int obligationPeriodId, string searchTerm, int pageSize, int page);

        /// <summary>
        /// Gets the summary of credit transfers for an obligationPeriodId with organisation name
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="searchTerm">Search term to filter results by organisation name on</param>
        /// <returns>Returns the Credits Transferred for a given obligation period</returns>
        Task<IList<CreditTransferSummary>> GetCreditTransfers(int obligationPeriodId, string searchTerm);
    }
}
