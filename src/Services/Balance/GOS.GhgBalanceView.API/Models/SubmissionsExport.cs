﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Helpers;
using System;

namespace DfT.GOS.GhgBalanceView.API.Models
{
    /// <summary>
    /// Represents summary to export when downloading
    /// </summary>
    public class SubmissionsExport
    {
        #region Properties

        /// <summary>
        /// The reporting month the volume was submitted for
        /// </summary>
        public DateTime Date { get; private set; }

        /// <summary>
        /// The type of submission
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        /// The Organisation Name
        /// </summary>
        public string OrganisationName { get; set; }

        /// <summary>
        /// The Organisation Id
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// The Obligation Period
        /// </summary>
        public int ObligationPeriod { get; set; }

        /// <summary>
        /// The Application Reference
        /// </summary>
        public Nullable<int> ApplicationReference { get; set; }

        /// <summary>
        /// The Application Item Reference
        /// </summary>
        public Nullable<int> Reference { get; set; }

        /// <summary>
        /// The Internal Reference
        /// </summary>
        public string InternalReference { get; set; }

        /// <summary>
        /// The Fuel Type description
        /// </summary>
        public string FuelType { get; set; }

        /// <summary>
        /// The Feedstock for the fuel
        /// </summary>
        public string Feedstock { get; set; }

        /// <summary>
        /// The amount of Upstream Emissions Reduction
        /// </summary>
        public Nullable<decimal> Amount { get; set; }

        /// <summary>
        /// The Energy Density of fuel (MJ / unit)
        /// </summary>
        public Nullable<decimal> EnergyDensity { get; set; }

        /// <summary>
        /// The GHG of the energy (gCO2 / MJ)
        /// </summary>
        public Nullable<decimal> GhgIntensity { get; set; }

        /// <summary>
        /// The difference to target CI (gCO2e / MJ)
        /// </summary>
        public Nullable<decimal> DifferenceToTarget { get; set; }

        /// <summary>
        /// The Energy Supplied in (MJ)
        /// </summary>
        public Nullable<decimal> EnergySupplied { get; set; }

        /// <summary>
        /// The number of credits produced (KgCO2e)
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// The obligation produced (KgCO2e)
        /// </summary>
        public decimal Obligation { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="date">The reporting month the volume was submitted for</param>
        /// <param name="type">The type of submission</param>
        /// <param name="organisationName">The Organisation Name</param>
        /// <param name="organisationId">The Organisation Id</param>
        /// <param name="obligationPeriod">The Obligation Period Id</param>
        /// <param name="applicationReference">The Application Reference</param>
        /// <param name="reference">The Application Item Reference</param>
        /// <param name="internalReference">The Internal Reference</param>
        /// <param name="fuelType">The Fuel Type description</param>
        /// <param name="feedstock">The Feedstock for the fuel</param>
        /// <param name="amount">The amount of Upstream Emissions Reduction</param>
        /// <param name="energyDensity">The Energy Density of fuel (MJ / unit)</param>
        /// <param name="ghgIntensity">The GHG of the energy (gCO2 / MJ)</param>
        /// <param name="differenceToTarget">The difference to target CI (gCO2e / MJ)</param>
        /// <param name="energySupplied">The Energy Supplied in (MJ)</param>
        /// <param name="credits">The number of credits produced (KgCO2e)</param>
        /// <param name="obligation">The obligation produced (KgCO2e)</param>
        public SubmissionsExport(DateTime date
            , string type
            , string organisationName
            , int organisationId
            , int obligationPeriod
            , int? applicationReference
            , int? reference
            , string internalReference
            , string fuelType
            , string feedstock
            , decimal? amount
            , decimal? energyDensity
            , decimal? ghgIntensity
            , decimal? differenceToTarget
            , decimal? energySupplied
            , decimal credits
            , decimal obligation)
        {
            this.Date = date;
            this.Type = type;
            this.OrganisationName = organisationName;
            this.OrganisationId = organisationId;
            this.ObligationPeriod = obligationPeriod;
            this.ApplicationReference = applicationReference;
            this.Reference = reference;
            this.InternalReference = internalReference;
            this.FuelType = fuelType;
            this.Feedstock = feedstock;
            this.Amount = amount;
            this.EnergyDensity = energyDensity;
            this.GhgIntensity = ghgIntensity;
            this.DifferenceToTarget = differenceToTarget;
            this.EnergySupplied = energySupplied;
            this.Credits = credits;
            this.Obligation = obligation;
        }

        /// <summary>
        /// Default constructor with no parameters
        /// </summary>
        public SubmissionsExport()
        {

        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Column headers for the submissions export
        /// </summary>
        public string GetHeaderText()
        {
            return string.Join(","
                , "Date"
                , "Type"
                , "Organisation Name"
                , "Organisation Id"
                , "Obligation Period"
                , "Application Reference"
                , "Sub Reference"
                , "Internal Reference"
                , "Fuel Type"
                , "Feedstock"
                , "Amount"
                , "Energy Density"
                , "Ghg Intensity"
                , "Difference To Target"
                , "Energy Supplied"
                , "Credits"
                , "Obligation");
        }

        /// <summary>
        /// Converting the values within the submissions into a complete string to be exported into a CSV
        /// </summary>
        public override string ToString()
        {
            var a = GhgIntensity.ToString();

            return string.Join(","
                , Date.ToString("dd/MM/yyyy")
                , Type
                , CsvHelper.ConvertToCsvCell(OrganisationName)
                , OrganisationId.ToString()
                , ObligationPeriod.ToString()
                , ApplicationReference.ToString()
                , Reference.ToString()
                , CsvHelper.ConvertToCsvCell(InternalReference)
                , CsvHelper.ConvertToCsvCell(FuelType)
                , CsvHelper.ConvertToCsvCell(Feedstock)
                , Amount.ToString()
                , EnergyDensity.ToString()
                , GhgIntensity.ToString()
                , DifferenceToTarget.ToString()
                , EnergySupplied.ToString()
                , Credits.ToString()
                , Obligation.ToString());
        }       

        #endregion Methods
    }
}
