﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using AutoMapper;
using DfT.GOS.GhgBalance.Common.Data;
using DfT.GOS.GhgBalance.Common.Repositories;
using DfT.GOS.GhgBalanceView.API.Services;
using DfT.GOS.HealthChecks.Common;
using DfT.GOS.HealthChecks.Common.Extensions;
using DfT.GOS.HealthChecks.Mongo;
using DfT.GOS.Security.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using dataElectricitySubmission = DfT.GOS.GhgBalance.Common.Data.Balance.ElectricitySubmission;
using dataRtfoAdminConsignmentSubmission = DfT.GOS.GhgBalance.Common.Data.Balance.RtfoAdminConsignmentSubmission;
using ElectricitySubmission = DfT.GOS.GhgBalanceView.Common.Models.ElectricitySubmission;
using OtherVolumesSubmission = DfT.GOS.GhgBalanceView.Common.Models.OtherVolumesSubmission;
using RtfoAdminConsignmentSubmission = DfT.GOS.GhgBalanceView.Common.Models.RtfoAdminConsignmentSubmission;
using RtfoVolumeSubmission = DfT.GOS.GhgBalance.Common.Data.Balance.RtfoVolumeSubmission;

namespace DfT.GOS.GhgBalanceView.API
{
    /// <summary>
    /// The startup file for the GHG Balance View API
    /// </summary>
    public class Startup
    {
        #region Constants

        /// <summary>
        /// The environment variable to parse for the connection string
        /// </summary>
        public const string EnvironmentVariable_ConnectionString = "CONNECTION_STRING";

        /// <summary>
        /// The environment variable to parse for the database name
        /// </summary>
        public const string EnvironmentVariable_DatabaseName = "DATABASE_NAME";

        /// <summary>
        /// The environment variable for the security JWT Token Audience used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenAudience = "JWT_TOKEN_AUDIENCE";

        /// <summary>
        /// The environment variable for the security JWT Token Issuer used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenIssuer = "JWT_TOKEN_ISSUER";

        /// <summary>
        /// The environment variable for the security JWT Token Key used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenKey = "JWT_TOKEN_KEY";

        /// <summary>
        /// The error message to throw when no connection string is supplied
        /// </summary>
        public const string ErrorMessage_ConnectionStringNotSupplied = "Unable to connect to the GHG Balance View datasource, the connection string was not specified. Missing variable: " + EnvironmentVariable_ConnectionString;

        /// <summary>
        /// The error message to throw when no connection string is supplied
        /// </summary>
        public const string ErrorMessage_DatabaseNameNotSupplied = "Unable to connect to the GHG Balance Orchestrator datasource, the database name was not specified. Missing variable: " + EnvironmentVariable_DatabaseName;

        /// <summary>
        /// The error message to throw when no JWT Token Audience environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenAudience = "Unable to get the JWT Token Audience from the enironmental variables.  Missing variable: " + EnvironmentVariable_JwtTokenAudience;

        /// <summary>
        /// The error message to throw when no JWT Token Issuer environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenIssuer = "Unable to get the JWT Token Issuer from the enironmental variables.  Missing variable: " + EnvironmentVariable_JwtTokenIssuer;

        /// <summary>
        /// The error message to throw when no JWT Token Key environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenKey = "Unable to get the JWT Token Key from the enironmental variables.  Missing variable: " + EnvironmentVariable_JwtTokenKey;

        #endregion Constants

        #region Properties
        private ILogger<Startup> Logger { get; set; }

        /// <summary>
        /// Gets the configuration settings
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="configuration">The configuration settings</param>
        /// <param name="logger">The logger</param>
        public Startup(IConfiguration configuration
            , ILogger<Startup> logger)
        {
            this.Logger = logger;
            this.Logger.LogInformation("Startup called for new instance of DfT.GOS.GhgBalanceView.API");
            this.Configuration = configuration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            this.ValidateEnvironmentVariables();
            var appSettings = Configuration.Get<AppSettings>();
            services.AddLogging();

            var connectionString = Configuration[EnvironmentVariable_ConnectionString];
            var databaseName = Configuration[EnvironmentVariable_DatabaseName];

            // - Resolve the mongo connection
            services.AddSingleton<IMongoClient>(c => new MongoClient(connectionString));
            services.AddTransient<IGhgBalanceContext>(s => new GhgBalanceContext(s.GetService<IMongoClient>(), databaseName));

            // - Resolve Repos
            services.AddScoped<IBalanceRepository, BalanceRepository>();

            //Resolve the services
            services.AddTransient<IBalanceViewService, BalanceViewService>();

            //JWT Security Authentication
            var jwtSettings = new JwtAuthenticationSettings(Configuration[EnvironmentVariable_JwtTokenIssuer]
                , Configuration[EnvironmentVariable_JwtTokenAudience]
                , Configuration[EnvironmentVariable_JwtTokenKey]);

            var jwtConfigurer = new JwtAuthenticationConfigurer(jwtSettings);
            jwtConfigurer.Configure(services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }));

            // Register the Swagger generator
            try
            {
                //Swagger required services
                services.AddSingleton<IApiDescriptionGroupCollectionProvider, ApiDescriptionGroupCollectionProvider>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "GHG Balance View API", Version = "v1", Description = "GHG Balance View API is used to aggregate GHG credit/obligation details." });
                    c.IncludeXmlComments(xmlPath);
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { In = ParameterLocation.Header, Description = "Please enter JWT with Bearer into field in the format 'Bearer [JWT token]'", Name = "Authorization", Type = SecuritySchemeType.ApiKey });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement() {
                        {
                            new OpenApiSecurityScheme {
                                Reference = new OpenApiReference {
                                    Id = "Bearer"
                                    , Type = ReferenceType.SecurityScheme
                                },
                                Scheme = "bearer",
                                Name = "security",
                                In = ParameterLocation.Header
                            }, new List<string>()
                        }
                    });
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Register Swagger generator because of the following exception: " + ex.ToString());
            }

            //Add Health Checks
            //Add the Mongo health check as a liveness check because, if it fails, we need to restart the container 
            //(this is due to a limitation in the Mongo API, which does not recover from a restart of the Mongo containers)
            //The RabbitMQ API does recover from restarts of the Rabbit containers hence we add it to the readiness check
            services.AddSingleton(o => new MongoHealthCheckOptions(databaseName, appSettings.MongoHealthCheck.TimoutMilliseconds));
            services.AddHealthChecks()
                .AddCheck<MongoHealthCheck>("mongo", tags: new[] { HealthCheckTypes.Liveness });

            services.AddControllers().AddNewtonsoftJson();

            services.AddAutoMapper(mapper =>
            {
                mapper.CreateMap<dataRtfoAdminConsignmentSubmission, RtfoAdminConsignmentSubmission>();
                mapper.CreateMap<RtfoVolumeSubmission, OtherVolumesSubmission>();
                mapper.CreateMap<dataElectricitySubmission, ElectricitySubmission>();
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            try
            {
                //Healthcheck
                app.UseGosHealthChecks();

                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "GHG Balance View V1");
                    c.RoutePrefix = string.Empty;
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Configure Swagger because of the following exception: " + ex.ToString());
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Validates that all expected environment variables have been supplied
        /// </summary>
        private void ValidateEnvironmentVariables()
        {
            //Connection String
            if (Configuration[EnvironmentVariable_ConnectionString] == null)
            {
                throw new NullReferenceException(ErrorMessage_ConnectionStringNotSupplied);
            }

            //Database Name
            if (Configuration[EnvironmentVariable_DatabaseName] == null)
            {
                throw new NullReferenceException(ErrorMessage_DatabaseNameNotSupplied);
            }

            //JWT Token Audience
            if (Configuration[EnvironmentVariable_JwtTokenAudience] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenAudience);
            }

            //JWT Token Issuer
            if (Configuration[EnvironmentVariable_JwtTokenIssuer] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenIssuer);
            }

            //JWT Token Key
            if (Configuration[EnvironmentVariable_JwtTokenKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenKey);
            }
        }

        #endregion Methods
    }
}
