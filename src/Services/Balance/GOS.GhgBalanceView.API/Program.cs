﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DfT.GOS.GhgBalanceView.API
{
    /// <summary>
    /// Contains entry point for the Balance View service
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entry point for the Balance View service
        /// </summary>
        /// <param name="args">Command line arguments</param>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Creates a Web Host Builder for the Balance View service
        /// </summary>
        /// <param name="args">Command line arguments</param>
        /// <returns>Web Host Builder for Balance View service</returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://*:80")
                .UseStartup<Startup>();
    }
}
