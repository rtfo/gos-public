﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgBalance.Common.Models;
using DfT.GOS.GhgBalanceView.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.GhgBalanceView.API.Controllers
{
    /// <summary>
    /// Controller for getting company balance information
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BalanceController : ControllerBase
    {
        #region Properties

        private IBalanceViewService BalanceService { get; set; }
        private ILogger<BalanceController> Logger { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        public BalanceController(IBalanceViewService balanceService
            , ILogger<BalanceController> logger)
        {
            this.BalanceService = balanceService;
            this.Logger = logger;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Returns a Balance Summary for an organisation / obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>returns a high level balance summary for the organisation in the obligation period</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Summary")]
        public async Task<IActionResult> GetSummary(int organisationId, int obligationPeriodId)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var summary = await this.BalanceService.GetSummary(organisationId, obligationPeriodId);
                if (summary == null)
                {
                    return NotFound("Balance Summary not found");
                }
                else
                {
                    return Ok(summary);
                }
            }
            else
            {
                result = Forbid();
            }

            return result;
        }

        /// <summary>
        /// Returns the total Balance Summary for all organisations in an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>returns a high level balance summary for all organisations in the obligation period</returns>
        [HttpGet("ObligationPeriod/{obligationPeriodId}/Summary")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> GetSummary(int obligationPeriodId)
        {
            var summary = await this.BalanceService.GetSummary(obligationPeriodId);
            if (summary == null)
            {
                this.Logger.LogWarning("No balance summary found for obligation period with id {id}", obligationPeriodId);
                return NotFound("Balance Summary not found");
            }
            else
            {
                return Ok(summary);
            }
        }


        /// <summary>
        /// Gets the trading credit events for an organisation within an obligation period
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageNumber">The page number of results to return</param>
        /// <param name="pageSize">The page size of results to return</param>
        /// <returns>returns a trading summary for the organisation in the obligation period</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Trading")]
        public async Task<IActionResult> GetTrading(int organisationId, int obligationPeriodId, [FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var tradingDetails = await this.BalanceService.GetTrading(organisationId, obligationPeriodId, pageNumber, pageSize);
                if (tradingDetails == null)
                {
                    result = NotFound("Trading details not found");
                }
                else
                {
                    result = Ok(tradingDetails);
                }
            }
            else
            {
                result = Forbid();
            }
            return result;
        }

        /// <summary>
        /// Gets the balance details for submissions
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>returns a submission balance summary for the organisation in the obligation period</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Submissions")]
        public async Task<IActionResult> GetSubmissions(int organisationId, int obligationPeriodId)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var submissions = await this.BalanceService.GetSubmissions(organisationId, obligationPeriodId);
                if (submissions == null)
                {
                    result = NotFound("Balance Submission details not found");
                }
                else
                {
                    result = Ok(submissions);
                }
            }
            else
            {
                result = Forbid();
            }
            return result;
        }

        /// <summary>
        /// Gets the balance details for liquid and gas submissions
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>returns a liquid and gas balance summary for the organisation in the obligation period</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Submissions/LiquidGasSummary")]
        public async Task<IActionResult> GetLiquidGasSummary(int organisationId, int obligationPeriodId)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var submissions = await this.BalanceService.GetLiquidGasSummary(organisationId, obligationPeriodId);
                if (submissions == null)
                {
                    result = NotFound("Liquid and gas balance submission details not found");
                }
                else
                {
                    result = Ok(submissions);
                }
            }
            else
            {
                result = Forbid();
            }
            return result;
        }

        /// <summary>
        /// Gets the rtfo admin consignment details
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="pageSize">The results per page to display</param>
        /// <returns>returns the rtfo admin consignment submissions for the organisation in the obligation period</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/RftoAdminConsignment")]
        public async Task<IActionResult> GetRftoAdminConsignment(int organisationId, int obligationPeriodId, [FromQuery]int pageSize, [FromQuery]int page = 1)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var submissions = await this.BalanceService.GetRftoAdminConsignment(organisationId, obligationPeriodId, page, pageSize);
                if (submissions == null)
                {
                    result = NotFound("Rtfo admin consignment submission details not found");
                }
                else
                {
                    result = Ok(submissions);
                }
            }
            else
            {
                result = Forbid();
            }
            return result;
        }

        /// <summary>
        /// Gets the fossil fuel submissions details
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="pageSize">The results per page to display</param>
        /// <returns>returns the fossil fuel submissions for the organisation in the obligation period</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/FossilFuelSubmissions")]
        public async Task<IActionResult> GetFossilFuelSubmissions(int organisationId, int obligationPeriodId, [FromQuery]int pageSize, [FromQuery]int page = 1)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var submissions = await this.BalanceService.GetFossilFuelSubmissions(organisationId, obligationPeriodId, page, pageSize);
                if (submissions == null)
                {
                    result = NotFound("Fossil fuel submission details not found");
                }
                else
                {
                    result = Ok(submissions);
                }
            }
            else
            {
                result = Forbid();
            }
            return result;
        }

        /// <summary>
        /// Gets the other volumes via RTFO details
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="page">The page number of results to return</param>
        /// <param name="pageSize">The page size of results to return</param>
        /// <returns>returns the other volumes via rtfo submissions for the organisation in the obligation period</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Submissions/OtherRtfoVolumes")]
        public async Task<IActionResult> GetOtherRtfoVolumes(int organisationId, int obligationPeriodId, int page, int pageSize)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var submissions = await this.BalanceService.GetOtherRtfoVolumes(organisationId, obligationPeriodId, page, pageSize);
                if (submissions == null)
                {
                    result = NotFound("Other volumes via RTFO submission details not found");
                }
                else
                {
                    result = Ok(submissions);
                }
            }
            else
            {
                result = Forbid();
            }
            return result;
        }

        /// <summary>
        /// Gets Electricity submissions details
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="page">The page number of results to display</param>
        /// <param name="pageSize">The results per page to display</param>
        /// <returns>returns a Electricity submissions details for the organisation in the obligation period</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/ElectricitySubmissions")]
        public async Task<IActionResult> GetElectricitySubmissions(int organisationId, int obligationPeriodId, [FromQuery]int pageSize, [FromQuery]int page = 1)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var submissions = await this.BalanceService.GetElectricitySubmissions(organisationId, obligationPeriodId, page, pageSize);
                if (submissions == null)
                {
                    result = NotFound("Electricity submission details not found");
                }
                else
                {
                    result = Ok(submissions);
                }
            }
            else
            {
                result = Forbid();
            }
            return result;
        }

        /// <summary>
        /// Gets Upstream emission reduction (UER) submissions details
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="pageSize">The results per page to display</param>
        /// <param name="page">The page number of results to display</param>
        /// <returns>returns an Upstream emission reduction (UER) submissions details for the organisation in the obligation period</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/UpstreamEmissionReductionSubmissions")]
        public async Task<IActionResult> GetUpstreamEmissionReductionSubmissions(int organisationId, int obligationPeriodId, [FromQuery]int pageSize, [FromQuery]int page = 1)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var submissions = await this.BalanceService.GetUpstreamEmissionReductionSubmissions(organisationId, obligationPeriodId, page, pageSize);
                if (submissions == null)
                {
                    result = NotFound("Upstream emission reduction (UER) submission details not found");
                }
                else
                {
                    result = Ok(submissions);
                }
            }
            else
            {
                result = Forbid();
            }
            return result;
        }

        /// <summary>
        /// Gets the submissions result to export as a CSV using the organisationId and obligationPeriodId
        /// </summary>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <returns>Gets the submissions result to export as a CSV using the organisationId and obligationPeriodId</returns>
        [HttpGet("Organisation/{organisationId}/obligationperiod/{obligationPeriodId}/GenerateExportForOrganisation")]
        public async Task<IActionResult> GenerateSubmissionsExport(int organisationId, int obligationPeriodId)
        {
            IActionResult result;

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var submissions = await this.BalanceService.GenerateExportForOrganisation(organisationId, obligationPeriodId);
                if (submissions == null)
                {
                    result = NotFound("No submission records not found");
                }
                else
                {
                    return Ok(Encoding.Default.GetBytes(submissions));
                }
            }
            else
            {
                result = Forbid();
            }
            return result;
        }

        /// <summary>
        /// View for Downloading all application data for Obligation Period
        /// </summary>
        /// <returns>Returns the download application data view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("obligationperiod/{obligationPeriodId}/GenerateExportForAllOrganisations")]
        public async Task<IActionResult> DownloadAllApplications(int obligationPeriodId)
        {
            IActionResult result;

            var submissions = await this.BalanceService.GenerateExportForAllOrganisations(obligationPeriodId);
            if (submissions == null)
            {
                result = NotFound("No submission records not found");
            }
            else
            {
                return Ok(Encoding.Default.GetBytes(submissions));
            }
            return result;
        }

        /// <summary>
        /// View for records of credit transfers
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="searchTerm"></param>
        /// <param name="pageSize">The results per page to display</param>
        /// <param name="pageNumber">The page number of results to display</param>
        /// <returns>Returns the credit transfer view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("ObligationPeriod/{obligationPeriodId}/CreditTransfers")]
        public async Task<IActionResult> GetCreditTransfers(int obligationPeriodId, [FromQuery]string searchTerm, [FromQuery]int pageSize = 10, [FromQuery]int pageNumber = 1)
        {
            var creditTransfers = await this.BalanceService.GetCreditTransfers(obligationPeriodId, searchTerm, pageSize, pageNumber);

            if (creditTransfers == null)
            {
                return NotFound("No credit transfer records found");
            }

            return Ok(creditTransfers);
        }

        /// <summary>
        /// Gets a CSV download for all credit events for a particular obligation period, filtered by a search term
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="searchTerm">The search term, typically an organisation name, to filter on</param>
        /// <returns>Returns the credit transfer view</returns>
        [Authorize(Roles = Roles.Administrator)]
        [HttpGet("ObligationPeriod/{obligationPeriodId}/CreditTransfers/Csv")]
        public async Task<IActionResult> GetCreditTransfersCsv(int obligationPeriodId, [FromQuery]string searchTerm)
        {
            var creditTransfers = await this.BalanceService.GetCreditTransfers(obligationPeriodId, searchTerm);

            if (creditTransfers == null)
            {
                return NotFound("No credit transfer records found for the obligation period");
            }

            var export = new StringBuilder(CreditTransferSummary.GetCsvHeader() + "\r\n");

            foreach (var creditTransfer in creditTransfers)
            {
                export.Append(creditTransfer.ToString() + "\r\n");
            }

            return Ok(Encoding.Default.GetBytes(export.ToString()));
        }

        #endregion Methods
    }
}
