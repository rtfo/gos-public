﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.HealthChecks.Mongo;

namespace DfT.GOS.GhgBalanceView.API
{
    /// <summary>
    /// Provides application settings for the GHG Balance View
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// Configuration for Mongo Health Checks
        /// </summary>
        public MongoHealthCheckConfiguration MongoHealthCheck { get; set; }
    }
}
