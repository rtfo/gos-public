﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Messaging.Models;

namespace DfT.GOS.GhgLedger.API
{
    /// <summary>
    /// Application settings for the API
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// The Identifier for the service, used to connect to remote revices via JWT Token
        /// </summary>
        public string SERVICE_IDENTIFIER { get; set; }

        /// <summary>
        /// HTTP Client setting for resilliant HTTP communication
        /// </summary>
        public HttpClientAppSettings HttpClient { get; set; }

        /// <summary>
        /// RabbitMQ settings for communicating with message queues
        /// </summary>
        public RabbitMessagingClientSettings MessagingClient { get; set; }

        /// <summary>
        /// Gets and sets the balance view service API URL.
        /// </summary>
        public string BalanceViewServiceApiUrl { get; set; }

        /// <summary>
        /// Gets and sets the reporting period service API URL
        /// </summary>
        public string ReportingPeriodsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets and sets the Organisation service API URL
        /// </summary>
        public string OrganisationsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets and sets the Identity service API URL
        /// </summary>
        public string IdentityServiceApiUrl { get; set; }

        /// <summary>
        /// The number of seconds to throttle transaction creation for
        /// </summary>
        public int TransactionThrottlingDurationSeconds { get; set; }
    }
}