﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Threading.Tasks;

namespace DfT.GOS.GhgLedger.API.Repositories
{
    /// <summary>
    /// Interface contract for a repository for CRUD operations on Transactions
    /// </summary>
    public interface ITransactionRepository
    {
        /// <summary>
        /// Creates a transaction for the obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="transactionTypeId">Id of the transaction type</param>
        /// <param name="userId">Id of the user who creates this transaction</param>
        /// <param name="additionalReference">Optional reference given by the user who creates this transaction</param>
        /// <returns>Returns the ID of newly create transaction</returns>
        Task<int> CreateTransaction(int obligationPeriodId, int transactionTypeId, string userId, string additionalReference);

        /// <summary>
        /// Date of first transfer transaction
        /// </summary>
        /// <returns>Returns date of first transfer transaction</returns>
        Task<DateTime?> GetFirstTransferTransactionDate();

        /// <summary>
        /// Number of transfer transactions for the supplied date range.
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Returns number of transfer transactions</returns>
        Task<int> GetNoOfTransferTransactions(DateTime startDate, DateTime endDate);
    }
}
