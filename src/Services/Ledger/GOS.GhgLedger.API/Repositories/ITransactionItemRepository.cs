﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Threading.Tasks;

namespace DfT.GOS.GhgLedger.API.Repositories
{
    /// <summary>
    /// Interface contract for a repository for CRUD operations on TransactionItems
    /// </summary>
    public interface ITransactionItemRepository
    {

        /// <summary>
        ///  Creates a transaction item for the transaction
        /// </summary>
        /// <param name="transactionId">Transaction id</param>
        /// <param name="transactionItemTypeId">Transaction item type</param>
        /// <param name="organisationId">Organisation Id</param>
        /// <param name="value">Transaction credits</param>
        /// <returns>ID of the transaction item created</returns>
        Task<int> CreateTransactionItem(int transactionId, int transactionItemTypeId, int organisationId
            , decimal value);
    }
}
