﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using DfT.GOS.Data.Repositories;
using DfT.GOS.GhgLedger.API.Data;
using Microsoft.EntityFrameworkCore;
using DfT.GOS.GhgLedger.API.Models;
using TransactionType = DfT.GOS.GhgLedger.API.Models.TransactionType;
using TransactionItemType = DfT.GOS.GhgLedger.API.Models.TransactionItemType;

namespace DfT.GOS.GhgLedger.API.Repositories
{
    /// <summary>
    /// Implementation of the ILedgerRepository interface
    /// </summary>
    public class LedgerRepository : AsyncEntityFrameworkRepository<Data.Transaction, int>, ILedgerRepository
    {
        #region Private Properties

        private GosLedgerDataContext GosLedgerDataContext { get { return this.DataContext as GosLedgerDataContext; } }
        private ITransactionRepository TransactionRepository { get; set; }
        private ITransactionItemRepository TransactionItemRepository { get; set; }
        #endregion Private Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="dataContext">Data Context for GHG Ledger database</param>
        /// <param name="transactionRepository"></param>
        /// <param name="transactionItem"></param>
        public LedgerRepository(GosLedgerDataContext dataContext
            , ITransactionRepository transactionRepository
            , ITransactionItemRepository transactionItem)
            : base(dataContext)
        {
            this.DataContext = dataContext;
            this.TransactionRepository = transactionRepository;
            this.TransactionItemRepository = transactionItem;
        }

        #endregion Constructors

        #region LedgerRepository Implementation

        /// <summary>
        /// Returns the most recent transaction for the given obligation period and organisation
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <returns>Returns the most recent transaction if exists, otherwise null</returns>
        public async Task<TransactionSummary> GetMostRecent(int obligationPeriodId, int organisationId)
        {
            var ti = await GosLedgerDataContext.TransactionItem
             .Include(i => i.Transaction)
             .Include(i => i.TransactionItemType)
             .Where(i => i.OrganisationId == organisationId
                       && i.Transaction.ObligationPeriodId == obligationPeriodId)
             .OrderByDescending(t => t.Transaction.CreatedDate)
             .FirstOrDefaultAsync();

            if (ti == null)
            {
                return null;
            }
            else
            {
                return new TransactionSummary(ti.TransactionId,
                               ti.TransactionItemId,
                               ti.Transaction.TransactionTypeId,
                               ti.TransactionItemTypeId,
                               ti.TransactionItemType.Type,
                               ti.TransactionItemType.Description,
                               ti.Transaction.ObligationPeriodId,
                               ti.OrganisationId,
                               this.GosLedgerDataContext.TransactionItem
                                     .Where(ti2 => ti2.OrganisationId != ti.OrganisationId
                                             && ti2.TransactionId == ti.TransactionId)
                                     .FirstOrDefault().OrganisationId, // This currently returns a null result for ti.TransactionItemType.Type 3 or 4 - it does not throw an exception for referencing OrganisationId
                               ti.Value,
                               ti.Transaction.CreatedDate,
                               ti.Transaction.CreatedBy,
                               ti.Transaction.AdditionalReference);
            }
        }

        /// <summary>
        /// Returns all transactions for given obligation period and organisation
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="organisationId">Id of required Organisation</param>
        /// <returns>Returns all transactions summaries for given obligation period and organisation</returns>
        public async Task<IEnumerable<Models.TransactionSummary>> Get(int obligationPeriodId, int organisationId)
        {
            var transactions = await GosLedgerDataContext.TransactionItem
             .Include(ti => ti.Transaction)
             .Include(ti => ti.TransactionItemType)
             .Where(ti => ti.OrganisationId == organisationId
                       && ti.Transaction.ObligationPeriodId == obligationPeriodId)
             .Select(ti => new TransactionSummary(ti.TransactionId,
                            ti.TransactionItemId,
                            ti.Transaction.TransactionTypeId,
                            ti.TransactionItemTypeId,
                            ti.TransactionItemType.Type,
                            ti.TransactionItemType.Description,
                            ti.Transaction.ObligationPeriodId,
                            ti.OrganisationId,
                            this.GosLedgerDataContext.TransactionItem
                                  .Where(ti2 => ti2.OrganisationId != ti.OrganisationId
                                          && ti2.TransactionId == ti.TransactionId)
                                  .FirstOrDefault().OrganisationId, // This currently returns a null result for ti.TransactionItemType.Type 3 or 4 - it does not throw an exception for referencing OrganisationId
                            ti.Value,
                            ti.Transaction.CreatedDate,
                            ti.Transaction.CreatedBy,
                            ti.Transaction.AdditionalReference))
             .ToListAsync();

            return transactions;
        }

        /// <summary>
        /// Creates a transaction for the obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="fromOrganisationId">ID of the organisation from there the credits are transferred</param>
        /// <param name="toOrganisationId">ID of the recipient organisation</param>
        /// <param name="value">Credit value</param>
        /// <param name="userId">Id of the user who creates this transaction</param>
        /// <param name="additionalReference">Optional reference given by the user who creates this transaction</param>
        /// <returns>Returns the ID of newly create transaction</returns>
        public async Task<int> TransferCredits(int obligationPeriodId, int fromOrganisationId
            , int toOrganisationId, decimal value, string userId, string additionalReference)
        {
            var transactionId = 0;
            var strategy = GosLedgerDataContext.Database.CreateExecutionStrategy();
            await strategy.ExecuteAsync(async () =>
           {
               using (var trans = GosLedgerDataContext.Database.BeginTransaction())
               {
                   try
                   {
                       transactionId = await this.TransactionRepository.CreateTransaction(obligationPeriodId, (int)TransactionType.Transfer, userId, additionalReference);
                       
                       //Transfer Out
                       await this.TransactionItemRepository.CreateTransactionItem(transactionId, (int)TransactionItemType.TransferOut
                           , fromOrganisationId, value * (-1));
                       //Transfer In
                       await this.TransactionItemRepository.CreateTransactionItem(transactionId, (int)TransactionItemType.TransferIn
                           , toOrganisationId, value);
                       trans.Commit();
                   }
                   catch
                   {
                       transactionId = 0;
                       trans.Rollback();
                   }
                   return transactionId;
               }
           });
            return transactionId;
        }

        #endregion LedgerRepository Implementation
    }
}
