﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Threading.Tasks;
using System.Linq;
using DfT.GOS.Data.Repositories;
using DfT.GOS.GhgLedger.API.Data;

namespace DfT.GOS.GhgLedger.API.Repositories
{
    /// <summary>
    /// Implementation of the ITransactionItemRepository interface
    /// </summary>
    public class TransactionItemRepository : AsyncEntityFrameworkRepository<Data.TransactionItem, int>, ITransactionItemRepository
    {
        #region Private members
        private GosLedgerDataContext GosLedgerDataContext { get { return this.DataContext as GosLedgerDataContext; } }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="dataContext">Data Context for GHG Ledger database</param>
        public TransactionItemRepository(GosLedgerDataContext dataContext)
            : base(dataContext)
        {
            this.DataContext = dataContext;
        }
        #endregion

        #region ITransactionItemRepository Implementation
        /// <summary>
        /// Creates a transaction item for the transaction
        /// </summary>
        /// <param name="transactionId">Transaction id</param>
        /// <param name="transactionItemTypeId">Transaction item type</param>
        /// <param name="organisationId">Organisation Id</param>
        /// <param name="value">Transaction credits</param>
        /// <returns>ID of the transaction item created</returns>
        public async Task<int> CreateTransactionItem(int transactionId, int transactionItemTypeId, int organisationId
            , decimal value)
        {
            var transactionItemType = GosLedgerDataContext.TransactionItemType.SingleOrDefault(
                x => x.TransactionItemTypeId == transactionItemTypeId);
            var transactionItem = new TransactionItem
            {
                TransactionId = transactionId,
                OrganisationId = organisationId,
                TransactionItemType = transactionItemType,
                Value = value
            };
            await GosLedgerDataContext.TransactionItem.AddAsync(transactionItem);
            GosLedgerDataContext.SaveChanges();
            return transactionItem.TransactionItemId;
        }

        #endregion ITransactionItemRepository Implementation
    }
}
