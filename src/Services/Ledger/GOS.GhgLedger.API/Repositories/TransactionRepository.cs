﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Threading.Tasks;
using System.Linq;
using DfT.GOS.Data.Repositories;
using DfT.GOS.GhgLedger.API.Data;
using System;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.GhgLedger.API.Repositories
{
    /// <summary>
    /// Implementation of the ITransactionRepository interface
    /// </summary>
    public class TransactionRepository : AsyncEntityFrameworkRepository<Data.Transaction, int>, ITransactionRepository
    {
        #region Private members
        private GosLedgerDataContext GosLedgerDataContext { get { return this.DataContext as GosLedgerDataContext; } }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="dataContext">Data Context for GHG Ledger database</param>
        public TransactionRepository(GosLedgerDataContext dataContext)
            : base(dataContext)
        {
            this.DataContext = dataContext;
        }
        #endregion

        #region ITransactionItemRepository Implementation
        /// <summary>
        /// Creates a transaction for the obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Obligation period id</param>
        /// <param name="transactionTypeId">Transaction type id</param>
        /// <param name="userId">User Id</param>
        /// <param name="additionalReference">Additional reference</param>
        /// <returns>ID of the created transaction</returns>
        public async Task<int> CreateTransaction(int obligationPeriodId, int transactionTypeId, string userId, string additionalReference)
        {
            var transactionType = GosLedgerDataContext.TransactionType.SingleOrDefault(x => x.TransactionTypeId == transactionTypeId);
            var transaction = new Transaction
            {
                ObligationPeriodId = obligationPeriodId,
                TransactionType = transactionType,
                CreatedBy = userId,
                CreatedDate = DateTime.Now,
                AdditionalReference = additionalReference
            };
            await GosLedgerDataContext.Transaction.AddAsync(transaction);
            GosLedgerDataContext.SaveChanges();
            return transaction.TransactionId;
        }

        /// <summary>
        /// Date of first application
        /// </summary>
        /// <returns>Returns date of first application</returns>
        async Task<DateTime?> ITransactionRepository.GetFirstTransferTransactionDate()
        {
            var firstTransaction = await this.GosLedgerDataContext.Transaction
                .OrderBy(u => u.CreatedDate)
                .Where(u => u.TransactionTypeId == (int)Models.TransactionType.Transfer)
                .FirstOrDefaultAsync();

            if (firstTransaction == null)
            {
                return null;
            }

            return firstTransaction.CreatedDate;
        }

        /// <summary>
        /// Date of first transfer transaction
        /// </summary>
        /// <returns>Returns date of first transfer transaction</returns>
        async Task<int> ITransactionRepository.GetNoOfTransferTransactions(DateTime startDate, DateTime endDate)
        {
            var count = await this.GosLedgerDataContext.Transaction
                .Where(u => u.TransactionTypeId == (int)Models.TransactionType.Transfer
                    && u.CreatedDate >= startDate && u.CreatedDate <= endDate)
                .CountAsync();
            return count;
        }

        #endregion  ITransactionRepository Implementation
    }
}
