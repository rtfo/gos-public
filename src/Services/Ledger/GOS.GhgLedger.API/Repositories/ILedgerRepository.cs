﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgLedger.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgLedger.API.Repositories
{
    /// <summary>
    /// Interface contract for a repository to get GHG Ledger details.
    /// </summary>
    public interface ILedgerRepository
    {
        /// <summary>
        /// Returns all transactions for given obligation period and organisation
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="organisationId">Id of required Organisation</param>
        /// <returns>Requested Obligation Period, if it exists, otherwise null</returns>
        Task<IEnumerable<TransactionSummary>> Get(int obligationPeriodId, int organisationId);

        /// <summary>
        /// Returns the most recent transaction for the given obligation period and organisation
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period</param>
        /// <param name="organisationId">The ID of the organisation</param>
        /// <returns>Returns the most recent transaction if exists, otherwise null</returns>
        Task<TransactionSummary> GetMostRecent(int obligationPeriodId, int organisationId);

        /// <summary>
        /// Creates a transaction for the obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="fromOrganisationId">ID of the organisation from there the credits are transferred</param>
        /// <param name="toOrganisationId">ID of the recipient organisation</param>
        /// <param name="value">Credit value</param>
        /// <param name="userId">Id of the user who creates this transaction</param>
        /// <param name="additionalReference">Optional reference given by the user who creates this transaction</param>
        /// <returns>Returns the ID of newly create transaction</returns>
        Task<int> TransferCredits(int obligationPeriodId, int fromOrganisationId
            , int toOrganisationId, decimal value, string userId, string additionalReference);
    }
}
