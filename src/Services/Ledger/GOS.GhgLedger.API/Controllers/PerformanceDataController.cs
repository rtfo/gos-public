﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgLedger.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Dft.GOS.GhgLedger.API.Controllers
{
    /// <summary>
    /// Gets GHG Transaction Details
    /// </summary>
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PerformanceDataController : Controller
    {
        #region Properties
        private ILedgerService LedgerService { get; set; }
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependencies
        /// </summary>
        /// <param name="ledgerService">The ledger service</param>
        public PerformanceDataController(ILedgerService ledgerService)
        {
            this.LedgerService = ledgerService;
        }
        #endregion Constructors

        #region Methods

        /// <summary>
        /// Returns the performance data relating to the ledger
        /// </summary>
        /// <remarks>
        /// - No. of transfer transactions for the supplied date range.
        /// </remarks>
        /// <returns>JSON array of objects for the ledger performance data</returns>
        [Route("Ledger")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> LedgerPerformanceData()
        {
            var ledgerPerformanceData = await LedgerService.GetLedgerPerformanceData();
            return new OkObjectResult(ledgerPerformanceData);
        }
        #endregion Methods
    }
}
