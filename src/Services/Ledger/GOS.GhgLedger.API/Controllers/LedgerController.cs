﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgLedger.API;
using DfT.GOS.GhgLedger.API.Commands;
using DfT.GOS.GhgLedger.API.Services;
using DfT.GOS.GhgLedger.Common.Exceptions;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Dft.GOS.GhgLedger.API.Controllers
{
    /// <summary>
    /// Gets GHG Ledger details
    /// </summary>
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LedgerController : ControllerBase
    {
        #region Properties

        private IOrganisationsService OrganisationsService { get; set; }
        private ILedgerService LedgerService { get; set; }
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private IBalanceViewService BalanceViewService { get; set; }
        private IIdentityService IdentityService { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependencies
        /// </summary>
        public LedgerController(ILedgerService ledgerService
            , IBalanceViewService viewService
            , IOrganisationsService organisationsService
            , IReportingPeriodsService reportingPeriodsService
            , IIdentityService identityService
            , IOptions<AppSettings> appSettings)
        {
            this.LedgerService = ledgerService;
            this.BalanceViewService = viewService;
            this.OrganisationsService = organisationsService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.IdentityService = identityService;
            this.AppSettings = appSettings;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets all transactions for an obligation period and organisation
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return all transactions with obligation id 14 and organisation id 1:     /api/v1/Ledger/ObligationPeriod/14/Organisation/1
        /// </remarks>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="organisationId">Id of required Organisation</param>
        /// <returns>Returns all transactions for given obligation period and organisation</returns>
        [HttpGet("ObligationPeriod/{obligationPeriodId}/Organisation/{organisationId}")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Get(int obligationPeriodId, int organisationId)
        {
            var authResult = await this.IdentityService.AuthenticateService(AppSettings.Value.SERVICE_IDENTIFIER);
            var authToken = authResult.Token;
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            if (obligationPeriod == null)
            {
                return NotFound("Obligation period not found");
            }
            var organisation = await this.OrganisationsService.GetOrganisation(organisationId, authToken);
            if (!organisation.HasResult && organisation.Result == null)
            {
                return NotFound("Organisation Not found.");
            }

            if (this.User.CanAccessOrganisationDetails(organisationId))
            {
                var transactions = await LedgerService.Get(obligationPeriodId, organisationId);
                if (transactions == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(transactions);
                }
            }
            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Creates a transaction items for the transaction.
        /// 
        /// Please note that transferring credits is throttled to improve security.
        /// </summary>
        /// <param name="command">Create transaction command contained the attribute to create a transaction</param>
        /// <returns></returns>
        [HttpPost("TransferCredits")]
        [Authorize(Roles = Roles.Administrator + ", " + Roles.Supplier + ", " + Roles.Trader)]
        public async Task<IActionResult> TransferCredits([FromBody] TransferCreditsCommand command)
        {
            if (ModelState.IsValid)
            {
                var user = this.User.GetUserId();
                var userAuthToken = this.User.GetJwtToken(this.HttpContext);

                if (command.OrganisationId == command.RecipientOrganisationId)
                {
                    return BadRequest("Transfer cannot be made within the same organisation.");
                }

                var organisation = await this.OrganisationsService.GetOrganisation(command.OrganisationId, userAuthToken);
                if (!organisation.HasResult || organisation.Result == null)
                {
                    return NotFound("Organisation not found");
                }

                //We have to use an elevated permission to get organisation details for an organisation the current user isn't linked to
                var authResult = await this.IdentityService.AuthenticateService(AppSettings.Value.SERVICE_IDENTIFIER);
                var elevatedAuthToken = authResult.Token;

                organisation = await this.OrganisationsService.GetOrganisation(command.RecipientOrganisationId, elevatedAuthToken);
                if (!organisation.HasResult || organisation.Result == null)
                {
                    return NotFound("Recipient organisation not found");
                }

                var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(command.ObligationPeriodId);
                if (obligationPeriod == null)
                {
                    return NotFound("Obligation period not found");
                }

                if (this.User.CanAccessOrganisationDetails(command.OrganisationId))
                {
                    var balanceSummary = await this.BalanceViewService.GetSummary(command.OrganisationId
                        , command.ObligationPeriodId
                        , userAuthToken);

                    if (command.Value > balanceSummary.Result.BalanceCredits)
                    {
                        return BadRequest("Number of credits to transfer exceeds the credits available for transfer.");
                    }

                    if (obligationPeriod.LatestDateForRedemption.Value < DateTime.Now)
                    {
                        return BadRequest("Transfers are no longer allowed for the Obligation Period.");
                    }

                    int transactionId;

                    try
                    {
                        transactionId = await this.LedgerService.TransferCredits(command.ObligationPeriodId
                            , command.OrganisationId, command.RecipientOrganisationId, command.Value, user, command.AdditionalReference);
                    }
                    catch (TransactionThrottlingException)
                    {
                        return StatusCode((int)HttpStatusCode.TooManyRequests, "Unable to transfer credits because transactions are being throttled.");
                    }

                    return Ok(transactionId);
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                var error = ModelState.Keys
                   .Aggregate(new StringBuilder(), (sb, k) => sb.AppendFormat("The value supplied for '{0}' is invalid. ", k), sb => sb.ToString());
                return BadRequest(error);
            }
        }
        #endregion Methods
    }
}