﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DfT.GOS.GhgLedger.API.Migrations
{
    /// <summary>
    /// Migration class for Ledger database
    /// Removes unused references to "Revoke" fields
    /// </summary>
    public partial class RemoveLedgerRevocations : Migration
    {
        /// <summary>
        /// Upgrades the Ledger database
        /// </summary>
        /// <param name="migrationBuilder">MigrationBuilder instance</param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionItem_TransactionItem_RevokedTransactionItemId",
                table: "TransactionItem");

            migrationBuilder.DropIndex(
                name: "IX_TransactionItem_RevokedTransactionItemId",
                table: "TransactionItem");

            migrationBuilder.DropColumn(
                name: "RevokedTransactionItemId",
                table: "TransactionItem");
        }

        /// <summary>
        /// Downgrades a Ledger database
        /// </summary>
        /// <param name="migrationBuilder">MigrationBuilder instance</param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RevokedTransactionItemId",
                table: "TransactionItem",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionItem_RevokedTransactionItemId",
                table: "TransactionItem",
                column: "RevokedTransactionItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionItem_TransactionItem_RevokedTransactionItemId",
                table: "TransactionItem",
                column: "RevokedTransactionItemId",
                principalTable: "TransactionItem",
                principalColumn: "TransactionItemId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
