﻿// <auto-generated />
using System;
using DfT.GOS.GhgLedger.API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DfT.GOS.GhgLedger.API.Migrations
{
    [DbContext(typeof(GosLedgerDataContext))]
    [Migration("20190701153143_AddAdditionalReferences")]
    partial class AddAdditionalReferences
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("DfT.GOS.GhgLedger.API.Data.Transaction", b =>
                {
                    b.Property<int>("TransactionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdditionalReference");

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int>("ObligationPeriodId");

                    b.Property<int>("TransactionTypeId");

                    b.HasKey("TransactionId");

                    b.HasIndex("TransactionTypeId");

                    b.ToTable("Transaction");
                });

            modelBuilder.Entity("DfT.GOS.GhgLedger.API.Data.TransactionItem", b =>
                {
                    b.Property<int>("TransactionItemId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("OrganisationId");

                    b.Property<int>("TransactionId");

                    b.Property<int>("TransactionItemTypeId");

                    b.Property<decimal>("Value");

                    b.HasKey("TransactionItemId");

                    b.HasIndex("TransactionId");

                    b.HasIndex("TransactionItemTypeId");

                    b.ToTable("TransactionItem");
                });

            modelBuilder.Entity("DfT.GOS.GhgLedger.API.Data.TransactionItemType", b =>
                {
                    b.Property<int>("TransactionItemTypeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<string>("Type")
                        .IsRequired();

                    b.HasKey("TransactionItemTypeId");

                    b.ToTable("TransactionItemType");
                });

            modelBuilder.Entity("DfT.GOS.GhgLedger.API.Data.TransactionType", b =>
                {
                    b.Property<int>("TransactionTypeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<string>("Type")
                        .IsRequired();

                    b.HasKey("TransactionTypeId");

                    b.ToTable("TransactionType");
                });

            modelBuilder.Entity("DfT.GOS.GhgLedger.API.Data.Transaction", b =>
                {
                    b.HasOne("DfT.GOS.GhgLedger.API.Data.TransactionType", "TransactionType")
                        .WithMany()
                        .HasForeignKey("TransactionTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DfT.GOS.GhgLedger.API.Data.TransactionItem", b =>
                {
                    b.HasOne("DfT.GOS.GhgLedger.API.Data.Transaction", "Transaction")
                        .WithMany()
                        .HasForeignKey("TransactionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DfT.GOS.GhgLedger.API.Data.TransactionItemType", "TransactionItemType")
                        .WithMany()
                        .HasForeignKey("TransactionItemTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
