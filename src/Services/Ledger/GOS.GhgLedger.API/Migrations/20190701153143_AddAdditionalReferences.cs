﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DfT.GOS.GhgLedger.API.Migrations
{
    public partial class AddAdditionalReferences : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdditionalReference",
                table: "Transaction",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdditionalReference",
                table: "Transaction");
        }
    }
}
