﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AutoMapper;
using DfT.GOS.GhgBalanceView.API.Client.Services;
using DfT.GOS.GhgLedger.API.Data;
using DfT.GOS.GhgLedger.API.Repositories;
using DfT.GOS.GhgLedger.API.Services;
using DfT.GOS.HealthChecks.Common;
using DfT.GOS.HealthChecks.Common.Extensions;
using DfT.GOS.HealthChecks.Rabbit;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Services;
using DfT.GOS.Messaging.Client;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace DfT.GOS.GhgLedger.API
{
    /// <summary>
    /// The startup file for GHG Ledger API
    /// </summary>
    public class Startup
    {
        #region Constants

        /// <summary>
        /// The environment variable to parse for the service identifier used to gain a JWT token
        /// for authentication against remote services
        /// </summary>
        public const string EnvironmentVariable_ServiceIdentifier = "SERVICE_IDENTIFIER";

        /// <summary>
        /// The environment variable to parse for the connection string
        /// </summary>
        public const string EnvironmentVariable_ConnectionString = "CONNECTION_STRING";

        /// <summary>
        /// The environment variable for the security JWT Token Audience used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenAudience = "JWT_TOKEN_AUDIENCE";

        /// <summary>
        /// The environment variable for the security JWT Token Issuer used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenIssuer = "JWT_TOKEN_ISSUER";

        /// <summary>
        /// The environment variable for the security JWT Token Key used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenKey = "JWT_TOKEN_KEY";

        /// <summary>
        /// The environment variable for the messaging client username
        /// </summary>
        public const string EnvironmentVariable_MessagingClientUsername = "MESSAGINGCLIENT:USERNAME";

        /// <summary>
        /// The environment variable for the messaging client password
        /// </summary>
        public const string EnvironmentVariable_MessagingClientPassword = "MESSAGINGCLIENT:PASSWORD";

        /// <summary>
        /// The error message to throw when no service identifier is supplied
        /// </summary>
        public const string ErrorMessage_ServiceIdentifierNotSupplied = "Unable to get the Service Idetifier from the environmental variables. Missing variable: " + EnvironmentVariable_ServiceIdentifier;

        /// <summary>
        /// The error message to throw when no connection string is supplied
        /// </summary>
        public const string ErrorMessage_ConnectionStringNotSupplied = "Unable to connect to the GHG Ledger datasource, the connection string was not specified. Missing variable: " + EnvironmentVariable_ConnectionString;

        /// <summary>
        /// The error message to throw when no JWT Token Audience environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenAudience = "Unable to get the JWT Token Audience from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenAudience;

        /// <summary>
        /// The error message to throw when no JWT Token Issuer environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenIssuer = "Unable to get the JWT Token Issuer from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenIssuer;

        /// <summary>
        /// The error message to throw when no JWT Token Key environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenKey = "Unable to get the JWT Token Key from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenKey;

        /// <summary>
        /// The error message to throw when messaging client user name environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_MessagingClientUsername = "Unable to get the Messaging Client User Name from the enironmental veriables.  Missing variable: " + EnvironmentVariable_MessagingClientUsername;

        /// <summary>
        /// The error message to throw when messaging client password environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_MessagingClientPassword = "Unable to get the Messaging Client Password from the enironmental veriables.  Missing variable: " + EnvironmentVariable_MessagingClientPassword;

        #endregion Constants

        #region Properties
        private ILogger<Startup> Logger { get; set; }

        /// <summary>
        /// Gets the configuration settings
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="configuration">The configuration settings</param>
        /// <param name="logger">The logger</param>
        public Startup(IConfiguration configuration
            , ILogger<Startup> logger)
        {
            this.Logger = logger;
            this.Logger.LogInformation("Startup called for new instance of DfT.GOS.GhgLedger.API");
            this.Configuration = configuration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Service(s) to be configured/added</param>
        public void ConfigureServices(IServiceCollection services)
        {
            //If the solution configuration is set to Migration skip the validation
#if (!Migration)
            this.ValidateEnvironmentVariables();
#endif

            var connectionString = Configuration[EnvironmentVariable_ConnectionString];

            //Add health check to probe the DB
            services.AddHealthChecks()
                .AddDbContextCheck<GosLedgerDataContext>(name: "postgres", tags: new[] { HealthCheckTypes.Readiness })
                .AddCheck<RabbitHealthCheck>("rabbit", tags: new[] { HealthCheckTypes.Readiness });

            //Configure the db context to use postgresql
            services.AddDbContext<GosLedgerDataContext>(options =>
            {
                // To create/manage migrations select the "Migrations" solution configuration
#if (Migration)
                options.UseNpgsql("Test");
#else
                options.UseNpgsql(connectionString
                    , npgsqlOptionsAction: sqlOptions =>
                    {
                        sqlOptions.EnableRetryOnFailure();
                    });
#endif
            });

            var appSettings = Configuration.Get<AppSettings>();

            //JWT Security Authentication
            var jwtSettings = new JwtAuthenticationSettings(Configuration[EnvironmentVariable_JwtTokenIssuer]
                , Configuration[EnvironmentVariable_JwtTokenAudience]
                , Configuration[EnvironmentVariable_JwtTokenKey]);

            var jwtConfigurer = new JwtAuthenticationConfigurer(jwtSettings);
            jwtConfigurer.Configure(services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }));

            var cacheEntryOptions = new MemoryCacheEntryOptions()
               .SetAbsoluteExpiration(TimeSpan.FromSeconds(appSettings.HttpClient.HttpClientCacheTimeout));

            // Configure Services / Resiliant HttpClient for service calls
            var httpConfigurer = new HttpClientConfigurer(appSettings.HttpClient, this.Logger);

            // - Resolve Repositories
            services.Configure<AppSettings>(Configuration);
            services.AddTransient<ILedgerRepository, LedgerRepository>();
            services.AddTransient<ITransactionRepository, TransactionRepository>();
            services.AddTransient<ITransactionItemRepository, TransactionItemRepository>();
            services.AddSingleton(typeof(MemoryCacheEntryOptions), cacheEntryOptions);

            // - Resolve Services
            services.AddTransient<ILedgerService, LedgerService>();
            services.AddTransient<IOrganisationsService, OrganisationsService>();
            services.AddTransient<IReportingPeriodsService, ReportingPeriodsService>();

            httpConfigurer.Configure(services.AddHttpClient<IBalanceViewService, BalanceViewService>(config => { config.BaseAddress = new Uri(appSettings.BalanceViewServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IOrganisationsService, OrganisationsService>(config => { config.BaseAddress = new Uri(appSettings.OrganisationsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IReportingPeriodsService, ReportingPeriodsService>(config => { config.BaseAddress = new Uri(appSettings.ReportingPeriodsServiceApiUrl); }));
            httpConfigurer.Configure(services.AddHttpClient<IIdentityService, IdentityService>(config => { config.BaseAddress = new Uri(appSettings.IdentityServiceApiUrl); }));


            // - Resolve Messaging Client
            // This needs to be a singleton so that the Rabbit connection is reused
            services.AddSingleton<IMessagingClient>(s => new RabbitMessagingClient(appSettings.MessagingClient, s.GetService<ILogger<RabbitMessagingClient>>()));

            // - Resolve Messaging Health Check
            // This needs to be a singleton so that the Rabbit connection is reused
            services.AddSingleton(s => new RabbitHealthCheck(RabbitMessagingClient.BuildConnectionFactory(appSettings.MessagingClient), s.GetService<ILogger<RabbitHealthCheck>>()));

            // Register the Swagger generator
            try
            {
                //Swagger required services
                services.AddSingleton<IApiDescriptionGroupCollectionProvider, ApiDescriptionGroupCollectionProvider>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "GHG Ledger API", Version = "v1", Description = "GHG Ledger API is used to deal with trades and credit revokes and any other adjustments." });
                    c.IncludeXmlComments(xmlPath);
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { In = ParameterLocation.Header, Description = "Please enter JWT with Bearer into field in the format 'Bearer [JWT token]'", Name = "Authorization", Type = SecuritySchemeType.ApiKey });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement() {
                        {
                            new OpenApiSecurityScheme {
                                Reference = new OpenApiReference {
                                    Id = "Bearer"
                                    , Type = ReferenceType.SecurityScheme
                                },
                                Scheme = "bearer",
                                Name = "security",
                                In = ParameterLocation.Header
                            }, new List<string>()
                        }
                    });
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Register Swagger generator because of the following exception: " + ex.ToString());
            }

            services.AddControllers().AddNewtonsoftJson();
            services.AddMemoryCache();

            // In order to map the List objects we need to explicitly create the map between the underlying objects
            services.AddAutoMapper();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The app</param>
        /// <param name="env">The environment</param>
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            try
            {
                //Enable Health Checks
                app.UseGosHealthChecks();

                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "GHG Ledger V1");
                    c.RoutePrefix = string.Empty;
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Configure Swagger because of the following exception: " + ex.ToString());
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Validates that all expected environment variables have been supplied
        /// </summary>
        private void ValidateEnvironmentVariables()
        {
            //Service Identifier
            if (Configuration[EnvironmentVariable_ServiceIdentifier] == null)
            {
                throw new NullReferenceException(ErrorMessage_ServiceIdentifierNotSupplied);
            }

            //Connection String
            if (Configuration[EnvironmentVariable_ConnectionString] == null)
            {
                throw new NullReferenceException(ErrorMessage_ConnectionStringNotSupplied);
            }

            //JWT Token Audience
            if (Configuration[EnvironmentVariable_JwtTokenAudience] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenAudience);
            }

            //JWT Token Issuer
            if (Configuration[EnvironmentVariable_JwtTokenIssuer] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenIssuer);
            }

            //JWT Token Key
            if (Configuration[EnvironmentVariable_JwtTokenKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenKey);
            }

            //Messaging Client User name
            if (Configuration[EnvironmentVariable_MessagingClientUsername] == null)
            {
                throw new NullReferenceException(ErrorMessage_MessagingClientUsername);
            }

            //Messaging Client Password
            if (Configuration[EnvironmentVariable_MessagingClientPassword] == null)
            {
                throw new NullReferenceException(ErrorMessage_MessagingClientPassword);
            }
        }

        #endregion Methods
    }
}
