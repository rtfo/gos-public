﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgLedger.API.Models;
using DfT.GOS.GhgLedger.API.Repositories;
using DfT.GOS.GhgLedger.Common.Exceptions;
using DfT.GOS.GhgLedger.Common.Models;
using DfT.GOS.Messaging.Client;
using DfT.GOS.Messaging.Messages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgLedger.API.Services
{
    /// <summary>
    /// GHG Ledger Service
    /// </summary>
    public class LedgerService : ILedgerService
    {
        #region Properties

        private readonly ILedgerRepository LedgerRepository;
        private readonly ITransactionRepository TransactionRepository;
        private readonly IMessagingClient MessagingClient;
        private readonly ILogger<LedgerService> Logger;
        private readonly IOptions<AppSettings> AppSettings;

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="ledgerRepository">The repository holding ledger details</param>
        /// <param name="transactionRepository">The repository holding transaction details</param>
        /// <param name="messagingClient">Messaging Client used to notify the system when the Ledger is updated</param>
        /// <param name="logger">Logger instance</param>
        /// <param name="appSettings">Application settings</param>
        public LedgerService(ILedgerRepository ledgerRepository
            , ITransactionRepository transactionRepository
            , IMessagingClient messagingClient
            , ILogger<LedgerService> logger
            , IOptions<AppSettings> appSettings)
        {
            this.LedgerRepository = ledgerRepository ?? throw new ArgumentNullException("ledgerRepository");
            this.TransactionRepository = transactionRepository ?? throw new ArgumentNullException("transactionRepository");
            this.MessagingClient = messagingClient ?? throw new ArgumentNullException("messagingClient");
            this.Logger = logger ?? throw new ArgumentNullException("logger");
            this.AppSettings = appSettings ?? throw new ArgumentNullException("appSettings");
        }

        #endregion Constructors

        #region ILedgerService implementation

        /// <summary>
        /// Gets all transactions for the obligation period and organisation
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="organisationId">Id of required Organisation</param>
        /// <returns>Returns all transactions for the obligation period and organisation</returns>
        public async Task<IEnumerable<TransactionSummary>> Get(int obligationPeriodId, int organisationId)
        {
            return await this.LedgerRepository.Get(obligationPeriodId, organisationId);
        }

        /// <summary>
        /// Creates a transaction for the obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="fromOrganisationId">ID of the organisation from there the credits are transferred</param>
        /// <param name="toOrganisationId">ID of the recipient organisation</param>
        /// <param name="value">Credit value</param>
        /// <param name="userId">Id of the user who creates this transaction</param>
        /// <param name="additionalReference">Optional reference by the user who creates this transaction</param>
        /// <returns>Returns the ID of newly create transaction</returns>
        /// <exception cref="TransactionThrottlingException">Thrown when there have 
        /// been too many requests in a given time period</exception>
        public async Task<int> TransferCredits(int obligationPeriodId, int fromOrganisationId
            , int toOrganisationId, decimal value, string userId, string additionalReference)
        {
            //Throttle the number of transactions we can create in a short period of time
            await this.ThrottleTransactions(obligationPeriodId, fromOrganisationId);

            var transactionId = await LedgerRepository.TransferCredits(obligationPeriodId, fromOrganisationId, toOrganisationId, value, userId, additionalReference);

            try
            {
                // If message publishing fails then we will still continue and allow the transaction to succeed
                // The balances will be recalculated at least once per day
                await this.MessagingClient.Publish(new LedgerUpdatedMessage() { ObligationPeriodId = obligationPeriodId, OrganisationId = fromOrganisationId, TransactionId = transactionId });
                await this.MessagingClient.Publish(new LedgerUpdatedMessage() { ObligationPeriodId = obligationPeriodId, OrganisationId = toOrganisationId, TransactionId = transactionId });
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "An error occurred whilst publishing messages for the Ledger updates.");
            }

            return transactionId;
        }

        /// <summary>
        /// Returns the performance data relating to the ledger
        /// - No. of transfer transactions
        /// </summary>
        /// <returns>List of the above metrics</returns>
        public async Task<List<LedgerPerformanceData>> GetLedgerPerformanceData()
        {
            var earliestDate = await this.TransactionRepository.GetFirstTransferTransactionDate();
            if (earliestDate.HasValue)
            {
                var startMonth = new DateTime(earliestDate.Value.Year, earliestDate.Value.Month, 1);

                var counts = new List<LedgerPerformanceData>();
                counts = await this.GetLedgerPerformanceDataSince(startMonth, counts);

                return counts;
            }
            this.Logger.LogInformation("Date of first Transfer transaction not found.");
            return new List<LedgerPerformanceData>();
        }

        /// <summary>
        /// Go through every month between the month of the date provided and todays month
        /// and get the ledger performance data
        /// </summary>
        /// <param name="startMonth">Month to start from</param>
        /// <param name="metrics">List of the counts so far</param>
        /// <returns>Returns list of metrics</returns>
        private async Task<List<LedgerPerformanceData>> GetLedgerPerformanceDataSince(DateTime startMonth, List<LedgerPerformanceData> metrics)
        {
            if (DateTime.Compare(startMonth, DateTime.Now) > 0)
            {
                throw new ArgumentException("Start month can't be over today's date");
            }

            //Set the end date to the end of the month, unless we're looking at the current month, then use todays date
            DateTime endDate = startMonth.Month.Equals(DateTime.Now.Month) && startMonth.Year.Equals(DateTime.Now.Year) ? DateTime.Now : startMonth.AddMonths(1).AddMilliseconds(-1);

            var NoOfTransferTransactions = await this.TransactionRepository.GetNoOfTransferTransactions(startMonth, endDate);

            var ledgerPerformanceData = new LedgerPerformanceData(startMonth
                , NoOfTransferTransactions);
            metrics.Add(ledgerPerformanceData);


            // Check if the month that we just got data for is this month
            if (DateTime.Now.Year.Equals(startMonth.Year) && DateTime.Now.Month.Equals(startMonth.Month))
            {
                return metrics;
            }
            else
            {
                // As it's not move on to the next month
                return await this.GetLedgerPerformanceDataSince(startMonth.AddMonths(1), metrics);
            }
        }

        #endregion ILedgerService implementation

        #region Private Methods

        /// <summary>
        /// Throttle how often a transaction can be created for an obligation period/organisation in a short period of time, for example once per 60 seconds.
        /// This reduces a security threat and reduces issue to do with recalculating the balance using eventual consistency. If we're actively throttling then
        /// we'll throw an exception.
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to apply the throttling to</param>
        /// <param name="organisationId">The ID of the organisation to apply the throttling to</param>
        private async Task ThrottleTransactions(int obligationPeriodId, int organisationId)
        {
            var throttlingSeconds = this.AppSettings.Value.TransactionThrottlingDurationSeconds;

            var mostRecentTransaction = await LedgerRepository.GetMostRecent(obligationPeriodId, organisationId);
            if (mostRecentTransaction != null
                && mostRecentTransaction.CreatedDate.AddSeconds(throttlingSeconds) > DateTime.Now)
            {
                throw new TransactionThrottlingException(string.Format("Unable to create a transaction for obligation period {0}, organisation {1} because transactions are being throttle to limit creation to one per {2} seconds."
                    , obligationPeriodId.ToString()
                    , organisationId.ToString()
                    , throttlingSeconds.ToString()));
            }
        }

        #endregion Methods
    }
}
