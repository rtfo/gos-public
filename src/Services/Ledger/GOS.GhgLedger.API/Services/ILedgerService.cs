﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgLedger.API.Models;
using DfT.GOS.GhgLedger.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgLedger.API.Services
{
    /// <summary>
    /// GHG Ledger Service interface definition
    /// </summary>
    public interface ILedgerService
    {
        /// <summary>
        /// Returns all transactions for given obligation period and organisation
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="organisationId">Id of required Organisation</param>
        /// <returns>Returns all transactions for given obligation period and organisation</returns>
        Task<IEnumerable<TransactionSummary>> Get(int obligationPeriodId, int organisationId);

        /// <summary>
        /// Creates a transaction for credit transfer
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="fromOrganisationId">ID of the organisation from there the credits are transferred</param>
        /// <param name="toOrganisationId">ID of the recipient organisation</param>
        /// <param name="value">Credit value</param>
        /// <param name="userId">Id of the user who creates this transaction</param>
        /// <param name="additionalReference">Optional reference given by the user who creates this transaction</param>
        /// <returns>Returns the ID of newly create transaction</returns>
        Task<int> TransferCredits(int obligationPeriodId, int fromOrganisationId
            , int toOrganisationId, decimal value, string userId, string additionalReference);

        /// <summary>
        /// Returns the performance data relating to the ledger
        /// - No. of transfer transactions
        /// </summary>
        /// <returns>List of the above metrics</returns>
        Task<List<LedgerPerformanceData>> GetLedgerPerformanceData();
    }
}
