﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgLedger.API.Models
{
    /// <summary>
    /// Represents a transaction in the GHG Ledger in GOS
    /// </summary>
    public class TransactionSummary
    {
        #region Properties

        /// <summary>
        /// Transaction Id
        /// </summary>
        public int TransactionId { get; private set; }

        /// <summary>
        /// Transaction Item Id
        /// </summary>
        public int TransactionItemId { get; private set; }

        /// <summary>
        /// Transaction Type Id
        /// </summary>
        public int TransactionTypeId { get; private set; }

        /// <summary>
        /// Transaction Item Type Id
        /// </summary>
        public int TransactionItemTypeId { get; private set; }

        /// <summary>
        /// Transaction Item Type
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        /// Transaction Item Type Description
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Obligation Period Id
        /// </summary>
        public int ObligationPeriodId { get; private set; }

        /// <summary>
        /// Organisation Id
        /// </summary>
        public int OrganisationId { get; private set; }

        /// <summary>
        /// Trading Organisation Id
        /// </summary>
        public int? TradingOrganisationId { get; private set; }

        /// <summary>
        /// Value
        /// </summary>
        public decimal Value { get; private set; }

        /// <summary>
        /// Revoked Transaction Item Id
        /// </summary>
        public int? RevokedTransactionItemId { get; private set; }

        /// <summary>
        /// Created Date
        /// </summary>
        public DateTime CreatedDate { get; private set; }

        /// <summary>
        /// Created By
        /// </summary>
        public string CreatedBy { get; private set; }

        /// <summary>
        /// Additional Reference
        /// </summary>
        public string AdditionalReference { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor for Transaction Type
        /// </summary>
        public TransactionSummary(int transactionId
            , int transactionItemId
            , int transactionTypeId
            , int transactionItemTypeId
            , string type
            , string description
            , int obligationPeriodId
            , int organisationId
            , int? tradingOrganisationId
            , decimal value
            , DateTime createdDate
            , string createdBy
            , string additionalReference
            )
        {
            this.TransactionId = transactionId;
            this.TransactionItemId = transactionItemId;
            this.TransactionTypeId = transactionTypeId;
            this.TransactionItemTypeId = transactionItemTypeId;
            this.Type = type;
            this.Description = description;
            this.ObligationPeriodId = obligationPeriodId;
            this.OrganisationId = organisationId;
            this.TradingOrganisationId = tradingOrganisationId;
            this.Value = value;
            this.CreatedDate = createdDate;
            this.CreatedBy = createdBy;
            this.AdditionalReference = additionalReference;
        }

        #endregion Constructors
    }
}
