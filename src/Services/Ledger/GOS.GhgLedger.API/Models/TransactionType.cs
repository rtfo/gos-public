﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgLedger.API.Models
{
    /// <summary>
    /// Represents transaction type
    /// </summary>
    public enum TransactionType
    {
        /// <summary>
        /// GHG Credits Redeemed
        /// </summary>
        Redeem = 1,
        /// <summary>
        /// GHG Credits Transferred
        /// </summary>
        Transfer = 2,
        /// <summary>
        /// GHG Credits Revoked
        /// </summary>
        Revoke = 3
    }

}
