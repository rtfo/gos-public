﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgLedger.API.Models
{
    /// <summary>
    /// Represents the transaction item type
    /// </summary>
    public enum TransactionItemType
    {
        /// <summary>
        /// Credits Transfers into the organisation
        /// </summary>
        TransferIn = 1,
        
        /// <summary>
        /// Credits Transferred to another organisation
        /// </summary>
        TransferOut = 2,
        
        /// <summary>
        /// Redeem a credit
        /// </summary>
        Redeem = 3,
        
        /// <summary>
        /// Revoke the transaction
        /// </summary>
        Revoke = 4
    }
}
