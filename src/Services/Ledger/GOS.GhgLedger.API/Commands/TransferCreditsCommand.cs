﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgLedger.API.Commands
{
    /// <summary>
    /// Represents the tranaction for a credit transfer
    /// </summary>
    public class TransferCreditsCommand
    {
        /// <summary>
        /// Obligation Period Id
        /// </summary>
        [Required]
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Organisation Id
        /// </summary>
        [Required]
        public int OrganisationId { get; set; }
        /// <summary>
        /// Organisation Id for the recipient
        /// </summary>
        [Required]
        public int RecipientOrganisationId { get; set; }

        /// <summary>
        /// Credit transferred
        /// </summary>
        [Decimal(2)]
        [Display(Name = "Credits to be transferred")]
        [Range(0.01, (double)decimal.MaxValue, ErrorMessage = "The value is not valid for '{0}'")]
        [Required]
        public decimal Value { get; set; }

        /// <summary>
        /// Optional additional reference
        /// </summary>
        [Display(Name = "Additional reference")]
        [StringLength(40, ErrorMessage = "Additional reference cannot be more than 40 characters.", MinimumLength = 0)]
        public string AdditionalReference { get; set; }
    }

}
