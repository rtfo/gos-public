﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgLedger.API.Data
{
    /// <summary>
    /// Database table definition for the GHG ledger
    /// </summary>
    public class TransactionItem : IEntity<int>
    {
        /// <summary>
        /// The PK
        /// </summary>
        [Key]
        [Required]
        public int TransactionItemId { get; set; }

        /// <summary>
        /// Foreign key to transaction
        /// </summary>
        [Required]
        public int TransactionId { get; set; }

        /// <summary>
        /// Navigation property to Transaction
        /// </summary>
        public virtual Transaction Transaction { get; set; }

        /// <summary>
        /// Foreign key to Transaction Item Type
        /// </summary>
        [Required]
        public int TransactionItemTypeId { get; set; }

        /// <summary>
        /// Navigation property to Transaction Item Type
        /// </summary>
        public virtual TransactionItemType TransactionItemType { get; set; }

        /// <summary>
        /// Company Id
        /// </summary>
        [Required]
        public int OrganisationId { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [Required]
        public decimal Value { get; set; }

        #region IEntity Implementation
        int IEntity<int>.Id
        {
            get
            {
                return this.TransactionItemId;
            }
            set
            {
                this.TransactionItemId = value;
            }
        }
        #endregion IEntity Implementation
    }
}
