﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DfT.GOS.GhgLedger.API.Data
{
    /// <summary>
    /// Database table definition for the GHG ledger
    /// </summary>
    public class Transaction : IEntity<int>
    {
        /// <summary>
        /// The PK
        /// </summary>
        [Key]
        [Required]
        public int TransactionId { get; set; }

        /// <summary>
        /// The Id of Obligation Period
        /// </summary>
        [Required]
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Foreign key to Transaction Type
        /// </summary>
        public int TransactionTypeId { get; set; }

        /// <summary>
        /// Navigation property to Transaction Type
        /// </summary>
        public virtual TransactionType TransactionType { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        [Required]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// The Id of the user who created it
        /// </summary>
        [Required]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Additional Reference
        /// </summary>
        public string AdditionalReference { get; set; }

        #region IEntity Implementation
        int IEntity<int>.Id
        {
            get
            {
                return this.TransactionId;
            }
            set
            {
                this.TransactionId = value;
            }
        }
        #endregion IEntity Implementation
    }
}
