﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.GhgLedger.API.Data
{
    /// <summary>
    /// Database context for GHG Ledger
    /// </summary>
    public class GosLedgerDataContext : DbContext
    {
        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="options">context options</param>
        public GosLedgerDataContext(DbContextOptions<GosLedgerDataContext> options)
           : base(options)
        {
        }

        /// <summary>
        /// Transaction
        /// </summary>
        public DbSet<Transaction> Transaction { get; set; }

        /// <summary>
        /// Transaction Item
        /// </summary>
        public DbSet<TransactionItem> TransactionItem { get; set; }

        /// <summary>
        /// Transaction Type
        /// </summary>
        public DbSet<TransactionType> TransactionType { get; set; }

        /// <summary>
        /// Transaction Item Type
        /// </summary>
        public DbSet<TransactionItemType> TransactionItemType { get; set; }
    }
}
