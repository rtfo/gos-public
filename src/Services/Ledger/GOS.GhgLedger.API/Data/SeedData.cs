﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace DfT.GOS.GhgLedger.API.Data
{
    /// <summary>
    /// Class used to migrate and seed the Applications Database
    /// </summary>
    public static class SeedData
    {
        #region Public Methods

        /// <summary>
        /// Initialises the database, migrating the schema and seeding with data
        /// </summary>
        /// <param name="serviceProvider">The service provider used to resolve services</param>
        public static void Initialise(IServiceProvider serviceProvider)
        {
            using(var context = serviceProvider.GetRequiredService<GosLedgerDataContext>())
            {
                //Migrate the database (creating schema if necessary)
                context.Database.Migrate();

                CreateTransactionTypes(context);
                CreateTransactionItemTypes(context);
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Creates Transaction Types
        /// <param name="context">The current db context</param>
        /// </summary>
        private static void CreateTransactionTypes(GosLedgerDataContext context)
        {
            if (!context.TransactionType.Any())
            {
                context.TransactionType.Add(new TransactionType(1, "Redeem", "GHG Credits Redeemed"));
                context.TransactionType.Add(new TransactionType(2, "Transfer", "GHG Credits Transferred"));
                context.TransactionType.Add(new TransactionType(3, "Revoke", "GHG Credits Revoked"));

                context.SaveChanges();
            }
        }

        /// <summary>
        /// Creates Transaction Item Types
        /// <param name="context">The current db context</param>
        /// </summary>
        private static void CreateTransactionItemTypes(GosLedgerDataContext context)
        {
            if (!context.TransactionItemType.Any())
            {
                context.TransactionItemType.Add(new TransactionItemType(1, "TransferIn", "GHG Credits Transferred In"));
                context.TransactionItemType.Add(new TransactionItemType(2, "TransferOut", "GHG Credits Transferred Out"));
                context.TransactionItemType.Add(new TransactionItemType(3, "Redeem", "GHG Credits Redeemed"));
                context.TransactionItemType.Add(new TransactionItemType(4, "Revoke", "GHG Credits Revoked"));

                context.SaveChanges();
            }
        }

        #endregion Private Methods
    }
}
