﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgLedger.API.Data
{
    /// <summary>
    /// Database table definition for the GHG ledger
    /// </summary>
    public class TransactionType
    {
        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public TransactionType()
        {
        }

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        public TransactionType(int transactionTypeId
            , string type
            , string description)
        {
            this.TransactionTypeId = transactionTypeId;
            this.Type = type;
            this.Description = description;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// The PK
        /// </summary>
        [Key]
        [Required]
        public int TransactionTypeId { get; set; }

        /// <summary>
        /// Transaction Type
        /// </summary>
        [Required]
        public string Type { get; set; }

        /// <summary>
        /// Transaction Type Description
        /// </summary>
        [Required]
        public string Description { get; set; }

        #endregion Properties
    }
}
