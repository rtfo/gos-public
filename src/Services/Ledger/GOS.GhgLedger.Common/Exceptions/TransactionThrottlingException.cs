﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.using System;
using System;

namespace DfT.GOS.GhgLedger.Common.Exceptions
{
    /// <summary>
    /// An exception indicating that a transaction could not be created because the number of transactions that can be created has been throttled
    /// </summary>
    public class TransactionThrottlingException : Exception
    {
        #region Constructors

        /// <summary>
        /// Default constructor for TransactionThrottlingException
        /// </summary>
        public TransactionThrottlingException()
            : base()
        {
        }

        /// <summary>
        /// Creates a TransactionThrottlingException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public TransactionThrottlingException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Creates a TransactionThrottlingException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="inner">The inner exception</param>
        public TransactionThrottlingException(string message, Exception inner)
            : base(message, inner)
        {
        }

        #endregion Constructors
    }
}
