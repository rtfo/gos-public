﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgLedger.Common.Models
{
    /// <summary>
    /// Ledger Performance Data
    /// </summary>
    public class LedgerPerformanceData
    {
        #region Properties

        /// <summary>
        /// Gets or sets the date for the metrics.
        /// </summary>
        /// <value>
        /// The date for the metrics.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the number of transfer transactions for the supplied date range.
        /// </summary>
        /// <value>
        /// No. of transfer transactions for the supplied date range.
        /// </value>
        public int NoOfTransferTransactions { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="date">Date</param>
        /// <param name="noOfTransferTransactions">No. Of Transfer Transactions</param>
        public LedgerPerformanceData(DateTime date
            , int noOfTransferTransactions)
        {
            this.Date = date;
            this.NoOfTransferTransactions = noOfTransferTransactions;
        }

        #endregion Constructors
    }
}
