﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgLedger.API.Client.Commands
{
    /// <summary>
    /// Represents a transaction item command
    /// </summary>
    public class TransferCreditsCommand
    {
        /// <summary>
        /// Obligation period
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Organisation id
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// Recipient Organisation Id 
        /// </summary>
        public int RecipientOrganisationId { get; set; }

        /// <summary>
        /// Credit value to be transferred
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Additional Reference
        /// </summary>
        public string AdditionalReference { get; set; }

    }
}
