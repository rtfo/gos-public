﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.GhgLedger.API.Client.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.GhgLedger.Common.Models;

namespace DfT.GOS.GhgLedger.API.Client.Services
{
    /// <summary>
    /// GHG Ledger Service interface definition
    /// </summary>
    public interface ILedgerService
    {
        /// <summary>
        /// Retrieves all transactions for given obligation period and organisation
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="organisationId">Id of required Organisation</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns all transactions for the specified Obligation Period and Supplier</returns>
        Task<HttpObjectResponse<IEnumerable<TransactionSummary>>> Get(int obligationPeriodId, int organisationId, string jwtToken);

        /// <summary>
        /// Create a transaction and tranaction item for the request
        /// </summary>
        /// <param name="obligationId">Obligation period id</param>
        /// <param name="organisationId">Organisation Id who transfers the credit</param>
        /// <param name="toOrganisationId">Recipient organisation id</param>
        /// <param name="value">Transfer credit value</param>
        /// <param name="additionalReference">Additional reference</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the newly created tranasction id</returns>
        Task<int> TransferCredits(int obligationPeriodId, int organisationId
            , int toOrganisationId, decimal value, string additionalReference, string jwtToken);

        /// <summary>
        /// Returns the performance data relating to the ledger
        /// - No. of transfer transactions
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>List of the above metrics</returns>
        Task<List<LedgerPerformanceData>> GetLedgerPerformanceData(string jwtToken);
    }
}
