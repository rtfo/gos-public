﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using DfT.GOS.Http;
using DfT.GOS.GhgLedger.API.Client.Models;
using DfT.GOS.GhgLedger.API.Client.Commands;
using System.Net;
using DfT.GOS.GhgLedger.Common.Exceptions;
using DfT.GOS.GhgLedger.Common.Models;

namespace DfT.GOS.GhgLedger.API.Client.Services
{
    /// <summary>
    /// Client implementation of the GHG Ledger Service
    /// </summary>
    public class LedgerService : HttpServiceClient, ILedgerService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="httpClient">HttpClient to use when making requests</param>
        public LedgerService(HttpClient httpClient) : base(httpClient)
        {
        }

        #endregion Constructors

        #region ILedgerService implementation

        /// <summary>
        /// Retrieves all transactions for the obligation period and organisation
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <param name="organisationId">Id of required Organisation</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns returns all transactions for the obligation period and organisation</returns>
        public async Task<HttpObjectResponse<IEnumerable<TransactionSummary>>> Get(int obligationPeriodId, int organisationId, string jwtToken)
        {
            var url = $"/api/v1/Ledger/ObligationPeriod/{obligationPeriodId}/Organisation/{organisationId}";
            return await this.HttpGetAsync<IEnumerable<TransactionSummary>>(url, jwtToken);

        }

        /// <summary>
        /// Create a transaction and tranaction for the tranafer request
        /// </summary>
        /// <param name="obligationId">Obligation period id</param>
        /// <param name="organisationId">Organisation Id who transfers the credit</param>
        /// <param name="toOrganisationId">Recipient organisation id</param>
        /// <param name="value">Transfer credit value</param>
        /// <param name="additionalReference">Additional reference</param>
        /// <param name="jwtToken">The JWT Token for authorisation</param>
        /// <returns>Returns the newly created tranasction id</returns>
        public async Task<int> TransferCredits(int obligationId, int organisationId
            , int toOrganisationId, decimal value, string additionalReference, string jwtToken)
        {
            var url = $"/api/v1/Ledger/TransferCredits";
            var command = new TransferCreditsCommand
            {
                ObligationPeriodId = obligationId,
                OrganisationId = organisationId,
                RecipientOrganisationId = toOrganisationId,
                Value = value,
                AdditionalReference = additionalReference
            };
            var result = await this.HttpPostAsync(url, command, jwtToken);
            if (result.StatusCode == HttpStatusCode.TooManyRequests)
            {
                throw new TransactionThrottlingException("Unable to transfer credits because transactions are being throttled.");
            }
            else
            {
                return int.Parse(await result.Content.ReadAsStringAsync());
            }
        }

        /// <summary>
        /// Returns the performance data relating to the ledger
        /// - No. of transfer transactions
        /// </summary>
        /// <param name="jwtToken">The security JWT Token for authentication on the remote service</param>
        /// <returns>List of the above metrics</returns>
        async Task<List<LedgerPerformanceData>> ILedgerService.GetLedgerPerformanceData(string jwtToken)
        {
            var response = await this.HttpGetAsync<List<LedgerPerformanceData>>("api/v1/PerformanceData/Ledger", jwtToken);
            return response.Result;
        }

        #endregion ILedgerService implementation
    }
}
