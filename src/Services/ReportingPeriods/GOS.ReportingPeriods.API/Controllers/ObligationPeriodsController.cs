﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.API.Services;
using DfT.GOS.ReportingPeriods.Common.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.Controllers
{
    /// <summary>
    /// Gets Reporting Period details
    /// </summary>
    [Route("api/v1/[controller]")]
    public class ObligationPeriodsController : ControllerBase
    {

        #region Properties

        private IObligationPeriodsService ObligationPeriodsService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependancies
        /// </summary>
        /// <param name="obligationPeriodsService">The service</param>
        public ObligationPeriodsController(IObligationPeriodsService obligationPeriodsService)
        {
            this.ObligationPeriodsService = obligationPeriodsService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Returns a list of all Obligation Periods.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return all obligation periods:     /api/v1/ObligationPeriods 
        /// </remarks>
        /// <returns>Returns a list of all Obligation periods</returns>
        [HttpGet()]
        public async Task<IActionResult> GetAll()
        {
            IList<ObligationPeriod> obligationPeriods = await this.ObligationPeriodsService.Get();
            return Ok(obligationPeriods);
        }

        /// <summary>
        /// Gets the current Obligation Period
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return all obligation periods:     /api/v1/ObligationPeriods/Current
        /// </remarks>
        /// <returns>Returns the current obligation period, if there is one, otherwise a 404</returns>
        [HttpGet("Current")]
        public async Task<IActionResult> GetCurrent()
        {
            ObligationPeriod currentObligationPeriod = await this.ObligationPeriodsService.GetCurrent();
            if (currentObligationPeriod == null)
            {
                return NotFound("No current obligation period could be found.");
            }
            else
            {
                return Ok(currentObligationPeriod);
            }
        }

        /// <summary>
        /// Gets the specified Obligation Period
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return obligation period with id 14:     /api/v1/ObligationPeriods/14
        /// </remarks>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <returns>Returns the requested obligation period, if it exists, otherwise a 404</returns>
        [HttpGet("{obligationPeriodId}")]
        public async Task<IActionResult> Get(int obligationPeriodId)
        {
            ObligationPeriod obligationPeriod = await this.ObligationPeriodsService.Get(obligationPeriodId);
            if (obligationPeriod == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(obligationPeriod);
            }
        }

        #endregion Methods
    }
}
