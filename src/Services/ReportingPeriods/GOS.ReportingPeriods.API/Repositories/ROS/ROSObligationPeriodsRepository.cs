﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using AutoMapper;
using DfT.GOS.ReportingPeriods.Common.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.Repositories.ROS
{
    /// <summary>
    /// Repository for connecting into Obligation Periods held within ROS
    /// </summary>
    public class RosObligationPeriodsRepository: IObligationPeriodsRepository
    {
        #region Properties

        private RosReportingPeriodsDataContext DataContext { get; set; }
        private IMapper Mapper { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataContext">The data context for ROS</param>
        /// <param name="mapper">The automapper mapper instane</param>
        public RosObligationPeriodsRepository(RosReportingPeriodsDataContext dataContext, IMapper mapper)
        {
            this.DataContext = dataContext;
            this.Mapper = mapper;
        }

        #endregion Constructors

        #region IObligationPeriodsRepository Implementatio

        /// <summary>
        /// Gets list of all obligation periods
        /// </summary>
        /// <returns>Returns a list of all obligation periods</returns>
        public async Task<IList<ObligationPeriod>> Get()
        {
            var obligationPeriodsView = await this.DataContext.GosObligationPeriodsView.ToListAsync();
            var obligationPeriods = 
                this.Mapper.Map<IList<GosObligationPeriodsView>, IList<ObligationPeriod>>(
                    obligationPeriodsView.OrderByDescending(x => x.EndDate).ToList());

            return obligationPeriods;
        }

        /// <summary>
        /// Gets the current obligation period
        /// </summary>
        /// <returns>Returns the current obligation period if it exists, otherwise null</returns>
        public async Task<ObligationPeriod> GetCurrent()
        {
            ObligationPeriod currentObligationPeriod = null;
            //TODO: Don't use datetime.now, inject helper instead. See https://jira.triad.co.uk/browse/ROSGOS-819
            var now = DateTime.Now;

            var obligationPeriodsView = await this.DataContext.GosObligationPeriodsView
                                                            .SingleOrDefaultAsync(op => now >= op.StartDate
                                                                                        && now <= op.EndDate);
            if (obligationPeriodsView != null)
            {
                currentObligationPeriod = this.Mapper.Map<GosObligationPeriodsView, ObligationPeriod>(obligationPeriodsView);
            }

            return currentObligationPeriod;
        }

        /// <summary>
        /// Returns the specified Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of Obligation Period</param>
        /// <returns>Requested Obligation Period, if it exists, otherwise null</returns>
        public async Task<ObligationPeriod> Get(int obligationPeriodId)
        {
            var obligationPeriod = await this.DataContext.GosObligationPeriodsView
                .SingleOrDefaultAsync(op => op.Id == obligationPeriodId);

            if (obligationPeriod != null)
            {
                return this.Mapper.Map<GosObligationPeriodsView, ObligationPeriod>(obligationPeriod);
            }
            else
            {
                return null;
            }
        }

        #endregion IObligationPeriodsRepository Implementation
    }
}
