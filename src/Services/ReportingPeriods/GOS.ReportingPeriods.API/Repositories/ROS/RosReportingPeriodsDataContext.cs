﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.Repositories.ROS
{
    /// <summary>
    /// Database context for Reporting Periods within ROS
    /// </summary>
    public class RosReportingPeriodsDataContext : DbContext
    {
        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="options">context options</param>
        public RosReportingPeriodsDataContext(DbContextOptions<RosReportingPeriodsDataContext> options)
           : base(options)
        {
        }

        /// <summary>
        /// Gets a reference to the Gos_ObligationPeriodsView in ROS
        /// </summary>
        public DbSet<GosObligationPeriodsView> GosObligationPeriodsView { get; set; }
    }
}
