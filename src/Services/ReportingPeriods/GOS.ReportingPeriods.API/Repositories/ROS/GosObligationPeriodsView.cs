﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DfT.GOS.ReportingPeriods.API.Repositories.ROS
{

    /// <summary>
    /// DTO corresponding to the Gos_ObligationPeriodsView in ROS database
    /// </summary>
    [Table("Gos_ObligationPeriodsView")]
    public class GosObligationPeriodsView
    {
        /// <summary>
        /// Obligation Period Id
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Start Date
        /// </summary>
        public Nullable<DateTime> StartDate { get; private set; }

        /// <summary>
        /// End Date
        /// </summary>
        public Nullable<DateTime> EndDate { get; private set; }

        /// <summary>
        /// Flag indicating if the Obligation Period is active
        /// </summary>
        public bool IsActivated { get; private set; }

        /// <summary>
        /// Latest Date for Applications from Suppliers
        /// </summary>
        public Nullable<DateTime> LatestDateForApplicationsFromSupplier { get; private set; }

        /// <summary>
        /// De Minimis (Litres)
        /// </summary>
        public int DeMinimisLitres { get; private set; }

        /// <summary>
        /// No De Minimis (Litres)
        /// </summary>
        public int NoDeMinimisLitres { get; private set; }

        /// <summary>
        /// Latest Date for Redemption
        /// </summary>
        public Nullable<DateTime> LatestDateForRedemption { get; private set; }

        /// <summary>
        /// Latest Date for Revocation
        /// </summary>
        public Nullable<DateTime> LatestDateForRevocation { get; private set; }

        /// <summary>
        /// Latest Date to Notify Revocation
        /// </summary>
        public Nullable<DateTime> LatestDateToNotifyRevocation { get; private set; }

        /// <summary>
        /// GHG Threshold (g CO2 / MJ)
        /// </summary>
        public Nullable<Decimal> GhgThresholdGrammesCO2PerMJ { get; private set; }

        /// <summary>
        /// GHG Low Volume Supplier Deduction (Kg CO2)
        /// </summary>
        public Nullable<int> GhgLowVolumeSupplierDeductionKGCO2 { get; private set; }

        /// <summary>
        /// GHG Buyout (pence / Kg CO2)
        /// </summary>
        public Nullable<Decimal> GhgBuyoutPencePerKGCO2 { get; private set; }

    }
}
