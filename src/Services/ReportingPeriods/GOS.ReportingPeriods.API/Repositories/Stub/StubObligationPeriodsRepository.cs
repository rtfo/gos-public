﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.Repositories.Stub
{
    /// <summary>
    /// Provides a stub obligation periods repository, based upon a Json file, to enable testing the obligation periods without being connected to ROS.
    /// </summary>
    public class StubObligationPeriodsRepository: IObligationPeriodsRepository
    {
        #region Properties

        private List<ObligationPeriod> ObligationPeriods { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Construct a stub obligation peirod repository based upon a Json file
        /// </summary>
        public StubObligationPeriodsRepository(string jsonFilePath)
        {
            //Read in organisations from json file
            var json = File.ReadAllText(jsonFilePath);
            this.ObligationPeriods = JsonConvert.DeserializeObject<List<ObligationPeriod>>(json);
        }

        #endregion Constructors

        #region IObligationPeriodsRepository Implementation

        /// <summary>
        /// Gets all obligation  periods
        /// </summary>
        /// <returns>Returns a list of all obligation periods</returns>
        public Task<IList<ObligationPeriod>> Get()
        {
            return Task.Run<IList<ObligationPeriod>>(() =>
            {
                return this.ObligationPeriods.OrderByDescending(x => x.EndDate).ToList();
            });
        }

        /// <summary>
        /// Gets the current obligation period
        /// </summary>
        /// <returns>Returns the current obligation period if it exists, otherwise null</returns>
        public Task<ObligationPeriod> GetCurrent()
        {
            return Task.Run<ObligationPeriod>(() =>
            {
                //TODO: Don't use datetime.now, inject helper instead. See https://jira.triad.co.uk/browse/ROSGOS-819
                var now = DateTime.Now;
                return this.ObligationPeriods.SingleOrDefault(op => now >= op.StartDate
                                                                && now <= op.EndDate);
            });
        }

        /// <summary>
        /// Gets the specified Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <returns>Requested obligation period, if it exists, otherwise null</returns>
        public Task<ObligationPeriod> Get(int obligationPeriodId)
        {
            return Task.Run(() => this.ObligationPeriods
                .SingleOrDefault(op => op.Id == obligationPeriodId));
        }

        #endregion IObligationPeriodsRepository Implementation
    }
}
