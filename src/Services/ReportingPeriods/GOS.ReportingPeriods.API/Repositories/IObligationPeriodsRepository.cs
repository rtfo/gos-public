﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.Repositories
{
    /// <summary>
    /// Interface contract for a repository to get obligation period details.
    /// </summary>
    public interface IObligationPeriodsRepository
    {
        /// <summary>
        /// Gets list of all Obligation Periods
        /// </summary>
        /// <returns>Returns a list of all obligation periods</returns>
        Task<IList<ObligationPeriod>> Get();

        /// <summary>
        /// Retrieves a specific Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <returns>Requested Obligation Period, if it exists, otherwise null</returns>
        Task<ObligationPeriod> Get(int obligationPeriodId);

        /// <summary>
        /// Gets the current obligation period
        /// </summary>
        /// <returns>Returns the current obligation period if it exists, otherwise null</returns>
        Task<ObligationPeriod> GetCurrent();
    }
}
