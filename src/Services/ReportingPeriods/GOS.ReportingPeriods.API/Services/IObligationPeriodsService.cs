﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.Services
{
    /// <summary>
    /// Obligation Period Service interface definition
    /// </summary>
    public interface IObligationPeriodsService
    {
        /// <summary>
        /// Get all Obligation Periods
        /// </summary>
        /// <returns>Returns all Obligation Periods</returns>
        Task<IList<ObligationPeriod>> Get();

        /// <summary>
        /// Get the current obligation period
        /// </summary>
        /// <returns>returns the current obligation period if it exists, otherwise NULL</returns>
        Task<ObligationPeriod> GetCurrent();

        /// <summary>
        /// Gets the specified Obligation Period
        /// </summary>
        /// <param name="obligationPeriodId">Id of required Obligation Period</param>
        /// <returns>Requested Obligation Period</returns>
        Task<ObligationPeriod> Get(int obligationPeriodId);
    }
}
