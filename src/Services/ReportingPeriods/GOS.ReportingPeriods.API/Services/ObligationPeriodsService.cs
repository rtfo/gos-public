﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.API.Repositories;
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.Services
{
    /// <summary>
    /// Obligation Periods Service
    /// </summary>
    public class ObligationPeriodsService: IObligationPeriodsService 
    {
        #region Properties

        private IObligationPeriodsRepository ObligationPeriodsRepository { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="obligationPeriodsRepository">The repository holding obligation periods details</param>
        public ObligationPeriodsService(IObligationPeriodsRepository obligationPeriodsRepository)
        {
            this.ObligationPeriodsRepository = obligationPeriodsRepository;
        }

        #endregion Constructors

        #region IObligationPeriodsService implementation

        /// <summary>
        /// Get all obligation periods
        /// </summary>
        /// <returns>Returns all obligation periods</returns>
        public Task<IList<ObligationPeriod>> Get()
        {
            return this.ObligationPeriodsRepository.Get();
        }

        /// <summary>
        /// Get the current obligation period
        /// </summary>
        /// <returns>returns the current obligation period if it exists, otherwise NULL</returns>
        public Task<ObligationPeriod> GetCurrent()
        {
            return this.ObligationPeriodsRepository.GetCurrent();
        }

        /// <summary>
        /// Gets the requested obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of required obligation period</param>
        /// <returns>Requested obligation period, if it exists, otherwise NULL</returns>
        public Task<ObligationPeriod> Get(int obligationPeriodId)
        {
            return this.ObligationPeriodsRepository.Get(obligationPeriodId);
        }

        #endregion IObligationPeriodsService implementation
    }
}
