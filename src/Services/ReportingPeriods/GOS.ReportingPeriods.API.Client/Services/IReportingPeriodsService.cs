﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.ReportingPeriods.API.Client.Services
{
    // <summary>
    /// Definition for getting reporting period details from a remote service
    /// </summary>
    public interface IReportingPeriodsService
    {
        Task<IList<ObligationPeriod>> GetObligationPeriods();
        Task<ObligationPeriod> GetObligationPeriod(int id);
        Task<ObligationPeriod> GetCurrentObligationPeriod();
    }
}
