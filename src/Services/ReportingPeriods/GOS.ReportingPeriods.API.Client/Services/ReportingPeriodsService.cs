﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using DfT.GOS.ReportingPeriods.Common.Models;

namespace DfT.GOS.ReportingPeriods.API.Client.Services
{
    /// <summary>
    /// Connect to a remote service to get reporting period details
    /// </summary>
    public class ReportingPeriodsService : IReportingPeriodsService
    {
        #region Properties

        private HttpClient HttpClient { get; set; }
        private IMemoryCache MemoryCache;
        private MemoryCacheEntryOptions MemoryCacheEntryOptions;
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        /// <param name="memoryCache">Represents a local in-memory cache</param>
        /// <param name="cacheEntryOptions">Represents the cache options applied to an entry of the IMemoryCache instance</param>
        public ReportingPeriodsService(HttpClient httpClient, IMemoryCache memoryCache, MemoryCacheEntryOptions cacheEntryOptions)
        {
            this.HttpClient = httpClient;
            this.MemoryCache = memoryCache;
            this.MemoryCacheEntryOptions = cacheEntryOptions;
        }

        #endregion Constructors

        #region IReportingPeriodsService implementation

        /// <summary>
        /// Gets an obligation period from the remote service. 
        /// Note, this implementation will call the get all method and filter.
        /// </summary>
        /// <param name="id">The ID of the supplier</param>
        /// <returns>Returns the obligation period</returns>
        public async Task<ObligationPeriod> GetObligationPeriod(int id)
        {
            var obligationPeriods = await GetOrCreateObligationPeriod();
            return obligationPeriods.SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Gets all obligation periods from the remote service
        /// </summary>
        /// <returns>Returns a list of all obligation periods from the remote service</returns>
        public async Task<IList<ObligationPeriod>> GetObligationPeriods()
        {
            return await GetOrCreateObligationPeriod();
        }

        /// <summary>
        /// Gets the current obligation period from the remote service
        /// </summary>
        /// <returns>Returns the current obligation period, if there is one</returns>
        public async Task<ObligationPeriod> GetCurrentObligationPeriod()
        {
            var obligationPeriods = await GetOrCreateObligationPeriod();
            var now = DateTime.Now;
            return obligationPeriods.SingleOrDefault(x => now >= x.StartDate && now <= x.EndDate);
        }

        #endregion IReportingPeriodsService implementation

        #region Private Methods
        private async Task<IList<ObligationPeriod>> GetOrCreateObligationPeriod()
        {
            IList<ObligationPeriod> obligationPeriods;
            if(!MemoryCache.TryGetValue("AllObligationPeriods", out obligationPeriods))
            {
                var response = await this.HttpClient.GetAsync("api/v1/obligationPeriods");

                if (response.IsSuccessStatusCode)
                {
                    var json = response.Content.ReadAsStringAsync().Result;
                    obligationPeriods = JsonConvert.DeserializeObject<List<ObligationPeriod>>(json);
                }
                else
                {
                    throw await response.GetUnexpectedResponseException();
                }
                MemoryCache.Set("AllObligationPeriods", obligationPeriods, MemoryCacheEntryOptions);
            }
            return obligationPeriods;
        }
        #endregion
    }
}
