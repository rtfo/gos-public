﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.ReportingPeriods.Common.Models
{
    /// <summary>
    /// Represents an Obligation Period in GOS
    /// </summary>
    public class ObligationPeriod
    {
        #region Properties

        /// <summary>
        /// Obligation Period Id
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Obligation Period Start Date
        /// </summary>
        public Nullable<DateTime> StartDate { get; private set; }

        /// <summary>
        /// Obligation Period End Date
        /// </summary>
        public Nullable<DateTime> EndDate { get; private set; }

        /// <summary>
        /// Flag indicating if Obligation Period is active
        /// </summary>
        public bool IsActivated { get; private set; }

        /// <summary>
        /// Date of latest date on which Suppliers may submit applications
        /// </summary>
        public Nullable<DateTime> LatestDateForApplicationsFromSupplier { get; private set; }

        /// <summary>
        /// De Minimis value (in Litres)
        /// </summary>
        public int DeMinimisLitres { get; private set; }

        /// <summary>
        /// No De Minimis value (in Litres)
        /// </summary>
        public int NoDeMinimisLitres { get; private set; }

        /// <summary>
        /// Latest Date for Redemption
        /// </summary>
        public Nullable<DateTime> LatestDateForRedemption { get; private set; }

        /// <summary>
        /// Latest Date for Revocation
        /// </summary>
        public Nullable<DateTime> LatestDateForRevocation { get; private set; }

        /// <summary>
        /// Latest Date to Notify Revocation
        /// </summary>
        public Nullable<DateTime> LatestDateToNotifyRevocation { get; private set; }

        /// <summary>
        /// GHG Threshold (in g CO2/MJ)
        /// </summary>
        public Nullable<Decimal> GhgThresholdGrammesCO2PerMJ { get; private set; }

        /// <summary>
        /// GHG Low Volume Supplier Deduction (in Kg CO2)
        /// </summary>
        public Nullable<int> GhgLowVolumeSupplierDeductionKGCO2 { get; private set; }

        /// <summary>
        /// GHG Buyout price (in pence/Kg CO2)
        /// </summary>
        public Nullable<Decimal> GhgBuyoutPencePerKGCO2 { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor for Obligation Period
        /// </summary>
        public ObligationPeriod(int id
            , Nullable<DateTime> startDate
            , Nullable<DateTime> endDate
            , bool isActivated
            , Nullable<DateTime> latestDateForApplicationsFromSupplier
            , int deMinimisLitres
            , int noDeMinimisLitres
            , Nullable<DateTime> latestDateForRedemption
            , Nullable<DateTime> latestDateForRevocation
            , Nullable<DateTime> latestDateToNotifyRevocation
            , Nullable<Decimal> ghgThresholdGrammesCO2PerMJ
            , Nullable<int> ghgLowVolumeSupplierDeductionKGCO2
            , Nullable<Decimal> ghgBuyoutPencePerKGCO2
            )
        {
            this.Id = id;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.IsActivated = isActivated;
            this.LatestDateForApplicationsFromSupplier = latestDateForApplicationsFromSupplier;
            this.DeMinimisLitres = deMinimisLitres;
            this.NoDeMinimisLitres = noDeMinimisLitres;
            this.LatestDateForRedemption = latestDateForRedemption;
            this.LatestDateForRevocation = latestDateForRevocation;
            this.LatestDateToNotifyRevocation = latestDateToNotifyRevocation;
            this.GhgThresholdGrammesCO2PerMJ = ghgThresholdGrammesCO2PerMJ;
            this.GhgLowVolumeSupplierDeductionKGCO2 = ghgLowVolumeSupplierDeductionKGCO2;
            this.GhgBuyoutPencePerKGCO2 = ghgBuyoutPencePerKGCO2;
        }

        #endregion Constructors
    }
}
