﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgFuels.Common
{
    /// <summary>
    /// Provides enumerations for GHG Fuels
    /// </summary>
    public static class Enums
    {
        /// <summary>
        /// Enum used to identify a fuel type by it's ID
        /// </summary>
        public enum FuelTypes
        {
            Electricity = 1,
            UpstreamEmissionsReduction = 2
        }

        /// <summary>
        /// Enum used to identify a fuel category by it's ID
        /// </summary>
        public enum FuelCategories
        {
            Electricity = 1,
            UpstreamEmissionsReduction = 2,
            FossilGas = 3
        }
    }
}
