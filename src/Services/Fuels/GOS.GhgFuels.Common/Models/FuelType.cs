﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

namespace DfT.GOS.GhgFuels.Common.Models
{
    /// <summary>
    /// Represents a GOS Fuel type
    /// </summary>
    public class FuelType
    {
        #region Properties

        /// <summary>
        /// The ID of the fuel type
        /// </summary>
        public int FuelTypeId { get; private set; }

        /// <summary>
        /// The ID of the fuel category this fuel type relates to
        /// </summary>
        public int FuelCategoryId { get; private set; }

        /// <summary>
        /// The fuel category the fuel type relates to
        /// </summary>
        public string FuelCategory { get; private set; }

        /// <summary>
        /// The name of the fuel type
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The carbon intensity of the fuel type. Note the fuel type might not have a carbon intensity associated with it, possibly because it must be user defined
        /// </summary>
        public decimal? CarbonIntensity { get; private set; }

        /// <summary>
        /// The energy densityy of the fuel type. Note the fuel type might not have an energy density associated with it, possibly because it must be user defined
        /// </summary>
        public decimal? EnergyDensity { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the Fuel Type
        /// </summary>
        public FuelType(int fuelTypeId
            , int fuelCategoryId
            , string fuelCategory
            , string name
            , decimal? carbonIntensity
            , decimal? energyDensity)
        {
            this.FuelTypeId = fuelTypeId;
            this.FuelCategoryId = fuelCategoryId;
            this.FuelCategory = fuelCategory;
            this.Name = name;
            this.CarbonIntensity = carbonIntensity;
            this.EnergyDensity = energyDensity;
        }
        #endregion Constructors
    }
}
