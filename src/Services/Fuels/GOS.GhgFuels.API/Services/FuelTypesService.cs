﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgFuels.API.Repositories;
using DfT.GOS.GhgFuels.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgFuels.API.Services
{
    /// <summary>
    /// Fuel Types Service
    /// </summary>
    public class FuelTypesService: IFuelTypesService
    {
        #region Properties

        private IFuelTypesRepository FuelTypesRepository { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="fuelTypesRepository">The repository holding fuel type details</param>
        public FuelTypesService(IFuelTypesRepository fuelTypesRepository)
        {
            this.FuelTypesRepository = fuelTypesRepository;
        }

        #endregion Constructors

        #region IFuelTypesService implementation

        /// <summary>
        /// Get all fuel types
        /// </summary>
        /// <returns>Returns all organisations</returns>
        Task<IList<FuelType>> IFuelTypesService.Get()
        {
            return this.FuelTypesRepository.Get();
        }

        #endregion IFuelTypesService implementation
    }
}
