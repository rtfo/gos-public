﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgFuels.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgFuels.API.Services
{
    /// <summary>
    /// Fuel Type Service interface definition
    /// </summary>
    public interface IFuelTypesService
    {
        /// <summary>
        /// Get all fuel types
        /// </summary>
        /// <returns>Returns all fuel types</returns>
        Task<IList<FuelType>> Get();
    }
}
