﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgFuels.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgFuels.API.Repositories
{
    /// <summary>
    /// Interface contract for a repository to get fuel type details.
    /// </summary>
    public interface IFuelTypesRepository
    {
        /// <summary>
        /// Gets list of all fuel types
        /// </summary>
        /// <returns>Returns a list of all fuel types</returns>
        Task<IList<FuelType>> Get();
    }
}
