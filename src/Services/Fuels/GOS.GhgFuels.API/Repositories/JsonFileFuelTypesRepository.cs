﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DfT.GOS.GhgFuels.Common.Models;
using Newtonsoft.Json;

namespace DfT.GOS.GhgFuels.API.Repositories
{
    /// <summary>
    /// Provides a fuel types repository, based upon a Json file.
    /// </summary>
    public class JsonFileFuelTypesRepository : IFuelTypesRepository
    {
        #region Properties

        private List<FuelType> FuelTypes { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Construct a fuel types repository based upon a Json file
        /// </summary>
        public JsonFileFuelTypesRepository(string jsonFilePath)
        {
            //Read in organisations from json file
            var json = File.ReadAllText(jsonFilePath);
            this.FuelTypes = JsonConvert.DeserializeObject<List<FuelType>>(json);
        }

        #endregion Constructors

        #region IFuelTypesRepository Implementation

        /// <summary>
        /// Gets all fuel types
        /// </summary>
        /// <returns>Returns a list of all fuel types</returns>
        Task<IList<FuelType>> IFuelTypesRepository.Get()
        {
            return Task.Run<IList<FuelType>>(() =>
            {
                return this.FuelTypes;
            });
        }

        #endregion IFuelTypesRepository Implementation
    }
}
