﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.GhgFuels.API.Services;
using DfT.GOS.GhgFuels.Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace GOS.GhgFuels.API.Controllers
{
    /// <summary>
    /// Gets GHG Fuel Type Details
    /// </summary>
    [Route("api/v1/[controller]")]
    public class FuelTypesController : ControllerBase
    {
        #region Properties

        private IFuelTypesService FuelTypesService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependancies
        /// </summary>
        /// <param name="fuelTypesService">The service</param>
        public FuelTypesController(IFuelTypesService fuelTypesService)
        {
            this.FuelTypesService = fuelTypesService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Returns a list of all GOS / GHG fuel types.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return all fuel types:     /api/v1/FuelTypes 
        /// </remarks>
        /// <returns>Returns a list of all fuel types</returns>
        [HttpGet()]
        public async Task<IActionResult> GetAll()
        {
            IList<FuelType> fuelTypes = await this.FuelTypesService.Get();
            return Ok(fuelTypes);
        }

        #endregion Methods
    }
}