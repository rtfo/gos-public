﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgFuels.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgFuels.API.Client.Services
{
    /// <summary>
    /// Definition for getting GHG fuel information from a remote service
    /// </summary>
    public interface IGhgFuelsService
    {
        /// <summary>
        /// Gets a fuel type by ID
        /// </summary>
        /// <param name="id">the ID of the fuel type</param>
        Task<FuelType> GetFuelType(int id);

        /// <summary>
        /// Gets all Fuel Types
        /// </summary>
        Task<IList<FuelType>> GetFuelTypes();

        /// <summary>
        /// Gets all Fossil Gas Fuel Types
        /// </summary>
        Task<IList<FuelType>> GetFossilGasFuelTypes();

        /// <summary>
        /// Gets the Fuel Type associated with Electricity
        /// </summary>
        Task<FuelType> GetElectricityFuelType();

        /// <summary>
        /// Gets the Fuel Type associated with UER
        /// </summary>
        Task<FuelType> GetUerFuelType();
    }
}
