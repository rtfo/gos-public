﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.GhgFuels.Common;

namespace DfT.GOS.GhgFuels.API.Client.Services
{
    /// <summary>
    /// Connect to a remote service to get GHG Fuel details
    /// </summary>
    public class GhgFuelsService : IGhgFuelsService
    {
        #region Properties

        private HttpClient HttpClient { get; set; }
        private IMemoryCache MemoryCache;
        private MemoryCacheEntryOptions MemoryCacheEntryOptions;
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        public GhgFuelsService(HttpClient httpClient, IMemoryCache memoryCache, MemoryCacheEntryOptions cacheEntryOptions)
        {
            this.HttpClient = httpClient;
            this.MemoryCache = memoryCache;
            this.MemoryCacheEntryOptions = cacheEntryOptions;
        }

        #endregion Constructors

        #region IGhgFuelsService implementation

        /// <summary>
        /// Gets a list of all Fossil Gas Fuel Types
        /// </summary>
        /// <returns>Returns a list of all Fossil Gas Fuel Types</returns>
        async Task<IList<FuelType>> IGhgFuelsService.GetFossilGasFuelTypes()
        {
            var result = await ((IGhgFuelsService)this).GetFuelTypes();
            return result.Where(ft => ft.FuelCategoryId == (int)Enums.FuelCategories.FossilGas).ToList();
            
        }

        /// <summary>
        /// Gets a list of all GHG fule types
        /// </summary>
        /// <returns>Returns a list of GHG fuel types</returns>
        async Task<IList<FuelType>> IGhgFuelsService.GetFuelTypes()
        {
            return await GetOrCreateFuelTypes();
        }

        /// <summary>
        /// Gets the Electricity Fuel Type
        /// </summary>
        /// <returns>Returns the single Electricity fuel type</returns>
        async Task<FuelType> IGhgFuelsService.GetElectricityFuelType()
        {
            var result = await ((IGhgFuelsService)this).GetFuelTypes();
            return result.Single(ft => ft.FuelCategoryId == (int)Enums.FuelCategories.Electricity);
        }

        /// <summary>
        /// Gets the UER Fuel Type
        /// </summary>
        /// <returns>Returns the single UER fuel type</returns>
        async Task<FuelType> IGhgFuelsService.GetUerFuelType()
        {
            var result = await ((IGhgFuelsService)this).GetFuelTypes();
            //TODO: Don't hardcode the Fuel Category Id
            return result.Single(ft => ft.FuelCategoryId == (int)Enums.FuelCategories.UpstreamEmissionsReduction);
        }

        /// <summary>
        /// Gets a Fuel Type by ID
        /// </summary>
        /// <param name="id">The ID of the fuel type to return</param>
        /// <returns>Returns a single Fuel type based upon ID</returns>
        async Task<FuelType> IGhgFuelsService.GetFuelType(int id)
        {
            var result = await ((IGhgFuelsService)this).GetFuelTypes();
            return result.FirstOrDefault(ft => ft.FuelTypeId == id);
        }

        #endregion IGhgFuelsService implementation

        #region Private Methods
        /// <summary>
        /// Get or create fule type cache
        /// </summary>
        /// <returns></returns>
        private async Task<IList<FuelType>> GetOrCreateFuelTypes()
        {
            IList<FuelType> fuelTypes;
            if (!MemoryCache.TryGetValue("AllFuelTypes", out fuelTypes))
            {
                var response = await this.HttpClient.GetAsync(string.Format("api/v1/FuelTypes"));

                if (response.IsSuccessStatusCode)
                {
                    var json = response.Content.ReadAsStringAsync().Result;
                    fuelTypes = JsonConvert.DeserializeObject<List<FuelType>>(json);
                }
                MemoryCache.Set("AllFuelTypes", fuelTypes, MemoryCacheEntryOptions);
            }
            return fuelTypes;
        }
        #endregion
    }
}
