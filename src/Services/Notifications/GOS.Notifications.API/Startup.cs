// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.HealthChecks.Common;
using DfT.GOS.HealthChecks.Common.Extensions;
using DfT.GOS.HealthChecks.Rabbit;
using DfT.GOS.Messaging.Client;
using DfT.GOS.Security.Authentication;
using DfT.GOS.Notifications.API.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DfT.GOS.Http;
using DfT.GOS.SystemParameters.API.Client.Services;
using Microsoft.Extensions.Caching.Memory;
using DfT.GOS.Identity.API.Client.Services;

namespace DfT.GOS.Notifications.API
{
    /// <summary>
    /// The startup file for the Notifications API
    /// </summary>
    public class Startup
    {

        #region Constants
        /// <summary>
        /// The environment variable to parse for the service identifier used to gain a JWT token
        /// for authentication against remote services
        /// This is added to applications configuration due to the AddEnvironmentVariables method in the Program startup
        /// </summary>
        public const string EnvironmentVariable_ServiceIdentifier = "SERVICE_IDENTIFIER";

        /// <summary>
        /// The environment variable for the security JWT Token Issuer used for Auth
        /// </summary>
        public const string EnvironmentVariable_JWTTokenIssuer = "JWT_TOKEN_ISSUER";

        /// <summary>
        /// The environment variable for the security JWT Token Audience used for Auth
        /// </summary>
        public const string EnvironmentVariable_JWTTokenAudience = "JWT_TOKEN_AUDIENCE";

        /// <summary>
        /// The environment variable for the security JWT Token Key used for Auth
        /// </summary>
        public const string EnvironmentVariable_JWTTokenKey = "JWT_TOKEN_KEY";

        /// <summary>
        /// The environment variable for the messaging client username
        /// This is added to applications configuration due to the AddEnvironmentVariables method in the Program startup
        /// </summary>
        public const string EnvironmentVariable_MessagingClientUsername = "MESSAGINGCLIENT:USERNAME";

        /// <summary>
        /// The environment variable for the messaging client password
        /// This is added to applications configuration due to the AddEnvironmentVariables method in the Program startup
        /// </summary>
        public const string EnvironmentVariable_MessagingClientPassword = "MESSAGINGCLIENT:PASSWORD";

        /// <summary>
        /// Environment variable name for the Gov.UK notify service
        /// </summary>
        public const string EnvironmentVariable_GovNotifyURL = "GOV_NOTIFY_URL";

        /// <summary>
        /// Environment variable name for the Gov.UK notify API key for GOS
        /// </summary>
        public const string EnvironmentVariable_GovNotifyAPIKey = "GOV_NOTIFY_API_KEY";

        /// <summary>
        /// The error message to throw when no service identifier is supplied
        /// </summary>
        public const string ErrorMessage_ServiceIdentifierNotSupplied = "Unable to get the Service Identifier from the environment variables. Missing variable: " + EnvironmentVariable_ServiceIdentifier;

        /// <summary>
        /// The error message to throw when no JWT Token Audience environment variable is supplied
        /// </summary>
        public const string ErrorMessage_JWTTokenAudience = "Unable to get the JWT Token Audience from the environment variables.  Missing variable: " + EnvironmentVariable_JWTTokenAudience;

        /// <summary>
        /// The error message to throw when no JWT Token Issuer environment variable is supplied
        /// </summary>
        public const string ErrorMessage_JWTTokenIssuer = "Unable to get the JWT Token Issuer from the environment variables.  Missing variable: " + EnvironmentVariable_JWTTokenIssuer;

        /// <summary>
        /// The error message to throw when no JWT Token Key environment variable is supplied
        /// </summary>
        public const string ErrorMessage_JWTTokenKey = "Unable to get the JWT Token Key from the environment variables.  Missing variable: " + EnvironmentVariable_JWTTokenKey;

        /// <summary>
        /// The error message to throw when messaging client user name environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_MessagingClientUsername = "Unable to get the Messaging Client User Name from the environment variables.  Missing variable: " + EnvironmentVariable_MessagingClientUsername;

        /// <summary>
        /// The error message to throw when messaging client password environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_MessagingClientPassword = "Unable to get the Messaging Client Password from the environment variables.  Missing variable: " + EnvironmentVariable_MessagingClientPassword;

        /// <summary>
        /// The error message to throw when the GOV Notify URL environment variable is not supplied
        /// </summary>
        public const string ErrorMessage_GovNotifyURLNotSupplied = "GOV Notify URL not specified.  Missing variable: " + EnvironmentVariable_GovNotifyURL;


        /// <summary>
        /// The error message to throw when the GOV Notify API Key environment variable is not supplied
        /// </summary>
        public const string ErrorMessage_GovNotifyAPIKeyNotSupplied = "GOV Notify API Key not specified.  Missing variable: " + EnvironmentVariable_GovNotifyAPIKey;

        #endregion Constants

        #region Properties
        /// <summary>
        /// Gets the configuration settings
        /// </summary>
        public IConfiguration Configuration { get; }

        private ILogger<Startup> Logger { get; set; }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="configuration">The configuration settings</param>
        /// <param name="logger">The logger</param>
        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            this.Configuration = configuration;
            this.Logger = logger;
            this.Logger.LogInformation("DfT.GOS.Notifications.API startup called");
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Service(s) to be configured/added</param>
        public void ConfigureServices(IServiceCollection services)
        {
            this.ValidateEnvironmentVariables();

            var appSettings = Configuration.Get<AppSettings>();
            services.AddLogging();

            // Configure Caching
            var cacheEntryOptions = new MemoryCacheEntryOptions()
               .SetAbsoluteExpiration(TimeSpan.FromSeconds(appSettings.HttpClient.HttpClientCacheTimeout));
            services.AddSingleton(typeof(MemoryCacheEntryOptions), cacheEntryOptions);
            services.AddMemoryCache();

            // Resolve services
            services.AddSingleton<MessagingBackgroundServiceHealthCheck>();
            services.AddHostedService<MessagingBackgroundService>();

            // Configure services / resilient HttpClient for service calls
            var httpConfigurer = new HttpClientConfigurer(appSettings.HttpClient, this.Logger);

            httpConfigurer.Configure(services.AddHttpClient<IIdentityService, IdentityService>(config => { config.BaseAddress = appSettings.IdentityServiceAPIURL; }));
            httpConfigurer.Configure(services.AddHttpClient<ISystemParametersService, SystemParametersService>(config => { config.BaseAddress = appSettings.SystemParametersServiceAPIURL; }));

            // Configure Messaging Client
            // This needs to be a singleton so that the Rabbit connection is reused
            services.AddSingleton<IMessagingClient>(s => new RabbitMessagingClient(appSettings.MessagingClient, s.GetService<ILogger<RabbitMessagingClient>>()));

            // - Resolve Messaging Health Check
            // This needs to be a singleton so that the Rabbit connection is reused
            services.AddSingleton(s => new RabbitHealthCheck(RabbitMessagingClient.BuildConnectionFactory(appSettings.MessagingClient), s.GetService<ILogger<RabbitHealthCheck>>()));

            //JWT Security Authentication
            var jwtSettings = new JwtAuthenticationSettings(Configuration[EnvironmentVariable_JWTTokenIssuer]
                , Configuration[EnvironmentVariable_JWTTokenAudience]
                , Configuration[EnvironmentVariable_JWTTokenKey]);

            var jwtConfigurer = new JwtAuthenticationConfigurer(jwtSettings);
            jwtConfigurer.Configure(services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            );

            //Add Health Checks
            services.AddHealthChecks()
                .AddCheck<RabbitHealthCheck>("rabbit", tags: new[] { HealthCheckTypes.Readiness })
                .AddCheck<MessagingBackgroundServiceHealthCheck>("messaging-background-service", tags: new[] { HealthCheckTypes.Readiness });

            // Register the Swagger generator
            try
            {
                //Swagger required services
                services.AddSingleton<IApiDescriptionGroupCollectionProvider, ApiDescriptionGroupCollectionProvider>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Notifications API", Version = "v1", Description = "Notifications API is used to send notifications (usually emails) to users based on system actions." });
                    c.IncludeXmlComments(xmlPath);
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { In = ParameterLocation.Header, Description = "Please enter JWT with Bearer into field in the format 'Bearer [JWT token]'", Name = "Authorization", Type = SecuritySchemeType.ApiKey });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement() {
                        {
                            new OpenApiSecurityScheme {
                                Reference = new OpenApiReference {
                                    Id = "Bearer"
                                    , Type = ReferenceType.SecurityScheme
                                },
                                Scheme = "bearer",
                                Name = "security",
                                In = ParameterLocation.Header
                            }, new List<string>()
                        }
                    });
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Register Swagger generator because of the following exception: " + ex.ToString());
            }

            services.AddControllers();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            try
            {
                //Enable Health Checks
                app.UseGosHealthChecks();

                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Notifications V1");
                    c.RoutePrefix = string.Empty;
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Configure Swagger because of the following exception: " + ex);
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Validates that all expected environment variables have been supplied
        /// </summary>
        private void ValidateEnvironmentVariables()
        {

            //Service Identifier
            if (Configuration[EnvironmentVariable_ServiceIdentifier] == null)
            {
                throw new NullReferenceException(ErrorMessage_ServiceIdentifierNotSupplied);
            }

            //JWT Token Audience
            if (Configuration[EnvironmentVariable_JWTTokenAudience] == null)
            {
                throw new NullReferenceException(ErrorMessage_JWTTokenAudience);
            }

            //JWT Token Issuer
            if (Configuration[EnvironmentVariable_JWTTokenIssuer] == null)
            {
                throw new NullReferenceException(ErrorMessage_JWTTokenIssuer);
            }

            //JWT Token Key
            if (Configuration[EnvironmentVariable_JWTTokenKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_JWTTokenKey);
            }

            //Messaging Client User name
            if (Configuration[EnvironmentVariable_MessagingClientUsername] == null)
            {
                throw new NullReferenceException(ErrorMessage_MessagingClientUsername);
            }

            //Messaging Client Password
            if (Configuration[EnvironmentVariable_MessagingClientPassword] == null)
            {
                throw new NullReferenceException(ErrorMessage_MessagingClientPassword);
            }

            //GOV Notify variables
            if (Configuration[EnvironmentVariable_GovNotifyURL] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovNotifyURLNotSupplied);
            }

            if (Configuration[EnvironmentVariable_GovNotifyAPIKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovNotifyAPIKeyNotSupplied);
            }
        }
        #endregion Methods
    }
}
