﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Messaging.Models;
using System;

namespace DfT.GOS.Notifications.API
{
    /// <summary>
    /// Application settings for the API
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// The Identifier for the service, used to connect to remote services via JWT Token
        /// </summary>
        public string SERVICE_IDENTIFIER { get; set; }

        /// <summary>
        /// HTTP Client setting for resilient HTTP communication
        /// </summary>
        public HttpClientAppSettings HttpClient { get; set; }

        /// <summary>
        /// Settings for the RabbitMQ messaging queue
        /// </summary>
        public RabbitMessagingClientSettings MessagingClient { get; set; }

        /// <summary>
        /// Gets or sets the system parameters service API URL
        /// </summary>
        public Uri SystemParametersServiceAPIURL { get; set; }

        /// <summary>
        /// Gets or sets the Identity service API URL
        /// </summary>
        public Uri IdentityServiceAPIURL { get; set; }

    }
}
