﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.HealthChecks.Rabbit;
using DfT.GOS.Messaging.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Notifications.API.Services
{
    /// <summary>
    /// Background service for processing messages from the message queue
    /// </summary>
    public class MessagingBackgroundService : BackgroundService
    {

        #region Properties
        private MessagingBackgroundServiceHealthCheck HealthCheck { get; set; }
        private ILogger<MessagingBackgroundService> Logger { get; set; }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="client"></param>
        /// <param name="healthCheck"></param>
        /// <param name="logger"></param>
        public MessagingBackgroundService(IMessagingClient client, MessagingBackgroundServiceHealthCheck healthCheck, ILogger<MessagingBackgroundService> logger)
        {
            this.HealthCheck = healthCheck ?? throw new ArgumentNullException(nameof(healthCheck));
            this.Logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        #endregion Constructors

        /// <summary>
        /// Process messages from the message queue
        /// </summary>
        /// <param name="stoppingToken">Cancellation token</param>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            this.Logger.LogInformation("Executing Async");
            this.HealthCheck.StartupTaskCompleted = true;
        }
    }
}
