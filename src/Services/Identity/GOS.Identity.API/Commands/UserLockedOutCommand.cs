﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Identity.API.Commands
{
    /// <summary>
    /// Command to provide values required to notify a user that their account has been locked
    /// </summary>
    public class UserLockedOutCommand
    {
        /// <summary>
        /// Email address of user to send notification to
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// Name of the user being sent the notification
        /// </summary>
        public string FullName { get; private set; }

        /// <summary>
        /// Constructor taking all parameters
        /// </summary>
        /// <param name="email">Email address of user to send notification to</param>
        /// <param name="fullName">/// Name of the user being sent the notification</param>
        public UserLockedOutCommand(string email, string fullName)
        {
            this.Email = email;
            this.FullName = fullName;
        }
    }
}
