﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Commands
{
    /// <summary>
    /// Command instructing the API layer to set admin role for a user
    /// </summary>
    public class SetAdminRoleCommand
    {
        /// <summary>
        /// Email address
        /// </summary>
        [GosEmailAddress]
        [Required]
        public string EmailAddress { get; set; }
    }
}
