﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;

namespace DfT.GOS.Identity.API.Commands
{
    /// <summary>
    ///  Command instructing the API layer to reset a user's account
    /// </summary>
    public class ResetPasswordCommand
    {
        #region Properties

        /// <summary>
        /// The Reset Code issued by the Identity API
        /// </summary>
        public string ResetCode { get; set; }

        /// <summary>
        /// The email address of the account we're attempting to reset
        /// </summary>        
        [GosEmailAddress]
        public string EmailAddress { get; set; }

        /// <summary>
        /// The New password
        /// </summary>
        [GosPassword]
        public string NewPassword { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public ResetPasswordCommand(string resetCode
            , string emailAddress
            , string newPassword)
        {
            this.ResetCode = resetCode;
            this.EmailAddress = emailAddress;
            this.NewPassword = newPassword;
        }

        #endregion Constructors
    }
}
