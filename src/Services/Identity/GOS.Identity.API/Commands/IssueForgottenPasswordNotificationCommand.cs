﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Identity.API.Commands
{
    /// <summary>
    /// Command instructing the API layer to send a Forgotten Password Notification
    /// </summary>
    public class IssueForgottenPasswordNotificationCommand
    {
        #region Properties

        /// <summary>
        /// The email address of the user to send the notification to
        /// </summary>
        [Required]
        [GosEmailAddress]
        public string EmailAddress { get; set; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="emailAddress">The email address of the user to send the notification to</param>
        public IssueForgottenPasswordNotificationCommand(string emailAddress)
        {
            this.EmailAddress = emailAddress;
        }

        #endregion Constructor
    }
}
