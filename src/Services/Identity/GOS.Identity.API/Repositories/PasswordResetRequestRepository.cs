﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Threading.Tasks;
using DfT.GOS.Identity.API.Data;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Repository for Password Reset Requests
    /// </summary>
    public class PasswordResetRequestRepository : IPasswordResetRequestRepository
    {
        #region Properties

        private GosIdentityDataContext DataContext { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataContext">The database context to db operations against</param>
        public PasswordResetRequestRepository(GosIdentityDataContext dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion Constructors

        #region IPasswordResetRequestRepository Implementation

        /// <summary>
        /// Creates a new Password Reset Request
        /// </summary>
        async Task IPasswordResetRequestRepository.Create(PasswordResetRequest entity)
        {
            await this.DataContext.PasswordResetRequests.AddAsync(entity);
        }

        /// <summary>
        /// Gets a Password Reset Request by ID
        /// </summary>
        async Task<PasswordResetRequest> IPasswordResetRequestRepository.Get(string id)
        {
            return await this.DataContext.PasswordResetRequests.SingleOrDefaultAsync(e => e.PasswordResetRequestId.Equals(id));
        }

        #endregion IPasswordResetRequestRepository Implementation
    }
}
