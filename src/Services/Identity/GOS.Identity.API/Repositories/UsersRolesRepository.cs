﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Threading.Tasks;
using DfT.GOS.Identity.API.Data;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Repository for getting user role details
    /// </summary>
    public class UsersRolesRepository :  IUsersRolesRepository
    {
        #region Private Properties
        private GosIdentityDataContext DataContext { get; set; }
        #endregion Private Properties

        #region Constructors
        /// <summary>
        /// Users Roles Repository
        /// </summary>
        /// <param name="dataContext">Data Context</param>
        public UsersRolesRepository(GosIdentityDataContext dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion Constructors

        #region AsyncEntityFrameworkRepository Implementation

        /// <summary>
        /// Add User/Role
        /// </summary>
        /// <param name="userId">user Id</param>
        /// <param name="roleId">Role id</param>
        /// <returns></returns>
        public async Task<string> AddAsync(string userId, string roleId)
        {
            await this.DataContext.UserRoles.AddAsync(new IdentityUserRole<string>() { UserId = userId, RoleId = roleId });
            var response = await this.DataContext.SaveChangesAsync();

            return response.ToString();
        }

        /// <summary>
        /// Remove user from system
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public async Task<string> RemoveAsync(string userId)
        {
            var updateObject = await DataContext.Users
                .Where(u => u.Id == userId)
                .SingleOrDefaultAsync();
            updateObject.OrganisationId = 0;
            updateObject.Role = null;
            updateObject.OrganisationAssociationBy = null;
            updateObject.OrganisationAssociationDate = null;
            DataContext.Users.Update(updateObject);
            var response = await DataContext.SaveChangesAsync();

            return response.ToString();
        }

        #endregion AsyncEntityFrameworkRepository Implementation
    }
}
