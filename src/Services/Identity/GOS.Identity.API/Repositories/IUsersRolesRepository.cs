﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Interface definition for User Roles repository
    /// </summary>
    public interface IUsersRolesRepository
    {
        /// <summary>
        /// Adds a User to a Role
        /// </summary>
        /// <param name="userId">Id of User</param>
        /// <param name="rolelId">Id of Role</param>
        /// <returns>SQL result code</returns>
        Task<string> AddAsync(string userId, string rolelId);
    }
}
