﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Interface definition for Roles repository
    /// </summary>
    public interface IRolesRepository
    {
        /// <summary>
        /// Retrieves all Roles for a given User
        /// </summary>
        /// <param name="userId">Id of User</param>
        /// <returns>Roles for specified User</returns>
        Task<IList<string>> GetByUserIdAsync(string userId);

        /// <summary>
        /// Retrieves the Id of a specific Role
        /// </summary>
        /// <param name="name">Name of Role</param>
        /// <returns>Id of Role</returns>
        Task<string> GetRoleIdByNameAsync(string name);
    }
}
