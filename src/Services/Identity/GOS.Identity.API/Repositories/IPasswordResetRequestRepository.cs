﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Data;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Repository for a Password Reset Request
    /// </summary>
    public interface IPasswordResetRequestRepository
    {
        /// <summary>
        /// Gets a Password Reset Request by ID
        /// </summary>
        Task<PasswordResetRequest> Get(string id);

        /// <summary>
        /// Creates a new Password Reset Request
        /// </summary>
        Task Create(PasswordResetRequest entity);
    }
}
