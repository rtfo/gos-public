﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data.Repositories;
using DfT.GOS.Identity.API.Data;
using DfT.GOS.Identity.API.Exceptions;
using DfT.GOS.Identity.API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Repository for getting user role details
    /// </summary>
    public class UsersRepository : AsyncEntityFrameworkRepository<GosApplicationUser, string>, IUsersRepository
    {
        #region Private Properties

        private GosIdentityDataContext GosIdentityDataContext { get { return this.DataContext as GosIdentityDataContext; } }

        #endregion Private Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="dataContext">Data Context for Identity database</param>
        public UsersRepository(GosIdentityDataContext dataContext)
            : base(dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion Constructors

        #region IUsersRepository Implementation

        async Task<List<GosApplicationUser>> IUsersRepository.GetUsersByOrganisationAsync(int id)
        {
            var users = await (from u in GosIdentityDataContext.Users
                               where u.OrganisationId == id
                               select u).ToListAsync();

            return users;
        }

        async Task<GosApplicationUser> IUsersRepository.GetByEmailAsync(string normalizedEmail)
        {
            return await this.Find(u => u.NormalizedEmail == normalizedEmail);
        }

        async Task<GosApplicationUser> IUsersRepository.GetByUserNameAsync(string normalizedUserName)
        {
            return await this.Find(u => u.NormalizedUserName == normalizedUserName);
        }

        async Task<string> IUsersRepository.GetUserIdByUsernameAsync(string normalizedUserName)
        {
            var userId = await (from u in this.GosIdentityDataContext.Users
                                where u.NormalizedUserName.Equals(normalizedUserName)
                                select u.Id).FirstAsync<string>();

            return userId;
        }

        /// <summary>
        /// Retrieves the date that the first user registered or the first invite was sent
        /// </summary>
        /// <exception cref="ValidDateNotFoundException">Thrown when neither a registration or invite date are found</exception>
        /// <returns>Earliest date out of the first registration or the first invitation</returns>
        async Task<DateTime> IUsersRepository.GetFirstRegistrationOrInviteDate()
        {
            var firstInvite = await this.GosIdentityDataContext.Invites.OrderBy(i => i.InvitedDate).FirstOrDefaultAsync();
            var firstUser = await this.GosIdentityDataContext.Users.OrderBy(u => u.CreatedDate).FirstOrDefaultAsync();

            if (firstInvite == null && firstUser == null)
            {
                throw new ValidDateNotFoundException("No user registration date or invitation date found");
            }

            if (firstInvite == null)
            {
                return firstUser.CreatedDate;
            }

            if (firstUser == null)
            {
                return firstInvite.InvitedDate;
            }

            return DateTime.Compare(firstInvite.InvitedDate, firstUser.CreatedDate) < 0 ? firstInvite.InvitedDate : firstUser.CreatedDate;
        }

        /// <summary>
        /// Find how many users were created between the given dates
        /// </summary>
        /// <param name="startDate">Date to find users created after</param>
        /// <param name="endDate">Date for find users created before</param>
        /// <returns>Number of users created in a given period</returns>
        async Task<int> IUsersRepository.GetUserCountForMonth(DateTime startDate, DateTime endDate)
        {
            return await this.GosIdentityDataContext.Users.CountAsync(u => u.CreatedDate >= startDate && u.CreatedDate <= endDate);
        }

        /// <summary>
        /// Find how many invitations were sent between the given dates
        /// </summary>
        /// <param name="startDate">Date to find invitations sent after</param>
        /// <param name="endDate">Date for find invitations sent before</param>
        /// <returns>Number of invitations sent for the given period</returns>
        async Task<int> IUsersRepository.GetInvitationCountForMonth(DateTime startDate, DateTime endDate)
        {
            return await this.GosIdentityDataContext.Invites.CountAsync(i => i.InvitedDate >= startDate && i.InvitedDate <= endDate);
        }

        /// <summary>
        /// Find how many invitations have not had an account created
        /// </summary>
        /// <param name="startDate">Date to find invitations sent after that didn't result in an account being created</param>
        /// <param name="endDate">Date for find invitations sent before that didn't result in an account being created</param>
        /// <returns>Number of invitations (unique to an email address) that didn't end up with an account being created</returns>
        async Task<int> IUsersRepository.GetUnclaimedInvitationCount(DateTime startDate, DateTime endDate)
        {
            var createdUsers = this.GosIdentityDataContext.Users.Where(u => u.CreatedDate >= startDate && u.CreatedDate <= endDate);
            //Get the invites in the date range provided
            var invites = await this.GosIdentityDataContext.Invites
                .Where(i => i.InvitedDate >= startDate && i.InvitedDate <= endDate)
                .ToListAsync();
            //Count any invites that don't have a corresponding user
            var invitesWithoutUser = invites.Where(i => !createdUsers.Any(u => u.NormalizedEmail.Equals(i.Email.ToUpper()))).Count();
            //Count multiple invites to the same user, they could have only accepted one
            var countInvites = invites.Where(i => createdUsers.Any(u => u.NormalizedEmail.Equals(i.Email.ToUpper())
                    && u.CreatedDate >= i.InvitedDate && u.CreatedDate <= i.ExpireInviteLink
                )).GroupBy(i => i.Email.ToUpper()).Where(i => i.Count() > 1).Select(i => i.Count() - 1).Sum();
            //Return the total number of invites from the previous two
            return invitesWithoutUser + countInvites;
        }

        /// <summary>
        /// Updates this given User in the database
        /// </summary>
        /// <param name="user">The user to update</param>
        /// <returns>Not implemented Exception</returns>
        public override async Task UpdateAsync(GosApplicationUser user)
        {
            var existing = await this.GosIdentityDataContext.Users.AsNoTracking().SingleOrDefaultAsync(e => e.Id == user.Id);

            if (existing != null)
            {
                this.GosIdentityDataContext.Update(user);
            }
        }

        #endregion IUsersRepository Implementation

    }
}
