﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Repository for getting user role details
    /// </summary>
    public class RolesRepository : IRolesRepository
    {
        #region Private Properties
        private GosIdentityDataContext DataContext { get; set; }

        #endregion Private Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="dataContext">GOS Identity database context</param>
        public RolesRepository(GosIdentityDataContext dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion Constructors

        #region IRolesRepository

        /// <summary>
        /// Gets a list of roles for a user
        /// </summary>
        /// <param name="userId">The ID of the user to get roles for</param>
        /// <returns>Returns a list of roles</returns>
        async Task<IList<string>> IRolesRepository.GetByUserIdAsync(string userId)
        {
            var roles = (from r in this.DataContext.Roles
                         join ur in this.DataContext.UserRoles on r.Id equals ur.RoleId
                         where ur.UserId == userId
                         select r.Name).ToListAsync();

            return await roles;
        }
        /// <summary>
        /// Get Role ID by name
        /// </summary>
        /// <param name="name">Role name</param>
        /// <returns>returns roleId</returns>
        async Task<string> IRolesRepository.GetRoleIdByNameAsync(string name)
        {
            var roleId = await (from r in this.DataContext.Roles
                         where r.Name.Equals(name)
                         select r.Id).FirstOrDefaultAsync();

            return roleId;
        }

        #endregion IRolesRepository
    }
}
