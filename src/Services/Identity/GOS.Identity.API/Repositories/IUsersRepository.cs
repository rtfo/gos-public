﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data.Repositories;
using DfT.GOS.Identity.API.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Interface definition for Users repository
    /// </summary>
    public interface IUsersRepository: IAsyncRepository<GosApplicationUser, string>
    {
        /// <summary>
        /// Retrieves a User by their username
        /// </summary>
        /// <param name="normalizedUserName">Username of user</param>
        /// <returns>User details</returns>
        Task<GosApplicationUser> GetByUserNameAsync(string normalizedUserName);

        /// <summary>
        /// Retrieves a User by their email address
        /// </summary>
        /// <param name="normalizedEmail">Email address of user</param>
        /// <returns>User details</returns>
        Task<GosApplicationUser> GetByEmailAsync(string normalizedEmail);

        /// <summary>
        /// Retrieves all users for a given Organisation
        /// </summary>
        /// <param name="id">Id of Organisation</param>
        /// <returns>All users for the specified Organisation</returns>
        Task<List<GosApplicationUser>> GetUsersByOrganisationAsync(int id);

        /// <summary>
        /// Retrieves the Id of the user with given username
        /// </summary>
        /// <param name="normalizedUserName">Username of user</param>
        /// <returns>Id of user</returns>
        Task<string> GetUserIdByUsernameAsync(string normalizedUserName);

        /// <summary>
        /// Retrieves the date that the first user registered or the first invite was sent
        /// </summary>
        /// <returns>Earliest date out of the first registration or the first invitation</returns>
        Task<DateTime> GetFirstRegistrationOrInviteDate();

        /// <summary>
        /// Find how many users were created between the given dates
        /// </summary>
        /// <param name="startDate">Date to find users created after</param>
        /// <param name="endDate">Date for find users created before</param>
        /// <returns>Number of users created in a given period</returns>
        Task<int> GetUserCountForMonth(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Find how many invitations were sent between the given dates
        /// </summary>
        /// <param name="startDate">Date to find invitations sent after</param>
        /// <param name="endDate">Date for find invitations sent before</param>
        /// <returns>Number of invitations sent for the given period</returns>
        Task<int> GetInvitationCountForMonth(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Find how many users did not create accounts from their invitations
        /// </summary>
        /// <param name="startDate">Date to find invitations sent after that didn't result in an account being created</param>
        /// <param name="endDate">Date for find invitations sent before that didn't result in an account being created</param>
        /// <returns>Number of invitations (unique to an email address) that didn't end up with an account being created</returns>
        Task<int> GetUnclaimedInvitationCount(DateTime startDate, DateTime endDate);
    }
}
