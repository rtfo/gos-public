﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Threading.Tasks;
using DfT.GOS.Identity.API.Data;
using DfT.GOS.Identity.API.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Repository for getting user invited
    /// </summary>
    public class InvitesRepository :  IInvitesRepository
    {
        #region Private Properties
        private GosIdentityDataContext DataContext { get; set; }

        #endregion Private Properties

        #region Constructors
        /// <summary>
        /// Invites Repository
        /// </summary>
        /// <param name="dataContext"></param>
        public InvitesRepository(GosIdentityDataContext dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion Constructors

        #region IInviteRepository
        /// <summary>
        /// Add Invite
        /// </summary>
        /// <param name="invite">Invite values to add</param>
        /// <returns></returns>
        async public Task Add(Invites invite)
        {
            //var roles0 = (from r in this.DataContext.Invites
            //              where r.SenderId == 1
            //              select r.Email).FirstOrDefault<string>();

            await DataContext.Invites.AddAsync(invite);
            await DataContext.SaveChangesAsync();
        }

        /// <summary>
        /// Delete Invite by Email
        /// </summary>
        /// <param name="email">Invite to delete</param>
        /// <returns></returns>
        async public Task Delete(string email)
        {
            var inviteToRemove = await DataContext.Invites.FindAsync(email);
            if (inviteToRemove != null)
            {
                DataContext.Invites.Remove(inviteToRemove);
                await DataContext.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Get the Organisation and the Email by link
        /// </summary>
        /// <param name="link">Link string to Organisation and Email</param>
        /// <returns></returns>
        async public Task<GosInvitedUserAssociation> GetEmailByLinkAsync(string link)
        {
            var association = await (from r in this.DataContext.Invites
                               where r.Uri.Equals(link)
                               select new { r.SenderId, r.Email, r.Role, r.OrganisationName, r.ExpireInviteLink, r.InvitedBy }).FirstOrDefaultAsync();

            if (association != null)
            {
                GosInvitedUserAssociation gosOrganisationUserAssociation = new GosInvitedUserAssociation()
                {
                    OrganisationId = association.SenderId,
                    Company = association.OrganisationName,
                    Email = association.Email,
                    Role = association.Role,
                    ExpireInviteLink = association.ExpireInviteLink,
                    InvitedBy = association.InvitedBy
                };
                return gosOrganisationUserAssociation;
            }

            return null;
        }

        /// <summary>
        /// Update Invite
        /// </summary>
        /// <param name="invite">Invite values</param>
        /// <returns></returns>
        async public Task Update(Invites invite)
        {
            var inviteToUpdate = await DataContext.Invites.FindAsync(invite.Email);
            if (inviteToUpdate != null)
            {
                inviteToUpdate.ExpireInviteLink = invite.ExpireInviteLink;
                inviteToUpdate.SenderId = invite.SenderId;
                inviteToUpdate.Role = invite.Role;
                inviteToUpdate.Uri = invite.Uri;

                await DataContext.SaveChangesAsync();
            }
        }

        #endregion IInviteRepository
    }
}
