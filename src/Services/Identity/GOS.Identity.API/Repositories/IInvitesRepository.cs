﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Models;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Repositories
{
    /// <summary>
    /// Interface definining Invitations Repository
    /// </summary>
    public interface IInvitesRepository
    {
        /// <summary>
        /// Create an invitation
        /// </summary>
        /// <param name="invite">Invitation to create</param>
        /// <returns></returns>
        Task Add(Invites invite);

        /// <summary>
        /// Delete an invitation
        /// </summary>
        /// <param name="email">Invitation to delete</param>
        /// <returns></returns>
        Task Delete(string email);

        /// <summary>
        /// Update an existing invitation
        /// </summary>
        /// <param name="invite">Invitation to update</param>
        /// <returns></returns>
        Task Update(Invites invite);

        /// <summary>
        /// Retrieves and Invitation by the pulbic link
        /// </summary>
        /// <param name="link">Public link sent to invited user</param>
        /// <returns>Invitation associated with given link</returns>
        Task<GosInvitedUserAssociation> GetEmailByLinkAsync(string link);
    }
}
