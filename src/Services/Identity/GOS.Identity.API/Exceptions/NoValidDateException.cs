﻿using System;
using System.Runtime.Serialization;

namespace DfT.GOS.Identity.API.Exceptions
{
    /// <summary>
    /// An exception thrown when an a date that was expected can't be found
    /// </summary>
    [Serializable]
    public class ValidDateNotFoundException : ApplicationException
    {
        /// <summary>
        /// Default Constructor for the ValidDateNotFoundException
        /// </summary>
        public ValidDateNotFoundException()
        {
        }

        /// <summary>
        /// Creates an ValidDateNotFoundException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public ValidDateNotFoundException(string message) : base(message)
        {
        }

        /// <summary>
        /// Creates an ValidDateNotFoundException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="inner">The inner exception</param>
        public ValidDateNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// Creates an ValidDateNotFoundException with serialized data
        /// </summary>
        /// <param name="info">The object that holds the serialized object data</param>
        /// <param name="context">The contextual information about the source or destination</param>
        protected ValidDateNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
