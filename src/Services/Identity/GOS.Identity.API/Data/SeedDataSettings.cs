﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Identity.API.Data
{
    /// <summary>
    /// Settings for seeding the identity database
    /// </summary>
    public class SeedDataSettings
    {
        /// <summary>
        /// Email Address for admin account
        /// </summary>
        public string AdminEmailAddress { get; set; }

        /// <summary>
        /// Password for admin account
        /// </summary>
        public string AdminPassword { get; set; }
    }
}
