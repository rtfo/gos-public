﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace DfT.GOS.Identity.API.Data
{
    /// <summary>
    /// Class used to migrate and seed the Identity Database
    /// </summary>
    public static class SeedData
    {
        #region Public Methods

        /// <summary>
        /// Initialises the database, migrating the schema and seeding with data
        /// </summary>
        /// <param name="serviceProvider">The service provider used to resolve services</param>
        /// <param name="settings">Settings for seeding data</param>
        public static void Initialise(IServiceProvider serviceProvider, SeedDataSettings settings)
        {
            using(var context = serviceProvider.GetRequiredService<GosIdentityDataContext>())
            {
                //Migrate the database (creating schema if necessary)
                context.Database.Migrate();

                CreateRoles(context);
                CreateAdminUser(settings, context);
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Create an admin user if one doesn't exist and one has been specified
        /// </summary>
        /// <param name="settings">Settings for seeding Admin user</param>
        /// <param name="context">The current db context</param>
        private static void CreateAdminUser(SeedDataSettings settings, GosIdentityDataContext context)
        {
            if (settings != null
                && settings.AdminEmailAddress != null
                && settings.AdminEmailAddress != null)
            {
                if (!context.Users.Any(u => u.UserName == settings.AdminEmailAddress))
                {
                    var user = new GosApplicationUser
                    {
                        FullName = "System Administrator",
                        UserName = settings.AdminEmailAddress,
                        NormalizedUserName = settings.AdminEmailAddress.ToUpper(),
                        Email = settings.AdminEmailAddress,
                        NormalizedEmail = settings.AdminEmailAddress.ToUpper(),
                        EmailConfirmed = true,
                        LockoutEnabled = true,
                        CreatedDate = DateTime.Now,
                        SecurityStamp = Guid.NewGuid().ToString()
                    };

                    var password = new PasswordHasher<GosApplicationUser>();
                    var hashed = password.HashPassword(user, settings.AdminPassword);
                    user.PasswordHash = hashed;

                    context.Users.Add(user);
                    var adminRole = context.Roles.SingleOrDefault(r => r.Name == "Administrator");

                    context.UserRoles.Add(new IdentityUserRole<string>() { UserId = user.Id, RoleId = adminRole.Id });
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Creates Identity Roles
        /// <param name="context">The current db context</param>
        /// </summary>
        private static void CreateRoles(GosIdentityDataContext context)
        {
            if (!context.Roles.Any())
            {
                string[] roles = { "Administrator", "Supplier", "Trader", "Verifier"};

                foreach(var role in roles)
                {
                    context.Roles.Add(new IdentityRole() { Name = role, NormalizedName = role.ToLower() });
                }

                context.SaveChanges();
            }
        }

        #endregion Private Methods
    }
}
