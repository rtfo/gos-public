﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Identity.API.Data
{
    /// <summary>
    /// Database table definition for a password reset request 
    /// </summary>
    public class PasswordResetRequest
    {
        /// <summary>
        /// The PK
        /// </summary>
        [Key]
        [Required]
        public string PasswordResetRequestId { get; set; }

        /// <summary>
        /// The ID of the user the request relates to
        /// </summary>
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// The expiry date of the password reset request
        /// </summary>
        [Required]
        public DateTime Expires { get; set; }
    }
}
