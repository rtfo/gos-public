﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using DfT.GOS.Identity.API.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.Identity.API.Data
{
    /// <summary>
    /// EF Data Context for Identity database
    /// </summary>
    public class GosIdentityDataContext : IdentityDbContext<GosApplicationUser>, IDbContext
    {
        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="options"></param>
        public GosIdentityDataContext(DbContextOptions<GosIdentityDataContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Table representing invitations to the GOS system
        /// </summary>
        public DbSet<Invites> Invites { get; set; }

        /// <summary>
        /// Table for representing password reset requests
        /// </summary>
        public DbSet<PasswordResetRequest> PasswordResetRequests { get; set; }

        /// <summary>
        /// Custom logic required when the EF Model is created
        /// </summary>
        /// <param name="builder">ModelBuilder instance</param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
