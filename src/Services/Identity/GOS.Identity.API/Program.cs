﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Npgsql.Logging;
using System;

namespace DfT.GOS.Identity.API
{
    /// <summary>
    /// Contains entry point for Identity microservice
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entry point for the Identity microservice
        /// </summary>
        /// <param name="args">Command line arguments</param>
        public static void Main(string[] args)
        {
            //Enable verbose logging for the npSQL (PostgreSQL) database driver
            NpgsqlLogManager.Provider = new ConsoleLoggingProvider(NpgsqlLogLevel.Warn, true, true);

            var host = BuildWebHost(args);

            //Migrate and Seed the database
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<GosIdentityDataContext>();
                var configuration = services.GetRequiredService<IConfiguration>();

                var seedDataSettings = new SeedDataSettings()
                {
                    AdminEmailAddress = configuration["ADMIN_EMAIL_ADDRESS"],
                    AdminPassword = configuration["ADMIN_PASSWORD"]
                };

                try
                {
                    SeedData.Initialise(services, seedDataSettings);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred seeding the DB.");
                }
            }

            host.Run();
        }

        /// <summary>
        /// Creates a WebHost for the Identity service
        /// </summary>
        /// <param name="args">Command line arguments</param>
        /// <returns>WebHost instance</returns>
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://*:80")
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    config.AddEnvironmentVariables();
                })
                .Build();
    }
}
