﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.Data;
using DfT.GOS.Identity.API.Commands;
using DfT.GOS.Identity.API.Data;
using DfT.GOS.Identity.API.Models;
using Microsoft.Extensions.Logging;

namespace DfT.GOS.Identity.API.Services
{
    /// <summary>
    /// Service class for Users
    /// </summary>
    public partial class UsersService : DataServiceBase, IUsersService
    {
        #region IUsersService implementation

        /// <summary>
        /// Sends a notification to a user to enable them to reset their password
        /// </summary>
        /// <param name="command">The command with details for the password reset</param>
        /// <returns>Returns a value indicating success</returns>
        async Task<bool> IUsersService.SendUserResetPasswordNotification(IssueForgottenPasswordNotificationCommand command)
        {
            bool success = false;

            var user = await this.UsersRepository.GetByEmailAsync(command.EmailAddress.ToUpper());
            if (user != null)
            {
                string resetResuestId = Guid.NewGuid().ToString();

                var resetRequest = new PasswordResetRequest()
                {
                    PasswordResetRequestId = resetResuestId,
                    UserId = user.Id,
                    Expires = DateTime.Now.AddHours(this.AppSettings.Value.PasswordResetNotificationExpiryHours)
                };
                await this.PasswordResetRequestRepository.Create(resetRequest);
                var passwordsResetRequestsInserted = await this.SaveAsync();

                if (passwordsResetRequestsInserted == 1)
                {
                    string email_template_id = Environment.GetEnvironmentVariable(Startup.EnvironmentVariable_GovNotifyPasswordResetRequestEmailTemplateId);
                    string linkUrl = Environment.GetEnvironmentVariable(Startup.EnvironmentVariable_NewUserInviteLinkBaseUrl) + "/Account/ResetPassword/" + resetResuestId;

                    Dictionary<String, dynamic> personalisation = new Dictionary<String, dynamic>
                    {
                        { "name", user.FullName},
                        { "link", linkUrl }
                    };

                    var response = await this.Notify.SendEmail(command.EmailAddress, email_template_id, personalisation);
                    if (response != null)
                    {
                        success = true;
                    }
                }
            }

            return success;
        }

        /// <summary>
        /// Send Invite link
        /// </summary>
        /// <param name="user">Recipient User</param>
        /// <param name="invitedByUserId">The ID of the user requesting the invite</param>
        /// <returns>True/false</returns>
        public async Task<bool> InviteUserToRegisterAccount(GosApplicationUserInvite user, string invitedByUserId)
        {
            if (user.Email != null)
            {
                string link = Guid.NewGuid().ToString();
                string email_template_id = Environment.GetEnvironmentVariable("GOV_NOTIFY_NEW_USER_INVITE_EMAIL_TEMPLATEID");
                string linkUrl = Environment.GetEnvironmentVariable("NEW_USER_INVITE_LINK_BASE_URL") + "/Account/Register/" + link;

                Dictionary<String, dynamic> personalisation = new Dictionary<String, dynamic>
                {
                    { "name", user.Email},
                    { "link", linkUrl }
                };

                try
                {
                    Invites invite = new Invites
                    {
                        Email = user.Email,
                        SenderId = user.OrganisationId,
                        Role = user.Role,
                        Uri = link,
                        InvitedBy = invitedByUserId,
                        InvitedDate = DateTime.Now,
                        OrganisationName = user.Company,
                        ExpireInviteLink = DateTime.Now.AddHours(this.AppSettings.Value.InviteExpiryHours)
                    };

                    await this.InvitesRepository.Add(invite);

                    var response = await this.Notify.SendEmail(user.Email, email_template_id, personalisation);
                    if (response == null)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError("IUsersService.InviteAsync:" + ex.ToString());
                    return false;
                };
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Send Email to New user
        /// </summary>
        /// <param name="user">Recipient User</param>
        /// <returns>Returns a value indicating success</returns>
        public async Task<bool> NewUserEmailAsync(GosApplicationNewUser user)
        {
            if (user.Email != null)
            {
                string link = Guid.NewGuid().ToString();
                string email_template_id = Environment.GetEnvironmentVariable("GOV_NOTIFY_NEW_USER_CREATED_EMAIL_TEMPLATEID");

                Dictionary<String, dynamic> personalisation = new Dictionary<String, dynamic>
                {
                    { "name", user.FullName},
                    { "email address", user.Email }
                };

                try
                {
                    await this.Notify.SendEmail(user.Email, email_template_id, personalisation);
                }
                catch (Exception ex)
                {
                    Logger.LogError("NewUserEmailAsync:" + ex.ToString());
                    return false;
                };

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Sent an email notification to the user that has been linked to the company
        /// </summary>
        /// <param name="user">Recipient</param>
        /// <returns>Returns a value indicating success</returns>
        public async Task<bool> NewUserLinkedEmailAsync(GosApplicationNewUser user)
        {
            return await this.UserLinkedEmailAsync(false, user);
        }

        /// <summary>
        /// Sent an email notification to the administrator user that has been added to the system
        /// </summary>
        /// <param name="user">Recipient</param>
        /// <returns>Returns a value indicating success</returns>
        public async Task<bool> NewAdminUserLinkedEmailAsync(GosApplicationNewUser user)
        {
            return await this.UserLinkedEmailAsync(true, user);

        }

        /// <summary>
        /// Sends a notification to a user when their account has been locked out due to too many login attempts
        /// </summary>
        /// <returns>Returns a value indicating success</returns>
        async Task<bool> IUsersService.UserLockedOutNotification(UserLockedOutCommand command)
        {
            if (string.IsNullOrEmpty(command.Email))
            {
                return false;
            }

            var emailTemplateId = Environment.GetEnvironmentVariable("GOV_NOTIFY_USER_LOCKED_OUT_EMAIL_TEMPLATEID");
            string linkUrl = Environment.GetEnvironmentVariable(Startup.EnvironmentVariable_NewUserInviteLinkBaseUrl) + "/Account/ForgottenPassword";
            var lockoutDuration = AppSettings.Value.Authentication.DefaultLockoutTimeSpan;
            var rtfoEmail = (await SystemParametersService.GetSystemParameters()).RtfoSupportEmailAddress;
            var rtfoPhone = (await SystemParametersService.GetSystemParameters()).RtfoSupportTelephoneNumber;

            Dictionary<string, dynamic> personalisation = new Dictionary<string, dynamic>
             {
                { "name", command.FullName},
                { "link", linkUrl },
                { "account-unlock-minutes", lockoutDuration },
                {"rtfo-compliance-email", rtfoEmail },
                {"rtfo-compliance-telephone", rtfoPhone },
             };

            var response = await this.Notify.SendEmail(command.Email, emailTemplateId, personalisation);

            return response != null;
        }
        #endregion IUsersService implementation

        #region Private methods

        /// <summary>
        /// Send an email notification to a user that has been added to the system
        /// </summary>
        /// <param name="isAdmin">Whether the user is an administrator</param>
        /// <param name="user">Recipient</param>
        /// <returns>true/false</returns>
        public async Task<bool> UserLinkedEmailAsync(bool isAdmin, GosApplicationNewUser user)
        {
            if (user == null || user.Email == null)
            {
                return false;
            }

            string email_template_id = isAdmin ?
                Environment.GetEnvironmentVariable("GOV_NOTIFY_ADMIN_USER_LINKED_EMAIL_TEMPLATEID") :
                Environment.GetEnvironmentVariable("GOV_NOTIFY_USER_LINKED_EMAIL_TEMPLATEID");

            Dictionary<string, dynamic> personalisation = new Dictionary<string, dynamic>
                {
                    { "name", user.FullName},
                    { "company", user.Company }
                };

            try
            {
                await this.Notify.SendEmail(user.Email, email_template_id, personalisation);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "IUsersService.InviteAsync failed to send confirmation email to " + user.Email);
                return false;
            }

            return true;

        }

        /// <summary>
        /// Sends a notification to a user informing them that a password reset has occurred
        /// </summary>
        ///<param name="user">The user to send the notification to</param>
        /// <returns>Returns a value indicating success</returns>
        private async Task<bool> SendPasswordResetNotification(GosApplicationUser user)
        {
            bool success = false;

            if (user != null)
            {
                string email_template_id = Environment.GetEnvironmentVariable(Startup.EnvironmentVariable_GovNotifyPasswordResetConfirmationEmailTemplateId);

                Dictionary<String, dynamic> personalisation = new Dictionary<String, dynamic>
                {
                    { "name", user.FullName}
                };

                var response = await this.Notify.SendEmail(user.Email, email_template_id, personalisation);
                if (response != null)
                {
                    success = true;
                }
            }

            return success;
        }

        #endregion Private Methods
    }
}
