﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Services
{
    /// <summary>
    /// Interface defining Roles service
    /// </summary>
    public interface IRolesService
    {
        /// <summary>
        /// Retrieves all Roles for the given User
        /// </summary>
        /// <param name="userId">Id of User</param>
        /// <returns>All Roles to which the specified User belongs</returns>
        Task<IList<string>> GetByUserId(string userId);
    }
}
