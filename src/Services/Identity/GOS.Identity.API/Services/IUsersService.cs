﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Commands;
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Services
{
    /// <summary>
    /// Service class for performing actions for a GOS user
    /// </summary>
    public interface IUsersService
    {
        /// <summary>
        /// Gets user by user name
        /// </summary>
        /// <param name="normalizedUserName">User name</param>
        /// <returns></returns>
        Task<GosApplicationUser> GetByUserName(string normalizedUserName);

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="normalizedEmail">user email id</param>
        /// <returns></returns>
        Task<GosApplicationUser> GetByEmail(string normalizedEmail);

        /// <summary>
        /// Gets user by id
        /// </summary>
        /// <param name="id">User identifier</param>
        /// <returns></returns>
        Task<GosApplicationUser> GetUserById(string id);

        /// <summary>
        /// Creates a new system user via invite
        /// </summary>
        /// <param name="user">The details of the user to create</param>
        /// <param name="password">The password of the user to create</param>
        /// <param name="link">The invite token for the user to create</param>
        /// <returns>Returns the ID of the user that was created</returns>
        Task<string> CreateUser(GosApplicationUserInvite user, string password, string link);

        /// <summary>
        /// Updates an user
        /// </summary>
        /// <param name="user">Gosapplication user object</param>
        /// <returns></returns>
        Task<GosApplicationUser> UpdateUser(GosApplicationUser user);

        #region Notifications

        /// <summary>
        /// Invites a user to register an account, sending out a notification
        /// </summary>
        /// <param name="user">The details of the user to invite</param>
        /// <param name="invitedByUserId">The ID of the user requesting the invite to be sent</param>
        /// <returns></returns>
        Task<bool> InviteUserToRegisterAccount(GosApplicationUserInvite user, string invitedByUserId);

        /// <summary>
        /// Sends an email when a new user account is created
        /// </summary>
        /// <param name="user">New user account being created</param>
        /// <returns>True if email sent successfully, false otherwise</returns>
        Task<bool> NewUserEmailAsync(GosApplicationNewUser user);

        /// <summary>
        /// Sends an email when a new user is invited to the system
        /// </summary>
        /// <param name="user">User being invited</param>
        /// <returns>True if email sent successfully, false otherwise</returns>
        Task<bool> NewUserLinkedEmailAsync(GosApplicationNewUser user);

        /// <summary>
        /// Sends an email when a new administrator user is invited to the system
        /// </summary>
        /// <param name="user">User being invited</param>
        /// <returns>True if email sent successfully, false otherwise</returns>
        Task<bool> NewAdminUserLinkedEmailAsync(GosApplicationNewUser user);

        /// <summary>
        /// Sends a notification to a user to enable them to reset their password
        /// </summary>
        /// <param name="command">The command with details for the password reset</param>
        /// <returns>Returns a value indicating success</returns>
        Task<bool> SendUserResetPasswordNotification(IssueForgottenPasswordNotificationCommand command);

        /// <summary>
        /// Sends a notification to a user when their account has been locked out due to too many login attempts
        /// </summary>
        /// <returns>Returns a value indicating success</returns>
        Task<bool> UserLockedOutNotification(UserLockedOutCommand command);

        #endregion 

        /// <summary>
        /// Get the user associated with organisation
        /// </summary>
        /// <param name="link"></param>
        /// <returns></returns>
        Task<GosInvitedUserAssociation> LinkAsync(string link);
        /// <summary>
        /// Assign role to the user
        /// </summary>
        /// <param name="user">Gosapplication user object</param>
        /// <returns></returns>
        Task<GosApplicationUser> AssignRoleToUserAsync(GosApplicationUser user);

        /// <summary>
        /// Assign Role to User by emailed link
        /// </summary>
        /// <param name="invitedUser">Invited user</param>
        /// <param name="organisationAssociationUserId">The ID of the user that created the organisation</param>
        /// <returns>user or null</returns>
        Task<GosApplicationUser> AssociateUserWithOrganisation(GosApplicationUserInvite invitedUser, string organisationAssociationUserId);


        /// <summary>
        /// Gets users by organisation
        /// </summary>
        /// <param name="organisationId"></param>
        /// <returns></returns>
        Task<List<GosApplicationUser>> GetUsersByOrganisationId(int organisationId);

        /// <summary>
        /// Gets user id by user registered email
        /// </summary>
        /// <param name="normalizedEmail">user email</param>
        /// <returns></returns>
        Task<string> GetUserIdByEmail(string normalizedEmail);

        /// <summary>
        /// Invites or adds a user to an organisation
        /// </summary>
        /// <param name="user">The details of the user to add</param>
        /// <param name="invitedByUserId">The ID of the user being invited</param>
        /// <returns></returns>
        Task<AddNewGosApplicationUserResult> AddUser(AddNewGosApplicationUser user, string invitedByUserId);

        /// <summary>
        /// Remove user
        /// </summary>
        /// <param name="userId">user identifier</param>
        /// <returns></returns>
        Task<bool> RemoveUser(string userId);


        /// <summary>
        /// Validates reset password token
        /// </summary>
        /// <param name="token">The password reset token</param>
        /// <returns>Returns a value indicating success</returns>
        Task<bool> ValidateResetPasswordToken(string token);

        /// <summary>
        /// Resets a user's password
        /// </summary>
        /// <param name="command">The command with details for the password reset</param>
        /// <returns>Returns a value indicating success</returns>
        Task<bool> ResetPassword(ResetPasswordCommand command);

        /// <summary>
        /// Gets admin users
        /// </summary>
        /// <returns></returns>
        Task<IList<GosApplicationUser>> GetAdminUsers();

        /// <summary>
        /// Add an administrator user
        /// </summary>
        /// <param name="userEmail">User email</param>
        /// <param name="requestedByUserId">Id of the user making the request</param>
        /// <returns>An object containing any required message and booleans as to whether the user has been made an admin and been sent an invite</returns>
        Task<AddNewAdminUserResult> AddAdminUser(string userEmail, string requestedByUserId);

        /// <summary>
        /// Removes administrator role for an user
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <returns>Boolean whether it successfully removed the users admin role</returns>
        Task<bool> RemoveAdminRole(string userId);


        /// <summary>
        /// Returns the performance data relating to registrations
        ///  - Number of invitations without an account, per month
        ///  - Number of invitations, per month
        ///  - Total number of users, per month
        /// </summary>
        /// <returns>List of the above metrics for each month</returns>
        Task<List<RegistrationsPerformanceData>> GetRegistrationPerformanceData();
    }
}
