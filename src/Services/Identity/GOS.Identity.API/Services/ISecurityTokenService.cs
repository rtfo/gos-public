﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Models;
using System.Collections.Generic;

namespace DfT.GOS.Identity.API.Services
{
    /// <summary>
    /// Definitions for a class to create JWT Security tokens
    /// </summary>
    public interface ISecurityTokenService
    {
        /// <summary>
        /// Gets a dictionary of claims for a user
        /// </summary>
        /// <param name="user">The user we're getting the claims for</param>
        /// <param name="roles">A list of the user's roles to append to the claims</param>
        /// <returns>Returns a dictionary of claims</returns>
        Dictionary<string, string> GetClaimsForUser(GosApplicationUser user, IList<string> roles);

        /// <summary>
        /// Creates a JWT Token, containing various claims about the user, for a user
        /// </summary>
        /// <param name="user">The user to create the JWT token for</param>
        /// <param name="claimsDictionary">A dictionary of security claims to add to the JWT token</param>
        /// <returns>Returns a JWT Token</returns>
        string CreateToken(GosApplicationUser user, Dictionary<string, string> claimsDictionary);

        /// <summary>
        /// Get the authorised service accounts
        /// </summary>
        /// <returns></returns>
        IList<AuthorisedService> GetServiceAccounts();

        /// <summary>
        /// Gets a dictionary of claims for a service
        /// </summary>
        /// <param name="user">The user we're getting the claims for</param>
        /// <param name="roles">A list of the user's roles to append to the claims</param>
        /// <returns>Returns a dictionary of claims</returns>
        Dictionary<string, string> GetClaimsForService(GosApplicationUser user, IList<string> roles);
    }
}
