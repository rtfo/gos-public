﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Services
{
    /// <summary>
    /// Service for getting User Role details
    /// </summary>
    public class RolesService : IRolesService
    {
        #region Private Constructors

        private IRolesRepository _repository { get; set; }

        #endregion Private Constructors

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="repository">Roles repository instance used to access data</param>
        public RolesService(IRolesRepository repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IRolesService implementation

        /// <summary>
        /// Get a list of roles for a user
        /// </summary>
        /// <param name="userId">The ID of the user to get the roles for</param>
        /// <returns>Returns a list of roles</returns>
        async Task<IList<string>> IRolesService.GetByUserId(string userId)
        {
            return await _repository.GetByUserIdAsync(userId);
        }

        #endregion IRolesService implementation
    }
}
