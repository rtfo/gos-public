﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Notify.Client;
using Notify.Models.Responses;
using Microsoft.Extensions.Logging;

namespace DfT.GOS.Identity.API.Services
{
    /// <summary>
    /// Service class for Notifications
    /// </summary>
    public partial class NotifyService : INotify
    {
        #region Private Constructors

        private string Gov_notify_url { get; set; }
        private string Gov_notify_api_key { get; set; }

        #endregion Private Constructors

        private ILogger<Startup> Logger { get; set; }

        #region Constructors

        /// <summary>
        /// Startup parameters to Notify Service
        /// </summary>
        /// <param name="logger">Logger</param>
        /// <param name="gov_notify_url">Notify Service Url</param>
        /// <param name="gov_notify_api_key">Notify Service API key</param>
        public NotifyService(ILogger<Startup> logger, string gov_notify_url, string gov_notify_api_key)
        {
            Logger = logger;
            Gov_notify_api_key = gov_notify_api_key;
            Gov_notify_url = gov_notify_url;
        }
        #endregion Constructors

        #region INotifyService implementation

        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="emailAddress">Recipient Email Address</param>
        /// <param name="templateId">Email Template Id</param>
        /// <param name="personalisation">Template Personalisation parameters</param>
        /// <param name="clientReference">Client Reference</param>
        /// <param name="emailReplyToId">Email Reply to Id</param>
        /// <returns></returns>
        public async Task<EmailNotificationResponse> SendEmail(string emailAddress, string templateId, Dictionary<string, dynamic> personalisation = null, string clientReference = null, string emailReplyToId = null)
        {
            try
            {
                NotificationClient client = new NotificationClient(Gov_notify_url, Gov_notify_api_key);
                EmailNotificationResponse reponse = await client.SendEmailAsync(emailAddress, templateId, personalisation);

                return reponse;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error in SendEmail.");          
                return null;
            }            
        }

        #endregion IUsersService implementation
    }
}
