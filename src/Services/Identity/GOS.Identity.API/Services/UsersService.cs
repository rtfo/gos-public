﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using DfT.GOS.Identity.API.Commands;
using DfT.GOS.Identity.API.Data;
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.API.Models.Configuration;
using DfT.GOS.Identity.API.Repositories;
using DfT.GOS.Identity.Common.Models;
using DfT.GOS.Security.Claims;
using DfT.GOS.SystemParameters.API.Client.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Services
{
    /// <summary>
    /// Service class for Users
    /// </summary>
    public partial class UsersService : DataServiceBase, IUsersService
    {
        #region Private Properties

        private IUsersRepository UsersRepository { get; set; }
        private IInvitesRepository InvitesRepository { get; set; }
        private IRolesRepository RolesRepository { get; set; }
        private IUsersRolesRepository UsersRolesRepository { get; set; }
        private GosIdentityDataContext DataContext { get; set; }
        private ILogger<Startup> Logger { get; set; }
        private INotify Notify { get; set; }
        private IPasswordResetRequestRepository PasswordResetRequestRepository { get; set; }
        private ISystemParametersService SystemParametersService { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }

        #endregion Private Properties

        #region Constructors

        /// <summary>
        /// Standard Constructor
        /// </summary>
        /// <param name="dataContext">Datacontext</param>
        /// <param name="logger">Logger</param>
        /// <param name="notify">Notification</param>
        /// <param name="appSettings">The application Settings</param>
        public UsersService(GosIdentityDataContext dataContext
            , ILogger<Startup> logger
            , INotify notify
            , ISystemParametersService systemParametersService
            , IOptions<AppSettings> appSettings)
            : base(dataContext)
        {
            this.UsersRepository = new UsersRepository(dataContext);
            this.InvitesRepository = new InvitesRepository(dataContext);
            this.RolesRepository = new RolesRepository(dataContext);
            this.UsersRolesRepository = new UsersRolesRepository(dataContext);
            this.PasswordResetRequestRepository = new PasswordResetRequestRepository(dataContext);
            this.DataContext = dataContext;
            this.SystemParametersService = systemParametersService;
            this.Logger = logger;
            this.Notify = notify;
            this.AppSettings = appSettings;
        }

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary> 
        public UsersService(IDbContext dataContext
            , IUsersRepository usersRepository
            , IRolesRepository rolesRepository
            , IUsersRolesRepository usersRolesRepository
            , IInvitesRepository invitesRepository
            , IPasswordResetRequestRepository passwordResetRequestRepository
            , INotify notify
            , ISystemParametersService systemParametersService
            , IOptions<AppSettings> appSettings
            , ILogger<Startup> logger)
           : base(dataContext)
        {
            this.UsersRepository = usersRepository;
            this.RolesRepository = rolesRepository;
            this.UsersRolesRepository = usersRolesRepository;
            this.InvitesRepository = invitesRepository;
            this.PasswordResetRequestRepository = passwordResetRequestRepository;
            this.Notify = notify;
            this.SystemParametersService = systemParametersService;
            this.AppSettings = appSettings;
            this.Logger = logger;
        }

        #endregion Constructors

        #region IUsersService implementation

        /// <summary>
        /// Get User By Name
        /// </summary>
        /// <param name="normalizedUserName">Uppercase User Name</param>
        /// <returns>user</returns>
        async Task<GosApplicationUser> IUsersService.GetByUserName(string normalizedUserName)
        {
            return await this.UsersRepository.GetByUserNameAsync(normalizedUserName);
        }

        /// <summary>
        /// Get User by Email
        /// </summary>
        /// <param name="normalizedEmail"></param>
        /// <returns>userId</returns>
        async Task<GosApplicationUser> IUsersService.GetByEmail(string normalizedEmail)
        {
            return await this.UsersRepository.GetByEmailAsync(normalizedEmail);
        }

        /// <summary>
        /// Get User Id By Email 
        /// </summary>
        /// <param name="normalizedEmail">uppercase email</param>
        /// <returns>userId</returns>
        async Task<string> IUsersService.GetUserIdByEmail(string normalizedEmail)
        {
            var userId = await (from r in this.DataContext.Users
                                where r.NormalizedEmail.Equals(normalizedEmail)
                                select r.Id).FirstOrDefaultAsync<string>();

            return userId;
        }
        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>user</returns>
        async Task<GosApplicationUser> IUsersService.GetUserById(string id)
        {
            return await this.UsersRepository.GetAsync(id);
        }

        /// <summary>
        /// Create User from invitation
        /// </summary>
        /// <param name="user">User information</param>
        /// <param name="password">The password for the user to create</param>
        /// <param name="link">The invite token for the user to create</param>
        /// <returns>Returns the username of the created user</returns>
        async Task<string> IUsersService.CreateUser(GosApplicationUserInvite user, string password, string link)
        {
            var invitedUser = await this.InvitesRepository.GetEmailByLinkAsync(link);
            if (invitedUser.Email == user.Email &&
                invitedUser.OrganisationId == user.OrganisationId &&
                invitedUser.Company == user.Company &&
                invitedUser.Role == user.Role &&
                invitedUser.ExpireInviteLink > DateTime.Now)
            {
                try
                {
                    if ((user.OrganisationId > 0 || user.Role == Roles.Administrator) && !string.IsNullOrEmpty(user.Company))
                    {
                        // Create User
                        var userForCreation = new GosApplicationUser
                        {
                            UserName = user.Email,
                            NormalizedUserName = user.Email.ToUpper(),
                            Email = user.Email,
                            NormalizedEmail = user.Email.ToUpper(),
                            FullName = user.FullName,
                            PasswordHash = password,
                            LockoutEnabled = true,
                            OrganisationId = user.OrganisationId,
                            Role = user.Role,
                            CreatedDate = DateTime.Now,
                            OrganisationAssociationBy = invitedUser.InvitedBy,
                            OrganisationAssociationDate = DateTime.Now,
                            SecurityStamp = Guid.NewGuid().ToString()
                        };

                        var passwordHasher = new PasswordHasher<GosApplicationUser>();
                        userForCreation.PasswordHash = passwordHasher.HashPassword(userForCreation, password);

                        await this.UsersRepository.CreateAsync(userForCreation);
                        await this.SaveAsync();

                        // Assign Role
                        var roleId = await RolesRepository.GetRoleIdByNameAsync(user.Role);
                        var userId = await UsersRepository.GetUserIdByUsernameAsync(user.Email.ToUpper());
                        await this.UsersRolesRepository.AddAsync(userId, roleId);

                        // Email
                        var userForEmail = new GosApplicationNewUser { Email = user.Email, Company = user.Company, FullName = user.FullName };
                        bool emailResponse;
                        if (user.Role == Roles.Administrator)
                        {
                            emailResponse = await this.NewAdminUserLinkedEmailAsync(userForEmail);
                        }
                        else
                        {
                            emailResponse = await this.NewUserLinkedEmailAsync(userForEmail);
                        }

                        if (!emailResponse)
                        {
                            Logger.LogInformation("User created a new account with password but email notification failed.");
                        }

                        return userId;
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex, "UsersService: CreateUser:");
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Update user information
        /// </summary>
        /// <param name="user">User information</param>
        /// <returns>result</returns>
        async Task<GosApplicationUser> IUsersService.UpdateUser(GosApplicationUser user)
        {
            //TODO: add unit tests to prove
            await this.UsersRepository.UpdateAsync(user);
            await this.SaveAsync();
            return await this.UsersRepository.GetAsync(user.Id);
        }

        /// <summary>
        /// Link User to role based on link
        /// </summary>
        /// <param name="link">link</param>
        /// <returns>Gos Organisation User Association </returns>
        async Task<GosInvitedUserAssociation> IUsersService.LinkAsync(string link)
        {
            if (link != null)
            {
                var gosOrganisationUserAssociation = await this.InvitesRepository.GetEmailByLinkAsync(link);

                if (gosOrganisationUserAssociation != null)
                {
                    var registeredUser = await this.UsersRepository.GetByEmailAsync(gosOrganisationUserAssociation.Email.ToUpper());
                    if (registeredUser != null)
                    {
                        //The user has already been registered, so the user invite is not valid
                        return null;
                    }
                }                

                return gosOrganisationUserAssociation;
            }
            else
            {
                Logger.LogError("IUsersService.LinkAsync: IUsersService.LinkAsync link is null.");
                return null;
            }
        }

        /// <summary>
        /// Assign role to user
        /// </summary>
        /// <param name="user">Gos Application User with embedded role</param>
        /// <returns></returns>
        public async Task<GosApplicationUser> AssignRoleToUserAsync(GosApplicationUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var roleId = await this.RolesRepository.GetRoleIdByNameAsync(user.Role);

            if (roleId != null)
            {
                var response = await this.UsersRolesRepository.AddAsync(user.Id, roleId);

                if (response != null)
                {
                    return user;
                }
            }

            return null;
        }

        /// <summary>
        /// Assign Role to User by emailed link
        /// </summary>
        /// <param name="invitedUser">Invited user</param>
        /// <param name="organisationAssociationUserId">The ID of the user that created the organisation</param>
        /// <returns>user or null</returns>
        public async Task<GosApplicationUser> AssociateUserWithOrganisation(GosApplicationUserInvite invitedUser
            , string organisationAssociationUserId)
        {
            //TODO: Refactor this method

            if (invitedUser == null)
            {
                throw new ArgumentNullException(nameof(invitedUser));
            }

            try
            {
                GosApplicationUser user = await UsersRepository.GetByEmailAsync(invitedUser.Email.ToUpper());
                var roleId = await RolesRepository.GetRoleIdByNameAsync(invitedUser.Role);
                await UsersRolesRepository.AddAsync(user.Id, roleId);

                user.OrganisationId = invitedUser.OrganisationId;
                user.OrganisationAssociationBy = organisationAssociationUserId;
                user.OrganisationAssociationDate = DateTime.Now;

                await UsersRepository.UpdateAsync(user);
                await SaveAsync();

                GosApplicationNewUser newUser = new GosApplicationNewUser
                {
                    Company = invitedUser.Company,
                    Email = user.Email,
                    FullName = user.FullName
                };

                await this.NewUserLinkedEmailAsync(newUser);

                return await UsersRepository.GetAsync(user.Id);

            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "UsersService: AssignRoleToUserByLink:");
                return null;
            }
        }
        /// <summary>
        /// Get Users By Organisation Id
        /// </summary>
        /// <param name="organisationId">Retrieve information by Id</param>
        /// <returns>A list of GosApplicationUser</returns>
        async Task<List<GosApplicationUser>> IUsersService.GetUsersByOrganisationId(int organisationId)
        {
            var users = await this.UsersRepository.GetUsersByOrganisationAsync(organisationId);
            return users;
        }

        /// <summary>
        /// Add user to an organisation
        /// </summary>
        /// <param name="user">AddNewGosApplicationUser</param>.
        /// <param name="invitedByUserId">The ID of the user adding the user to the organisation</param>
        /// <returns>returns true/false</returns>
        public async Task<AddNewGosApplicationUserResult> AddUser(AddNewGosApplicationUser user, string invitedByUserId)
        {
            //TODO: Refactor this method

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var addNewGosApplicationUserResult = new AddNewGosApplicationUserResult
            {
                Message = await AddUserValidationRules(user)
            };

            try
            {
                if (addNewGosApplicationUserResult.Message.Equals("")) //No Error Message - Valid Response
                {
                    GosApplicationUserInvite inviteUser = new GosApplicationUserInvite
                    {
                        Email = user.Email,
                        OrganisationId = user.OrganisationId,
                        Role = user.Role,
                        Company = user.Company
                    };

                    var existingUser = await this.UsersRepository.GetByEmailAsync(user.Email.ToUpper());

                    if (existingUser == null)
                    {
                        //If user doesn't have a user account, then send them an invite to register one
                        await this.InviteUserToRegisterAccount(inviteUser, invitedByUserId);
                        addNewGosApplicationUserResult.UserAssociatedWithOrganisation = false;
                    }
                    else
                    {
                        //If user has a user account then link their account to the organisation
                        await this.AssociateUserWithOrganisation(inviteUser, invitedByUserId);
                        addNewGosApplicationUserResult.UserAssociatedWithOrganisation = true;
                    }
                }
                return addNewGosApplicationUserResult;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "AddUser:");
                addNewGosApplicationUserResult.Message = "Error";
                return addNewGosApplicationUserResult;
            }
        }

        /// <summary>
        /// Remove user
        /// </summary>
        /// <param name="userId">Gos application user</param>
        /// <returns>returns true/false</returns>
        public async Task<bool> RemoveUser(string userId)
        {
            try
            {
                var updateObject = await DataContext.Users
                    .Where(u => u.Id == userId)
                    .SingleOrDefaultAsync();
                updateObject.OrganisationId = null;
                updateObject.Role = null;
                updateObject.OrganisationAssociationBy = null;
                updateObject.OrganisationAssociationDate = null;
                DataContext.Users.Update(updateObject);
                await DataContext.SaveChangesAsync();

                var deleteObject = await DataContext.UserRoles
                    .Where(u => u.UserId == userId)
                    .SingleOrDefaultAsync();
                DataContext.Remove(deleteObject);

                await DataContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "RemoveUser:");
                return false;
            }
        }

        /// <summary>
        /// Validates reset password token
        /// </summary>
        /// <param name="token">The token for the password reset</param>
        /// <returns>Returns a value indicating success</returns>
        async Task<bool> IUsersService.ValidateResetPasswordToken(string token)
        {
            var passwordResetRequest = await this.PasswordResetRequestRepository.Get(token);
            if (passwordResetRequest == null)
            {
                //An invalid password reset code has been supplied
                this.Logger.LogWarning(string.Format("An invalid Password request has been made using the following reset code {0}", token));
            }
            else if (DateTime.Now >= passwordResetRequest.Expires)
            {
                //An expired password reset has been supplied
                this.Logger.LogWarning(string.Format("An expired Password request has been made using following reset code {0}. The code expired on {1}"
                    , token, passwordResetRequest.Expires.ToString()));
            }
            else
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Resets a user's password
        /// </summary>
        /// <param name="command">The command with details for the password reset</param>
        /// <returns>Returns a value indicating success</returns>
        async Task<bool> IUsersService.ResetPassword(ResetPasswordCommand command)
        {
            var success = false;

            var passwordResetRequest = await this.PasswordResetRequestRepository.Get(command.ResetCode);

            if (passwordResetRequest == null)
            {
                //An invalid password reset code has been supplied
                this.Logger.LogWarning(string.Format("An invalid Password request has been made using the email address {0} and the reset code {1}", command.EmailAddress, command.ResetCode));
            }
            else if (DateTime.Now >= passwordResetRequest.Expires)
            {
                //An expired password reset has been supplied
                this.Logger.LogWarning(string.Format("An expired Password request has been made using the email address {0} and the reset code {1}. The code expired on {2}"
                    , command.EmailAddress, command.ResetCode, passwordResetRequest.Expires.ToString()));
            }
            else
            {
                var user = await this.UsersRepository.GetAsync(passwordResetRequest.UserId);

                //Check that the correct email address was supplied (for extra security)
                if (user != null
                    && user.NormalizedEmail == command.EmailAddress.Trim().ToUpper())
                {
                    var passwordHasher = new PasswordHasher<GosApplicationUser>();
                    user.PasswordHash = passwordHasher.HashPassword(user, command.NewPassword);

                    await this.UsersRepository.UpdateAsync(user);
                    var recordsUpdated = await this.SaveAsync();

                    if (recordsUpdated == 1)
                    {
                        await this.SendPasswordResetNotification(user);
                        success = true;
                    }
                }
                else
                {
                    this.Logger.LogWarning(string.Format("The supplied password reset code {0} did not correlate to a user with email address {1}"
                        , command.ResetCode, command.EmailAddress));
                }
            }

            return success;
        }

        /// <summary>
        /// Returns Admin users
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GosApplicationUser>> GetAdminUsers()
        {
            var adminRole = await DataContext.Roles
                .FirstOrDefaultAsync(x => x.Name == Roles.Administrator);

            var userRoles = await DataContext.UserRoles
                .Where(x => x.RoleId == adminRole.Id).Select(x => x.UserId)
                .ToListAsync();
            var users = await DataContext.Users
                .Where(x => userRoles.Contains(x.Id))
                .ToListAsync();
            return users;
        }

        /// <summary>
        /// Add an administrator user
        /// </summary>
        /// <param name="userEmail">User email</param>
        /// <param name="requestedByUserId">Id of the user making the request</param>
        /// <returns>An object containing any required message and booleans as to whether the user has been made an admin and been sent an invite</returns>
        public async Task<AddNewAdminUserResult> AddAdminUser(string userEmail, string requestedByUserId)
        {
            var user = await this.UsersRepository.GetByEmailAsync(userEmail.ToUpper());
            IList<string> roles;
            if (user == null)
            {
                //Add the user and send them an invite as they don't have a GOS account
                await this.AddUser(new AddNewGosApplicationUser
                {
                    Email = userEmail,
                    RequestedByUserId = requestedByUserId,
                    Role = Roles.Administrator,
                    Company = "None",
                    OrganisationId = null
                }, requestedByUserId);
                return new AddNewAdminUserResult
                {
                    Message = userEmail + " invited as administrator.",
                    IsNewAdministrator = true,
                    UserInvited = true
                };
            }
            else if (user.OrganisationId != null)
            {
                //Reject the request as a user can't be associated to an organisation and be an administrator
                return new AddNewAdminUserResult
                {
                    Message = userEmail + " cannot be added as administrator. User is associated with an organisation. Go to 'Manage Users' for their organisation and remove them first.",
                    IsNewAdministrator = false,
                    UserInvited = false,
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
            }
            else if ((roles = await this.RolesRepository.GetByUserIdAsync(user.Id)) != null && roles.Contains(Roles.Administrator))
            {
                //Reject the request if the user is already an administrator
                return new AddNewAdminUserResult
                {
                    Message = "User " + userEmail + " is already an administrator",
                    IsNewAdministrator = false,
                    UserInvited = false,
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
            }
            else
            {
                //Check/get the id of the admin role in the system
                var adminRole = await this.RolesRepository.GetRoleIdByNameAsync(Roles.Administrator);
                if (adminRole == null)
                {
                    return new AddNewAdminUserResult
                    {
                        Message = "Unable to find administrator role.",
                        IsNewAdministrator = false,
                        UserInvited = false,
                        StatusCode = System.Net.HttpStatusCode.InternalServerError
                    };
                }

                //Make the user an administrator if they have an account, aren't already an admin or associated to an organisation
                var addUserRole = this.UsersRolesRepository.AddAsync(user.Id, adminRole);
                user.Role = Roles.Administrator;
                var updateUser = this.UsersRepository.UpdateAsync(user);
                await Task.WhenAll(addUserRole, updateUser);
                await this.SaveAsync();

                //Send them an email to let them know they're an administrator
                _ = this.NewAdminUserLinkedEmailAsync(new GosApplicationNewUser
                {
                    Email = user.Email,
                    FullName = user.FullName
                });

                return new AddNewAdminUserResult
                {
                    Message = "Added " + userEmail + " as an administrator",
                    IsNewAdministrator = true,
                    UserInvited = false
                };
            }

        }

        /// <summary>
        /// Removes administrator role for an user
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <returns>Boolean whether it successfully removed the users admin role</returns>
        public async Task<bool> RemoveAdminRole(string userId)
        {
            if (await DataContext.UserRoles.CountAsync(x => x.UserId == userId) > 0)
            {
                var adminRole = await DataContext.Roles.FirstOrDefaultAsync(x => x.Name == Roles.Administrator);
                if (adminRole != null)
                {
                    DataContext.UserRoles.Remove(new IdentityUserRole<string>
                    { UserId = userId, RoleId = adminRole.Id });
                }
            }
            return await DataContext.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// Returns the performance data relating to registrations
        ///  - Number of invitations without an account, per month
        ///  - Number of invitations, per month
        ///  - Total number of users, per month
        /// </summary>
        /// <returns>List of the above metrics for each month</returns>
        public async Task<List<RegistrationsPerformanceData>> GetRegistrationPerformanceData()
        {
            var earliestDate = await this.UsersRepository.GetFirstRegistrationOrInviteDate();
            var startMonth = new DateTime(earliestDate.Year, earliestDate.Month, 1);

            var registrationList = new List<RegistrationsPerformanceData>();
            registrationList = await this.GetRegistrationsSince(startMonth, registrationList);

            return registrationList;
        }

        #endregion IUsersService implementation

        #region Private methods

        /// <summary>
        /// Go through each month between the month of the date provided and todays month
        /// and get the registrations performance data
        /// </summary>
        /// <param name="startMonth">Month to start from</param>
        /// <param name="registrations">List of the data found so far</param>
        /// <returns>List of the registrations performance data</returns>
        private async Task<List<RegistrationsPerformanceData>> GetRegistrationsSince(DateTime startMonth, List<RegistrationsPerformanceData> registrations)
        {
            if (DateTime.Compare(startMonth, DateTime.Now) > 0)
            {
                throw new ArgumentException("Start month can't be over today's date");
            }

            //Set the end date to the end of the month, unless we're looking at the current month, then use todays date
            DateTime endDate = startMonth.Month.Equals(DateTime.Now.Month) && startMonth.Year.Equals(DateTime.Now.Year) ? DateTime.Now : startMonth.AddMonths(1).AddMilliseconds(-1);

            var totalUserTask = await this.UsersRepository.GetUserCountForMonth(startMonth, endDate);
            var invitationsTask = await this.UsersRepository.GetInvitationCountForMonth(startMonth, endDate);
            var invitationsWitoutAccountTask = await this.UsersRepository.GetUnclaimedInvitationCount(startMonth, endDate);

            // NB: We are awaiting the repository calls individually here to avoid the following error noted during Integration testing
            // System.InvalidOperationException: A second operation started on this context before a previous operation completed. This is usually caused by different threads using the same instance of DbContext.

            var registrationsData = new RegistrationsPerformanceData(startMonth, invitationsWitoutAccountTask, invitationsTask, totalUserTask);

            registrations.Add(registrationsData);

            // Check if the month that we just got data for is this month and in this year
            if (DateTime.Now.Year.Equals(startMonth.Year) && DateTime.Now.Month.Equals(startMonth.Month))
            {
                return registrations;
            }
            else
            {
                // As it's not move on to the next month
                return await this.GetRegistrationsSince(startMonth.AddMonths(1), registrations);
            }
        }

        /// <summary>
        /// Add User Validation Rules
        /// </summary>
        /// <param name="user">Validate new user</param>
        /// <returns>Validation Message</returns>
        private async Task<string> AddUserValidationRules(AddNewGosApplicationUser user)
        {
            string message = string.Empty;

            var gosApplicationUser = await this.UsersRepository.GetByEmailAsync(user.Email.ToUpper());
            if (gosApplicationUser != null)
            {
                var userRoles = await this.RolesRepository.GetByUserIdAsync(gosApplicationUser.Id);
                if (userRoles.Contains(Roles.Administrator))
                {
                    message = "You cannot add a user with the Administrator role to an organisation";
                }
                else if (gosApplicationUser.OrganisationId != null)
                {
                    message = "A user with this email address is already linked to an organisation.";
                }
                else
                { // No message - this is a valid new user that does not exist in this organisation
                    message = "";
                }
            }
            else
            {// No message - this is a valid new user
                message = "";
            }
            return message;
        }
        #endregion
    }
}
 