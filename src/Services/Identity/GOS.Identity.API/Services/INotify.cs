﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Notify.Models.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Services
{
    /// <summary>
    /// Notifications Interface
    /// </summary>
    public interface INotify
    {
        /// <summary>
        /// Send an email notification
        /// </summary>
        /// <param name="emailAddress">Email address to which email should be sent</param>
        /// <param name="templateId">Id of email template to use</param>
        /// <param name="personalisation">Custom details to include in email</param>
        /// <param name="clientReference">Client Reference for email</param>
        /// <param name="emailReplyToId">Reploy To email address</param>
        /// <returns>Outcome of sending email</returns>
        Task<EmailNotificationResponse> SendEmail(string emailAddress, string templateId, Dictionary<string, dynamic> personalisation = null, string clientReference = null, string emailReplyToId = null);
    }
}
