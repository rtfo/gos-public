﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.API.Models.Configuration;
using DfT.GOS.Security.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace DfT.GOS.Identity.API.Services
{
    /// <summary>
    /// Provides a service for creating JWT Tokens
    /// </summary>
    public class SecurityTokenService : ISecurityTokenService
    {
        #region Properties

        private SecurityTokenConfiguration Configuration { get; set; }
        private IConfiguration AppConfiguration { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all 
        /// </summary>
        public SecurityTokenService(SecurityTokenConfiguration configuration
            , IConfiguration appconfiguration)
        {
            this.Configuration = configuration;
            this.AppConfiguration = appconfiguration;
        }

        #endregion Constructors

        #region ISecurityTokenService implementation

        /// <summary>
        /// Creates a JWT Token, containing various claims about the user, for a user
        /// </summary>
        /// <param name="user">The user to create the JWT token for</param>
        /// <param name="claimsDictionary">A dictionary of security claims to add to the JWT token</param>
        /// <returns>Returns a JWT Token</returns>
        string ISecurityTokenService.CreateToken(GosApplicationUser user, Dictionary<string, string> claimsDictionary)
        {
            var tokenAudience = Configuration.JwtTokenAudience;
            var tokenIssuer = Configuration.JwtTokenIssuer;
            var tokenKey = Encoding.UTF8.GetBytes(Configuration.JwtTokenKey);

            var key = new SymmetricSecurityKey(tokenKey);

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>();
            foreach (var claimEntry in claimsDictionary)
            {
                claims.Add(new Claim(claimEntry.Key, claimEntry.Value));
            }

            var token = new JwtSecurityToken(
                issuer: tokenIssuer,
                audience: tokenAudience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(Configuration.JwtTimeoutMinutes),
                signingCredentials: credentials
                );

            return (new JwtSecurityTokenHandler().WriteToken(token)).ToString();
        }

        /// <summary>
        /// Gets a dictionary of claims for a user
        /// </summary>
        /// <param name="user">The user we're getting the claims for</param>
        /// <param name="roles">A list of the user's roles to append to the claims</param>
        /// <returns>Returns a dictionary of claims</returns>
        Dictionary<string, string> ISecurityTokenService.GetClaimsForUser(GosApplicationUser user, IList<string> roles)
        {
            var claims = new Dictionary<string, string>();
            claims.Add(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()); //JWT ID
            claims.Add(Claims.SecurityStamp, user.SecurityStamp);

            //Add roles to enable role based authentication
            foreach (var role in roles)
            {
                claims.Add(ClaimTypes.Role, role);
            }

            claims.Add(Claims.UserId, user.Id);
            if (user.OrganisationId.HasValue)
            {
                claims.Add(Claims.OrganisationId, user.OrganisationId.Value.ToString());
            }

            return claims;
        }

        /// <summary>
        /// Get the authorised service accounts
        /// </summary>
        /// <returns></returns>
        IList<AuthorisedService> ISecurityTokenService.GetServiceAccounts()
        {
            return JsonConvert.DeserializeObject<IList<AuthorisedService>>(
                AppConfiguration[Startup.EnvironmentVariable_AuthorisedServices]);
        }

        /// <summary>
        /// Gets a dictionary of claims for a user
        /// </summary>
        /// <param name="user">The user we're getting the claims for</param>
        /// <param name="roles">A list of the user's roles to append to the claims</param>
        /// <returns>Returns a dictionary of claims</returns>
        Dictionary<string, string> ISecurityTokenService.GetClaimsForService(GosApplicationUser user, IList<string> roles)
        {
            var claims = new Dictionary<string, string>();
            claims.Add(JwtRegisteredClaimNames.Sub, user.UserName); //Subject
            claims.Add(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()); //JWT ID
            claims.Add(ClaimTypes.NameIdentifier, user.UserName);
            claims.Add(ClaimTypes.Name, user.UserName);

            //Add roles to enable role based authentication
            foreach (var role in roles)
            {
                claims.Add(ClaimTypes.Role, role);
            }
            claims.Add(Claims.UserId, user.Id);
            return claims;
        }

        #endregion ISecurityTokenService implementation
    }
}
