﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DfT.GOS.HealthChecks.Common;
using DfT.GOS.HealthChecks.Common.Extensions;
using DfT.GOS.Identity.API.Data;
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.API.Models.Configuration;
using DfT.GOS.Identity.API.Repositories;
using DfT.GOS.Identity.API.Services;
using DfT.GOS.Security.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using DfT.GOS.SystemParameters.API.Client.Services;
using DfT.GOS.Http;
using Microsoft.Extensions.Caching.Memory;

namespace DfT.GOS.Identity.API
{
    /// <summary>
    /// Startup for Identity API
    /// </summary>
    public class Startup
    {
        #region Constants

        /// <summary>
        /// Environment variable name for the database connection string
        /// </summary>
        public const string EnvironmentVariable_ConnectionString = "DB_CONNECTION_STRING";

        /// <summary>
        /// Environmental variable name for the initial administrator email address (to seed the database)
        /// </summary>
        public const string EnvironmentVariable_AdminEmailAddress = "ADMIN_EMAIL_ADDRESS";

        /// <summary>
        /// Environmental variable name for the initial administrator password (to seed the database)
        /// </summary>
        public const string EnvironmentVariable_AdminPassword = "ADMIN_PASSWORD";

        /// <summary>
        /// Environmental variable name for the Gov.UK notify URL
        /// </summary>
        public const string EnvironmentVariable_GovUkNotifyUrl = "GOV_NOTIFY_URL";

        /// <summary>
        /// Environmental variable name for the Gov.UK notify API key
        /// </summary>
        public const string EnvironmentVariable_GovUkNotifyApiKey = "GOV_NOTIFY_API_KEY";

        /// <summary>
        /// The environment variable for the security JWT Token Audience used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenAudience = "JWT_TOKEN_AUDIENCE";

        /// <summary>
        /// The environment variable for the security JWT Token Issuer used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenIssuer = "JWT_TOKEN_ISSUER";

        /// <summary>
        /// The environment variable for the security JWT Token Key used for Auth
        /// </summary>
        public const string EnvironmentVariable_JwtTokenKey = "JWT_TOKEN_KEY";

        /// <summary>
        /// Environmental variable name for the Gov.UK email template ID to use when inviting new users to GOS
        /// </summary>
        public const string EnvironmentVariable_GovUkNewUserInviteEmailTemplateId = "GOV_NOTIFY_NEW_USER_INVITE_EMAIL_TEMPLATEID";

        /// <summary>
        /// Environmental variable name for the Gov.UK email template ID to use when a new user is added in GOS
        /// </summary>
        public const string EnvironmentVariable_GovUkNewUserCreatedEmailTemplateId = "GOV_NOTIFY_NEW_USER_CREATED_EMAIL_TEMPLATEID";

        /// <summary>
        /// Environmental variable name for the Gov.UK email template ID to use when a user is linked to an organisation
        /// </summary>
        public const string EnvironmentVariable_GovUkUserLinkedToCompanyEmailTemplateId = "GOV_NOTIFY_USER_LINKED_EMAIL_TEMPLATEID";

        /// <summary>
        /// Environmental variable name for the Gov.UK email template ID to use when a user is made an administrator
        /// </summary>
        public const string EnvironmentVariable_GovUkAdminUserEmailTemplateId = "GOV_NOTIFY_ADMIN_USER_LINKED_EMAIL_TEMPLATEID";

        /// <summary>
        /// Environmental variable name for the Gov.UK email template ID to use when a password reset is requested
        /// </summary>
        public const string EnvironmentVariable_GovNotifyPasswordResetRequestEmailTemplateId = "GOV_NOTIFY_PASSWORD_RESET_REQUEST_EMAIL_TEMPLATEID";

        /// <summary>
        /// Environmental variable name for the Gov.UK email template ID to use when a password reset confirmation is made
        /// </summary>
        public const string EnvironmentVariable_GovNotifyPasswordResetConfirmationEmailTemplateId = "GOV_NOTIFY_PASSWORD_RESET_CONFIRMATION_EMAIL_TEMPLATEID";

        /// <summary>
        /// Environment variable name for the Gov.UK email template ID to use when a user has been locked out of their account due to invalid logins
        /// </summary>
        public const string EnvironmentVariable_GovNotifyUserLockedOutEmailTemplateId = "GOV_NOTIFY_USER_LOCKED_OUT_EMAIL_TEMPLATEID";

        /// <summary>
        /// Environmental variable name for the base URL for invite email links
        /// </summary>
        public const string EnvironmentVariable_NewUserInviteLinkBaseUrl = "NEW_USER_INVITE_LINK_BASE_URL";

        /// <summary>
        /// Environmental variable name for the Authorised services
        /// </summary>
        public const string EnvironmentVariable_AuthorisedServices = "AUTHORISED_SERVICES";
        /// <summary>
        /// The exception message to throw when connection string is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_ConnectionStringNotSupplied = "Unable to connect to the Identity datasource, the connection string was not specified. Missing variable: " + EnvironmentVariable_ConnectionString;

        /// <summary>
        /// The exception message to throw when Admin Email Address is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_AdminEmailAddressNotSupplied = "Admin Email Address was not specified in the environment variables. Missing variable: " + EnvironmentVariable_AdminEmailAddress;

        /// <summary>
        /// The exception message to throw when Admin Password is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_AdminPasswordNotSupplied = "Admin Password was not specified in the environment variables. Missing variable: " + EnvironmentVariable_AdminPassword;

        /// <summary>
        /// The exception message to throw when Gov.UK notify URL is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_GovUkNotifyUrlNotSupplied = "Gov.uk Notify URL was not specified in the environment variables. Missing variable: " + EnvironmentVariable_GovUkNotifyUrl;

        /// <summary>
        /// The exception message to throw when Gov.UK notify API key is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_GovUkApiKeyNotSupplied = "Gov.uk Notify API Key was not specified in the environment variables. Missing variable: " + EnvironmentVariable_GovUkNotifyApiKey;

        /// <summary>
        /// The exception message to throw when Gov.uk New User Invite Email Template Id is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_GovUkNewUserInviteEmailTemplateIdNotSupplied = "Gov.uk New User Invite Email Template Id was not specified in the environment variables. Missing variable: " + EnvironmentVariable_GovUkNewUserInviteEmailTemplateId;

        /// <summary>
        /// The exception message to throw when Gov.uk User linked to organisation Email Template Id is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_GovUkUserLinkedToCompanyEmailTemplateIdNotSupplied = "Gov.uk User linked to company Email Template Id was not specified in the environment variables. Missing variable: " + EnvironmentVariable_GovUkUserLinkedToCompanyEmailTemplateId;

        /// <summary>
        /// The exception message to throw when Gov.uk admin user Email Template Id is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_GovUkAdminUserEmailTemplateIdNotSupplied = "Gov.uk Admin user Email Template Id was not specified in the environment variables. Missing variable: " + EnvironmentVariable_GovUkAdminUserEmailTemplateId;

        /// <summary>
        /// The exception message to throw when Gov.uk new user is created Email Template Id is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_GovUkNewUserCreatedEmailTemplateIdNotSupplied = "Gov.uk new user created Email Template Id was not specified in the environment variables. Missing variable: " + EnvironmentVariable_GovUkNewUserCreatedEmailTemplateId;

        /// <summary>
        /// The exception message to throw when the password reset email template Id is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_GovNotifyPasswordResetRequestEmailTemplateIdNotSupplied = "Password Reset Email Template Id was not specified in the environment variables. Missing variable: " + EnvironmentVariable_GovNotifyPasswordResetRequestEmailTemplateId;

        /// <summary>
        /// The exception message to throw when the password reset confirmation email template Id is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_GovNotifyPasswordResetConfirmationEmailTemplateIdNotSupplied = "Password Reset confirmation Email Template Id was not specified in the environment variables. Missing variable: " + EnvironmentVariable_GovNotifyPasswordResetConfirmationEmailTemplateId;

        /// <summary>
        /// The exception message to throw when the password reset confirmation email template Id is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_GovNotifyUserLockedOutEmailTemplateIdNotSupplied = "Password Reset confirmation Email Template Id was not specified in the environment variables. Missing variable: " + EnvironmentVariable_GovNotifyUserLockedOutEmailTemplateId;
        
        /// <summary>
        /// The exception message to throw when New User Invite Link Base Url is not passed in the environment variables
        /// </summary>
        public const string ErrorMessage_NewUserInviteLinkBaseUrlNotSupplied = "New User Invite Link Base Url was not specified in the environment variables. Missing variable: " + EnvironmentVariable_NewUserInviteLinkBaseUrl;

        /// <summary>
        /// The error message to throw when no JWT Token Audience environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenAudience = "Unable to get the JWT Token Audience from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenAudience;

        /// <summary>
        /// The error message to throw when no JWT Token Issuer environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenIssuer = "Unable to get the JWT Token Issuer from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenIssuer;

        /// <summary>
        /// The error message to throw when no JWT Token Key environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_JwtTokenKey = "Unable to get the JWT Token Key from the enironmental veriables.  Missing variable: " + EnvironmentVariable_JwtTokenKey;

        /// <summary>
        /// The error message to throw when no Authorised service environmental variable is supplied
        /// </summary>
        public const string ErrorMessage_AuthoriseServices = "Uuable to get the Authorised services key from the environmental variables. Missing variable: " + EnvironmentVariable_AuthorisedServices;
        #endregion Constants

        #region Properties

        private ILogger<Startup> Logger { get; set; }

        /// <summary>
        /// Gets the configuration settings for use by the API
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="configuration">The configuration settings to use</param>
        /// <param name="logger">The logger to use</param>
        public Startup(IConfiguration configuration
            , ILogger<Startup> logger)
        {
            this.Logger = logger;
            this.Logger.LogInformation("Startup called for new instance of DfT.GOS.Identity.API");
            Configuration = configuration;
        }

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            //If the solution configuration is set to Migration skip the validation
#if (!Migration)
            this.ValidateEnvironmentVariables();
#endif
            var appSettings = Configuration.Get<AppSettings>();

            //Setup Identity / JWT for authentication
            services.AddIdentity<GosApplicationUser, GosRole>(options =>
            {
                // Lockout settings
                options.Lockout.AllowedForNewUsers = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(appSettings.Authentication.DefaultLockoutTimeSpan);
                options.Lockout.MaxFailedAccessAttempts = appSettings.Authentication.MaxFailedAccessAttempts;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequiredLength = 8;
            })
            .AddDefaultTokenProviders();

            //Configure ASP.Net Core Identity implementations
            services.AddTransient<IUserStore<GosApplicationUser>, GosUserManager>();
            services.AddTransient<IRoleStore<GosRole>, GosUserManager>();
            services.AddTransient<IUserRoleStore<GosApplicationUser>, GosUserManager>();

            //Add health check to probe the DB
            services.AddHealthChecks()
                .AddDbContextCheck<GosIdentityDataContext>(name: "postgres", tags: new[] { HealthCheckTypes.Readiness });

            //Configure the db context to use postgresql
            services.AddDbContext<GosIdentityDataContext>(options =>
            {
                //
                // To create/manage migrations select the "Migrations" solution configuration
#if (Migration)
                options.UseNpgsql("Test");
#else
                options.UseNpgsql(Configuration["DB_CONNECTION_STRING"]
                    , npgsqlOptionsAction: sqlOptions =>
                    {
                        sqlOptions.EnableRetryOnFailure();
                    });
#endif
            });

            // Add application services.
            services.Configure<AppSettings>(Configuration);

            // - Add application Extensions
            string gov_notify_url = Environment.GetEnvironmentVariable("GOV_NOTIFY_URL");
            string gov_notify_api_key = Environment.GetEnvironmentVariable("GOV_NOTIFY_API_KEY");
            services.AddTransient<INotify, NotifyService>(s => new NotifyService(Logger, gov_notify_url, gov_notify_api_key));

            // - Resolve Repository References
            services.AddTransient<IUsersRepository, UsersRepository>();
            services.AddTransient<IRolesRepository, RolesRepository>();
            services.AddTransient<IInvitesRepository, InvitesRepository>();

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetAbsoluteExpiration(TimeSpan.FromSeconds(appSettings.HttpClient.HttpClientCacheTimeout));
            services.AddSingleton(typeof(MemoryCacheEntryOptions), cacheEntryOptions);

            // Configure Services / Resiliant HttpClient for service calls
            var httpConfigurer = new HttpClientConfigurer(appSettings.HttpClient, this.Logger);

            // - Resolve Service References
            httpConfigurer.Configure(services.AddHttpClient<ISystemParametersService, SystemParametersService>(config => { config.BaseAddress = new Uri(appSettings.SystemParametersServiceApiUrl); }));
            services.AddTransient<IUsersService, UsersService>();
            services.AddTransient<IRolesService, RolesService>();

            var securityTokenConfiguration = new SecurityTokenConfiguration(Configuration[Startup.EnvironmentVariable_JwtTokenAudience]
                , Configuration[Startup.EnvironmentVariable_JwtTokenIssuer]
                , Configuration[Startup.EnvironmentVariable_JwtTokenKey]
                , appSettings.JwtTokenExpiryMinutes);

            services.AddTransient<ISecurityTokenService, SecurityTokenService>(s => new SecurityTokenService(securityTokenConfiguration, Configuration));

            //JWT Security Authentication
            var jwtSettings = new JwtAuthenticationSettings(Configuration[EnvironmentVariable_JwtTokenIssuer]
                , Configuration[EnvironmentVariable_JwtTokenAudience]
                , Configuration[EnvironmentVariable_JwtTokenKey]);

            var jwtConfigurer = new JwtAuthenticationConfigurer(jwtSettings);
            jwtConfigurer.Configure(services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }));

            // Register the Swagger generator
            try
            {
                //Swagger required services
                services.AddSingleton<IApiDescriptionGroupCollectionProvider, ApiDescriptionGroupCollectionProvider>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Identity API", Version = "v1", Description = "Identity API is used to manage Application users." });
                    c.IncludeXmlComments(xmlPath);
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme { In = ParameterLocation.Header, Description = "Please enter JWT with Bearer into field in the format 'Bearer [JWT token]'", Name = "Authorization", Type = SecuritySchemeType.ApiKey });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement() {
                        {
                            new OpenApiSecurityScheme {
                                Reference = new OpenApiReference {
                                    Id = "Bearer"
                                    , Type = ReferenceType.SecurityScheme
                                },
                                Scheme = "bearer",
                                Name = "security",
                                In = ParameterLocation.Header
                            }, new List<string>()
                        }
                    });
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Register Swagger generator because of the following exception: " + ex.ToString());
            }

            services.AddControllers().AddNewtonsoftJson();
            services.AddMemoryCache();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app
            , IHostEnvironment env
            , ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            try
            {
                //Enable Health Checks
                app.UseGosHealthChecks();

                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Identity V1");
                    c.RoutePrefix = string.Empty;
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Configure Swagger because of the following exception: " + ex.ToString());
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Validates that all expected environment variables have been supplied
        /// </summary>
        private void ValidateEnvironmentVariables()
        {
            //Connection String
            if (Configuration[EnvironmentVariable_ConnectionString] == null)
            {
                throw new NullReferenceException(ErrorMessage_ConnectionStringNotSupplied);
            }

            //Admin Email Address
            if (Configuration[EnvironmentVariable_AdminEmailAddress] == null)
            {
                throw new NullReferenceException(ErrorMessage_AdminEmailAddressNotSupplied);
            }

            //Admin Password
            if (Configuration[EnvironmentVariable_AdminPassword] == null)
            {
                throw new NullReferenceException(ErrorMessage_AdminPasswordNotSupplied);
            }

            //Gov UK Notify Url
            if (Configuration[EnvironmentVariable_GovUkNotifyUrl] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovUkNotifyUrlNotSupplied);
            }

            //Gov UK Notify API key
            if (Configuration[EnvironmentVariable_GovUkNotifyApiKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovUkApiKeyNotSupplied);
            }

            //Gov UK New User Invite Email Template Id
            if (Configuration[EnvironmentVariable_GovUkNewUserInviteEmailTemplateId] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovUkNewUserInviteEmailTemplateIdNotSupplied);
            }

            //Gov UK User linked to organisation Email Template Id
            if (Configuration[EnvironmentVariable_GovUkUserLinkedToCompanyEmailTemplateId] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovUkUserLinkedToCompanyEmailTemplateIdNotSupplied);
            }

            //Gov UK Admin user Email Template Id
            if (Configuration[EnvironmentVariable_GovUkAdminUserEmailTemplateId] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovUkAdminUserEmailTemplateIdNotSupplied);
            }

            //Gov UK New user created Email Template Id
            if (Configuration[EnvironmentVariable_GovUkNewUserCreatedEmailTemplateId] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovUkNewUserCreatedEmailTemplateIdNotSupplied);
            }

            //Gov UK Password Reset email Template Id
            if (Configuration[EnvironmentVariable_GovNotifyPasswordResetRequestEmailTemplateId] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovNotifyPasswordResetRequestEmailTemplateIdNotSupplied);
            }

            //Gov UK Password Reset confirmation email Template Id
            if (Configuration[EnvironmentVariable_GovNotifyPasswordResetConfirmationEmailTemplateId] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovNotifyPasswordResetConfirmationEmailTemplateIdNotSupplied);
            }

            //Gov UK user locked out email Template Id
            if (Configuration[EnvironmentVariable_GovNotifyUserLockedOutEmailTemplateId] == null)
            {
                throw new NullReferenceException(ErrorMessage_GovNotifyUserLockedOutEmailTemplateIdNotSupplied);
            }

            //New User invite link Base Url
            if (Configuration[EnvironmentVariable_NewUserInviteLinkBaseUrl] == null)
            {
                throw new NullReferenceException(ErrorMessage_NewUserInviteLinkBaseUrlNotSupplied);
            }

            //JWT Token Audience
            if (Configuration[EnvironmentVariable_JwtTokenAudience] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenAudience);
            }

            //JWT Token Issuer
            if (Configuration[EnvironmentVariable_JwtTokenIssuer] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenIssuer);
            }

            //JWT Token Key
            if (Configuration[EnvironmentVariable_JwtTokenKey] == null)
            {
                throw new NullReferenceException(ErrorMessage_JwtTokenKey);
            }

            //Authorized Services
            if (Configuration[EnvironmentVariable_AuthorisedServices] == null)
            {
                throw new NullReferenceException(ErrorMessage_AuthoriseServices);
            }
        }

        #endregion Private Methods
    }
}
