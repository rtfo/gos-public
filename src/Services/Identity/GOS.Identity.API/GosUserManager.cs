﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.API.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.IUserStore{T}" />
    /// <seealso cref="Microsoft.AspNetCore.Identity.IUserPasswordStore{T}" />
    /// <seealso cref="Microsoft.AspNetCore.Identity.IUserEmailStore{T}" />
    /// <seealso cref="Microsoft.AspNetCore.Identity.IRoleStore{T}" />
    /// <seealso cref="Microsoft.AspNetCore.Identity.IUserRoleStore{T}" />
    /// <seealso cref="Microsoft.AspNetCore.Identity.IUserLockoutStore{T}" />
    public class GosUserManager : IUserStore<GosApplicationUser>
       , IUserPasswordStore<GosApplicationUser>
       , IUserEmailStore<GosApplicationUser>
       , IRoleStore<GosRole>
       , IUserRoleStore<GosApplicationUser>
       , IUserLockoutStore<GosApplicationUser>
        , IUserSecurityStampStore<GosApplicationUser>
    {
        #region Properties

        private IUsersService UsersService { get; set; }
        private IRolesService RolesService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        public GosUserManager(IUsersService usersService, 
            IRolesService rolesService)
        {
            this.UsersService = usersService;
            this.RolesService = rolesService;
        }

        #endregion Constructors

        #region IUserStore Implementation

        /// <summary>
        /// Handles creation of a new user
        /// </summary>
        /// <param name="user">The user to create</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<IdentityResult> IUserStore<GosApplicationUser>.CreateAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            //TODO: implement
            throw new NotImplementedException();
            //string content = await this.IdentityService.CreateUserAsync(user, cancellationToken);
            //return IdentityResult.Success;
        }

        Task<IdentityResult> IUserStore<GosApplicationUser>.DeleteAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Finds a user by Id
        /// </summary>
        /// <param name="userId">The ID of the user to find</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<GosApplicationUser> IUserStore<GosApplicationUser>.FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return this.UsersService.GetUserById(userId);
        }

        Task<GosApplicationUser> IUserStore<GosApplicationUser>.FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return this.UsersService.GetByUserName(normalizedUserName);
        }

        Task<string> IUserStore<GosApplicationUser>.GetNormalizedUserNameAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormalizedUserName);
        }

        Task<string> IUserStore<GosApplicationUser>.GetUserIdAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id);
        }

        Task<string> IUserStore<GosApplicationUser>.GetUserNameAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        Task IUserStore<GosApplicationUser>.SetNormalizedUserNameAsync(GosApplicationUser user, string normalizedName, CancellationToken cancellationToken)
        {
            user.NormalizedUserName = normalizedName;
            return Task.FromResult(user);
        }

        Task IUserStore<GosApplicationUser>.SetUserNameAsync(GosApplicationUser user, string userName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        async Task<IdentityResult> IUserStore<GosApplicationUser>.UpdateAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            var result = await this.UsersService.UpdateUser(user);

            return IdentityResult.Success;
        }

        #endregion IUserStore Implementation

        #region IUserPasswordStore Implementation

        Task IUserPasswordStore<GosApplicationUser>.SetPasswordHashAsync(GosApplicationUser user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        Task<bool> IUserPasswordStore<GosApplicationUser>.HasPasswordAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IUserPasswordStore<GosApplicationUser>.GetPasswordHashAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        #endregion IUserPasswordStore Implementation

        #region IUserEmailStore implementation

        Task IUserEmailStore<GosApplicationUser>.SetEmailAsync(GosApplicationUser user, string email, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IUserEmailStore<GosApplicationUser>.GetEmailAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        Task<bool> IUserEmailStore<GosApplicationUser>.GetEmailConfirmedAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.EmailConfirmed);
        }

        Task IUserEmailStore<GosApplicationUser>.SetEmailConfirmedAsync(GosApplicationUser user, bool confirmed, CancellationToken cancellationToken)
        {
            user.EmailConfirmed = confirmed;
            return Task.FromResult(user);
        }

        /// <summary>
        /// Finds a user by normalised email
        /// </summary>
        /// <param name="normalizedEmail">The normalised email address to find the user by</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<GosApplicationUser> IUserEmailStore<GosApplicationUser>.FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            return this.UsersService.GetByUserName(normalizedEmail);
        }

        Task<string> IUserEmailStore<GosApplicationUser>.GetNormalizedEmailAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task IUserEmailStore<GosApplicationUser>.SetNormalizedEmailAsync(GosApplicationUser user, string normalizedEmail, CancellationToken cancellationToken)
        {
            user.NormalizedEmail = normalizedEmail;
            return Task.FromResult(user);
        }

        #endregion IUserEmailStore implementation

        #region IUserRoleStore implementation

        Task IUserRoleStore<GosApplicationUser>.AddToRoleAsync(GosApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task IUserRoleStore<GosApplicationUser>.RemoveFromRoleAsync(GosApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<IList<string>> IUserRoleStore<GosApplicationUser>.GetRolesAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return this.RolesService.GetByUserId(user.Id);
            //return this.IdentityService.GetRolesByUserIdAsync(user.Id, cancellationToken);
        }

        Task<bool> IUserRoleStore<GosApplicationUser>.IsInRoleAsync(GosApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<IList<GosApplicationUser>> IUserRoleStore<GosApplicationUser>.GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion IUserRoleStore implementation

        #region IRoleStore implementation

        Task<IdentityResult> IRoleStore<GosRole>.CreateAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<IdentityResult> IRoleStore<GosRole>.UpdateAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<IdentityResult> IRoleStore<GosRole>.DeleteAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IRoleStore<GosRole>.GetRoleIdAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IRoleStore<GosRole>.GetRoleNameAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task IRoleStore<GosRole>.SetRoleNameAsync(GosRole role, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<string> IRoleStore<GosRole>.GetNormalizedRoleNameAsync(GosRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task IRoleStore<GosRole>.SetNormalizedRoleNameAsync(GosRole role, string normalizedName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<GosRole> IRoleStore<GosRole>.FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<GosRole> IRoleStore<GosRole>.FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion IRoleStore implementation        

        #region IUserLockoutStore Implementation

        Task<DateTimeOffset?> IUserLockoutStore<GosApplicationUser>.GetLockoutEndDateAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.LockoutEnd);
        }

        Task IUserLockoutStore<GosApplicationUser>.SetLockoutEndDateAsync(GosApplicationUser user, DateTimeOffset? lockoutEnd, CancellationToken cancellationToken)
        {
            this.UsersService.UserLockedOutNotification(new Commands.UserLockedOutCommand(user.Email, user.FullName));
            user.LockoutEnd = lockoutEnd;
            return Task.FromResult(0);
        }

        Task<int> IUserLockoutStore<GosApplicationUser>.IncrementAccessFailedCountAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            user.AccessFailedCount++;
            return Task.FromResult(user.AccessFailedCount);
        }

        Task IUserLockoutStore<GosApplicationUser>.ResetAccessFailedCountAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            user.AccessFailedCount = 0;
            return Task.FromResult(0);
        }

        Task<int> IUserLockoutStore<GosApplicationUser>.GetAccessFailedCountAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.AccessFailedCount);
        }

        Task<bool> IUserLockoutStore<GosApplicationUser>.GetLockoutEnabledAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.LockoutEnabled);
        }

        Task IUserLockoutStore<GosApplicationUser>.SetLockoutEnabledAsync(GosApplicationUser user, bool enabled, CancellationToken cancellationToken)
        {
            user.LockoutEnabled = enabled;
            return Task.FromResult(0);
        }

        #endregion IUserLockoutStore Implementation

        #region IUserSecurityStampStore Implementation
        /// <summary>
        /// SetSecurityStampAsync sets the provided security stamp for the provided user
        /// </summary>
        /// <param name="user">User to set the security stamp for</param>
        /// <param name="stamp">Random string that should be set as the current security stamp for the user</param>
        /// <param name="cancellationToken">Token that propagates notifications that operations should be canceled</param>
        /// <returns>A completed task</returns>
        Task IUserSecurityStampStore<GosApplicationUser>.SetSecurityStampAsync(GosApplicationUser user, string stamp, CancellationToken cancellationToken)
        {
            user.SecurityStamp = stamp;
            return Task.CompletedTask;
        }

        /// <summary>
        /// GetSecurityStampAsync returns the provided user's security stamp
        /// if they don't have one it creates a new one for them
        /// </summary>
        /// <param name="user">User to get the security stamp for</param>
        /// <param name="cancellationToken">Token that propagates notifications that operations should be canceled</param>
        /// <returns>SecurityStamp as a string</returns>
        Task<string> IUserSecurityStampStore<GosApplicationUser>.GetSecurityStampAsync(GosApplicationUser user, CancellationToken cancellationToken)
        {
            if (user.SecurityStamp == null)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }
            return Task.FromResult(user.SecurityStamp);
        }
        #endregion IUserSecurityStampStore Implementation

        void IDisposable.Dispose()
        {
            //TODO: Dispose of any child objects
        }
    }
}
