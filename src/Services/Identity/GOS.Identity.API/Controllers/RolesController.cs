﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Controllers
{
    /// <summary>
    /// Controller responsible for getting role information about users
    /// </summary>
    [Route("api/v1")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RolesController : Controller
    {
        #region Private Variables
        private IRolesService _dataService { get; set; }

        #endregion Private Variables

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataService">The data service to get data from</param>
        public RolesController(IRolesService dataService)
            : base()
        {
            _dataService = dataService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get user information by userID
        /// </summary>
        /// <param name="userId">Parameter to filter for a single user by userId</param>
        // GET api/v1/users/{userId}/roles
        [HttpGet("users/{userId}/roles")]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            var roles = await _dataService.GetByUserId(userId);
            return Ok(roles);
        }

        #endregion Methods
    }
}
