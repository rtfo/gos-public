﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.API.Models.Configuration;
using DfT.GOS.Identity.API.Services;
using DfT.GOS.Identity.Common.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Controllers
{
    /// <summary>
    /// Identity Controller for providing JWT Token Authorisation
    /// </summary>
    [Route("api/v1/[controller]")]
    public class IdentityController : ControllerBase
    {
        #region Properties

        private SignInManager<GosApplicationUser> SignInManager { get; set; }
        private IUserStore<GosApplicationUser> UserStore { get; set; }
        private IUserRoleStore<GosApplicationUser> UserRoleStore { get; set; }
        private ISecurityTokenService SecurityTokenService { get; set; }
        private IOptions<AppSettings> AppSettings { get; set; }

        #endregion Properties

        #region Constructors        
        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityController"/> class.
        /// </summary>
        /// <param name="userStore">The user store.</param>
        /// <param name="userRoleStore">The user role store.</param>
        /// <param name="signInManager">The sign in manager.</param>
        /// <param name="securityTokenService">The security token service responsible for creating JWT tokens</param>
        /// <param name="appSettings">Application Settings</param>
        public IdentityController(IUserStore<GosApplicationUser> userStore
            , IUserRoleStore<GosApplicationUser> userRoleStore
            , SignInManager<GosApplicationUser> signInManager
            , ISecurityTokenService securityTokenService
            , IOptions<AppSettings> appSettings)
        {
            this.UserStore = userStore;
            this.UserRoleStore = userRoleStore;
            this.SignInManager = signInManager;
            this.SecurityTokenService = securityTokenService;
            this.AppSettings = appSettings;
        }

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Authenticates the user.
        /// </summary>
        ///<param name="request">The request for authentication, containing username and password</param>
        /// <returns>Returns the result of the authentication</returns>
        [AllowAnonymous]
        [HttpPost("AuthenticateUser")]
        public async Task<IActionResult> AuthenticateUser([FromBody] AuthenticationRequest request)
        {
            if (ModelState.IsValid)
            {
                var authenticationResult = new AuthenticationResult();
                var result = await this.SignInManager.PasswordSignInAsync(request.UserName, request.Password, isPersistent: false, lockoutOnFailure: true);
                authenticationResult.Succeeded = result.Succeeded;
                authenticationResult.IsLockedOut = result.IsLockedOut;

                if (result.Succeeded)
                {
                    var user = await this.UserStore.FindByNameAsync(request.UserName.ToUpper(), default(CancellationToken));
                    await this.SignInManager.UserManager.UpdateSecurityStampAsync(user);
                    var roles = await this.UserRoleStore.GetRolesAsync(user, default(CancellationToken));

                    var claims = this.SecurityTokenService.GetClaimsForUser(user, roles);
                    authenticationResult.Claims = claims;
                    authenticationResult.UserName = request.UserName;
                    authenticationResult.Token = this.SecurityTokenService.CreateToken(user, claims);
                }

                return Ok(authenticationResult);
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Authenticates the user.
        /// </summary>
        ///<param name="request">The request for authentication, containing username and password</param>
        /// <returns>Returns the result of the authentication</returns>
        [AllowAnonymous]
        [HttpPost("AuthenticateService")]
        public IActionResult AuthenticateService([FromBody] AuthenticateServiceRequest request)
        {
            if (ModelState.IsValid)
            {
                var authenticationResult = new AuthenticationResult();
                var authorisedServices = this.SecurityTokenService.GetServiceAccounts();
                var service = authorisedServices.FirstOrDefault(x => x.Id == request.ServiceId);
                if (service != null)
                {
                    var user = new GosApplicationUser { UserName = service.Id };
                    var roles = new List<string>() { "Administrator" };

                    var claims = this.SecurityTokenService.GetClaimsForService(user, roles);
                    authenticationResult.Claims = claims;
                    authenticationResult.Succeeded = true;
                    authenticationResult.IsLockedOut = false;
                    authenticationResult.Token = this.SecurityTokenService.CreateToken(user, claims);

                    return Ok(authenticationResult);
                }
                else
                {
                    return BadRequest("Invalid Service");
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// ValidateSecurityStamp compares the provided stamp with that for the provided user
        /// </summary>
        /// <param name="request">ValidateSecurityStampRequest containing the userID and a SecurityStamp</param>
        /// <returns>Status indicating if the SecurityStamp matches, OK or Unauthorized if it doesn't</returns>
        [HttpPost("ValidateSecurityStamp")]
        public async Task<IActionResult> ValidateSecurityStamp([FromBody] ValidateSecurityStampRequest request)
        {
            var user = await this.UserStore.FindByIdAsync(request.UserID, default);
            
            if (user == null || user.SecurityStamp != request.SecurityStamp)
            {
                return Unauthorized();
            }
            return Ok();
        }

        /// <summary>
        /// SignOut updates the users SecurityStamp in the database so new requests require SignIn
        /// </summary>
        /// <param name="request">SignOutUserRequest containing the userID</param>
        /// <returns>Status representing success, OK if user has signed out, NotFound if user doesn't exist</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("SignOut")]
        public async Task<IActionResult> SignOut([FromBody] SignOutUserRequest request)
        {
            var user = await this.UserStore.FindByIdAsync(request.UserID, default);
            if (user != null)
            {
                await this.SignInManager.UserManager.UpdateSecurityStampAsync(user);
                return Ok();
            }
            return NotFound();
        }

        /// <summary>
        /// Gets the authentication settings used by the Identity Service
        /// </summary>
        /// <returns>Returns the authentication settings</returns>
        [AllowAnonymous]
        [HttpGet("AuthenticationSettings")]
        public AuthenticationConfiguration GetAuthenticationSettings()
        {
            return AppSettings.Value.Authentication;
        }

        #endregion Public Methods
    }
}
