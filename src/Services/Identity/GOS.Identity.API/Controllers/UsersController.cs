﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Commands;
using DfT.GOS.Identity.API.Models;
using DfT.GOS.Identity.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Controllers
{
    /// <summary>
    /// Manage Application users
    /// </summary>
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UsersController : Controller
    {
        #region Private Variables
        private IUsersService _dataService { get; set; }
        private INotify _notify { get; set; }
        #endregion Private Variables

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataService">The data service to get data from</param>
        /// <param name="notification">Send Notifications</param>
        public UsersController(IUsersService dataService
            , INotify notification)
            : base()
        {
            _dataService = dataService;
            _notify = notification;
        }

        #endregion Constructors

        #region Private Methods

        /// <summary>
        /// Default behaviour for reporting ModelState errors
        /// </summary>
        /// <returns>A BadRequest (HTTP 400) response with details of the model errors detected by the model binder</returns>
        private IActionResult BadRequestModelState()
        {
            var errors = ModelState.Values
                .SelectMany(e => e.Errors)
                .Where(e => !string.IsNullOrEmpty(e.ErrorMessage))
                .Select(e => e.ErrorMessage)
                .Concat(ModelState.Values
                    .SelectMany(e => e.Errors)
                    .Where(e => e.Exception != null)
                    .Select(e => e.Exception)
                    .Select(e => e.Message))
                .Aggregate(new StringBuilder(), (sb, k) => sb.Append(k), sb => sb.ToString());
            return BadRequest(errors);
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Get user information by UserName
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return a user by userName
        /// </remarks> 
        /// <param name="normalizedUserName">Parameter to filter users by UserName</param>
        /// <returns></returns>
        // GET api/v1/users/username/{normalisedUsername}
        [HttpGet("username/{normalizedUserName}")]
        public async Task<IActionResult> GetByUserName(string normalizedUserName)
        {
            var user = await _dataService.GetByUserName(normalizedUserName);
            return Ok(user);
        }

        /// <summary>
        /// Get user information by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return a user by id
        /// </remarks> 
        /// <param name="id">Parameter to filter users by id</param>
        /// <returns></returns>
        // GET api/v1/users/ed9a9181-41f4-49c0-a24a-5e8a2b66e307
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(string id)
        {
            var user = await _dataService.GetUserById(id);
            return Ok(user);
        }

        /// <summary>
        /// Get user information by Email
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return a user by email
        /// </remarks>  
        /// <param name="normalizedEmail">Parameter to filter users by Email</param>
        /// <returns></returns>
        // GET api/v1/users/email/{normalizedEmail}
        [HttpGet("email/{normalizedEmail}")]
        public async Task<IActionResult> GetByEmail(string normalizedEmail)
        {
            var user = await _dataService.GetByEmail(normalizedEmail);
            return Ok(user);
        }

        /// <summary>
        /// Create a user from invite 
        /// </summary>
        /// <remarks>
        /// This can be an administrator user or one associated with an organisation.
        /// Set "company" to "None" if it's an administrator user.
        /// Sample request:
        ///     - create a user 
        /// </remarks>        
        /// <param name="user">Parameter to create a user as in model</param>
        /// <param name="link">The invite token for the user to create</param>
        /// <returns></returns>
        //POST api/users/
        [Route("create/{link}")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [AllowAnonymous]
        public async Task<IActionResult> CreateUser(string link, [FromBody]GosApplicationUserInvite user)
        {
            if (ModelState.IsValid)
            {
                var userId = await _dataService.CreateUser(user, user.Password, link);
                var userIdGuid = Guid.Empty;
                return userId != null && Guid.TryParse(userId, out userIdGuid) ?
                    (IActionResult)Ok() :
                    (IActionResult)BadRequest();
            }
            else
            {
                return BadRequestModelState();
            }
        }

        /// <summary>
        /// Edit a user by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - update a user by user id
        /// </remarks>
        /// <param name="id">Parameter for user id</param>
        /// <param name="user">Parameter for user information as in model</param>
        /// <returns></returns>
        //POST api/v1/users/ed9a9181-41f4-49c0-a24a-5e8a2b66e307
        [Route("{id}")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateUser(string id, [FromBody]GosApplicationUser user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequestModelState();
            }
            if (user.Id == id)
            {
                var updatedUser = await _dataService.UpdateUser(user);
                return updatedUser != null ?
                   (IActionResult)Ok(updatedUser) :
                   (IActionResult)BadRequest();
            }
            else
            {
                return BadRequest("Ids in request url and body must match");
            }
        }

        /// <summary>
        /// Send an Email invitation
        /// </summary>
        /// <param name="user">Recipient information</param>
        /// <returns></returns>
        [Route("invite")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Invite([FromBody]GosApplicationUserInvite user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequestModelState();
            }
            else
            {
                var inviteUser = await _dataService.InviteUserToRegisterAccount(user, this.User.GetUserId());
                return inviteUser != false ?
                   (IActionResult)Ok(inviteUser) :
                   (IActionResult)BadRequest();
            }
        }

        /// <summary>
        /// Get Email by Link
        /// </summary>
        /// <param name="link">Link to Email</param>
        /// <returns>OK or BadRequest</returns>
        [Route("link/{link}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [AllowAnonymous]
        public async Task<IActionResult> GetEmailByLink(string link)
        {
            if (link != null)
            {
                var inviteDetails = await _dataService.LinkAsync(link);

                return inviteDetails != null ?
                   (IActionResult)Ok(inviteDetails) :
                   (IActionResult)BadRequest();
            }
            else
            {
                return BadRequest("Supplied link cannot be null");
            }
        }

        /// <summary>
        /// Send an Email notification to new user
        /// </summary>
        /// <param name="user">New user</param>
        /// <returns>OK or BadRequest</returns>
        [Route("newuseremail")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [AllowAnonymous]
        public async Task<IActionResult> NewUserEmail([FromBody]GosApplicationNewUser user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequestModelState();
            }
            else
            {
                var newUser = await _dataService.NewUserEmailAsync(user);
                return newUser != false ?
                   (IActionResult)Ok(newUser) :
                   (IActionResult)BadRequest();
            }
        }

        /// <summary>
        ///  Send email notification to user that the account has been linked
        /// </summary>
        /// <param name="user">Receipient</param>
        /// <param name="company">Company</param>
        /// <returns></returns>
        [Route("newuserlinkedemail")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> NewUserLinkedEmail([FromBody]GosApplicationNewUser user, string company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequestModelState();
            }
            else
            {
                var newUser = await _dataService.NewUserLinkedEmailAsync(user);
                return newUser != false ?
                   (IActionResult)Ok(newUser) :
                   (IActionResult)BadRequest();
            }
        }

        /// <summary>
        /// Assign role to user
        /// </summary>
        /// <param name="inviteUser">invited user</param>
        /// <returns></returns>
        [Route("role")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AssignRoleToUser([FromBody]GosApplicationUser inviteUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequestModelState();
            }
            var userId = await _dataService.AssignRoleToUserAsync(inviteUser);

            return userId != null ?
                (IActionResult)Ok() :
                (IActionResult)BadRequest();
        }

        /// <summary>
        /// Add new Gos Application User
        /// </summary>
        /// <param name="addNewGosApplicationUser">
        ///     RequestedByUserId - Requestor user Id
        ///     OrganisationId - Request on behalf of Organisation
        ///     Email - New User's Email
        /// </param>
        /// <returns>Status Message</returns>
        [Route("adduser")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddUser([FromBody]AddNewGosApplicationUser addNewGosApplicationUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequestModelState();
            }
            var result = await _dataService.AddUser(addNewGosApplicationUser, this.User.GetUserId());

            return (result == null) ?
                (IActionResult)BadRequest(result) :
                (IActionResult)Ok(result);
        }

        /// <summary>
        /// Get Users by Organisation ID
        /// </summary>
        /// <param name="id">Organisation id</param>
        /// <returns></returns>
        [Route("organisation/{id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Organisation(int id)
        {
            var users = await _dataService.GetUsersByOrganisationId(id);

            return users != null ?
                (IActionResult)Ok(users) :
                (IActionResult)BadRequest();
        }

        /// <summary>
        /// Remove user
        /// </summary>
        /// <param name="userId">UserId to remove</param>
        /// <returns>OK or BadRequest</returns>
        [Route("remove")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> RemoveUser([FromBody]string userId)
        {
            var removeduser = await _dataService.RemoveUser(userId);
            return removeduser ?
                (IActionResult)Ok() :
                (IActionResult)BadRequest();
        }

        /// <summary>
        /// Requests a forgotten password notification is sent
        /// </summary>
        /// <param name="command">The command containing details for the notification</param>
        [Route("IssueForgottenPasswordNotification")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [AllowAnonymous]
        public async Task<IActionResult> IssueForgottenPasswordNotification([FromBody] IssueForgottenPasswordNotificationCommand command)
        {
            if (ModelState.IsValid)
            {
                var success = await _dataService.SendUserResetPasswordNotification(command);
                return success ? (IActionResult)Ok() : BadRequest();
            }
            else
            {
                return BadRequestModelState();
            }
        }

        /// <summary>
        /// Get Admin users
        /// </summary>
        /// <returns></returns>
        [HttpGet("adminusers")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> AdminUsers()
        {
            var users = await this._dataService.GetAdminUsers();
            return Ok(users);
        }

        /// <summary>
        /// Add an administrator user, either invites a new user or gives an existing one the administrator role
        /// </summary>
        /// <param name="newUser">SetAdminRoleCommand containing the email of the user to add</param>
        /// <returns>A http response of OK or bad request</returns>
        [HttpPost("addAdminUser")]
        [Authorize(Roles = Roles.Administrator)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddAdminUser([FromBody]SetAdminRoleCommand newUser)
        {
            if (newUser == null || !ModelState.IsValid)
            {
                return BadRequestModelState();
            }
            else
            {
                var result = await this._dataService.AddAdminUser(newUser.EmailAddress, this.User.GetUserId());
                return result.IsNewAdministrator ? (IActionResult)Ok(result) : BadRequest(result);

            }
        }

        /// <summary>
        /// Removes the administrator role for an user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost("removeAdminRole")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> RemoveAdminRole([FromBody]string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return BadRequest("userId cannot be null");
            }
            var user = await this._dataService.GetUserById(userId);
            if (user == null)
            {
                return NotFound();
            }
            else if (user.Role != Roles.Administrator && user.Role != null)
            {
                return BadRequest("User is not an administrator");
            }
            else
            {
                var users = await this._dataService.GetAdminUsers();
                if (users.Any(x => x.Id == userId))
                {
                    var result = await this._dataService.RemoveAdminRole(userId);
                    return result ? (IActionResult)Ok(result) : BadRequest("Failed to remove Admin role");
                }
                else
                {
                    return BadRequest("User is not assigned to the Admin role");
                }
            }
        }

        /// <summary>
        /// Validates the password reset token
        /// </summary>
        /// <param name="token">The token to be validated</param>
        [Route("ValidateResetPasswordToken/{token}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [AllowAnonymous]
        public async Task<IActionResult> ValidateResetPasswordToken(string token)
        {
            if (ModelState.IsValid)
            {
                var success = await _dataService.ValidateResetPasswordToken(token);
                return success ? (IActionResult)Ok() : BadRequest();
            }
            else
            {
                return BadRequestModelState();
            }
        }

        /// <summary>
        /// Resets a user's password
        /// </summary>
        /// <param name="command">The command containing details for the notification</param>
        [Route("ResetPassword")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordCommand command)
        {
            if (ModelState.IsValid)
            {
                var success = await _dataService.ResetPassword(command);
                return success ? (IActionResult)Ok() : BadRequest();
            }
            else
            {
                return BadRequestModelState();
            }
        }
        #endregion Public Methods
    }
}
