﻿﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Identity.API.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace GOS.Identity.API.Controllers
{
    /// <summary>
    /// Retrieve various data on the performance of the application
    /// </summary>
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PerformanceDataController : Controller
    {
        #region Private Variables
        private IUsersService DataService { get; set; }
        #endregion Private Variables

        #region Constructors
        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataService">The data service to get data from</param>
        public PerformanceDataController(IUsersService dataService)
            : base()
        {
            this.DataService = dataService;
        }
        #endregion Constructors

        #region public methods
        /// <summary>
        /// Returns the performance data relating to registrations
        /// </summary>
        /// <remarks>
        ///  - Number of invitations without an account, per month
        ///  - Number of invitations, per month
        ///  - Total number of users, per month
        /// </remarks>
        /// <returns>JSON array of objects for the registrations performance data</returns>
        [Route("Registrations")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IActionResult> RegistrationsData()
        {
            var registrations = await this.DataService.GetRegistrationPerformanceData();
            return new OkObjectResult(registrations);
        }
        #endregion public methods
    }
}
