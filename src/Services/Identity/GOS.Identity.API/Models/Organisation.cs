﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Models
{
    /// <summary>
    /// Represents an Organisation (Supplier, Trader, Verifier) in GOS
    /// </summary>
    public class Organisation
    {
        #region Properties

        /// <summary>
        /// Organisation Id
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Organisation Name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Organisation Status Id
        /// </summary>
        public int StatusId { get; private set; }

        /// <summary>
        /// Organisation Status Description
        /// </summary>
        public string Status { get; private set; }

        /// <summary>
        /// Organisation Type Id
        /// </summary>
        public int TypeId { get; private set; }

        /// <summary>
        /// Organisation Type Description
        /// </summary>
        public string Type { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Contructor for an organisation
        /// </summary>
        public Organisation(int id
            , string name
            , int statusId
            , string status
            , int typeId
            , string type)
        {
            this.Id = id;
            this.Name = name;
            this.StatusId = statusId;
            this.Status = status;
            this.TypeId = typeId;
            this.Type = type;
        }

        #endregion Constructors
    }
}
