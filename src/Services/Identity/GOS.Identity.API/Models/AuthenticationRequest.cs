﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Identity.API.Models
{
    /// <summary>
    /// DTO for a request to perform authentcation
    /// </summary>
    public class AuthenticationRequest
    {
        #region Properties

        /// <summary>
        /// The User name of the user attempting to authenticate
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The Password of the user attempting to authenticate
        /// </summary>
        public string Password { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="userName">The username of the user to authenticate</param>
        /// <param name="password">The password of the user to authenticate</param>
        public AuthenticationRequest(string userName, string password)
        {
            this.UserName = userName;
            this.Password = password;
        }

        #endregion Constructors
    }
}
