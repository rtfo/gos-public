﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Newtonsoft.Json;

namespace DfT.GOS.Identity.API.Models
{
    /// <summary>
    /// Configuration object consisting of authorized service to access the APIs
    /// </summary>
    public class AuthorisedService
    {
        /// <summary>
        /// Name of the service
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Identifier for the service
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
