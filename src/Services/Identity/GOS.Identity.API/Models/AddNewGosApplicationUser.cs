﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System;

namespace DfT.GOS.Identity.API.Models
{
    /// <summary>
    /// DTO to Add New Gos Application User
    /// </summary>
    public class AddNewGosApplicationUser 
    {
        /// <summary>
        /// User that requests the creation of a new user
        /// </summary>
        public string RequestedByUserId { get; set; }

        /// <summary>
        /// Request on behalf of an Organisation
        /// </summary>
        public int? OrganisationId { get; set; }

        /// <summary>
        /// Requested user Email address
        /// </summary>
        [GosEmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Role to assign to user
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// Compamy name
        /// </summary>
        public string Company { get; set; }
    }
}
