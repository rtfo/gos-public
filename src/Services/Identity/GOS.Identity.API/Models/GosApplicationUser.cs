﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using DfT.GOS.Web.Validation;
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Identity.API.Models
{
    /// <summary>
    /// Represents a System User
    /// </summary>
    public class GosApplicationUser : IdentityUser, IEntity<string>
    {
        /// <summary>
        /// The Full name of the user
        /// </summary>
        [Required]
        public string FullName { get; set; }

        /// <summary>
        /// Overridden Email property
        /// </summary>
        [Required]
        [GosEmailAddress]
        public override string Email { get; set; }

        /// <summary>
        /// Overriden Username property
        /// </summary>
        [Required]
        public override string UserName { get; set; }

        /// <summary>
        /// The ID of the organisation this user is associated with
        /// </summary>
        public int? OrganisationId { get; set; }

        /// <summary>
        /// The role assigned to this user
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// The date the User record was created
        /// </summary>
        [Required]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// The date the user was associated with the organistion
        /// </summary>
        public Nullable<DateTime> OrganisationAssociationDate { get; set; }

        /// <summary>
        /// The ID of the user that made the association with the organisation
        /// </summary>
        public string OrganisationAssociationBy { get; set; }
    }
}
