﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Identity.API.Models
{
    /// <summary>
    /// This is the model of what is populated from the invite table
    /// </summary>
    public class Invites
    {
        /// <summary>
        /// The Organisation ID of whom sent the invite
        /// </summary>
        public int? SenderId { get; set; }
        /// <summary>
        /// The role for the user that is being invited
        /// </summary>
        [Required]
        public string Role { get; set; }
        /// <summary>
        /// The Organisation Name of whom sent the invite
        /// </summary>
        [Required]
        public string OrganisationName { get; set; }
        /// <summary>
        /// Email for where invite was sent
        /// </summary>
        [Required]
        public string Email { get; set; }
        /// <summary>
        /// The invite token for creating a new user
        /// </summary>
        [Key]
        [Required]
        public string Uri { get; set; }
        /// <summary>
        /// The expiry Date/Time for the invite token
        /// </summary>
        [Required]
        public DateTime ExpireInviteLink { get; set; }
        /// <summary>
        /// The ID of the user who made the invite
        /// </summary>
        [Required]
        public string InvitedBy { get; set; }

        /// <summary>
        /// The Date/Time the user was invited
        /// </summary>
        [Required]
        public DateTime InvitedDate { get; set; }
    }
}
