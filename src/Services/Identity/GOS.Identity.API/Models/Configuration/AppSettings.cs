﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;

namespace DfT.GOS.Identity.API.Models.Configuration
{
    /// <summary>
    /// Application configuration settings
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// User authentication settings
        /// </summary>
        public AuthenticationConfiguration Authentication { get; set; }

        /// <summary>
        /// The amount of time to expire JWT tokens after issuing them
        /// </summary>
        public int JwtTokenExpiryMinutes { get; set; }

        /// <summary>
        /// The number of hours to expire a password reset notification
        /// </summary>
        public int PasswordResetNotificationExpiryHours { get; set; }

        /// <summary>
        /// The number of hours to expire a an invite to register
        /// </summary>
        public int InviteExpiryHours { get; set; }

        /// <summary>
        /// HTTP Client setting for resilliant HTTP communication
        /// </summary>
        public HttpClientAppSettings HttpClient { get; set; }

        /// <summary>
        /// Gets or sets the system parameters service API URL.
        /// </summary>
        public string SystemParametersServiceApiUrl { get; set; }
    }
}
