﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Identity.API.Models.Configuration
{
    /// <summary>
    /// Configuration for creation of JWT token
    /// </summary>
    public class SecurityTokenConfiguration
    {
        #region Properties

        /// <summary>
        /// The JWT Token Audience
        /// </summary>
        public string  JwtTokenAudience { get; private set; }

        /// <summary>
        /// The JWT Token Issuer
        /// </summary>
        public string JwtTokenIssuer { get; private set; }

        /// <summary>
        /// The JWT Token key for signing the token
        /// </summary>
        public string JwtTokenKey { get; private set; }

        /// <summary>
        /// The timeout, in minutes, of the JWT token
        /// </summary>
        public int JwtTimeoutMinutes { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        ///  Constructor taking all values
        /// </summary>
        /// <param name="jwtTokenAudience">The JWT Token Audience</param>
        /// <param name="jwtTokenIssuer">The JWT Token Issuer</param>
        /// <param name="jwtTokenKey">The JWT Token key for signing the token</param>
        /// <param name="jwtTimeoutMinutes">The timeout, in minutes, of the JWT token</param>
        public SecurityTokenConfiguration(string jwtTokenAudience
            , string jwtTokenIssuer
            , string jwtTokenKey
            , int jwtTimeoutMinutes)
        {
            this.JwtTokenAudience = jwtTokenAudience;
            this.JwtTokenIssuer = jwtTokenIssuer;
            this.JwtTokenKey = jwtTokenKey;
            this.JwtTimeoutMinutes = jwtTimeoutMinutes;
        }

        #endregion Constructors
    }
}
