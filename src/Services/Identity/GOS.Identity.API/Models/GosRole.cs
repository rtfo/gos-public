﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Identity;

namespace DfT.GOS.Identity.API.Models
{
    /// <summary>
    /// GOS Role DTO
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.IdentityRole" />
    public class GosRole: IdentityRole
    {
    }
}
