﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;
using System;

namespace DfT.GOS.Identity.API.Models
{
    /// <summary>
    /// All fields from an invite to create a user
    /// </summary>
    public class GosApplicationUserInvite
    {
        /// <summary>
        /// Organisation ID for whom invited the user to be used to create user
        /// Shouldn't be included when the invite is for an administrator
        /// </summary>
        public int? OrganisationId { get; set; }
        /// <summary>
        /// Organisation Name for whom invited the user to be used to create user
        /// When the invite is for an administrator this should be "None"
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// Full name to be used to create user
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// The role of the user to be created
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// Email address of where the invite was sent
        /// </summary>
        [GosEmailAddress]
        [Required]
        public string Email { get; set; }
        /// <summary>
        /// the password to be assigned during user creation
        /// </summary>
        public string Password { get; set; }
    }
}
