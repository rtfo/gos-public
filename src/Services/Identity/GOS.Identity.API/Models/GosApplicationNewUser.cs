﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Identity.API.Models
{
    /// <summary>
    /// Represents a new GOS user
    /// </summary>
    public class GosApplicationNewUser
    {
        /// <summary>
        /// Company (Supplier) to which new user belongs
        /// </summary>
        public string Company { get; set; } 

        /// <summary>
        /// Email address of new user
        /// </summary>
        [GosEmailAddress]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Full name of new user
        /// </summary>
        public string FullName { get; set;}
    }
}
