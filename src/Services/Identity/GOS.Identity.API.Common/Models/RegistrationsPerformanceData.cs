﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.Identity.Common.Models
{
    /// <summary>
    /// Holds the data about registrations in the given month
    /// </summary>
    public class RegistrationsPerformanceData
    {
        #region properties
        /// <summary>
        /// Month the data relates to
        /// </summary>
        public DateTime Date { get; private set; }

        /// <summary>
        /// Number of invitations sent in the given month that aren't linked to an account for the supplied date range.
        /// </summary>
        public int InvitationsWithoutAccount { get; private set; }

        /// <summary>
        /// Number of invitations sent for the supplied date range.
        /// </summary>
        public int Invitations { get; private set; }

        /// <summary>
        /// Total number of new users for the supplied date range.
        /// </summary>
        public int TotalUsers { get; private set; }
        #endregion properties

        #region constructor

        /// <summary>
        /// Constructor taking all parameters
        /// </summary>
        /// <param name="month">Start date of the month that the data is for</param>
        /// <param name="invitationsWithoutAccount">Number of invitations sent that don't have an account</param>
        /// <param name="invitations">Number of invitations sent</param>
        /// <param name="totalUsers">Number of user accounts</param>
        public RegistrationsPerformanceData(DateTime date, int invitationsWithoutAccount, int invitations, int totalUsers)
        {
            this.Date = date;
            this.InvitationsWithoutAccount = invitationsWithoutAccount;
            this.Invitations = invitations;
            this.TotalUsers = totalUsers;
        }
        #endregion constructor
    }
}
