﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Identity.Common.Models
{
    /// <summary>
    /// Holds fields required to validate a security stamp
    /// </summary>
    public class ValidateSecurityStampRequest
    {

        /// <summary>
        /// SecurityStamp being validated
        /// </summary>
        public string SecurityStamp { get; set; }

        /// <summary>
        /// ID of the user to validated the security stamp for
        /// </summary>
        public string UserID { get; set; }

        /// <summary>
        /// Constructor taking the security stamp and user who it's for
        /// </summary>
        /// <param name="securityStamp">Security stamp to validate</param>
        /// <param name="userID">ID of the user</param>
        public ValidateSecurityStampRequest(string securityStamp, string userID)
        {
            this.SecurityStamp = securityStamp;
            this.UserID = userID;
        }
    }
}
