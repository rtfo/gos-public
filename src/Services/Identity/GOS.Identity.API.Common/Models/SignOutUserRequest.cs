﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Identity.Common.Models
{
    /// <summary>
    /// Model containing fields required to sign out a user
    /// </summary>
    public class SignOutUserRequest
    {
        /// <summary>
        /// ID of the user being passed in the SignOut request
        /// </summary>
        public string UserID { get; set; }

        /// <summary>
        /// Constructor taking the user ID
        /// </summary>
        /// <param name="userID">ID of the user to sign out</param>
        public SignOutUserRequest(string userID)
        {
            this.UserID = userID;
        }
    }
}
