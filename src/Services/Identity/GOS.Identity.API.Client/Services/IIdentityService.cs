﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Models;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Client.Services
{
    /// <summary>
    /// Definition for calling the remote Identity Service
    /// </summary>
    public interface IIdentityService
    {
        /// <summary>
        /// Authenticates the user.
        /// </summary>
        /// <param name="username">The username of the user we are authenticating</param>
        /// <param name="password">The password of the user we are authenticating</param>
        /// <returns>Returns the result of the authentication</returns>
        Task<AuthenticationResult> AuthenticateUser(string username, string password);

        /// <summary>
        /// Authenticates the user.
        /// </summary>
        /// <param name="serviceId">The serviceId of the service we are authenticating</param>
        /// <returns>Returns the result of the authentication</returns>
        Task<AuthenticationResult> AuthenticateService(string serviceId);

        /// <summary>
        /// Gets authentication settings used by the Identity Service
        /// </summary>
        /// <returns>Returns the authentication configuration settings</returns>
        Task<HttpObjectResponse<AuthenticationConfiguration>> GetAuthenticationSettings();

        /// <summary>
        /// ValidateSecurityStamp checks whether the provided security matches the one for the provided user as currently
        /// stored in the database
        /// </summary>
        /// <param name="securityStamp">Last known security stamp</param>
        /// <param name="userID">ID of the user to check against</param>
        /// <returns>Boolean indicating success</returns>
        Task<bool> ValidateSecurityStamp(string securityStamp, string userID);
        
        /// <summary>
        /// SignOut invalidates the user on the server
        /// </summary>
        /// <param name="userID">ID of the user to sign out</param>
        /// <param name="jwtToken">JWT token to authenticate with</param>
        /// <returns>Boolean indicating success</returns>
        Task<bool> SignOut(string userID, string jwtToken);
    }
}
