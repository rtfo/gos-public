﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.Common.Models;
using Newtonsoft.Json;

namespace DfT.GOS.Identity.API.Client.Services
{
    /// <summary>
    /// Connect to the remote Identity Service to get Identity details
    /// </summary>
    public class IdentityService : HttpServiceClient, IIdentityService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        public IdentityService(HttpClient httpClient)
            :base(httpClient)
        {
        }

        #endregion Constructors

        #region IIdentityService implementation

        /// <summary>
        /// Authenticates the user.
        /// </summary>
        /// <param name="username">The username of the user we are authenticating</param>
        /// <param name="password">The password of the user we are authenticating</param>
        /// <returns>Returns the result of the authentication</returns>
        async Task<AuthenticationResult> IIdentityService.AuthenticateUser(string username, string password)
        {
            var request = new AuthenticationRequest(username, password);

            var json = JsonConvert.SerializeObject(request);
            var response = await this.HttpClient.PostAsync("api/v1/Identity/AuthenticateUser", new StringContent(json, Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<AuthenticationResult>(jsonResponse);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Authenticates the user.
        /// </summary>
        /// <param name="serviceId">The serviceId of the service we are authenticating</param>
        /// <returns>Returns the result of the authentication</returns>
        async Task<AuthenticationResult> IIdentityService.AuthenticateService(string serviceId)
        {
            var request = new AuthenticateServiceRequest { ServiceId = serviceId };

            var json = JsonConvert.SerializeObject(request);
            var response = await this.HttpClient.PostAsync("api/v1/Identity/AuthenticateService", new StringContent(json, Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<AuthenticationResult>(jsonResponse);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets authentication settings used by the Identity Service
        /// </summary>
        /// <returns>Returns the authentication configuration settings</returns>
        async Task<HttpObjectResponse<AuthenticationConfiguration>> IIdentityService.GetAuthenticationSettings()
        {
            return await this.HttpGetAsync<AuthenticationConfiguration>("api/v1/Identity/AuthenticationSettings");
        }

        /// <summary>
        /// ValidateSecurityStamp checks whether the provided security matches the one for the provided user as currently
        /// stored in the database
        /// </summary>
        /// <param name="securityStamp">Last known security stamp</param>
        /// <param name="userID">ID of the user to check against</param>
        /// <returns>Boolean indicating success</returns>
        async Task<bool> IIdentityService.ValidateSecurityStamp(string securityStamp, string userID)
        {
            var request = new ValidateSecurityStampRequest(securityStamp, userID);
            var response = await this.HttpPostAsync("api/v1/Identity/ValidateSecurityStamp", request);

            return response.StatusCode != HttpStatusCode.Unauthorized;
        }

        /// <summary>
        /// SignOut invalidates the user on the server
        /// </summary>
        /// <param name="userID">ID of the user to sign out</param>
        /// <param name="jwtToken">JWT token to authenticate with</param>
        /// <returns>Boolean indicating success</returns>
        async Task<bool> IIdentityService.SignOut(string userID, string jwtToken)
        {
            var request = new SignOutUserRequest(userID);
            var response = await this.HttpPostAsync("api/v1/Identity/SignOut", request, jwtToken);

            return response.IsSuccessStatusCode;
        }

        #endregion IIdentityService implementation
    }
}
