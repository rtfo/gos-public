﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Commands;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.Common.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Identity.API.Client.Services
{
    /// <summary>
    /// User service interface 
    /// </summary>
    public interface IUsersService
    {
        /// <summary>
        /// Creates a new user from invite
        /// </summary>
        /// <param name="user">The user to create</param>
        /// <param name="password">The password for the user to create</param>
        /// <param name="link">The invite token for the user to create</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<IdentityResult> CreateUserAsync(GosApplicationUserInvite user, string password, string link, CancellationToken cancellationToken);

        /// <summary>
        /// Updates an existing user
        /// </summary>
        /// <param name="user">The user to update</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns>Returns a copy of the update user</returns>
        Task<GosApplicationUser> UpdateUserAsync(GosApplicationUser user, CancellationToken cancellationToken, string jwtToken);

        /// <summary>
        /// Finds a user by userId
        /// </summary>
        /// <param name="userId">The ID of the user to find</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<GosApplicationUser> FindUserByIdAsync(string userId, string jwtToken, CancellationToken cancellationToken);

        /// <summary>
        /// Finds a user by normalised user name
        /// </summary>
        /// <param name="normalizedUserName">The normalised user name</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<GosApplicationUser> FindUserByNameAsync(string normalizedUserName, CancellationToken cancellationToken, string jwtToken);

        /// <summary>
        /// Finds auser by normalised email address
        /// </summary>
        /// <param name="normalizedEmail">The normalised email address to find the user by</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<GosApplicationUser> FindUserByEmailAsync(string normalizedEmail, CancellationToken cancellationToken, string jwtToken);

        /// <summary>
        /// Gets a list of roles for a user
        /// </summary>
        /// <param name="userId">The ID of the user to get the roles for</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        Task<IList<string>> GetRolesByUserIdAsync(string userId, CancellationToken cancellationToken, string jwtToken);

        /// <summary>
        /// Retrieve Invite details from link
        /// </summary>
        /// <param name="link">Link to user</param>
        /// <returns></returns>
        Task<GosInvitedUserAssociation> GetInviteDetailsByLinkAsync(string link);

        /// <summary>
        /// Assign Role To User
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>Http response</returns>
        Task<string> AssignRoleToUserAsync(GosApplicationUser user, string jwtToken);

        /// <summary>
        /// Remove User sets the Organisation and Role to 0
        /// </summary>
        /// <param name="user">User to remove from fromn Roles </param>
        /// <returns>Http Post reponse</returns>
        Task<string> RemoveUserAsync(string userId, string jwtToken);

        /// <summary>
        /// Get Organisation Users
        /// </summary>
        /// <param name="organisationId">Organisation Id</param>
        /// <returns>List of GosApplicationUser</returns>
        Task<List<GosApplicationUser>> GetOrganisationUsers(int organisationId, string jwtToken);

        /// <summary>
        /// Add an organisation Users by creating a new account or linking an existing user to a role
        /// </summary>
        /// <param name="requestedBy">User that requests to add user</param>
        /// <param name="newUser">New User</param>
        /// <param name="organisationId">Organisation Id</param>
        /// <returns>Status  of the adding a user</returns>
        Task<AddNewGosApplicationUserResult> AddUserAsync(AddNewGosApplicationUser requestedBy, string jwtToken);

        /// <summary>
        /// Gets the admin users
        /// </summary>
        /// <param name="jwtToken"></param>
        /// <returns>List of all the users who are administrators</returns>
        Task<HttpObjectResponse<IList<GosApplicationUser>>> GetAdminUsers(string jwtToken);

        /// <summary>
        /// Add a user as an administrator
        /// </summary>
        /// <param name="user">User information for creating the new administrator</param>
        /// <param name="jwtToken">Token of the user making the request</param>
        /// <returns>An object containing any required message and booleans as to whether the user has been made an admin and been sent an invite</returns>
        Task<AddNewAdminUserResult> AddAdminUser(AddNewGosApplicationUser user, string jwtToken);

        /// <summary>
        /// Removes administrator role for an user
        /// </summary>
        /// <param name="userId">Id of the user to remove</param>
        /// <param name="jwtToken">Token of the user making the request</param>
        /// <returns>Boolean whether it successfully removed the users admin role</returns>
        Task<HttpResponseMessage> RemoveAdministratorRole(string userId, string jwtToken);

        #region Notifications

        /// <summary>
        /// Send an email notification to New User
        /// </summary>
        /// <param name="user">Recipient information</param>
        /// <returns></returns>
        Task<HttpResponseMessage> NewUserEmailAsync(GosApplicationUser user);

        /// <summary>
        /// Send an email notification to New User Linked
        /// </summary>
        /// <param name="user">Recipient information</param>
        /// <returns></returns>
        Task<HttpResponseMessage> NewUserLinkedEmailAsync(GosApplicationUser user, string company, string jwtToken);

        /// <summary>
        /// Requests a forgotten password notification is sent
        /// </summary>
        /// <param name="command">The command containing details for the notification</param>
        Task<HttpResponseMessage> IssueForgottenPasswordNotification(IssueForgottenPasswordNotificationCommand command);

        /// <summary>
        /// Validates the reset password token
        /// </summary>
        /// <param name="token">The reset password token</param>
        Task<HttpResponseMessage> ValidateResetPasswordToken(string token);

        /// <summary>
        /// Requests a reset password
        /// </summary>
        /// <param name="command">The command containing details for the reset</param>
        Task<HttpResponseMessage> ResetPassword(ResetPasswordCommand command);

        /// <summary>
        /// Returns the performance data relating to registrations
        ///  - Number of invitations without an account, per month
        ///  - Number of invitations, per month
        ///  - Total number of users, per month
        /// </summary>
        /// <param name="jwtToken">JWT Token of the user making the request</param>
        /// <returns>List of the performance data for registrations</returns
        Task<List<RegistrationsPerformanceData>> RegistrationsPerformanceData(string jwtToken);

        #endregion

    }
}
