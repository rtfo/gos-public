﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DfT.GOS.Http;
using DfT.GOS.Identity.API.Client.Models;
using DfT.GOS.Identity.Common.Models;
using DfT.GOS.Identity.API.Client.Commands;
using System;
using Microsoft.AspNetCore.Identity;

namespace DfT.GOS.Identity.API.Client.Services
{
    public class UsersService : HttpServiceClient, IUsersService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        public UsersService(HttpClient httpClient) : base(httpClient)
        {
        }

        #endregion Constructors

        #region IUsersService implementation

        /// <summary>
        /// Creates a new user from invite
        /// </summary>
        /// <param name="user">The user to create</param>
        /// <param name="link">The invite token to create user</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns>Result of creation from identity operation</returns>
        async Task<IdentityResult> IUsersService.CreateUserAsync(GosApplicationUserInvite user, string password, string link, CancellationToken cancellationToken)
        {
            JObject jo = JObject.FromObject(user);
            jo.Add("Password", password);
            var json = JsonConvert.SerializeObject(jo);
            var response = await this.HttpClient.PostAsync("api/v1/Users/create/" + link, new StringContent(json, Encoding.UTF8, "application/json"), cancellationToken);

            if (response.IsSuccessStatusCode)
            {
                return IdentityResult.Success;
            }
            else
            {
                var error = new IdentityError() { Description = "Unable to create user account" };
                return IdentityResult.Failed(new IdentityError[] { error });
            }            
        }

        /// <summary>
        /// Updates an existing user
        /// </summary>
        /// <param name="user">The user to update</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns>Returns a copy of the update user</returns>
        async Task<GosApplicationUser> IUsersService.UpdateUserAsync(GosApplicationUser user, CancellationToken cancellationToken, string jwtToken)
        {
            var json = JsonConvert.SerializeObject(user);
            var response = await this.HttpPostAsync("api/v1/Users/" + user.Id, new StringContent(json, Encoding.UTF8, "application/json"), jwtToken);
            response.EnsureSuccessStatusCode();

            GosApplicationUser updatedUser = null;

            if (response.IsSuccessStatusCode)
            {
                var updatedUserJson = await response.Content.ReadAsStringAsync();
                updatedUser = JsonConvert.DeserializeObject<GosApplicationUser>(updatedUserJson);
            }

            return updatedUser;
        }

        /// <summary>
        /// Finds a user by userId
        /// </summary>
        /// <param name="userId">The ID of the user to find</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        async Task<GosApplicationUser> IUsersService.FindUserByIdAsync(string userId, string jwtToken, CancellationToken cancellationToken)
        {
            GosApplicationUser user = null;

            var response = await this.HttpGetAsync<GosApplicationUser>("api/v1/Users/" + userId, jwtToken);

            if (response.HasResult)
            {
                user = response.Result;
            }

            return user;
        }

        /// <summary>
        /// Finds a user by normalised user name
        /// </summary>
        /// <param name="normalizedUserName">The normalised user name</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        async Task<GosApplicationUser> IUsersService.FindUserByNameAsync(string normalizedUserName, CancellationToken cancellationToken, string jwtToken)
        {
            GosApplicationUser user = null;

            var response = await this.HttpGetAsync<GosApplicationUser>("api/v1/Users/username/" + normalizedUserName, jwtToken);

            if (response.HasResult)
            {
                user = response.Result;
            }

            return user;
        }

        /// <summary>
        /// Finds auser by normalised email address
        /// </summary>
        /// <param name="normalizedEmail">The normalised email address to find the user by</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        async Task<GosApplicationUser> IUsersService.FindUserByEmailAsync(string normalizedEmail, CancellationToken cancellationToken, string jwtToken)
        {
            GosApplicationUser user = null;

            var response = await this.HttpGetAsync<GosApplicationUser>("api/v1/Users/email/" + normalizedEmail, jwtToken);
            if (response.HasResult)
            {
                user = response.Result;
            }

            return user;
        }

        /// <summary>
        /// Gets a list of roles for a user
        /// </summary>
        /// <param name="userId">The ID of the user to get the roles for</param>
        /// <param name="cancellationToken">Token to propogate cancellation</param>
        /// <returns></returns>
        async Task<IList<string>> IUsersService.GetRolesByUserIdAsync(string userId, CancellationToken cancellationToken, string jwtToken)
        {
            IList<string> roles = new List<string>();

            var response = await this.HttpGetAsync<IList<string>>(string.Format("api/v1/Users/{0}/Roles", userId), jwtToken);

            if (response.HasResult)
            {
                roles = response.Result;
            }

            return roles;
        }

        /// <summary>
        /// Send an email notification to New User
        /// </summary>
        /// <param name="user">Recipient information</param>
        /// <returns></returns>
        async Task<HttpResponseMessage> IUsersService.NewUserEmailAsync(GosApplicationUser user)
        {
            var response = await this.HttpPostAsync("api/v1/users/newuseremail", user);

            return response;
        }

        /// <summary>
        /// Send an email notification to New User Linked
        /// </summary>
        /// <param name="user">Recipient information</param>
        /// <returns></returns>
        async Task<HttpResponseMessage> IUsersService.NewUserLinkedEmailAsync(GosApplicationUser user, string company, string jwtToken)
        {
            JObject jo = JObject.FromObject(user);
            jo.Add("Company", company);
            var json = JsonConvert.SerializeObject(jo);

            var response = await this.HttpPostAsync("api/v1/users/newuserlinkedemail/", new StringContent(json, Encoding.UTF8, "application/json"), jwtToken);

            return response;
        }

        /// <summary>
        /// Retrieve Invite details from link
        /// </summary>
        /// <param name="link">Link to token</param>
        /// <returns>Returns invited user association information in order create user</returns>
        async Task<GosInvitedUserAssociation> IUsersService.GetInviteDetailsByLinkAsync(string link)
        {
            var response = await this.HttpGetAsync<GosInvitedUserAssociation>(string.Format("api/v1/Users/link/{0}", link));

            // If result found and its not expired
            if (response.HasResult && response.Result.ExpireInviteLink > DateTime.Now)
            {
                return response.Result;
            }
            return null;
        }

        /// <summary>
        /// Assign Role To User
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>Http response</returns>
        public async Task<string> AssignRoleToUserAsync(GosApplicationUser user, string jwtToken)
        {
            var json = JsonConvert.SerializeObject(user);
            var response = await this.HttpPostAsync("api/v1/users/role", new StringContent(json, Encoding.UTF8, "application/json"), jwtToken);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Get Organisation Users
        /// </summary>
        /// <param name="organisationId">Organisation Id</param>
        /// <returns>List of GosApplicationUser</returns>
        async Task<List<GosApplicationUser>> IUsersService.GetOrganisationUsers(int organisationId, string jwtToken)
        {
            var result = new List<GosApplicationUser>();

            var response = await this.HttpGetAsync<List<GosApplicationUser>>("api/v1/users/organisation/" + organisationId.ToString(), jwtToken);

            if (response.HasResult)
            {
                result = response.Result;
            }

            return result;
        }

        /// <summary>
        /// Add an organisation Users by creating a new account or linking an existing user to a role
        /// </summary>
        /// <param name="requestedBy">User that requests to add user</param>
        /// <param name="newUser">New User</param>
        /// <param name="organisationId">Organisation Id</param>
        /// <returns>Status  of the adding a user</returns>

        async Task<AddNewGosApplicationUserResult> IUsersService.AddUserAsync(AddNewGosApplicationUser addUserRequestedBy, string jwtToken)
        {
            var result = new AddNewGosApplicationUserResult();

            var json = JsonConvert.SerializeObject(addUserRequestedBy);
            var response = await this.HttpPostAsync(string.Format("api/v1/users/adduser"), addUserRequestedBy, jwtToken);

            if (response.IsSuccessStatusCode)
            {
                var jsonResult = response.Content.ReadAsStringAsync().Result;
                result = JsonConvert.DeserializeObject<AddNewGosApplicationUserResult>(jsonResult);
            }
            return result;
        }

        /// <summary>
        /// Remove User sets the Organisation and Role to 0
        /// </summary>
        /// <param name="user">User to remove from fromn Roles </param>
        /// <returns>Http Post reponse</returns>
        async Task<string> IUsersService.RemoveUserAsync(string userId, string jwtToken)
        {
            var response = await this.HttpPostAsync("api/v1/users/remove", userId, jwtToken);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Requests a forgotten password notification is sent
        /// </summary>
        /// <param name="command">The command containing details for the notification</param>
        /// <returns>Returns an HTTP response indicating status of call</returns>
        async Task<HttpResponseMessage> IUsersService.IssueForgottenPasswordNotification(IssueForgottenPasswordNotificationCommand command)
        {
            return await this.HttpPostAsync("api/v1/users/IssueForgottenPasswordNotification", command);
        }

        /// <summary>
        /// Validates the reset password token
        /// </summary>
        /// <param name="token">The reset password token</param>
        /// <returns>Returns an HTTP response indicating status of call</returns>
        async Task<HttpResponseMessage> IUsersService.ValidateResetPasswordToken(string token)
        {
            return await this.HttpClient.GetAsync(string.Format("api/v1/users/ValidateResetPasswordToken/{0}", token));
        }

        /// <summary>
        /// Requests a Password reset
        /// </summary>
        /// <param name="command">The command containing details of the password reset</param>
        /// <returns>Returns an HTTP response indicating status of call</returns>
        async Task<HttpResponseMessage> IUsersService.ResetPassword(ResetPasswordCommand command)
        {
            return await this.HttpPostAsync("api/v1/users/ResetPassword", command);
        }

        /// <summary>
        /// Gets the admin users
        /// </summary>
        /// <param name="jwtToken"></param>
        /// <returns></returns>
        async Task<HttpObjectResponse<IList<GosApplicationUser>>> IUsersService.GetAdminUsers(string jwtToken)
        {
            return await this.HttpGetAsync<IList<GosApplicationUser>>("api/v1/users/adminusers", jwtToken);
        }

        /// <summary>
        /// Add an administrator user
        /// </summary>
        /// <param name="userEmail">Email address of the user to be made an administrator</param>
        /// <param name="jwtToken">Token of the user making the request</param>
        /// <returns></returns>
        async Task<AddNewAdminUserResult> IUsersService.AddAdminUser(AddNewGosApplicationUser user, string jwtToken)
        {
            var response = await this.HttpPostAsync("api/v1/users/addAdminUser",
                new SetAdminRoleCommand { EmailAddress = user.Email },
                jwtToken);

            var jsonResult = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<AddNewAdminUserResult>(jsonResult);
            
        }

        /// <summary>
        /// Removes administrator role for an user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="jwtToken"></param>
        /// <returns></returns>
        async Task<HttpResponseMessage> IUsersService.RemoveAdministratorRole(string userId, string jwtToken)
        {
            return await this.HttpPostAsync("api/v1/users/removeAdminRole", userId, jwtToken);
        }

        /// <summary>
        /// Returns the performance data relating to registrations
        ///  - Number of invitations without an account, per month
        ///  - Number of invitations, per month
        ///  - Total number of users, per month
        /// </summary>
        /// <param name="jwtToken">JWT Token of the user making the request</param>
        /// <returns>List of the performance data for registrations</returns>
        async Task<List<RegistrationsPerformanceData>> IUsersService.RegistrationsPerformanceData(string jwtToken)
        {
            var response = await this.HttpGetAsync<List<RegistrationsPerformanceData>>("/api/v1/PerformanceData/Registrations", jwtToken);

            if (response.HasResult)
            {
               return response.Result;
            }
            return new List<RegistrationsPerformanceData>();
        }

        #endregion IUsersService implementation
    }
}
