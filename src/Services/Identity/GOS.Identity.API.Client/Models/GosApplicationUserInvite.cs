﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Text;

namespace DfT.GOS.Identity.API.Client.Models
{
    /// <summary>
    /// Gos Application User Invite
    /// </summary>
    public class GosApplicationUserInvite
    {
        /// <summary>
        /// The organisation ID to which the user belong
        /// </summary>
        public int? OrganisationId { get; set; }

        /// <summary>
        /// Company name
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// User role
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// Full name
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; set; }
    }
}
