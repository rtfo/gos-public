﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Identity;
using System;

namespace DfT.GOS.Identity.API.Client.Models
{
    /// <summary>
    /// Represents the gos application user 
    /// </summary>
    public class GosApplicationUser : IdentityUser
    {
        /// <summary>
        /// Full name of the user
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// The organisation ID to which the user belong
        /// </summary>
        public Nullable<int> OrganisationId { get; set; }

        /// <summary>
        /// User role
        /// </summary>
        public string Role { get; set; }
    }
}
