﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Identity.API.Client.Models
{
    /// <summary>
    /// DTO for a service request to perform authentcation
    /// </summary>
    public class AuthenticateServiceRequest
    {
        /// <summary>
        /// Service identifier
        /// </summary>
        [Required]
        public string ServiceId { get; set; }
    }
}
