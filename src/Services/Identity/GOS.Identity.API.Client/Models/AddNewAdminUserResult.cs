﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Net.Http;

namespace DfT.GOS.Identity.API.Client.Models
{
    /// <summary>
    /// Object with the status and  message of making the user an administrator
    /// </summary>
    public class AddNewAdminUserResult : HttpResponseMessage
    {
        /// <summary>
        /// Status message if there is an error
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Flag to indicate that the account is an administrator
        /// </summary>
        public bool IsUserAdministrator { get; set; }

        /// <summary>
        /// Flag to indicate if an invite has been sent
        /// (or if the user already existed)
        /// </summary>
        public bool UserInvited { get; set; }
    }
}
