﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Identity.API.Client.Models
{
    /// <summary>
    /// Configuration settings for authentication
    /// </summary>
    public class AuthenticationConfiguration
    {
        /// <summary>
        /// The amount of time to lockout a user account after it has been marked as locked out
        /// </summary>
        public int DefaultLockoutTimeSpan { get; set; }

        /// <summary>
        /// The number of invalid auth attempts before a user account is locked out
        /// </summary>
        public int MaxFailedAccessAttempts { get; set; }
    }
}
