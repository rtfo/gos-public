﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Security.Claims;

namespace DfT.GOS.Identity.API.Client.Models
{
    /// <summary>
    /// DTO for the result of an authentication attempt, containing the JWT Token if it was issued
    /// </summary>
    public class AuthenticationResult
    {
        /// <summary>
        /// Indicates whether the user was successfully authenticated against the user repository
        /// </summary>
        public bool Succeeded { get; set; }

        /// <summary>
        /// Indicates whether the user account has been Locked out
        /// </summary>
        public bool IsLockedOut { get; set; }

        /// <summary>
        /// The JWT token that was issued
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// A dictionary of security claims a user has
        /// </summary>
        public Dictionary<string, string> Claims { get; set; }

        /// <summary>
        /// UserName for the authenticated user
        /// </summary>
        public string UserName { get; set; }
    }
}
