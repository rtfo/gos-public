﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.Identity.API.Client.Models
{
    /// <summary>
    /// All required fields from an invite to create a user
    /// </summary>
    public class GosInvitedUserAssociation
    {
        /// <summary>
        /// Organisation ID for whom invited the user
        /// </summary>
        public int? OrganisationId { get; set; }
        /// <summary>
        /// Organisation Name for whom invited the user
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// The role of the user to be created
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// Email address of where the invite was sent
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// The expiry date for the invite sent to user email
        /// </summary>
        public DateTime ExpireInviteLink { get; set; }

    }
}
