﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Text;

namespace DfT.GOS.Identity.API.Client.Models
{
    /// <summary>
    /// Object with the status and  messageof adding new user
    /// </summary>
    public class AddNewGosApplicationUserResult
    {
        /// <summary>
        /// Status message if there is an error
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Flag to indicate that the account is linked
        /// </summary>
        public bool UserAssociatedWithOrganisation { get; set; }
    }
}
