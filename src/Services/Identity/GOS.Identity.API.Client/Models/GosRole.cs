﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Identity;

namespace DfT.GOS.Identity.API.Client.Models
{
    /// <summary>
    /// Represents GOS User role
    /// </summary>
    public class GosRole : IdentityRole
    {
    }
}
