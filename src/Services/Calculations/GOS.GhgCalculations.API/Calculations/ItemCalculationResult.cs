﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgCalculations.API.Calculations
{
    /// <summary>
    /// The Result of an Item Calculation
    /// </summary>
    public class ItemCalculationResult
    {
        #region Properties

        /// <summary>
        /// Gets the Credits from the calculation
        /// </summary>
        public decimal Credits { get; private set; }

        /// <summary>
        /// Gets the obligation from the calculation
        /// </summary>
        public decimal Obligation { get; private set; }

        /// <summary>
        /// The difference between the Carbon Intensity and the target level Intensity 
        /// </summary>
        public decimal DifferenceToTarget { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the result based upon all variables
        /// </summary>
        /// <param name="credits">The credits created by the calculation</param>
        /// <param name="obligation">The obligation created by the calculation</param>
        /// <param name="differenceToTarget">The difference between the Carbon Intensity and the target level Intensity</param>
        public ItemCalculationResult(decimal credits, decimal obligation, decimal differenceToTarget)
        {
            this.Credits = credits;
            this.Obligation = obligation;
            this.DifferenceToTarget = differenceToTarget;
        }

        #endregion Constructors
    }
}
