﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgCalculations.API.Calculations
{
    /// <summary>
    /// V1 of the GHG calculation
    /// 
    /// ==============
    /// IMPORTANT NOTE
    /// ==============
    /// 
    /// Changing this class may affect historic data. If this is not the intention, please create a V2 of the class.
    /// 
    /// </summary>
    public class Calculation_v1
    {
        #region Public Methods        

        /// <summary>
        /// Performs the calculation across volume based items, energy based items and emissions reduction items
        /// </summary>
        /// <param name="command">The command to perform the calculation on</param>
        /// <returns>Returns calculations for each calculation type, plus aggregated calculation information</returns>
        public async Task<CalculationResult> PerformCalculation(CalculationCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            var volumeCalculationResultTask = this.PerformVolumeCalculation(command.VolumeItems
                , command.GhgThresholdGrammesCO2PerMJ
                , command.DeMinimisLitres
                , command.NoDeMinimisLitres
                , command.GhgLowVolumeSupplierDeductionKGCO2);

            var energyCalculationResultTask = this.PerformEnergyCalculation(command.EnergyItems, command.GhgThresholdGrammesCO2PerMJ);
            var emmissionsReductionCalculationTask = this.PerformEmissionsReductionCalculation(command.EmissionsReductionItems);

            await Task.WhenAll(volumeCalculationResultTask, energyCalculationResultTask, emmissionsReductionCalculationTask);

            var volumeCalculationResult = await volumeCalculationResultTask;
            var energyCalculationResult = await energyCalculationResultTask;
            var emmissionsReductionCalculation = await emmissionsReductionCalculationTask;

            //Net Total Credits
            var netTotalCredits = volumeCalculationResult.TotalVolumeCredits
                + energyCalculationResult.TotalEnergyCredits
                + emmissionsReductionCalculation.TotalEmissionsReductionCredits;

            //Total Credits With Obligation Period Adjustments (Note, currently there aren't any adjustments to make to the credits)
            var totalCreditsWithObligationPeriodAdjustments = netTotalCredits;

            //Net Total Obligation
            var netTotalObligation = volumeCalculationResult.NetTotalVolumeObligation
                + energyCalculationResult.TotalEnergyObligation
                + emmissionsReductionCalculation.TotalEmissionsReductionObligation;

            //Total Obligation With Obligation Period Adjustments
            var totalObligationWithObligationPeriodAdjustments = volumeCalculationResult.TotalVolumeObligationWithObligationPeriodAdjustments
                    + energyCalculationResult.TotalEnergyObligation
                    + emmissionsReductionCalculation.TotalEmissionsReductionObligation;

            var result = new CalculationResult(command.SupplierOrganisationId
                , command.ObligationPeriodId
                , command.GhgThresholdGrammesCO2PerMJ
                , volumeCalculationResult
                , energyCalculationResult
                , emmissionsReductionCalculation
                , netTotalCredits
                , totalCreditsWithObligationPeriodAdjustments
                , netTotalObligation
                , totalObligationWithObligationPeriodAdjustments);           

            return result;
        }

        #endregion Public Methods

        #region Protected Methods

        /// <summary>
        /// Performs the calculation of credits and obligation for an item
        /// </summary>
        /// <param name="ghgTargetLevel">The GHG Target Level</param>
        /// <param name="carbonIntensity">The item's Carbon Intensity</param>
        /// <param name="energySupplied">The amount of energy supplied</param>
        /// <returns>Returns the result of the item calculation</returns>
        protected ItemCalculationResult PerformItemCalculation(decimal ghgTargetLevel, Nullable<decimal> carbonIntensity, decimal energySupplied)
        {
            decimal credits = 0;
            decimal obligation = 0;
            decimal differenceToTarget = 0;

            if (carbonIntensity.HasValue)
            {
                differenceToTarget = ghgTargetLevel - carbonIntensity.Value;

                decimal calculation = Math.Round(((ghgTargetLevel - carbonIntensity.Value) * energySupplied) / 1000, 2);

                if (differenceToTarget > 0)
                {
                    credits = calculation;
                }
                else if (differenceToTarget < 0)
                {
                    obligation = calculation * -1;
                }
                else
                {
                    credits = 0;
                    obligation = 0;
                }
            }            

            return new ItemCalculationResult(credits, obligation, differenceToTarget);
        }

        /// <summary>
        /// Performs the calculation for volume based items
        /// </summary>
        /// <param name="items">The volume based items to perform the calculation on</param>
        /// <param name="ghgThresholdGrammesCO2PerMJ">The GHG Threshold in Grammes of CO2 per MJ to apply against the calulation</param>
        /// <param name="deMinimisLitres">De Minimis value (in Litres)</param>
        /// <param name="noDeMinimisLitres">No De Minimis value (in Litres)</param>
        /// <param name="ghgLowVolumeSupplierDeductionKGCO2">GHG Low Volume Supplier Deduction (in Kg CO2)</param>
        /// <returns>Returns volume calculation result, including basic aggregation information</returns>
        protected Task<VolumeCalculationResult> PerformVolumeCalculation(IList<VolumeCalculationItem> items
            , decimal ghgThresholdGrammesCO2PerMJ
            , int deMinimisLitres
            , int noDeMinimisLitres
            , int ghgLowVolumeSupplierDeductionKGCO2)
        {
            return Task.Run(() =>
            {
                var itemResults = new List<VolumeCalculationItemResult>();

                foreach (var item in items)
                {                                    
                    var energySupplied = item.EnergyDensity.GetValueOrDefault(0) * item.Volume;
                    var itemCalculationResult = this.PerformItemCalculation(ghgThresholdGrammesCO2PerMJ, item.CarbonIntensity, energySupplied);

                    var itemResult = new VolumeCalculationItemResult(item.Reference
                        , item.FuelCategoryReference
                        , item.Description
                        , item.Volume
                        , item.EnergyDensity
                        , item.CarbonIntensity
                        , energySupplied
                        , itemCalculationResult.Credits
                        , itemCalculationResult.Obligation
                        , itemCalculationResult.DifferenceToTarget);

                    itemResults.Add(itemResult);
                }

                //Total Volume Credits
                var totalVolumeCredits = itemResults.Sum(i => i.Credits);

                // Net Total Volume Obligation
                var netTotalVolumeObligation = itemResults.Sum(i => i.Obligation);

                // Total Volume Supplied
                var totalVolumeSupplied = itemResults.Sum(i => i.Volume);

                // Total Volume Above Ghg Target
                var totalVolumeAboveGhgTarget = itemResults.Where(i => i.GhgIntensity > ghgThresholdGrammesCO2PerMJ)
                                                    .Sum(i => i.Volume);

                // Low Volume Supplier Deduction (for the entire obligation period)
                var obligationPeriodLowVolumeSupplierDeduction = this.GetObligationPeriodLowVolumeSupplierDeduction(deMinimisLitres
                        , noDeMinimisLitres
                        , totalVolumeSupplied
                        , netTotalVolumeObligation
                        , ghgLowVolumeSupplierDeductionKGCO2
                        , totalVolumeAboveGhgTarget);

                // Total Volume Obligation With (Obligation Period) Adjustments
                decimal totalVolumeObligationWithObligationPeriodAdjustments = 0;
                if (netTotalVolumeObligation >= 0)
                {
                    totalVolumeObligationWithObligationPeriodAdjustments = netTotalVolumeObligation - obligationPeriodLowVolumeSupplierDeduction;
                }

                var result = new VolumeCalculationResult(itemResults
                   , ghgThresholdGrammesCO2PerMJ
                   , ghgLowVolumeSupplierDeductionKGCO2
                   , deMinimisLitres
                   , noDeMinimisLitres
                   , totalVolumeCredits
                   , netTotalVolumeObligation
                   , totalVolumeSupplied
                   , obligationPeriodLowVolumeSupplierDeduction
                   , totalVolumeAboveGhgTarget
                   , totalVolumeObligationWithObligationPeriodAdjustments);

                return result;
            });
        }

        /// <summary>
        /// Performs the calculation for Energy based items
        /// </summary>
        /// <param name="items">The energy based items to perform the calculation on</param>
        /// <param name="ghgThresholdGrammesCO2PerMJ">The GHG Threshold in Grammes of CO2 per MJ to apply against the calulation</param>
        /// <returns>Returns the energy calculation result, including basic aggregation information</returns>
        protected Task<EnergyCalculationResult> PerformEnergyCalculation(IList<EnergyCalculationItem> items
            , decimal ghgThresholdGrammesCO2PerMJ)
        {
            return Task.Run(() =>
            {                
                var itemResults = new List<EnergyCalculationItemResult>();

                foreach (var item in items)
                {
                    var itemCalculationResult = this.PerformItemCalculation(ghgThresholdGrammesCO2PerMJ, item.GhgIntensity, item.EnergySupplied);

                    var itemResult = new EnergyCalculationItemResult(item.Reference
                        , item.Description
                        , item.EnergySupplied
                        , item.GhgIntensity
                        , itemCalculationResult.Credits
                        , itemCalculationResult.Obligation
                        , itemCalculationResult.DifferenceToTarget);

                    itemResults.Add(itemResult);
                }

                //Total Energy Supplied
                var totalEnergySupplied = itemResults.Sum(i => i.EnergySupplied);

                //Total Energy Obligation
                var totalEnergyObligation = itemResults.Sum(i => i.Obligation);

                //Total Energy Credits
                var totalEnergyCredits = itemResults.Sum(i => i.Credits);

                var result = new EnergyCalculationResult(itemResults
                    , ghgThresholdGrammesCO2PerMJ
                    , totalEnergySupplied
                    , totalEnergyObligation
                    , totalEnergyCredits);

                return result;
            });
        }

        /// <summary>
        /// Performs the calculation for Emissions Reduction based items
        /// </summary>
        /// <param name="items">The emissions reduction based items to perform the calculation on</param>
        /// <returns>Returns the energy calculation result, including basic aggregation information</returns>
        protected Task<EmissionsReductionCalculationResult> PerformEmissionsReductionCalculation(IList<EmissionsReductionCalculationItem> items)
        {
            return Task.Run(() =>
            {
                var itemResults = new List<EmissionsReductionCalculationItemResult>();

                foreach (var item in items)
                {
                    var itemResult = new EmissionsReductionCalculationItemResult(reference: item.Reference
                        , description: item.Description
                        , amount: item.Amount
                        , credits: item.Amount
                        , obligation: 0
                        , internalReference: item.InternalReference);

                    itemResults.Add(itemResult);
                }

                // Total Emissions Reduction Obligation
                var totalEmissionsReductionObligation = itemResults.Sum(i => i.Obligation);

                // Total Emissions Reduction Credits
                var totalEmissionsReductionCredits = itemResults.Sum(i => i.Credits);

                var result = new EmissionsReductionCalculationResult(itemResults
                    , totalEmissionsReductionObligation
                    , totalEmissionsReductionCredits);

                return result;
            });
        }

        /// <summary>
        /// Gets the Low Volume Supplier Deduction that will be applied across the entire obligation period
        /// </summary>
        /// <param name="deMinimisLitres">The Deminimis value for the obligation period</param>
        /// <param name="noDeMinimisLitres">The No-Deminimis value for the obligation period</param>
        /// <param name="totalVolumeSupplied">The total volume supplied</param>
        /// <param name="netTotalVolumeObligation">The Net Total Volume Obligation</param>
        /// <param name="ghgLowVolumeSupplierDeductionKGCO2">The GHG low volume supplier deduction amount</param>
        /// <param name="totalVolumeAboveGhgTarget">The total volume above the GHG Target</param>
        /// <returns>Returns the Low Volume Supplier Deduction, which is used to reduce the net obligation</returns>
        protected decimal GetObligationPeriodLowVolumeSupplierDeduction(decimal deMinimisLitres
            , decimal noDeMinimisLitres
            , decimal totalVolumeSupplied
            , decimal netTotalVolumeObligation
            , decimal ghgLowVolumeSupplierDeductionKGCO2
            , decimal totalVolumeAboveGhgTarget)
        {
            if (totalVolumeSupplied <= deMinimisLitres)
            {
                // If you supply less than or equal to the DeMinimis Total fuel (e.g. 450,000 Units), there is no obligation
                if (netTotalVolumeObligation < 0)
                {
                    return 0;
                }
                else
                {
                    return netTotalVolumeObligation;
                }
            }
            else
            {
                if (totalVolumeAboveGhgTarget < noDeMinimisLitres)
                {
                    // If you supply > DeMinimis (e.g. 450,000) units (total fuel) but the amount of 'fuel above the GHG target' 
                    // is < NoDeMinimis (e.g. 10,000,000) there is a low volume supplier reduction applied to the obligation                    
                    if (netTotalVolumeObligation > ghgLowVolumeSupplierDeductionKGCO2)
                    {
                        return ghgLowVolumeSupplierDeductionKGCO2;
                    }
                    else
                    {
                        // If the obligation would become negative due to the low volume supplier deduction 
                        // being applied, it should be 0.
                        return netTotalVolumeObligation;
                    }
                }
                else
                {
                    // if you supply more than the NoDeMinimis (e.g. 10,000,000) units of 'fuel above the GHG Target' (regardless of the 
                    // total fuel supplied), there is no low volume supplier reduction
                    return 0;
                }
            }
        }

        #endregion Protected Methods
    }
}
