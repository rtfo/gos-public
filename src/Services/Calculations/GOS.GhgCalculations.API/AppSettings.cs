﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Http;

namespace DfT.GOS.GhgCalculations.API
{
    /// <summary>
    /// Application Settings for the service
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// The URL of the reporting periods service
        /// </summary>
        public string ReportingPeriodsApiUrl { get; set; }

        /// <summary>
        /// Settings for HTTP client calls
        /// </summary>
        public HttpClientAppSettings HttpClient { get; set; }
    }
}
