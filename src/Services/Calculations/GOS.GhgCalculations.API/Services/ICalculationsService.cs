﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using System.Threading.Tasks;

namespace DfT.GOS.GhgCalculations.API.Services
{
    /// <summary>
    /// Calculations Service interface definition
    /// </summary>
    public interface ICalculationsService
    {
        /// <summary>
        /// Performs a calculation for volume based, energy based, and emissions reductions items
        /// </summary>
        /// <param name="command">The command to perform the calculation on</param>
        /// <returns>Returns a calculation result, including basic aggregation information</returns>
        Task<CalculationResult> Calculate(CalculationCommand command);
    }
}
