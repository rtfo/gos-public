﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgCalculations.API.Calculations;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using System.Threading.Tasks;

namespace DfT.GOS.GhgCalculations.API.Services
{
    /// <summary>
    /// Ghg Calculations Service
    /// </summary>
    public class CalculationsService : ICalculationsService
    {
        #region IGhgCalculationsService implementation

        /// <summary>
        /// Performs a calculation for volume based, energy based, and emissions reductions items
        /// 
        /// ==============
        /// IMPORTANT NOTE
        /// ==============
        /// This method handles versioning of calculations to enable the calculation to vary by obligation 
        /// period, or time period, without affecting historic data calculations.
        /// 
        /// </summary>
        /// <param name="command">The command to perform the calculation on</param>
        /// <returns>Returns a net breakdown result for the List, including basic aggregation information</returns>
        async Task<CalculationResult> ICalculationsService.Calculate(CalculationCommand command)
        {
            // ==============
            // IMPORTANT NOTE
            // ==============
            // Add further versions of the calculation here over time (see method header)

            var calculation = new Calculation_v1();

            return await calculation.PerformCalculation(command);
        }

        #endregion IGhgCalculationsService implementation
    }
}
