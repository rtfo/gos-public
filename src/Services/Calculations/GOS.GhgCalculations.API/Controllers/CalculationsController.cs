﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Threading.Tasks;
using DfT.GOS.GhgCalculations.API.Services;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace Dft.GOS.GhgCalculations.API.Controllers
{
    /// <summary>
    /// Gets GHG Calculations Details
    /// </summary>
    [Route("api/v1/[controller]")]
    public class CalculationsController : ControllerBase
    {
        #region Properties

        private ICalculationsService GhgCalculationsService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependancies
        /// </summary>
        /// <param name="calculationsService">The calculation service</param>
        public CalculationsController(ICalculationsService calculationsService)
        {
            this.GhgCalculationsService = calculationsService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Performs a calculation for volume based, energy based, and emissions reductions items. Returns a complete
        /// breakdown of the resulting calculation for volume based, energy based, and emissions reductions items, plus
        /// aggregations across each of these.
        /// </summary>
        /// <param name="command">The command containing information to perform the calculation on. Note that, for performance reasons, a number
        /// of the command parameters aren't validated by the calculation API, instead the service simply returns what is was passed. The following
        /// command parameters aren't validated - supplierOrganisationId, obligationPeriodId.</param>
        /// <returns>Returns the calculation result</returns>
        [HttpPost("GhgCalculationBreakdown")]
        public async Task<IActionResult> GhgCalculationBreakdown([FromBody] CalculationCommand command)
        {
            if (ModelState.IsValid)
            {                
                try
                {
                    var result = await this.GhgCalculationsService.Calculate(command);
                    return Ok(result);
                }
                catch(OverflowException)
                {
                    return BadRequest("The supplied calculation command results in an overflow, due to parameters exceeding maximum combined values");
                } 
            }
            else
            {
                return BadRequest("The supplied Calculation Command was invalid");
            }
        }

        /// <summary>
        /// Performs a calculation for volume based, energy based, and emissions reductions items. Returns a
        /// summary of the resulting calculation, reporting aggregate values.
        /// </summary>
        /// <param name="command">The command containing information to perform the calculation on. Note that, for performance reasons, a number
        /// of the command parameters aren't validated by the calculation API, instead the service simply returns what is was passed. The following
        /// command parameters aren't validated - supplierOrganisationId, obligationPeriodId.</param>
        /// <returns>Returns a summary of the calculation result</returns>
        [HttpPost("GhgCalculationSummary")]
        public async Task<IActionResult> GhgCalculationSummary([FromBody] CalculationCommand command)
        {
            if (ModelState.IsValid)
            {               
                try
                { 
                    var breakdownResult = await this.GhgCalculationsService.Calculate(command);

                    var summaryResult = new CalculationSummaryResult(breakdownResult.SupplierOrganisationId
                        , breakdownResult.ObligationPeriodId
                        , breakdownResult.GhgTargetLevel
                        , breakdownResult.NetTotalCredits
                        , breakdownResult.TotalCreditsWithObligationPeriodAdjustments
                        , breakdownResult.NetTotalObligation
                        , breakdownResult.TotalObligationWithObligationPeriodAdjustments);

                    return Ok(summaryResult);
                }
                catch (OverflowException)
                {
                    return BadRequest("The supplied calculation command results in an overflow, due to parameters exceeding maximum combined values");
                }                
            }
            else
            {
                return BadRequest("The supplied Calculation Command was invalid");
            }
        }

        #endregion Methods
    }
}