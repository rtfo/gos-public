﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgCalculations.Common.Models;

namespace DfT.GOS.GhgCalculations.Orchestration
{
    /// <summary>
    /// Groups a calculation summary result with an application ID
    /// </summary>
    public class ApplicationCalculationSummaryResult
    {
        #region Properties

        public int ApplicationId { get; set; }
        public CalculationSummaryResult Result { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public ApplicationCalculationSummaryResult(int applicationId
            , CalculationSummaryResult result)
        {
            this.ApplicationId = applicationId;
            this.Result = result;
        }

        #endregion Constructors
    }
}
