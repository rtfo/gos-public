﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgApplications.Common.Models;
using DfT.GOS.GhgApplications.API.Client.Services;
using DfT.GOS.GhgCalculations.API.Client.Services;
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using DfT.GOS.GhgFuels.Common.Models;
using DfT.GOS.RtfoApplications.API.Client.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgCalculations.Orchestration
{
    /// <summary>
    /// Orchestration class for getting GHG calculations for an application
    /// </summary>
    public class ApplicationsCalculationOrchestrator
    {
        #region Enums

        //TODO: Remove this hack (put the fuel categories enum in one place)

        /// <summary>
        /// Enum for fuel categories
        /// </summary>
        public enum FuelCategories
        {
            [Display(Name = "Electricity")]
            Electricity = 1,
            [Display(Name = "UER")]
            UER = 2,
            [Display(Name = "Fossil Gas")]
            FossilGas = 3
        }

        #endregion Enums

        #region Constants

        /// <summary>
        /// A Unique reference for Admin Consignment Liquid/Gases
        /// </summary>
        public const string VolumeFuelCategory_AdminConsignment = "AdminConsignment";

        /// <summary>
        /// A Unique reference for Volumes Liquid/Gases
        /// </summary>
        public const string VolumeFuelCategory_Volume = "Volume";

        /// <summary>
        /// A Unique reference for Fossil Gas Liquid/Gases
        /// </summary>
        public const string VolumeFuelCategory_FossilGas = "FossilGas";

        #endregion Constants

        #region Properties

        private IApplicationsService ApplicationsService { get; set; }
        private ICalculationsService CalculationsService { get; set;}
        private IList<FuelType> FuelTypes { get; set; }
        private string JwtToken { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="applicationsService">The Application Service</param>     
        /// <param name="calculationsService">The Calculations service</param>
        /// <param name="fuelTypes">The list of fuel types</param>
        /// <param name="jwtToken">The Token to use to connect to remote services</param>
        public ApplicationsCalculationOrchestrator(IApplicationsService applicationsService
            , ICalculationsService calculationsService
            , IList<FuelType> fuelTypes
            , string jwtToken)
        {
            this.ApplicationsService = applicationsService;
            this.CalculationsService = calculationsService;
            this.FuelTypes = fuelTypes;
            this.JwtToken = jwtToken;
        }

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Gets A Calculation Summary for an application
        /// </summary>
        /// <param name="applicationId">The ID of the application</param>
        /// <param name="supplierOrganisationId">The ID of the supplier</param>
        /// <param name="obligationPeriodId">The ID of the Obligation Period</param>
        /// <param name="ghgThresholdGrammesCO2PerMJ">The GHG Threshold in Grammes of CO2 per MJ to apply against the calulation</param>
        /// <param name="deMinimisLitres">De Minimis value (in Litres)</param>
        /// <param name="noDeMinimisLitres">No De Minimis value (in Litres)</param>
        /// <param name="ghgLowVolumeSupplierDeductionKGCO2">GHG Low Volume Supplier Deduction (in Kg CO2)</param> 
        /// <returns>Returns a Calculation summary wrapped with an application ID</returns>
        public async Task<ApplicationCalculationSummaryResult> GetCalculationSummaryForApplication(int applicationId
            , int supplierOrganisationId
            , int obligationPeriodId
            , decimal ghgThresholdGrammesCO2PerMJ
            , int deMinimisLitres
            , int noDeMinimisLitres
            , int ghgLowVolumeSupplierDeductionKGCO2)
        {
            var applicationItemsReponse = await this.ApplicationsService.GetApplicationItems(applicationId, this.JwtToken);

            var applicationCalculationSummaryResult = new ApplicationCalculationSummaryResult(applicationId
                , await this.CalculationsService.GhgCalculationSummary(this.CreateCalculationCommand(supplierOrganisationId
                                                                        , obligationPeriodId
                                                                        , ghgThresholdGrammesCO2PerMJ
                                                                        , deMinimisLitres
                                                                        , noDeMinimisLitres
                                                                        , ghgLowVolumeSupplierDeductionKGCO2
                                                                        , applicationItemsReponse.Result
                                                                        , adminConsignments: null
                                                                        , volumes: null)));
            return applicationCalculationSummaryResult;
        }

        /// <summary>
        /// Gets a calculation result for a list of application items
        /// </summary>
        /// <param name="applicationItems">The application items to create the command for</param>
        /// <param name="supplierOrganisationId">The ID of the supplier</param>
        /// <param name="obligationPeriodId">The Obligation Period ID</param>
        /// <param name="ghgThresholdGrammesCO2PerMJ">The GHG Threshold in Grammes of CO2 per MJ to apply against the calulation</param>
        /// <param name="deMinimisLitres">De Minimis value (in Litres)</param>
        /// <param name="noDeMinimisLitres">No De Minimis value (in Litres)</param>
        /// <param name="ghgLowVolumeSupplierDeductionKGCO2">GHG Low Volume Supplier Deduction (in Kg CO2)</param>
        /// <returns>Returns a calculation breakdown</returns>
        public async Task<CalculationResult> GetCalculationBreakdownForApplicationItems(IList<ApplicationItemSummary> applicationItems
            , int supplierOrganisationId
            , int obligationPeriodId
            , decimal ghgThresholdGrammesCO2PerMJ
            , int deMinimisLitres
            , int noDeMinimisLitres
            , int ghgLowVolumeSupplierDeductionKGCO2)
        {           
            return await this.CalculationsService.GhgCalculationBreakdown(this.CreateCalculationCommand(supplierOrganisationId
                                                                        , obligationPeriodId
                                                                        , ghgThresholdGrammesCO2PerMJ
                                                                        , deMinimisLitres 
                                                                        , noDeMinimisLitres 
                                                                        , ghgLowVolumeSupplierDeductionKGCO2
                                                                        , applicationItems
                                                                        , adminConsignments: null
                                                                        , volumes: null));
        }

        /// <summary>
        /// Gets a calculation result for a list of application items, volumes and admin consignments
        /// </summary>
        /// <param name="applicationItems">The application items to create the command for</param>
        /// <param name="adminConsignments">The admin consignments to create the command for</param>
        /// <param name="volumes">The volumes to create the command for</param>
        /// <param name="supplierOrganisationId">The ID of the supplier</param>
        /// <param name="obligationPeriodId">The Obligation Period ID</param>
        /// <param name="ghgThresholdGrammesCO2PerMJ">The GHG Threshold in Grammes of CO2 per MJ to apply against the calulation</param>
        /// <param name="deMinimisLitres">De Minimis value (in Litres)</param>
        /// <param name="noDeMinimisLitres">No De Minimis value (in Litres)</param>
        /// <param name="ghgLowVolumeSupplierDeductionKGCO2">GHG Low Volume Supplier Deduction (in Kg CO2)</param>
        /// <returns>Returns a calculation breakdown</returns>
        public async Task<CalculationResult> GetCalculationBreakdownForApplicationItems(IList<ApplicationItemSummary> applicationItems
            , IEnumerable<AdminConsignment> adminConsignments
            , IEnumerable<NonRenewableVolume> volumes
            , int supplierOrganisationId
            , int obligationPeriodId
            , decimal ghgThresholdGrammesCO2PerMJ
            , int deMinimisLitres
            , int noDeMinimisLitres
            , int ghgLowVolumeSupplierDeductionKGCO2)
        {
            return await this.CalculationsService.GhgCalculationBreakdown(this.CreateCalculationCommand(supplierOrganisationId
                , obligationPeriodId
                , ghgThresholdGrammesCO2PerMJ
                , deMinimisLitres
                , noDeMinimisLitres
                , ghgLowVolumeSupplierDeductionKGCO2
                , applicationItems                
                , adminConsignments
                , volumes));
        }


        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Creates a calculation command from application items
        /// </summary>
        /// <param name="supplierOrganisationId">The organisation ID of the supplier</param>
        /// <param name="obligationPeriodId">The ID of the obligation Period</param>
        /// <param name="ghgThresholdGrammesCO2PerMJ">The GHG Threshold in Grammes of CO2 per MJ to apply against the calulation</param>
        /// <param name="deMinimisLitres">De Minimis value (in Litres)</param>
        /// <param name="noDeMinimisLitres">No De Minimis value (in Litres)</param>
        /// <param name="ghgLowVolumeSupplierDeductionKGCO2">GHG Low Volume Supplier Deduction (in Kg CO2)</param>
        /// <param name="applicationItems">The Application Items to form the basis of the command</param>
        /// <param name="adminConsignments">The admin consignments to create the command for</param>
        /// <param name="volumes">The volumes to create the command for</param>
        /// <returns>Returns the calculation command for passing to the calculation service</returns>
        private CalculationCommand CreateCalculationCommand(int supplierOrganisationId
            , int obligationPeriodId
            , decimal ghgThresholdGrammesCO2PerMJ
            , int deMinimisLitres
            , int noDeMinimisLitres
            , int ghgLowVolumeSupplierDeductionKGCO2
            , IList<ApplicationItemSummary> applicationItems
            , IEnumerable<AdminConsignment> adminConsignments
            , IEnumerable<NonRenewableVolume> volumes)
        {
            var volumeItems = new List<VolumeCalculationItem>();
            var energyItems = new List<EnergyCalculationItem>();
            var emissionsReductionItems = new List<EmissionsReductionCalculationItem>();

            //Application Items
            if (applicationItems != null)
            {               
                foreach (var applicationItem in applicationItems)
                {
                    var fuelType = this.FuelTypes.Single(ft => ft.FuelTypeId == applicationItem.FuelTypeId);

                    switch (fuelType.FuelCategoryId)
                    {
                        case (int)FuelCategories.FossilGas:
                            volumeItems.Add(new VolumeCalculationItem(applicationItem.ApplicationItemId.ToString()
                                , VolumeFuelCategory_FossilGas
                                , null                                
                                , applicationItem.AmountSupplied.GetValueOrDefault()
                                , applicationItem.GhgIntensity
                                , applicationItem.EnergyDensity));
                            break;

                        case (int)FuelCategories.Electricity:
                            energyItems.Add(new EnergyCalculationItem(applicationItem.ApplicationItemId.ToString()
                                , null
                                , applicationItem.AmountSupplied.GetValueOrDefault()
                                , applicationItem.GhgIntensity.GetValueOrDefault()));
                            break;

                        case (int)FuelCategories.UER:
                            emissionsReductionItems.Add(new EmissionsReductionCalculationItem(applicationItem.ApplicationItemId.ToString()
                                , null
                                , applicationItem.AmountSupplied.GetValueOrDefault()
                                , applicationItem.Reference));
                            break;
                        default:
                            throw new NotSupportedException("Fuel Category not supported by the calculation orchestrator");
                    }
                }
            }

            //Admin Consignment Items
            if(adminConsignments != null)
            {
                foreach(var adminConsignment in adminConsignments)
                {
                    volumeItems.Add(new VolumeCalculationItem(adminConsignment.AdminConsignmentId.ToString()
                        , VolumeFuelCategory_AdminConsignment
                        , null                        
                        , adminConsignment.Volume
                        , adminConsignment.CarbonIntensity
                        , adminConsignment.EnergyDensity));
                }
            }

            //Volume Items
            if(volumes != null)
            {
                foreach(var volume in volumes)
                {
                    var key = new VolumeCompositeKey(volume.OrganisationId, volume.ObligationPeriodId, volume.ReportedMonthEndDate, volume.FuelTypeId);

                    volumeItems.Add(new VolumeCalculationItem(key.CompositeKey
                        , VolumeFuelCategory_Volume
                        , null                        
                        , volume.Volume
                        , volume.CarbonIntensity
                        , volume.EnergyDensity));
                }
            }

            return new CalculationCommand(supplierOrganisationId
                , obligationPeriodId
                , ghgThresholdGrammesCO2PerMJ
                , deMinimisLitres
                , noDeMinimisLitres
                , ghgLowVolumeSupplierDeductionKGCO2
                , volumeItems
                , energyItems
                , emissionsReductionItems);
        }

        #endregion Private Methods
    }
}
