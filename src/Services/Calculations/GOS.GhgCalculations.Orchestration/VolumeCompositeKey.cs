﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgCalculations.Orchestration
{
    /// <summary>
    /// Represents a composite key to uniquely identity a volume.
    /// 
    /// This key can be compacted down to a string to pass into the calculation.
    /// </summary>
    public class VolumeCompositeKey
    {
        #region Properties

        /// <summary>
        /// The Organisation Id
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// The Obligation Period Id
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// The Reported Month End Date
        /// </summary>
        public DateTime ReportedMonthEndDate { get; set; }

        /// <summary>
        /// The ID of the Fuel Type
        /// </summary>
        public int FuelTypeId { get; set; }

        /// <summary>
        /// Gets a string representation of the composite key
        /// </summary>
        public string CompositeKey
        {
            get
            {
                return this.OrganisationId.ToString()
                     + "," + this.ObligationPeriodId.ToString()
                     + "," + this.ReportedMonthEndDate.ToString()
                     + "," + this.FuelTypeId.ToString();
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Creates a Key based upon it's composite components
        /// </summary>
        /// <param name="organisationId">The Organisation Id</param>
        /// <param name="obligationPeriodId">The Obligation Period Id</param>
        /// <param name="reportedMonthEndDate">The Reported Month End Date</param>
        /// <param name="fuelTypeId">The ID of the Fuel Type</param>
        public VolumeCompositeKey(int organisationId
            , int obligationPeriodId
            , DateTime reportedMonthEndDate
            , int fuelTypeId)
        {
            this.OrganisationId = organisationId;
            this.ObligationPeriodId = obligationPeriodId;
            this.ReportedMonthEndDate = reportedMonthEndDate.Date;
            this.FuelTypeId = fuelTypeId;
        }

        /// <summary>
        /// Creates a Key based upon the composite string representation of the key
        /// </summary>
        /// <param name="compositeKey">The composite representation of the key</param>
        public VolumeCompositeKey(string compositeKey)
        {
            var compositeKeyParts = compositeKey.Split(",");

            this.OrganisationId = int.Parse(compositeKeyParts[0]);
            this.ObligationPeriodId = int.Parse(compositeKeyParts[1]);
            this.ReportedMonthEndDate = DateTime.Parse(compositeKeyParts[2]).Date;
            this.FuelTypeId = int.Parse(compositeKeyParts[3]);
        }

        #endregion Constructors
    }
}
