﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgCalculations.Common.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgCalculations.Common.Commands
{
    /// <summary>
    /// A command instructing the service to perform a calculation for credits and obligation
    /// </summary>
    public class CalculationCommand
    {
        #region Properties

        /// <summary>
        /// The organisation ID of the supplier
        /// </summary>
        [Required]
        public int SupplierOrganisationId { get; set; }

        /// <summary>
        /// The ID of the obligation this calculation is being performed against
        /// </summary>
        [Required]
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// The GHG Threshold in Grammes of CO2 per MJ to apply against the calulation
        /// </summary>
        [Required]
        public decimal GhgThresholdGrammesCO2PerMJ { get; set; }

        /// <summary>
        /// De Minimis value (in Litres)
        /// </summary>
        [Required]
        public int DeMinimisLitres { get; set; }

        /// <summary>
        /// No De Minimis value (in Litres)
        /// </summary>
        [Required]
        public int NoDeMinimisLitres { get; set; }

        /// <summary>
        /// GHG Low Volume Supplier Deduction (in Kg CO2)
        /// </summary>
        [Required]
        public int GhgLowVolumeSupplierDeductionKGCO2 { get; set; }

        /// <summary>
        /// The volume based items, such as Liquid and Gas Items, to perform the calculation upon
        /// </summary>
        [Required]
        public IList<VolumeCalculationItem> VolumeItems { get; set; }

        /// <summary>
        /// The Energy based items, such as Electricity, to perform the calculation upon
        /// </summary>
        [Required]
        public IList<EnergyCalculationItem> EnergyItems { get; set; }

        /// <summary>
        /// Items that directly reduce emissions, such as Upstream Emissions Reductions
        /// </summary>
        [Required]
        public IList<EmissionsReductionCalculationItem> EmissionsReductionItems { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="supplierOrganisationId">The Organisation ID of the supplier</param>
        /// <param name="obligationPeriodId">The ID of the obligation period this calculation is for</param>
        /// <param name="ghgThresholdGrammesCO2PerMJ">The GHG Threshold in Grammes of CO2 per MJ to apply against the calulation</param>
        /// <param name="deMinimisLitres">De Minimis value (in Litres)</param>
        /// <param name="noDeMinimisLitres">No De Minimis value (in Litres)</param>
        /// <param name="ghgLowVolumeSupplierDeductionKGCO2">GHG Low Volume Supplier Deduction (in Kg CO2)</param>
        /// <param name="volumeItems">The volume based items, such as liquid and gas, to perform the calculation on</param>
        /// <param name="energyItems">The energy based items, such as Electricity, to perform the calculation on</param>
        /// <param name="emissionsReductionItems">The items that directly reduce emissions, such as Upstream Emissions Reductions</param>
        public CalculationCommand(int supplierOrganisationId
            , int obligationPeriodId
            , decimal ghgThresholdGrammesCO2PerMJ
            , int deMinimisLitres
            , int noDeMinimisLitres
            , int ghgLowVolumeSupplierDeductionKGCO2
            , IList<VolumeCalculationItem> volumeItems
            , IList<EnergyCalculationItem> energyItems
            , IList<EmissionsReductionCalculationItem> emissionsReductionItems)
        {
            this.SupplierOrganisationId = supplierOrganisationId;
            this.ObligationPeriodId = obligationPeriodId;
            this.GhgThresholdGrammesCO2PerMJ = ghgThresholdGrammesCO2PerMJ;
            this.DeMinimisLitres = deMinimisLitres;
            this.NoDeMinimisLitres = noDeMinimisLitres;
            this.GhgLowVolumeSupplierDeductionKGCO2 = ghgLowVolumeSupplierDeductionKGCO2;
            this.VolumeItems = volumeItems;
            this.EnergyItems = energyItems;
            this.EmissionsReductionItems = emissionsReductionItems;
        }

        #endregion Constructors
    }
}
