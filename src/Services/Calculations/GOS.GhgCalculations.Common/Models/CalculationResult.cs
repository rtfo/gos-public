﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Linq;

namespace DfT.GOS.GhgCalculations.Common.Models
{
    /// <summary>
    /// The result of a calculation, containing Volume based, Energy based and Emissions based calculations and aggregations
    /// </summary>
    public class CalculationResult
    {
        #region Properties

        /// <summary>
        /// The supplier ID of the organisation the calculation is for
        /// </summary>
        public int SupplierOrganisationId { get; set; }

        /// <summary>
        /// The ID of the obligation period the calulation was performed on
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// The GHG Target Level used to calculate whether the item generates a credit or obligation (gCO2e / MJ)
        /// </summary>
        public decimal GhgTargetLevel { get; set; }

        /// <summary>
        /// The Volume based calculation result
        /// </summary>
        public VolumeCalculationResult VolumeCalculation { get; set; }

        /// <summary>
        /// The Energy based calculation result
        /// </summary>
        public EnergyCalculationResult EnergyCalculation { get; set; }

        /// <summary>
        /// The Emissions reduction calculation
        /// </summary>
        public EmissionsReductionCalculationResult EmissionsReductionCalculation { get; set; }

        /// <summary>
        /// Gets the Net Total credits, before any adjustments, for all items
        /// </summary>
        public decimal NetTotalCredits { get; set; }

        /// <summary>
        /// Gets the Total Credits, after all adjustments for the obligation period, for all items (KgCO2e)
        /// </summary>
        public decimal TotalCreditsWithObligationPeriodAdjustments { get; set; }

        /// <summary>
        /// Gets the Net Total obligation, before any adjustments, for all items (KgCO2e)
        /// </summary>
        public decimal NetTotalObligation { get; set; }

        /// <summary>
        /// Gets the Total obligation, after all adjustments for the obligation period, for all items (KgCO2e)
        /// </summary>
        public decimal TotalObligationWithObligationPeriodAdjustments { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all calculation results
        /// </summary>
        /// <param name="supplierOrganisationId">The Organisation ID of the supplier this calculation relates to</param>
        /// <param name="obligationPeriodId">The ID of the obligation period this calculation relates to</param>
        /// <param name="ghgTargetLevel">The GHG Target that is used to decide whether credits or an obligation is produced</param>
        /// <param name="volumeCalculation">The Volume based calculation result</param>
        /// <param name="energyCalculation">The Energy based calculation result</param>
        /// <param name="emissionsReductionCalculation">The emissions reduction calculation result</param>
        /// <param name="netTotalCredits">The Net Total credits, before any adjustments, for all items</param>
        /// <param name="totalCreditsWithObligationPeriodAdjustments">The Total Credits, after all adjustments for the obligation period, for all items (KgCO2e)</param>
        /// <param name="netTotalObligation">The Net Total obligation, before any adjustments, for all items (KgCO2e)</param>
        /// <param name="totalObligationWithObligationPeriodAdjustments">The Total obligation, after all adjustments for the obligation period, for all items (KgCO2e)</param>
        public CalculationResult(int supplierOrganisationId
            , int obligationPeriodId
            , decimal ghgTargetLevel
            , VolumeCalculationResult volumeCalculation
            , EnergyCalculationResult energyCalculation
            , EmissionsReductionCalculationResult emissionsReductionCalculation
            , decimal netTotalCredits
            , decimal totalCreditsWithObligationPeriodAdjustments
            , decimal netTotalObligation
            , decimal totalObligationWithObligationPeriodAdjustments)
        {
            this.SupplierOrganisationId = supplierOrganisationId;
            this.ObligationPeriodId = obligationPeriodId;
            this.GhgTargetLevel = ghgTargetLevel;
            this.VolumeCalculation = volumeCalculation;
            this.EnergyCalculation = energyCalculation;
            this.EmissionsReductionCalculation = emissionsReductionCalculation;
            this.NetTotalCredits = netTotalCredits;
            this.TotalCreditsWithObligationPeriodAdjustments = totalCreditsWithObligationPeriodAdjustments;
            this.NetTotalObligation = netTotalObligation;
            this.TotalObligationWithObligationPeriodAdjustments = totalObligationWithObligationPeriodAdjustments;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets a summary of all calculation items from the volume calculation, energy 
        /// calculation and emissions reduct calculation items.
        /// </summary>
        public IList<CalculationItemResult> GetAllCalculationItems()
        {
            return this.VolumeCalculation.Items.Select(i => new CalculationItemResult(i.Reference, i.Credits, i.Obligation))
                    .Concat(this.EnergyCalculation.Items.Select(i => new CalculationItemResult(i.Reference, i.Credits, i.Obligation)))
                    .Concat(this.EmissionsReductionCalculation.Items.Select(i => new CalculationItemResult(i.Reference, i.Credits, i.Obligation)))
                .ToList();
        }

        #endregion Methods
    }
}
