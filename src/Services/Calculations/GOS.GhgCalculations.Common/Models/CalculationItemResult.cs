﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

namespace DfT.GOS.GhgCalculations.Common.Models
{
    public class CalculationItemResult
    {
        #region Properties

        /// <summary>
        /// The unique reference ID for the calculation item
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// The number of credits (net) that would be issued for this item (KgCO2e)
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// The obligation (net) that would be issued for this item (KgCO2e)
        /// </summary>
        public decimal Obligation { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        /// <param name="reference">The unique reference ID for the calculation item</param>
        /// <param name="credits">The number of credits (net) that would be issued for this item (KgCO2e)</param>
        /// <param name="obligation">The obligation (net) that would be issued for this item (KgCO2e)</param>
        public CalculationItemResult(string reference
            , decimal credits
            , decimal obligation)
        {
            this.Reference = reference;
            this.Credits = credits;
            this.Obligation = obligation;
        }

        #endregion Constructors
    }
}
