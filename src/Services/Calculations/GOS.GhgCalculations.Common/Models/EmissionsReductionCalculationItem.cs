﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgCalculations.Common.Models
{
    /// <summary>
    /// Represents an emissions reduction based item, such Upstream Emissions Reduction, to perform the calculation on
    /// </summary>
    public class EmissionsReductionCalculationItem
    {
        #region Properties

        /// <summary>
        /// A reference, for use by the calling client, for the Fuel
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// A description, for use by the calling client, for the Fuel
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The Amount of reduction (KgCO2e)
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The Internal Reference
        /// </summary>
        public string InternalReference { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public EmissionsReductionCalculationItem(string reference
            , string description
            , decimal amount
            , string internalReference)
        {
            this.Reference = reference;
            this.Description = description;
            this.Amount = amount;
            this.InternalReference = internalReference;
        }

        #endregion Constructors
    }
}
