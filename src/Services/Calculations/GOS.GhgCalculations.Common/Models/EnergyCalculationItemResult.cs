﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgCalculations.Common.Models
{
    /// <summary>
    /// The result of a calculation on an energy based fuel item, such as electricity
    /// </summary>
    public class EnergyCalculationItemResult
    {
        #region Properties

        /// <summary>
        /// A unique reference, supplied by the calling client, for the item
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// A description, supplied by the calling client, for the item
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The Energy Supplied in (MJ)
        /// </summary>
        public decimal EnergySupplied { get; set; }

        /// <summary>
        /// The GHG of the energy (gCO2 / MJ)
        /// </summary>
        public decimal GhgIntensity { get; set; }

        /// <summary>
        /// The number of credits produced (KgCO2e)
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// The obligation produced (KgCO2e)
        /// </summary>
        public decimal Obligation { get; set; }

        /// <summary>
        /// The difference to target CI (gCO2e / MJ)
        /// </summary>
        public decimal DifferenceToTarget { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public EnergyCalculationItemResult(string reference
                        , string description
                        , decimal energySupplied
                        , decimal ghgIntensity
                        , decimal credits
                        , decimal obligation
                        , decimal differenceToTarget)
        {
            this.Reference = reference;
            this.Description = description;
            this.EnergySupplied = energySupplied;
            this.GhgIntensity = ghgIntensity;
            this.Credits = credits;
            this.Obligation = obligation;
            this.DifferenceToTarget = differenceToTarget;
        }

        #endregion Constructors
    }
}
