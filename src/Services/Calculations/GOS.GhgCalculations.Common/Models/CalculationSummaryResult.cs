﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgCalculations.Common.Models
{
    /// <summary>
    /// A summary of the result of a calculation, containing aggregations
    /// </summary>
    public class CalculationSummaryResult
    {
        #region Properties

        /// <summary>
        /// The supplier ID of the organisation the calculation is for
        /// </summary>
        public int SupplierOrganisationId { get; set; }

        /// <summary>
        /// The ID of the obligation period the calulation was performed on
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// The GHG Target Level used to calculate whether the item generates a credit or obligation (gCO2e / MJ)
        /// </summary>
        public decimal GhgTargetLevel { get; set; }

        /// <summary>
        /// Gets the Net Total credits, before any adjustments, for all items (KgCO2e)
        /// </summary>
        public decimal NetTotalCredits { get; set; }

        /// <summary>
        /// Gets the Total Credits, after all adjustments for the obligation period, for all items (KgCO2e)
        /// </summary>
        public decimal TotalCreditsWithObligationPeriodAdjustments { get; set; }

        /// <summary>
        /// Gets the Net Total obligation, before any adjustments, for all items (KgCO2e)
        /// </summary>
        public decimal NetTotalObligation { get; set; }

        /// <summary>
        /// Gets the Total obligation, after all adjustments for the obligation period, for all items (KgCO2e)
        /// </summary>
        public decimal TotalObligationWithObligationPeriodAdjustments { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all calculation results
        /// </summary>
        /// <param name="supplierOrganisationId">The Organisation ID of the supplier this calculation relates to</param>
        /// <param name="obligationPeriodId">The ID of the obligation period this calculation relates to</param>
        /// <param name="ghgTargetLevel">The GHG Target that is used to decide whether credits or an obligation is produced</param>
        /// <param name="netTotalCredits">Gets the Net Total credits, before any adjustments, for all items</param>
        /// <param name="totalCreditsWithObligationPeriodAdjustments">Gets the Total Credits, after all adjustments for the obligation period, for all items</param>
        /// <param name="totalObligationWithObligationPeriodAdjustments">Gets the Total obligation, after all adjustments for the obligation period, for all items</param>
        /// <param name="netTotalObligation">Gets the Net Total obligation, before any adjustments, for all items</param>
        public CalculationSummaryResult(int supplierOrganisationId
            , int obligationPeriodId
            , decimal ghgTargetLevel
            , decimal netTotalCredits
            , decimal totalCreditsWithObligationPeriodAdjustments
            , decimal netTotalObligation
            , decimal totalObligationWithObligationPeriodAdjustments)
        {
            this.SupplierOrganisationId = supplierOrganisationId;
            this.ObligationPeriodId = obligationPeriodId;
            this.GhgTargetLevel = ghgTargetLevel;
            this.NetTotalCredits = netTotalCredits;
            this.TotalCreditsWithObligationPeriodAdjustments = totalCreditsWithObligationPeriodAdjustments;
            this.NetTotalObligation = netTotalObligation;
            this.TotalObligationWithObligationPeriodAdjustments = totalObligationWithObligationPeriodAdjustments;
        }

        #endregion Constructors
    }
}
