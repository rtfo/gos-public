﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.GhgCalculations.Common.Models
{
    /// <summary>
    /// Represents an energy based fuel item, such as electricity, to perform the calculation on
    /// </summary>
    public class EnergyCalculationItem
    {
        #region Properties

        /// <summary>
        /// A reference, for use by the calling client, for the Fuel
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// A description, for use by the calling client, for the Fuel
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The amount supplied (MJ)
        /// </summary>
        public decimal EnergySupplied { get; set; }

        /// <summary>
        /// The GHG of the energy (gCO2 / MJ)
        /// </summary>
        public decimal GhgIntensity { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public EnergyCalculationItem(string reference
            , string description
            , decimal energySupplied
            , decimal ghgIntensity)
        {
            this.Reference = reference;
            this.Description = description;
            this.EnergySupplied = energySupplied;
            this.GhgIntensity = ghgIntensity;
        }

        #endregion Constructors
    }
}
