﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System; 

namespace DfT.GOS.GhgCalculations.Common.Models
{
    /// <summary>
    /// Represents a volume based fuel item, such as a liquid or gas, to perform the calculation on
    /// </summary>
    public class VolumeCalculationItem
    {
        #region Properties

        /// <summary>
        /// A reference, for use by the calling client, for the Fuel
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// The Fuel Category of the fuel (as supplied by the calling client)
        /// </summary>
        public string FuelCategoryReference { get; set; }

        /// <summary>
        /// A description, for use by the calling client, for the Fuel
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The volume of fuel (units)
        /// </summary>
        public decimal Volume { get; set; }

        /// <summary>
        /// The carbon intensity of fuel (gCO2 / MJ)
        /// </summary>
        public Nullable<decimal> CarbonIntensity { get; set; }

        /// <summary>
        /// The Energy Density of fuel (MJ / unit)
        /// </summary>
        public Nullable<decimal> EnergyDensity { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        public VolumeCalculationItem(string reference
            , string fuelCategoryReference
            , string description
            , decimal volume
            , Nullable<decimal> carbonIntensity
            , Nullable<decimal> energyDensity)
        {
            this.Reference = reference;
            this.FuelCategoryReference = fuelCategoryReference;
            this.Description = description;
            this.Volume = volume;
            this.CarbonIntensity = carbonIntensity;
            this.EnergyDensity = energyDensity;
        }

        #endregion Constructors
    }
}
