﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;

namespace DfT.GOS.GhgCalculations.Common.Models
{
    /// <summary>
    /// The result of a volume based calcualtion
    /// </summary>
    public class VolumeCalculationResult
    {
        /// <summary>
        /// The GHG Target Level used to calculate whether the item generates a credit or obligation (gCO2e / MJ)
        /// </summary>
        public decimal GhgTargetLevel { get; set; }

        /// <summary>
        /// The individual calculation results for an application item
        /// </summary>
        public IList<VolumeCalculationItemResult> Items { get; set; }

        /// <summary>
        /// The total credits, if any, associated with the volume items calculation (KgCO2e)
        /// </summary>
        public decimal TotalVolumeCredits { get; set; }

        /// <summary>
        /// The net obligation (before deductions), if any, associated with the application item calculation (KgCO2e)
        /// </summary>
        public decimal NetTotalVolumeObligation { get; set; }

        /// <summary>
        /// The aggregated volume supplied across all items (Units)
        /// </summary>
        public decimal TotalVolumeSupplied { get; set; }

        /// <summary>
        /// The total obligation after all obligation period adjustments have been applied (KgCO2e)
        /// </summary>
        public decimal TotalVolumeObligationWithObligationPeriodAdjustments { get; set; }

        /// <summary>
        /// The low volume supplier deduction to be taken off the supplier's obligation for an obligation period (KgCO2e)
        /// </summary>
        public decimal ObligationPeriodLowVolumeSupplierDeduction { get; set; }

        /// <summary>
        /// The aggregated volume supplied across all items that is above the target level (Units)
        /// </summary>
        public decimal TotalVolumeAboveGhgTarget { get; set; }

        /// <summary>
        /// The ghg Low Volume Supplier Deduction for the obligation period (KGCO2)
        /// </summary>
        public int GhgLowVolumeSupplierDeductionKGCO2 { get; set; }

        /// <summary>
        /// The deminimis (minimum level) volume needed before an obligation deduction is made (litres)
        /// </summary>
        public int DeMinimisLitres { get; set; }

        /// <summary>
        /// The no-deminimis (maximum level) volume needed before no obligation deduction is made (litres)
        /// </summary>
        public int NoDeMinimisLitres { get; set; }

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="items">The individual calculation results for an application item</param>
        /// <param name="ghgTargetLevel">The GHG target level associated with the obligation period</param>
        /// <param name="ghgLowVolumeSupplierDeductionKGCO2">The ghg Low Volume Supplier Deduction in KGCO2 for the obligation period</param>
        /// <param name="deMinimisLitres">The deminimis (minimum level) volume needed before an obligation deduction is made</param>
        /// <param name="noDeMinimisLitres">The no-deminimis (maximum level) volume needed before no obligation deduction is made</param>
        /// <param name="totalVolumeCredits">The total credits, if any, associated with the volume items calculation (KgCO2e)</param>
        /// <param name="netTotalVolumeObligation">The net obligation (before deductions), if any, associated with the application item calculation (KgCO2e)</param>
        /// <param name="totalVolumeSupplied">The aggregated volume supplied across all items (Units)</param>
        /// <param name="obligationPeriodLowVolumeSupplierDeduction">The low volume supplier deduction to be taken off the supplier's obligation for an obligation period (KgCO2e)</param>
        /// <param name="totalVolumeAboveGhgTarget">The aggregated volume supplied across all items that is above the target level (Units)</param>
        /// <param name="totalVolumeObligationWithObligationPeriodAdjustments">The total obligation after all obligation period adjustments have been applied (KgCO2e)</param>
        public VolumeCalculationResult(IList<VolumeCalculationItemResult> items
            , decimal ghgTargetLevel
            , int ghgLowVolumeSupplierDeductionKGCO2
            , int deMinimisLitres
            , int noDeMinimisLitres
            , decimal totalVolumeCredits
            , decimal netTotalVolumeObligation
            , decimal totalVolumeSupplied
            , decimal obligationPeriodLowVolumeSupplierDeduction
            , decimal totalVolumeAboveGhgTarget
            , decimal totalVolumeObligationWithObligationPeriodAdjustments)
        {
            this.Items = items;
            this.GhgTargetLevel = ghgTargetLevel;
            this.GhgLowVolumeSupplierDeductionKGCO2 = ghgLowVolumeSupplierDeductionKGCO2;
            this.DeMinimisLitres = deMinimisLitres;
            this.NoDeMinimisLitres = noDeMinimisLitres;
            this.TotalVolumeCredits = totalVolumeCredits;
            this.NetTotalVolumeObligation = netTotalVolumeObligation;
            this.TotalVolumeSupplied = totalVolumeSupplied;
            this.ObligationPeriodLowVolumeSupplierDeduction = obligationPeriodLowVolumeSupplierDeduction;
            this.TotalVolumeAboveGhgTarget = totalVolumeAboveGhgTarget;
            this.TotalVolumeObligationWithObligationPeriodAdjustments = totalVolumeObligationWithObligationPeriodAdjustments;
        }

        #endregion Constructors
    }
}
