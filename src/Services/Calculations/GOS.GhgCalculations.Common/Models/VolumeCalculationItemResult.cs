﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;

namespace DfT.GOS.GhgCalculations.Common.Models
{
    /// <summary>
    /// The result of a calculation on a volume based fuel item, such as liquid or gas
    /// </summary>
    public class VolumeCalculationItemResult
    {
        #region Properties

        /// <summary>
        /// The unique reference number of the fuel (as supplied by the calling client)
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// The Fuel Category of the fuel (as supplied by the calling client)
        /// </summary>
        public string FuelCategoryReference { get; set; }

        /// <summary>
        /// The description of the fuel (as supplied by the calling client)
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The volume supplied (units)
        /// </summary>
        public decimal Volume { get; set; }

        /// <summary>
        /// The energy density of the fuel (MJ / unit)
        /// </summary>
        public Nullable<decimal> EnergyDensity { get; set; }

        /// <summary>
        /// The GHG intensity of the fuel (gCO2 / MJ)
        /// </summary>
        public Nullable<decimal> GhgIntensity { get; set; }

        /// <summary>
        /// The number of credits (net) that would be issued for this item (KgCO2e)
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// The obligation (net) that would be issued for this item (KgCO2e)
        /// </summary>
        public decimal Obligation { get; set; }

        /// <summary>
        /// The difference in GHG to the target for this item (gCO2e / MJ)
        /// </summary>
        public decimal DifferenceToTarget { get; set; }

        /// <summary>
        /// The amount of energy supplied by this item (MJ)
        /// </summary>
        public decimal EnergySupplied { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructors taking all variables
        /// </summary>
        public VolumeCalculationItemResult(string reference
            , string fuelCategoryReference
            , string description
            , decimal volume
            , Nullable<decimal> energyDensity
            , Nullable<decimal> ghgIntensity
            , decimal energySupplied
            , decimal credits
            , decimal obligation
            , decimal differenceToTarget)
        {
            this.Reference = reference;
            this.FuelCategoryReference = fuelCategoryReference;
            this.Description = description;
            this.Volume = volume;
            this.EnergyDensity = energyDensity;
            this.GhgIntensity = ghgIntensity;
            this.EnergySupplied = energySupplied;
            this.DifferenceToTarget = differenceToTarget;
            this.Credits = credits;
            this.Obligation = obligation;
        }

        #endregion Constructors
    }
}
