﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

namespace DfT.GOS.GhgCalculations.Common.Models
{
    /// <summary>
    /// The result of a calculation on an emissions reduction item, such as an Upstream Emissions Reduction
    /// </summary>
    public class EmissionsReductionCalculationItemResult
    {
        #region Properties

        /// <summary>
        /// The unique reference for the the emissions reduction (as supplied by the requesting client)
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// The description for the emissions reduction (as supplied by the requestion client)
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The amount of emissions reduction (KgCO2e)
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The obligation generated (KgCO2e)
        /// </summary>
        public decimal Obligation { get; set; }

        /// <summary>
        /// The credits generated (KgCO2e)
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// The internal reference
        /// </summary>
        public string InternalReference { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all parameters
        /// </summary>
        public EmissionsReductionCalculationItemResult(string reference
                        , string description
                        , decimal amount
                        , decimal credits
                        , decimal obligation
                        , string internalReference)
        {
            this.Reference = reference;
            this.Description = description;
            this.Amount = amount;
            this.Credits = credits;
            this.Obligation = obligation;
            this.InternalReference = internalReference;
        }

        #endregion Constructors
    }
}
