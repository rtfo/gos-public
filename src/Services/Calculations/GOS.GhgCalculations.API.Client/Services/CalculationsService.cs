﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using DfT.GOS.Http;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace DfT.GOS.GhgCalculations.API.Client.Services
{
    /// <summary>
    /// Client interface to Calculations Service
    /// </summary>
    public class CalculationsService : HttpServiceClient, ICalculationsService
    {
        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        public CalculationsService(HttpClient httpClient)
            : base(httpClient)
        {
        }

        #endregion Constructors

        #region ICalculationsService Implementation

        /// <summary>
        /// Performs a GHG Calculation, returing a full breakdown of the results
        /// </summary>
        /// <param name="command">The command describing the input into the calculation</param>
        /// <returns>Returns the result of the calculation</returns>
        async Task<CalculationResult> ICalculationsService.GhgCalculationBreakdown(CalculationCommand command)
        {
            var response = await this.HttpPostAsync("api/v1/Calculations/GhgCalculationBreakdown", command);
            var result = JsonConvert.DeserializeObject<CalculationResult>(await response.Content.ReadAsStringAsync());
            return result;
        }

        /// <summary>
        /// Performs a GHG Calculation, returing a summary of the results
        /// </summary>
        /// <param name="command">The command describing the input into the calculation</param>
        /// <returns>Returns the summary result of the calculation</returns>
        async Task<CalculationSummaryResult> ICalculationsService.GhgCalculationSummary(CalculationCommand command)
        {
            var response = await this.HttpPostAsync("api/v1/Calculations/GhgCalculationSummary", command);
            var result = JsonConvert.DeserializeObject<CalculationSummaryResult>(await response.Content.ReadAsStringAsync());
            return result;
        }

        #endregion ICalculationsService Implementation
    }
}
