﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgCalculations.Common.Commands;
using DfT.GOS.GhgCalculations.Common.Models;
using System.Threading.Tasks;

namespace DfT.GOS.GhgCalculations.API.Client.Services
{
    /// <summary>
    /// Client interface to Calculations Service
    /// </summary>
    public interface ICalculationsService
    {

        /// <summary>
        /// Performs a GHG Calculation, returing a full breakdown of the results
        /// </summary>
        /// <param name="command">The command describing the input into the calculation</param>
        /// <returns>Returns the result of the calculation</returns>
        Task<CalculationResult> GhgCalculationBreakdown(CalculationCommand command);

        /// <summary>
        /// Performs a GHG Calculation, returing a summary of the results
        /// </summary>
        /// <param name="command">The command describing the input into the calculation</param>
        /// <returns>Returns the summary result of the calculation</returns>
        Task<CalculationSummaryResult> GhgCalculationSummary(CalculationCommand command);
    }
}
