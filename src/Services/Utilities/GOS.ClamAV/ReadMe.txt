﻿
https://www.clamav.net/


ClamAV as a Container

This Docker file creates a ClamAV service with up to date virus definitions serving on port 3310.
The bootstrap file runs 'freshclam' as a daemon to keep virus definitions up to date whilst the container is running.

the nClam package can be used to instruct ClamAV to scan files using various methods (via bytearray, or file on disk)


﻿using nClam;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ClamAC
{
    class Program
    {
        public static void Main(string[] args)
        {
			//EICAR String is designed to trigger a virus warning in any virus checker
            byte[] EICAR = Encoding.ASCII.GetBytes("X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*");
            bool result = new Program().ScanFile(EICAR).GetAwaiter().GetResult();
            Console.WriteLine("Result for EICAR String was " + result);

            byte[] Safe = Encoding.ASCII.GetBytes("Hi There");
            result = new Program().ScanFile(Safe).GetAwaiter().GetResult();
            Console.WriteLine("Result for Safe was" + result);
        }

        public async Task<bool> ScanFile(byte[] file)
        {
            var clam = new ClamClient("localhost", 3310);
            ClamScanResult result = await clam.SendAndScanFileAsync(file);
            return (result.Result == ClamScanResults.Clean);
        }
    }
}