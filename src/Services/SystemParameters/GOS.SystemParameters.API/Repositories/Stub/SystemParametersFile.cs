﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.SystemParameters.API.Models;
using System.Collections.Generic;

namespace DfT.GOS.SystemParameters.API.Repositories.Stub
{
    /// <summary>
    /// Structure for holding all types of System Parameters for the System Parameters Stub
    /// </summary>
    public class SystemParametersFile
    {
        /// <summary>
        /// Gets a list of all GOS system Parameters from the Stub
        /// </summary>
        public List<SystemParameter> SystemParameters {get;set;}

        /// <summary>
        /// Gets a list of all doucment types from the stub
        /// </summary>
        public List<DocumentType> DocumentTypes { get; set; }
    }
}
