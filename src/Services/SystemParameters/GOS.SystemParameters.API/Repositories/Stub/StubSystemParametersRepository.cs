﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DfT.GOS.SystemParameters.API.Models;
using Newtonsoft.Json;

namespace DfT.GOS.SystemParameters.API.Repositories.Stub
{
    /// <summary>
    /// Stub repository implementation for system parameters, based upon a json file
    /// </summary>
    public class StubSystemParametersRepository: ISystemParametersRepository
    {
        #region Properties

        private SystemParametersFile SystemParametersFile { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Construct a stub system parameters repository based upon a Json file
        /// </summary>
        public StubSystemParametersRepository(string jsonFilePath)
        {
            //Read in system parameters from json file
            var json = File.ReadAllText(jsonFilePath);
            this.SystemParametersFile = JsonConvert.DeserializeObject<SystemParametersFile>(json);
        }

        #endregion Constructors

        #region ISystemParametersRepository Implementation

        /// <summary>
        /// Gets the document types from the stub
        /// </summary>
        /// <returns>returns a list of document types</returns>
        Task<IList<DocumentType>> ISystemParametersRepository.GetDocumentTypes()
        {
            return Task.Run<IList<DocumentType>>(() =>
            {
                return this.SystemParametersFile.DocumentTypes;
            });
        }        

        /// <summary>
        /// Gets a list of GOS System Parameters
        /// </summary>
        /// <returns>Returns a list of system parameters</returns>
        Task<IList<SystemParameter>> ISystemParametersRepository.GetSystemParameters()
        {
            return Task.Run<IList<SystemParameter>>(() =>
            {
                return this.SystemParametersFile.SystemParameters;
            });
        }

        #endregion ISystemParametersRepository Implementation
    }
}
