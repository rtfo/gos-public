﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.SystemParameters.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.SystemParameters.API.Repositories
{
    /// <summary>
    /// Interface contract for a repository to get organisation details.
    /// </summary>
    public interface ISystemParametersRepository
    {
        /// <summary>
        /// Get all system parameters used by GOS
        /// </summary>
        /// <returns>Returns all system parameters used by GOS</returns>
        Task<IList<SystemParameter>> GetSystemParameters();

        /// <summary>
        /// Get all document types
        /// </summary>
        /// <returns>Returns all document types</returns>
        Task<IList<DocumentType>> GetDocumentTypes();
    }
}
