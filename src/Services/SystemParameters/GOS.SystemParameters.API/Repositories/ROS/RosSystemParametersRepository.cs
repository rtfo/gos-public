﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DfT.GOS.SystemParameters.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.SystemParameters.API.Repositories.ROS
{
    /// <summary>
    /// System parameters repository for connecting to ROS
    /// </summary>
    public class RosSystemParametersRepository : ISystemParametersRepository
    {
        #region Constants

        private const int SystemParameter_RtfoSupportTelephone = 3;
        private const int SystemParameter_RtfoSupportEmail = 4;
        private const int SystemParameter_RtfoAddressLine1 = 15;
        private const int SystemParameter_RtfoAddressLine2 = 16;
        private const int SystemParameter_RtfoAddressLine3 = 17;
        private const int SystemParameter_RtfoAddressLine4 = 18;
        private const int SystemParameter_RtfoPostcode = 19;
        private const int SystemParameter_RtfoWebsiteUrl = 23;

        #endregion Constants

        #region Properties
        private RosSystemParametersDataContext DataContext { get; set; }
        private IMapper Mapper { get; set; }

        #endregion Properties

        #region Constructors       

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="dataContext">The data context for ROS</param>
        /// <param name="mapper">The automapper mapper instane</param>
        public RosSystemParametersRepository(RosSystemParametersDataContext dataContext, IMapper mapper)
        {
            this.DataContext = dataContext;
            this.Mapper = mapper;
        }

        #endregion Constructors

        #region ISystemParametersRepository Implementation

        /// <summary>
        /// Gets a list of all document types
        /// </summary>
        /// <returns>Returns a list of all document types</returns>
        async Task<IList<DocumentType>> ISystemParametersRepository.GetDocumentTypes()
        {
            var documentTypesView = await this.DataContext.GosDocumentTypesView.ToListAsync();
            var documentTypes = this.Mapper.Map<IList<GosDocumentTypesView>, IList<DocumentType>>(documentTypesView);

            return documentTypes;
        }

        /// <summary>
        /// Gets a list of all system parameters used by gos
        /// </summary>
        /// <returns>retuns a list of system parameters</returns>
        async Task<IList<SystemParameter>> ISystemParametersRepository.GetSystemParameters()
        {
            var systemParametersView = await this.DataContext.GosSystemParametersView
                                            .Where(sp => sp.Id == SystemParameter_RtfoSupportTelephone
                                                        || sp.Id == SystemParameter_RtfoSupportEmail
                                                        || sp.Id == SystemParameter_RtfoAddressLine1
                                                        || sp.Id == SystemParameter_RtfoAddressLine2
                                                        || sp.Id == SystemParameter_RtfoAddressLine3
                                                        || sp.Id == SystemParameter_RtfoAddressLine4
                                                        || sp.Id == SystemParameter_RtfoPostcode
                                                        || sp.Id == SystemParameter_RtfoWebsiteUrl)
                                            .ToListAsync();

            var systemParameters = this.Mapper.Map<IList<GosSystemParametersView>, IList<SystemParameter>>(systemParametersView);
            return systemParameters;
        }

        #endregion ISystemParametersRepository Implementation
    }
}
