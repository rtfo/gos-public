﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations.Schema;

namespace DfT.GOS.SystemParameters.API.Repositories.ROS
{
    /// <summary>
    /// DAO corresponding to the Gos_DocumentTypesView in ROS database
    /// </summary>
    [Table("Gos_DocumentTypesView")]
    public class GosDocumentTypesView
    {
        /// <summary>
        /// Gets the ID of the document type
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Get the content type of the document type
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// Get the file extension of the document type
        /// </summary>
        public string FileExtension { get; set; }

        /// <summary>
        /// Get the magic number, for use in security checks, of the document type
        /// </summary>
        public string MagicNumber { get; set; }
    }
}
