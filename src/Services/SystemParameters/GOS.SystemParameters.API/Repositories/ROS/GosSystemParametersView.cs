﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations.Schema;

namespace DfT.GOS.SystemParameters.API.Repositories.ROS
{
    /// <summary>
    /// DAO corresponding to the Gos_SystemParametersView in ROS database
    /// </summary>
    [Table("Gos_SystemParametersView")]
    public class GosSystemParametersView
    {
        /// <summary>
        /// The ID of the system parameter
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the system parameter
        /// </summary>
        public string ParameterName { get; set; }

        /// <summary>
        /// The value of the system parameter
        /// </summary>
        public string Value { get; set; }
    }
}
