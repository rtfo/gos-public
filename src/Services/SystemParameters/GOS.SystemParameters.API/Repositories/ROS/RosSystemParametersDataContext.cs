﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.SystemParameters.API.Repositories.ROS
{
    /// <summary>
    /// Database context for System Parameters within ROS
    /// </summary>
    public class RosSystemParametersDataContext : DbContext
    {
        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="options">context options</param>
        public RosSystemParametersDataContext(DbContextOptions<RosSystemParametersDataContext> options)
           : base(options)
        {
        }

        /// <summary>
        /// Gets a reference to the Gos_SystemParametersView in ROS
        /// </summary>
        public DbSet<GosSystemParametersView> GosSystemParametersView { get; set; }

        /// <summary>
        /// Gets a reference to the Gos_DocumentTypesView in ROS
        /// </summary>
        public DbSet<GosDocumentTypesView> GosDocumentTypesView { get; set; }
    }
}
