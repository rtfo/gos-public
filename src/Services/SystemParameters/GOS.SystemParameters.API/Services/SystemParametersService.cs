﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.SystemParameters.API.Models;
using DfT.GOS.SystemParameters.API.Repositories;
using System.Linq;

namespace DfT.GOS.SystemParameters.API.Services
{
    /// <summary>
    /// Service for System Parameters
    /// </summary>
    public class SystemParametersService : ISystemParametersService
    {
        #region Properties

        private ISystemParametersRepository SystemParametersRepository { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="systemParametersRepository">The repository holding system parameter details</param>
        public SystemParametersService(ISystemParametersRepository systemParametersRepository)
        {
            this.SystemParametersRepository = systemParametersRepository;
        }

        #endregion Constructors

        #region ISystemParametersService implementation

        /// <summary>
        /// Get all document types
        /// </summary>
        /// <returns>Returns all document types</returns>
        async Task<IList<DocumentType>> ISystemParametersService.GetDocumentTypes()
        {
            return await this.SystemParametersRepository.GetDocumentTypes();
        }        

        /// <summary>
        /// Get all system parameters used by GOS
        /// </summary>
        /// <returns>Returns all system parameters used by GOS</returns>
        async Task<IList<SystemParameter>> ISystemParametersService.GetSystemParameters()
        {
            return await this.SystemParametersRepository.GetSystemParameters();
        }

        #endregion ISystemParametersService implementation
    }
}
