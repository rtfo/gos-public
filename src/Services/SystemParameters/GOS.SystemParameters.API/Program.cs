﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace DfT.GOS.SystemParameters.API
{
    /// <summary>
    /// Entry point for the GOS System Parameters service
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entry point for the GOS System Parameters Service
        /// </summary>
        /// <param name="args">any arguments</param>
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        /// <summary>
        /// Configures the service
        /// </summary>
        /// <param name="args">any arguaments</param>
        /// <returns>Returns the configured service</returns>
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://*:80")
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    config.AddEnvironmentVariables();
                })
                .Build();
    }
}
