﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.SystemParameters.API.Models;
using DfT.GOS.SystemParameters.API.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.SystemParameters.API.Controllers
{
    /// <summary>
    /// Gets system parameters
    /// </summary>
    [Route("api/v1/[controller]")]
    public class SystemParametersController: ControllerBase
    {
        #region Properties

        private ISystemParametersService SystemParametersService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the controller using all dependancies
        /// </summary>
        /// <param name="systemParametersService">The service</param>
        public SystemParametersController(ISystemParametersService systemParametersService)
        {
            this.SystemParametersService = systemParametersService;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Returns a list of all system parameters.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     - return all document types:     /api/v1/SystemParameters 
        /// </remarks>
        /// <returns>Returns a list of all system parameters</returns>
        [HttpGet()]
        public async Task<IActionResult> GetAll()
        {
            IList<SystemParameter> systemParameters = await this.SystemParametersService.GetSystemParameters();
            return Ok(systemParameters);
        }

        /// <summary>
        /// Get a list of document types 
        /// </summary>
        /// <returns>list of document types</returns>
        [HttpGet("documenttypes")]
        public async Task<IActionResult> GetDocumentTypes()
        {
            var documentTypes = await this.SystemParametersService.GetDocumentTypes();
            return Ok(documentTypes);
        }
       
        #endregion Methods
    }
}
