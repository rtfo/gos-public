# Build from the src directory with :
# docker build -f src/Services/SystemParameters/GOS.SystemParameters.API/Dockerfile .
ARG LINUX_SWITCH
FROM mcr.microsoft.com/dotnet/core/sdk:3.1$LINUX_SWITCH AS build
COPY . /.
RUN dotnet restore --disable-parallel "DfT - GOS.sln"

FROM build AS publish
RUN dotnet publish /src/Services/SystemParameters/GOS.SystemParameters.API/GOS.SystemParameters.API.csproj -c Release -o /app

#$LINUX_SWITCH can be passed as '-alpine' to build with a linux base instead of the default Debian
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1$LINUX_SWITCH

# Need to add globabisation when building the SQLServer related services with Alpine
# see https://github.com/dotnet/SqlClient/issues/220 relating to .Net Core 3 and EFCore 3
ARG LINUX_SWITCH
RUN if [ "$LINUX_SWITCH" = "-alpine" ] ; then apk add icu-libs ; fi
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

ENTRYPOINT ["dotnet", "DfT.GOS.SystemParameters.API.dll"]
WORKDIR /app
EXPOSE 80

COPY --from=publish /app .
ARG APPLICATION_VERSION=unspecified
LABEL application_version=$APPLICATION_VERSION
ENV APPLICATION_VERSION=$APPLICATION_VERSION
#embed optional git commit into container
ARG GIT_COMMIT=unspecified
LABEL git_commit=$GIT_COMMIT
ENV GIT_COMMIT=$GIT_COMMIT
ARG BUILD_NUMBER=unspecified
LABEL build_number=$BUILD_NUMBER
ENV BUILD_NUMBER=$BUILD_NUMBER
