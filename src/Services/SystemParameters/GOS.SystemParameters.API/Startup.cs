﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.IO;
using System.Reflection;
using AutoMapper;
using DfT.GOS.HealthChecks.Common;
using DfT.GOS.HealthChecks.Common.Extensions;
using DfT.GOS.SystemParameters.API.Repositories;
using DfT.GOS.SystemParameters.API.Repositories.ROS;
using DfT.GOS.SystemParameters.API.Repositories.Stub;
using DfT.GOS.SystemParameters.API.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace DfT.GOS.SystemParameters.API
{
    /// <summary>
    /// The startup file for System Parameters API
    /// </summary>
    public class Startup
    {
        #region Constants

        /// <summary>
        /// The Environmental variable name for the connection string
        /// </summary>
        public const string EnvironmentVariable_ConnectionString = "CONNECTION_STRING";

        /// <summary>
        /// The error message to report if no connection string is supplied
        /// </summary>
        public const string ErrorMessage_ConnectionStringNotSupplied = "Unable to connect to the system parameters datasource, the connection string was not specified. Missing variable: " + EnvironmentVariable_ConnectionString;

        #endregion Constants

        #region Properties
        private ILogger<Startup> Logger { get; set; }

        /// <summary>
        /// Gets the configuration settings
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="configuration">The configuration settings</param>
        /// <param name="logger">The logger</param>
        public Startup(IConfiguration configuration
            , ILogger<Startup> logger)
        {
            this.Logger = logger;
            this.Logger.LogInformation("Startup called for new instance of DfT.GOS.SystemParameters.API");
            Configuration = configuration;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            // - Resolve Service References
            services.AddTransient<ISystemParametersService, SystemParametersService>();

            // - Resolve Repository References
            var connectionString = Configuration[EnvironmentVariable_ConnectionString];

            if (connectionString == null)
            {
                throw new NullReferenceException(ErrorMessage_ConnectionStringNotSupplied);
            }
            else if (connectionString.ToLower().StartsWith("file="))
            {
                //Use the stub repository
                var jsonRepositoryPath = connectionString.Split("=")[1];
                services.AddTransient<ISystemParametersRepository>(r => new StubSystemParametersRepository(jsonRepositoryPath));

                //Add standard health check (we don't have a DB to probe)
                services.AddHealthChecks();
            }
            else
            {
                //Use ROS as the repository

                //Add health check to probe the DB
                services.AddHealthChecks()
                    .AddSqlServer(connectionString, name: "sql-server", tags: new[] { HealthCheckTypes.Readiness });

                //Configure the db context, using retries for resilience
                services.AddDbContext<RosSystemParametersDataContext>(options =>
                {
                    options.UseSqlServer(connectionString
                        , sqlServerOptionsAction: sqlOptions =>
                        {
                            sqlOptions.EnableRetryOnFailure();
                        });
                });

                services.AddTransient<ISystemParametersRepository, RosSystemParametersRepository>();
            }          

            // Register the Swagger generator
            try
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "System Parameters API", Version = "v1", Description = "System parameters to config the GOS applicartion" });
                    c.IncludeXmlComments(xmlPath);                   
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Register Swagger generator because of the following exception: " + ex.ToString());
            }

            services.AddControllers().AddNewtonsoftJson();

            services.AddAutoMapper();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            try
            {
                //Enable Health Checks
                app.UseGosHealthChecks();

                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "System Parameters V1");
                    c.RoutePrefix = string.Empty;
                });
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Unable to Configure Swagger because of the following exception: " + ex.ToString());
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        #endregion Methods
    }
}
