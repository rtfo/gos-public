﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.SystemParameters.API.Client.Models
{
    /// <summary>
    /// DTO for document type system parameter
    /// </summary>
    public class DocumentType
    {
        #region Properties

        /// <summary>
        /// Gets the ID of the document type
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Get the content type of the document type
        /// </summary>
        public string ContentType { get; private set; }

        /// <summary>
        /// Get the file extension of the document type
        /// </summary>
        public string FileExtension { get; private set; }

        /// <summary>
        /// Get the magic number, for use in security checks, of the document type
        /// </summary>
        public string MagicNumber { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        public DocumentType(int id
            , string contentType
            , string fileExtension
            , string magicNumber)
        {
            this.Id = id;
            this.ContentType = contentType;
            this.FileExtension = fileExtension;
            this.MagicNumber = magicNumber;
        }

        #endregion Constructors
    }
}
