﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Linq;

namespace DfT.GOS.SystemParameters.API.Client.Models
{
    /// <summary>
    /// Exposes all system parameters in a single class
    /// </summary>
    public class GosDocumentTypes
    {
        #region Private Properties
        public List<DocumentType> DocumentTypes { get; set; }

        #endregion Private Properties

        #region Constructors
        /// <summary>
        /// is the list.
        /// </summary>
        /// <typeparam name="DocumentType">The type of the ocument type.</typeparam>
        /// <returns></returns>
        public GosDocumentTypes(IList<DocumentType> documentTypes)
        {
            this.DocumentTypes = documentTypes.ToList();
        }

        #endregion Constructors
    }
}
