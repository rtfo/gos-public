﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.SystemParameters.API.Client.Models
{
    /// <summary>
    /// DTO for system parameters
    /// </summary>
    public class SystemParameter
    {
        #region Properties

        /// <summary>
        /// The ID of the system parameter
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// The name of the system parameter
        /// </summary>
        public string ParameterName { get; private set; }

        /// <summary>
        /// The value of the system parameter
        /// </summary>
        public string Value { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        public SystemParameter(int id
            , string parameterName
            , string value)
        {
            this.Id = id;
            this.ParameterName = parameterName;
            this.Value = value;
        }

        #endregion Constructors
    }
}
