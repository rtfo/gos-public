﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Linq;

namespace DfT.GOS.SystemParameters.API.Client.Models
{
    /// <summary>
    /// Exposes all system parameters in a single class
    /// </summary>
    public class GosSystemParameters
    {
        #region Constants

        public const int SystemParameterId_RtfoSupportTelephoneNumber = 3;
        public const int SystemParameterId_RtfoSupportEmailAddress = 4;

        #endregion Constants

        #region Private Properties

        private IList<SystemParameter> SystemParameters { get; set; }

        #endregion Private Properties

        #region Public Properties

        /// <summary>
        /// Gets the value for the RTFO Support Telephone Number from the system parameters
        /// </summary>
        public string RtfoSupportTelephoneNumber { get { return this.SystemParameters.Single(sp => sp.Id == SystemParameterId_RtfoSupportTelephoneNumber).Value; } }

        /// <summary>
        /// Gets the value for the RTFO Support Email Address from the system parameters
        /// </summary>
        public string RtfoSupportEmailAddress { get { return this.SystemParameters.Single(sp => sp.Id == SystemParameterId_RtfoSupportEmailAddress).Value; } }

        #endregion Public Properties

        #region Constructors

        /// <summary>
        /// Constructor taking a list of system parameters
        /// </summary>
        /// <param name="systemParameters">The list of system parameters from the service</param>
        public GosSystemParameters(IList<SystemParameter> systemParameters)
        {
            this.SystemParameters = systemParameters;
        }

        #endregion Constructors
    }
}
