﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using DfT.GOS.Http;
using DfT.GOS.SystemParameters.API.Client.Models;

namespace DfT.GOS.SystemParameters.API.Client.Services
{
    /// <summary>
    /// Definition for getting System Parameters from the remote service
    /// </summary>
    public interface ISystemParametersService
    {
        /// <summary>
        /// Gets a structure containing all GOS relevant system parameters
        /// </summary>
        /// <returns></returns>
        Task<GosSystemParameters> GetSystemParameters();

        /// <summary>
        /// Get all document types
        /// </summary>
        /// <returns>Returns all document types</returns>
        Task<HttpObjectResponse<IList<DocumentType>>> GetDocumentTypes();
    }
}
