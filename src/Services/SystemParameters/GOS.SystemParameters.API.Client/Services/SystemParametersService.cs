﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using DfT.GOS.Http;
using DfT.GOS.SystemParameters.API.Client.Models;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;

namespace DfT.GOS.SystemParameters.API.Client.Services
{
    /// <summary>
    /// Class for getting System Parameters from the remote service
    /// </summary>
    public class SystemParametersService : HttpServiceClient, ISystemParametersService
    {
        #region Private Members
        private readonly IMemoryCache MemoryCache;
        private readonly MemoryCacheEntryOptions MemoryCacheEntryOptions;
        #endregion

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        public SystemParametersService(HttpClient httpClient, IMemoryCache memoryCache, MemoryCacheEntryOptions cacheEntryOptions)
            : base(httpClient)
        {
            this.MemoryCache = memoryCache;
            this.MemoryCacheEntryOptions = cacheEntryOptions;
        }

        #endregion Constructors

        #region ISystemParameterService

        /// <summary>
        /// Gets a structure containing all GOS relevant system parameters
        /// </summary>
        /// <returns></returns>
        async Task<GosSystemParameters> ISystemParametersService.GetSystemParameters()
        {
            return await GetOrCreateSystemParameters();
        }

        /// <summary>
        /// Get all document types
        /// </summary>
        /// <returns>
        ///     Returns all document types
        /// </returns>
        public async Task<HttpObjectResponse<IList<DocumentType>>> GetDocumentTypes()
        {
            return await GetOrCreateDocumentTypes();
        }

        #endregion ISystemParameterService

        #region Private Methods

        /// <summary>
        /// Get or create the SystemParameters cache 
        /// </summary>
        private async Task<GosSystemParameters> GetOrCreateSystemParameters()
        {
            if (!MemoryCache.TryGetValue("AllSystemParameters", out GosSystemParameters result))
            {
                var response = await this.HttpClient.GetAsync(string.Format("api/v1/SystemParameters"));

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var systemParameterList = JsonConvert.DeserializeObject<IList<SystemParameter>>(json);

                    if (systemParameterList != null)
                    {
                        result = new GosSystemParameters(systemParameterList);
                        MemoryCache.Set("AllSystemParameters", result, MemoryCacheEntryOptions);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Get or create document type list cache
        /// </summary>
        private async Task<HttpObjectResponse<IList<DocumentType>>> GetOrCreateDocumentTypes()
        {
            HttpObjectResponse<IList<DocumentType>> documentTypes = null;
            if (!MemoryCache.TryGetValue("AllDocumentTypes", out IList<DocumentType> docTypeList))
            {
                var response = await this.HttpClient.GetAsync(string.Format("api/v1/SystemParameters/documenttypes"));

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    docTypeList = JsonConvert.DeserializeObject<IList<DocumentType>>(json);
                    documentTypes = new HttpObjectResponse<IList<DocumentType>>(docTypeList);
                    MemoryCache.Set("AllDocumentTypes", docTypeList, MemoryCacheEntryOptions);
                }
                else
                {
                    documentTypes = new HttpObjectResponse<IList<DocumentType>>(response.StatusCode);
                }
            }
            else
            {
                documentTypes = new HttpObjectResponse<IList<DocumentType>>(docTypeList);
            }
            return documentTypes;
        }

        #endregion Private Methods
    }
}
