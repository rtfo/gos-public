﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.API.Services;
using DfT.GOS.GhgMarketplace.Common.Commands;
using DfT.GOS.Organisations.API.Client.Services;
using DfT.GOS.ReportingPeriods.API.Client.Services;
using DfT.GOS.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DfT.GOS.GhgMarketplace.API.Controllers
{
    /// <summary>
    /// Allows users to set their interest in buying or selling credits
    /// </summary>
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MarketplaceController : ControllerBase
    {

        #region Properties

        private readonly IMarketplaceService MarketplaceService;
        private readonly ILogger<MarketplaceController> Logger;
        private IReportingPeriodsService ReportingPeriodsService { get; set; }
        private IOrganisationsService OrganisationsService { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        public MarketplaceController(IMarketplaceService marketplaceService
            , IReportingPeriodsService reportingPeriodsService
            , IOrganisationsService organisationsService
            , ILogger<MarketplaceController> logger)
        {
            this.MarketplaceService = marketplaceService;
            this.ReportingPeriodsService = reportingPeriodsService;
            this.OrganisationsService = organisationsService;
            this.Logger = logger;
        }
        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets marketplace adverts for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the adverts for</param>
        /// <returns>Returns the marketplace adverts associated with an obligation period</returns>
        [HttpGet("ObligationPeriod/{obligationPeriodId}")]
        public async Task<IActionResult> GetMarketplaceAdverts(int obligationPeriodId)
        {
            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            if (obligationPeriod == null)
            {
                return NotFound("Obligation period not found");
            }
            else
            {
                var marketplaceAdverts = await this.MarketplaceService.GetMarketplaceAdverts(obligationPeriodId);
                return Ok(marketplaceAdverts);
            }
        }

        /// <summary>
        /// Gets the marketplace advert for an organisation within an obligation period
        /// </summary>
        /// <param name="organisationId">ID of organisation to get data for</param>
        /// <param name="obligationPeriodId">ID of obligation period to get data for</param>
        /// <returns>The organisations interest in buying or selling and the number of credits available</returns>
        [HttpGet("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Advert")]
        public async Task<IActionResult> GetMarketplaceAdvert(int organisationId, int obligationPeriodId)
        {
            var organisation = await this.OrganisationsService.GetOrganisation(organisationId, this.User.GetJwtToken(this.HttpContext));
            if (!organisation.HasResult)
            {
                this.Logger.LogWarning("Unable to find organisation with id {id}", organisationId);
                return NotFound("Organisation not found");
            }

            if (!this.User.CanAccessOrganisationDetails(organisationId))
            {
                return Forbid();
            }

            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            if (obligationPeriod == null)
            {
                this.Logger.LogWarning("Unable to find obligation period with id {id}", obligationPeriodId);
                return NotFound("Obligation period not found");
            }
            var advert = await this.MarketplaceService.GetMarketplaceAdvert(obligationPeriod, organisationId);
            return Ok(advert);
        }

        /// <summary>
        /// Set the trading advertising for an organisation
        /// </summary>
        /// <param name="organisationId">Id of the organisation to set the advert for</param>
        /// <param name="obligationPeriodId">Id of the obligation period to set the advert for</param>
        /// <param name="advert">Advert to set</param>
        /// <returns>Result of creating the advert</returns>
        [HttpPost("Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Advert")]
        public async Task<IActionResult> SetMarketplaceAdvert(int organisationId, int obligationPeriodId, [FromBody]MarketplaceAdvertCommand advert)
        {

            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            var organisation = await this.OrganisationsService.GetOrganisation(organisationId, this.User.GetJwtToken(this.HttpContext));
            if (!organisation.HasResult)
            {
                this.Logger.LogWarning("Unable to find organisation with id {id}", organisationId);
                return NotFound("Organisation not found");
            }

            if (!this.User.CanAccessOrganisationDetails(organisationId))
            {
                this.Logger.LogWarning("User {user} is unable to access this organisation {orgId}", this.User.GetUserId(), organisationId);
                return Forbid();
            }

            var obligationPeriod = await this.ReportingPeriodsService.GetObligationPeriod(obligationPeriodId);
            if (obligationPeriod == null)
            {
                this.Logger.LogWarning("Unable to find obligation period with id {id}", obligationPeriodId);
                return NotFound("Obligation period not found");
            }

            var updated = await this.MarketplaceService.SetMarketplaceAdvert(this.User.GetUserId(), obligationPeriod, organisationId, advert);

            if (updated)
            {
                this.Logger.LogDebug("Advert for organisation {organisation} in obligation period {obPeriod} existed so was updated", organisationId, obligationPeriodId);
                return Ok();
            }
            else
            {
                this.Logger.LogDebug("Advert for organisation {organisation} in obligation period {obPeriod} didn't exist and was created", organisationId, obligationPeriodId);
                return Created("/Organisation/" + organisationId + "/ ObligationPeriod /" + obligationPeriodId + "/Advert)", true);
            }
        }

        #endregion Methods
    }
}
