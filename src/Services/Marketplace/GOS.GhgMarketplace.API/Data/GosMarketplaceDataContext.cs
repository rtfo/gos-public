﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Data;
using Microsoft.EntityFrameworkCore;

namespace DfT.GOS.GhgMarketplace.API.Data
{
    /// <summary>
    /// Database context for the GHG Marketplace
    /// </summary>
    public class GosMarketplaceDataContext : DbContext, IDbContext
    {
        #region Constructors
        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="options">context options</param>
        public GosMarketplaceDataContext(DbContextOptions<GosMarketplaceDataContext> options)
           : base(options)
        {
        }
        #endregion Constructors

        #region Types
        /// <summary>
        /// GhgCreditAdvert
        /// </summary>
        public DbSet<GhgCreditAdvert> GhgCreditAdverts { get; set; }
        #endregion Types

        #region Overrides
        /// <summary>
        /// Use the Fluent API to create a composite key for GhGCreditAdvert
        /// </summary>
        /// <param name="modelBuilder">The builder being used to construct the model for this context</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GhgCreditAdvert>()
                .HasKey(a => new { a.OrganisationId, a.ObligationPeriodId });
        }
        #endregion Overrides
    }
}
