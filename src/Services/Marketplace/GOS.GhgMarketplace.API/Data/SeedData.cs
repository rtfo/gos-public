﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DfT.GOS.GhgMarketplace.API.Data
{
    /// <summary>
    /// Class used to migrate the database and seed any required data
    /// </summary>
    public class SeedData
    {
        #region Public methods
        /// <summary>
        /// Initialises the database, migrating the schema
        /// </summary>
        /// <param name="serviceProvider"></param>
        public static void Initialise(IServiceProvider serviceProvider)
        {
            using (var context = serviceProvider.GetRequiredService<GosMarketplaceDataContext>())
            {
                //Migrate the database (creating the schema if necessary)
                context.Database.Migrate();
            }
        }
        #endregion Public methods
    }
}
