﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DfT.GOS.GhgMarketplace.API.Data
{
    /// <summary>
    /// Database table definition for the GHG Marketplace advert settings
    /// </summary>
    public class GhgCreditAdvert
    {
        private const int MaxInputLength = 50;
        private const int MaxPhoneNumberLength = 16;

        /// <summary>
        /// ID of the organisations
        /// Composite key along with ObligationPeriodId, <see cref="GosMarketplaceDataContext"/> for where it's defined
        /// </summary>
        [Required]
        [Column(Order =0)]
        public int OrganisationId { get; set; }

        /// <summary>
        /// ID of the obligation period the settings are for
        /// Composite key along with OrganisationId, <see cref="GosMarketplaceDataContext"/> for where it's defined
        /// </summary>
        [Required]
        [Column(Order = 1)]
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Boolean representing if the organisation is interested in buying
        /// </summary>
        public bool AdvertiseToBuy { get; set; }

        /// <summary>
        /// Boolean representing if the organisation is interested in selling
        /// </summary>
        public bool AdvertiseToSell { get; set; }

        /// <summary>
        /// Date the advert was created
        /// </summary>
        [Required]
        public DateTime AdvertisedDate { get; set; }

        /// <summary>
        /// ID of the user who created the advert
        /// </summary>
        [Required]
        public string AdvertisedBy { get; set; }

        /// <summary>
        /// Optional name of contact
        /// </summary>
        [MaxLength(MaxInputLength)]
        public string FullName { get; set; }

        /// <summary>
        /// Optional email contact
        /// </summary>
        [MaxLength(MaxInputLength)]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Optional phone number contact
        /// </summary>
        [MaxLength(MaxPhoneNumberLength)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Update takes a credit advert and updates the current object with the new values
        /// </summary>
        /// <param name="advert">New values to set on this advert</param>
        public void Update(GhgCreditAdvert advert)
        {
            this.AdvertiseToBuy = advert.AdvertiseToBuy;
            this.AdvertiseToSell = advert.AdvertiseToSell;
            this.AdvertisedBy = advert.AdvertisedBy;
            this.AdvertisedDate = advert.AdvertisedDate;
            this.FullName = advert.FullName;
            this.EmailAddress = advert.EmailAddress;
            this.PhoneNumber = advert.PhoneNumber;
        }
    }
}
