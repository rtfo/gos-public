﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.API.Data;
using DfT.GOS.GhgMarketplace.API.Repositories;
using DfT.GOS.GhgMarketplace.Common.Commands;
using DfT.GOS.GhgMarketplace.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgMarketplace.API.Services
{
    /// <summary>
    /// Implementation of the Marketplace Service interface
    /// </summary>
    public class MarketplaceService : IMarketplaceService
    {

        #region Properties
        private readonly IMarketplaceRepository MarketplaceRepository;
        private readonly ILogger<MarketplaceService> Logger;
        #endregion Properties

        /// <summary>
        /// Constructor taking all the dependencies
        /// </summary>
        /// <param name="marketplaceRepository">The repository holding marketplace details</param>
        /// <param name="logger">Logger instance</param>
        #region Constructors
        public MarketplaceService(IMarketplaceRepository marketplaceRepository, ILogger<MarketplaceService> logger)
        {
            this.MarketplaceRepository = marketplaceRepository;
            this.Logger = logger;
        }
        #endregion Constructors

        #region IMarketplaceService implementation
        /// <summary>
        /// Get the current advert for the organisation in the given obligation period
        /// </summary>
        /// <param name="obligationPeriod">Obligation period to get advert for</param>
        /// <param name="organisationId">Id of the organisation to get advert for</param>
        /// <returns>The marketplace advert for the organisation in the given obligation period</returns>
        async Task<MarketplaceAdvert> IMarketplaceService.GetMarketplaceAdvert(ObligationPeriod obligationPeriod, int organisationId)
        {
            var advert = await this.MarketplaceRepository.GetMarketplaceAdvert(obligationPeriod.Id, organisationId);
            if (advert == null)
            {
                this.Logger.LogDebug("Organisation {id} did not have an advert for this obligation period {obPeriodId}", organisationId, obligationPeriod.Id);
                return new MarketplaceAdvert(organisationId, false, false, null, null, null);
            }
            return new MarketplaceAdvert(advert.OrganisationId, advert.AdvertiseToBuy, advert.AdvertiseToSell, advert.FullName, advert.EmailAddress, advert.PhoneNumber);

        }

        /// <summary>
        /// Set the marketplace advert for an organisation
        /// </summary>
        /// <param name="userId">ID of user making the request</param>
        /// <param name="obligationPeriod">Obligation period that the advert is for</param>
        /// <param name="organisationId">ID of the organisation to set the advert for</param>
        /// <param name="advert">Advert to set</param>
        /// <returns>boolean indicating if advert was updated</returns>
        async Task<bool> IMarketplaceService.SetMarketplaceAdvert(string userId, ObligationPeriod obligationPeriod, int organisationId, MarketplaceAdvertCommand advert)
        {
            if (obligationPeriod == null)
            {
                this.Logger.LogError("Obligation period was not passed to SetMarketplaceAdvert");
                throw new ArgumentNullException(nameof(obligationPeriod));
            }

            if (advert == null)
            {
                this.Logger.LogError("A MarketplaceAdvert was not passed to SetMarketplaceAdvert");
                throw new ArgumentNullException(nameof(advert));
            }

            if (!advert.AdvertiseToBuy && !advert.AdvertiseToSell)
            {
                advert.FullName = null;
                advert.EmailAddress = null;
                advert.PhoneNumber = null;
            }

            return await this.MarketplaceRepository.SetMarketplaceAdvert(new GhgCreditAdvert
            {
                ObligationPeriodId = obligationPeriod.Id,
                OrganisationId = organisationId,
                AdvertiseToBuy = advert.AdvertiseToBuy,
                AdvertiseToSell = advert.AdvertiseToSell,
                AdvertisedBy = userId,
                AdvertisedDate = DateTime.Now,
                FullName = advert.FullName,
                EmailAddress = advert.EmailAddress,
                PhoneNumber = advert.PhoneNumber
            });
        }

        /// <summary>
        /// Gets all marketplace adverts for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The obligation period to get the adverts for</param>
        /// <returns>Returns a list of marketplace adverts for the supplied obligation period</returns>
        async Task<MarketplaceAdverts> IMarketplaceService.GetMarketplaceAdverts(int obligationPeriodId)
        {
            var adverts = await this.MarketplaceRepository.GetMarketplaceAdverts(obligationPeriodId);
            var organisationsAdvertisingToBuy = adverts.Where(a => a.AdvertiseToBuy)
                .Select(a => new MarketplaceAdvert(a.OrganisationId, a.AdvertiseToBuy, a.AdvertiseToSell, a.FullName, a.EmailAddress, a.PhoneNumber))
                .ToList();
            var organisationsAdvertisingToSell = adverts.Where(a => a.AdvertiseToSell)
                .Select(a => new MarketplaceAdvert(a.OrganisationId, a.AdvertiseToBuy, a.AdvertiseToSell, a.FullName, a.EmailAddress, a.PhoneNumber))
                .ToList();

            var marketplaceAdverts = new MarketplaceAdverts(obligationPeriodId
                , organisationsAdvertisingToBuy
                , organisationsAdvertisingToSell);

            return marketplaceAdverts;
        }

        #endregion IMarketplaceService implementation
    }
}
