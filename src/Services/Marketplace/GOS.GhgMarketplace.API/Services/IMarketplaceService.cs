﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.Common.Commands;
using DfT.GOS.GhgMarketplace.Common.Models;
using DfT.GOS.ReportingPeriods.Common.Models;
using System.Threading.Tasks;

namespace DfT.GOS.GhgMarketplace.API.Services
{
    /// <summary>
    /// Interface defining the GHG Marketplace Service
    /// </summary>
   public interface IMarketplaceService
    {
        /// <summary>
        /// Get the current advert for the organisation in the given obligation period
        /// </summary>
        /// <param name="obligationPeriod">Obligation period to get advert for</param>
        /// <param name="organisationId">Id of the organisation to get advert for</param>
        /// <returns>The marketplace advert for the organisation in the given obligation period</returns>
        Task<MarketplaceAdvert> GetMarketplaceAdvert( ObligationPeriod obligationPeriod, int organisationId);

        /// <summary>
        /// Set the marketplace advert for an organisation
        /// </summary>
        /// <param name="userId">ID of user making the request</param>
        /// <param name="obligationPeriod">Obligation period that the advert is for</param>
        /// <param name="organisationId">ID of the organisation to set the advert for</param>
        /// <param name="advert">Advert to set</param>
        /// <returns>boolean indicating if advert was updated</returns>
        Task<bool> SetMarketplaceAdvert(string userId, ObligationPeriod obligationPeriod, int organisationId, MarketplaceAdvertCommand advert);

        /// <summary>
        /// Gets all marketplace adverts for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The obligation period to get the adverts for</param>
        /// <returns>Returns a list of marketplace adverts for the supplier obligation period</returns>
        Task<MarketplaceAdverts> GetMarketplaceAdverts(int obligationPeriodId);
    }
}
