﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DfT.GOS.GhgMarketplace.API.Migrations
{
    public partial class AddMarketplaceContactDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EmailAddress",
                table: "GhgCreditAdverts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "GhgCreditAdverts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "GhgCreditAdverts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmailAddress",
                table: "GhgCreditAdverts");

            migrationBuilder.DropColumn(
                name: "FullName",
                table: "GhgCreditAdverts");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "GhgCreditAdverts");
        }
    }
}
