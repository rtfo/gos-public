﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.EntityFrameworkCore.Migrations;

namespace DfT.GOS.GhgMarketplace.API.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GhGCreditAdvert",
                columns: table => new
                {
                    OrganisationId = table.Column<int>(nullable: false),
                    ObligationPeriodId = table.Column<int>(nullable: false),
                    AdvertiseToBuy = table.Column<bool>(nullable: false),
                    AdvertiseToSell = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GhGCreditAdvert", x => new { x.OrganisationId, x.ObligationPeriodId });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GhGCreditAdvert");
        }
    }
}
