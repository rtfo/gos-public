﻿// <auto-generated />
using DfT.GOS.GhgMarketplace.API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DfT.GOS.GhgMarketplace.API.Migrations
{
    [DbContext(typeof(GosMarketplaceDataContext))]
    [Migration("20190624150852_MakeGhgCreditAdvertsPlural")]
    partial class MakeGhgCreditAdvertsPlural
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.0-rtm-35687")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("DfT.GOS.GhgMarketplace.API.Data.GhgCreditAdvert", b =>
                {
                    b.Property<int>("OrganisationId");

                    b.Property<int>("ObligationPeriodId");

                    b.Property<bool>("AdvertiseToBuy");

                    b.Property<bool>("AdvertiseToSell");

                    b.HasKey("OrganisationId", "ObligationPeriodId");

                    b.ToTable("GhgCreditAdverts");
                });
#pragma warning restore 612, 618
        }
    }
}
