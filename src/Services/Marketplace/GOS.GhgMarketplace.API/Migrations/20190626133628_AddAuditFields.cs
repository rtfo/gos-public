﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DfT.GOS.GhgMarketplace.API.Migrations
{
    public partial class AddAuditFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdvertisedBy",
                table: "GhgCreditAdverts",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "AdvertisedDate",
                table: "GhgCreditAdverts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdvertisedBy",
                table: "GhgCreditAdverts");

            migrationBuilder.DropColumn(
                name: "AdvertisedDate",
                table: "GhgCreditAdverts");
        }
    }
}
