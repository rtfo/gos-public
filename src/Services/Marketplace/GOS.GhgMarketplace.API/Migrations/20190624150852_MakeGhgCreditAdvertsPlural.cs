﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DfT.GOS.GhgMarketplace.API.Migrations
{
    public partial class MakeGhgCreditAdvertsPlural : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GhGCreditAdvert",
                table: "GhGCreditAdvert");

            migrationBuilder.RenameTable(
                name: "GhGCreditAdvert",
                newName: "GhgCreditAdverts");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GhgCreditAdverts",
                table: "GhgCreditAdverts",
                columns: new[] { "OrganisationId", "ObligationPeriodId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GhgCreditAdverts",
                table: "GhgCreditAdverts");

            migrationBuilder.RenameTable(
                name: "GhgCreditAdverts",
                newName: "GhGCreditAdvert");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GhGCreditAdvert",
                table: "GhGCreditAdvert",
                columns: new[] { "OrganisationId", "ObligationPeriodId" });
        }
    }
}
