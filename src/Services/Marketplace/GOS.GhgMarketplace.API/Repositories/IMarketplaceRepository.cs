﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

using DfT.GOS.GhgMarketplace.API.Data;
using DfT.GOS.GhgMarketplace.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DfT.GOS.GhgMarketplace.API.Repositories
{
    /// <summary>
    /// Interface for a repository to get GHG Marketplace details
    /// </summary>
    public interface IMarketplaceRepository
    {
        /// <summary>
        /// Get the current advert for the organisation in the given obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of the obligation period to get advert for</param>
        /// <param name="organisationId">Id of the organisation to get advert for</param>
        /// <returns>The advert for the organisation in the given obligation period</returns>
        Task<GhgCreditAdvert> GetMarketplaceAdvert(int obligationPeriodId, int organisationId);

        /// <summary>
        /// Set the advert for an organisation
        /// </summary>
        /// <param name="advert">advert to set</param>
        /// <returns>boolean indicating success</returns>
        Task<bool> SetMarketplaceAdvert(GhgCreditAdvert advert);

        /// <summary>
        /// Gets the marketplace adverts for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the adverts for</param>
        /// <returns>Returns a list of GhgCreditAdverts</returns>
        Task<IEnumerable<GhgCreditAdvert>> GetMarketplaceAdverts(int obligationPeriodId);
    }
}
