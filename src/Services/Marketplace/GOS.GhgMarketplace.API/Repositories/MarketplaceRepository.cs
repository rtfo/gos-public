﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

using DfT.GOS.GhgMarketplace.API.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DfT.GOS.GhgMarketplace.API.Repositories
{
    /// <summary>
    /// Implementation for the IMarketplaceRepository interface
    /// </summary>
    public class MarketplaceRepository : IMarketplaceRepository
    {
        #region Properties

        private GosMarketplaceDataContext DataContext { get; set; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="dataContext">Data Context for GHG Ledger database</param>
        public MarketplaceRepository(GosMarketplaceDataContext dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion

        #region IMarketplaceRepository implementation

        /// <summary>
        /// Get the current advert for an organisation in the given obligation period
        /// </summary>
        /// <param name="obligationPeriodId">Id of the obligation period to get advert for</param>
        /// <param name="organisationId">Id of the organisation to get advert for</param>
        /// <returns>The advert for the organisation in the given obligation period</returns>
        async Task<GhgCreditAdvert> IMarketplaceRepository.GetMarketplaceAdvert(int obligationPeriodId, int organisationId)
        {
            return await this.DataContext.GhgCreditAdverts.Where(a => a.ObligationPeriodId == obligationPeriodId && a.OrganisationId == organisationId).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Set the advert for an organisation
        /// </summary>
        /// <param name="advert">advert to set</param>
        /// <returns>boolean indicating if the record was updated</returns>
        async Task<bool> IMarketplaceRepository.SetMarketplaceAdvert(GhgCreditAdvert advert)
        {
            // Try to find the advert
            var record = await this.DataContext.GhgCreditAdverts.Where(a =>
            a.ObligationPeriodId == advert.ObligationPeriodId
            && a.OrganisationId == advert.OrganisationId).SingleOrDefaultAsync();

            if (record == null)
            {
                //Create a new one
                await this.DataContext.AddAsync(advert);
                await this.DataContext.SaveChangesAsync();
                return false;
            }
            else
            {
                // Update the existing one
                record.Update(advert);
                await this.DataContext.SaveChangesAsync();
                return true;
            }
        }

        /// <summary>
        /// Gets the marketplace adverts for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the adverts for</param>
        /// <returns>Returns a list of GhgCreditAdverts</returns>
        async Task<IEnumerable<GhgCreditAdvert>> IMarketplaceRepository.GetMarketplaceAdverts(int obligationPeriodId)
        {
            return await this.DataContext.GhgCreditAdverts.Where(a => a.ObligationPeriodId == obligationPeriodId).ToListAsync();
        }

        #endregion IMarketplaceRepository implementation
    }
}
