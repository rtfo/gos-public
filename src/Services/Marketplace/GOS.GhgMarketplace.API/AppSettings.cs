﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

using DfT.GOS.Http;

namespace DfT.GOS.GhgMarketplace.API
{
    /// <summary>
    /// Application settings for the API
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// HTTP Client setting for resilliant HTTP communication
        /// </summary>
        public HttpClientAppSettings HttpClient { get; set; }

        /// <summary>
        /// Gets and sets the reporting period service API URL
        /// </summary>
        public string ReportingPeriodsServiceApiUrl { get; set; }

        /// <summary>
        /// Gets and sets the organisations service API URL
        /// </summary>
        public string OrganisationsServiceApiUrl { get; set; }
    }
}
