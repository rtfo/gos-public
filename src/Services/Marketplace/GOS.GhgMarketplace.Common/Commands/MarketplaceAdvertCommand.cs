﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgMarketplace.Common.Commands
{
    /// <summary>
    /// Contains the flags for whether an organisation wants to advertise to buy and/or sell
    /// and optionally contact details for the organisation advertising
    /// </summary>
    public class MarketplaceAdvertCommand
    {
        private const int MaxInputLength = 50;
        private const int MaxPhoneNumberLength = 16;
        #region Properties
        /// <summary>
        /// Boolean representing if the organisation is interested in buying
        /// </summary>
        public bool AdvertiseToBuy { get; set; }

        /// <summary>
        /// Boolean representing if the organisation is interested in selling
        /// </summary>
        public bool AdvertiseToSell { get; set; }

        /// <summary>
        /// Optional name of contact
        /// </summary>
        [MaxLength(MaxInputLength)]
        public string FullName { get; set; }

        /// <summary>
        /// Optional email contact
        /// </summary>
        [GosEmailAddress]
        [MaxLength(MaxInputLength)]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Optional phone number contact
        /// </summary>
        [MaxLength(MaxPhoneNumberLength)]
        [Phone(ErrorMessage = "Enter a telephone number, like 01632 960 001, 07700 900 982 or +44 0808 157 0192")]
        public string PhoneNumber { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking advert and contact settings
        /// </summary>
        /// <param name="advertiseToBuy">Whether organisation is interested in buying</param>
        /// <param name="advertiseToSell">Whether organisation is interested in selling</param>
        /// <param name="fullName">Name of contact</param>
        /// <param name="emailAddress">Email address to use to contact organisation</param>
        /// <param name="phoneNumber">Phone number to use to contact organisation</param>
        public MarketplaceAdvertCommand(bool advertiseToBuy, bool advertiseToSell, string fullName, string emailAddress, string phoneNumber)
        {
            this.AdvertiseToBuy = advertiseToBuy;
            this.AdvertiseToSell = advertiseToSell;
            this.FullName = fullName;
            this.EmailAddress = emailAddress;
            this.PhoneNumber = phoneNumber;
        }
        #endregion Constructors
    }
}
