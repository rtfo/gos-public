﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Web.Validation;
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.GhgMarketplace.Common.Models
{
    /// <summary>
    /// Contains the flags for whether an organisation wants to advertise to buy and/or sell
    /// </summary>
    public class MarketplaceAdvert
    {
        #region Properties
        /// <summary>
        /// ID of the organisation advertising
        /// </summary>
        public int OrganisationId { get; private set; }

        /// <summary>
        /// Boolean representing if the organisation is interested in buying
        /// </summary>
        public bool AdvertiseToBuy { get; private set; }

        /// <summary>
        /// Boolean representing if the organisation is interested in selling
        /// </summary>
        public bool AdvertiseToSell { get; private set; }

        /// <summary>
        /// Optional name of contact
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Optional email contact
        /// </summary>
        [GosEmailAddress]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Optional phone number contact
        /// </summary>
        [Phone(ErrorMessage = "Enter a telephone number, like 01632 960 001, 07700 900 982 or +44 0808 157 0192")]
        public string PhoneNumber { get; set; }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Constructor taking advert and contact settings
        /// </summary>
        /// <param name="organisationId">ID of the organisation advertising</param>
        /// <param name="advertiseToBuy">Whether organisation is interested in buying</param>
        /// <param name="advertiseToSell">Whether organisation is interested in selling</param>
        /// <param name="fullName">Name of contact</param>
        /// <param name="emailAddress">Email address to use to contact organisation</param>
        /// <param name="phoneNumber">Phone number to use to contact organisation</param>
        public MarketplaceAdvert(int organisationId, bool advertiseToBuy, bool advertiseToSell, string fullName, string emailAddress, string phoneNumber)
        {
            this.OrganisationId = organisationId;
            this.AdvertiseToBuy = advertiseToBuy;
            this.AdvertiseToSell = advertiseToSell;
            this.FullName = fullName;
            this.EmailAddress = emailAddress;
            this.PhoneNumber = phoneNumber;
        }
        #endregion Constructors
    }
}
