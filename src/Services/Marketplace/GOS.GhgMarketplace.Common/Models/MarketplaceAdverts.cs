﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.

using System.Collections.Generic;

namespace DfT.GOS.GhgMarketplace.Common.Models
{
    /// <summary>
    /// A summary of organisations looking to buy or sell GHG Credits
    /// </summary>
    public class MarketplaceAdverts
    {
        #region Properties

        /// <summary>
        /// The ID for the obligation period the adverts are for
        /// </summary>
        public int ObligationPeriodId { get; private set; }

        /// <summary>
        /// A list of IDs associated with organisations wishing to buy credits
        /// </summary>
        public List<MarketplaceAdvert> OrganisationsAdvertisingToBuy { get; private set; }

        /// <summary>
        /// A list of IDs associated with organisations wishing to sell credit
        /// </summary>
        public List<MarketplaceAdvert> OrganisationsAdvertisingToSell { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="obligationPeriodId">The ID for the obligation period the adverts are for</param>
        /// <param name="organisationsAdvertisingToBuy">A list of organisations wishing to buy credits</param>
        /// <param name="organisationsAdvertisingToSell">A list of organisations wishing to sell credits</param>
        public MarketplaceAdverts(int obligationPeriodId
            , List<MarketplaceAdvert> organisationsAdvertisingToBuy
            , List<MarketplaceAdvert> organisationsAdvertisingToSell)
        {
            this.ObligationPeriodId = obligationPeriodId;
            this.OrganisationsAdvertisingToBuy = organisationsAdvertisingToBuy;
            this.OrganisationsAdvertisingToSell = organisationsAdvertisingToSell;
        }

        #endregion Constructors
    }
}
