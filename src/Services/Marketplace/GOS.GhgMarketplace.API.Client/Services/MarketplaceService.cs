﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.GhgMarketplace.Common.Commands;
using DfT.GOS.GhgMarketplace.Common.Models;
using DfT.GOS.Http;
using System.Net.Http;
using System.Threading.Tasks;

namespace DfT.GOS.GhgMarketplace.API.Client.Services
{
    /// <summary>
    /// Client implementation for the Marketplace Service Interface
    /// </summary>
    public class MarketplaceService : HttpServiceClient, IMarketplaceService
    {
        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">HttpClient to use when making requests</param>
        public MarketplaceService(HttpClient httpClient) : base(httpClient)
        {
        }

        /// <summary>
        /// Get the current advert for the organisation in the given obligation period
        /// </summary>
        /// <param name="organisationId">Id of the organisation to get advert for</param>
        /// <param name="obligationPeriodId">Id of the obligation period to get advert for</param>
        /// <param name="jwtToken">JWT of the user calling the method</param>
        /// <returns>The marketplace advert for the organisation in the given obligation period</returns>
        public async Task<HttpObjectResponse<MarketplaceAdvert>> GetGhGCreditAdvert(int organisationId, int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/Marketplace/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Advert";
            return await this.HttpGetAsync<MarketplaceAdvert>(url, jwtToken);            
        }

        /// <summary>
        /// Set the trading advert for an organisation
        /// </summary>
        /// <param name="organisationId">Id of the organisation to set advert for</param>
        /// <param name="obligationPeriodId">Id of the obligation period to set advert for</param>
        /// <param name="advert">Advert to set</param>
        /// <param name="jwtToken">JWT of the user calling the method</param>
        /// <returns>boolean indicating if advert was created</returns>
        public async Task<bool> SetGhGCreditAdvert(int organisationId, int obligationPeriodId, MarketplaceAdvertCommand advert, string jwtToken)
        {
            var url = $"/api/v1/Marketplace/Organisation/{organisationId}/ObligationPeriod/{obligationPeriodId}/Advert";
            var response =  await this.HttpPostAsync(url, advert, jwtToken);
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Gets all Marketplace adverts for an obligation period
        /// </summary>
        /// <param name="obligationPeriodId">The ID of the obligation period to get the adverts for</param>
        /// <param name="jwtToken">The JWT Token for authentication</param>
        /// <returns>Returns an object containing all marketplace adverts for the specified obligation period</returns>
        async Task<HttpObjectResponse<MarketplaceAdverts>> IMarketplaceService.GetMarketplaceAdverts(int obligationPeriodId, string jwtToken)
        {
            var url = $"/api/v1/Marketplace/ObligationPeriod/{obligationPeriodId}";
            return await this.HttpGetAsync<MarketplaceAdverts>(url, jwtToken);
        }

    }
}
