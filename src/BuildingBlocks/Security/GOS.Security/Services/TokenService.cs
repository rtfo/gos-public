﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.Security.Authentication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace DfT.GOS.Security.Services
{
    /// <summary>
    /// Provides access to the JWT Token on the HTTP Context
    /// </summary>
    public class TokenService : ITokenService
    {
        #region Properties

        private IHttpContextAccessor HttpContextAccessor { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="httpContextAccessor">The accessor for the HTTPContext</param>
        public TokenService(IHttpContextAccessor httpContextAccessor)
        {
            this.HttpContextAccessor = httpContextAccessor;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Gets the JWT Token associated with the current HTTP Context
        /// </summary>
        /// <returns>Returns a task containing the string JWT token</returns>
        async Task<string> ITokenService.GetJwtToken()
        {
            return await HttpContextAccessor.HttpContext.GetTokenAsync(JwtAuthenticationToken.AuthenticationTokenName);
        }

        #endregion Methods
    }
}
