﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Threading.Tasks;

namespace DfT.GOS.Security.Services
{
    /// <summary>
    /// Definition to provide access to the JWT Token on the HTTP Context
    /// </summary>
    public interface ITokenService
    {
        /// <summary>
        /// Gets the JWT Token associated with the current HTTP Context
        /// </summary>
        /// <returns>Returns a task containing the string JWT token</returns>
        Task<string> GetJwtToken();
    }
}
