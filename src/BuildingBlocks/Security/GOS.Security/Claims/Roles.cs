﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Linq;

namespace DfT.GOS.Security.Claims
{
    /// <summary>
    /// Provides information about roles the user can have
    /// </summary>
    public static class Roles
    {
        /// <summary>
        /// The role associated with Administrators
        /// </summary>
        public const string Administrator = "Administrator";

        /// <summary>
        /// The role associated with Suppliers
        /// </summary>
        public const string Supplier = "Supplier";

        /// <summary>
        /// The role associated with Trader
        /// </summary>
        public const string Trader = "Trader";

        /// <summary>
        /// The role associated with Verifier
        /// </summary>
        public const string Verifier = "Verifier";

        /// <summary>
        /// The roles associated with Trading functions
        /// </summary>
        public const string TradingRoles = "Administrator, Supplier, Trader";


    }
}
