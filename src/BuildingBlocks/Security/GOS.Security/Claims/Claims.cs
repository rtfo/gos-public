﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Security.Claims
{
    /// <summary>
    /// Provides information about claims the user can have
    /// </summary>
    public static class Claims
    {
        /// <summary>
        /// The User ID claim key of the user
        /// </summary>
        public const string UserId = "UserId";

        /// <summary>
        /// The Organisation ID claim key of the user
        /// </summary>
        public const string OrganisationId = "OrganisationId";

        /// <summary>
        /// The security stamp claim key
        /// </summary>
        public const string SecurityStamp = "SecurityStamp";
    }
}
