﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Claims;

namespace DfT.GOS.Security.Claims
{
    /// <summary>
    /// Extends the ClaimsPrincipal class to enable natural use of claim information 
    /// from the security token (JWT) against the current user.
    /// </summary>
    public static class ClaimsPrincipalExtensions
    {
        #region Extension Methods

        /// <summary>
        /// Gets the User ID from the claims of the current claims principal (user)
        /// </summary>
        /// <param name="claimsPrincipal">The Claims Principal we're extending</param>
        /// <returns>Returns the User Id from the claims</returns>
        public static string GetUserId(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims.First(i => i.Type == Claims.UserId).Value;
        }

        /// <summary>
        /// Gets the User name from the claims of the current claims principal
        /// </summary>
        /// <param name="claimsPrincipal">The Claims Principal we're extending</param>
        /// <returns></returns>
        public static string GetUserName(this ClaimsPrincipal claimsPrincipal)
        {
            var userName = claimsPrincipal.Claims.FirstOrDefault(i => i.Type == "UserName");
            return userName != null ? userName.Value : null;
        }
        /// <summary>
        /// Gets the Organisation ID that the user is linked to, if it exists, from the claims of the current claims principal (user)
        /// </summary>
        /// <param name="claimsPrincipal">The Claims Principal we're extending</param>
        /// <returns>Returns the Organisation ID associated with the user</returns>
        public static int? GetOrganisationId(this ClaimsPrincipal claimsPrincipal)
        {
            var organisationIdClaim = claimsPrincipal.Claims.FirstOrDefault(i => i.Type == Claims.OrganisationId);

            if(organisationIdClaim == null)
            {
                return null;
            }
            else
            {
                return int.Parse(organisationIdClaim.Value);
            }
        }

        /// <summary>
        /// Indicates whether the current user is an administrator
        /// </summary>
        /// <param name="claimsPrincipal">The Claims Principal we're extending</param>
        /// <returns>Returns true if the user is an administrator, otherwise false</returns>
        public static bool IsAdministrator(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.IsInRole(Roles.Administrator);
        }

        /// <summary>
        /// Indicates whether the current user is a supplier
        /// </summary>
        /// <param name="claimsPrincipal">The Claims Principal we're extending</param>
        /// <returns>Returns true if the user is a supplier, otherwise false</returns>
        public static bool IsSupplier(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.IsInRole(Roles.Supplier);
        }


        /// <summary>
        /// Indicates whether the current user is a trader
        /// </summary>
        /// <param name="claimsPrincipal">The Claims Principal we're extending</param>
        /// <returns>Returns true if the user is a trader, otherwise false</returns>
        public static bool IsTrader(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.IsInRole(Roles.Trader);
        }

        /// <summary>
        /// Indicates whether the current user is a verifier
        /// </summary>
        /// <param name="claimsPrincipal">The Claims Principal we're extending</param>
        /// <returns>Returns true if the user is a verifier, otherwise false</returns>
        public static bool IsVerifier(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.IsInRole(Roles.Verifier);
        }

        /// <summary>
        /// Gets a bool value indicating whether the current user has access to the specified organisation
        /// </summary>
        /// <param name="claimsPrincipal">The Claims Principal we're extending</param>
        /// <param name="organisationId">The ID of the organisation to test</param>
        /// <returns>Returns true if the user can access this organisation, otherwise false</returns>
        public static bool CanAccessOrganisationDetails(this ClaimsPrincipal claimsPrincipal, int organisationId)
        {
            bool userCanAccessOrganisationDetails = false;

            if (IsAdministrator(claimsPrincipal))
            {
                userCanAccessOrganisationDetails = true;
            }
            else
            {
                var userOrganisationId = GetOrganisationId(claimsPrincipal);
                if(userOrganisationId.HasValue
                    && userOrganisationId.Value == organisationId)
                {
                    userCanAccessOrganisationDetails = true;
                }
            }

            return userCanAccessOrganisationDetails;
        }

        /// <summary>
        /// Gets the JWT Token for the current user
        /// </summary>
        /// <param name="claimsPrincipal">The Claims Principal we're extending</param>
        /// <param name="httpContext">The HttpContext to extract the token from</param>
        /// <returns></returns>
        public static string GetJwtToken(this ClaimsPrincipal claimsPrincipal, HttpContext httpContext)
        {
            var auth = httpContext.Request.Headers["Authorization"];
            return auth.ToString().Replace("Bearer ", "");
        }

        #endregion Extension Methods
    }
}
