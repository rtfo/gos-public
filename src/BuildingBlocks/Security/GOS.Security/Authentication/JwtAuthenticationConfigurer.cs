﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace DfT.GOS.Security.Authentication
{
    /// <summary>
    /// Provides configuration for JWT Bearer Token for Authentication
    /// </summary>
    public class JwtAuthenticationConfigurer
    {
        #region Properties

        private JwtAuthenticationSettings Settings { get; set;}

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the class based with the settings dependancy
        /// </summary>
        /// <param name="settings">The settings that will be used to configure services with</param>
        public JwtAuthenticationConfigurer(JwtAuthenticationSettings settings)
        {
            this.Settings = settings;
        }

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Configures a service for JWT (AuthenticationBuilder) based upon the supplied settings
        /// </summary>
        /// <param name="authenticationBuilder">The service to configure</param>
        /// <returns>Returns the service that has been configured</returns>
        public AuthenticationBuilder Configure(AuthenticationBuilder authenticationBuilder)
        {
            authenticationBuilder.AddJwtBearer(cfg =>
            {
                //Setup JWT Auth Token
                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidIssuer = this.Settings.JwtTokenIssuer,
                    ValidateAudience = true,
                    ValidAudience = this.Settings.JwtTokenAudience,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.Settings.JwtTokenKey))
                };
            });

            return authenticationBuilder;
        }

        #endregion Public Methods
    }
}
