﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Security.Authentication
{
    /// <summary>
    /// Configuration Settings for JWT token authentication
    /// </summary>
    public class JwtAuthenticationSettings
    {
        #region Properties

        public string JwtTokenIssuer { get; private set; }
        public string JwtTokenAudience { get; private set; }
        public string JwtTokenKey { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        public JwtAuthenticationSettings(string jwtTokenIssuer
            , string jwtTokenAudience
            , string jwtTokenKey)
        {
            this.JwtTokenIssuer = jwtTokenIssuer;
            this.JwtTokenAudience = jwtTokenAudience;
            this.JwtTokenKey = jwtTokenKey;
        }

        #endregion Constructors
    }
}
