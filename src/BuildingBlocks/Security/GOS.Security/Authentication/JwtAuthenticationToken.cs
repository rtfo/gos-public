﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Security.Authentication
{
    /// <summary>
    /// Provides information about the JWT Authentication Token 
    /// </summary>
    public static class JwtAuthenticationToken
    {
        /// <summary>
        /// The name of the JWT token to be stored within the authentication properties of the cookie used for authentication
        /// </summary>
        public const string AuthenticationTokenName = "jwt";
    }
}
