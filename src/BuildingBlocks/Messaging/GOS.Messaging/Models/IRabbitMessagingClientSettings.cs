﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Messaging.Models
{
    /// <summary>
    /// Interface defining the settings required by the Rabbit Messaging Client
    /// </summary>
    public interface IRabbitMessagingClientSettings
    {
        /// <summary>
        /// Name of Host where RabbitMQ is running
        /// </summary>
        string HostName { get; }

        /// <summary>
        /// Port on Host where RabbitMQ service is listening
        /// </summary>
        int Port { get; }

        /// <summary>
        /// UserName to use when connecting to RabbitMQ
        /// </summary>
        string UserName { get; }

        /// <summary>
        /// Password to use when connecting to RabbitMQ
        /// </summary>
        string Password { get; }

        /// <summary>
        /// Number of times to retry if initial connection to RabbitMQ fails
        /// </summary>
        int RetryCount { get; }

        /// <summary>
        /// Initial length of delay (in seconds) when retrying initial connection to RabbitMQ
        /// </summary>
        int InitialRetryDelayInSeconds { get; }
    }
}
