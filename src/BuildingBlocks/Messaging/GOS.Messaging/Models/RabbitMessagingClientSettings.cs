﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Messaging.Models
{
    public class RabbitMessagingClientSettings : IRabbitMessagingClientSettings
    {
        /// <summary>
        /// HostName for RabbitMQ server
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// Port for RabbitMQ server
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Username for connecting to RabbitMQ server
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password for connecting to RabbitMQ server
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Number of times to retry if initial connection to RabbitMQ fails
        /// </summary>
        public int RetryCount { get; set; }

        /// <summary>
        /// Initial length of delay (in seconds) when retrying initial connection to RabbitMQ
        /// </summary>
        public int InitialRetryDelayInSeconds { get; set; }
    }
}
