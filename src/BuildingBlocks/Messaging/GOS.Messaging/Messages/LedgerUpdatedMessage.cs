﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Messaging.Messages
{
    /// <summary>
    /// Message sent when the GHG Ledger has been updated for a specific Organisation and Obligation Period
    /// </summary>
    public class LedgerUpdatedMessage
    {
        /// <summary>
        /// Id of Organisation
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// Id of Obligation Period
        /// </summary>
        public int ObligationPeriodId { get; set; }

        /// <summary>
        /// Id of Transaction applied to the Ledger
        /// </summary>
        public int TransactionId { get; set; }
    }
}
