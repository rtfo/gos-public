﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Messaging.Messages
{
    /// <summary>
    /// Message sent when GHG Credits have been issued for an Application
    /// </summary>
    public class CreditsIssuedMessage
    {
        /// <summary>
        /// Id of Application for which Credits have been issues
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Organisation Id
        /// </summary>
        public int OrganisationId { get; set; }

        /// <summary>
        /// Obligation Period Id
        /// </summary>
        public int ObligationPeriodId { get; set; }
    }
}
