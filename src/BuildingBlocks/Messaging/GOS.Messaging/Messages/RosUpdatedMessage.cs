﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Messaging.Messages
{
    /// <summary>
    /// Message sent when ROS data has been updated
    /// </summary>
    public class RosUpdatedMessage
    {
        // This class intentionally has no properties
        // It indicates simply that data in ROS has been updated but we are not interested in details of the updates that have been made
    }
}
