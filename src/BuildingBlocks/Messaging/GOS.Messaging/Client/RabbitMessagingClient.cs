﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Text;
using System.Threading.Tasks;
using DfT.GOS.Messaging.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;

namespace DfT.GOS.Messaging.Client
{
    /// <summary>
    /// IMessagingClient implementation using RabbitMQ
    /// </summary>
    public class RabbitMessagingClient : IMessagingClient, IDisposable
    {
        #region Fields

        private IConnection _connection;
        private IModel _channel;

        #endregion Fields

        #region Properties

        private Policy InitialConnectionRetryPolicy { get; set; }
        private IConnectionFactory ConnectionFactory { get; set; }
        private ILogger<RabbitMessagingClient> Logger { get; set; }

        private IConnection Connection
        {
            get
            {
                if (_connection == null)
                {
                    CreateConnection();
                }

                return _connection;
            }
        }

        private IModel Channel
        {
            get
            {
                if (_channel == null)
                {
                    _channel = this.Connection.CreateModel();
                }

                if (_channel.IsClosed)
                {
                    _channel.Dispose();
                    _channel = this.Connection.CreateModel();
                }

                return _channel;
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor accepted all dependencies
        /// </summary>
        /// <param name="connectionFactory">RabbitMQ ConnectionFactory instance</param>
        /// <param name="logger">Logger instance</param>
        public RabbitMessagingClient(IConnectionFactory connectionFactory, ILogger<RabbitMessagingClient> logger)
        {
            this.Logger = logger ?? throw new ArgumentNullException("logger");
            this.ConnectionFactory = connectionFactory ?? throw new ArgumentNullException("connectionFactory");
            this.InitialConnectionRetryPolicy = Policy.NoOp();
        }

        /// <summary>
        /// Convenience constructor that has no dependency on RabbitMQ API
        /// </summary>
        /// <param name="settings">Settings to use to connect to RabbitMQ server</param>
        /// <param name="logger">Logger instance</param>
        public RabbitMessagingClient(IRabbitMessagingClientSettings settings, ILogger<RabbitMessagingClient> logger) : this(BuildConnectionFactory(settings), logger)
        {
            // Configure retry policy for initial connection to RabbitMQ
            Random jitterer = new Random();
            this.InitialConnectionRetryPolicy = Policy.Handle<BrokerUnreachableException>()
                .WaitAndRetry(settings.RetryCount,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(settings.InitialRetryDelayInSeconds, retryAttempt))
                        + TimeSpan.FromMilliseconds(jitterer.Next(0, 100)),
                    (ex, delay) => this.Logger.LogInformation(ex, $"Polly captured an exception whilst connecting to RabbitMQ.  Trying again in {delay.TotalSeconds} seconds."));
        }

        #endregion Constructors

        #region Static Methods

        /// <summary>
        /// Creates a ConnectionFactory instance from the supplied settings
        /// </summary>
        /// <param name="settings">Connection settings for RabbitMQ server</param>
        /// <returns>IConnectionFactory instance</returns>
        public static IConnectionFactory BuildConnectionFactory(IRabbitMessagingClientSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }

            return new ConnectionFactory()
            {
                HostName = settings.HostName,
                Port = settings.Port,
                UserName = settings.UserName,
                Password = settings.Password,
                DispatchConsumersAsync = true
            };
        }

        #endregion Static Methods

        #region Methods

        /// <summary>
        /// Creates a connection to the RabbitMQ server
        /// </summary>
        private void CreateConnection()
        {
            // If a BrokerUnreachable exception is thrown here during startup on a development machine, just continue.
            // It's  waiting for the RabbitMQ server to start, this is handled gracefully in production. 
            _connection = this.InitialConnectionRetryPolicy.Execute(() => this.ConnectionFactory.CreateConnection());
        }

        /// <summary>
        /// Retrieves the name to use for a queue holding messages of the given type
        /// </summary>
        /// <typeparam name="T">Type of messages held in the queue</typeparam>
        /// <returns>Queue name</returns>
        private string GetQueueName<T>()
        {
            return typeof(T).FullName;
        }

        /// <summary>
        /// Declares a queue (ensures that it exists before attempting queue operations)
        /// </summary>
        /// <typeparam name="T">Type of messages held in the queue</typeparam>
        /// <returns>Summary details about the queue</returns>
        private QueueDeclareOk DeclareQueue<T>()
        {
            return this.Channel.QueueDeclare(queue: GetQueueName<T>(), durable: true, exclusive: false, autoDelete: false, arguments: null);
        }

        #endregion Methods

        #region IMessagingClient interface

        /// <summary>
        /// Publishes a message on the appropriate queue
        /// </summary>
        /// <typeparam name="T">Message type</typeparam>
        /// <param name="message">Message to publish</param>
        public Task Publish<T>(T message)
        {
            return Task.Run(() => 
            {
                DeclareQueue<T>();
                // Messages will be stored on the queue in JSON format
                var json = JsonConvert.SerializeObject(message);
                var body = Encoding.UTF8.GetBytes(json);
                // Ensure messages on the queue are persisted
                var basicProperties = this.Channel.CreateBasicProperties();
                basicProperties.Persistent = true;
                // Use the default exchange (denoted by an empty string) to route to a queue with the name specified by the routing key
                this.Channel.BasicPublish(exchange: string.Empty, routingKey: GetQueueName<T>(), basicProperties: basicProperties, body: body);
                return true;
            });
        }

        /// <summary>
        /// Registers a subscriber to messages of a given type
        /// </summary>
        /// <typeparam name="T">Type of message to which subscription applies</typeparam>
        /// <param name="subscriberAsync">Code to execute when a message is received</param>
        public Task Subscribe<T>(Func<T, Task<bool>> subscriberAsync)
        {
            return Task.Run(() =>
            {
                DeclareQueue<T>();
                var consumer = new GosBasicConsumer<T>(subscriberAsync, this.Logger);
                this.Channel.BasicConsume(GetQueueName<T>(), true, consumer);
            });
        }

        #endregion IMessagingClient interface

        #region IDisposable interface

        /// <summary>
        /// Releases non-managed resources
        /// </summary>
        public void Dispose()
        {
            if (_channel != null)
            {
                _channel.Close();
                _channel.Dispose();
            }

            if (_connection != null)
            {
                _connection.Close();
                _connection.Dispose();
            }
        }

        #endregion IDisposable interface
    }
}
