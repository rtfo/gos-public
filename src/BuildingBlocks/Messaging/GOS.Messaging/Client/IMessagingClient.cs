﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Messaging.Client
{
    /// <summary>
    /// Defines the interface for the GHG Balance Message Queue
    /// </summary>
    public interface IMessagingClient
    {
        /// <summary>
        /// Publishes a message via the GHG Balance Message Queue
        /// </summary>
        /// <typeparam name="T">Message Type</typeparam>
        /// <param name="message">Message to be published</param>
        Task Publish<T>(T message);

        /// <summary>
        /// Subscribes to messages published via the GHG Balance Message Queue
        /// </summary>
        /// <typeparam name="T">Message Type</typeparam>
        /// <param name="subscriberAsync">Subscriber function that handles messages asynchronously</param>
        Task Subscribe<T>(Func<T, Task<bool>> subscriberAsync);
    }
}
