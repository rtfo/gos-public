﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.Messaging.Client
{
    /// <summary>
    /// Consumer class used to handle messages received from the Message Queues
    /// </summary>
    /// <typeparam name="T">Type of message that this class will consume</typeparam>
    public class GosBasicConsumer<T> : AsyncDefaultBasicConsumer
    {
        #region Properties

        private Func<T, Task<bool>> ProcessMessageAsync { get; set; }
        private ILogger Logger { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor that accepts all dependencies
        /// </summary>
        /// <param name="processMessageAsync">Asynchronous code to run when a message is received</param>
        /// <param name="logger">Logger instance</param>
        public GosBasicConsumer(Func<T, Task<bool>> processMessageAsync, ILogger logger)
        {
            this.ProcessMessageAsync = processMessageAsync ?? throw new ArgumentNullException("subscriberAsync");
            this.Logger = logger ?? throw new ArgumentNullException("logger");
        }

        #endregion Constructors

        #region Overridden Methods

        /// <summary>
        /// Handles delivery of a message
        /// </summary>
        /// <param name="consumerTag">Consumer Tag of message</param>
        /// <param name="deliveryTag">Delivery Tag of message</param>
        /// <param name="redelivered">Flag indicating whether message has been redelivered</param>
        /// <param name="exchange">Exchange that routed the message</param>
        /// <param name="routingKey">Routing Key of message</param>
        /// <param name="properties">Custom properties for message</param>
        /// <param name="body">Message body</param>
        public override async Task HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            if (body != null)
            {
                try
                {
                    // Assume that the body of the message contains an instance of the Message Type serialised as JSON
                    var json = Encoding.UTF8.GetString(body);
                    var message = JsonConvert.DeserializeObject<T>(json);
                    // Execute the custom code to process the message
                    await this.ProcessMessageAsync(message);
                }
                catch (Exception ex)
                {
                    this.Logger.LogError(ex, $"Error when receiving message with consumerTag '{consumerTag}' and deliveryTag '{deliveryTag}'.  The expected message type was '{typeof(T).FullName}'.");
                }
            }
        }

        #endregion Overridden Methods
    }
}
