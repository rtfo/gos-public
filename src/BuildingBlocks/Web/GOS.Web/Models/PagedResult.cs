﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Collections.Generic;
using System.Linq;

namespace DfT.GOS.Web.Models
{
    /// <summary>
    /// Generic structure for holding a page of results
    /// </summary>
    /// <typeparam name="T">The type of class to hold a page of</typeparam>
    public class PagedResult<T>
    {
        /// <summary>
        /// A single page of results of type T.
        /// </summary>
        public IList<T> Result { get; set; }

        /// <summary>
        /// The total number of results in the entire result set
        /// </summary>
        public int TotalResults { get; set; }

        /// <summary>
        /// The page number this paged result represents
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// The number of results in this page
        /// </summary>
        public int PageSize { get; set; }
    }

    /// <summary>
    /// Factory class to build an instance of PageResults (of type) based upon some IEnumerable query
    /// </summary>
    public static class PagedResult
    {
        #region Static Methods

        /// <summary>
        /// Gets an instance of PageResults (of type) based upon some IEnumerable query
        /// </summary>
        /// <typeparam name="TEntity">The type of the class to page</typeparam>
        /// <param name="query">The underlying IEnemerable Query we wish to be paged</param>
        /// <param name="pageSize">The size of the page to return</param>
        /// <param name="pageNumber">The page of results to return</param>
        /// <returns>A page of results</returns>
        public static PagedResult<TEntity> GetPagedResult<TEntity>(IEnumerable<TEntity> query, int pageSize, int pageNumber)
        {
            var totalResults = query.Count();

            var skipRecords = (pageNumber - 1) * pageSize;
            var actualPageSize = pageSize < 0 ? totalResults : pageSize;
            return new PagedResult<TEntity>
            {
                TotalResults = query.Count(),
                Result = query.Skip(skipRecords).Take(pageSize).ToList(),
                PageSize = actualPageSize,
                PageNumber = pageNumber
            };
        }

        #endregion Static Methods
    }
}
