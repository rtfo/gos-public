﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DfT.GOS.Web.Validation
{
    /// <summary>
    /// Custom Validation 'Decimal' to validate that a property has a number of decimal places
    /// </summary>
    public class DecimalAttribute : RegularExpressionAttribute
    {
        public int DecimalPlaces { get; set; }
        public DecimalAttribute(int decimalPlaces)
            : base(string.Format(@"^\-?\d*\.?\d{{0,{0}}}$", decimalPlaces))
        {
            DecimalPlaces = decimalPlaces;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format("'{0}' can have maximum {1} decimal places", name, this.DecimalPlaces);
        }
    }
}
