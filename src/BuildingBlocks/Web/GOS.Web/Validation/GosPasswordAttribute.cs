﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.ComponentModel.DataAnnotations;

namespace DfT.GOS.Web.Validation
{
    /// <summary>
    /// Provides custom validation for a GOS Password
    /// </summary>
    public class GosPasswordAttribute : RegularExpressionAttribute
    {
        #region Constants

        private const string minimumLength = "8";
        private const string specialChars = "!@#$%^&*():;'£?_.,";

        private const string HasAtLeastOneLowerCaseChar = "(?=.*[a-z])";
        private const string HasAtLeastOneUpperCaseChar = "(?=.*[A-Z])";
        private const string HasAtLeastOneDigit = @"(?=.*\d)";
        private const string HasAtLeastOneSpecialChar = "(?=.*[" + specialChars + "])";
        private const string HasALengthOfAtLeast8Chars = ".{" + minimumLength  + ",}";

        private const string passwordRegEx = "^" + HasAtLeastOneLowerCaseChar
                                                 + HasAtLeastOneUpperCaseChar
                                                 + HasAtLeastOneDigit
                                                 + HasAtLeastOneSpecialChar
                                                 + HasALengthOfAtLeast8Chars
                                                 + "$";
        #endregion Constants

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public GosPasswordAttribute()
            : base(passwordRegEx)
        {
        }

        #endregion Constructors

        #region Overriden Methods

        /// <summary>
        /// Formats the error string
        /// </summary>
        /// <param name="name">The name of the field to validate</param>
        /// <returns>Returns a validation message</returns>
        public override string FormatErrorMessage(string name)
        {
            return string.Format("'{0}' must be at least {1} characters long and contain at least one of each of the following: a lower case character"
                +", an upper case character, a digit, a special character ({2})", name, minimumLength, specialChars);
        }

        #endregion Overriden Methods
    }
}
