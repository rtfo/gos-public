﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DfT.GOS.Web.Validation
{
    /// <summary>
    /// Provides custom email address validation
    /// 
    /// note, the standard EmailAddressAttirbute class allows local domain email addresses, e.g. foo@bar
    /// </summary>
    public class GosEmailAddressAttribute : RegularExpressionAttribute
    {
        /// <summary>
        /// Regular expression, which is used to validate an E-Mail address.
        /// 
        /// See: https://www.codeproject.com/Articles/22777/%2FArticles%2F22777%2FEmail-Address-Validation-Using-Regular-Expression
        /// </summary>
        public const string MatchEmailPattern =
                  @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
           + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
           + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

        /// <summary>
        /// Default constructor
        /// </summary>
        public GosEmailAddressAttribute()
            : base(MatchEmailPattern)
        {
        }

        /// <summary>
        /// The error message to display if a validation error occurs
        /// </summary>
        /// <param name="name">The name of the field being validated</param>
        /// <returns></returns>
        public override string FormatErrorMessage(string name)
        {
            return "Enter an email address in the correct format, like name@example.com";
        }
    }
}
