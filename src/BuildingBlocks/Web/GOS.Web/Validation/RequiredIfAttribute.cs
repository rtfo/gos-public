﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DfT.GOS.Web.Validation
{
    /// <summary>
    /// Custom validation attibute 'RequiredIf' to validate that a field is supplied 
    /// if another field's value is set to a specific value
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class RequiredIfAttribute : ValidationAttribute
    {
        #region Constants
        private const string DefaultErrorMessage = "{0} is required.";
        #endregion Constants

        #region Properties

        private string OtherProperty { get; set; }
        private string OtherPropertyValue { get; set; }
        private string OtherPropertyDisplayName { get; set; }
        private string OtherPropertyDisplayValue { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all values
        /// </summary>
        /// <param name="otherProperty">The name of the other property to test</param>
        /// <param name="otherPropertyValue">The value of the other property to test for</param>
        public RequiredIfAttribute(string otherProperty
            , string otherPropertyValue)
          : base(DefaultErrorMessage)
        {
            if (string.IsNullOrEmpty(otherProperty))
            {
                throw new ArgumentNullException("otherProperty");
            }

            if (string.IsNullOrEmpty(otherPropertyValue))
            {
                throw new ArgumentNullException("otherPropertyValue");
            }

            this.OtherProperty = otherProperty;
            this.OtherPropertyValue = otherPropertyValue;

            //Default the other property display value to the value of the property (we might 
            //overwrite this using a display attribute, for example if using an enum)
            this.OtherPropertyDisplayValue = otherPropertyValue;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Overrides the method for formatting the error message that is displayed
        /// </summary>
        /// <returns>Returns the formatted error message to display</returns>
        public override string FormatErrorMessage(string name)
        {
            return string.Format(this.ErrorMessageString, name);
        }

        /// <summary>
        /// Overrides the method for detecting whether the field is valid based upon it's value and the value of the other property
        /// </summary>
        /// <returns>Returns true if valid, otherwise false</returns>
        protected override ValidationResult IsValid(object value,
                              ValidationContext validationContext)
        {
            if (value == null)
            {
                var otherProperty = validationContext.ObjectInstance.GetType()
                                   .GetProperty(OtherProperty);

                var otherPropertyCurrentValue = otherProperty
                              .GetValue(validationContext.ObjectInstance, null);

                //See if there is a display attribute on the value (for example on an enum)
                Type type = otherPropertyCurrentValue.GetType();
                var field = type.GetField(otherPropertyCurrentValue.ToString());
                if (field != null)
                {
                    var otherPropertyValueDisplayAttribute = field.GetCustomAttribute<DisplayAttribute>();
                    if (otherPropertyValueDisplayAttribute != null)
                    {
                        this.OtherPropertyDisplayValue = otherPropertyValueDisplayAttribute.Name;
                    }
                }

                //Get the display name of the other attribute (if one has been set)
                var otherPropertyDisplayAttribute = otherProperty.GetCustomAttributes(typeof(DisplayAttribute), true).SingleOrDefault() as DisplayAttribute;

                if (otherPropertyDisplayAttribute == null)
                {
                    this.OtherPropertyDisplayName = otherProperty.Name;
                }
                else
                {
                    this.OtherPropertyDisplayName = otherPropertyDisplayAttribute.Name;
                }

                if (otherPropertyCurrentValue.ToString() == this.OtherPropertyValue)
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
            }

            return ValidationResult.Success;
        }

        #endregion Methods
    }
}
