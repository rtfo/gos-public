﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Web.Hosting
{
    /// <summary>
    /// Provides the a queue of tasks to be executed within a background hosted service
    /// 
    /// See: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-2.1#queued-background-tasks)
    /// 
    /// Note, at the time of implementing this class was not part of .Net Core 2.2. It is anticipated it may be 
    /// included in .Net Core 3.0, at which time we could retire this and use the framework implementation.
    /// </summary>
    public class BackgroundTaskQueue : IBackgroundTaskQueue, IDisposable
    {
        #region Private variables

        private ConcurrentQueue<Func<CancellationToken, Task>> _workItems = new ConcurrentQueue<Func<CancellationToken, Task>>();
        private SemaphoreSlim _signal = new SemaphoreSlim(0);

        #endregion Private variables

        #region IBackgroundTaskQueue Implementation

        /// <summary>
        /// Adds a work item to the queue to be executed by the background hosted service
        /// </summary>
        /// <param name="workItem">The work item to be be queued ready for execution</param>
        public void QueueBackgroundWorkItem(
            Func<CancellationToken, Task> workItem)
        {
            if (workItem == null)
            {
                throw new ArgumentNullException(nameof(workItem));
            }

            _workItems.Enqueue(workItem);
            _signal.Release();
        }

        /// <summary>
        /// Takes a work item off the queue to be exected by the background service
        /// </summary>
        /// <param name="cancellationToken">The cancellation token</param>
        /// <returns>Returns the work item to be executed</returns>
        public async Task<Func<CancellationToken, Task>> DequeueAsync(
            CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _workItems.TryDequeue(out var workItem);

            return workItem;
        }

        #endregion IBackgroundTaskQueue Implementation

        #region IDisposable Implementation

        /// <summary>
        /// Disposes the signal object
        /// </summary>
        public void Dispose()
        {
            ((IDisposable)_signal).Dispose();
        }

        #endregion IDisposable Implementation
    }
}
