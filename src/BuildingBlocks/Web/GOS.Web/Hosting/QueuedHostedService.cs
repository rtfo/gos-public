﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Web.Hosting
{
    /// <summary>
    /// Provides a service for executing async tasks in the background
    /// 
    /// See: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-2.1#queued-background-tasks)
    /// 
    /// Note, at the time of implementing this class was not part of .Net Core 2.2. It is anticipated it may be 
    /// included in .Net Core 3.0, at which time we could retire this and use the framework implementation.
    /// </summary>
    public class QueuedHostedService : BackgroundService
    {
        #region Private Variables

        private readonly ILogger _logger;

        #endregion Private Variables

        #region Properties

        /// <summary>
        /// Gets the underlying queue
        /// </summary>
        public IBackgroundTaskQueue TaskQueue { get; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all variables
        /// </summary>
        /// <param name="taskQueue">The Queue to use for task scheduling/execution</param>
        /// <param name="logger">The Logger</param>
        public QueuedHostedService(IBackgroundTaskQueue taskQueue,
            ILogger<QueuedHostedService> logger)
        {
            TaskQueue = taskQueue;
            _logger = logger;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Executes task from the queue in the background on startup (using Background service)
        /// </summary>
        /// <param name="cancellationToken">The cancellation token</param>
        /// <returns>Returns Task indicating completion</returns>
        protected async override Task ExecuteAsync(
            CancellationToken cancellationToken)
        {
            _logger.LogInformation("Queued Hosted Service is starting.");

            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var workItem = await TaskQueue.DequeueAsync(cancellationToken);

                    try
                    {
                        await workItem(cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex,
                           $"Error occurred executing {nameof(workItem)}: " + ex.ToString());
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error occurred when retrieving a Task from the Queue.");
                }
            }

            _logger.LogInformation("Queued Hosted Service is stopping.");
        }

        #endregion Methods
    }
}
