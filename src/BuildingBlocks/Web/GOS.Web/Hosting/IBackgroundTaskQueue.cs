﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Web.Hosting
{
    /// <summary>
    /// Provides the definition for a queue of tasks to be executed within a background hosted service
    /// 
    /// See: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-2.1#queued-background-tasks)
    /// 
    /// Note, at the time of implementing this interface was not part of .Net Core 2.2. It is anticipated it may be 
    /// included in .Net Core 3.0, at which time we could retire this and use the framework implementation.
    /// </summary>
    public interface IBackgroundTaskQueue
    {
        /// <summary>
        /// Adds a work item to the queue to be executed by the background hosted service
        /// </summary>
        /// <param name="workItem">The work item to be be queued ready for execution</param>
        void QueueBackgroundWorkItem(Func<CancellationToken, Task> workItem);

        /// <summary>
        /// Takes a work item off the queue to be exected by the background service
        /// </summary>
        /// <param name="cancellationToken">The cancellation token</param>
        /// <returns>Returns the work item to be executed</returns>
        Task<Func<CancellationToken, Task>> DequeueAsync(CancellationToken cancellationToken);
    }
}
