﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace DfT.GOS.Web.ExtensionMethods.Enum
{
    /// <summary>
    /// Provides common extension methods for Enums
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Gets the description attribute from the Enum value
        /// </summary>
        /// <param name="GenericEnum">The enum</param>
        /// <returns>Returns the desctipion value</returns>
        public static string GetDisplayName(this System.Enum GenericEnum)
        {
            Type genericEnumType = GenericEnum.GetType();
            MemberInfo[] memberInfo = genericEnumType.GetMember(GenericEnum.ToString());

            if (memberInfo != null && memberInfo.Length > 0)
            {
                var _Attribs = memberInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false);
                if (_Attribs != null && _Attribs.Count() > 0)
                {
                    return ((DisplayAttribute)_Attribs.ElementAt(0)).Name;
                }
            }
            return GenericEnum.ToString();
        }
    }
}
