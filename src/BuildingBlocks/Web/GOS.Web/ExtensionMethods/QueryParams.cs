﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information. *@
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DfT.GOS.Web.ExtensionMethods
{
    /// <summary>
    /// QueryParams provides methods to convert objects to query strings, and back.
    /// Using the format:
    /// key=value&key2=value2
    /// </summary>
    public static class QueryParams
    {
        /// <summary>
        /// ToString converts an object to query parameter string
        /// </summary>
        /// <param name="queryParameters">Object to use properties of</param>
        /// <returns>String in the query parameter format</returns>
        public static string ToString(object queryParameters)
        {
            var properties = from p in queryParameters.GetType().GetProperties()
                             where p.GetValue(queryParameters, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(queryParameters, null).ToString());

            return String.Join("&", properties.ToArray());
        }

        /// <summary>
        /// Convert a query string to an object
        /// </summary>
        /// <param name="query">Query string to convert (including leading ?)</param>
        /// <returns>Dictionary containing key/values from the query string</returns>
        public static Dictionary<string, string> ParseQueryString(string query)
        {
            if (!String.IsNullOrEmpty(query))
            {
                var trimmedQuery = query.Substring(1);
                return trimmedQuery.Split('&')
                 .ToDictionary(c => c.Split('=')[0],
                               c => Uri.UnescapeDataString(c.Split('=')[1]));
            }
            //Return an empty object if the provided string is empty
            return new Dictionary<string, string>();
        }
    }
}
