﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Http
{
    /// <summary>
    /// Provides information for adding to the Polly context during HTTP request execution
    /// </summary>
    public static class HttpPolicyContext
    {
        /// <summary>
        /// The Context key for Request URI
        /// </summary>
        public const string RequestUriKey = "RequestUri";

        /// <summary>
        /// Gets the method of the request, e.g. GET or POST
        /// </summary>
        public const string RequestMethod = "RequestMethod";
    }
}
