﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Newtonsoft.Json;
using Polly;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.Http
{
    /// <summary>
    /// Provides base implementation for Services that act as a HTTP client, making requests to a remote service
    /// </summary>
    public abstract class HttpServiceClient
    {
        #region Properties
        protected HttpClient HttpClient { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependencies
        /// </summary>
        /// <param name="httpClient">The instance of httpclient to use to call the remote service</param>
        public HttpServiceClient(HttpClient httpClient)
        {
            this.HttpClient = httpClient;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Performs a GET method to the request URI using a Bearer JWT Token
        /// </summary>
        /// <typeparam name="T">The type of expected response from the target service, to enable de-serialisation</typeparam>
        /// <param name="requestUri">The URI for the GET request</param>
        /// <param name="jwtToken">The security JWT Token to enable authentication on the target service</param>
        /// <returns>Returns the de-serialised result wrapped in a HttpObjectResponse if successful, otherwise returns an empty 
        /// HttpObjectResponse, indicating the response status code</returns>
        protected async Task<HttpObjectResponse<T>> HttpGetAsync<T>(string requestUri, string jwtToken) where T : class
        {
            return await this.HttpGetAsync<T>(requestUri, true, jwtToken);
        }

        /// <summary>
        /// Performs a GET method to the request URI
        /// </summary>
        /// <typeparam name="T">The type of expected response from the target service, to enable de-serialisation</typeparam>
        /// <param name="requestUri">The URI for the GET request</param>
        /// <returns>Returns the de-serialised result wrapped in a HttpObjectResponse if successful, otherwise returns an empty 
        /// HttpObjectResponse, indicating the response status code</returns>
        protected async Task<HttpObjectResponse<T>> HttpGetAsync<T>(string requestUri) where T : class
        {
            return await this.HttpGetAsync<T>(requestUri, false, null);
        }

        /// <summary>
        /// Performs a POST method to the request URI using a Bearer JWT Token
        /// </summary>
        /// <param name="requestUri">The URI to post the request to</param>
        /// <param name="content">The content to serialize and send as the content of the reqest</param>
        /// <param name="jwtToken">The security JWT Token to enable authentication on the target service</param>
        /// <returns>The HTTP Response</returns>
        protected async Task<HttpResponseMessage> HttpPostAsync(string requestUri, object content, string jwtToken)
        {
            return await this.HttpPostAsync(requestUri, content, true, jwtToken);
        }

        /// <summary>
        /// Performs a POST method to the request URI
        /// </summary>
        /// <param name="requestUri">The URI to post the request to</param>
        /// <param name="content">The content to serialize and send as the content of the reqest</param>
        /// <returns>The HTTP Response</returns>
        protected async Task<HttpResponseMessage> HttpPostAsync(string requestUri, object content)
        {
            return await this.HttpPostAsync(requestUri, content, false, null);
        }

        /// <summary>
        /// Performs a DELETE method to the request URI using a Bearer JWT Token
        /// </summary>
        /// <param name="requestUri">The URI to post the request to</param>
        /// <param name="jwtToken">The security JWT Token to enable authentication on the target service</param>
        /// <returns>The HTTP Response</returns>
        protected async Task<HttpResponseMessage> HttpDeleteAsync(string requestUri, string jwtToken)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, requestUri);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", jwtToken);
            return await this.HttpClient.SendAsync(request);
        }

        #endregion Methods

        #region Private Methods

        /// <summary>
        /// Performs a GET method to the request URI using a Bearer JWT Token
        /// </summary>
        /// <typeparam name="T">The type of expected response from the target service, to enable de-serialisation</typeparam>
        /// <param name="requestUri">The URI for the GET request</param>
        /// <param name="authenticate">Whether to pass JWT authentication ticket</param>
        /// <param name="jwtToken">The security JWT Token to enable authentication on the target service</param>
        /// <returns>Returns the de-serialised result wrapped in a HttpObjectResponse if successful, otherwise returns an empty 
        /// HttpObjectResponse, indicating the response status code</returns>
        private async Task<HttpObjectResponse<T>> HttpGetAsync<T>(string requestUri, bool authenticate, string jwtToken) where T : class
        {
            HttpObjectResponse<T> result;

            var request = SetPolicyExecutionContext(new HttpRequestMessage(HttpMethod.Get, requestUri), requestUri);

            if (authenticate)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", jwtToken);
            }

            var response = await this.HttpClient.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync(); result = new HttpObjectResponse<T>(JsonConvert.DeserializeObject<T>(json));
            }
            else
            {
                result = new HttpObjectResponse<T>(response.StatusCode);
            }
            return result;
        }

        /// <summary>
        /// Performs a POST method to the request URI, optionally using a Bearer JWT Token
        /// </summary>
        /// <param name="requestUri">The URI to post the request to</param>
        /// <param name="content">The content to serialize and send as the content of the reqest</param>
        /// <param name="authenticate">Whether to authentiate with the JWT Token</param>
        /// <param name="jwtToken">The security JWT Token to enable authentication on the target service</param>
        /// <returns>The HTTP Response</returns>
        private async Task<HttpResponseMessage> HttpPostAsync(string requestUri, object content, bool authenticate, string jwtToken)
        {
            var json = JsonConvert.SerializeObject(content);

            var request = SetPolicyExecutionContext(new HttpRequestMessage(HttpMethod.Post, requestUri), requestUri);

            if (authenticate)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", jwtToken);
            }

            request.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return await this.HttpClient.SendAsync(request);
        }

        /// <summary>
        /// Sets some context information in the request for Polly to pick up and report on if there are connection issues
        /// </summary>
        /// <param name="request">The request to add context information to</param>
        /// <param name="requestUri">The URL of the resource we're requesting</param>
        protected HttpRequestMessage SetPolicyExecutionContext(HttpRequestMessage request, string requestUri)
        {
            var context = new Context();
            context[HttpPolicyContext.RequestUriKey] = this.HttpClient.BaseAddress.AbsoluteUri + requestUri;
            context[HttpPolicyContext.RequestMethod] = request.Method.ToString();
            request.SetPolicyExecutionContext(context);
            return request;
        }

        #endregion Private Methods
    }
}
