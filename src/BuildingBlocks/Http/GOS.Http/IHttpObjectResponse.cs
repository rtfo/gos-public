﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Net;

namespace DfT.GOS.Http
{
    /// <summary>
    /// Provides a definition for a generic handler for a HTTP response with an object
    /// </summary>
    public interface IHttpObjectResponse
    {
        HttpStatusCode HttpStatusCode { get; }
    }
}
