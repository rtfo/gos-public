﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.DependencyInjection;
using System;
using Polly;
using Polly.Extensions.Http;
using System.Net.Http;
using Microsoft.Extensions.Logging;

namespace DfT.GOS.Http
{
    /// <summary>
    /// Provides configuration for a HTTP Client
    /// </summary>
    public class HttpClientConfigurer
    {
        #region Properties

        private HttpClientAppSettings Settings { get; set; }
        private ILogger Logger { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructs the class based with the settings dependancy
        /// </summary>
        /// <param name="settings">The settings that will be used to configure services with</param>
        public HttpClientConfigurer(HttpClientAppSettings settings
            , ILogger logger)
        {
            this.Settings = settings;
            this.Logger = logger;
        }

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Configures a service (IHttpClientBuilder) based upon the supplied settings
        /// </summary>
        /// <param name="httpClientBuilder">The service to configure</param>
        /// <returns>Returns the service that has been configured</returns>
        public IHttpClientBuilder Configure(IHttpClientBuilder httpClientBuilder)
        {
            //Configure the number of minutes the HttpClient will stay alive in the pool
            httpClientBuilder.SetHandlerLifetime(TimeSpan.FromMinutes(this.Settings.HttpClientLifetimeSeconds));

            //Configure the retry policy for HTTP Requests, note we don't want to retry in debug mode because it makes debugging frustrating!
            var retryCount = 0;
#if !DEBUG
            retryCount = this.Settings.HttpClientRetryCount;
#endif

            httpClientBuilder.AddPolicyHandler(this.GetHttpRetryPolicy(retryCount, this.Settings.HttpClientInitialRetryDelaySeconds));

            return httpClientBuilder;
        }

        #endregion Public Method

        #region Private Methods

        /// <summary>
        /// Gets the Poly HTTP retry policy for the HTTP Client based upon the configuration
        /// </summary>
        /// <returns>returns the HTTP retry policy</returns>
        private IAsyncPolicy<HttpResponseMessage> GetHttpRetryPolicy(int retryCount, int retryInitialDelaySeconds)
        {
            Random jitterer = new Random();

            return HttpPolicyExtensions
                .HandleTransientHttpError() //Handle errors such as network errors
                .WaitAndRetryAsync(retryCount,    // Retry with exponential back-off plus some random jitter to overcome many re-tries from partial outages - See ROSGOS-514 for more details
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(retryInitialDelaySeconds, retryAttempt)) // Retry exponentially, starting with the retryInitialDelaySeconds, plus up to 100 milliseconds random jitter
                            + TimeSpan.FromMilliseconds(jitterer.Next(0, 100)),
                        (result, timeSpan, retryAttemptCount, context) => {
                            //Log a message indicating a retry occurred
                            var excpetionHandler = new HttpClientRetryExceptionHandler(this.Logger);
                            excpetionHandler.HandleException(result.Exception, timeSpan, retryAttemptCount, context, this.Settings.HttpClientRetryCount);              
                        });
        }

        #endregion Private Methods
    }
}
