﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Http
{
    public class HttpClientAppSettings
    {
        public int HttpClientLifetimeSeconds { get; set; }
        public int HttpClientRetryCount { get; set; }
        public int HttpClientInitialRetryDelaySeconds { get; set; }
        public int HttpClientCacheTimeout { get; set; }
        public long CacheSize { get; set; }
    }
}
