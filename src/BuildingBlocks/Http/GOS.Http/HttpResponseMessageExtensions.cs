﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DfT.GOS.Http
{
    /// <summary>
    /// Extension methods for the System.Net.Http.HttpResponseMessage class
    /// </summary>
    public static class HttpResponseMessageExtensions
    {
        /// <summary>
        /// Creates an exception reporting details of the HttpResponseMessage
        /// </summary>
        /// <param name="httpResponseMessage">HttpResponseMessage instance to report on</param>
        /// <returns>Exception containing details of the unexpected HttpResponseMessage</returns>
        /// <remarks>Use this method to report unexpected responses when calling the microservices</remarks>
        public async static Task<Exception> GetUnexpectedResponseException(this HttpResponseMessage httpResponseMessage)
        {
            return new Exception(string.Format("Unexpected response received: Status Code = '{0}'; Content = '{1}'.", httpResponseMessage.StatusCode, await httpResponseMessage.Content.ReadAsStringAsync()));
        }
    }
}
