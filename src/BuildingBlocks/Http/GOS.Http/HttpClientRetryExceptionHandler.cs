﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Logging;
using Polly;
using System;

namespace DfT.GOS.Http
{
    /// <summary>
    /// Handles exceptions when Polly retries occur
    /// </summary>
    public class HttpClientRetryExceptionHandler
    {
        #region Properties

        private ILogger Logger { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="logger">The logger</param>
        public HttpClientRetryExceptionHandler(ILogger logger)
        {
            this.Logger = logger;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Handles an exception
        /// </summary>
        /// <param name="exception">The exception to handle</param>
        /// <param name="timeSpan">The timespan for the retry</param>
        /// <param name="retryAttemptCount">The attempted retry account from Polly</param>
        /// <param name="context">The Polly Context</param>
        public void HandleException(Exception exception, TimeSpan timeSpan, int retryAttemptCount, Context context, int maximumRetries)
        {
            if (exception != null)
            {
                string requestUri;
                if (context.ContainsKey(HttpPolicyContext.RequestUriKey))
                {
                    requestUri = context[HttpPolicyContext.RequestUriKey].ToString();
                }
                else
                {
                    requestUri = "[Unknown request URI]";
                }

                string requestMethod;
                if (context.ContainsKey(HttpPolicyContext.RequestMethod))
                {
                    requestMethod = context[HttpPolicyContext.RequestMethod].ToString();
                }
                else
                {
                    requestMethod = "[Unknown request method]";
                }

                this.Logger.LogWarning(exception, $"HTTP connection issue handled by Polly for URI {requestUri} ({requestMethod}). "
                    + $"This was the attempt {retryAttemptCount} after a period of {timeSpan}. The maximum number of retries for this request is {maximumRetries}. ");
            }
        }

        #endregion Methods
    }
}
