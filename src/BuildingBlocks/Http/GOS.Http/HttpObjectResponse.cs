﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Net;

namespace DfT.GOS.Http
{
    /// <summary>
    /// Provides a generic handler for a HTTP response with an object
    /// </summary>
    /// <typeparam name="T">The result type of the response</typeparam>
    public class HttpObjectResponse<T> : IHttpObjectResponse where T : class
    {
        #region Properties

        public HttpStatusCode HttpStatusCode { get; private set; }

        public T Result { get; private set; }

        /// <summary>
        /// Indicates whether a the response has a corresponding result value
        /// </summary>
        public bool HasResult
        {
            get { return this.Result != null; }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Creates a HTTP Object Result based upon a result of type T
        /// </summary>
        /// <param name="result">The object result of the response</param>
        public HttpObjectResponse(T result)
        {
            this.Result = result;
            this.HttpStatusCode = HttpStatusCode.OK;
        }

        /// <summary>
        /// Creates a HTTP Object Result indicating that no result was returned
        /// </summary>
        /// <param name="httpStatusCode">The status code</param>
        public HttpObjectResponse(HttpStatusCode httpStatusCode)
        {
            this.HttpStatusCode = httpStatusCode;
        }

        #endregion Constructors
    }
}
