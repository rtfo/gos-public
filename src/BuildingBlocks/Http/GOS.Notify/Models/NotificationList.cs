﻿using Newtonsoft.Json;
using System.Collections.Generic;
using DfT.GOS.Notify.Models.Responses;

namespace DfT.GOS.Notify.Models
{
    public class NotificationList
    {
        [JsonProperty("notifications")]
        public List<Notification> notifications;
        [JsonProperty("links")]
        public Link links;
    }
}