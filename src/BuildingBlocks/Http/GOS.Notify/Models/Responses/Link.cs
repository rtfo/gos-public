using Newtonsoft.Json;

namespace DfT.GOS.Notify.Models.Responses
{
    public class Link
    {
        [JsonProperty("current")]
        public string current;
        [JsonProperty("next")]
        public string next;
    }
}