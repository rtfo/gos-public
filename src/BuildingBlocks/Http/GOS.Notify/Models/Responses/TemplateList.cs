﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace DfT.GOS.Notify.Models.Responses
{
    public class TemplateList
    {
        [JsonProperty("templates")]
        public List<TemplateResponse> templates;
    }
}
