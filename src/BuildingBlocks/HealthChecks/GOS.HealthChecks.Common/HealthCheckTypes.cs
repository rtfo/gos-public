﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.HealthChecks.Common
{
    /// <summary>
    /// Common constants used to define the types of health check
    /// </summary>
    public static class HealthCheckTypes
    {
        /// <summary>
        /// Denotes a Liveness health check - failures cause Kubernetes to restart the pod
        /// </summary>
        public static readonly string Liveness = "liveness";

        /// <summary>
        /// Denotes a Readiness health check - failures cause Kubernetes to stop forwarding requests to the pod
        /// </summary>
        public static readonly string Readiness = "readiness";
    }
}
