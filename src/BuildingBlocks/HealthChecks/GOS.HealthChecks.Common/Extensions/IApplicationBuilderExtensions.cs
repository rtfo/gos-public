﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using System;

namespace DfT.GOS.HealthChecks.Common.Extensions
{
    /// <summary>
    /// Extension methods for the IApplicationBuilder interface that relate to health checks
    /// </summary>
    public static class IApplicationBuilderExtensions
    {
        public static void UseGosHealthChecks(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            app.UseHealthChecks("/health/ready", new HealthCheckOptions { Predicate = reg => reg.Tags.Contains(HealthCheckTypes.Readiness) });
            app.UseHealthChecks("/health/live", new HealthCheckOptions { Predicate = reg => reg.Tags.Contains(HealthCheckTypes.Liveness) });
        }
    }
}
