﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.HealthChecks.Common
{
    /// <summary>
    /// Base class for a health check implemented using a Background Service
    /// </summary>
    public class BackgroundServiceHealthCheck : IHealthCheck
    {
        #region Properties

        /// <summary>
        /// Flag indicating whether the startup task for this health check has completed successfully
        /// </summary>
        public bool StartupTaskCompleted { get; set; }

        #endregion Properties

        #region IHealthCheck implementation

        /// <summary>
        /// Performs a health check based on the StartupTaskCompleted flag
        /// </summary>
        /// <param name="context">Health Check context</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>Healthy status if the flag is set, Unhealthy status otherwise</returns>
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            return StartupTaskCompleted ? Task.FromResult(HealthCheckResult.Healthy()) : Task.FromResult(HealthCheckResult.Unhealthy());
        }

        #endregion IHealthCheck implementation
    }
}
