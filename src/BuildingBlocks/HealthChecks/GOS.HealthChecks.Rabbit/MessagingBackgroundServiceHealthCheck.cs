﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using DfT.GOS.HealthChecks.Common;

namespace DfT.GOS.HealthChecks.Rabbit
{
    /// <summary>
    /// Health Check used for the MessagingBackgroundService class
    /// </summary>
    public class MessagingBackgroundServiceHealthCheck : BackgroundServiceHealthCheck
    {
    }
}
