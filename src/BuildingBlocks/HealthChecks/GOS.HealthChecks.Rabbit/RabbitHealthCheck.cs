﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.HealthChecks.Rabbit
{
    /// <summary>
    /// Health Check for RabbitMQ service
    /// </summary>
    public class RabbitHealthCheck : IHealthCheck
    {
        #region Properties

        private readonly IConnectionFactory ConnectionFactory;
        private readonly ILogger<RabbitHealthCheck> Logger;
        private IConnection _connection;
        private IModel _channel;

        private IConnection Connection
        {
            get
            {
                if (_connection == null)
                {
                    _connection = this.ConnectionFactory.CreateConnection();
                }

                return _connection;
            }
        }       

        #endregion Properties

        #region Constructors

        public RabbitHealthCheck(IConnectionFactory connectionFactory, ILogger<RabbitHealthCheck> logger)
        {
            this.ConnectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));
            this.Logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #endregion Constructors

        #region IHealthCheck implementation

        /// <summary>
        /// Executes health check to determine whether the RabbitMQ service is available
        /// </summary>
        /// <param name="context">Health check context</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>Outcome of health check (Healthy or Unhealthy)</returns>
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                await Task.Run(() => this.GetChannel());
                cancellationToken.ThrowIfCancellationRequested();
                return new HealthCheckResult(HealthStatus.Healthy);
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "An error occurred whilst performing health check for RabbitMQ.");
                return new HealthCheckResult(HealthStatus.Unhealthy);
            }
        }

        #endregion IHealthCheck implementation

        #region Methods

        private IModel GetChannel()
        {
            if (_channel == null)
            {
                _channel = this.Connection.CreateModel();
            }

            if (_channel.IsClosed)
            {
                _channel.Dispose();
                _channel = this.Connection.CreateModel();
            }

            return _channel;
        }

        #endregion Methods
    }
}
