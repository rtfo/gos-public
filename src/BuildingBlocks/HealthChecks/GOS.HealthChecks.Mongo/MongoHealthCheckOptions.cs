﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.HealthChecks.Mongo
{
    /// <summary>
    /// Provides options for configuring the Mongo Health Check
    /// </summary>
    public class MongoHealthCheckOptions
    {
        #region Properties

        /// <summary>
        /// The name of the database to connect to during the health check execution
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// The timeout time, in milliseconds, for the ping to mongo to timeout before returning unhealthy
        /// </summary>
        public int ConnectionTimeoutMilliseconds { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="databaseName">The name of the database to connect to during the health check execution</param>
        /// <param name="connectionTimeoutMilliseconds">The timeout time, in milliseconds, for the ping to mongo to timeout before returning unhealthy</param>
        public MongoHealthCheckOptions(string databaseName
            , int connectionTimeoutMilliseconds)
        {
            this.DatabaseName = databaseName;
            this.ConnectionTimeoutMilliseconds = connectionTimeoutMilliseconds;
        }

        #endregion Constructors
    }
}
