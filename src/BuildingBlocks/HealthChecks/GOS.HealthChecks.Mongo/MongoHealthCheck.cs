﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.HealthChecks.Mongo
{
    /// <summary>
    /// Provides a Health Check for a service that is backed by a Mongo Database
    /// </summary>
    public class MongoHealthCheck : IHealthCheck
    {
        #region Properties

        private IMongoClient MongoClient { get; set; }
        private MongoHealthCheckOptions Options { get; set; }
        private ILogger<MongoHealthCheck> Logger { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Constructor taking all dependancies
        /// </summary>
        /// <param name="mongoClient">The mongo client instance to perform a health check on</param>
        /// <param name="options">Options used to configure the Mongo Health Check</param>
        /// <param name="logger">The Logger</param>
        public MongoHealthCheck(IMongoClient mongoClient, MongoHealthCheckOptions options, ILogger<MongoHealthCheck> logger)
        {
            this.MongoClient = mongoClient;
            this.Options = options;
            this.Logger = logger;
        }

        #endregion Constructors

        #region IHealthCheck Implementation

        /// <summary>
        /// Performs a health check, connecting to Mongo 
        /// </summary>
        /// <param name="context">The health check context</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Returns the whether the mongo connection is healthly</returns>
        async Task<HealthCheckResult> IHealthCheck.CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken)
        {
            bool isMongoLive = false;

            try
            {
                var database = this.MongoClient.GetDatabase(this.Options.DatabaseName);
                var pingMongoTask = database.RunCommandAsync((Command<BsonDocument>)"{ping:1}");

                if (await Task.WhenAny(pingMongoTask, Task.Delay(this.Options.ConnectionTimeoutMilliseconds)) == pingMongoTask)
                {
                    // Check that the operation completed without errors (throw any exceptions that occurred)
                    await pingMongoTask;

                    // The ping returned successfully before the timeout
                    isMongoLive = true;
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "An error occurred in the mongo healthcheck.");
            }

            if (isMongoLive)
            {
                return new HealthCheckResult(HealthStatus.Healthy);
            }
            else
            {
                return new HealthCheckResult(HealthStatus.Unhealthy);
            }
        }

        #endregion IHealthCheck Implementation
    }
}
