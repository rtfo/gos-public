﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.HealthChecks.Mongo
{
    /// <summary>
    /// Configuration Settings for Mongo Health Checks
    /// </summary>
    public class MongoHealthCheckConfiguration
    {
        /// <summary>
        /// The number of milliseconds to wait for Mongo to respond before timing out
        /// </summary>
        public int TimoutMilliseconds { get; set; }
    }
}
