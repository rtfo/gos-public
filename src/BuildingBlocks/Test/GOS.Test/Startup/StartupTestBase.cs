﻿using Microsoft.Extensions.Configuration;
using Moq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DfT.GOS.Test.Startup
{
    /// <summary>
    /// Provides a base class for testing API/Website startup classes
    /// </summary>
    public abstract class StartupTestBase
    {
        /// <summary>
        /// Gets an instance of the startup class for testing
        /// </summary>
        /// <returns></returns>
        private Startup GetStartupInstance(IDictionary<string, string> configurationItems)
        {
            var configuration = new ConfigurationBuilder();
            var mockConfiguration = new Mock<IConfiguration>();

            foreach (var configurationItem in configurationItems)
            {
                mockConfiguration.SetupGet(c => c[configurationItem.Key]).Returns(configurationItem.Value);
            }

            var mockLogger = new Mock<ILogger<Startup>>();
            return new Startup(mockConfiguration.Object, mockLogger.Object);
        }
    }
}
