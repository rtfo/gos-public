﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
namespace DfT.GOS.Data
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}
