﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System.Threading;
using System.Threading.Tasks;

namespace DfT.GOS.Data
{
    /// <summary>
    /// Provides a base implementation for a data service
    /// </summary>
    public abstract class DataServiceBase
    {
        #region Properties

        private IDbContext DataContext { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor, used for unit testing
        /// </summary>
        public DataServiceBase()
        {
        }

        /// <summary>
        /// Constructor taking a datacontext to enable all repositories to be saved within the same context / transaction
        /// </summary>
        /// <param name="dataContext"></param>
        public DataServiceBase(IDbContext dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Saves the changes in the dbContext to the database
        /// </summary>
        /// <returns>Returns the number of objects that were written back to the database</returns>
        protected async Task<int> SaveAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var numberOfObjectsWrittenToDatabase = 0;
            if (this.DataContext != null)
            {
                numberOfObjectsWrittenToDatabase = await this.DataContext.SaveChangesAsync(cancellationToken);
            }
            return numberOfObjectsWrittenToDatabase;
        }

        #endregion Methods
    }
}
