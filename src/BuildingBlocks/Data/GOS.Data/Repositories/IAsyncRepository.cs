﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DfT.GOS.Data.Repositories
{
    public interface IAsyncRepository<TEntity, TKey> where TEntity : class
    {
        Task<TEntity> GetAsync(TKey id);
        Task<TEntity> Find(Expression<Func<TEntity, bool>> match);
        Task<IList<TEntity>> FindAll(Expression<Func<TEntity, bool>> match);
        Task CreateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
    }
}
