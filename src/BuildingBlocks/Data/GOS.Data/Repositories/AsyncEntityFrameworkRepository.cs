﻿// Copyright (c) Crown Copyright (Department for Transport). Licensed under The MIT License (MIT).  See License file in the project root for license information.
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DfT.GOS.Data.Repositories
{
    /// <summary>
    /// Entity Framework implementation of the generic repository pattern.
    /// </summary>
    /// <typeparam name="TEntity">The type of entity we want a repository for</typeparam>
    /// <typeparam name="TKey">The type of the Key (ID) of the entity accessed through the repository</typeparam>
    public abstract class AsyncEntityFrameworkRepository<TEntity, TKey> : IAsyncRepository<TEntity, TKey>
        where TKey : IEquatable<TKey>
        where TEntity : class, IEntity<TKey>
    {
        #region Properties

        protected DbContext DataContext { get; set; }

        #endregion Properties

        #region Constructors

        public AsyncEntityFrameworkRepository(DbContext dataContext)
        {
            this.DataContext = dataContext;
        }

        #endregion Constructors

        #region IAsyncRepository Implementation

        public virtual async Task<TEntity> GetAsync(TKey id)
        {
            return await this.DataContext.Set<TEntity>().SingleOrDefaultAsync(e => e.Id.Equals(id));
        }

        public async Task CreateAsync(TEntity entity)
        {
            await this.DataContext.Set<TEntity>().AddAsync(entity);
        }

        public virtual async Task UpdateAsync(TEntity entity)
        {
            throw new NotSupportedException();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            TEntity existing = await this.DataContext.Set<TEntity>().FindAsync(entity);
            if (existing != null)
            {
                this.DataContext.Set<TEntity>().Remove(existing);
            }
        }

        public async Task<TEntity> Find(Expression<Func<TEntity, bool>> match)
        {
            return await this.DataContext.Set<TEntity>().SingleOrDefaultAsync(match);
        }

        public async Task<IList<TEntity>> FindAll(Expression<Func<TEntity, bool>> match)
        {
            return await this.DataContext.Set<TEntity>().Include(match).ToListAsync<TEntity>();
        }

        #endregion IAsyncRepository Implementation
    }
}
