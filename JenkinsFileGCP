
///////////////////////////////////////////////////////////
//
// Jenkins Job responsible for Building All Services
//
// Only changed Services are built
// Images are pushed to the container registry and tagged with
// the commit hash of the GIT repo
// The version (commit hash) of each service is then stored
// in another 'Tracking' git repo under the
// 'environment/branches/SOURCE_BRANCH' folder
//
///////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////
// Map of
// Affected Directory(Service) : Dockerfile
// or
// Affected Directory(Service) : Map of Affected Services
///////////////////////////////////////////////////////////
DEPENDENCIES = [
	"BuildingBlocks"	: ["GOS.Website", "GOS.Identity.API", "GOS.Organisations.API", "GOS.ReportingPeriods.API", "GOS.GhgFuels.API", "GOS.GhgApplications.API", "GOS.SystemParameters.API", "GOS.GhgCalculations.API", "GOS.GhgBalanceOrchestrator.API", "GOS.GhgBalanceView.API", "GOS.RtfoApplications.API", "GOS.GhgLedger.API", "GOS.RtfoApplications.Scheduler", "GOS.GhgMarketplace.API"],
	"GOS.Website" 		: "WebApps/GOS.Website",
	"GOS.Identity.API"	: "Services/Identity/GOS.Identity.API",
	"GOS.Organisations.API" : "Services/Organisations/GOS.Organisations.API",
	"GOS.ReportingPeriods.API" : "Services/ReportingPeriods/GOS.ReportingPeriods.API",
	"GOS.GhgFuels.API" : "Services/Fuels/GOS.GhgFuels.API",
	"GOS.GhgApplications.API" : "Services/Applications/GOS.GhgApplications.API",
	"GOS.SystemParameters.API" : "Services/SystemParameters/GOS.SystemParameters.API",
	"GOS.ClamAV" : "Services/Utilities/GOS.ClamAV",
	"GOS.GhgCalculations.API" : "Services/Calculations/GOS.GhgCalculations.API",
	"GOS.GhgBalanceOrchestrator.API" : "Services/Balance/GOS.GhgBalanceOrchestrator.API",
	"GOS.GhgBalanceView.API" : "Services/Balance/GOS.GhgBalanceView.API",
	"GOS.TlsCertificates" : "Utilities/GOS.TlsCertificates",
	"GOS.GhgLedger.API" : "Services/Ledger/GOS.GhgLedger.API",
	"GOS.RtfoApplications.API" : "Services/Applications/GOS.RtfoApplications.API",
	"GOS.RtfoApplications.Scheduler" : "Services/Applications/GOS.RtfoApplications.Scheduler",
	"GOS.GhgMarketplace.API": "Services/Marketplace/GOS.GhgMarketplace.API",
	"GOS.DBAnonymiser": "Utilities/GOS.DBAnonymiser",
	"GOS.Notifications.API": "Services/Notifications/GOS.Notifications.API"
]

////////////////////////////
// Unique set of services
// requiring build
////////////////////////////
SERVICES_TO_BUILD = [] as Set
IMAGES_TO_DELETE = [] as Set
BRANCH_HASH = ""

def String GetDockerFileName(String SERVICE, boolean FOR_TEST)
{
	PATH = DEPENDENCIES.get(SERVICE)

	if (FOR_TEST)
	{
		DOCKERFILE = "test/${PATH}.UnitTest/Dockerfile"
	}
	else
	{
		DOCKERFILE = "src/${PATH}/Dockerfile"
	}
	return DOCKERFILE
}

pipeline {
	agent {
		label 'docker_new'
	}

	environment {
		SLACK_TOKEN = credentials("ROSGOS_SLACK_TOKEN")
	}

	parameters {
		booleanParam(defaultValue: false, description: "Force a build all microservices regardless of what changed", name : 'BUILDALL')
		booleanParam(defaultValue: true, description: "Run Unit Tests", name : 'RUN_UNIT_TESTS')
		choice(choices: ['-alpine',''], description: "Choose Linux Distribution or Leave Blank for Debian", name : 'LINUX_SWITCH')
		gitParameter branchFilter: 'origin/(.*)', defaultValue: 'master', selectedValue: 'DEFAULT', quickFilterEnabled: true, name: 'SOURCE_BRANCH', type: 'PT_BRANCH', listSize: '20', description: 'NOTE: you may want to tick full rebuild if building a specific branch and this job normally only builds what changed in the latest commit by default'
	}

	stages {
		stage('Check Out SCM') {
		environment {
			GOS_REPO = credentials('RTFO_GOS_SOURCECODE_REPO_URL')
		}
			steps {
				bitbucketStatusNotify(buildState: 'INPROGRESS')
				deleteDir()
			  git branch: "${params.SOURCE_BRANCH}", credentialsId: "346c4743-d78e-484d-8998-f17a2f90f5a9", url: "${GOS_REPO}"
				script {
				BRANCH_HASH = sh (
					script: "git rev-parse HEAD",
					returnStdout: true).trim()
					sh "printf branch hash is ${BRANCH_HASH}"
					}
			}
		}

		////////////////////////////////////
		// Populates SERVICES_TO_BUILD with
		// a unique set of Service names
		// requiring building
		////////////////////////////////////
		stage('Identify Affected Services') {

			steps {
				////////////////////////////////////////
				// Build the Services that need building only
				////////////////////////////////////////
				script {

					if (params.BUILDALL)
					{
						///////////////////////////////////////////////////////
						// add all the services if we're forcing a build of all
						///////////////////////////////////////////////////////
						DEPENDENCIES.keySet().each {srv ->
							if (! (DEPENDENCIES.get(srv) instanceof ArrayList) )
								SERVICES_TO_BUILD.add(srv)
						}
					}
					else
					{
						/////////////////////////////
						// otherwise, find just affected services
						/////////////////////////////

						////////////////////////////////////
						// Get the file and path for every
						// change in the most recent checkin
						////////////////////////////////////
						String diff = sh (
							script: "git diff HEAD^ HEAD --name-only | sort -u | uniq",
							returnStdout: true)

						String[] changedFiles = diff.split("\\r?\\n")


						//////////////////////////////////////////
						// Compare each Dependent path to each
						// changed file to work out which service
						// has changed
						//////////////////////////////////////////

						changedFiles.each {chg ->

							DEPENDENCIES.keySet().each { srv ->
								if(chg.indexOf(srv)>0)
								{
									//////////////////////////////////////////
									// if the changed dependency is associated
									// with a map of Services, add all the
									// related services, otherwise it's a single
									// service to add
									//////////////////////////////////////////
									if (DEPENDENCIES.get(srv) instanceof ArrayList)
									{
										SERVICES_TO_BUILD.addAll(DEPENDENCIES.get(srv))
									}
									else
									{
										SERVICES_TO_BUILD.add(srv)
									}
								}
							}
						}
					}

					///////////////
					// Debug
					///////////////
					if(SERVICES_TO_BUILD.isEmpty())
					{
						echo "**** Nothing needs building ****"
					}
					else
					{
						echo "Need to build : "
						SERVICES_TO_BUILD.each  { srv ->
							echo srv
						}
					}

				}
			}
	   }


		////////////////////////////////
		// docker build each service and
		// any associated Unit Tests
		// and push to the registry
		// tagged with the current Git
		// Commit Hash
		////////////////////////////////
		stage('Build and Push to Registry') {
			environment {
							SERVICE_ACCOUNT = credentials('GCP_SERVICE_ACCOUNT')
							REGISTRY_NAME = "eu.gcr.io"
							PROJECT_ID = "dft-gos"
			}
			steps {
				script {
					if(!SERVICES_TO_BUILD.isEmpty())
					{
						sh "gcloud auth activate-service-account --key-file '$SERVICE_ACCOUNT' --project dft-gos"
						sh "gcloud auth configure-docker -q"

						SERVICES_TO_BUILD.each { service ->
								TAG1 = "${REGISTRY_NAME}/${PROJECT_ID}/" + service.toLowerCase() + ":" + BRANCH_HASH
								CONTAINER=""

								//try/finally to ensure
								//we publish the unit test results
								//whether they pass or fail
								try{
									//Look for a Dockerfile in the ./test/<PathToService> folder
									UNIT_TEST_DOCKERFILE = "${GetDockerFileName(service,true)}"
									if( params.RUN_UNIT_TESTS && fileExists (UNIT_TEST_DOCKERFILE) )
									{
										//tag the image <service>.unittest so we can then run it easily
										sh "docker build --tag '${service.toLowerCase()}.unittests.${BUILD_NUMBER}' -f '${UNIT_TEST_DOCKERFILE}' ."

										//Create the Container to obtain the Container ID before it's started.
										CONTAINER = sh (script: "docker create ${service.toLowerCase()}.unittests.${BUILD_NUMBER}", returnStdout: true).trim()
										sh "printf Unit Test Container is ${CONTAINER}"

										//Start the Container to run the tests (in attached mode so we capture and fail on error)
										sh (script: "docker start -a ${CONTAINER}", returnStdout: true).trim()
									}
									else
									{
										sh "printf No Unit test Dockerfile at ${UNIT_TEST_DOCKERFILE} - skipping"
									}

									APPLICATION_VERSION = sh (script: "cat APPLICATION_VERSION", returnStdout: true).trim()
									//build-arg BRANCH_HASH embeds a label in the image with the git commit hash (in addition to tagging the image with the same hash)
									sh "docker build --tag '${TAG1}' --build-arg LINUX_SWITCH=${LINUX_SWITCH} --build-arg BUILD_NUMBER=${BUILD_NUMBER} --build-arg APPLICATION_VERSION=${APPLICATION_VERSION} --build-arg GIT_COMMIT=${BRANCH_HASH} -f '${GetDockerFileName(service,false)}' ."
									sh "docker push '${TAG1}'"
								}
								finally
								{
									sh "printf Copying results.."

									if(!CONTAINER.isEmpty())
									{
										sh "docker cp ${CONTAINER}:/results/ ./"
										sh "chown -R jenkins:jenkins ./results/*"

										//Cleanup UnitTest Container and Image
										sh "docker rm -f ${CONTAINER}"
										//sh "docker rmi -f  ${service.toLowerCase()}.unittests.${BUILD_NUMBER} 2>/dev/null"
										IMAGES_TO_DELETE.add("${service.toLowerCase()}.unittests.${BUILD_NUMBER}")
									}

									//Cleanup Build Images
									//sh "docker rmi -f  ${TAG1} 2>/dev/null"
									IMAGES_TO_DELETE.add("${TAG1}")
								}
						}//each
					}
				}
			}
			post
			{
				always
				{
				 step([$class: 'MSTestPublisher', testResultsFile:"**/*.trx", failOnError: false, keepLongStdio: true])
				}
			}
		}

		/////////////////////////////////////////////////////////
		// populate a file for each service in the
		// ./environment/branches/SOURCE_BRANCH with the git commit hash
		// of the docker image pushed
		/////////////////////////////////////////////////////////
		stage ('Update Tracking Repo')
		{
			environment {
				TRACKING_REPO = credentials('RTFO_K8S_TRACKING_REPO')
			}
			steps
			{
				git credentialsId: "346c4743-d78e-484d-8998-f17a2f90f5a9", url: "${TRACKING_REPO}"

				script {
						if(!SERVICES_TO_BUILD.isEmpty())
						{
							SERVICES_TO_BUILD.each { service ->
								sh "mkdir -p branches/${SOURCE_BRANCH}"
								//try to avoid the new line an echo introduces
								//format of the tracking file is 1 file per service with contents service=gitcommit (this allows
								//them to be used as properties files more easily)
								sh "printf ${service.toLowerCase()}=${BRANCH_HASH} > branches/${SOURCE_BRANCH}/${service.toLowerCase()}"
								sh "git add branches/${SOURCE_BRANCH}/${service.toLowerCase()}"
							}

							sh "git commit -m 'Jenkins Build' || echo 'No Change'"
						}
				}

				////////////////////////////////////////////////////////////////////////////////////////////////////
				// This 'push' may decide to break one day !
				// Not sure why, but the other job failed with
				// fatal: could not read Password for 'https://rosgos@bitbucket.org': No such device or address
				// and the only way I could get round it was to wrap the following line in a withCredentials
				// and give it a special credential.helper. That job runs within Docker and this one doesn'tag
				// which is the only difference I can see.
				////////////////////////////////////////////////////////////////////////////////////////////////////
				//withCredentials([usernamePassword(credentialsId: '346c4743-d78e-484d-8998-f17a2f90f5a9', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
        //                sh('git push https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/rtfo/tracking-repo.git')
        //            }
				//sh("git push origin master")

				withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: '346c4743-d78e-484d-8998-f17a2f90f5a9',
                    usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
					sh("git config --global credential.helper '!echo password=\$PASSWORD; echo'")
					sh("git push origin master")
					sh("git config --global --unset credential.helper")
					}
			}
		}
	}
	post {

		success {
			script {
				if (!SERVICES_TO_BUILD.isEmpty())
				{
					slackSend (color: '#00FF00', channel: '#gosbuildanddeploy', message: "GOS BUILD #${env.BUILD_NUMBER} SUCCESS for services ${SERVICES_TO_BUILD} on branch ${SOURCE_BRANCH}", teamDomain: 'rosgos', token: "${env.SLACK_TOKEN}")
				}
				else
				{
					slackSend (color: '#00FF00', channel: '#gosbuildanddeploy', message: "GOS BUILD #${env.BUILD_NUMBER} Nothing Needed Building on branch ${SOURCE_BRANCH}", teamDomain: 'rosgos', token: "${env.SLACK_TOKEN}")
				}
				bitbucketStatusNotify(buildState: 'SUCCESSFUL')
			}
		}
		failure {
			slackSend (color: '#FF0000', channel: '#gosbuildanddeploy', message: "GOS BUILD #${env.BUILD_NUMBER} FAILED for services ${SERVICES_TO_BUILD} on on branch ${SOURCE_BRANCH} ${env.BUILD_URL}", teamDomain: 'rosgos', token: "${env.SLACK_TOKEN}")
			bitbucketStatusNotify(buildState: 'FAILED')
		}
		always
		{
			script
			{
				sh "printf Cleaning up Images"
				if(!IMAGES_TO_DELETE.isEmpty())
				{
					IMAGES_TO_DELETE.each{image ->
						sh "docker rmi -f  ${image} 2>/dev/null"
						}
				}
		 	}
		}
	}
}
